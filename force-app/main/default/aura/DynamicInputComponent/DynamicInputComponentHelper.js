({
    fireInputUpdateEvent : function(component, event, helper) {
        var thisObjectSend = component.get("v.thisObject");
        var dynamicInputValueUpdatedEvent = component.getEvent("dynamicInputValueUpdated");
        dynamicInputValueUpdatedEvent.setParams({
          "currentPageListIndexVar": component.get("v.currentPageListIndexVar"),
          "planIndexVar"           : component.get("v.planIndexVar"),
          "updateObject"           : thisObjectSend
        });
        dynamicInputValueUpdatedEvent.fire();
    }
})