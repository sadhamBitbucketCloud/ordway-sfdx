({
  doInit: function (component, event, helper) {
    var thisObjectSend = component.get("v.thisObject");
    var fieldKey = component.get("v.fieldKey");
    var thisRequiredFieldVar = component.get("v.thisRequiredField");
    if(thisRequiredFieldVar.isReadOnlyPickListField){
    }

    if (thisRequiredFieldVar.componentType != null 
      && thisObjectSend != null
      && thisObjectSend != undefined) {
      if (thisRequiredFieldVar.type != undefined
        && thisRequiredFieldVar.type == 'checkbox') {
        thisRequiredFieldVar.attributeObject["checked"] = thisObjectSend[fieldKey];
      }
      else {
        if(thisRequiredFieldVar.isReadOnlyPickListField){
          var thisPickListFieldValueLabelMap = thisRequiredFieldVar["pickListFieldValueLabelMap"];
          if(thisObjectSend[fieldKey]){
            thisRequiredFieldVar.attributeObject["value"] = thisPickListFieldValueLabelMap[thisObjectSend[fieldKey].toUpperCase()];
          }
        }
        else{
          thisRequiredFieldVar.attributeObject["value"] = thisObjectSend[fieldKey];      
        }
      }

      if(fieldKey === 'OrdwayLabs__BillingPeriod__c'
        && thisObjectSend['OrdwayLabs__ChargeType__c'] === 'One Time'){
        thisRequiredFieldVar.attributeObject["disabled"] = true;
        if(thisObjectSend[fieldKey] !== "N/A"){
          thisObjectSend[fieldKey] = "N/A";
          thisRequiredFieldVar.attributeObject["value"] = "N/A";
          component.set("v.thisObject", thisObjectSend);
          helper.fireInputUpdateEvent(component, event, helper);
        }
      }else{
        thisRequiredFieldVar.attributeObject["disabled"] = false;
      }

      thisRequiredFieldVar.attributeObject["onchange"] = component.getReference("c.onDynamicChange");
      thisRequiredFieldVar.attributeObject["aura:id"] = 'DynamicInputFieldAuraID';

      $A.createComponent(
        thisRequiredFieldVar.componentType,
        thisRequiredFieldVar.attributeObject,
        function (newInputField, status, errorMessage) {
          if (status === "SUCCESS") {
            var body = component.get("v.body");
            body.push(newInputField);
            component.set("v.body", body);
          }
          else if (status === "INCOMPLETE") {

          }
          else if (status === "ERROR") {

          }
        }
      );
    }
  },

  onDynamicChange: function (component, event, helper) {
    var fieldKeyToFireUpdateEvent = component.get("v.fieldKeyToFireUpdateEvent")
    var thisObjectSend = component.get("v.thisObject");
    var fieldKey = component.get("v.fieldKey");
    if (event.getSource().get("v.type") == 'checkbox') {
      thisObjectSend[fieldKey] = event.getSource().get("v.checked");
    }
    else {
      thisObjectSend[fieldKey] = event.getSource().get("v.value");
      if(fieldKey === 'OrdwayLabs__ChargeType__c'
        && thisObjectSend[fieldKey] === 'One Time'
        && thisObjectSend["OrdwayLabs__BillingPeriod__c"] !== "N/A"){
          thisObjectSend["OrdwayLabs__BillingPeriod__c"] = "N/A";
      }
    }
    component.set("v.thisObject", thisObjectSend);
    if(fieldKeyToFireUpdateEvent.includes(fieldKey)){
      helper.fireInputUpdateEvent(component, event, helper);
    }
  }
})