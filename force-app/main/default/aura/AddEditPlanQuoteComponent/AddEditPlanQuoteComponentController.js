({
  onInit: function (component, event, helper) {
    var pageRef = component.get("v.pageReference");
    
    component.set("v.quoteId", pageRef.state.c__QuoteID);
    component.set("v.quoteName", pageRef.state.c__QuoteName);
    component.set("v.pricebookId", pageRef.state.c__PlanPriceBookId);

    var workspaceAPI = component.find("addEditPlanComponent");
    workspaceAPI.isConsoleNavigation().then(function(response) {
      if(response){
        component.set("v.parentConsoleTabID", pageRef.state.c__ParentObjectTab);
        workspaceAPI.getEnclosingTabId().then(function(response) {
            workspaceAPI.setTabLabel({
                tabId: response,
                label: "Manage Plans"
            });

            workspaceAPI.setTabIcon({
                tabId: response,
                icon: "standard:product",
                iconAlt: "Manage Plans"
            });
        })
        .catch(function(error) {
            
        });
    }
    })
    .catch(function(error) {
        
    });

    helper.showSpinnerHelper(component, event, helper);
    helper.getOrdwayPlans(component, event, helper, pageRef.state.c__QuoteID, pageRef.state.c__PlanPriceBookId);
  },

  reInit: function (component, event, helper) {
    $A.get('e.force:refreshView').fire();
  },

  handleNext: function (component, event, helper) {
    component.set("v.currentPageNumber", Math.min(component.get("v.currentPageNumber") + 1, component.get("v.maxPageNumber")));
  },

  handleCancel: function (component, event, helper) {

    helper.showSpinnerHelper(component, event, helper);
    var workspaceAPI = component.find("addEditPlanComponent");
    workspaceAPI.isConsoleNavigation().then(function(response) {
      if(response){
        workspaceAPI.getFocusedTabInfo().then(function(response) {
          var parentTab = component.get("v.parentConsoleTabID");
          
          var focusedTabId = response.tabId;
          workspaceAPI.refreshTab({
              tabId: parentTab,
              includeAllSubtabs: true
          });
          workspaceAPI.focusTab({tabId : parentTab});
          workspaceAPI.closeTab({
            tabId: focusedTabId
        });
      })
      .catch(function(error) {
          
      });
    }
    else{
      var quoteIdVar = component.get("v.quoteId");
      var urlEvent = $A.get("e.force:navigateToURL");
      urlEvent.setParams({
        "url": "\/" + quoteIdVar
      });
      urlEvent.fire();
    }
    })
    .catch(function(error) {
        
    });
  },

  handlePrev: function (component, event, helper) {
    component.set("v.currentPageNumber", Math.max(component.get("v.currentPageNumber") - 1, 1));
  },

  handleSort: function (component, event, helper) {
    component.set("v.showSpinner", true);
    var columnName = event.target.dataset.columnname;
    if (columnName === "planId") {
      var isAscending = component.get("v.isPlanIdAsc");
      helper.sortTableByAttributeHelper(component, "planId", !isAscending);
      component.set("v.isPlanIdAsc", !isAscending);
      component.set("v.lastSortedBy", 'planId');
    } else if (columnName === "planName") {
      var isAscending = component.get("v.isPlanNameAsc");
      helper.sortTableByAttributeHelper(component, "planName", !isAscending);
      component.set("v.isPlanNameAsc", !isAscending);
      component.set("v.lastSortedBy", 'planName');
    }
  },

  planSelected: function (component, event, helper) {
    helper.showSpinnerHelper(component, event, helper);
    var thisPlan = event.getSource().get("v.name");
    var thisPlanId = thisPlan.planId;
    var thisplanName = thisPlan.planName;
    var thisplanList = thisPlan.planList;
    var thisPlanIdIsSelected = thisPlan.isSelected;

    var allPlanMapVar = component.get("v.allPlanMap");
    allPlanMapVar[thisPlanId] = thisPlan;
    component.set("v.allPlanList", Object.values(allPlanMapVar));
    component.set("v.allPlanMap", allPlanMapVar);

    var selectedPlanMapVar = component.get("v.selectedPlanMap");

    if (!thisPlanIdIsSelected &&
      selectedPlanMapVar.hasOwnProperty(thisPlanId)) {
      delete selectedPlanMapVar[thisPlanId];
      var thisQuoteLineItemIdsSet = component.get("v.quoteLineItemIdsToDelete");

      if (thisPlan.hasExistingPlan) {
        for (var i = 0; i < thisplanList.length; i++) {
          if (thisplanList[i].Id != null &&
            thisplanList[i].Id != undefined &&
            !thisQuoteLineItemIdsSet.includes(thisplanList[i].Id)) {
            thisQuoteLineItemIdsSet.push(thisplanList[i].Id);
            delete thisplanList[i].Id;
            delete thisplanList[i].UnitPrice;
          }
        }
      }
      for (var i = 0; i < thisplanList.length; i++) {
        thisplanList[i].isChargeSelected = false;
      }
      allPlanMapVar[thisPlanId].planList = thisplanList;
      allPlanMapVar[thisPlanId].hasExistingPlan = false;
      component.set("v.allPlanList", Object.values(allPlanMapVar));
      component.set("v.allPlanMap", allPlanMapVar);
    } else if (thisPlanIdIsSelected) {
      var thisPlanSelectedPlan = {};
      thisPlanSelectedPlan["planId"] = thisPlanId;
      thisPlanSelectedPlan["planName"] = thisplanName;
      thisPlanSelectedPlan["isSelected"] = thisPlanIdIsSelected;
      for (var i = 0; i < thisplanList.length; i++) {
        thisplanList[i].isChargeSelected = true;
      }
      thisPlanSelectedPlan["planList"] = thisplanList;
      selectedPlanMapVar[thisPlanId] = thisPlanSelectedPlan;

    }
    component.set("v.selectedPlanMap", selectedPlanMapVar);
    var selectedPlansOnly = Object.values(selectedPlanMapVar);
    component.set("v.selectedPlanList", selectedPlansOnly);
    var showAllrec = component.get("v.showAllRecord");

    if (!showAllrec && !thisPlanIdIsSelected) {
      var selectedListRecords = component.get("v.selectedPlanList");
      var pageSizeVar = component.get("v.pageSize");
      var pageNumber = component.get("v.currentPageNumber");
      var maxPage = Math.floor((selectedListRecords.length + pageSizeVar - 1) / pageSizeVar);
      component.set("v.maxPageNumber", Math.floor((selectedListRecords.length + pageSizeVar - 1) / pageSizeVar));
      if (pageNumber > maxPage) {
        component.set("v.currentPageNumber", pageNumber - 1);
      }
    }
    helper.reRenderTableList(component);
    helper.sortAfterPlanSelected(component, event, helper);
  },

  handleSearchFilter: function (component, event, helper) {
    component.set("v.showSpinner", true);
    var pageSizeVar = component.get("v.pageSize");
    component.set("v.isPlansAvailable", true);
    var searchResult =[];
    var allRecords = component.get("v.allPlanList");

    var requiredFieldsArray = ['OrdwayLabs__OrdwayProductId__c','OrdwayLabs__BillingPeriod__c','OrdwayLabs__ChargeName__c','OrdwayLabs__OrdwayListPrice__c','OrdwayLabs__PricingModel__c','OrdwayLabs__ChargeType__c'];
    try{
    var searchValue = component.get("v.searchFilterValue");
    if(searchValue
      && searchValue.trim().length >= 3 ){
        for(var i=0 ; i<allRecords.length ; i++){
          if(allRecords[i]['planId'].toUpperCase().includes(searchValue.toUpperCase()) || allRecords[i]['planName'].toUpperCase().includes(searchValue.toUpperCase())){
            searchResult.push(allRecords[i]);
          }
          else{
            var thisPlan = {};
            thisPlan["planId"] = allRecords[i]['planId'];
            thisPlan["planName"] = allRecords[i]['planName'];
            thisPlan["isSelected"] = false;
            thisPlan["hasExistingPlan"] = allRecords[i]['hasExistingPlan'];
            thisPlan["planList"] = [];
            for(var j=0 ; j<requiredFieldsArray.length ; j++){
              for(var k=0 ; k<allRecords[i]["planList"].length ; k++){
                if(allRecords[i]["planList"][k][requiredFieldsArray[j]]
                && allRecords[i]["planList"][k][requiredFieldsArray[j]].toString().toUpperCase().includes(searchValue.toUpperCase())){
                  thisPlan["planList"].push(allRecords[i]["planList"][k]);
                }
              }
            }
            if( thisPlan["planList"].length > 0){
              searchResult.push(thisPlan);
            }
          }
        }

        if(searchResult.length <= 0){
          component.set("v.isPlansAvailable", false);
        }
        else{
          component.set("v.searchFilteredList", searchResult);
          component.set("v.issearching", true);
          component.set("v.currentPageNumber", 1);
          var thisPageRecords = searchResult.slice((1 - 1) * pageSizeVar, 1 * pageSizeVar);
          component.set("v.currentPageList", thisPageRecords);
          component.set("v.maxPageNumber", Math.floor((searchResult.length + pageSizeVar - 1) / pageSizeVar));
        }
      }
      else if(!searchValue || searchValue.trim().length == 0){
        component.set("v.issearching", false);
        component.set("v.maxPageNumber", Math.floor((allRecords.length + pageSizeVar - 1) / pageSizeVar));
        helper.reRenderTableList(component);
      }
    }
    catch(err) {
      helper.handleError(component, err.message)
    }
    helper.hideSpinnerHelper(component);
  },

  chargeSelected: function (component, event, helper) {
    var thisCharge = event.getSource().get("v.name");
    var thisPlan = event.getSource().get("v.label");
    var thisPlanId = thisPlan.planId;
    var thisplanName = thisPlan.planName;
    var thisplanList = thisPlan.planList;
    var thisChargeIsSelected = thisCharge.isChargeSelected;
    var allPlanMapVar = component.get("v.allPlanMap");
    allPlanMapVar[thisPlanId] = thisPlan;
    component.set("v.allPlanList", Object.values(allPlanMapVar));
    component.set("v.allPlanMap", allPlanMapVar);
    var selectedPlanMapVar = component.get("v.selectedPlanMap");
    var thisQuoteLineItemIdsSet = component.get("v.quoteLineItemIdsToDelete");
    if (!thisChargeIsSelected && selectedPlanMapVar.hasOwnProperty(thisPlanId)) {
      for (var i = 0; i < thisplanList.length; i++) {
        if (thisplanList[i].OrdwayLabs__OrdwayChargeId__c == thisCharge.OrdwayLabs__OrdwayChargeId__c) {
          thisplanList[i].isChargeSelected = false;
          if (thisplanList[i].Id != null &&
            thisplanList[i].Id != undefined &&
            !thisQuoteLineItemIdsSet.includes(thisplanList[i].Id)) {
            thisQuoteLineItemIdsSet.push(thisplanList[i].Id);
            delete thisplanList[i].Id;
            delete thisplanList[i].UnitPrice;
          }
        }
      }
      allPlanMapVar[thisPlanId].planList = thisplanList;
      component.set("v.allPlanList", Object.values(allPlanMapVar));
      component.set("v.allPlanMap", allPlanMapVar);
      selectedPlanMapVar[thisPlanId].planList = thisplanList;
      component.set("v.selectedPlanMap", selectedPlanMapVar);
      var selectedPlansOnly = Object.values(selectedPlanMapVar);
      component.set("v.selectedPlanList", selectedPlansOnly);
      helper.reRenderTableList(component);
    } else if (thisChargeIsSelected) {
      if (!selectedPlanMapVar.hasOwnProperty(thisPlanId)) {
        var thisPlanSelectedPlan = allPlanMapVar[thisPlanId];
        for (var i = 0; i < thisplanList.length; i++) {
          if (thisplanList.length == 1) {
            thisPlanSelectedPlan.isSelected = true;
          } else if (thisplanList[i].OrdwayLabs__OrdwayChargeId__c == thisCharge.OrdwayLabs__OrdwayChargeId__c) {
            thisplanList[i].isChargeSelected = true;
          }
          break;
        }
        helper.reRenderTableList(component);
        allPlanMapVar[thisPlanId].isSelected = thisPlanSelectedPlan.isSelected;
        allPlanMapVar[thisPlanId].planList = thisplanList;
        thisPlanSelectedPlan.planList = thisplanList;
        selectedPlanMapVar[thisPlanId] = thisPlanSelectedPlan;
        component.set("v.selectedPlanMap", selectedPlanMapVar);
        var selectedPlansOnly = Object.values(selectedPlanMapVar);
        component.set("v.selectedPlanList", selectedPlansOnly);
        component.set("v.allPlanList", Object.values(allPlanMapVar));
        component.set("v.allPlanMap", allPlanMapVar);
      }
    }
    helper.reRenderTableList(component);
    helper.sortAfterPlanSelected(component, event, helper);
    helper.hideSpinnerHelper(component, event, helper);
  },

  reRenderTableList: function (component, event, helper) {
    helper.showSpinnerHelper(component, event, helper);
    helper.reRenderTableList(component);
  },

  savePlans: function (component, event, helper) {

    helper.showSpinnerHelper(component, event, helper);
    var selectedPlans = component.get("v.selectedPlanList");
    var isValid = true;
    for (var i = 0; i < selectedPlans.length; i++) {
      var thisPlanlist = selectedPlans[i]["planList"];
      for (var j = 0; j < thisPlanlist.length; j++) {

        if (thisPlanlist[j].isChargeSelected == true && !thisPlanlist[j].OrdwayLabs__Quantity__c) {
          isValid = false;
          break;
        }
      }
      if (!isValid) {
        break;
      }
    }
    if (!isValid) {
      helper.handleError(component, 'Quantity cannot be empty for selected charges');
      helper.hideSpinnerHelper(component, event, helper);
    } else {
      component.set("v.showAddButton", false);
      var action = component.get("c.addPlanToQuote");
      var finalOLIList = [];

      for (var i = 0; i < selectedPlans.length; i++) {
        var thisPlanlist = selectedPlans[i]["planList"];
        for (var j = 0; j < thisPlanlist.length; j++) {
          if (thisPlanlist[j].isChargeSelected == true) {
            finalOLIList.push(thisPlanlist[j]);
          }
        }
      }

      action.setParams({
        qliList: JSON.stringify(finalOLIList),
        quoteLineItemToDeleteIds: JSON.stringify(component.get("v.quoteLineItemIdsToDelete"))
      });

      action.setCallback(this, function (response) {
        var state = response.getState();

        if (state == "SUCCESS") {
          helper.hideSpinnerHelper(component, event, helper);
          helper.handleSuccess(component, "Plans updated successfully");
        } else {
          helper.hideSpinnerHelper(component, event, helper);
          helper.handleError(component, action.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    }
  },

  handleShowAllRecord: function (component, event, helper) {
    helper.resetSearchFilter(component, event, helper);
    helper.showSpinnerHelper(component, event, helper);
    var showAllrec = component.get("v.showAllRecord");
    var pageSizeVar = component.get("v.pageSize");
    component.set("v.currentPageNumber", 1);
    if (showAllrec) {
      var selectedPlan = component.get("v.selectedPlanList");
      component.set("v.maxPageNumber", Math.floor((selectedPlan.length + pageSizeVar - 1) / pageSizeVar));
      component.set("v.showAllRecord", false);
    } else {
      var allPlan = component.get("v.allPlanList");
      component.set("v.maxPageNumber", Math.floor((allPlan.length + pageSizeVar - 1) / pageSizeVar));
      component.set("v.showAllRecord", true);
    }
    component.set("v.isPlanIdAsc", true);
    helper.reRenderTableList(component);
  },

  handleShowPricingTiers: function (component, event, helper) {
    var thisTierPlan = event.target.name;
    var planName = event.target.title;

    let thisPlan = JSON.parse(JSON.stringify(thisTierPlan));
    thisPlan["tierList"] = JSON.parse(thisTierPlan.OrdwayLabs__TierJSON__c);
    var planIndex = event.currentTarget.getAttribute("data-planindex");
    component.set("v.selectedPlanToViewTierPlan", thisPlan);
    component.set("v.selectedPlanIndexVar", planIndex);
    component.set("v.tierPlanName", planName);
    component.set("v.showTierPrice", true);
    component.set("v.tierPlanId", thisPlan.OrdwayLabs__OrdwayPlanId__c);
    component.set("v.tierOrdwayProductId", thisPlan.OrdwayLabs__OrdwayProductId__c);
  },

  handleShowPricingTiers: function (component, event, helper) {
    var thisTierPlan = event.target.name;
    var planName = event.target.title;
    let thisPlan = JSON.parse(JSON.stringify(thisTierPlan));

    thisPlan["tierList"] = JSON.parse(thisTierPlan.OrdwayLabs__TierJSON__c);
    component.set("v.selectedPlanToViewTierPlan", thisPlan);
    component.set("v.tierPlanName", planName);
    component.set("v.showTierPrice", true);
    component.set("v.tierPlanId", thisPlan.OrdwayLabs__OrdwayPlanId__c);
    component.set("v.tierOrdwayProductId", thisPlan.OrdwayLabs__OrdwayProductId__c);
    helper.validateSelectedTierPlanFields(component);
  },

  changeNextPlanFromValue: function (component, event, helper) {
    component.set("v.showRecordMessage", false);
    var thisIndex = event.getSource().get("v.name");
    var selectedPlanlist = component.get("v.selectedPlanToViewTierPlan");

    if((selectedPlanlist["tierList"].length-1)  != thisIndex){
      selectedPlanlist["tierList"][thisIndex+1].starting_unit = selectedPlanlist["tierList"][thisIndex].ending_unit;
    }
    component.set("v.selectedPlanToViewTierPlan", selectedPlanlist);
    helper.validateSelectedTierPlanFields(component);
  },

  handleTierPriceUpdate: function (component, event, helper) {
    helper.validateSelectedTierPlanFields(component);
  },

  addTieringPlan: function (component, event, helper) {
    var selectedPlanlist = component.get("v.selectedPlanToViewTierPlan");
    selectedPlanlist["tierList"][selectedPlanlist["tierList"].length-1].ending_unit = (selectedPlanlist["tierList"][selectedPlanlist["tierList"].length-1].starting_unit * 1) + 1;
    selectedPlanlist["tierList"].push(
      {
        "type":"Per unit",
        "price":null,
        "ending_unit":null,
        "starting_unit":selectedPlanlist["tierList"][selectedPlanlist["tierList"].length-1].ending_unit,
        "tier":selectedPlanlist["tierList"].length+1
      }
    )
    component.set("v.selectedPlanToViewTierPlan", selectedPlanlist);
    helper.validateSelectedTierPlanFields(component);
  },

  deleteTierPlan: function (component, event, helper) {
    var indexToDelete = event.getSource().get("v.title");
    var tierPlanList = component.get("v.selectedPlanToViewTierPlan");
    tierPlanList["tierList"].splice(indexToDelete, 1);
    tierPlanList["tierList"][tierPlanList["tierList"].length - 1]["ending_unit"] = null;
    component.set("v.selectedPlanToViewTierPlan",tierPlanList );
    helper.validateSelectedTierPlanFields(component);
  },

  tierPlanUpdate: function (component, event, helper) {
    var planId = component.get("v.tierPlanId");
    var productId = component.get("v.tierOrdwayProductId");
    var allPlanMapVar = component.get("v.allPlanMap");
    var updatedTier = component.get("v.selectedPlanToViewTierPlan");
    var planIndex = component.get("v.selectedPlanIndexVar");


    updatedTier.tierList.forEach((element) => {
      if(element["starting_unit"]){element["starting_unit"] = parseFloat(element["starting_unit"]);}
      if(element["ending_unit"]){element["ending_unit"] = parseFloat(element["ending_unit"]);}
    });

    var updatedTierJSON = JSON.stringify(updatedTier.tierList);
    var hasEmptyEndingUnit = false;

    var tierPlanLength = updatedTier["tierList"].length;
    var allValid = component.find('tierfield').reduce(function (validSoFar, inputCmp) {
      inputCmp.reportValidity();
      return validSoFar && inputCmp.checkValidity();
    }, true);
    if (allValid) {
      for(var i=0; i<updatedTier["tierList"].length; i++){
        if( ( (i !== tierPlanLength -1) && (updatedTier["tierList"][i]["ending_unit"] == null || updatedTier["tierList"][i]["ending_unit"] == undefined))
        || (updatedTier["tierList"][i]["price"] == null || updatedTier["tierList"][i]["price"] == undefined)){
          hasEmptyEndingUnit = true;
          break;
        }
      }
      if(hasEmptyEndingUnit){
        helper.handleError(component, "Fields in previous price tiers must have values before a new row can be added.")
      }
      else{
        if(allPlanMapVar[planId] != null || allPlanMapVar[planId] != undefined){
          for(var i=0; i< allPlanMapVar[planId]["planList"].length; i++){
            if(allPlanMapVar[planId]["planList"][i]["OrdwayLabs__OrdwayProductId__c"] != null 
              && allPlanMapVar[planId]["planList"][i]["OrdwayLabs__OrdwayProductId__c"] != undefined
              && allPlanMapVar[planId]["planList"][i]["OrdwayLabs__OrdwayProductId__c"] == productId
              && planIndex == i){
                allPlanMapVar[planId]["planList"][i]["OrdwayLabs__TierJSON__c"] = updatedTierJSON;
              }
          }
        }
        
        helper.reRenderTableList(component);
        component.set("v.showTierPrice", false);
      }
    }
  },

  closeModel: function (component, event, helper) {
    component.set("v.showTierPrice", false);
    component.set("v.showRecordMessage", false);
  },

  refreshRecord: function (component, event, helper) {
    var msgType = component.get("v.messageType");

    if (msgType === 'error') {
      component.set("v.showRecordMessage", false);
      component.set("v.messageTitle", null);
      component.set("v.messageType", null);
      component.set("v.message", null);
      component.set("v.showAddButton", true);
    } else {
      helper.showSpinnerHelper(component, event, helper);
      var workspaceAPI = component.find("addEditPlanComponent");
      workspaceAPI.isConsoleNavigation().then(function(response) {
        if(response){
          workspaceAPI.getFocusedTabInfo().then(function(response) {
            var parentTab = component.get("v.parentConsoleTabID");
            var focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: parentTab,
                includeAllSubtabs: false
            });
            workspaceAPI.focusTab({tabId : parentTab});
            workspaceAPI.closeTab({
              tabId: focusedTabId
          });
        })
        .catch(function(error) {
            
        });
      }
      else{
        var quoteIdVar = component.get("v.quoteId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "\/" + quoteIdVar
        });
        urlEvent.fire();
      }
      })
      .catch(function(error) {
          
      });
    }
  }
})