({
	onInit: function (component, event, helper) {
		var pageRef = component.get("v.pageReference");
		component.set("v.parentId", pageRef.state.c__ParentId);
		component.set("v.parentName", pageRef.state.c__ParentName);
		component.set("v.pricebookId", pageRef.state.c__PlanPriceBookId);

    var workspaceAPI = component.find("addEditPlanComponent");
    workspaceAPI.isConsoleNavigation().then(function(response) {
      if(response){
        component.set("v.parentConsoleTabID", pageRef.state.c__ParentObjectTab);
        workspaceAPI.getEnclosingTabId().then(function(response) {
            workspaceAPI.setTabLabel({
                tabId: response,
                label: "Manage Plans"
            });
            workspaceAPI.setTabIcon({
                tabId: response,
                icon: "standard:product",
                iconAlt: "Manage Plans"
            });
        })
        .catch(function(error) {
            
        });
    }
    })
    .catch(function(error) {
        
    });
    
		helper.showSpinnerHelper(component, event, helper);
		helper.getOrdwayPlans(component, event, helper);
	},
  
  getUpdatedOrdwayPlans:function (component, event, helper){
    helper.showSpinnerHelper(component, event, helper);

    component.set("v.selectedPlanMap", {});
    component.set("v.allPlanTableSelectedPlanMap", {});
    component.set("v.allPlanTableSelectedPlanList", []);
    component.set("v.planToBundleMap", {});

    helper.getOrdwayPlans(component, event, helper);
  },

  tabChangeEvent:function (component, event, helper) {
    helper.showSpinnerHelper(component, event, helper);
    component.set("v.showTabs", false);
    component.set("v.showTabs", true);
    helper.hideSpinnerHelper(component, event, helper);
  },

	reInit: function (component, event, helper) {
		$A.get('e.force:refreshView').fire();
	},

  handleSearchFilter: function (component, event, helper) {
  try{
    
    var allPlanTable = component.find("planPickerAllPlansTable");
    var searchFilterTextVar = component.get("v.searchFilterValue");
    
    allPlanTable.handleSearchMethod(searchFilterTextVar);
  }
  catch(error){
    helper.hideSpinnerHelper(component);
    console.error('Method reRenderTableList ' + error.name + ': ' + error.message);
    helper.handleError(component, 'Method handleSearchFilter ' + error.name + ': ' + error.message);
  }
  },
  handleDone: function (component, event, helper) {
    helper.showSpinnerHelper(component, event, helper);
    var parentRecordId = component.get("v.parentObject").Id;
    var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
        "url": "\/" + parentRecordId
    });
    urlEvent.fire();
    
  },
  

  handleCancel: function (component, event, helper) {
		helper.showSpinnerHelper(component, event, helper);
    var workspaceAPI = component.find("addEditPlanComponent");
    workspaceAPI.isConsoleNavigation().then(function(response) {
      if(response){
        workspaceAPI.getFocusedTabInfo().then(function(response) {
          var parentTab = component.get("v.parentConsoleTabID");
          var focusedTabId = response.tabId;
          workspaceAPI.refreshTab({
              tabId: parentTab,
              includeAllSubtabs: true
          });
          workspaceAPI.focusTab({tabId : parentTab});
          workspaceAPI.closeTab({
            tabId: focusedTabId
        });
      })
      .catch(function(error) {
          
      });
    }
    else{
      var parentRecordId = component.get("v.parentObject").Id;
			var urlEvent = $A.get("e.force:navigateToURL");
			urlEvent.setParams({
					"url": "\/" + parentRecordId
			});
			urlEvent.fire();
    }
    })
    .catch(function(error) {
        
    });
  },

  handleUtilityApplicationEvent : function(component, event, helper){
    try{
      if( event.getParam("eventSource") === "PlanPickerTableHelper"
        && event.getParam("targetSource") ==="PlanPickerController"){
          component.set("v.showTabs", false);
          component.set("v.showTabs", true);
        }
    }
    catch(error){
      helper.hideSpinnerHelper(component);
      helper.handleError(component, 'Method handleApplicationEvent ' + error.name + ': ' + error.message);
    }
  }
})