({
	showSpinnerHelper: function (component, event, helper) {
		component.set("v.showSpinner", true);
	},
	
	hideSpinnerHelper: function (component) {
		component.set("v.showSpinner", false);
	},

	getOrdwayPlans: function (component, event, helper) {

		var action = component.get("c.getInitialWrapper");

    action.setParams({
      parentRecordId: component.get("v.parentId"),
      pricebookId: component.get("v.pricebookId")
    });

    action.setCallback(this, function (response) {
      var selectedPlanMapVar = component.get("v.selectedPlanMap");
      var planToBundleMapvar = component.get("v.planToBundleMap");

      try{
          var state = response.getState();
          if (state == "SUCCESS" && response.getReturnValue() != null) {
          var thisInitialWrapper = response.getReturnValue();          
          //Dynamic Field Map
          component.set("v.currencyISOCode", thisInitialWrapper.currencyISO);
          var fieldWrapperMap = thisInitialWrapper.planPickerFieldWrapper;
          component.set("v.dynamicFieldsWrapper", Object.values(fieldWrapperMap));

          component.set("v.chargeTiersMap", thisInitialWrapper.chargeTiersMap);
          component.set("v.lineItemTiersMap", thisInitialWrapper.lineItemTiersMap);
          component.set("v.lineItemTierRelationShipAPIName", thisInitialWrapper.lineItemTierRelationShipAPIName);
        
          component.set("v.requiredFieldsSize", Object.values(fieldWrapperMap).length);
         
          component.set("v.parentObject", thisInitialWrapper.parentObject);

          //Ordway OLI Plans
          if (thisInitialWrapper.lineItemMap != null) {
            var planMap = {};
            var planList = [];

            var plansMapObject = thisInitialWrapper.lineItemMap;
            var keyList = Object.keys(plansMapObject).sort();

            for (var i = 0; i < keyList.length; i++) {
              var thisPlan = {};
              var thisPlanIdValue = keyList[i].split("_")[0];
              thisPlan["planId"] = keyList[i].split("_")[0];
              thisPlan["planName"] = keyList[i].split("_")[1];
              thisPlan["isSelected"] = false;
              thisPlan["hasExistingPlan"] = false;
              
              var thisPlanForAllPlanMapVar = {};
              var uniqueChargeKey;
              var planBundleId;
              for (let j = 0; j < plansMapObject[keyList[i]].length; j++) {
                uniqueChargeKey = plansMapObject[keyList[i]][j].OrdwayLabs__OrdwayPlanId__c+'_'+plansMapObject[keyList[i]][j].OrdwayLabs__OrdwayChargeId__c;
                plansMapObject[keyList[i]][j].isChargeSelected = false;
                if(!Object.keys(thisPlanForAllPlanMapVar).includes(uniqueChargeKey)
                  && plansMapObject[keyList[i]][j]["Id"] == undefined){
                  thisPlanForAllPlanMapVar[uniqueChargeKey] = JSON.parse(JSON.stringify(plansMapObject[keyList[i]][j]));
                }

                if(plansMapObject[keyList[i]][j]["OrdwayLabs__PlanBundleID__c"] == null
                  || plansMapObject[keyList[i]][j]["OrdwayLabs__PlanBundleID__c"] == undefined){
                    
                    planBundleId = thisPlan["planId"]+'_1';
                    plansMapObject[keyList[i]][j]["OrdwayLabs__PlanBundleID__c"] = planBundleId;
                  }
                  else{
                    planBundleId = plansMapObject[keyList[i]][j].OrdwayLabs__PlanBundleID__c;
                  }

                if(!Object.keys(planToBundleMapvar).includes(thisPlan["planId"])){
                  planToBundleMapvar[thisPlan["planId"]] = [];
                }
                if(!planToBundleMapvar[thisPlan["planId"]].includes(planBundleId)){
                  planToBundleMapvar[thisPlan["planId"]].push(planBundleId);
                  planToBundleMapvar[thisPlan["planId"]] = planToBundleMapvar[thisPlan["planId"]].sort();
                }
                
                var thisPlanClone =  JSON.parse(JSON.stringify(thisPlan));
                plansMapObject[keyList[i]][j]["dynamicChargeId"] = 'DYNAMIC_CHARGE_'+(j+1);
                thisPlanClone["planBundleId"] = planBundleId;

                if (plansMapObject[keyList[i]][j].Id != undefined
                || plansMapObject[keyList[i]][j].OrdwayLabs__ContractLineItem__c != undefined) {
                  
                  thisPlanClone["hasExistingPlan"] = true;
                  thisPlanClone["isSelected"]      = true;
                  plansMapObject[keyList[i]][j].isChargeSelected = true;
                  //thisPlanClone["planList"]        = plansMapObject[keyList[i]];
                  if(!Object.keys(selectedPlanMapVar).includes(planBundleId)){
                    thisPlanClone["planList"] = [];
                    selectedPlanMapVar[planBundleId] = thisPlanClone;
                  }

                  var existingSelectedBundle = selectedPlanMapVar[planBundleId]["planList"];

                  existingSelectedBundle.push(plansMapObject[keyList[i]][j]);
                  selectedPlanMapVar[planBundleId]["planList"] = existingSelectedBundle;
                }
              }

              thisPlan["planList"] = Object.values(thisPlanForAllPlanMapVar);
              planMap[thisPlanIdValue] = thisPlan;
              planList.push(thisPlan);
            }

            component.set("v.selectedPlanMap", selectedPlanMapVar);
            var selectedPlansOnly = Object.values(selectedPlanMapVar);

            if (planList.length > 0 || selectedPlansOnly.length > 0) {
              
              component.set("v.planToBundleMap", planToBundleMapvar);
              component.set("v.selectedPlanList", selectedPlansOnly);
              component.set("v.allPlanList", planList);
              component.set("v.allPlanMap", planMap);
              component.set("v.isPlansAvailable", true);
              
              var selectedTab = selectedPlansOnly.length > 0 ? 'selectedPlansTabSetId' : 'allPlansTabSetId';
              component.set("v.defaultTabId", selectedTab);
              component.set("v.showTabs", true);
              this.hideSpinnerHelper(component);
            }
            else{
              component.set("v.isPlansAvailable", false);
              this.hideSpinnerHelper(component);
            }
          }
          component.set("v.salesforeOrgBaseUrl", thisInitialWrapper.salesforceBaseURL);
          
        } else {
          this.hideSpinnerHelper(component);
          this.handleError(component, JSON.stringify(action.getError(), 0, 2));
        }
      }
      catch(error){
        this.hideSpinnerHelper(component);
        this.handleError(component, error.name + ': '+' Line : '+ error.lineNumber +' : '+ error.message);
      }
      
    });
    $A.enqueueAction(action);
	},
  
  handleSuccess: function (component, successMessage) {
    component.set("v.messageTitle", "Success!");
    component.set("v.messageType", "success");
    component.set("v.message", successMessage);
    component.set("v.showRecordMessage", true);
    window.setTimeout(function(){ component.set("v.showRecordMessage", false); }, 3000);

  },

  handleError: function (component, errorMessage) {
    component.set("v.messageTitle", "Error");
    component.set("v.messageType", "error");
    component.set("v.message", errorMessage);
    component.set("v.showRecordMessage", true);
    window.setTimeout(function(){ component.set("v.showRecordMessage", false); }, 3000);

  },

})