({

  checkPermSetAssignment: function (component, event, helper) {
		var action = component.get("c.isPermissionSetAssigned");
		action.setCallback(this, function (response) {
			var state = response.getState();

			if (state === "SUCCESS" &&
				response.getReturnValue()) {
        component.set("v.isPermissionSetAssigned", true);
        helper.subscribe(component, event, helper);
			}
		});
		$A.enqueueAction(action);
  },

  checkEditAccess: function (component, event, helper) {
    var action = component.get("c.hasEditPermission");
    
    action.setParams({
      quoteId: component.get("v.recordId")
    });
		action.setCallback(this, function (response) {
			var state = response.getState();

			if (state === "SUCCESS" &&
				response.getReturnValue()) {
          component.set("v.hasEditAccess", true);
			}
		});
		$A.enqueueAction(action);
  },

  hasQuoteFeature: function (component, event, helper) {
    var action = component.get("c.hasQuotesEnabled");
    
		action.setCallback(this, function (response) {
			var state = response.getState();

			if (state === "SUCCESS" &&
				response.getReturnValue()) {
          component.set("v.hasQuoteLicense", true);
			}
		});
		$A.enqueueAction(action);
  },

  getQuotePermission: function (component, event, helper) {
    var action = component.get("c.getButtonPermission");
    
		action.setCallback(this, function (response) {
      var state = response.getState();
      var permission = response.getReturnValue();
      console.log(JSON.stringify(permission, 0, 2));
			if (state === "SUCCESS") {
        component.set("v.syncQuote", permission.SyncQuote);
        component.set("v.activateQuote", permission.ActivateSendQuote);
        component.set("v.cancelQuote", permission.CancelQuote);
        component.set("v.convertQuote", permission.ConvertQuote);
        component.set("v.previewQuote", permission.TemplatePreviewQuote);
        component.set("v.templateQuote", permission.TemplatePreviewQuote);
        component.set("v.viewInOrdway", permission.ViewQuoteInOrdway);
			}
		});
		$A.enqueueAction(action);
  },

  getPlanPickerConfiguration: function (component, event, helper) {
    var action = component.get("c.hasPlanPickerEnabled");
    
		action.setCallback(this, function (response) {
			var state = response.getState();

			if (state === "SUCCESS" &&
				response.getReturnValue()) {
          component.set("v.showPlanPicker", response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
  },

  subscribe: function (component, event, helper) {
		const empApi = component.find('empApi');
		const channel = component.get('v.quoteChannel');
		const replayId = -1;
	
		const callback = function (message) {
			helper.onReceiveNotification(component, message);
    };
    
		empApi.subscribe(channel, replayId, $A.getCallback(callback)).then($A.getCallback(function (newSubscription) {
			component.set('v.quoteRec', newSubscription);
		}));
	},
 
	onReceiveNotification: function (component, message) {
			
		if(component.get('v.recordId') === message.data.payload.OrdwayLabs__RecordId__c){
      if(message.data.payload.OrdwayLabs__Status__c ===  'In Sync'){
       this.handleSuccess(component, message.data.payload.OrdwayLabs__Message__c);
      }
      else{
       this.handleError(component, message.data.payload.OrdwayLabs__Message__c);
      }
		}
	},
  
	redirectToAddPlanPage: function (component, planPriceBookID) {
		var quoteRecordVar = component.get("v.quoteRecordVal");
    var pageReferenceObj;

    if(component.get("v.showPlanPicker")){
      pageReferenceObj = {
        type: 'standard__component',
        attributes: {
          componentName: 'OrdwayLabs__PlanPicker',
        },
        state: {
          "c__ParentId": quoteRecordVar.Id,
          "c__ParentName": quoteRecordVar.Name,
          "c__PlanPriceBookId": quoteRecordVar.OrdwayLabs__PriceBook2Id__c
        }
      };
    }
    else{
      pageReferenceObj = {
        type: 'standard__component',
        attributes: {
          componentName: 'OrdwayLabs__AddEditPlanQuoteComponent',
        },
        state: {
          "c__QuoteID": quoteRecordVar.Id,
          "c__QuoteName": quoteRecordVar.Name,
          "c__PlanPriceBookId": quoteRecordVar.OrdwayLabs__PriceBook2Id__c
        }
      };
    }
    
    var workspaceAPI = component.find("ordwayButtonComponentWorkSpace");
    workspaceAPI.isConsoleNavigation().then(function(response) {
        if(response){
          workspaceAPI.getFocusedTabInfo().then(function(focusedTab) {
            
            pageReferenceObj.state["c__ParentObjectTab"] = focusedTab.tabId;
            workspaceAPI.openSubtab({
                parentTabId: focusedTab.tabId,
                pageReference: pageReferenceObj
            }).catch(function(error) {
                
            });
        });
      }
      else{
        var navService = component.find("navService");
        navService.navigate(pageReferenceObj);
      }
    })
    .catch(function(error) {
        
    });
  },
  
  handleSyncToOrdwayHelper: function (component, event, helper) {
    
    var quote = component.get("v.quoteRecordVal");
  
    if (quote.OrdwayLabs__TotalQuantity__c === 0) {
      this.handleError(component,'Please add at least one product');
    }
    else {
      helper.showSpinnerHelper(component, event, helper);
      var action = component.get("c.syncToOrdway");
      action.setParams({
        quoteRecordId: component.get("v.recordId")
      });
    
      action.setCallback(this, function (response) {
        var state = response.getState();
    
        if (state === "SUCCESS") {
          this.handleSuccess(component,'Quote Synced Successfully with Ordway');
        } 
        else {
          this.handleError(component, action.getError()[0].message);
        }
        this.hideSpinnerHelper(component, event, helper);
      });
      $A.enqueueAction(action);
    }
  },

  handleActivateQuote: function (component, event, helper) {

    var quote = component.get("v.quoteRecordVal");

    if (!quote.OrdwayLabs__QuoteTemplateId__c) {
      this.handleError(component, 'Please select a template');
    }
    else {
      
      helper.showSpinnerHelper(component, event, helper);

      var action = component.get("c.activateQuote");
      action.setParams({
        quoteId: component.get("v.recordId")
      });
    
      action.setCallback(this, function (response) {
        var state = response.getState();
    
        if (state === "SUCCESS") {
          this.handleSuccess(component,'Quote Activated Successfully');
        } 
        else {
          this.handleError(component, action.getError()[0].message);
        }
        this.hideSpinnerHelper(component, event, helper);
      });
      $A.enqueueAction(action);
    }
  },

  handleConvertQuote: function (component, event, helper) {
    this.showSpinnerHelper(component, event, helper);
    var action = component.get("c.convertQuote");
    action.setParams({
      quoteId: component.get("v.recordId")
    });
   
    action.setCallback(this, function (response) {
      var state = response.getState();
  
      if (state === "SUCCESS") {
        this.handleSuccess(component,'Quote Converted Successfully');
      } 
      else {
        this.handleError(component, action.getError()[0].message);
      }
      this.hideSpinnerHelper(component, event, helper);
    });
    $A.enqueueAction(action);
  },

  getNamedCredentialHelper : function (component, event, helper) {
		var action = component.get("c.getNamedCredential");
		action.setCallback(this, function (response) {
			var state = response.getState();
      if (state === "SUCCESS" && response.getReturnValue()) {
        component.set("v.namedCredential", response.getReturnValue())
      }
		});
		$A.enqueueAction(action);
	},

  handleSuccess : function(component, successMessage) {
		component.set("v.messageTitle", "Success!");
		component.set("v.messageType", "success");
		component.set("v.message", successMessage);
		component.set("v.showRecordMessage", true);
	},

	handleError: function (component, errorMessage) {
		component.set("v.messageTitle", "Error");
		component.set("v.messageType", "error");
		component.set("v.message", errorMessage);
		component.set("v.showRecordMessage", true);
  },
  
  showSpinnerHelper : function(component, event, helper) {
		component.set("v.showSpinner", true);
	},

	hideSpinnerHelper : function(component, event, helper) {
		component.set("v.showSpinner", false);
	}
})