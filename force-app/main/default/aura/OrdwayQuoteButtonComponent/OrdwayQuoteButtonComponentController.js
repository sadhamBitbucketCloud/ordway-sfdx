({
  onInit: function (component, event, helper) {
		component.find('quoteRecord').reloadRecord(true);
    helper.checkPermSetAssignment(component, event, helper);
    helper.checkEditAccess(component, event, helper);
    helper.getQuotePermission(component, event, helper);
    helper.hasQuoteFeature(component, event, helper);
    helper.getNamedCredentialHelper(component, event, helper);
    helper.getPlanPickerConfiguration(component, event, helper);
  },
  
	onRecordUpdated: function (component, event, helper) {
    var changeType = event.getParams().changeType;
    if (changeType === "ERROR") {  
      helper.handleError(component, component.get("v.recordLoadError"));
    }
    else{
      var quoteRecordVal = component.get("v.quoteRecordVal");
      if(quoteRecordVal){
        component.set("v.selectedTemplateId", quoteRecordVal.OrdwayLabs__QuoteTemplate__c);
        component.set("v.quoteRecordVal", component.get("v.quoteRecordVal"));
      }
    }
	},

	handleSelectTemplate: function (component, event, helper) {
		var modalBody;
		var quoteRecordVal = component.get("v.quoteRecordVal");

		$A.createComponent("c:QuoteSelectTemplate", {"quoteRecordVal" : quoteRecordVal },
      function(content, status) {
        if (status === "SUCCESS") {
          modalBody = content;
          component.find('overlayLib').showCustomModal({
              header: "Select Quote Template",
              body: modalBody, 
              showCloseButton: true,
              closeCallback: function() {
              }
          });
        }
			});
  },
  
  handleView: function (component, event, helper) {
		var quoteId = component.get("v.quoteRecordVal").OrdwayLabs__OrdwayQuoteId__c;
		var namedCredentialVar = component.get("v.namedCredential");
		var URL = namedCredentialVar+'/quotes/'+quoteId;
		var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": URL
    });
    urlEvent.fire();
	},

	handlePreview: function (component, event, helper) {
		var quoteRecordVal = component.get("v.quoteRecordVal");
		var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": quoteRecordVal.OrdwayLabs__PreviewURL__c
    });
    urlEvent.fire();
	},

  handleAddEditPlanClick: function (component, event, helper) {
    
		var quoteRecordVal = component.get("v.quoteRecordVal");
    

		if (quoteRecordVal.OrdwayLabs__PriceBook2Id__c) {
			helper.redirectToAddPlanPage(component, quoteRecordVal);
		} else {
			var selectPriceBookToastEvent = $A.get("e.force:showToast");
			selectPriceBookToastEvent.setParams({
				"mode": 'sticky',
				"title": "Error",
				"type": "error",
				"message": $A.get("$Label.c.QuoteAddPlanPriceBookWarningMessage")
			});
			selectPriceBookToastEvent.fire();
		}
  },
  
  handleSyncToOrdway: function (component, event, helper) {

    var quote = component.get("v.quoteRecordVal");
  
    if (!quote.OrdwayLabs__OrdwayQuoteId__c) {
			helper.handleSyncToOrdwayHelper(component, event, helper);
    }
    else {
      var navService = component.find("navService");
      var pageReference = {
        type: 'standard__component',
        attributes: {
            componentName: 'OrdwayLabs__OrdwayFieldComparison',
        },
        state: {
            "c__parentRecordObject": quote,
            "c__isSync": true
        }
      };
      event.preventDefault();
      navService.navigate(pageReference);
    }	
  },

  activate_Quote: function (component, event, helper) {
    
    var quote = component.get("v.quoteRecordVal");

    if (quote.OrdwayLabs__OrdwayQuoteId__c) {
			helper.handleActivateQuote(component, event, helper);
    }
  },

  cancel_Quote: function (component, event, helper) {
    var quote = component.get("v.quoteRecordVal");
    if(quote.OrdwayLabs__OrdwayQuoteId__c) {
      var modalBody;
      $A.createComponent("c:CancelQuoteConfirmationModal", {"quoteId" : component.get("v.recordId")},
        function(content, status) {
          if (status === "SUCCESS") {
            modalBody = content;
            component.find('overlayLib').showCustomModal({
                header: "Cancel Ordway Quote?",
                body: modalBody, 
                showCloseButton: true,
                closeCallback: function() {
                }
            });
          }
        });
      }
  },

  convert_Quote: function (component, event, helper) {
    var quote = component.get("v.quoteRecordVal");
    if(quote.OrdwayLabs__Opportunity__r.OrdwayLabs__OrdwaySubscriptionID__c){
      
      var modalBody;
      $A.createComponent("c:ConvertQuoteConfirmation", {
        "quoteId" : quote.Id,
        "subscriptionId" : quote.OrdwayLabs__Opportunity__r.OrdwayLabs__OrdwaySubscriptionID__c
        },
        function(content, status) {
          if (status === "SUCCESS") {
            modalBody = content;
            component.find('overlayLib').showCustomModal({
                header: "Convert Ordway Quote?",
                body: modalBody, 
                showCloseButton: true,
                closeCallback: function() {
                }
            });
          }
        });
    }
    else if(quote.OrdwayLabs__OrdwayQuoteId__c) {
			helper.handleConvertQuote(component, event, helper);
    }
  },

  handleUpdateFromOrdway: function (component, event, helper) {

    helper.showSpinnerHelper(component, event, helper);

    var quote = component.get("v.quoteRecordVal");
  
    var navService = component.find("navService");
    var pageReference = {
      type: 'standard__component',
      attributes: {
          componentName: 'OrdwayLabs__OrdwayFieldComparison',
      },
      state: {
          "c__parentRecordObject": quote,
          "c__isSync": false
      }
    };
    event.preventDefault();
    navService.navigate(pageReference);	
  },
	
	refreshRecord : function (component, event, helper) {
		component.set("v.showRecordMessage", false);
		component.set("v.messageTitle", "");
		component.set("v.messageType", "");
		component.set("v.message", "");
	  $A.get('e.force:refreshView').fire();
  }
})