({
	onInit: function (component, event, helper) {
		try {
			helper.showSpinnerHelper(component, event, helper);
			// component.set("v.allPlanList", JSON.parse(JSON.stringify(component.get("v.parentAllPlanList"))));
			// component.set("v.allPlanMap", JSON.parse(JSON.stringify(component.get("v.parentAllPlanMap"))));

			var isSelectedTab = component.get("v.isSelectedTable");
			if(isSelectedTab){
				helper.initSelectedPlanHelper(component, event, helper);
			}
			else{
				helper.initAllPlanHelper(component, event, helper);
			}
			
		} catch (error) {
			helper.hideSpinnerHelper(component);
      console.error('Method initAllPlanHelper ' + error.name + ': ' + error.message);
      helper.handleError(component, 'Method initAllPlanHelper ' + error.name + ': ' + error.message);
		}
	
	},

	handlePrev: function (component, event, helper) {
    component.set("v.currentPageNumber", Math.max(component.get("v.currentPageNumber") - 1, 1));
  },

	handleNext: function (component, event, helper) {
    component.set("v.currentPageNumber", Math.min(component.get("v.currentPageNumber") + 1, component.get("v.maxPageNumber")));
  },

	refreshRecord: function (component, event, helper) {
    var msgType = component.get("v.messageType");

    if (msgType === 'error') {
      component.set("v.showRecordMessage", false);
      component.set("v.messageTitle", null);
      component.set("v.messageType", null);
      component.set("v.message", null);
      component.set("v.showAddButton", true);
    }
    else {
				helper.showSpinnerHelper(component, event, helper);
        var parentRecordId = component.get("v.parentObject").Id;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "\/" + parentRecordId
        });
        urlEvent.fire();
    }
  },

	handleSort: function (component, event, helper) {
		try {
			component.set("v.showSpinner", true);
			var columnName = event.target.dataset.columnname;
			if (columnName === "planId") {
				var isAscending = component.get("v.isPlanIdAsc");
				helper.sortTableByAttributeHelper(component, "planId", !isAscending, false);
				component.set("v.isPlanIdAsc", !isAscending);
				component.set("v.lastSortedBy", 'planId');
			} else if (columnName === "planName") {
				var isAscending = component.get("v.isPlanNameAsc");
				helper.sortTableByAttributeHelper(component, "planName", !isAscending, false);
				component.set("v.isPlanNameAsc", !isAscending);
				component.set("v.lastSortedBy", 'planName');
    	}
		} catch (error) {
			component.set("v.showSpinner", false);
			helper.handleError(component, 'Method handleSort controller' + error.name + ': ' + error.message);
		}
  },

	planSelected: function (component, event, helper) {
		var isSelectedTab = component.get("v.isSelectedTable");
		if(isSelectedTab){
			helper.selectedTablePlanSelected(component, event, helper);
		}
		else{
			helper.allTablePlanSelected(component, event, helper);
		}
  },

	chargeSelected: function (component, event, helper) {
		
		var isSelectedTab = component.get("v.isSelectedTable");
		if(isSelectedTab){
			helper.selectedTableChargeSelected(component, event, helper);
		}
		else{
			
			helper.allPlansTableChargeSelected(component, event, helper);
		}
	},

	handleShowPricingTiers: function (component, event, helper) {
		try {
			var thisTierPlan = event.target.name;
			var planName = event.target.title;
			let thisPlan = JSON.parse(JSON.stringify(thisTierPlan));
			var isSelectedTab = component.get("v.isSelectedTable");
			var lineItemTierRelationShipAPI = component.get("v.lineItemTierRelationShipAPIName");
			var selectedTierList =[];
			if(isSelectedTab){
				var lineItemTiersVar = component.get("v.lineItemTiersMap"); 
				if(Object.keys(lineItemTiersVar).includes(thisPlan.Id)){
					selectedTierList = lineItemTiersVar[thisPlan.Id];
				}
				else if(thisTierPlan.OrdwayLabs__TierJSON__c != null 
					&& thisTierPlan.OrdwayLabs__TierJSON__c != undefined
					&& thisTierPlan.OrdwayLabs__TierJSON__c){

					selectedTierList = [];
					var tierListFromJSON = JSON.parse(thisTierPlan.OrdwayLabs__TierJSON__c);
				
					tierListFromJSON.forEach((tier) => {

						var newLineItemTier =	{
							"OrdwayLabs__PricingType__c" : tier.type,
							"OrdwayLabs__ListPrice__c"   : tier.price,
							"OrdwayLabs__EndingUnit__c"  : tier.ending_unit,
							"OrdwayLabs__StartingUnit__c": tier.starting_unit,
							"OrdwayLabs__Tier__c"        : tier.tier
						}

						if(thisPlan.Id != null && thisPlan.Id != undefined){
							newLineItemTier[lineItemTierRelationShipAPI] = thisPlan.Id;
						}
						selectedTierList.push(newLineItemTier);
					});
				}
			}
			else{
				var bundleIdToPlanTierMap = component.get("v.bundleIdToPlanTierMap");
				var chargeTierMap =  component.get("v.chargeTiersMap");

				if(Object.keys(bundleIdToPlanTierMap).includes(thisPlan["OrdwayLabs__OrdwayChargeId__c"])){
					selectedTierList = bundleIdToPlanTierMap[thisPlan["OrdwayLabs__OrdwayChargeId__c"]];
				}
				else{
					selectedTierList =  chargeTierMap[thisPlan["OrdwayLabs__OrdwayChargeId__c"]];
				}
			}

			if(selectedTierList == undefined 
				|| selectedTierList == null
				|| selectedTierList.length == 0){
				selectedTierList = [];
				var newLineItemTier = {
					"OrdwayLabs__PricingType__c":"Per unit",
					"OrdwayLabs__ListPrice__c":0,
					"OrdwayLabs__EndingUnit__c":null,
					"OrdwayLabs__StartingUnit__c":0,
					"OrdwayLabs__Tier__c":1
				}

				if(thisPlan.Id != null && thisPlan.Id != undefined){
					newLineItemTier[lineItemTierRelationShipAPI] = thisPlan.Id;
				}

				selectedTierList.push(newLineItemTier);
			}
			
			component.set("v.selectedPlanToViewTierPlan", thisPlan);
			component.set("v.selectedPlanTierListToView", selectedTierList);
			component.set("v.tierPlanName", planName);
			component.set("v.showTierPrice", true);
			component.set("v.tierPlanId", thisPlan.OrdwayLabs__OrdwayPlanId__c);
			component.set("v.tierOrdwayProductId", thisPlan.OrdwayLabs__OrdwayProductId__c);    
			helper.validateSelectedTierPlanFields(component);
		} catch (error) {
			component.set("v.showSpinner", false);
			helper.handleError(component, 'Method handleShowPricingTiers Controller ' + error.name + ': ' + error.message);
		}
  },
	
	changeNextPlanFromValue: function (component, event, helper) {
    component.set("v.showRecordMessage", false);
    var thisIndex = event.getSource().get("v.name");
    var selectedPlanlist = component.get("v.selectedPlanTierListToView");

    if((selectedPlanlist.length-1)  != thisIndex){
      selectedPlanlist[thisIndex+1].OrdwayLabs__StartingUnit__c = selectedPlanlist[thisIndex].OrdwayLabs__EndingUnit__c;
    }
    component.set("v.selectedPlanTierListToView", selectedPlanlist);
    helper.validateSelectedTierPlanFields(component);
  },

	handleTierPriceUpdate: function (component, event, helper) {
    helper.validateSelectedTierPlanFields(component);
  },

	deleteTierPlan: function (component, event, helper) {
		var tierIdToDelete = component.get("v.lineItemTiersToDelete");

    var indexToDelete = event.getSource().get("v.title");
    var tierPlanList = component.get("v.selectedPlanTierListToView");
		if(tierPlanList[indexToDelete].Id != null 
			&& tierPlanList[indexToDelete].Id != undefined
			&& !tierIdToDelete.includes(tierPlanList[indexToDelete].Id)){
				tierIdToDelete.push(tierPlanList[indexToDelete].Id);
			}

		

    tierPlanList.splice(indexToDelete, 1);
    tierPlanList[tierPlanList.length - 1]["OrdwayLabs__EndingUnit__c"] = null;
    component.set("v.selectedPlanTierListToView",tierPlanList );
		component.set("v.lineItemTiersToDelete",tierIdToDelete );

    helper.validateSelectedTierPlanFields(component);
  },

	
  addTieringPlan: function (component, event, helper) {
		var isSelectedTab = component.get("v.isSelectedTable");
	
		var lineItemTierRelationShipAPI = component.get("v.lineItemTierRelationShipAPIName");
    
		
    var selectedPlanlist = component.get("v.selectedPlanTierListToView");
    selectedPlanlist[selectedPlanlist.length-1].OrdwayLabs__EndingUnit__c = (selectedPlanlist[selectedPlanlist.length-1].OrdwayLabs__StartingUnit__c * 1) + 1;
    var newLineItemTier = {
			"OrdwayLabs__PricingType__c":"Per unit",
			"OrdwayLabs__ListPrice__c": 0,
			"OrdwayLabs__EndingUnit__c":null,
			"OrdwayLabs__StartingUnit__c":selectedPlanlist[selectedPlanlist.length-1].OrdwayLabs__EndingUnit__c,
			"OrdwayLabs__Tier__c":selectedPlanlist.length+1
		};

		if(isSelectedTab){
			var thisPlan = component.get("v.selectedPlanToViewTierPlan");
			newLineItemTier[lineItemTierRelationShipAPI] = thisPlan.Id;
		}
		selectedPlanlist.push(newLineItemTier);
		
    component.set("v.selectedPlanTierListToView", selectedPlanlist);
    helper.validateSelectedTierPlanFields(component);
  },

	tierPlanUpdate: function (component, event, helper) {
		component.set("v.showSpinner", true);
		
		try {
			var planId = component.get("v.tierPlanId");
			var productId = component.get("v.tierOrdwayProductId");
			var allPlanMapVar = component.get("v.allPlanMap");
			var updatedTier = component.get("v.selectedPlanToViewTierPlan");
			var isSelectedTab = component.get("v.isSelectedTable");
			var thisPlan = component.get("v.selectedPlanToViewTierPlan");
      
			var selectedPlanTierlist = component.get("v.selectedPlanTierListToView");
			selectedPlanTierlist.forEach((element) => {
				if(element["OrdwayLabs__StartingUnit__c"]){element["OrdwayLabs__StartingUnit__c"] = parseFloat(element["OrdwayLabs__StartingUnit__c"]);}
				if(element["OrdwayLabs__EndingUnit__c"]){element["OrdwayLabs__EndingUnit__c"] = parseFloat(element["OrdwayLabs__EndingUnit__c"]);}
			});
			
			var hasEmptyEndingUnit = false;
			var tierPlanLength = selectedPlanTierlist.length;
	
			var allValid = component.find('tierfield').reduce(function (validSoFar, inputCmp) {
				inputCmp.reportValidity();
				return validSoFar && inputCmp.checkValidity();
			}, true);
			if (allValid) {
				for(var i=0; i<selectedPlanTierlist.length; i++){
					if( ( (i !== tierPlanLength -1) && (selectedPlanTierlist[i]["OrdwayLabs__EndingUnit__c"] == null || selectedPlanTierlist[i]["OrdwayLabs__EndingUnit__c"] == undefined))
					|| (selectedPlanTierlist[i]["OrdwayLabs__ListPrice__c"] == null || selectedPlanTierlist[i]["OrdwayLabs__ListPrice__c"] == undefined)){
						hasEmptyEndingUnit = true;
						break;
					}
				}
				if(hasEmptyEndingUnit){
					helper.handleError(component, "Fields in previous price tiers must have values before a new row can be added.")
				}
				else{
					if(isSelectedTab){
						var lineItemTiersVar = component.get("v.lineItemTiersMap"); 
						lineItemTiersVar[thisPlan.Id] = selectedPlanTierlist;
            
						component.set("v.lineItemTiersMap", lineItemTiersVar);
					}
					else{
						var bundleIdToPlanTierMap = component.get("v.bundleIdToPlanTierMap");
						bundleIdToPlanTierMap[thisPlan["OrdwayLabs__OrdwayChargeId__c"]] = selectedPlanTierlist;
						
						component.set("v.bundleIdToPlanTierMap", bundleIdToPlanTierMap);

					}
					component.set("v.showSpinner", false);
					component.set("v.showTierPrice", false);
				}
			} 
		} catch (error) {
			component.set("v.showSpinner", false);
			helper.handleError(component, 'Method tierPlanUpdate Controller ' + error.name + ': ' + error.message);
		}
  },

	closeModel: function (component, event, helper) {
    component.set("v.showTierPrice", false);
		component.set("v.showSpinner", false);

    component.set("v.showRecordMessage", false);
  },

	reRenderTableList: function (component, event, helper) {
    helper.showSpinnerHelper(component, event, helper);
    helper.reRenderTableList(component);
  },

	handleSearch : function(component, event, helper){
		try {
		component.set("v.showSpinner", true);
		var parameters = event.getParam('arguments');
    
		var searchValue = parameters.searchFilterText;
		component.set("v.searchFilterValue", searchValue);
    var pageSizeVar = component.get("v.pageSize");
    component.set("v.isPlansAvailable", true);
    var searchResult =[];
    var allRecords = component.get("v.allPlanList");
    var requiredFieldsArray = ['OrdwayLabs__OrdwayProductId__c','OrdwayLabs__BillingPeriod__c','OrdwayLabs__ChargeName__c','OrdwayLabs__OrdwayListPrice__c','OrdwayLabs__PricingModel__c','OrdwayLabs__ChargeType__c'];
			if(searchValue
				&& searchValue.trim().length >= 3 ){
					for(var i=0 ; i<allRecords.length ; i++){
						if(allRecords[i]['planId'].toUpperCase().includes(searchValue.toUpperCase()) || allRecords[i]['planName'].toUpperCase().includes(searchValue.toUpperCase())){
							searchResult.push(allRecords[i]);
						}
						else{
							var thisPlan = {};
							thisPlan["planId"] = allRecords[i]['planId'];
							thisPlan["planName"] = allRecords[i]['planName'];
							thisPlan["isSelected"] = false;
							thisPlan["hasExistingPlan"] = allRecords[i]['hasExistingPlan'];
							thisPlan["planList"] = [];
							for(var j=0 ; j<requiredFieldsArray.length ; j++){
								for(var k=0 ; k<allRecords[i]["planList"].length ; k++){
									if(allRecords[i]["planList"][k][requiredFieldsArray[j]]
									&& allRecords[i]["planList"][k][requiredFieldsArray[j]].toString().toUpperCase().includes(searchValue.toUpperCase())){
										thisPlan["planList"].push(allRecords[i]["planList"][k]);
									}
								}
							}
							if( thisPlan["planList"].length > 0){
								searchResult.push(thisPlan);
							}
						}
					}
	
					if(searchResult.length <= 0){
						component.set("v.isPlansAvailable", false);
						component.set("v.issearching", true);
					}
					else{
						component.set("v.searchFilteredList", searchResult);
						component.set("v.issearching", true);
						component.set("v.currentPageNumber", 1);
						var thisPageRecords = searchResult.slice((1 - 1) * pageSizeVar, 1 * pageSizeVar);
						component.set("v.currentPageList", thisPageRecords);
						component.set("v.maxPageNumber", Math.floor((searchResult.length + pageSizeVar - 1) / pageSizeVar));
					}
					component.set("v.showSpinner", false);
				}
				else if(!searchValue || searchValue.trim().length == 0){
					component.set("v.issearching", false);
					component.set("v.maxPageNumber", Math.floor((allRecords.length + pageSizeVar - 1) / pageSizeVar));
					helper.reRenderTableList(component);
				}
				component.set("v.showSpinner", false);
		} catch (error) {
			component.set("v.showSpinner", false);
			helper.handleError(component, 'Method handleSearch ' + error.name + ': ' + error.message);
		}
	},

	savePlans :function (component, event, helper) {
		try {
			var isSelectedPlantable = component.get("v.isSelectedTable");
			var parentRecord = component.get("v.parentObject");
			var lineItemTierRelationShipAPI = component.get("v.lineItemTierRelationShipAPIName");
			var tierIdsToDelete = component.get("v.lineItemTiersToDelete");
			var hasBillingPeriodError = false;
			component.set("v.showAddButton", false);
			helper.showSpinnerHelper(component, event, helper);
			var selectedPlans = component.get("v.selectedPlanList");
			var isValid = true;
			for (var i = 0; i < selectedPlans.length; i++) {
				var thisPlanlist = selectedPlans[i]["planList"];
				for (var j = 0; j < thisPlanlist.length; j++) {
					if(!isSelectedPlantable 
						&& thisPlanlist[j].hasOwnProperty('Id')){
							delete thisPlanlist[j]["Id"];
							delete thisPlanlist[j]["TotalPrice"];
						}
					if (thisPlanlist[j].isChargeSelected == true &&  
						( (thisPlanlist[j].hasOwnProperty('Quantity') && !thisPlanlist[j].Quantity)) 
							|| ((thisPlanlist[j].hasOwnProperty('OrdwayLabs__Quantity__c') && !thisPlanlist[j].OrdwayLabs__Quantity__c))) {
						isValid = false;
						break;
					}
					else if(thisPlanlist[j].isChargeSelected == true
						&& thisPlanlist[j].hasOwnProperty('OrdwayLabs__ChargeType__c')
						&& thisPlanlist[j]["OrdwayLabs__ChargeType__c"] !== 'One Time'
						&& (thisPlanlist[j]["OrdwayLabs__BillingPeriod__c"] === 'N/A'
								|| thisPlanlist[j]["OrdwayLabs__BillingPeriod__c"] === null
								|| thisPlanlist[j]["OrdwayLabs__BillingPeriod__c"] === undefined)){
									isValid = false;
									hasBillingPeriodError = true;
									break;
						}
				}
				if (!isValid) {
					break;
				}
			}
			

			if (!isValid) {
				var errorMessage = hasBillingPeriodError ? 'Charges that are recurring and usage require a billing period value. Please verify Charge type and Billing period value for Selected plans' : 'Quantity cannot be empty for selected charges';
				helper.handleError(component, errorMessage);
				component.set("v.showAddButton", true);
				helper.hideSpinnerHelper(component, event, helper);
			} else {
				component.set("v.showAddButton", false);
				var action = component.get("c.updatePlans");
				var finalOLIList = [];
				var isSelectedTab = component.get("v.isSelectedTable");

				var lineItemTiersVar = component.get("v.lineItemTiersMap"); 
				var bundleIdToPlanTierMap = component.get("v.bundleIdToPlanTierMap");
				var chargeTierMap =  component.get("v.chargeTiersMap");

				for (var i = 0; i < selectedPlans.length; i++) {
					var thisPlanlist = selectedPlans[i]["planList"];
					for (var j = 0; j < thisPlanlist.length; j++) {
						if (thisPlanlist[j].isChargeSelected == true) {
							if(!isSelectedTab){
								if(!Object.keys(bundleIdToPlanTierMap).includes(thisPlanlist[j]["OrdwayLabs__OrdwayChargeId__c"])
								&& Object.keys(chargeTierMap).includes(thisPlanlist[j]["OrdwayLabs__OrdwayChargeId__c"])){
									bundleIdToPlanTierMap[thisPlanlist[j]["OrdwayLabs__OrdwayChargeId__c"]] = chargeTierMap[thisPlanlist[j]["OrdwayLabs__OrdwayChargeId__c"]];
								}
							}
							
							if(thisPlanlist[j]["OrdwayLabs__PricingModel__c"] != 'Per Unit'){

								var lineItemTiersVar = component.get("v.lineItemTiersMap"); 
								var bundleIdToPlanTierMap = component.get("v.bundleIdToPlanTierMap");
								var selectedTierList = [];
									var newLineItemTier = {
										"OrdwayLabs__PricingType__c":"Per unit",
										"OrdwayLabs__ListPrice__c":0,
										"OrdwayLabs__EndingUnit__c":null,
										"OrdwayLabs__StartingUnit__c":0,
										"OrdwayLabs__Tier__c":1
									}

								if(thisPlanlist[j].hasOwnProperty("Id") &&  !lineItemTiersVar.hasOwnProperty(thisPlanlist[j].Id)){
									selectedTierList.push(newLineItemTier);
									newLineItemTier[lineItemTierRelationShipAPI] = thisPlanlist[j].Id;
									lineItemTiersVar[thisPlanlist[j].Id] = selectedTierList;
								}
								else if(thisPlanlist[j].hasOwnProperty("OrdwayLabs__OrdwayChargeId__c") && !bundleIdToPlanTierMap.hasOwnProperty(thisPlanlist[j].OrdwayLabs__OrdwayChargeId__c)){
									selectedTierList.push(newLineItemTier);
									bundleIdToPlanTierMap[thisPlanlist[j]["OrdwayLabs__OrdwayChargeId__c"]] = selectedTierList;
								}

								if(thisPlanlist[j].hasOwnProperty("Id") && lineItemTiersVar.hasOwnProperty(thisPlanlist[j].Id)){
									lineItemTiersVar[thisPlanlist[j].Id].forEach((tier) => {
										delete tier.Id;
									});
								}
							}
							else if(thisPlanlist[j]["OrdwayLabs__PricingModel__c"] == 'Per Unit'){
								if(thisPlanlist[j].hasOwnProperty("Id") && lineItemTiersVar.hasOwnProperty(thisPlanlist[j].Id)){
									lineItemTiersVar[thisPlanlist[j].Id].forEach((tier) => {
										tierIdsToDelete.push(tier.Id)
									});
									delete lineItemTiersVar[thisPlanlist[j].Id];
								}
								else if(thisPlanlist[j].hasOwnProperty("OrdwayLabs__OrdwayChargeId__c") && bundleIdToPlanTierMap.hasOwnProperty(thisPlanlist[j].OrdwayLabs__OrdwayChargeId__c)){
									delete bundleIdToPlanTierMap[thisPlanlist[j].OrdwayLabs__OrdwayChargeId__c];
								}
							}
							finalOLIList.push(thisPlanlist[j]);
						}
					}
				}

				action.setParams({
					lineItemList: JSON.stringify(finalOLIList),
					lineItemToDeleteId: JSON.stringify(component.get("v.oppLineItemIdsToDelete")),
					parentRecordId: parentRecord["Id"],
					lineItemTiersToDeleteId : JSON.stringify(tierIdsToDelete),
					lineItemTiersString : JSON.stringify(lineItemTiersVar),
					chargeTierMapString : JSON.stringify(bundleIdToPlanTierMap),
					isSelectedTable : isSelectedTab,
					lineItemTierRelationShipAPIName : component.get("v.lineItemTierRelationShipAPIName")
				});
	
				action.setCallback(this, function (response) {
					var state = response.getState();
	
					if (state == "SUCCESS") {
						helper.handleSuccess(component, "Plans updated successfully");
						var initMethodEvent = component.get("v.parentInitMethod");
						$A.enqueueAction(initMethodEvent);
						helper.hideSpinnerHelper(component, event, helper);
						
					} else {
						helper.hideSpinnerHelper(component, event, helper);
						helper.handleError(component, action.getError()[0].message);
					}
				});
				$A.enqueueAction(action);
			}
		} catch (error) {
			component.set("v.showSpinner", false);
			helper.handleError(component, 'Method savePlans ' + error.name + ': ' + error.message);
		}
	},

	handleDynamicInputUpdated: function (component, event, helper) {
		try {
			helper.showSpinnerHelper(component, event, helper);
			component.set("v.showPricingModelData", false);
			component.set("v.showPricingModelData", true);
			helper.hideSpinnerHelper(component, event, helper);
		}catch (error) {
			component.set("v.showSpinner", false);
			helper.handleError(component, 'Method handleDynamicInputUpdated ' + error.name + ': ' + error.message);
		}
	},

	handleDone: function (component, event, helper) {
    helper.showSpinnerHelper(component, event, helper);
    var parentRecordId = component.get("v.parentObject").Id;
    var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
        "url": "\/" + parentRecordId
    });
    urlEvent.fire();
    
  },

	handleCancel: function (component, event, helper) {
		helper.showSpinnerHelper(component, event, helper);
		var action = component.get("c.triggerUpdateOnParentRecord");
		action.setParams({
      parentRecordId: component.get("v.parentObject").Id
    });

		action.setCallback(this, function (response) {
      try{
        var state = response.getState();
				if (state == "SUCCESS") {
					var workspaceAPI = component.find("addEditPlanComponent");
					workspaceAPI.isConsoleNavigation().then(function(response) {
						if(response){
							workspaceAPI.getFocusedTabInfo().then(function(response) {
								var parentTab = component.get("v.parentConsoleTabID");
								var focusedTabId = response.tabId;
								workspaceAPI.refreshTab({
										tabId: parentTab,
										includeAllSubtabs: true
								});
								workspaceAPI.focusTab({tabId : parentTab});
								workspaceAPI.closeTab({
									tabId: focusedTabId
							});
						})
						.catch(function(error) {
						});
					}
					else{
						var parentRecordId = component.get("v.parentObject").Id;
						var urlEvent = $A.get("e.force:navigateToURL");
						urlEvent.setParams({
								"url": "\/" + parentRecordId
						});
						urlEvent.fire();
					}
				})
				.catch(function(error) {
				});
        } else {
          helper.hideSpinnerHelper(component);
          helper.handleError(component, JSON.stringify(action.getError(), 0, 2));
        }
      }
      catch(error){
        helper.hideSpinnerHelper(component);
        helper.handleError(component, error.name + ': '+' Line : '+ error.lineNumber +' : '+ error.message);
      }
    });
    $A.enqueueAction(action);
  }
})