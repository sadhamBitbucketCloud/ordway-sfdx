({
	//Selected Plan Table - Init helper
	initSelectedPlanHelper: function (component, event, helper) {
		this.showSpinnerHelper(component);

		try{
			var selectedPlanMapVar  = component.get("v.selectedPlanMap");
			var selectedPlanListVar  = component.get("v.selectedPlanList");
			var selectedPlansOnly = Object.values(selectedPlanMapVar);
			
			if(selectedPlansOnly.length > 0){
				var pageSizeVar = component.get("v.pageSize");
				component.set("v.maxPageNumber", Math.floor((selectedPlanListVar.length + pageSizeVar - 1) / pageSizeVar));
				component.set("v.isPlansAvailable", true);
				this.reRenderTableList(component);    
				this.sortTableByAttributeHelper(component, "planId", true, false);
			}
			else{
				this.hideSpinnerHelper(component);
				component.set("v.isPlansAvailable", false);
			}
		}
		catch(error){
			this.hideSpinnerHelper(component);
      console.error('Method initSelectedPlanHelper ' + error.name + ': ' + error.message);
      this.handleError(component, 'Method initSelectedPlanHelper ' + error.name + ': ' + error.message);
		}
	},

	//All Plan Table - Init helper
	initAllPlanHelper: function (component, event, helper) {
		try {
			var allPlanListVar  = component.get("v.allPlanList");

			if (allPlanListVar.length > 0) {
				var pageSizeVar = component.get("v.pageSize");
				component.set("v.maxPageNumber", Math.floor((allPlanListVar.length + pageSizeVar - 1) / pageSizeVar));
				component.set("v.isPlansAvailable", true);
				this.reRenderTableList(component);   
			}
			else{
				component.set("v.isPlansAvailable", false);
				this.hideSpinnerHelper(component);
			}
		} catch (error) {
			this.hideSpinnerHelper(component);
      console.error('Method initAllPlanHelper ' + error.name + ': ' + error.message);
      this.handleError(component, 'Method initAllPlanHelper ' + error.name + ': ' + error.message);
		}
	},

	reRenderTableList: function (component) {
		try{
			var isSearch = component.get("v.issearching");
			component.set("v.isPlansAvailable", true);
			var allRecords;
			if(isSearch){
				allRecords = component.get("v.searchFilteredList");
			}
			else{
				var isSelectedTabVar = component.get("v.isSelectedTable");
				if (isSelectedTabVar) {
					allRecords = component.get("v.selectedPlanList");
				} else {
					allRecords = component.get("v.allPlanList");
				}
			}
			
			if(allRecords.length <= 0){
				component.set("v.isPlansAvailable", false);
			}
			var pageNumber = component.get("v.currentPageNumber");
			var pageSizeVar = component.get("v.pageSize");

			var thisPageRecords = allRecords.slice((pageNumber - 1) * pageSizeVar, pageNumber * pageSizeVar);
			component.set("v.currentPageList", thisPageRecords);
			
			component.set("v.showSpinner", false);
		}
		catch(error){
			this.hideSpinnerHelper(component);
      console.error('Method reRenderTableList ' + error.name + ': ' + error.message);
      this.handleError(component, 'Method reRenderTableList ' + error.name + ': ' + error.message);
		}
  },

	sortTableByAttributeHelper: function (component, attributeName, isAsc, isAfterPlanSelected) {
		try {
			component.set("v.showSpinner", true);
			var isSelectedTabVar = component.get("v.isSelectedTable");

			var selectedListRecords = component.get("v.selectedPlanList");
			var allPlanRecords = component.get("v.allPlanList");
			var filteredRecords = component.get("v.searchFilteredList");
			if (isAsc) {
				if(isSelectedTabVar && selectedListRecords.length > 0){
					selectedListRecords.sort((a, b) => a[attributeName] > b[attributeName] ? 1 : -1);
				}
				if(!isSelectedTabVar && allPlanRecords.length > 0){
					allPlanRecords.sort((a, b) => a[attributeName] > b[attributeName] ? 1 : -1);
				}
				if(!isSelectedTabVar && filteredRecords.length > 0){
					filteredRecords.sort((a, b) => a[attributeName] > b[attributeName] ? 1 : -1);
				}
			} else {
				if(isSelectedTabVar && selectedListRecords.length > 0){
					selectedListRecords.sort((a, b) => a[attributeName] > b[attributeName] ? -1 : 1);
				}
				if(!isSelectedTabVar && allPlanRecords.length > 0){
					allPlanRecords.sort((a, b) => a[attributeName] > b[attributeName] ? -1 : 1);

				}
				if(!isSelectedTabVar && filteredRecords.length > 0){
					filteredRecords.sort((a, b) => a[attributeName] > b[attributeName] ? -1 : 1);
				}
							}
			component.set("v.allPlanList", allPlanRecords);

			component.set("v.selectedPlanList", selectedListRecords);
			component.set("v.searchFilteredList", filteredRecords);
			if(!isAfterPlanSelected){
				this.reRenderTableList(component);
			}
		} catch (error) {
			component.set("v.showSpinner", false);
			this.handleError(component, 'Method sortTableByAttributeHelper ' + error.name + ': ' + error.message);
		}
  },

	//Selected Plan Table - plan selected helper
	selectedTablePlanSelected: function (component, event, helper) {
		try {
			var planToBundleMapvar = component.get("v.planToBundleMap");
			var thisPlan = event.getSource().get("v.name");
			var planBundleId = thisPlan.planBundleId;
			var thisPlanId = thisPlan.planId;
			var thisplanName = thisPlan.planName;
			var thisplanList = thisPlan.planList;
			var thisPlanIdIsSelected = thisPlan.isSelected;

			var selectedPlanMapVar = component.get("v.selectedPlanMap");
	
			if (!thisPlanIdIsSelected &&
				selectedPlanMapVar.hasOwnProperty(planBundleId)) {
				delete selectedPlanMapVar[planBundleId];

				if(Object.keys(planToBundleMapvar).includes(thisPlanId)){
					planToBundleMapvar[thisPlanId] = this.removeElementFromNonObjectArray(planToBundleMapvar[thisPlanId], planBundleId);
					planToBundleMapvar[thisPlanId] = planToBundleMapvar[thisPlanId].sort();
				}

				var thisOppLineItemIdsSet = component.get("v.oppLineItemIdsToDelete");
	
				if (thisPlan.hasExistingPlan) {
					for (var i = 0; i < thisplanList.length; i++) {
						if (thisplanList[i].Id != null &&
							thisplanList[i].Id != undefined &&
							!thisOppLineItemIdsSet.includes(thisplanList[i].Id)) {
							thisOppLineItemIdsSet.push(thisplanList[i].Id);
							//delete thisplanList[i].Id;
							delete thisplanList[i].UnitPrice;
						}
					}
				}
			} 
			component.set("v.oppLineItemIdsToDelete", thisOppLineItemIdsSet);
			component.set("v.selectedPlanMap", selectedPlanMapVar);
			component.set("v.planToBundleMap", planToBundleMapvar);

			var selectedPlansOnly = Object.values(selectedPlanMapVar);
			component.set("v.selectedPlanList", selectedPlansOnly);
			if (!thisPlanIdIsSelected) {
				var selectedListRecords = component.get("v.selectedPlanList");
				var pageSizeVar = component.get("v.pageSize");
				var pageNumber = component.get("v.currentPageNumber");
				var maxPage = Math.floor((selectedListRecords.length + pageSizeVar - 1) / pageSizeVar);
				component.set("v.maxPageNumber", Math.floor((selectedListRecords.length + pageSizeVar - 1) / pageSizeVar));
				if (pageNumber > maxPage) {
					component.set("v.currentPageNumber", pageNumber - 1);
				}
			}
			this.sortAfterPlanSelected(component, event, helper);
			this.reRenderTableList(component);
			this.fireSelectedListUpdatedUtilityEvent(component);
			this.hideSpinnerHelper(component, event, helper);
		} catch (error) {
			component.set("v.showSpinner", false);
			this.handleError(component, 'Method selectedTablePlanSelected ' + error.name + ': ' + error.message);
		}
	},

	//All Plan Table - plan selected helper
	allTablePlanSelected: function (component, event, helper) {
		try {

		this.showSpinnerHelper(component, event, helper);
		var thisPlanIndexVar = event.getSource().get("v.label");

		var currentPageListVar = component.get("v.currentPageList");

		var lastAddedDynamicCharge = component.get("v.lastAddedDynamicChargeMap");
		var planToBundleMapVar		 = component.get("v.planToBundleMap");
		var lastAddedPlanBundleIdMapVar = component.get("v.lastAddedPlanBundleIdMap");
		var uniqueKey;
    var thisPlan = event.getSource().get("v.name");
    var thisPlanId = thisPlan.planId;
    var thisplanName = thisPlan.planName;
    var thisplanList = thisPlan.planList;
    var thisPlanIdIsSelected = thisPlan.isSelected;

    var allPlanMapVar = component.get("v.allPlanMap");
			
    allPlanMapVar[thisPlanId] = thisPlan;
    // component.set("v.allPlanList", Object.values(allPlanMapVar));
    // component.set("v.allPlanMap", allPlanMapVar);

		var allPlanTableSelectedMapVar = component.get("v.selectedPlanMap");
		

    if (!thisPlanIdIsSelected &&
      Object.keys(planToBundleMapVar).includes(thisPlanId) && planToBundleMapVar[thisPlanId].length > 0) {

				var lastBundleId = planToBundleMapVar[thisPlanId][planToBundleMapVar[thisPlanId].length - 1];

     // delete allPlanTableSelectedMapVar[thisPlanId];
      var thisOppLineItemIdsSet = component.get("v.oppLineItemIdsToDelete");

      if (thisPlan.hasExistingPlan) {
        for (var i = 0; i < thisplanList.length; i++) {
          if (thisplanList[i].Id != null &&
            thisplanList[i].Id != undefined &&
            !thisOppLineItemIdsSet.includes(thisplanList[i].Id)) {
            thisOppLineItemIdsSet.push(thisplanList[i].Id);
            //delete thisplanList[i].Id;
            delete thisplanList[i].UnitPrice;
          }
        }
      }

			if(allPlanTableSelectedMapVar.hasOwnProperty(lastBundleId)){
				delete allPlanTableSelectedMapVar[lastBundleId];

				planToBundleMapVar[thisPlanId] = this.removeElementFromNonObjectArray(planToBundleMapVar[thisPlanId], lastBundleId);
				planToBundleMapVar[thisPlanId] = planToBundleMapVar[thisPlanId].sort();
			}
      for (var i = 0; i < thisplanList.length; i++) {
        thisplanList[i].isChargeSelected = false;
      }
      allPlanMapVar[thisPlanId].planList = thisplanList;
			currentPageListVar[thisPlanIndexVar] = allPlanMapVar[thisPlanId];
			component.set("v.currentPageList", currentPageListVar);
      component.set("v.allPlanList", Object.values(allPlanMapVar));
      component.set("v.allPlanMap", allPlanMapVar);

    } else if (thisPlanIdIsSelected) {

			var newPlanBundleId;

			if(Object.keys(lastAddedPlanBundleIdMapVar).includes(thisPlanId)){
				newPlanBundleId = lastAddedPlanBundleIdMapVar[thisPlanId];
			}
			else if(!Object.keys(planToBundleMapVar).includes(thisPlanId) ||  planToBundleMapVar[thisPlanId].length == 0){
				planToBundleMapVar[thisPlanId] = [];
				planToBundleMapVar[thisPlanId].push(thisPlanId+'_1');
				newPlanBundleId = thisPlanId+'_1';
			}
			else if(Object.keys(planToBundleMapVar).includes(thisPlanId) && planToBundleMapVar[thisPlanId].length > 0){
				var lastBundleId = planToBundleMapVar[thisPlanId][planToBundleMapVar[thisPlanId].length - 1];
				var lastBundleNumber = lastBundleId.split("_")[1];
				var newBundleNumber = parseInt(lastBundleNumber) + 1;
				newPlanBundleId = thisPlanId+`_${newBundleNumber}`;
				planToBundleMapVar[thisPlanId].push(newPlanBundleId);
				planToBundleMapVar[thisPlanId].sort();
			}

				var dynamicCharge = 1;
				var thisPlanSelectedPlan = {};
				thisPlanSelectedPlan["planId"] = thisPlanId;
				thisPlanSelectedPlan["planName"] = thisplanName;
				thisPlanSelectedPlan["isSelected"] = thisPlanIdIsSelected;
				thisPlanSelectedPlan["planBundleId"] = newPlanBundleId;
				for (var i = 0; i < thisplanList.length; i++) {
					uniqueKey = thisplanList[i].OrdwayLabs__OrdwayPlanId__c+'_'+thisplanList[i].OrdwayLabs__OrdwayChargeId__c;
					thisplanList[i]["dynamicChargeId"] = 'DYNAMIC_CHARGE_'+dynamicCharge;
					lastAddedDynamicCharge[uniqueKey] = 'DYNAMIC_CHARGE_'+dynamicCharge;
					thisplanList[i]["OrdwayLabs__PlanBundleID__c"] = newPlanBundleId;
					thisplanList[i].isChargeSelected = true;
					dynamicCharge = dynamicCharge + 1;
				}
				thisPlanSelectedPlan["planList"] = thisplanList;
				allPlanTableSelectedMapVar[newPlanBundleId] = thisPlanSelectedPlan;
				

			lastAddedPlanBundleIdMapVar[thisPlanId] = newPlanBundleId;

			
      allPlanMapVar[thisPlanId].planList = thisplanList;
			currentPageListVar[thisPlanIndexVar] = allPlanMapVar[thisPlanId];
			component.set("v.currentPageList", currentPageListVar);
      component.set("v.allPlanList", Object.values(allPlanMapVar));
      component.set("v.allPlanMap", allPlanMapVar);
		}

		component.set("v.lastAddedPlanBundleIdMap", lastAddedPlanBundleIdMapVar);

			component.set("v.planToBundleMap", planToBundleMapVar);
			

			component.set("v.selectedPlanList", Object.values(allPlanTableSelectedMapVar));
			component.set("v.selectedPlanMap", allPlanTableSelectedMapVar);
			component.set("v.lastAddedDynamicChargeMap", lastAddedDynamicCharge);
			// this.reRenderTableList(component);
			this.sortAfterPlanSelected(component, event, helper);
			this.hideSpinnerHelper(component, event, helper);
			
		} catch (error) {
			component.set("v.showSpinner", false);
			this.handleError(component, 'Method allTablePlanSelected ' + error.name + ': ' + error.message);
		}
	},

	//Selected Plan Table - Charge selected helper
	selectedTableChargeSelected: function (component, event, helper) {
		try {
			var thisPlanIndexVar = event.getSource().get("v.placeholder");
      
			var currentPageListVar = component.get("v.currentPageList");
      

			var thisCharge = event.getSource().get("v.name");
			var thisPlan = event.getSource().get("v.label");
			var planBundleId = thisPlan.planBundleId;
			var thisPlanId = thisPlan.planId;
			var thisplanName = thisPlan.planName;
			var thisplanList = thisPlan.planList;
			var thisChargeIsSelected = thisCharge.isChargeSelected;

			var selectedPlanMapVar = component.get("v.selectedPlanMap");
			var thisOppLineItemIdsSet = component.get("v.oppLineItemIdsToDelete");
			if (!thisChargeIsSelected && selectedPlanMapVar.hasOwnProperty(planBundleId)) {
				for (var i = 0; i < thisplanList.length; i++) {
					if (thisplanList[i]["dynamicChargeId"] == thisCharge["dynamicChargeId"]) {
						if (thisplanList[i].Id != null &&
							thisplanList[i].Id != undefined &&
							!thisOppLineItemIdsSet.includes(thisplanList[i].Id)) {
							thisOppLineItemIdsSet.push(thisplanList[i].Id);
							//delete thisplanList[i].Id;
							delete thisplanList[i].UnitPrice;
						}
						//thisplanList[i].isChargeSelected = false;
						thisplanList.splice(i, 1);
						break;
					}
					this.reevaluateDynamicChargeId(component, thisplanList);
				}
				var shouldInvokeRerender = false;
				if(thisplanList.length > 0){
					selectedPlanMapVar[planBundleId].planList = thisplanList;
					currentPageListVar[thisPlanIndexVar] = selectedPlanMapVar[planBundleId];
					component.set("v.currentPageList", currentPageListVar);
				}
				else{
					delete selectedPlanMapVar[planBundleId];
					shouldInvokeRerender = true;
					//
				}
				
				
				component.set("v.selectedPlanMap", selectedPlanMapVar);
				var selectedPlansOnly = Object.values(selectedPlanMapVar);
				component.set("v.selectedPlanList", selectedPlansOnly);
				
				if(shouldInvokeRerender){
					this.reRenderTableList(component);
				}
			} 
			
			//this.reRenderTableList(component);
			this.sortAfterPlanSelected(component, event, helper);
		} 
		catch (error) {
			component.set("v.showSpinner", false);
			this.handleError(component, 'Method selectedTableChargeSelected ' + error.name + ': ' + error.message);
		}
	},

	//All Plan Table - Charge selected helper
	allPlansTableChargeSelected: function (component, event, helper) {
		try {

			
			var thisPlanIndexVar = event.getSource().get("v.placeholder");
      
			var currentPageListVar = component.get("v.currentPageList");
      

			var lastAddedDynamicCharge = component.get("v.lastAddedDynamicChargeMap");
			var planToBundleMapVar		 = component.get("v.planToBundleMap");
			var uniqueChargeKey;
			var thisCharge = event.getSource().get("v.name");
			var thisPlan = event.getSource().get("v.label");
			var thisPlanId = thisPlan.planId;
			var thisplanName = thisPlan.planName;
			var thisplanList = thisPlan.planList;
			var thisChargeIsSelected = thisCharge.isChargeSelected;
			var allPlanMapVar = component.get("v.allPlanMap");
			var lastAddedPlanBundleIdMapVar = component.get("v.lastAddedPlanBundleIdMap");
			allPlanMapVar[thisPlanId] = thisPlan;

			var allPlanTableSelectedMapVar = component.get("v.selectedPlanMap");
      
			var thisOppLineItemIdsSet = component.get("v.oppLineItemIdsToDelete");
			if (!thisChargeIsSelected && lastAddedPlanBundleIdMapVar.hasOwnProperty(thisPlanId)) {
				var totalNotSelectedCharges = 0;

				var lastAddedBundleId = lastAddedPlanBundleIdMapVar[thisPlanId];

				for (var i = 0; i < thisplanList.length; i++) {

					if (thisplanList[i].OrdwayLabs__OrdwayChargeId__c == thisCharge.OrdwayLabs__OrdwayChargeId__c) {
						thisplanList[i].isChargeSelected = false;

						allPlanTableSelectedMapVar[lastAddedBundleId]["planList"] = this.removeElementFromArray(allPlanTableSelectedMapVar[lastAddedBundleId]["planList"], 'OrdwayLabs__OrdwayChargeId__c', thisCharge.OrdwayLabs__OrdwayChargeId__c);

						if (thisplanList[i].Id != null &&
							thisplanList[i].Id != undefined &&
							!thisOppLineItemIdsSet.includes(thisplanList[i].Id)) {
							thisOppLineItemIdsSet.push(thisplanList[i].Id);
							delete thisplanList[i].Id;
							delete thisplanList[i].UnitPrice;
						}
					}

					if(!thisplanList[i].isChargeSelected){
						totalNotSelectedCharges = totalNotSelectedCharges + 1;
					}

				}

				if(totalNotSelectedCharges >= 1){
					allPlanMapVar[thisPlanId]["isSelected"] = false;
				}
				

				if(totalNotSelectedCharges === thisplanList.length){
					delete allPlanTableSelectedMapVar[lastAddedBundleId];
				}
				allPlanMapVar[thisPlanId].planList = thisplanList;
				currentPageListVar[thisPlanIndexVar] = allPlanMapVar[thisPlanId];
				component.set("v.currentPageList", currentPageListVar);

				component.set("v.allPlanList", Object.values(allPlanMapVar));
				component.set("v.allPlanMap", allPlanMapVar);
				component.set("v.selectedPlanMap", allPlanTableSelectedMapVar);
				var selectedPlansOnly = Object.values(allPlanTableSelectedMapVar);
				component.set("v.selectedPlanList", selectedPlansOnly);
				//helper.reRenderTableList(component);
			} 
			else if (thisChargeIsSelected) {
				//var lastAddedPlanBundleIdMapVar = component.get("v.lastAddedPlanBundleIdMap");

				var totalChargeSelected = 0; 
				var planBundleIdForNewCharge;
				var isBundleAlreadyAdded = false;

				if(!Object.keys(lastAddedPlanBundleIdMapVar).includes(thisPlanId)){

					if(!Object.keys(planToBundleMapVar).includes(thisPlanId) ||  planToBundleMapVar[thisPlanId].length == 0){
						planToBundleMapVar[thisPlanId] = [];
						planToBundleMapVar[thisPlanId].push(thisPlanId+'_1');
						planBundleIdForNewCharge = thisPlanId+'_1';
					}
					else if(Object.keys(planToBundleMapVar).includes(thisPlanId) && planToBundleMapVar[thisPlanId].length > 0){
						var lastBundleId = planToBundleMapVar[thisPlanId][planToBundleMapVar[thisPlanId].length - 1];
						var lastBundleNumber = lastBundleId.split("_")[1];
						var newBundleNumber = parseInt(lastBundleNumber) + 1;
						planBundleIdForNewCharge = thisPlanId+`_${newBundleNumber}`;
						planToBundleMapVar[thisPlanId].push(planBundleIdForNewCharge);
						planToBundleMapVar[thisPlanId].sort();
					}

					lastAddedPlanBundleIdMapVar[thisPlanId] = planBundleIdForNewCharge;
					planBundleIdForNewCharge = planBundleIdForNewCharge;
				}
				
				else if(Object.keys(lastAddedPlanBundleIdMapVar).includes(thisPlanId)){
					planBundleIdForNewCharge = lastAddedPlanBundleIdMapVar[thisPlanId];
					isBundleAlreadyAdded = true;
				}

				var dynamicCharge = 1;
				var thisPlanSelectedPlan = {};
				thisPlanSelectedPlan["planId"] = thisPlanId;
				thisPlanSelectedPlan["planName"] = thisplanName;
				thisPlanSelectedPlan["isSelected"] = thisChargeIsSelected;
				thisPlanSelectedPlan["planBundleId"] = planBundleIdForNewCharge;


					if(isBundleAlreadyAdded && allPlanTableSelectedMapVar.hasOwnProperty(planBundleIdForNewCharge)){
						thisPlanSelectedPlan["planList"] = allPlanTableSelectedMapVar[planBundleIdForNewCharge]["planList"];
					}
					else{
						thisPlanSelectedPlan["planList"] = [];
					}

					for (var i = 0; i < thisplanList.length; i++) {
						if (thisplanList[i].OrdwayLabs__OrdwayChargeId__c == thisCharge.OrdwayLabs__OrdwayChargeId__c) {
							uniqueChargeKey = thisplanList[i].OrdwayLabs__OrdwayPlanId__c+'_'+thisplanList[i].OrdwayLabs__OrdwayChargeId__c;
							thisplanList[i]["dynamicChargeId"] = 'DYNAMIC_CHARGE_'+dynamicCharge;
							lastAddedDynamicCharge[uniqueChargeKey] = 'DYNAMIC_CHARGE_'+dynamicCharge;
							thisplanList[i]["OrdwayLabs__PlanBundleID__c"] = planBundleIdForNewCharge;
							thisplanList[i].isChargeSelected = true;
							dynamicCharge = dynamicCharge + 1;
							thisPlanSelectedPlan["planList"].push(thisplanList[i]);
							//break;
						}
						if(thisplanList[i].isChargeSelected){
							totalChargeSelected = totalChargeSelected + 1;
						}
						
					}

					if(totalChargeSelected === thisplanList.length){
						allPlanMapVar[thisPlanId]["isSelected"] = true;
					}
					allPlanTableSelectedMapVar[planBundleIdForNewCharge] = thisPlanSelectedPlan;
					
				allPlanMapVar[thisPlanId].planList = thisplanList;
				currentPageListVar[thisPlanIndexVar] = allPlanMapVar[thisPlanId];
				component.set("v.currentPageList", currentPageListVar);
				component.set("v.lastAddedPlanBundleIdMap", lastAddedPlanBundleIdMapVar);
				component.set("v.planToBundleMap", planToBundleMapVar);

				

				component.set("v.allPlanList", Object.values(allPlanMapVar));
				component.set("v.allPlanMap", allPlanMapVar);
				component.set("v.selectedPlanList", Object.values(allPlanTableSelectedMapVar));
				component.set("v.selectedPlanMap", allPlanTableSelectedMapVar);
				component.set("v.lastAddedDynamicChargeMap", lastAddedDynamicCharge);
					
			}
			//helper.reRenderTableList(component);
			this.sortAfterPlanSelected(component, event, helper);


		} catch (error) {
			component.set("v.showSpinner", false);
			this.handleError(component, 'Method allPlansTableChargeSelected ' + error.name + ': ' + error.message);
		}
	},

	validateSelectedTierPlanFields: function (component) {
    var selectedTierPlanList = component.get("v.selectedPlanTierListToView");
    var hasEmptyFields = false;
    var tierPlanLength = selectedTierPlanList.length;
    for(var i=0; i<tierPlanLength; i++){
      if(( (i !== tierPlanLength -1) && (!selectedTierPlanList[i]["OrdwayLabs__EndingUnit__c"] || selectedTierPlanList[i]["OrdwayLabs__EndingUnit__c"] == null || selectedTierPlanList[i]["OrdwayLabs__EndingUnit__c"] == undefined) )
      || ((!selectedTierPlanList[i]["OrdwayLabs__ListPrice__c"] && selectedTierPlanList[i]["OrdwayLabs__ListPrice__c"] != 0) || selectedTierPlanList[i]["OrdwayLabs__ListPrice__c"] == null || selectedTierPlanList[i]["OrdwayLabs__ListPrice__c"] == undefined)){
				hasEmptyFields = true;
        break;
      }
    }
    component.set("v.disableTierPlanDone", hasEmptyFields);
  },

  showSpinnerHelper: function (component, event, helper) {
	component.set("v.showSpinner", true);
	},
	
	hideSpinnerHelper: function (component) {
		component.set("v.showSpinner", false);
	},
    
	handleSuccess: function (component, successMessage) {
		component.set("v.messageTitle", "Success!");
		component.set("v.messageType", "success");
		component.set("v.message", successMessage);
		component.set("v.showRecordMessage", true);
		window.setTimeout(function(){ component.set("v.showRecordMessage", false); }, 3000);

	},
	
	handleError: function (component, errorMessage) {
		component.set("v.messageTitle", "Error");
		component.set("v.messageType", "error");
		component.set("v.message", errorMessage);
		component.set("v.showRecordMessage", true);
		window.setTimeout(function(){ component.set("v.showRecordMessage", false); }, 3000);

	},

	sortAfterPlanSelected: function (component, event, helper) {
		try {
			component.set("v.showSpinner", true);
			var lastSortby = component.get("v.lastSortedBy");
			if(lastSortby){
				if(lastSortby === 'planName'){
					var isPlanNameAscending = component.get("v.isPlanNameAsc");
					this.sortTableByAttributeHelper(component, "planName", isPlanNameAscending, true);
				}
				else if(lastSortby === 'planId'){
					var isPlanIdAscending = component.get("v.isPlanIdAsc");
					this.sortTableByAttributeHelper(component, "planId", isPlanIdAscending, true);
				}
			}
			component.set("v.showSpinner", false);
		} catch (error) {
			component.set("v.showSpinner", false);
			this.handleError(component, 'Method sortAfterPlanSelected ' + error.name + ': ' + error.message);
		}
  },

	removeElementFromArray: function (array, attributeName, value) {
		try {
			return array.filter(function(element){ return element[attributeName] != value; });
		} catch (error) {
			component.set("v.showSpinner", false);
			this.handleError(component, 'Method removeElementFromArray ' + error.name + ': ' + error.message);
		}
		
	},

	removeElementFromNonObjectArray: function (array, value) {
		try {
			return array.filter(element => element !== value);
		} catch (error) {
			component.set("v.showSpinner", false);
			this.handleError(component, 'Method removeElementFromArray ' + error.name + ': ' + error.message);
		}
		
	},

	reevaluateDynamicChargeId: function (component, planList) {
		try {
			var dynamicCharge = 1;
			planList.forEach(function (thisPlan) {
				thisPlan["dynamicChargeId"] = 'DYNAMIC_CHARGE_'+dynamicCharge;
				dynamicCharge = dynamicCharge + 1; 
			});

		} catch (error) {
			component.set("v.showSpinner", false);
			this.handleError(component, 'Method reevaluateDynamicChargeId ' + error.name + ': ' + error.message);
		}
	},

	fireSelectedListUpdatedUtilityEvent: function (component) {
		try{
			var eventData = {};
			eventData["isSelectedTab"] = component.get("v.isSelectedTable");
			//eventData["listLength"] = lengthvalue;
			var utility = $A.get("e.c:UtilityApplicationEvent");
			utility.setParams({ "eventSource" : "PlanPickerTableHelper" });
			utility.setParams({ "targetSource" : "PlanPickerController" });
			utility.setParams({ "eventDataObject" : eventData });
			utility.fire();
		}
		catch (error) {
			component.set("v.showSpinner", false);
			this.handleError(component, 'Method fireSelectedListUpdatedUtilityEvent ' + error.name + ': ' + error.message);
		}
  }
})