({
	checkPermissionAssignement: function (component, event, helper) {
		var action = component.get("c.isPermissionSetAssigned");
		// set call back
		action.setCallback(this, function (response) {
				var state = response.getState();

				if (state === "SUCCESS"
				&& response.getReturnValue()) {
          component.set("v.isPermissionSetAssigned", true);
          this.doInit(component, event, helper);
				}
		});

		//adds the server-side action to the queue
		$A.enqueueAction(action);
  },

  getNamedCredentialHelper : function (component, event, helper) {
		var action = component.get("c.getNamedCredential");
		action.setCallback(this, function (response) {
			var state = response.getState();
      if (state === "SUCCESS" && response.getReturnValue()) {
        component.set("v.namedCredential", response.getReturnValue())
      }
		});
		$A.enqueueAction(action);
  },
  
  getPermission: function (component, event, helper) {
    
    var action = component.get("c.getButtonPermission");
    
     // set call back
     action.setCallback(this, function (response) {
       var state = response.getState();
       var permission = response.getReturnValue();

      if (state === "SUCCESS") {
        if (!component.get("v.SyncToOrdway")) {
          component.set("v.SyncToOrdway", permission.SyncContract);
        }

        if (!component.get("v.ActivateSubscription")) {
          component.set("v.ActivateSubscription", permission.ActivateContract);
        }

        if (!component.get("v.disableChangeRenewalButton")) {
          component.set("v.disableChangeRenewalButton", permission.ChangeRenewal);
        }

        if (!component.get("v.disableUpdateFromOrdway")) {
          component.set("v.disableUpdateFromOrdway", permission.SyncContract);
        }
       
        component.set("v.cancelSubscription", permission.CancelContract);
        component.set("v.viewInOrdway", permission.ViewInOrdway);
      } else {
        component.set("v.SyncToOrdway", true);
        component.set("v.ActivateSubscription", true);
        component.set("v.disableChangeRenewalButton", true);
        component.set("v.disableUpdateFromOrdway", true);
      }
    });
    //adds the server-side action to the queue
    $A.enqueueAction(action);
  },

	doInit : function(component, event, helper) {
		component.set('v.subscription', null);

    helper.showSpinnerHelper(component, event, helper);

    helper.subscribe(component, event, helper);
    var action = component.get("c.checkOrdwayStatus");

    action.setParams({
      "ordwayContractId": component.get("v.recordId")
    });

    // set call back
    action.setCallback(this, function (response) {
      var state = response.getState();

      if (state === "SUCCESS") {
        var thisOrdwayContract = response.getReturnValue();
        component.set("v.ordwayContract", thisOrdwayContract);

        if (thisOrdwayContract.OrdwayLabs__SyncStatus__c == 'Sync In Progress') {
          component.set("v.SyncToOrdway", true);
          component.set("v.ActivateSubscription", true);
          component.set("v.disableChangeRenewalButton", true);
        }
        else if (thisOrdwayContract.OrdwayLabs__SyncStatus__c == 'Do Not Send') {
          component.set("v.SyncToOrdway", true);
          component.set("v.ActivateSubscription", true);
          component.set("v.disableChangeRenewalButton", false);
        }
        else if (thisOrdwayContract.OrdwayLabs__OrdwayContractStatus__c == 'Not Linked To Ordway') {
          component.set("v.SyncToOrdway", false);
          component.set("v.ActivateSubscription", true);
          component.set("v.disableChangeRenewalButton", true);
        }
        else if (thisOrdwayContract.OrdwayLabs__OrdwayContractStatus__c == 'Draft') {
          component.set("v.SyncToOrdway", false);
          component.set("v.ActivateSubscription", true);
          if(thisOrdwayContract.OrdwayLabs__OrdwaySubscriptionID__c){
            component.set("v.ActivateSubscription", false);
            component.set("v.disableChangeRenewalButton", true);
          }
        }
        else if (thisOrdwayContract.OrdwayLabs__OrdwayContractStatus__c == 'Active') {
          component.set("v.SyncToOrdway", false);
          component.set("v.ActivateSubscription", true);
          component.set("v.disableChangeRenewalButton", false);
        }
        else if (thisOrdwayContract.OrdwayLabs__OrdwayContractStatus__c == 'Cancelled') {
          component.set("v.SyncToOrdway", true);
          component.set("v.ActivateSubscription", true);
          component.set("v.disableChangeRenewalButton", true);
        }

				if (thisOrdwayContract.OrdwayLabs__OrdwaySubscriptionID__c
					&& thisOrdwayContract.OrdwayLabs__SyncStatus__c == 'Not In Sync'
					&& thisOrdwayContract.OrdwayLabs__CustomerId__c) {
          component.set("v.disableUpdateFromOrdway", false);
				}

        this.hideSpinnerHelper(component, event, helper);
        this.getPermission(component, event, helper);
      } else {

      }
    });
    //adds the server-side action to the queue
    $A.enqueueAction(action);
	},

	showSpinnerHelper : function(component, event, helper) {
		component.set("v.showSpinner", true);
	},

	hideSpinnerHelper : function(component, event, helper) {
		component.set("v.showSpinner", false);
	},

	handleSuccess : function(component, successMessage) {
		component.set("v.messageTitle", "Success!");
		component.set("v.messageType", "success");
		component.set("v.message", successMessage);
		component.set("v.showRecordMessage", true);
	}	,

	handleError : function(component, errorMessage) {
		component.set("v.messageTitle", "Error");
		component.set("v.messageType", "error");
		component.set("v.message", errorMessage);
		component.set("v.showRecordMessage", true);
  },

  subscribe: function (component, event, helper) {
		const empApi = component.find('empApi');
		const channel = component.get('v.channel');
		const replayId = -1;

		const callback = function (message) {
			helper.onReceiveNotification(component, message);
    };

		empApi.subscribe(channel, replayId, $A.getCallback(callback)).then($A.getCallback(function (newSubscription) {
			component.set('v.subscription', newSubscription);
		}));
	},

	onReceiveNotification: function (component, message) {

		if(component.get("v.recordId") === message.data.payload.OrdwayLabs__RecordId__c){
      if (message.data.payload.OrdwayLabs__Status__c === component.get("v.inSyncStatus")
      || message.data.payload.OrdwayLabs__Status__c ===  component.get("v.activeContractStatus")) {
        this.handleSuccess(component, message.data.payload.OrdwayLabs__Message__c);
      }
      else{
        this.handleError(component, message.data.payload.OrdwayLabs__Message__c);
      }
		}
	},

  handleSyncToOrdway: function (component, event, helper) {
    helper.showSpinnerHelper(component, event, helper);
    var action = component.get("c.syncToOrdway");
		action.setParams({
			"contractId": component.get("v.recordId")
    });

		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				this.handleSuccess(component,'Contract Synced Successfully with Ordway');
			}
			else {
				this.handleError(component, action.getError()[0].message);
			}
			this.hideSpinnerHelper(component, event, helper);
		});
		$A.enqueueAction(action);
	}
})