({
  onInit: function (cmp, event, helper) {
		helper.checkPermissionAssignement(cmp, event, helper);
		helper.getNamedCredentialHelper(cmp, event, helper);
  },

	validateSyncToOrdway : function (component, event, helper) {
	
    var ordwayContractObject = component.get("v.ordwayContractRecord");
   
    if (ordwayContractObject.OrdwayLabs__DeferStartDate__c == false &&
      (!ordwayContractObject.OrdwayLabs__ContractEffectiveDate__c
			|| !ordwayContractObject.OrdwayLabs__BillingStartDate__c
      || !ordwayContractObject.OrdwayLabs__ServiceStartDate__c)) {
        helper.handleError(component, "Populate Contract Effective Date, Service Start Date and Billing Start Date before Syncing with Ordway");
    }
    else if (ordwayContractObject.OrdwayLabs__DeferStartDate__c == true && !ordwayContractObject.OrdwayLabs__ContractEffectiveDate__c) {
      helper.handleError(component, "Populate Contract Effective Date before Syncing with Ordway");
    }
		else if(!ordwayContractObject.OrdwayLabs__OrdwaySubscriptionID__c || ordwayContractObject.OrdwayLabs__SyncStatus__c === 'Not Synced'){
			helper.handleSyncToOrdway(component, event, helper);
		}
		else {
      var navService = component.find("navService");
      var pageReference = {
        type: 'standard__component',
        attributes: {
            componentName: 'OrdwayLabs__OrdwayFieldComparison',
        },
        state: {
            "c__parentRecordObject": component.get("v.ordwayContract"),
            "c__isSync": true
        }
      };
      event.preventDefault();
      navService.navigate(pageReference);
		}
	},

	handleView: function (component, event, helper) {
		var subId = component.get("v.ordwayContractRecord").OrdwayLabs__OrdwaySubscriptionID__c;
		var namedCredentialVar = component.get("v.namedCredential");
		var URL = namedCredentialVar+'/subscriptions/'+subId;
		var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": URL
    });
    urlEvent.fire();
	},

	handleCancelContract : function (component, event, helper) {
			var modalBody;
      $A.createComponent("c:DynamicModalComponent", {"ordwayContract" : component.get("v.ordwayContract")},
        function(content, status) {
          if (status === "SUCCESS") {
            modalBody = content;
            component.find('overlayLib').showCustomModal({
                header: "Cancel Contract("+component.get("v.ordwayContractRecord").OrdwayLabs__OrdwaySubscriptionID__c+")?",
                body: modalBody, 
                showCloseButton: true,
                closeCallback: function() {
                }
            });
          }
        });
	},
	

	handleActivateSubscription: function (component, event, helper) {
		helper.showSpinnerHelper(component, event, helper);
		var action = component.get("c.activateSubscription");
		action.setParams({
			"ordwayContractString": JSON.stringify(component.get("v.ordwayContract"))
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
      if (state === "SUCCESS") {
        helper.handleSuccess(component, 'Subscription Activated Successfully');
      }
			else {
				helper.handleError(component, action.getError()[0].message);
			}
			component.set("v.showSpinner", false);
		});
		$A.enqueueAction(action);
	},

	redirectToFieldComparison: function(component, event, helper) {
		helper.showSpinnerHelper(component, event, helper);
		var navService = component.find("navService");
		var pageReference = {
			type: 'standard__component',
			attributes: {
					componentName: 'OrdwayLabs__OrdwayFieldComparison',
			},
			state: {
					"c__parentRecordObject": component.get("v.ordwayContract"),
					"c__isSync": false
			}
		};
		event.preventDefault();
		navService.navigate(pageReference);
	},

	redirectToChangeRenewal: function(component, event, helper) {
		helper.showSpinnerHelper(component, event, helper);
		var isEverGreen = component.get("v.ordwayContractRecord").OrdwayLabs__ContractTerm__c == '-1' ? true : false;
		var navService = component.find("navService");
		var pageReference = {
			type: 'standard__component',
			attributes: {
				componentName: 'OrdwayLabs__CreateChangeRenewalOpportunity',
			},
			state: {
				"c__OrdwayContractID"   : component.get("v.ordwayContractRecord").Id,
				"c__OrdwayContractName" : component.get("v.ordwayContractRecord").Name,
				"c__IsAutoRenew"        : component.get("v.ordwayContractRecord").OrdwayLabs__AutoRenew__c,
				"c__IsEverGreen"        : isEverGreen
			}
		};
		event.preventDefault();
		navService.navigate(pageReference);
	},
	
	handleSaveSuccess : function(component, event, helper) {
		
	},

	refreshRecord : function (component, event, helper) {
		component.set("v.showRecordMessage", false);
		component.set("v.messageTitle", "");
		component.set("v.messageType", "");
		component.set("v.message", "");
	 $A.get('e.force:refreshView').fire();
  }
})