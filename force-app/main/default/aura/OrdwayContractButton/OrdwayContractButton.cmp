<!--
  @File Name          : OrdwayContractButton.cmp
  @Description        :
  @Author             :
  @Group              :
  @Last Modified By   : 
  @Last Modified On   : 09-22-2020
  @Modification Log   :
  Ver       Date            Author      		    Modification
  1.0    25/10/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
-->
<aura:component controller="OrdwayContractController" implements="flexipage:availableForAllPageTypes,force:hasRecordId,force:hasSObjectName" access="global">

  <aura:attribute name="showSpinner" type="Boolean" default="false"/>
  <lightning:overlayLibrary aura:id="overlayLib" />
  <aura:handler name="init" value="{!this}" action="{!c.onInit}" />
  <aura:handler name="onSaveSuccess" event="force:recordSaveSuccess" action="{!c.handleSaveSuccess}"/>
  <aura:attribute name="ordwayContract" type="Object" default="true" />
  <aura:attribute name="ordwayContractList" type="List" />
  <aura:attribute name="SyncToOrdway" type="Boolean" />
  <aura:attribute name="ActivateSubscription" type="Boolean" />
  <aura:attribute name="cancelSubscription" type="Boolean" />
  <aura:attribute name="viewInOrdway" type="Boolean" />
  <aura:attribute name="disableUpdateFromOrdway" type="Boolean"  default="true"/>
  <aura:attribute name="disableChangeRenewalButton" type="Boolean"  default="true"/>
  <aura:dependency resource="markup://force:editRecord" type="EVENT" />
  <aura:attribute name="showRecordMessage" type="Boolean" />
  <aura:attribute name="message" type="String" access="public"/>
  <aura:attribute name="namedCredential" type="String"/>
  <aura:attribute name="messageType" type="String"/>
  <aura:attribute name="messageTitle" type="String"/>
  <aura:attribute name="sObjectName" type="String" />
  <aura:attribute name="pageReference" type="Object"/>
 
  <aura:attribute name="isPermissionSetAssigned" type="Boolean" default="false"/>

  <aura:attribute name="inSyncStatus" type="String" default="In Sync" />
  <aura:attribute name="activeContractStatus" type="String" default="Active" />
  <aura:attribute name="notInSyncStatus" type="Boolean" default="Not In Sync"/>
  <aura:attribute name="recordId" type="String" />

  <!--Subscribe to Ordway Contract Plat form Event-->
  <lightning:empApi aura:id="empApi"/>
  <lightning:navigation aura:id="navService"/>
  <aura:attribute name="channel" type="String" default="/event/OrdwayLabs__OrdwayContractAsyncEvent__e"/>
  <aura:attribute name="subscription" type="Map"/>

  <aura:attribute name="ordwayContractRecord" type="Object"/>
  <force:recordData aura:id="ordwayContractRecordData"
    recordId="{!v.recordId}"
    fields="Id,Name,OrdwayLabs__Account__c,OrdwayLabs__DeferStartDate__c,OrdwayLabs__OrdwaySubscriptionID__c,OrdwayLabs__ContractTerm__c, OrdwayLabs__SyncStatus__c, OrdwayLabs__AutoRenew__c,OrdwayLabs__ContractEffectiveDate__c,OrdwayLabs__BillingStartDate__c,OrdwayLabs__ServiceStartDate__c,OrdwayLabs__OrdwayLastSyncDate__c"
    targetFields="{!v.ordwayContractRecord}"/>

  <aura:handler event="force:refreshView" action="{!c.onInit}" />
  <aura:if isTrue="{!v.showSpinner}">
		<lightning:spinner aura:id="loadingSpinnerId" alternativeText="Page loading..." />
  </aura:if>
  <aura:if isTrue="{!v.showRecordMessage}">
    <div class="slds-notify_container">
        <div class="{!'slds-notify slds-notify_toast slds-theme_'+v.messageType}" role="status">
            <span class="slds-assistive-text">{!v.messageType}</span>
                <span class="{!'slds-icon_container slds-icon-utility-'+v.messageType+' slds-icon-utility-success slds-m-right_small slds-no-flex slds-align-top'}" title="{!v.message}">
                <lightning:icon iconName="{!'utility:'+v.messageType}" size="small" variant="inverse" styleclass="slds-icon slds-icon_small"/>
            </span>
            <div class="slds-notify__content">
                <h1 class="slds-text-heading_medium ">{!v.messageTitle}</h1>
                <h2 class="slds-text-heading_small ">{!v.message}</h2>
            </div>
            <div class="slds-notify__close">
                <button class="slds-button slds-button_icon slds-button_icon-inverse" title="Close" onclick="{!c.refreshRecord}">
                    <lightning:icon iconName="utility:close" size="small" variant="inverse"/>
                    <span class="slds-assistive-text">Close</span>
                </button>
            </div>
        </div>
    </div>
</aura:if>
<aura:if isTrue="{!v.isPermissionSetAssigned}">
  <lightning:layout multipleRows="true">
    <lightning:layoutItem size="12">
      <aura:if isTrue="{! v.ordwayContract.OrdwayLabs__SyncStatus__c != 'Sync In Progress' }">
      <lightning:card>
        <img src="{!$Resource.OrdwayIcon}" style="height:20px;margin-left:1%;margin-right:5px;" />
        <lightning:button variant="neutral" label="Sync to Ordway" onclick="{!c.validateSyncToOrdway }" disabled="{!v.SyncToOrdway}"/>
        <lightning:button variant="neutral" label="Activate" onclick="{!c.handleActivateSubscription}" disabled="{!v.ActivateSubscription}" />
        <lightning:button variant="neutral" label="Update from Ordway" onclick="{!c.redirectToFieldComparison}" disabled="{!v.disableUpdateFromOrdway}"/>
        <lightning:button variant="neutral" label="Create Change / Renewal Opportunity" onclick="{!c.redirectToChangeRenewal}"
              disabled="{! or(v.disableChangeRenewalButton, v.ordwayContract.OrdwayLabs__OrdwayContractStatus__c == 'Cancelled')}"/>
        <lightning:button variant="neutral" label="View in Ordway" onclick="{!c.handleView}"
              disabled="{! or(or(empty(v.ordwayContract.OrdwayLabs__OrdwaySubscriptionID__c), empty(v.namedCredential)), v.viewInOrdway)}"/>
        <lightning:button variant="neutral" label="Cancel Contract" onclick="{!c.handleCancelContract}"
              disabled="{! or(or(empty(v.ordwayContract.OrdwayLabs__OrdwaySubscriptionID__c), v.ordwayContract.OrdwayLabs__OrdwayContractStatus__c != 'Active'), v.cancelSubscription)}"/>

        &nbsp;
        <aura:if isTrue="{!v.ordwayContract.OrdwayLabs__SyncStatus__c == 'Not In Sync'}" >
          <lightning:icon iconName="utility:warning" size="small" variant="Warning" alternativeText="Warning"/> Not In Sync
        </aura:if>
        <aura:if isTrue="{!v.ordwayContract.OrdwayLabs__SyncStatus__c == 'Sync Failed'}" >
          <lightning:icon iconName="utility:error" size="small" variant="Error" alternativeText="Error"/> Sync Failed
        </aura:if>
        <aura:if isTrue="{!v.ordwayContract.OrdwayLabs__SyncStatus__c == 'Not Synced'}" >
          <lightning:icon iconName="utility:info" size="small"  variant="Warning"  alternativeText="Info"/> Not Synced
        </aura:if>
        <aura:if isTrue="{!v.ordwayContract.OrdwayLabs__SyncStatus__c == 'In Sync'}" >
          <lightning:icon iconName="utility:success" size="small"  variant="Success"  alternativeText="Info"/> In Sync
        </aura:if>
      </lightning:card>
      </aura:if>

      <aura:if isTrue="{! v.ordwayContract.OrdwayLabs__SyncStatus__c == 'Sync In Progress' }">
        <lightning:card>
          <div>
            <lightning:spinner class="spinnerCss" alternativeText="Loading" variant="Brand" title="Sync In Progress"/>
            <span class="slds-p-left_x-large" style="text-align: center;color: #e36c26;"> Sync In Progress... </span>
          </div>
          <br/>
        </lightning:card>
      </aura:if>

    </lightning:layoutItem>
  </lightning:layout>
  </aura:if>
</aura:component>