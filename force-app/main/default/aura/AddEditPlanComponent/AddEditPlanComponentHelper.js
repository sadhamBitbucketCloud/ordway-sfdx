({
  showSpinnerHelper: function (component, event, helper) {
    component.set("v.showSpinner", true);
  },

  hideSpinnerHelper: function (component) {
    component.set("v.showSpinner", false);
  },

  getOrdwayPlans: function (component, event, helper) {

    var action = component.get("c.getInitialWrapper");
    action.setParams({
      opportunityId: component.get("v.opportunityId"),
      pricebookId: component.get("v.pricebookId")
    });

    action.setCallback(this, function (response) {
      var selectedPlanMapVar = component.get("v.selectedPlanMap");

      var state = response.getState();
      if (state == "SUCCESS" && response.getReturnValue() != null) {
        var thisInitialWrapper = response.getReturnValue();

        //Dynamic Field Map
        var requiredPlanObjectWrapperVar = thisInitialWrapper.requiredPlanObjectWrapper;
        component.set("v.opportunityCurrencyISO", thisInitialWrapper.currencyISO);
        component.set("v.requiredFieldsMap", requiredPlanObjectWrapperVar);
        component.set("v.requiredFields", Object.values(requiredPlanObjectWrapperVar));
        component.set("v.readOnlyFields", Object.values(thisInitialWrapper.readOnlyPlanObjectWrapper));
        var requiredPlanObjectVarLength = Object.values(requiredPlanObjectWrapperVar).length;
        var readPlanObjectVarLength = Object.values(thisInitialWrapper.readOnlyPlanObjectWrapper).length;
        if(readPlanObjectVarLength
          && readPlanObjectVarLength > 0){
            requiredPlanObjectVarLength = requiredPlanObjectVarLength + readPlanObjectVarLength;
        }
        component.set("v.requiredFieldsSize", requiredPlanObjectVarLength);
        component.set("v.opportunityObject", thisInitialWrapper.thisOpportunity);

        //Ordway OLI Plans
        if (thisInitialWrapper.oliMap != null) {
          var planMap = {};
          var planList = [];

          var plansMapObject = thisInitialWrapper.oliMap;
          var keyList = Object.keys(plansMapObject).sort();

          for (var i = 0; i < keyList.length; i++) {
            var thisPlan = {};
            
            var thisPlanIdValue = keyList[i].split("_")[0];
            thisPlan["planId"] = keyList[i].split("_")[0];
            thisPlan["planName"] = keyList[i].split("_")[1];
            thisPlan["isSelected"] = false;
            thisPlan["hasExistingPlan"] = false;
            thisPlan["planList"] = plansMapObject[keyList[i]];
                  for (let j = 0; j < plansMapObject[keyList[i]].length; j++) {
                    plansMapObject[keyList[i]][j].isChargeSelected = false;
                    if (plansMapObject[keyList[i]][j].Id != undefined
                    || plansMapObject[keyList[i]][j].OrdwayLabs__ContractLineItem__c != undefined) {
                      thisPlan["hasExistingPlan"] = true;
                      thisPlan["isSelected"]      = true;
                      selectedPlanMapVar[thisPlanIdValue] = thisPlan;
                      plansMapObject[keyList[i]][j].isChargeSelected = true;
                      component.set("v.deleteExistingCharges", true);
                    }
                  }

            planMap[thisPlanIdValue] = thisPlan;
            planList.push(thisPlan);
          }

          if (planList.length > 0) {
            
            component.set("v.selectedPlanMap", selectedPlanMapVar);
            var selectedPlansOnly = Object.values(selectedPlanMapVar);
            component.set("v.selectedPlanList", selectedPlansOnly);
            component.set("v.allPlanList", planList);
            component.set("v.allPlanMap", planMap);
  
            var pageSizeVar = component.get("v.pageSize");
            component.set("v.maxPageNumber", Math.floor((planList.length + pageSizeVar - 1) / pageSizeVar));
            component.set("v.isPlansAvailable", true);

            if (selectedPlansOnly.length > 0) {
              component.set("v.showAllRecord", false);
              component.set("v.maxPageNumber", Math.floor((selectedPlansOnly.length + pageSizeVar - 1) / pageSizeVar));
            }
          }
          else{
            component.set("v.isPlansAvailable", false);
            this.hideSpinnerHelper(component);
          }
          this.reRenderTableList(component);    
        }
        component.set("v.salesforeOrgBaseUrl", thisInitialWrapper.salesforceBaseURL);
        
      } else {
        this.hideSpinnerHelper(component);
        this.handleError(component, JSON.stringify(action.getError(), 0, 2));
      }
    });
    $A.enqueueAction(action);
  },

  reRenderTableList: function (component) {
    var isSearch = component.get("v.issearching");
    component.set("v.isPlansAvailable", true);
    var allRecords;
    if(isSearch){
      allRecords = component.get("v.searchFilteredList");
    }
    else{
      var showAllrec = component.get("v.showAllRecord");
      if (showAllrec) {
        allRecords = component.get("v.allPlanList");
      } else {
        allRecords = component.get("v.selectedPlanList");
      }
    }
    
    if(allRecords.length <= 0){
      component.set("v.isPlansAvailable", false);
    }
    var pageNumber = component.get("v.currentPageNumber");
    var pageSizeVar = component.get("v.pageSize");
    var thisPageRecords = allRecords.slice((pageNumber - 1) * pageSizeVar, pageNumber * pageSizeVar);
    component.set("v.currentPageList", thisPageRecords);
    component.set("v.showSpinner", false);
  },

  sortTableByAttributeHelper: function (component, attributeName, isAsc) {
    component.set("v.showSpinner", true);
    var selectedListRecords = component.get("v.selectedPlanList");
    var allPlanRecords = component.get("v.allPlanList");
    var filteredRecords = component.get("v.searchFilteredList");
    if (isAsc) {
      if(allPlanRecords.length > 0)
      allPlanRecords.sort((a, b) => a[attributeName] > b[attributeName] ? 1 : -1);
      selectedListRecords.sort((a, b) => a[attributeName] > b[attributeName] ? 1 : -1);
      filteredRecords.sort((a, b) => a[attributeName] > b[attributeName] ? 1 : -1);
    } else {
      allPlanRecords.sort((a, b) => a[attributeName] > b[attributeName] ? -1 : 1);
      selectedListRecords.sort((a, b) => a[attributeName] > b[attributeName] ? -1 : 1);
      filteredRecords.sort((a, b) => a[attributeName] > b[attributeName] ? -1 : 1);
    }
    component.set("v.allPlanList", allPlanRecords);
    component.set("v.selectedPlanList", selectedListRecords);
    component.set("v.searchFilteredList", filteredRecords);
    this.reRenderTableList(component);
  },

  resetSearchFilter: function (component, event, helper) {
    component.set("v.searchFilteredList", []);
    component.set("v.issearching", false);
    component.set("v.searchFilterValue", '');
  },

  handleSuccess: function (component, successMessage) {
    component.set("v.messageTitle", "Success!");
    component.set("v.messageType", "success");
    component.set("v.message", successMessage);
    component.set("v.showRecordMessage", true);
  },

  handleError: function (component, errorMessage) {
    component.set("v.messageTitle", "Error");
    component.set("v.messageType", "error");
    component.set("v.message", errorMessage);
    component.set("v.showRecordMessage", true);
  },

  validateSelectedTierPlanFields: function (component) {
    var selectedTierPlan = component.get("v.selectedPlanToViewTierPlan");
    var hasEmptyFields = false;
    var tierPlanLength = selectedTierPlan["tierList"].length;
    for(var i=0; i<tierPlanLength; i++){
      if(( (i !== tierPlanLength -1) && (!selectedTierPlan["tierList"][i]["ending_unit"] || selectedTierPlan["tierList"][i]["ending_unit"] == null || selectedTierPlan["tierList"][i]["ending_unit"] == undefined) )
      || (!selectedTierPlan["tierList"][i]["price"] || selectedTierPlan["tierList"][i]["price"] == null || selectedTierPlan["tierList"][i]["price"] == undefined)){
        hasEmptyFields = true;
        break;
      }
    }
    component.set("v.disableTierPlanDone", hasEmptyFields);
  },

  sortAfterPlanSelected: function (component, event, helper) {
    component.set("v.showSpinner", true);
    var lastSortby = component.get("v.lastSortedBy");
    if(lastSortby){
      if(lastSortby === 'planName'){
        var isPlanNameAscending = component.get("v.isPlanNameAsc");
        this.sortTableByAttributeHelper(component, "planName", isPlanNameAscending);
      }
      else if(lastSortby === 'planId'){
        var isPlanIdAscending = component.get("v.isPlanIdAsc");
        this.sortTableByAttributeHelper(component, "planId", isPlanIdAscending);
      }
    }
    component.set("v.showSpinner", false);
  }
});