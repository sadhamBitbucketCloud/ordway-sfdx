({
  fetchErrorRecords: function (component, limit) {
    var action = component.get("c.getErrorLog");
    action.setParams({
      recordId: component.get("v.recordId"),
      limitValue: limit
    });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var records = response.getReturnValue();
        records.forEach(function (record) {
          record.linkName = '/' + record.Id;
        });
        component.set("v.errorLogs", records);
        component.set("v.allErrorLogs", records);
      } else if (state === "ERROR") {
        var errors = response.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
            
          }
        } else {
          
        }
      }
    });
    $A.enqueueAction(action);
  }
})