({
  doInit: function (component, event, helper) {

    component.set('v.errorLogColumns', [{
        label: 'Log Id',
        fieldName: 'linkName',
        type: 'url',
        typeAttributes: {
          label: {
            fieldName: 'Name'
          },
          target: '_blank'
        }
      },
      {
        label: 'Class',
        fieldName: 'OrdwayLabs__InitialClass__c',
        type: 'text'
      },
      {
        label: 'Function',
        fieldName: 'OrdwayLabs__InitialFunction__c',
        type: 'text'
      },
      {
        label: 'Created Date',
        fieldName: 'CreatedDate',
        type: 'date'
      }
    ]);

    helper.fetchErrorRecords(component, 5);
  },

  showHasErrorController : function (component, event, helper) {
    var allErrorLogsVar = component.get("v.allErrorLogs");
    var hasErrorLogs = [];
    for(var i=0; i<allErrorLogsVar.length; i++)  {
      if(allErrorLogsVar[i].OrdwayLabs__HasError__c){
        hasErrorLogs.push(allErrorLogsVar[i]);
      }
    }
    component.set("v.errorLogs", hasErrorLogs);
    component.set("v.showHasError", false);
  },

  showAllErrorController : function (component, event, helper) {
    component.set("v.showHasError", true);
    var allErrorLogsVar = component.get("v.allErrorLogs");
    component.set("v.errorLogs", allErrorLogsVar);
  },
})