({
  closeModel: function (component, event, helper) {
    component.find("overlayLib").notifyClose();
  },

  handleSuccess: function (component, successMessage) {
    component.set("v.messageTitle", "Success!");
    component.set("v.messageType", "success");
    component.set("v.message", successMessage);
    component.set("v.showRecordMessage", true);
  },

  handleError: function (component, errorMessage) {
    component.set("v.messageTitle", "Error");
    component.set("v.messageType", "error");
    component.set("v.message", errorMessage);
    component.set("v.showRecordMessage", true);
  },

  getContractDate : function (component, event, helper) {
    component.set("v.showSpinner", true);
    var contractDate = component.get("v.contractEffectiveDate");
    var action = component.get("c.getContractLastDate");
    action.setParams({
        contractEffectiveDate : contractDate
    });
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        component.set("v.lastActiveDate", response.getReturnValue());
        component.set("v.showSpinner", false);
      } 
      else {
        component.set("v.showSpinner", false);
      }
    });
    $A.enqueueAction(action);
}

})