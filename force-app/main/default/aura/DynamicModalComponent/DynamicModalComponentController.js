({
    handleClose : function (component, event, helper) {
        helper.closeModel(component, event, helper);
        $A.get('e.force:refreshView').fire();
    },

    handleDateChange : function (component, event, helper) {
      helper.getContractDate(component, event, helper);
    },

    handleCancelQuote: function (component, event, helper) {
        component.set("v.disableCancel", true);
        component.set("v.disableKeep", true);
        var contractVar = component.get("v.ordwayContract");
        contractVar.OrdwayLabs__CancellationDate__c = component.get("v.contractEffectiveDate");
        
        var action = component.get("c.cancelSubscription");
        action.setParams({
            ordwayContractString : JSON.stringify(contractVar)
        });
        action.setCallback(this, function (response) {
          var state = response.getState();
          if (state === "SUCCESS") {
             helper.handleSuccess(component,'Subscription Cancelled Successfully');
          } 
          else {
             helper.handleError(component, action.getError()[0].message);
             component.set("v.disableKeep", false);
          }
        });
        $A.enqueueAction(action);
    }
    
})