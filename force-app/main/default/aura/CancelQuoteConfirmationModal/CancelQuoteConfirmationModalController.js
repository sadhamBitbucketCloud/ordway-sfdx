({
    handleClose : function (component, event, helper) {
        helper.closeModel(component, event, helper);
        $A.get('e.force:refreshView').fire();
    },

    handleCancelQuote: function (component, event, helper) {
        var action = component.get("c.cancelQuote");
        action.setParams({
          quoteId: component.get("v.quoteId")
        });
        action.setCallback(this, function (response) {
          var state = response.getState();
      
          if (state === "SUCCESS") {
            helper.handleSuccess(component,'Quote Cancelled Successfully');
            component.set("v.disableCancel" , true);
            component.set("v.disableKeep" , true);
          } 
          else {
            helper.handleError(component, action.getError()[0].message);
            component.set("v.disableCancel" , true);
          }
        });
        $A.enqueueAction(action);
    }
    
})