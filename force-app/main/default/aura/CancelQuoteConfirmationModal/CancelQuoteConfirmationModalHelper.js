({
    closeModel : function (component, event, helper) {
        component.find("overlayLib").notifyClose();
    },
    
    handleSuccess : function(component, successMessage) {
		component.set("v.messageTitle", "Success!");
		component.set("v.messageType", "success");
		component.set("v.message", successMessage);
		component.set("v.showRecordMessage", true);
	},

	handleError: function (component, errorMessage) {
		component.set("v.messageTitle", "Error");
		component.set("v.messageType", "error");
		component.set("v.message", errorMessage);
		component.set("v.showRecordMessage", true);
    }
})