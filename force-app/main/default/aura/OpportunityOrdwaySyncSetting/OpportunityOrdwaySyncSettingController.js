({
    doInit: function (component, event, helper) {
        var action = component.get("c.getOpportunityOrdwayContractSync");
        action.setCallback(this, function (a) {
            var state = a.getState();
            if (state == "SUCCESS") {
                var result = a.getReturnValue();
                if (!$A.util.isEmpty(result) && !$A.util.isUndefined(result))
                    component.set("v.contractSyncData", result);
            } else if (state == "ERROR") {}
        });
        $A.enqueueAction(action);
    }
})