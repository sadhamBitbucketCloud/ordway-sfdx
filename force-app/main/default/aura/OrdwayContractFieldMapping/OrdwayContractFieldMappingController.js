({
  doInit: function (component, event, helper) {
    helper.getApplicationSetting(component, event, helper);
  },

  getObjectFields: function (component, event, helper) {
    component.set("v.showSearch", false);
    helper.showSpinnerHelper(component, event, helper);
    helper.getObjectFieldsHelper(component, event, helper);
  },

  handleSave: function (component, event, helper) {
    var descriptionField = component.find("whereItIsUsedDescriptionId");
    if ($A.util.isEmpty(component.find("whereItIsUsedDescriptionId").get("v.value")) || $A.util.isUndefined(component.find("whereItIsUsedDescriptionId").get("v.value"))) {
      descriptionField.setCustomValidity("Please enter the description");
      descriptionField.reportValidity();
      return;
    } else {
      descriptionField.setCustomValidity("");
      descriptionField.reportValidity();
      component.set("v.showSaveUpdate", false);
      helper.showSpinnerHelper(component, event, helper);
      helper.deployMetadataHelper(component, event, helper);
    }
  },

  onTargetObjectChange: function (component, event, helper) {
    component.set("v.showSearch", true);
    component.set("v.showSaveUpdate", false);
    component.set("v.showTable", false);
  },

  objectOnchange: function (component, event, helper) {
    var selectedSource = component.get("v.selectedSourceObject");
    var targetObjectListMap = component.get('v.targetObjectMap');
    var targetListOption = targetObjectListMap[selectedSource];
    component.set("v.selectedTargetObject", targetObjectListMap[selectedSource][0].value);
    if (targetListOption != undefined
      && targetListOption != null
      && targetListOption.length > 1) {
      component.set("v.disableTargetObjectList", false);
    }
    else {
      component.set("v.disableTargetObjectList", true);
    }
    component.set("v.targetObjectList", targetListOption);
    component.set("v.showSearch", true);
    component.set("v.showSaveUpdate", false);
    component.set("v.showTable", false);
  },

  handleSourceChange: function (component, event, helper) {
    component.set("v.showSaveUpdate", true);
  },
})