({

	getApplicationSetting : function(component, event, helper){
		var action = component.get("c.getContractPickerSettings");
		action.setCallback(this, function(response){
		var state = response.getState();
			if(state =='SUCCESS'){
				var thisResponse = response.getReturnValue();
				component.set("v.opportunityInstance", thisResponse);
				this.configureTargetConfigurations(component, event, helper);
			}
			else{
				this.handleError(action.getError()[0].message);
				this.hideSpinnerHelper(component, event, helper);
			}
		});
		$A.enqueueAction(action);
	},

	configureTargetConfigurations : function(component, event, helper){
		component.set("v.selectedTargetObject", 'Please Select Source Object');
		var opportunityInstance = component.get("v.opportunityInstance");
		var sourceObjectOptions = [
			{ value: "noSourceSelected", label: "---Select Source Object---" },
			{ value: "Opportunity", label: "Opportunity" },
      { value: "OpportunityLineItem", label: "Opportunity Product" },
      { value: "OrdwayLabs__OrdwayContract__c", label: "Ordway Contract" },
      { value: "OrdwayLabs__ContractLineItem__c", label: "Ordway Contract Line Item" },
      { value: "OrdwayLabs__Charge__c", label: "Ordway Charge" },
			{ value: "Product2", label: "Product (Product2)" }
		];

		if(!opportunityInstance.OrdwayLabs__EnableMultiEntity__c){
			sourceObjectOptions.push({ value: "OrdwayLabs__OrdwayProduct__c", label: "Ordway Product" });
		}

		var targetObjectListMap         =  {};
		var opportunityTarget           =  [{ value: "", label: "---Select Target Object---" }, { value: "OrdwayLabs__OrdwayContract__c", label: "Ordway Contract" }];
		var opportunityLineItemTarget   =  [{ value: "", label: "---Select Target Object---" }, { value: "OrdwayLabs__ContractLineItem__c", label: "Ordway Contract Line Item" }];
    var chargeTarget   =  [{ value: "", label: "---Select Target Object---" }, { value: "OpportunityLineItem", label: "Opportunity Product" }];
		
		if(opportunityInstance.OrdwayLabs__EnableQuotes__c){
			opportunityTarget.push({ value: "OrdwayLabs__Quote__c", label: "Ordway Quote" });
			opportunityLineItemTarget.push({ value: "OrdwayLabs__QuoteLineItem__c", label: "Ordway Quote Line Item" });
      sourceObjectOptions.push({ value: "OrdwayLabs__Quote__c", label: "Ordway Quote" }, { value: "OrdwayLabs__QuoteLineItem__c", label: "Ordway Quote Line Item" });
      chargeTarget.push({ value: "OrdwayLabs__QuoteLineItem__c", label: "Ordway Quote Line Item" });
		}
		
		targetObjectListMap["Opportunity"]                     = opportunityTarget;
		targetObjectListMap["OpportunityLineItem"]             = opportunityLineItemTarget;
		targetObjectListMap["OrdwayLabs__OrdwayContract__c"]   = [{ value: "Opportunity", label: "Opportunity" }];
    targetObjectListMap["Product2"] = [{ value: "OrdwayLabs__OrdwayProduct__c", label: "Ordway Product" }];
    targetObjectListMap["OrdwayLabs__OrdwayProduct__c"]   = [{ value: "Product2", label: "Product" }];
    targetObjectListMap["OrdwayLabs__ContractLineItem__c"] = [{ value: "OpportunityLineItem", label: "Opportunity Product" }];
    targetObjectListMap["OrdwayLabs__Charge__c"] = chargeTarget;

		if(opportunityInstance.OrdwayLabs__EnableQuotes__c){
			targetObjectListMap["OrdwayLabs__Quote__c"]            = [{ value: "Opportunity", label: "Opportunity" }];
      targetObjectListMap["OrdwayLabs__QuoteLineItem__c"] = [{ value: "OpportunityLineItem", label: "Opportunity Product" }];
		}

		targetObjectListMap["noSourceSelected"]                = [{ value: "", label: "Please Select Source Object" }];
		component.set("v.targetObjectMap", targetObjectListMap);
		
		component.set("v.targetObjectList", [{ value: "", label: "Please Select Source Object" }]);
		component.set("v.sourceObjectList", sourceObjectOptions);
			this.hideSpinnerHelper(component, event, helper);
	},

	getObjectLabelListHelper : function(component, event, helper) {
		var action = component.get("c.getObjectListWrapper");
		action.setCallback(this, function(response){
		var state = response.getState();
			if(state =='SUCCESS'){
				component.set("v.objectLabeWrapperList", response.getReturnValue());
				this.hideSpinnerHelper(component, event, helper);
			}
			else{
				this.handleError(action.getError()[0].message);
				this.hideSpinnerHelper(component, event, helper);
			}
		});
		$A.enqueueAction(action);
	},

	getObjectFieldsHelper : function(component, event, helper) {
		var action = component.get("c.getObjectFieldsWrapperList");
		action.setParams({
			selectedSourceObjectName: component.get("v.selectedSourceObject"),
			selectedTargetObjectName : component.get("v.selectedTargetObject")
		});

		action.setCallback(this, function(response){
		var state = response.getState();
			if(state =='SUCCESS'){
				var fieldWrapper =  response.getReturnValue();
				var targetObjectFields = [];
				fieldWrapper.targetObjectFieldWrapperList.forEach(targetField =>{
					if(targetField.fieldLabel != null
						&& targetField.fieldLabel != undefined
						&& targetField.fieldDataTypeKey != null
						&& targetField.fieldDataTypeKey != undefined){
							targetObjectFields.push(targetField);
						}
				});
				component.set("v.targetObjectFieldList", targetObjectFields);
				component.set("v.isExisting", fieldWrapper.isExistingMapping);
				component.set("v.responseDataTypeFieldMap", fieldWrapper.dataTypeFieldMap);
				component.set("v.showTable", true);
				component.set("v.selectedFieldList", fieldWrapper.selectedField);
				component.set("v.whereItIsUsed", fieldWrapper.description);
				this.generateDataTypeMap(component, event, helper);
				this.hideSpinnerHelper(component, event, helper);
			}
			else{
				this.handleError(action.getError()[0].message);
				this.hideSpinnerHelper(component, event, helper);
			}
		});
		$A.enqueueAction(action);
	},
	
	generateDataTypeMap : function(component, event, helper) {
		 var dataMap = component.get("v.responseDataTypeFieldMap"); 
		var arrayMapKeys = [];
		for(var key in dataMap){
			arrayMapKeys.push({key: key, value: dataMap[key]});
		}
		component.set("v.dataTypeFieldMap", arrayMapKeys);
	},   

	deployMetadataHelper : function(component, event, helper) {
		var mappingList = component.get("v.targetObjectFieldList");
		var fieldsMapping = [];
		for(var i = 0; i< mappingList.length; i++){
			if(mappingList[i].sourceFieldAPIName.trim() != null
			&& mappingList[i].sourceFieldAPIName.trim().length > 1){
				fieldsMapping.push(mappingList[i]);
			}
		}
		var action = component.get("c.createUpdateOrdwayFieldMappingMetadata");
		action.setParams({
			sourceObjectName : component.get("v.selectedSourceObject"),
			targetObjectName : component.get("v.selectedTargetObject"),
			fieldMappingJSON : JSON.stringify(fieldsMapping),
			description      : component.get("v.whereItIsUsed"),
		});

		action.setCallback(this, function(response){
		var state = response.getState();
			if(state =='SUCCESS'){
				var jobIDVar = response.getReturnValue();
				if(component.get("v.isExisting")){
					this.deployedStatusToastNotification(jobIDVar);
				}
				else{
					this.deployedStatusToastNotification(jobIDVar);
				}
				this.hideSpinnerHelper(component, event, helper);
			}
			else{
				this.handleError(action.getError()[0].message);
				this.hideSpinnerHelper(component, event, helper);
			}
		});
		$A.enqueueAction(action);
		},

		deployedStatusToastNotification : function(deploymentStatusURL) {
		var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        mode: 'sticky',
				type : 'success',
        message: 'Allow from 5-10 minutes for your changes to deploy on the server before using the application.',
        messageTemplate: 'Allow from 5-10 minutes for your changes to deploy on the server before using the application. {0}!',
        messageTemplateData: [{
            url: deploymentStatusURL,
            label: 'Click here to view the deployment status',
            }
        ]
    });
    toastEvent.fire();
	},

	saveFieldMappingHelper : function(component, event, helper) {
		var mappingList = component.get("v.sourceObjectFieldList");
		var fieldsMapping = [];
		for(var i = 0; i< mappingList.length; i++){
			if(mappingList[i].sourceFieldAPIName.trim() != null
			&& mappingList[i].sourceFieldAPIName.trim().length > 1){
				fieldsMapping.push(mappingList[i]);
			}
		}
		var action = component.get("c.createNewFieldMapping");
		action.setParams({
			selectedSourceObjectName : component.get("v.selectedSourceObject"),
			selectedTargetObjectName : component.get("v.selectedTargetObject"),
			fieldMappingJSON         : JSON.stringify(fieldsMapping)
		});

		action.setCallback(this, function(response){
		var state = response.getState();
			if(state =='SUCCESS'){
				this.handleSuccess('Filed Mapping inserted successfully')
				this.hideSpinnerHelper(component, event, helper);
			}
			else{
				this.handleError(action.getError()[0].message);
				this.hideSpinnerHelper(component, event, helper);
			}
		});
		$A.enqueueAction(action);
   	},   
		 
		 updateFieldMappingHelper : function(component, event, helper) {
			var mappingList = component.get("v.sourceObjectFieldList");
			var fieldsMapping = [];
			for(var i = 0; i< mappingList.length; i++){
				if(mappingList[i].sourceFieldAPIName.trim() != null
				&& mappingList[i].sourceFieldAPIName.trim().length > 1){
					fieldsMapping.push(mappingList[i]);
				}
			}
			var action = component.get("c.updateFieldMapping");
			action.setParams({
				selectedSourceObjectName : component.get("v.selectedSourceObject"),
				selectedTargetObjectName : component.get("v.selectedTargetObject"),
				fieldMappingJSON         : JSON.stringify(fieldsMapping)
			});
	
			action.setCallback(this, function(response){
			var state = response.getState();
				if(state =='SUCCESS'){
					this.handleSuccess('Filed Mapping updated successfully')
					this.hideSpinnerHelper(component, event, helper);
				}
				else{
					this.handleError(action.getError()[0].message);
					this.hideSpinnerHelper(component, event, helper);
				}
			});
			$A.enqueueAction(action);
			 },   

	showSpinnerHelper : function(component, event, helper) {
		component.set("v.showSpinner", true);
	},

	hideSpinnerHelper : function(component, event, helper) {
		component.set("v.showSpinner", false);
	},

	handleSuccess : function(successMessage) {
		var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
			"title"  : "Success!",
			"mode"   : "sticky",
			"type"   : "success",
      "message": successMessage
    });
    toastEvent.fire();
	},

	handleError : function(errorMessage) {
		var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
			"title"  : "Error",
			"mode"   : "sticky",
			"type"   : "error",
      "message": errorMessage
    });
    toastEvent.fire();
	}
})