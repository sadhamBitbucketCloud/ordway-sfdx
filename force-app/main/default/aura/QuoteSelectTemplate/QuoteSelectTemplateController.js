({
    onInit : function (component, event, helper) {
        var quoteRecordVal = component.get("v.quoteRecordVal");
        var action = component.get("c.getQuoteTemplates");
        action.setParams({
			quoteId: quoteRecordVal.Id,
		});
		action.setCallback(this, function (response) {
			var state = response.getState();

			if (state == 'SUCCESS') {
                var thisResponse = [];
				thisResponse = response.getReturnValue();
				if (thisResponse) {
                    component.set("v.templateValuesList", thisResponse);
				}
			} else {
				var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "mode": 'sticky',
                    "title": "Error",
                    "type": "error",
                    "message": action.getError()[0].message
                });
                toastEvent.fire();
			}
		});
		$A.enqueueAction(action);

    },
    
    closeModel : function (component, event, helper) {
        component.find("overlayLib").notifyClose();
        $A.get('e.force:refreshView').fire();
    },

    onChangeTemplate : function (component, event, helper) {
        var selectedTemplateVar = component.get("v.selectedTemplateId");
        var quoteRecordVal = component.get("v.quoteRecordVal");
        if(selectedTemplateVar && selectedTemplateVar != quoteRecordVal.OrdwayLabs__QuoteTemplate__c){
            component.set("v.disableSave", false);
        }
        else{
            component.set("v.disableSave", true);
        }
    },
    
    onSelectTemplateSave: function (component, event, helper) {
        var selectedTemplateVar = component.get("v.selectedTemplateId");
        var quoteRecordVal = component.get("v.quoteRecordVal");
		var action = component.get("c.updateQuoteTemplate");
		action.setParams({
			quoteId: quoteRecordVal.Id,
			templateId : selectedTemplateVar  
		});
		action.setCallback(this, function (response) {
			var state = response.getState();

			if (state == 'SUCCESS') {
				var thisResponse = response.getReturnValue();
				if (thisResponse) {
                    component.find("overlayLib").notifyClose();
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "mode": 'dismissible',
                            "title": "",
                            "type": "success",
                            "message":  "Quote Template Updated Successfully!"
                        });
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();
				}
			} else {
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "mode": 'sticky',
                        "title": "Error",
                        "type": "error",
                        "message": action.getError()[0].message
                    });
                toastEvent.fire();
			}
		});
		$A.enqueueAction(action);
	},

})