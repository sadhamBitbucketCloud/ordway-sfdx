({
    doInit : function(component,event,helper){	        	
        var pageRef = component.get("v.pageReference");	
        var state = pageRef.state;
        var base64Context = state.inContextOfRef;	
        if (base64Context.startsWith("1\.")) {	            
            base64Context = base64Context.substring(2);	
        }
        var addressableContext = JSON.parse(window.atob(base64Context));	
        component.set("v.recId", addressableContext.attributes.recordId);
        component.set("v.objectName", addressableContext.attributes.objectApiName);
        component.set("v.isLoaded", true);
    }, 

    reInit : function(component, event, helper) { 
        $A.get('e.force:refreshView').fire();
    }
})