({
    enableQuotesHelper : function(component, event, helper) {
        var action = component.get("c.saveOpportunitySetting");
        action.setParams({
            opportunitySettingInstance: component.get("v.opportunityInstance")
        });
    
		action.setCallback(this, function (response) {
			var state = response.getState();

			if (state == 'SUCCESS') {
                 component.find("overlayLib").notifyClose();
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "mode": 'dismissible',
                            "title": "",
                            "type": "success",
                            "message":  "Quote Activated Successfully!"
                        });
                    toastEvent.fire();
                    $A.get("e.c:setOpportunityTabEvent").fire();
            }
            else {
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "mode": 'sticky',
                        "title": "Error",
                        "type": "error",
                        "message": action.getError()[0].message
                    });
                toastEvent.fire();
            }
            component.set("v.showSpinner", false);
            component.set("v.isYesOrNoButtonClicked", false);
		});
		$A.enqueueAction(action);
    }   
})