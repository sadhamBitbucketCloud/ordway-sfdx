({
    closeModel : function (component, event, helper) {
        component.find("overlayLib").notifyClose();
        $A.get("e.c:setOpportunityTabEvent").setParams({changeDefaultTab : "close"}).fire();;
    },

    activateController : function (component, event, helper) {
        component.set("v.isYesOrNoButtonClicked", true);
        component.set("v.showSpinner", true);
		var action = component.get("c.activateQuote");
    
		action.setCallback(this, function (response) {
			var state = response.getState();

			if (state == 'SUCCESS') {
                helper.enableQuotesHelper(component, event, helper);
			} else {
                component.set("v.showSpinner", false);
                component.set("v.isYesOrNoButtonClicked", false);
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "mode": 'sticky',
                        "title": "Error",
                        "type": "error",
                        "message": action.getError()[0].message
                    });
                toastEvent.fire();
			}
		});
		$A.enqueueAction(action);
    },
    
})