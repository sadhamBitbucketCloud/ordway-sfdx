({

  checkPermissionAssignement: function (component, event, helper) {
    var action = component.get("c.isPermissionSetAssigned");
    // set call back
    action.setCallback(this, function (response) {
      var state = response.getState();

      if (state === "SUCCESS"
        && response.getReturnValue()) {
        component.set("v.isPermissionSetAssigned", true);
        this.addProducts(component, event);
        this.checkQuotesEnabled(component, event);
      }
    });
    //adds the server-side action to the queue
    $A.enqueueAction(action);
  },

  checkQuotesEnabled : function (component, event) {

    var action = component.get("c.isQuoteObjectEnabled");
    // set call back
    action.setCallback(this, function (response) {
      var state = response.getState();

      if (state === "SUCCESS") {
        component.set("v.isQuoteObjectEnabled" , response.getReturnValue());
      } else {
      }
    });
    //adds the server-side action to the queue
    $A.enqueueAction(action);
  },

  addProducts: function (component, event) {
    var action = component.get("c.getDataWrapper");
    action.setParams({
      "opportunityId": component.get("v.recordId"),
    });
    // set call back
    action.setCallback(this, function (response) {
      var state = response.getState();

      if (state === "SUCCESS") {
        var result = response.getReturnValue();

        component.set("v.currentOpportunity", result.currentOpportunity);
        component.set("v.showPlanPicker", result.isNewPlanPickerToShow);
        if (result.currentOpportunity.OrdwayLabs__OrdwayContract__c !== undefined) {
          component.set("v.contractLabel", 'Update Ordway Contract');
        }

        //Add Plan Button visibility
        if (result.currentOpportunity.OrdwayLabs__OrdwayContract__c == null
          || (result.currentOpportunity.OrdwayLabs__OrdwayContract__c != null
            && result.currentOpportunity.OrdwayLabs__OrdwayContractStatus__c == "Draft")
          || (result.currentOpportunity.OrdwayLabs__OrdwayContract__c != null
            && result.currentOpportunity.OrdwayLabs__OrdwayContractStatus__c == "Active"
            && result.currentOpportunity.OrdwayLabs__OrdwayOpportunityType__c != 'New Business')) {
          component.set("v.showAddPlans", true);
        }

        component.set("v.pricebookId", result.currentOpportunity.Pricebook2Id);
      } else {
      }
    });
    //adds the server-side action to the queue
    $A.enqueueAction(action);
  },

  createOrdwayContractHelper: function (component, event, helper) {
    this.showSpinnerHelper(component, event, helper);
    var action = component.get("c.createOrdwayContract");
    action.setParams({
      opportunityId: component.get("v.recordId")
    });

    action.setCallback(this, function (response) {
      var state = response.getState();
      this.hideSpinnerHelper(component, event, helper);
      if (state == 'SUCCESS') {
        if (response.getReturnValue() != null
          && response.getReturnValue().includes(component.get("v.recordId"))) {
          this.handleSuccess(component, "Ordway contract created successfully");
        }
        else if (response.getReturnValue().includes("ERROR")) {
          this.handleError(component, "Failed to create the contract");
        }
      }
      else {
        this.handleError(component, action.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },

  getDynamicMappingValues: function (component, event, helper) {
    var opportunityRecord = component.get("v.opportunityRecordVal");
    var action = component.get("c.getDynamicMappingForQuote");
    action.setParams({
      opportunityId: opportunityRecord.Id
    });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state == 'SUCCESS') {
        if (response.getReturnValue()) {
          var thisResponse = [];
          thisResponse = response.getReturnValue();
          var urlToQuote = '/lightning/o/OrdwayLabs__Quote__c/new?defaultFieldValues=';
          urlToQuote += 'OrdwayLabs__Opportunity__c' + '=' + opportunityRecord.Id + ',OrdwayLabs__Status__c' + '=Draft,OrdwayLabs__SyncStatus__c=Not In Sync,'
          for (var key in thisResponse) {
            urlToQuote = urlToQuote + key + '=' + thisResponse[key] + ','

          }
          urlToQuote = urlToQuote.slice(0, -1);
          this.createQuote(component, event, helper, urlToQuote);
        }
      }
      else {
        this.handleError(component, action.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },

  createQuote: function (component, event, helper, urlToQuote) {
    var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": urlToQuote
    });
    urlEvent.fire();
  },

  updateOrdwayContractHelper: function (component, event, helper) {
    this.showSpinnerHelper(component, event, helper);
    var action = component.get("c.updateOrdwayContract");
    action.setParams({
      opportunityId: component.get("v.recordId")
    });

    action.setCallback(this, function (response) {
      var state = response.getState();
      this.hideSpinnerHelper(component, event, helper);
      if (state == 'SUCCESS') {
        if (response.getReturnValue() != null
          && !response.getReturnValue().includes("ERROR")) {
          component.set("v.ordwayContractId", response.getReturnValue());
          this.handleSuccess(component, "Ordway contract updated succesfully");
        }
        else if (response.getReturnValue().includes("ERROR")) {
          this.handleError(component, "Ordway contract update Failed");
        }
      }
      else {
        this.handleError(component, action.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },

  redirectToAddPlanPage: function (component, planPriceBookID) {
    var pageReferenceObj;
    if(component.get("v.showPlanPicker")){
      pageReferenceObj = {
        type: 'standard__component',
        attributes: {
          componentName: 'OrdwayLabs__PlanPicker',
        },
        state: {
          "c__ParentId": component.get("v.recordId"),
          "c__ParentName": component.get("v.currentOpportunity").Name,
          "c__PlanPriceBookId": planPriceBookID
        }
      };
    }
    else{
      pageReferenceObj = {
        type: 'standard__component',
        attributes: {
          componentName: 'OrdwayLabs__AddEditPlanComponent',
        },
        state: {
          "c__OpportunityID": component.get("v.recordId"),
          "c__OpportunityName": component.get("v.currentOpportunity").Name,
          "c__PlanPriceBookId": planPriceBookID
        }
      };
    }

    var workspaceAPI = component.find("ordwayButtonComponentWorkSpace");
        workspaceAPI.isConsoleNavigation().then(function(response) {
            if(response){
              workspaceAPI.getFocusedTabInfo().then(function(focusedTab) {
                
                pageReferenceObj.state["c__ParentObjectTab"] = focusedTab.tabId;
                workspaceAPI.openSubtab({
                    parentTabId: focusedTab.tabId,
                    pageReference: pageReferenceObj
                }).catch(function(error) {
                    
                });
            });
          }
          else{
            var navService = component.find("navService");
            navService.navigate(pageReferenceObj);
          }
        })
        .catch(function(error) {
            
        });
  },

  showSpinnerHelper: function (component, event, helper) {
    component.set("v.showSpinner", true);
  },

  hideSpinnerHelper: function (component, event, helper) {
    component.set("v.showSpinner", false);
  },

  handleSuccess: function (component, successMessage) {
    component.set("v.messageTitle", "Success!");
    component.set("v.messageType", "success");
    component.set("v.message", successMessage);
    component.set("v.showRecordMessage", true);
  },

  handleError: function (component, errorMessage) {
    component.set("v.messageTitle", "Error");
    component.set("v.messageType", "error");
    component.set("v.message", errorMessage);
    component.set("v.showRecordMessage", true);
  }
})