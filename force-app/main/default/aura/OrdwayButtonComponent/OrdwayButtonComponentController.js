({
  onInit: function (component, event, helper) {
    helper.checkPermissionAssignement(component, event, helper);
  },
  handleClick: function (component, event, helper) {
    var oppPriceBookID = component.get("v.pricebookId");
    if (oppPriceBookID != null
      && oppPriceBookID != undefined) {
      helper.redirectToAddPlanPage(component, oppPriceBookID);
    }
    else {
      var selectPriceBooktoastEvent = $A.get("e.force:showToast");
      selectPriceBooktoastEvent.setParams({
        "mode": 'sticky',
        "title": "Error",
        "type": "error",
        "message": $A.get("$Label.c.Add_Plan_Select_Price_Book_Warning_Message")
      });
      selectPriceBooktoastEvent.fire();
    }
  },

  onRecordUpdated : function (component, event, helper) {
    component.set("v.isQuoteCreateable", true);
    $A.get('e.force:refreshView').fire();
  },

  handleCreateOrdwayContract: function (component, event, helper) {
    if (component.get("v.contractLabel") == 'Update Ordway Contract') {
      helper.updateOrdwayContractHelper(component, event, helper);
    }
    else {
      helper.createOrdwayContractHelper(component, event, helper);
    }
  },

  handleCreateQuote : function (component, event, helper) {
    helper.getDynamicMappingValues(component, event, helper);
  },

  close: function (component, event) {
    var message = event.getParam("cancelled");
    component.set("v.isClicked", message);
  },

  navigateToOrdwayContract: function (component, event, helper) {
    var navEvt = $A.get("e.force:navigateToSObject");
    navEvt.setParams({
      "recordId": component.get("v.ordwayContractId"),
      "slideDevName": "detail"
    });
    navEvt.fire();
  },

  handleSaveSuccess: function (component, event, helper) {
    helper.handleSuccess(component, "Test handle success");
  },

  refreshRecord: function (component, event, helper) {
    component.set("v.showRecordMessage", false);
    component.set("v.messageTitle", "");
    component.set("v.messageType", "");
    component.set("v.message", "");
    $A.get('e.force:refreshView').fire();
  }
})