/**
 * @File Name          : OpportunityConfigurationController.js
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 01-12-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    11/21/2019   Ordway Labs     Initial Version
 **/
({
  doInit: function (component, event, helper) {
    helper.showSpinnerHelper(component, event, helper);
    helper.onInit(component, event, helper);
    helper.hideSpinnerHelper(component, event);
  },

  handleActivateChange: function (component, event, helper) {
    var changeValue = event.getParam("value");
    if(changeValue === 'Yes'){
      helper.assignValues(component, event, helper);
      var modalBody;
      $A.createComponent("c:ActivateQuoteConfirmationModal", {"opportunityInstance" : component.get("v.opportunityInstance")},
        function(content, status) {
          if (status === "SUCCESS") {
            modalBody = content;
            component.find('overlayLib').showCustomModal({
                header: "Activate Ordway Quotes?",
                body: modalBody, 
                showCloseButton: true,
                closeCallback: function() {
                  component.set("v.activateQuoteId","No");
                }
            });
          }
        }); 
    }
  },

  toggleExpandCollapse: function (component, event, helper) {
    var thisBooleanName = event.target.dataset.booleanname;
    var thisAttributeKey = 'v.'+ thisBooleanName;
    component.set(thisAttributeKey, !component.get(thisAttributeKey));
  },

  setAttributeValue : function (component, event, helper) {
    var eventParam = event.getParam("changeDefaultTab");
    if(eventParam ==='close'){
      component.set("v.activateQuoteId","No");
    }
    else{
      helper.onInit(component, event, helper);  
    }
  },

  saveApplicationConfiguration : function (component, event, helper) {
    helper.showSpinnerHelper(component, event, helper);
    helper.assignValues(component, event, helper);

    var action = component.get("c.saveOpportunitySetting");
    action.setParams({
      opportunitySettingInstance: component.get("v.opportunityInstance")
    });

    // set call back
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
          title: "Success!",
          message: "Saved Successfully",
          type: "success"
        });
        helper.hideSpinnerHelper(component, event);
        toastEvent.fire();
        $A.get('e.force:refreshView').fire();
      } else if (response.getState() === "ERROR") {
        helper.hideSpinnerHelper(component, event);
        var errors = action.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
              title: "Error",
              message: "",
              type: "error"
            });
            toastEvent.fire();
          }
        }
      }
    });
    $A.enqueueAction(action);
  }
});