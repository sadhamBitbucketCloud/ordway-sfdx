({
    onInit : function (component, event, helper) {
    var action = component.get("c.getOpportunitySettingWrappper");

        action.setCallback(this, function (response) {
          if (response.getState() == "SUCCESS") {
            var newstageTypeMap = [];
            var StoreResponse = response.getReturnValue();
            var stageMap = StoreResponse.stageWithDefaultRecordType;

            for (var key in stageMap) {
              if (key == "Closed Won") {
                component.set("v.selectedStage", stageMap[key]);
              }
              newstageTypeMap.push({
                key: key,
                value: stageMap[key]
              });
            }
            var stageMacontractMap = StoreResponse.ordwaySubscriptionStatus;
            var contractMap = [];
            for (var key in stageMacontractMap) {
              if (key == "Draft") {
                component.set(
                  "v.selectedSubscriptionStatus",
                  stageMacontractMap[key]
                );
              }
              contractMap.push({
                key: key,
                value: stageMap[key]
              });
            }

            var pricebookMap = StoreResponse.pricebookMap;

            var newpricebookMap = [];
            for (var key in pricebookMap) {
              newpricebookMap.push({
                key: key,
                value: pricebookMap[key]
              });
            }
            component.set("v.opportunityStage", newstageTypeMap);
            component.set("v.contractPicklistMap", contractMap);
            component.set("v.pricebookMap", newpricebookMap);
            component.set("v.isMultiCurrencyOrganization", StoreResponse.isMultiCurrencyOrganization);

            if (
              StoreResponse.opportunitySetting !== null &&
              StoreResponse.opportunitySetting.Name !== null
            ) {
              if (
                StoreResponse.opportunitySetting.OrdwayLabs__OpportunityStage__c !==
                undefined
              ) {
                component.set(
                  "v.selectedStage",
                  StoreResponse.opportunitySetting.OrdwayLabs__OpportunityStage__c
                );
              }

              if (
                StoreResponse.opportunitySetting
                  .OrdwayLabs__AutomaticallyCreateSubscription__c !== undefined
              ) {
                if (
                  StoreResponse.opportunitySetting
                    .OrdwayLabs__AutomaticallyCreateSubscription__c
                ) {
                   component.set("v.automaticallystageValue", "Yes");
                } else {
                  component.set("v.automaticallystageValue", "No");
                }
              }

              if (
                StoreResponse.opportunitySetting
                  .OrdwayLabs__AutomaticallySyncAccount__c !== undefined
              ) {
                if (
                  StoreResponse.opportunitySetting
                    .OrdwayLabs__AutomaticallySyncAccount__c
                ) {
                   component.set("v.automaticallySyncAccount", "Yes");
                } else {
                  component.set("v.automaticallySyncAccount", "No");
                }
              }

              if (
                StoreResponse.opportunitySetting
                  .OrdwayLabs__AutomaticallySyncContact__c !== undefined
              ) {
                if (
                  StoreResponse.opportunitySetting
                    .OrdwayLabs__AutomaticallySyncContact__c
                ) {
                   component.set("v.automaticallySyncContact", "Yes");
                } else {
                  component.set("v.automaticallySyncContact", "No");
                }
              }

              if (
                StoreResponse.opportunitySetting
                  .OrdwayLabs__LinkSubscriptionAutomatically__c !== undefined
              ) {
                if (
                  StoreResponse.opportunitySetting
                    .OrdwayLabs__LinkSubscriptionAutomatically__c
                ) {
                  component.set("v.automaticallylink", "Yes");
                } else {
                  component.set("v.automaticallylink", "No");
                }
              }
              if (
                StoreResponse.opportunitySetting
                  .OrdwayLabs__AutomaticallyActivateContract__c !== undefined
              ) {
                if (
                  StoreResponse.opportunitySetting
                    .OrdwayLabs__AutomaticallyActivateContract__c
                ) {
                  component.set("v.autoActivate", "Yes");
                } else {
                  component.set("v.autoActivate", "No");
                }
              }

              if (
                StoreResponse.opportunitySetting
                  .OrdwayLabs__AutoSyncFromOrdway__c !== undefined
              ) {
                if (
                  StoreResponse.opportunitySetting.OrdwayLabs__AutoSyncFromOrdway__c
                ) {
                  component.set("v.autoSync", "Yes");
                } else {
                  component.set("v.autoSync", "No");
                }
              }

              if (
                StoreResponse.opportunitySetting.OrdwayLabs__PriceBook2Id__c !==
                undefined
              ) {
                component.set(
                  "v.selectedPricebook",
                  StoreResponse.opportunitySetting.OrdwayLabs__PriceBook2Id__c
                );
              }

              if(StoreResponse.isMultiCurrencyOrganization){
                component.set("v.ordwayPrice", "No");
              }else if (
                StoreResponse.opportunitySetting.OrdwayLabs__UseOrdwayPrice__c !==
                undefined
              ) {
                if (
                  StoreResponse.opportunitySetting.OrdwayLabs__UseOrdwayPrice__c
                ) {
                  component.set("v.ordwayPrice", "Yes");
                } else {
                  component.set("v.ordwayPrice", "No");
                }
              }

              if (
                StoreResponse.opportunitySetting
                  .OrdwayLabs__UseOrdwayPricingCalculation__c !== undefined
              ) {
                if (
                  StoreResponse.opportunitySetting
                    .OrdwayLabs__UseOrdwayPricingCalculation__c
                ) {
                  component.set("v.ordwayPriceCalculation", "Yes");
                } else {
                  component.set("v.ordwayPriceCalculation", "No");
                }
              }

              if (StoreResponse.hasQuoteLicense !== undefined) {
                if (StoreResponse.hasQuoteLicense){
                  component.set("v.showActivateQuotes", true);
                }
              }

              if ( StoreResponse.opportunitySetting.OrdwayLabs__EnableQuotes__c !== undefined ) {
                if(StoreResponse.opportunitySetting.OrdwayLabs__EnableQuotes__c){
                  component.set("v.showQuoteSettings", true);
                  component.set("v.showActivateQuotes", false);

                  if (
                    StoreResponse.opportunitySetting
                      .OrdwayLabs__SyncQuoteAutomatically__c !== undefined
                  ) {
                    if (
                      StoreResponse.opportunitySetting
                        .OrdwayLabs__SyncQuoteAutomatically__c
                    ) {
                      component.set("v.quoteAutomaticallylink", "Yes");
                    } else {
                      component.set("v.quoteAutomaticallylink", "No");
                    }
                  }

                  if (
                    StoreResponse.opportunitySetting
                      .OrdwayLabs__AutoSyncQuotefromOrdway__c !== undefined
                  ) {
                    if (
                      StoreResponse.opportunitySetting.OrdwayLabs__AutoSyncQuotefromOrdway__c
                    ) {
                      component.set("v.quoteAutoSync", "Yes");
                    } else {
                      component.set("v.quoteAutoSync", "No");
                    }
                  }
                }
              }

            }
          }
        });
        $A.enqueueAction(action);
      },

      assignValues : function (component, event, helper) {

      var automaticallyCreateSubscription = component.get("v.automaticallystageValue") == "Yes" ? "True" : "False";
      var defaultOrdwaySubscriptionStatus = component.get("v.selectedSubscriptionStatus");
      var linkSubscriptionAutomatically   = component.get("v.automaticallylink") == "Yes" ? "True" : "False";
      var syncFromOrdwayAutomatically     = component.get("v.autoSync") == "Yes" ? "True" : "False";

      var syncAccountAutomatically     = component.get("v.automaticallySyncAccount") == "Yes" ? "True" : "False";
      var syncContactAutomatically     = component.get("v.automaticallySyncContact") == "Yes" ? "True" : "False";

      component.set("v.opportunityInstance.OrdwayLabs__AutomaticallySyncAccount__c", syncAccountAutomatically);
      component.set("v.opportunityInstance.OrdwayLabs__AutomaticallySyncContact__c", syncContactAutomatically);
      var activateContractAutomatically   = component.get("v.autoActivate") == "Yes" ? "True" : "False";

      component.set("v.opportunityInstance.OrdwayLabs__AutomaticallyCreateSubscription__c",automaticallyCreateSubscription);
      component.set("v.opportunityInstance.OrdwayLabs__LinkSubscriptionAutomatically__c",linkSubscriptionAutomatically);
      component.set("v.opportunityInstance.OrdwayLabs__AutoSyncFromOrdway__c",syncFromOrdwayAutomatically);
      component.set("v.opportunityInstance.OrdwayLabs__AutomaticallyActivateContract__c",activateContractAutomatically);


      var ordwayPrice               = component.get("v.ordwayPrice") == "Yes" ? "True" : "False";
      var ordwayPriceCalculationVal = component.get("v.ordwayPriceCalculation") == "Yes" ? "True": "False";
      component.set("v.opportunityInstance.OrdwayLabs__UseOrdwayPricingCalculation__c",ordwayPriceCalculationVal);
      component.set("v.opportunityInstance.OrdwayLabs__UseOrdwayPrice__c",ordwayPrice);

      if(component.get("v.showActivateQuotes")){
        var enableQuotes = component.get("v.activateQuoteId") == "Yes" ? "True" : "False";
        component.set("v.opportunityInstance.OrdwayLabs__EnableQuotes__c", enableQuotes);
      }

      if(component.get("v.showQuoteSettings")){
        var syncQuoteFromOrdwayAutomatically = component.get("v.quoteAutoSync") == "Yes" ? "True" : "False";
        var linkQuoteAutomatically = component.get("v.quoteAutomaticallylink") == "Yes" ? "True"  : "False";

        component.set("v.opportunityInstance.OrdwayLabs__SyncQuoteAutomatically__c", linkQuoteAutomatically);
        component.set("v.opportunityInstance.OrdwayLabs__AutoSyncQuotefromOrdway__c",syncQuoteFromOrdwayAutomatically);
        component.set("v.opportunityInstance.OrdwayLabs__EnableQuotes__c", true);
      }

      var opportunityStage = component.get("v.selectedStage");
      var selectedPricebooks = component.get("v.selectedPricebook");

      component.set("v.opportunityInstance.OrdwayLabs__OpportunityStage__c", opportunityStage);
      component.set( "v.opportunityInstance.OrdwayLabs__PriceBook2Id__c", selectedPricebooks);
    },

    showSpinnerHelper: function (component, event, helper) {
      component.set("v.showSpinner", true);
    },
  
    hideSpinnerHelper: function (component, event, helper) {
      component.set("v.showSpinner", false);
    },
})