({
  doInit: function (component, event, helper) {
    component.set("v.showSpinner", true);
    window.setTimeout(
      $A.getCallback(function () {
        helper.hideSpinner(component, event)
      }), 1500
    );
    helper.getApplicationCustomSetting(component, event, helper);
    helper.getEnvironmentOptions(component, helper);
  }, 

  saveLogin: function (component, event, helper) {
    helper.loginHelper(component, helper, false);
  },
  disconnectOrdway: function (component, event, helper) {
    helper.disconnectHelper(component, helper);
  },
  testLogin: function (component, event, helper) {
    helper.loginHelper(component, helper, true);
  },

  onTokenValueChange: function (component, event, helper) {
    
    var thisToken = event.getSource().get("v.value");

    if (thisToken && component.get("v.lastValidationDate") !== undefined && component.get("v.lastValidationDate") !== '') {
      component.set("v.valueDoesExists", true);
    }
    else {
      component.set("v.valueChange", true);
      component.set("v.valueDoesExists", false);
    }
  },

  onChangeValue: function (component, event, helper) {
    component.set("v.valueChange", true);
    component.set("v.valueDoesExists", false);
  },
  OrdwaySalesURL: function (component, event, helper) {
    window.open(component.get("v.OrdwaySalesURL"));
  },
  onchangeTab: function (component, event, helper) {
    
    if(component.get("v.selectedTabId") == 'OrdwayEntityConfigurationTab'){
      component.find('OrdwayEntityConfigurationLWC').handleTabChangeEvent();
    }
    else{
      component.set("v.showSpinner", true);
      window.setTimeout(
        $A.getCallback(function () {
          helper.hideSpinner(component, event)
        }), 1000
      );
    }
  },
})