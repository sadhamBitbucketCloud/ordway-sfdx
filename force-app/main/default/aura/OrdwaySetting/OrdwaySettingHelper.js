({
  loginHelper: function (component, event, isTestConnection) {

    var emailid = component.find("emailid");
    var company = component.find("company");
    var token = component.find("token");
    var environment = component.find("environment");

    //Validation
    if ($A.util.isEmpty(component.find("emailid").get("v.value")) || $A.util.isUndefined(component.find("emailid").get("v.value"))) {
      emailid.setCustomValidity("Please enter email");
      emailid.reportValidity();
      return;
    } else {
      emailid.setCustomValidity("");
      emailid.reportValidity();
    }
    var newEmail = component.find("emailid").get("v.value");
    if (newEmail.includes("@")) {
      emailid.setCustomValidity("");
      emailid.reportValidity();

    } else {
      emailid.setCustomValidity("Enter a valid email");
      emailid.reportValidity();
      return;
    }

    if ($A.util.isEmpty(company.get("v.value")) || $A.util.isUndefined(company.get("v.value"))) {
      company.setCustomValidity("Please enter company");
      company.reportValidity();
      return;
    } else {
      company.setCustomValidity("");
      company.reportValidity();
    }


    if ($A.util.isEmpty(token.get("v.value")) || $A.util.isUndefined(token.get("v.value"))) {
      token.setCustomValidity("Please enter token");
      token.reportValidity();
      return;
    } else {
      token.setCustomValidity("");
      token.reportValidity();
    }
    this.showSpinner(component, event);
    var action = component.get("c.validateToken");

    action.setParams({
      "email": component.find("emailid").get("v.value"),
      "company": company.get("v.value"),
      "token": token.get("v.value"),
      "environment": component.get("v.selectedProduction"),
      'isTestConnection': isTestConnection

    });
    // set call back 
    action.setCallback(this, function (response) {
      var state = response.getState();

      if (state === "SUCCESS" && response.getReturnValue() === 'You are connected successfully') {

        if (component.isValid() && state === "SUCCESS") {
          var toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({
            "title": "Success!",
            "message": response.getReturnValue(),
            "type": "success"

          });
          toastEvent.fire();
          this.hideSpinner(component, event);
          if (!isTestConnection) {
            $A.get('e.force:refreshView').fire();
            location.reload(true);
          }
        }
      } else {
        var toastEvent = $A.get("e.force:showToast");
        this.hideSpinner(component, event);
        toastEvent.setParams({
          "title": "Error",
          "message": "Authorization failed",
          "type": "error"

        });
        toastEvent.fire();

      }
    });

    //adds the server-side action to the queue        
    $A.enqueueAction(action);
  },

  disconnectHelper: function (component, event) {

    var action = component.get("c.disconnect");
    
    action.setCallback(this, function (response) {
      this.showSpinner(component, event);
      var state = response.getState();

      if (state === "SUCCESS") {

        if (component.isValid() && state === "SUCCESS") {
          component.find("emailid").set("v.value", undefined);
          component.find("company").set("v.value", undefined);
          component.find("token").set("v.value", undefined);

          var toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({
            "title": "Success!",
            "message": 'Disconnected Successfully',
            "type": "success"

          });
          toastEvent.fire();
          this.hideSpinner(component, event);
          $A.get('e.force:refreshView').fire();
          location.reload(true);
        }
      } else {
        var toastEvent = $A.get("e.force:showToast");
        this.hideSpinner(component, event);
        toastEvent.setParams({
          "title": "Error",
          "message": "Failed to disconnect",
          "type": "error"

        });
        toastEvent.fire();
      }
    });     
    $A.enqueueAction(action);
  },

  getApplicationCustomSetting: function (component, event, helper) {

    var action = component.get("c.getApplicationSetting");
    action.setCallback(this, function (response) {
      if (response.getState() == "SUCCESS") {
        component.set("v.isMultiEntityPermissionAssigned", response.getReturnValue().isMultiEntityPermissionAssigned);
        var StoreResponse = response.getReturnValue().applicationSetting;

        if (StoreResponse !== null && StoreResponse.Name !== null) {
          if (StoreResponse.OrdwayLabs__Email__c !== undefined) {
            component.find("emailid").set("v.value", StoreResponse.OrdwayLabs__Email__c);
          } else {
            component.set("v.valueChange", true);
          }
          if (StoreResponse.OrdwayLabs__Company__c !== undefined) {
            component.find("company").set("v.value", StoreResponse.OrdwayLabs__Company__c);
          } else {
            component.set("v.valueChange", true);
          }
          if (StoreResponse.OrdwayLabs__Token__c !== undefined) {
            component.find("token").set("v.value", StoreResponse.OrdwayLabs__Token__c);
            component.set("v.valueDoesExists", true);
          } else {
            component.set("v.valueChange", true);
          }
          if (StoreResponse.OrdwayLabs__Environment__c !== undefined) {
            component.set('v.selectedProduction', StoreResponse.OrdwayLabs__Environment__c);
          } else {
            component.set("v.valueChange", true);
          }
          if (StoreResponse.OrdwayLabs__OrdwayContactURL__c !== undefined) {
            component.set('v.OrdwaySalesURL', StoreResponse.OrdwayLabs__OrdwayContactURL__c);
          }
          if (StoreResponse.OrdwayLabs__LastTokenValidationDate__c !== undefined) {
            component.set("v.lastValidationDate", StoreResponse.OrdwayLabs__LastTokenValidationDate__c.replace('T', ' ').slice(0, -5));
          } else {
            component.set("v.valueChange", true);
          }
        }
      }
    });
    $A.enqueueAction(action);
  },

  getEnvironmentOptions: function (component, helper) {
    var action = component.get("c.getOrdwayNamedCredential");
    action.setCallback(this, function (response) {
      if (response.getState() == "SUCCESS") {
        var environmentOptionList = response.getReturnValue();
        if (environmentOptionList !== null
          && environmentOptionList.length > 0) {
          component.set("v.environmentOptionList", environmentOptionList);
        }
      }
    });
    $A.enqueueAction(action);
  },
  showSpinner: function (component, event) {
    component.set("v.showSpinner", true);
  },

  hideSpinner: function (component, event) {
    component.set("v.showSpinner", false);
  },
})