/**
 * @File Name          : GetOrdwayPlan_Batch_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 01-03-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    4/25/2020        Initial Version
 **/
@isTest
private class GetOrdwayPlan_Batch_Test {
  @isTest
  private static void getOrdwayPlanTest() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new MockAddPlan_Test(null));
    Database.executebatch(new GetOrdwayPlan_Batch(1, null));
    Test.stopTest();
    List<OrdwayLabs__Plan__c> plansInserted = [
      SELECT Id, Name
      FROM OrdwayLabs__Plan__c
    ];
    System.assertEquals(
      2,
      plansInserted.size(),
      'Mock response returns two Plans Only'
    );
  }

  @isTest
  private static void getOrdwayPlanTest_WithPageNumber() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new MockAddPlan_Test(null));
    Database.executebatch(new GetOrdwayPlan_Batch(1));
    Test.stopTest();
    List<OrdwayLabs__Plan__c> plansInserted = [
      SELECT Id, Name
      FROM OrdwayLabs__Plan__c
    ];
    System.assertEquals(
      2,
      plansInserted.size(),
      'Mock response returns two Plans Only'
    );
  }
}