@isTest
private class AutoBulkSyncScheduler_Test {
    @isTest
    private static void AutoBulkSyncScheduler() {
        ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
        insert thisApplicationsetting;
        Test.startTest();
        SchedulableContext testsc = null;
        AutoBulkSync_Scheduler bulkSyncScheduler = new AutoBulkSync_Scheduler();
        bulkSyncScheduler.execute(testsc);
        System.assertEquals(null, testsc, 'Should return null');
        Test.stopTest();
    }
}