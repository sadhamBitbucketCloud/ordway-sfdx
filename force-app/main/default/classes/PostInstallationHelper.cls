public with sharing class PostInstallationHelper {
   
  public static void updateQuoteMetadataMapping() {
    Metadata.CustomMetadata dynamicMetadata = new Metadata.CustomMetadata();

    //Custom Metadata API Name + Namespace Prefix + Developer Name
    //No need to provide developer name, as the same will be derived from fullname
    dynamicMetadata.fullName = 'OrdwayLabs__DynamicObjectMapping.OrdwayLabs__Quote';
    dynamicMetadata.Label = 'Quote';

    Metadata.CustomMetadataValue targetField = new Metadata.CustomMetadataValue();

    targetField = new Metadata.CustomMetadataValue();
    targetField.field = 'OrdwayLabs__ChildExternalID__c'; //Metadata Field
    targetField.value = 'OrdwayLabs__ExternalID__c'; //Field Value
    dynamicMetadata.values.add(targetField);

    Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
    mdContainer.addMetadata(dynamicMetadata);
    DeployOrdwayObjectFieldMappingMetaData callback = new DeployOrdwayObjectFieldMappingMetaData();
    Metadata.Operations.enqueueDeployment(mdContainer, callback);
  }

}