/**
 * @File Name          : SyncOrdwayContract_Batch_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 01-03-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    2/2/2020   Ordway Labs     Initial Version
 **/
@isTest
private class SyncOrdwayContract_Batch_Test {
  @isTest
  static void test_SyncOrdwayContractBatch() {
    Account ordwayAccount = TestObjectFactory.createAccount();
    insert ordwayAccount;

    Contact ordwayContact = TestObjectFactory.createContact();
    insert ordwayContact;

    OpportunitySettingController.saveOpportunitySetting(
      TestObjectFactory.createContractPlanPickerSetting()
    );

    OrdwayContract__c ordwayContract = TestObjectFactory.createOrdwayContract();
    ordwayContract.BillingContact__c = ordwayContact.Id;
    ordwayContract.Account__c = ordwayAccount.Id;
    ordwayContract.OrdwaySubscriptionID__c = 'Sub-123';

    insert ordwayContract;

    System.assertEquals(
      'Not Synced',
      ordwayContract.SyncStatus__c,
      'Should return Not Synced'
    );

    ContractLineItem__c ordwayContractLineItem = new ContractLineItem__c();
    ordwayContractLineItem.OrdwayLabs__OrdwayContract__c = ordwayContract.Id;
    ordwayContractLineItem = TestObjectFactory.createContractLineItem(
      ordwayContractLineItem
    );
    insert ordwayContractLineItem;

    System.assertEquals(ordwayAccount.Id, ordwayContract.Account__c);

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Integer RESPONSE_CODE = 200;
    String RESPONSE_BODY =
      '{ ' +
      ' "customer" : { ' +
      '   "Website" : null,' +
      '   "Name" : "Demo"' +
      '  },' +
      ' "plans" : [ {' +
      '   "OrdwayLabs__ChargeType__c" : "One time",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "1000.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "1000.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Recurring",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "500.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "500.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Usage based",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 5.0,' +
      ' "OrdwayLabs__Quantity__c" : 100.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "950.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "10.00"' +
      '} ],' +
      '"OrdwayLabs__OrdwayMRR__c" : null,' +
      '"OrdwayLabs__OrdwayContractStatus__c" : "Active",' +
      '"OrdwayLabs__AutoRenew__c" : true,' +
      '"OrdwayLabs__RenewalTerm__c" : "12",' +
      '"OrdwayLabs__ContractTerm__c" : "12",' +
      '"OrdwayLabs__ContractEffectiveDate__c" : "2020-03-27",' +
      '"OrdwayLabs__BillingStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__ServiceStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__TotalContractValue__c" : 111520.0,' +
      '"OrdwayLabs__CurrentTermEndDate__c" : "2021-03-23",' +
      '"OrdwayLabs__CurrentTermStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__CustomerId__c" : "C-00001",' +
      '"Id" : "testContractId"' +
      '}';
    Test.startTest();
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(
        RESPONSE_CODE,
        RESPONSE_BODY.replace(
          'testContractId',
          String.valueOf(ordwayContract.Id)
        )
      )
    );
    System.assertEquals(System.today(), ordwayContract.BillingStartDate__c);
    SyncOrdwayContract_Batch testsyncBatch = new SyncOrdwayContract_Batch();
    Database.executeBatch(testsyncBatch);
    Test.stopTest();
  }

  @isTest
  static void SyncOrdwayContractBatch() {
    Account ordwayAccount = TestObjectFactory.createAccount();
    insert ordwayAccount;

    Contact ordwayContact = TestObjectFactory.createContact();
    insert ordwayContact;

    OpportunitySettingController.saveOpportunitySetting(
      TestObjectFactory.createContractPlanPickerSetting()
    );

    OrdwayContract__c ordwayContract = TestObjectFactory.createOrdwayContract();
    ordwayContract.BillingContact__c = ordwayContact.Id;
    ordwayContract.Account__c = ordwayAccount.Id;
    ordwayContract.OrdwaySubscriptionID__c = 'Sub-123';

    insert ordwayContract;

    System.assertEquals(
      'Not Synced',
      ordwayContract.SyncStatus__c,
      'Should return Not Synced'
    );

    ContractLineItem__c ordwayContractLineItem = new ContractLineItem__c();
    ordwayContractLineItem.OrdwayLabs__OrdwayContract__c = ordwayContract.Id;
    ordwayContractLineItem = TestObjectFactory.createContractLineItem(
      ordwayContractLineItem
    );
    insert ordwayContractLineItem;

    System.assertEquals(ordwayAccount.Id, ordwayContract.Account__c);

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Integer RESPONSE_CODE = 200;
    String RESPONSE_BODY =
      '{ ' +
      ' "customer" : { ' +
      '   "Website" : null,' +
      '   "Name" : "Demo"' +
      '  },' +
      ' "plans" : [ {' +
      '   "OrdwayLabs__ChargeType__c" : "One time",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "1000.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "1000.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Recurring",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "500.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "500.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Usage based",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 5.0,' +
      ' "OrdwayLabs__Quantity__c" : 100.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "950.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "10.00"' +
      '} ],' +
      '"OrdwayLabs__OrdwayMRR__c" : null,' +
      '"OrdwayLabs__OrdwayContractStatus__c" : "Active",' +
      '"OrdwayLabs__AutoRenew__c" : true,' +
      '"OrdwayLabs__RenewalTerm__c" : "12",' +
      '"OrdwayLabs__ContractTerm__c" : "12",' +
      '"OrdwayLabs__ContractEffectiveDate__c" : "2020-03-27",' +
      '"OrdwayLabs__BillingStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__ServiceStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__TotalContractValue__c" : 111520.0,' +
      '"OrdwayLabs__CurrentTermEndDate__c" : "2021-03-23",' +
      '"OrdwayLabs__CurrentTermStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__CustomerId__c" : "C-00001",' +
      '"Id" : "testContractId"' +
      '}';
    Test.startTest();
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(
        RESPONSE_CODE,
        RESPONSE_BODY.replace(
          'testContractId',
          String.valueOf(ordwayContract.Id)
        )
      )
    );
    System.assertEquals(System.today(), ordwayContract.BillingStartDate__c);
    Set<Id> contractIds = new Set<Id>();
    contractIds.add(ordwayContract.Id);
    SyncOrdwayContract_Batch testsyncBatch = new SyncOrdwayContract_Batch(contractIds);
    Database.executeBatch(testsyncBatch);
    Test.stopTest();
  }

  @isTest
  static void test_SyncOrdwayContractBatchCustomQuery() {
    Account ordwayAccount = TestObjectFactory.createAccount();
    insert ordwayAccount;

    Contact ordwayContact = TestObjectFactory.createContact();
    insert ordwayContact;

    OpportunitySettingController.saveOpportunitySetting(
      TestObjectFactory.createContractPlanPickerSetting()
    );

    OrdwayContract__c ordwayContract = TestObjectFactory.createOrdwayContract();
    ordwayContract.BillingContact__c = ordwayContact.Id;
    ordwayContract.Account__c = ordwayAccount.Id;
    ordwayContract.OrdwaySubscriptionID__c = 'Sub-123';

    insert ordwayContract;

    System.assertEquals(
      'Not Synced',
      ordwayContract.SyncStatus__c,
      'Should return Not Synced'
    );

    ContractLineItem__c ordwayContractLineItem = new ContractLineItem__c();
    ordwayContractLineItem.OrdwayLabs__OrdwayContract__c = ordwayContract.Id;
    ordwayContractLineItem = TestObjectFactory.createContractLineItem(
      ordwayContractLineItem
    );
    insert ordwayContractLineItem;

    System.assertEquals(ordwayAccount.Id, ordwayContract.Account__c);

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Integer RESPONSE_CODE = 200;
    String RESPONSE_BODY =
      '{ ' +
      ' "customer" : { ' +
      '   "Website" : null,' +
      '   "Name" : "Demo"' +
      '  },' +
      ' "plans" : [ {' +
      '   "OrdwayLabs__ChargeType__c" : "One time",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "1000.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "1000.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Recurring",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "500.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "500.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Usage based",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 5.0,' +
      ' "OrdwayLabs__Quantity__c" : 100.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "950.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "10.00"' +
      '} ],' +
      '"OrdwayLabs__OrdwayMRR__c" : null,' +
      '"OrdwayLabs__OrdwayContractStatus__c" : "Active",' +
      '"OrdwayLabs__AutoRenew__c" : true,' +
      '"OrdwayLabs__RenewalTerm__c" : "12",' +
      '"OrdwayLabs__ContractTerm__c" : "12",' +
      '"OrdwayLabs__ContractEffectiveDate__c" : "2020-03-27",' +
      '"OrdwayLabs__BillingStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__ServiceStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__TotalContractValue__c" : 111520.0,' +
      '"OrdwayLabs__CurrentTermEndDate__c" : "2021-03-23",' +
      '"OrdwayLabs__CurrentTermStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__CustomerId__c" : "C-00001",' +
      '"Id" : "testContractId"' +
      '}';
    Test.startTest();
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(
        RESPONSE_CODE,
        RESPONSE_BODY.replace(
          'testContractId',
          String.valueOf(ordwayContract.Id)
        )
      )
    );

    String query = 'SELECT OrdwayLabs__Account__c, Id, OrdwayLabs__VersionType__c, OrdwayLabs__OrdwayContractStatus__c, OrdwayLabs__OrdwaySubscriptionID__c FROM OrdwayLabs__OrdwayContract__c WHERE OrdwayLabs__OrdwayLastSyncDate__c = null';
    SyncOrdwayContract_Batch testsyncBatch = new SyncOrdwayContract_Batch(
      query
    );
    System.assertEquals(
      'Not Synced',
      ordwayContract.SyncStatus__c,
      'Should return Not Synced'
    );
    Database.executeBatch(testsyncBatch);
    Test.stopTest();
  }
}