/**
 * @File Name          : OrdwayContractFieldComparisonController.cls
 * @Description        : Ordway Contract Field Comparison Controller
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-10-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    3/26/2020        Initial Version
 **/
public with sharing class OrdwayContractFieldComparisonController {
  /**
  * @description Method to retrieve Object Data wrapper used in field comparison component
                 which contains which is used for comparing record data in org with the record data in Ordway.
  * @param parentRecordId Sobject Id
  * @return Map<String, List<FieldComparisonHelper.objectDataWrapper>> Map of Object Field Comparison Wrapper
  **/
  @AuraEnabled
  public static FieldComparisonWrapper getOrdwayContractDataWrapper(
    String parentRecordId
  ) {
    FieldComparisonWrapper thisComparisonWrapper = new FieldComparisonWrapper();
    thisComparisonWrapper.isNewVersionEnabled    = LoadLeanSettingController.isPlanPicker2Enabled(); 
    List<sObject> sObjectList = new List<sObject>();
    if (((Id) parentRecordId).getSObjectType() == Schema.Quote__c.SObjectType) {
      sObjectList.addAll(
        OrdwayQuoteService.getOrdwayQuoteMap(new Set<Id>{ parentRecordId })
          .values()
      );
      Map<Id, QuoteLineItem__c> quoteLineItemMap = OrdwayQuoteLineItemService.getQuoteLineItemMapByQuoteIds(new Set<Id>{ parentRecordId });

      sObjectList.addAll(quoteLineItemMap.values());
      thisComparisonWrapper.objectComparisonDataWrapper =FieldComparisonHelper.getRecordWrapper(
        sObjectList, 
        OrdwayQuoteController.getOrdwayQuote(parentRecordId, sObjectList, thisComparisonWrapper.ordwayLineItemToTierStringMap) );
        thisComparisonWrapper.salesforceLineItemToTierMap = OrdwayQuoteLineItemService.getOrdwayQuoteLineItemTiers(quoteLineItemMap.keySet());

      return thisComparisonWrapper;
    } else {
      sObjectList.addAll(
        OrdwayContractService.getOrdwayContract(new Set<Id>{ parentRecordId })
          .values()
      );
      Map<Id, ContractLineItem__c> contractLineItemMap = ContractLineItemService.getOrdwayContractLineItemMapById(new Set<Id>{ parentRecordId });
      sObjectList.addAll(contractLineItemMap.values());

      thisComparisonWrapper.objectComparisonDataWrapper =  FieldComparisonHelper.getRecordWrapper(
        sObjectList,
        OrdwayContractController.getOrdwayContract(parentRecordId, sObjectList, thisComparisonWrapper.ordwayLineItemToTierStringMap)
      );
      thisComparisonWrapper.salesforceLineItemToTierMap = ContractLineItemService.getOrdwayContractLineItemTiers(contractLineItemMap.keySet());
      return thisComparisonWrapper;
    }
  }

  /**
   * @description Method to sync Quote or Contract to Ordway
   * @param recordId Sobject record id
   * @return String Synced Object Id
   **/
  @AuraEnabled
  public static String syncToOrdway(String recordId) {
    if (((Id) recordId).getSObjectType() == Schema.Quote__c.sObjectType) {
      return OrdwayQuoteController.syncToOrdway((Id) recordId);
    }
    return OrdwayContractController.syncToOrdway((Id) recordId);
  }

  /**
   * @description Method to update Quote or Contract from Ordway
   * @param recordId Sobject record id
   * @return String updated Object Id
   **/
  @AuraEnabled
  public static String updateFromOrdway(String recordId) {
    if (((Id) recordId).getSObjectType() == Schema.Quote__c.sObjectType) {
      return OrdwayQuoteController.updateOrdwayQuote((Id) recordId);
    }
    return OrdwayContractController.updateOrdwayContract((Id) recordId);
  }
  
  public class FieldComparisonWrapper {
    @AuraEnabled
    public Map<String, List<FieldComparisonHelper.objectDataWrapper>> objectComparisonDataWrapper;
    @AuraEnabled
    public Boolean isNewVersionEnabled;
    @AuraEnabled
    public Map<String, String> ordwayLineItemToTierStringMap;
    @AuraEnabled
    public Map<Id, List<SObject>> salesforceLineItemToTierMap;
    public FieldComparisonWrapper(){
      this.objectComparisonDataWrapper = new Map<String, List<FieldComparisonHelper.objectDataWrapper>>();
      this.ordwayLineItemToTierStringMap =  new Map<String, String>();
      this.salesforceLineItemToTierMap =  new Map<Id, List<SObject>>();
    }
  }

}