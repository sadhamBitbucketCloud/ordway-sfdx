public  class StaticFieldMappingHelper {

	public static Map<String, String> STATIC_FIELD_MAPPINGS = new Map<String, String>{
  'OpportunityLineItem_to_QuoteLineItem' => '['+
                                                '{'+
                                                  '"fieldAPIName":"OrdwayLabs__PlanBundleID__c",'+
                                                  '"sourceFieldAPIName":"OrdwayLabs__PlanBundleID__c"'+
                                                '},'+
                                                '{'+
                                                  '"fieldAPIName":"OrdwayLabs__CalculatedTierPrice__c",'+
                                                  '"sourceFieldAPIName":"OrdwayLabs__CalculatedTierPrice__c"'+
                                                '}'+
                                              ']',
	'QuoteLineItem_to_OpportunityLineItem' => '['+
																								'{'+
																									'"fieldAPIName":"OrdwayLabs__PlanBundleID__c",'+
																									'"sourceFieldAPIName":"OrdwayLabs__PlanBundleID__c"'+
																								'},'+
																								'{'+
																									'"fieldAPIName":"OrdwayLabs__CalculatedTierPrice__c",'+
																									'"sourceFieldAPIName":"OrdwayLabs__CalculatedTierPrice__c"'+
																								'}'+
																							']',
  'ContractLineItem_to_OpportunityLineItem' => '['+
                                              '{'+
                                                '"fieldAPIName":"OrdwayLabs__CalculatedTierPrice__c",'+
                                                '"sourceFieldAPIName":"OrdwayLabs__CalculatedTierPrice__c"'+
                                              '}'+
                                            ']',
  'OpportunityLineItem_to_ContractLineItem' => '['+
                                              '{'+
                                                '"fieldAPIName":"OrdwayLabs__CalculatedTierPrice__c",'+
                                                '"sourceFieldAPIName":"OrdwayLabs__CalculatedTierPrice__c"'+
                                              '}'+
                                            ']'
	};
}