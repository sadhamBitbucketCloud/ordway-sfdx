/**
 * @File Name          : SObjectFieldMappingHelper.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-05-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    9/11/2019   Ordway Labs     Initial Version
 **/
public class SObjectFieldMappingHelper {
  final static String DEPLOYMENT_STATUS_INDEPLOYMENT = 'InDeployment';

  final static Set<String> BAD_OBJECTS = new Set<String>{
    'Campaign',
    'CampaignMember',
    'Account',
    'Contact',
    'Lead',
    'OpportunityContactRole',
    'Partner',
    'Attachment',
    'Product2',
    'Asset',
    'Case',
    'CaseComment',
    'Solution',
    'ContentVersion',
    'ContentDocument',
    'ContentDocumentLink',
    'ContentDistribution',
    'Note',
    'FeedItem',
    'FeedComment',
    'CollaborationGroup',
    'CollaborationGroupMember',
    'ChatterActivity',
    'Task',
    'Event',
    'Topic',
    'TopicAssignment',
    'User',
    'EmailMessage',
    'Individual',
    'Pricebook2',
    'Idea',
    'IdeaComment',
    'Macro',
    'CollaborationGroupRecord',
    'StreamingChannel',
    'DandBCompany',
    'QuickText',
    'UserProvisioningRequest',
    'UserProvisioningLog',
    'UserProvMockTarget',
    'UserProvAccount',
    'UserProvAccountStaging',
    'DuplicateRecordSet',
    'DuplicateRecordItem',
    'AssetRelationship',
    'MessagingEndUser',
    'MessagingSession',
    'RecordAction',
    'OrgDeleteRequest',
    'ContactRequest',
    'ConsumptionSchedule',
    'ConsumptionRate',
    'ProductConsumptionSchedule',
    'AppAnalyticsQueryRequest',
    'ContactPointTypeConsent',
    'DataUseLegalBasis',
    'DataUsePurpose',
    'Image',
    'Recommendation',
    'AuthorizationForm',
    'AuthorizationFormConsent',
    'AuthorizationFormDataUse',
    'AuthorizationFormText',
    'MacroUsage',
    'PromptAction',
    'QuickTextUsage',
    'OrgMetric',
    'OrgMetricScanResult',
    'OrgMetricScanSummary',
    'ChatterMessage',
    'WorkCoaching',
    'WorkFeedback',
    'WorkFeedbackQuestion',
    'WorkFeedbackQuestionSet',
    'WorkFeedbackRequest',
    'WorkFeedbackTemplate',
    'Goal',
    'GoalLink',
    'MessagingDeliveryError',
    'Metric',
    'MetricDataLink',
    'WorkPerformanceCycle',
    'ContentDocumentListViewMapping',
    'ErrorLog__c',
    'ApplicationLog__c'
  };

  final static Set<String> BAD_FIELDS = new Set<String>{
    OrdwayHelper.SOBJECT_FIELD_ID,
    'OwnerId',
    'CreatedById',
    'CreatedDate',
    'IsDeleted',
    'LastActivityDate',
    'LastModifiedById',
    'LastModifiedDate',
    'LastReferencedDate',
    'LastViewedDate',
    'SystemModstamp',
    'UserRecordAccessId',
    'OrdwayLabs_PricingType__c',
    'PricingType__c',
    'RecordVisibility',
    'TierJSON__c'
  };

  public static Map<String, String> dataTypeToKeyMap = new Map<String, String>{
    'Rich Text Area' => 'Rich Text Area',
    'Long Text Area' => 'Long Text Area',
    'Text Area' => 'Text',
    'Text' => 'Text',
    'Formula (Text)' => 'Text',
    'Email' => 'Email',
    'Number' => 'Number',
    'Formula (Number)' => 'Number',
    'Formula (Currency)' => 'Number',
    'Currency' => 'Number',
    'Percent' => 'Percent',
    'Formula (Percent)' => 'Percent',
    'Phone' => 'Phone',
    'Picklist' => 'Picklist, Text',
    'Date' => 'Date',
    'Formula (Date)' => 'Date',
    'Date/Time' => 'Date/Time',
    'Formula (Date/Time)' => 'Date/Time',
    'Time' => 'Time',
    'Formula (Time)' => 'Time',
    'Checkbox' => 'Checkbox',
    'Formula (Checkbox)' => 'Checkbox',
    'URL' => 'URL',
    'Auto Number' => 'Text',
    'Geolocation' => 'Geolocation'
  };

  private static Map<String, String> targetFieldDataTypeKey = new Map<String, String>{
    'Rich Text Area' => 'Rich Text Area',
    'Long Text Area' => 'Long Text Area',
    'Text Area' => 'Text',
    'Text' => 'Text',
    'Formula (Text)' => 'Text',
    'Email' => 'Email',
    'Number' => 'Number',
    'Formula (Number)' => 'Number',
    'Formula (Currency)' => 'Number',
    'Currency' => 'Number',
    'Percent' => 'Percent',
    'Formula (Percent)' => 'Percent',
    'Phone' => 'Phone',
    'Picklist' => 'Picklist',
    'Date' => 'Date',
    'Formula (Date)' => 'Date',
    'Date/Time' => 'Date/Time',
    'Formula (Date/Time)' => 'Date/Time',
    'Time' => 'Time',
    'Formula (Time)' => 'Time',
    'Checkbox' => 'Checkbox',
    'Formula (Checkbox)' => 'Checkbox',
    'URL' => 'URL',
    'Auto Number' => 'Text',
    'Geolocation' => 'Geolocation'
  };

  /**
   * @description Method to get Entity Definition(Object Definition)
   * @return List<EntityDefinition>
   **/
  public static List<EntityDefinition> getObjectDefinition() {
    return [
      SELECT QualifiedAPIName, MasterLabel, DurableId
      FROM EntityDefinition
      WHERE
        IsTriggerable = TRUE
        AND IsQueryable = TRUE
        AND IsCustomSetting = FALSE
        AND IsApexTriggerable = TRUE
        AND QualifiedAPIName NOT IN :BAD_OBJECTS
      ORDER BY MasterLabel
    ];
  }

  /**
   * @description  Method to Field Definition for an object
   * @param  objectAPIName: Object API Name
   * @return  List of FieldDefinition
   */
  public static List<FieldDefinition> getFieldDefinition(String objectAPIName) {
    return [
      SELECT
        QualifiedApiName,
        Length,
        MasterLabel,
        DurableId,
        DataType,
        isCalculated,
        ExtraTypeInfo
      FROM FieldDefinition
      WHERE
        EntityDefinition.DurableId = :objectAPIName
        AND QualifiedApiName NOT IN :BAD_FIELDS
      ORDER BY MasterLabel
    ];
  }

  /**
   * @description  Method to Datatype Unique Key
   * @param  dataType Field Data Type API Name
   * @return  Datatype Unique Key
   */
  public static String getTargetFieldDataTypeKey(String dataType) {
    if (dataType.contains('Lookup')) {
      return dataType;
    } else if (dataType.contains('Roll-Up Summary (SUM')) {
      return 'Number';
    } else if (dataType.contains('(') && !dataType.contains('Formula')) {
      return targetFieldDataTypeKey.get(dataType.substringBefore('('));
    } else if (targetFieldDataTypeKey.containsKey(dataType)) {
      return targetFieldDataTypeKey.get(dataType);
    } else {
      return dataType;
    }
  }

  /**
   * @description  Method to Create Custom MetadataValue
   * @param  fieldName Metadata Field API Name
   * @param  fieldName Metadata Field Value
   * @return  Custom MetadataValue
   */
  public static Metadata.CustomMetadataValue createMetaDataValue(
    String fieldName,
    String fieldValue
  ) {
    Metadata.CustomMetadataValue targetField = new Metadata.CustomMetadataValue();
    targetField.field = fieldName;
    targetField.value = fieldValue;
    return targetField;
  }

  /**
   * @description Create New Sobject Record based on Field Mapping Metadata based on a boolean to Overwrite the Target Object Values
   * @param  sourceSObject Source Sobject record
   * @param  targetSobjectType Target Object API Name
   * @param  overwriteFieldValue Boolean for deciding whether to overwrite the target field value or not
   * @return  Sobject new Sobject record
   */
  public static Sobject dynamicSobjectMapping(
    SObject sourceSObject,
    SObject targetSobject,
    Boolean overwriteFieldValue
  ) {
    Schema.sObjectType sourceSobjectType = sourceSObject.getSObjectType();
    Schema.sObjectType targetSobjectType = targetSobject.getSObjectType();
    String metaDatakey = SObjectFieldMappingController.getUniqueMetadataKeyValue(
      string.valueOf(sourceSobjectType) +
      '_to_' +
      string.valueOf(targetSobjectType)
    );
    if (
      MetadataService.fieldMappingMetadataMap != null &&
      MetadataService.fieldMappingMetadataMap.containsKey(metaDatakey) &&
      MetadataService.fieldMappingMetadataMap.get(metaDatakey)
        .FieldMappingJSON__c != null
    ) {
      for (
        OrdwayFieldMappingParser.MappingParser thisMapping : OrdwayFieldMappingParser.parse(
          MetadataService.fieldMappingMetadataMap.get(metaDatakey)
            .FieldMappingJSON__c
        )
      ) {
        if (
          sourceSObject.get(thisMapping.sourceFieldAPIName) != null &&
          ((targetSobject.get(thisMapping.fieldAPIName) != null &&
          overwriteFieldValue) ||
          targetSobject.get(thisMapping.fieldAPIName) == null)
        ) {
          targetSobject.put(
            thisMapping.fieldAPIName,
            sourceSObject.get(thisMapping.sourceFieldAPIName)
          );
        }
      }
    }
    return targetSobject;
  }

  /**
   * @description Method to populate Field value in Sobject Record
   * @param objectFieldDataTypeMap Object Field Data Type Map
   * @param thisSObject Sobject record in which value needs to be populated
   * @param fieldAPIName Field API Name of the SObject to which value needsto be populate
   * @param fieldValue Value to be populate in Sobject record
   **/
  public static void populateSObjectFieldValue(
    Map<String, String> objectFieldDataTypeMap,
    SObject thisSObject,
    String fieldAPIName,
    String fieldValue
  ) {
    if (String.isNotBlank(fieldValue)) {
      //Type Conversion Below - So not using standard deserializeStrict as it will result in error
      if(
        objectFieldDataTypeMap.get(fieldAPIName) == 'REFERENCE' &&
        OrdwayHelper.isValidId(fieldValue)
      ) {
        thisSObject.put(fieldAPIName, fieldValue);
      }
      else if (
        objectFieldDataTypeMap.get(fieldAPIName) == 'STRING' ||
        objectFieldDataTypeMap.get(fieldAPIName) == 'EMAIL' ||
        objectFieldDataTypeMap.get(fieldAPIName) == 'TEXTAREA' ||
        objectFieldDataTypeMap.get(fieldAPIName) == 'ADDRESS' ||
        objectFieldDataTypeMap.get(fieldAPIName) == 'PHONE' ||
        objectFieldDataTypeMap.get(fieldAPIName) == 'PICKLIST' ||
        objectFieldDataTypeMap.get(fieldAPIName) == 'URL'
      ) {
        thisSObject.put(fieldAPIName, fieldValue);
      } else if (
        objectFieldDataTypeMap.get(fieldAPIName) == 'CURRENCY' ||
        objectFieldDataTypeMap.get(fieldAPIName) == 'DOUBLE' ||
        objectFieldDataTypeMap.get(fieldAPIName) == 'INTEGER' ||
        objectFieldDataTypeMap.get(fieldAPIName) == 'PERCENT'
      ) {

        if(isNumeric(fieldValue)) {
          thisSObject.put(fieldAPIName, Decimal.valueOf(fieldValue));
        }
      } else if (
        objectFieldDataTypeMap.get(fieldAPIName) == 'BOOLEAN' &&
        (fieldValue == 'true' ||
        fieldValue == 'false')
      ) {
        thisSObject.put(fieldAPIName, Boolean.valueOf(fieldValue));
      } else if (
        (objectFieldDataTypeMap.get(fieldAPIName) == 'DATE' ||
        objectFieldDataTypeMap.get(fieldAPIName) == 'DATETIME') &&
        fieldValue != '-'
      ) {
        thisSObject.put(
          fieldAPIName,
          Date.parse(Date.ValueOf(fieldValue).format())
        );
      }
    }
  }

  public static Boolean isNumeric(String thisString){
    Boolean ReturnValue;
    try{
        Decimal.valueOf(thisString);
        ReturnValue = TRUE; 
    } catch (Exception e) {
        ReturnValue = FALSE;
    }
    return ReturnValue;
    }
}