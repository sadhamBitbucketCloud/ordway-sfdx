/**
 * @File Name          : OrdwayContractTriggerHandler_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    2/6/2020  Ordway Labs     Initial Version
 **/
@istest
private class OrdwayContractTriggerHandler_Test {
  @isTest
  static void test_OrdwayContractTriggerHandler() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    insert newSetting;
    Map<Id, OrdwayContract__c> oldOrdwayContractMap = new Map<Id, OrdwayContract__c>();

    List<Contact> ordwayContactList = new List<Contact>{
      TestObjectFactory.createContact(),
      TestObjectFactory.createContact()
    };
    insert ordwayContactList;
    OrdwayContract__c ordwayContract = new OrdwayContract__c();
    ordwayContract.BillingContact__c = ordwayContactList[0].Id;
    ordwayContract = TestObjectFactory.createOrdwayContract(ordwayContract);
    insert ordwayContract;

    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    insert newOpportunity;

    oldOrdwayContractMap.put(ordwayContract.Id, ordwayContract);
    ordwayContract.CustomerId__c = '123456';
    ordwayContract.OrdwaySubscriptionID__c = 'S-12344';
    ordwayContract.SyncedOpportunityId__c = newOpportunity.Id;
    ordwayContract.Account__c = newAccount.Id;
    ordwayContract.BillingContact__c = ordwayContactList[1].Id;
    ordwayContract.CustomerId__c = newAccount.Id;
    update ordwayContract;

    Test.startTest();
    Opportunity oppToAssert = [SELECT Id, IsSyncing__c FROM Opportunity];
    system.assertEquals(
      true,
      oppToAssert.IsSyncing__c,
      'should return true as it is synced'
    );
    Test.stopTest();
  }

  @isTest
  static void OrdwayContractTriggerHandlerDeleteTest() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    newSetting.LinkSubscriptionAutomatically__c = true;
    insert newSetting;

    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    Contact ordwayContact = TestObjectFactory.createContact();
    insert ordwayContact;

    OrdwayContract__c ordwayContract = new OrdwayContract__c();
    ordwayContract.BillingContact__c = ordwayContact.Id;
    insert ordwayContract;

    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    insert newOpportunity;

    ordwayContract.CustomerId__c = '123456';
    ordwayContract.OrdwaySubscriptionID__c = 'S-12344';
    ordwayContract.SyncedOpportunityId__c = newOpportunity.Id;
    ordwayContract.Account__c = newAccount.Id;
    ordwayContract.BillingContact__c = ordwayContact.Id;
    ordwayContract.CustomerId__c = newAccount.Id;
    update ordwayContract;

    Test.startTest();
    try {
      delete ordwayContract;
    } catch (Exception e) {
      OrdwayContract__c ordwayContractToAssert = [
        SELECT Id
        FROM OrdwayContract__c
      ];
      system.assertNotEquals(
        null,
        ordwayContractToAssert,
        'should not be null'
      );
    }

    Test.stopTest();
  }

  @isTest
  static void OrdwayContractTriggerHandlerNotSyncNightly() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    newSetting.LinkSubscriptionAutomatically__c = true;
    insert newSetting;

    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    Contact ordwayContact = TestObjectFactory.createContact();
    insert ordwayContact;

    OrdwayContract__c ordwayContract = new OrdwayContract__c();
    ordwayContract.BillingContact__c = ordwayContact.Id;
    insert ordwayContract;

    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    insert newOpportunity;

    ordwayContract.CustomerId__c = '123456';
    ordwayContract.OrdwaySubscriptionID__c = 'S-12344';
    ordwayContract.SyncedOpportunityId__c = newOpportunity.Id;
    ordwayContract.Account__c = newAccount.Id;
    ordwayContract.BillingContact__c = ordwayContact.Id;
    ordwayContract.CustomerId__c = newAccount.Id;
    Test.startTest();

    Integer RESPONSE_CODE = 200;
    String RESPONSE_BODY =
      '{ ' +
      ' "customer" : { ' +
      '   "Website" : null,' +
      '   "Name" : "Demo"' +
      '  },' +
      ' "plans" : [ {' +
      '   "OrdwayLabs__ChargeType__c" : "One time",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "1000.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "1000.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Recurring",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "500.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "500.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Usage based",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 5.0,' +
      ' "OrdwayLabs__Quantity__c" : 100.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "950.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "10.00"' +
      '} ],' +
      '"OrdwayLabs__OrdwayMRR__c" : null,' +
      '"OrdwayLabs__OrdwayContractStatus__c" : "Active",' +
      '"OrdwayLabs__AutoRenew__c" : true,' +
      '"OrdwayLabs__RenewalTerm__c" : "12",' +
      '"OrdwayLabs__ContractTerm__c" : "12",' +
      '"OrdwayLabs__ContractEffectiveDate__c" : "2020-03-27",' +
      '"OrdwayLabs__BillingStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__ServiceStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__TotalContractValue__c" : 111520.0,' +
      '"OrdwayLabs__CurrentTermEndDate__c" : "2021-03-23",' +
      '"OrdwayLabs__CurrentTermStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__CustomerId__c" : "C-00001",' +
      '"Id" : "testContractId"' +
      '}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(
        RESPONSE_CODE,
        RESPONSE_BODY.replace(
          'testContractId',
          String.valueOf(ordwayContract.Id)
        )
      )
    );
    OrdwayContractController.aSyncToOrdway(ordwayContract.Id);

    update ordwayContract;

    OrdwayContract__c ordwayContractToAssert = [
      SELECT Id, SyncStatus__c
      FROM OrdwayContract__c
    ];

    system.assertNotEquals(
      null,
      ordwayContractToAssert.SyncStatus__c,
      'should not be null'
    );

    Test.stopTest();
  }

  @isTest
  static void OrdwayContractTriggerHelper_Callout() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    newSetting.LinkSubscriptionAutomatically__c = true;
    insert newSetting;

    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    Contact ordwayContact = TestObjectFactory.createContact();
    insert ordwayContact;

    OrdwayContract__c ordwayContract = new OrdwayContract__c();
    ordwayContract.BillingContact__c = ordwayContact.Id;  
    ordwayContract.ContractEffectiveDate__c = system.today();
    ordwayContract.BillingStartDate__c = system.today();
    ordwayContract.ServiceStartDate__c = system.today();
    ordwayContract.Account__c = newAccount.Id;  
    insert ordwayContract;

    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    insert newOpportunity;

    ContractLineItem__c ordwayContractLineItem = new ContractLineItem__c();
    ordwayContractLineItem.OrdwayLabs__OrdwayContract__c = ordwayContract.Id;
    ordwayContractLineItem = TestObjectFactory.createContractLineItem(
      ordwayContractLineItem
    );
    insert ordwayContractLineItem;

    ordwayContract.CustomerId__c = '123456';
    ordwayContract.OrdwaySubscriptionID__c = 'S-12344';
    ordwayContract.SyncedOpportunityId__c = newOpportunity.Id;
    ordwayContract.BillingContact__c = ordwayContact.Id;
    ordwayContract.SyncStatus__c = 'In Sync';
    ordwayContract.CustomerId__c = newAccount.Id;

    Test.startTest();

    Integer RESPONSE_CODE = 200;
    String RESPONSE_BODY =
      '{ ' +
      ' "customer" : { ' +
      '   "Website" : null,' +
      '   "Name" : "Demo"' +
      '  },' +
      ' "plans" : [ {' +
      '   "OrdwayLabs__ChargeType__c" : "One time",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "1000.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "1000.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Recurring",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "500.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "500.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Usage based",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 5.0,' +
      ' "OrdwayLabs__Quantity__c" : 100.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "950.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "10.00"' +
      '} ],' +
      '"OrdwayLabs__OrdwayMRR__c" : null,' +
      '"OrdwayLabs__OrdwayContractStatus__c" : "Active",' +
      '"OrdwayLabs__AutoRenew__c" : true,' +
      '"OrdwayLabs__RenewalTerm__c" : "12",' +
      '"OrdwayLabs__ContractTerm__c" : "12",' +
      '"OrdwayLabs__ContractEffectiveDate__c" : "2020-03-27",' +
      '"OrdwayLabs__BillingStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__ServiceStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__TotalContractValue__c" : 111520.0,' +
      '"OrdwayLabs__CurrentTermEndDate__c" : "2021-03-23",' +
      '"OrdwayLabs__CurrentTermStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__CustomerId__c" : "C-00001",' +
      '"Id" : "testContractId"' +
      '}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(
        RESPONSE_CODE,
        RESPONSE_BODY.replace(
          'testContractId',
          String.valueOf(ordwayContract.Id)
        )
      )
    );
    OrdwayContractController.aSyncToOrdway(ordwayContract.Id);
    update ordwayContract;
    OrdwayContract__c ordwayContractToAssert = [
      SELECT Id, SyncStatus__c
      FROM OrdwayContract__c
    ];
    system.assertEquals(
      OrdwayHelper.SYNC_STATE_IN_SYNC,
      ordwayContractToAssert.SyncStatus__c,
      'should be In sync'
    );
    Test.stopTest();
  }
}