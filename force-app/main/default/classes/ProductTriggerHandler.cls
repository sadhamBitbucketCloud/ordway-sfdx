public with sharing class ProductTriggerHandler extends TriggerHandler.TriggerHandlerBase {
  
  public static Boolean createOrdwayProduct = true; //Set to false to prevent duplicate Ordway Product records from being created

  public override void afterInsert(Map<Id, SObject> mapNewSObjs) {
    if(createOrdwayProduct) {
      ProductService.createOrdwayProduct(mapNewSObjs.values());
    }
  }

  public override void afterUpdate(Map<Id, SObject> mapOldSObjs, Map<Id, SObject> mapNewSObjs) {
    ProductService.upsertOrdwayProduct((Map<Id, Product2>) mapOldSObjs, (Map<Id, Product2>) mapNewSObjs, createOrdwayProduct);
  }
}