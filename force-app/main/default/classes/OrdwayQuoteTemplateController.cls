/**
 * @File Name          : OrdwayQuoteTemplateController.cls
 * @Description        :
 * @Author             : 
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 08-25-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/22/2020        Initial Version
 **/
public with sharing class OrdwayQuoteTemplateController {
  /**
   * @description Method to get Ordway Quote Template from Ordway
   * @param xEntityId Entity Id
   **/
  public static void getOrdwayTemplate(String xEntityId) {
    ApplicationLog.StartLoggingContext(
      'OrdwayQuoteTemplateController',
      'getOrdwayTemplate()'
    );

    Map<String, Object> responseMap = new Map<String, Object>();
    List<QuoteTemplate__c> quoteTemplateList = new List<QuoteTemplate__c>();

    try {
      HttpResponse thisResponse = OrdwayService.getQuoteTemplate(xEntityId);

      if (thisResponse.getStatusCode() == 200) {
        for (
          Object thisObject : (List<Object>) JSON.deserializeUntyped(
            thisResponse.getBody()
          )
        ) {
          responseMap = (Map<String, Object>) JSON.deserializeUntyped(
            JSON.serialize(thisObject)
          );

          QuoteTemplate__c quoteTemplate = new QuoteTemplate__c();
          quoteTemplate.TemplateId__c = String.valueOf(
            responseMap.get('template_id')
          );
          quoteTemplate.Id__c = xEntityId == null
            ? String.valueOf(responseMap.get('id'))
            : xEntityId + ':' + String.valueOf(responseMap.get('id'));
          quoteTemplate.Name = String.valueOf(responseMap.get('name'));
          quoteTemplate.Default__c = Boolean.valueOf(
            responseMap.get('enabled')
          );
          quoteTemplate.Active__c = true;
          quoteTemplate.Description__c = String.valueOf(
            responseMap.get('description')
          );
          quoteTemplate.EntityID__c = xEntityId;
          OrdwayEntityHelper.populateOrdwayEntityName(quoteTemplate);
          quoteTemplateList.add(quoteTemplate);
        }
      }
      //PlanId Unique External Id
      SObjectAccessDecision decision = Security.stripInaccessible(
        AccessType.UPSERTABLE,
        quoteTemplateList
      );
      ApplicationLog.Info(JSON.serializePretty(decision.getRemovedFields()));
      ApplicationLog.Info(JSON.serializePretty(decision.getRecords()));
      upsert (List<QuoteTemplate__c>) decision.getRecords() Id__c;
    } catch (Exception e) {
      ApplicationLog.Info(e.getMessage());
      ApplicationLog.Info(e.getStackTraceString());
      ApplicationLog.Error(e);
      ApplicationLog.FinishLoggingContext();
    }
  }

  /**
   * @description Method to Get Template From Ordway Quote Template Object
   * @param entityId Entity Id
   * @return List<QuoteTemplate__c> List of Quote Template
   **/
  public static List<QuoteTemplate__c> getOrdwayQuoteTemplate(String entityId) {
    List<QuoteTemplate__c> ordwayQuoteTemplateList = new List<QuoteTemplate__c>();

    if (Schema.sObjectType.QuoteTemplate__c.isAccessible()) {
      ordwayQuoteTemplateList = new List<QuoteTemplate__c>(
        [
          SELECT Id, Name, Description__c, Active__c, Default__c, TemplateId__c
          FROM QuoteTemplate__c
          WHERE Active__c = TRUE AND EntityID__c = :entityId
          ORDER BY TemplateId__c DESC
        ]
      );
    }
    return ordwayQuoteTemplateList;
  }

  public static Map<String, Id> defaultQuoteTemplateMap {
    get {
      if (defaultQuoteTemplateMap == null) {
        defaultQuoteTemplateMap = new Map<String, Id>();
        if (Schema.sObjectType.QuoteTemplate__c.isAccessible()) {
          for(QuoteTemplate__c thisTemplate : [SELECT Id, Default__c, EntityId__c FROM QuoteTemplate__c WHERE Default__c = TRUE AND Active__c = TRUE]) {
            if(thisTemplate.EntityId__c != null && OpportunitySettingController.isMultiEntityEnabled()) {
              if(defaultQuoteTemplateMap.containsKey(thisTemplate.EntityId__c)) { continue; }
              defaultQuoteTemplateMap.put(thisTemplate.EntityId__c, thisTemplate.Id);
            }
            else {
              defaultQuoteTemplateMap.put(thisTemplate.Id, thisTemplate.Id);
            } 
          }
        }
      }
      return defaultQuoteTemplateMap;
    }
    private set;
  }
}