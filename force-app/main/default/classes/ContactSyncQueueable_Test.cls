@isTest
private class ContactSyncQueueable_Test {
    @isTest
    private static void syncContactToOrdway_Insert() {
        ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
        insert thisApplicationsetting;

        OrdwayLabs__ContractPlanPickerSetting__c opportunitySettingInstance = TestObjectFactory.createContractPlanPickerSetting();
        opportunitySettingInstance.AutomaticallySyncContact__c = true;
        OpportunitySettingController.saveOpportunitySetting(opportunitySettingInstance);

        Account ordwayAccount = TestObjectFactory.createAccount();
        ordwayAccount.OrdwayCustomerId__c = 'C-00011';
        insert ordwayAccount;

        Contact ordwayContact = TestObjectFactory.createContact();
        ordwayContact.AccountId = ordwayAccount.Id;
        ordwayContact.Sync_to_Ordway__c = true;

        Test.startTest();

        Integer RESPONSE_CODE = 200;
        String RESPONSE_BODY ='  {"Name":"Test NPPP", '
                            +' "LastName":"NPPP", '
                            +' "FirstName":"Test"} ';
        Test.setMock(HttpCalloutMock.class, new OrdwayService_Mock( RESPONSE_CODE, RESPONSE_BODY));
        insert ordwayContact;

        Test.stopTest();
        Contact contactToAssert = [SELECT Id, Name FROM Contact];
        System.assertEquals('Test NPPP', contactToAssert.Name, 'Should return same as response - Test NPPP');
    }

    @isTest
    private static void syncContactToOrdway_InsertBulk() {
        ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
        insert thisApplicationsetting;

        OrdwayLabs__ContractPlanPickerSetting__c opportunitySettingInstance = TestObjectFactory.createContractPlanPickerSetting();
        opportunitySettingInstance.AutomaticallySyncContact__c = true;
        OpportunitySettingController.saveOpportunitySetting(opportunitySettingInstance);

        Account ordwayAccount = TestObjectFactory.createAccount();
        ordwayAccount.OrdwayCustomerId__c = 'C-00011';
        insert ordwayAccount;

        List<Contact> contactToInsert = new List<Contact>();
        for(Integer i =0; i<=50;i++){
            Contact ordwayContact = TestObjectFactory.createContact();
            ordwayContact.AccountId = ordwayAccount.Id;
            ordwayContact.Sync_to_Ordway__c = true;
            contactToInsert.add(ordwayContact);
        }
        insert contactToInsert;
        Test.startTest();

        Integer RESPONSE_CODE = 200;
        String RESPONSE_BODY ='  {"Name":"Test NPPP", '
                            +' "LastName":"NPPP", '
                            +' "FirstName":"Test"} ';
        Test.setMock(HttpCalloutMock.class, new OrdwayService_Mock( RESPONSE_CODE, RESPONSE_BODY));

        Test.stopTest();
        system.assertEquals(1, [SELECT Id, Name FROM Contact WHERE Name = 'Test NPPP'].size(), 'should return only one account as it is not supported for bulk');
    }

    @isTest
    private static void syncContactToOrdway_Update() {
        ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
        insert thisApplicationsetting;

        OrdwayLabs__ContractPlanPickerSetting__c opportunitySettingInstance = TestObjectFactory.createContractPlanPickerSetting();
        opportunitySettingInstance.AutomaticallySyncContact__c = true;
        OpportunitySettingController.saveOpportunitySetting(opportunitySettingInstance);

        Account ordwayAccount = TestObjectFactory.createAccount();
        ordwayAccount.OrdwayCustomerId__c = 'C-00011';
        insert ordwayAccount;

        Contact ordwayContact = TestObjectFactory.createContact();
        ordwayContact.AccountId = ordwayAccount.Id;
        ordwayContact.Sync_to_Ordway__c = false;
        insert ordwayContact;

        Test.startTest();
        Integer RESPONSE_CODE = 200;
        String RESPONSE_BODY ='  {"Name":"Test NPPP", '
                            +' "LastName":"NPPP", '
                            +' "FirstName":"Test"} ';
        Test.setMock(HttpCalloutMock.class, new OrdwayService_Mock( RESPONSE_CODE, RESPONSE_BODY));
        ordwayContact.Sync_to_Ordway__c = true;
        update ordwayContact;
        Test.stopTest();
        Contact contactToAssert = [SELECT Id, Name FROM Contact];
        System.assertEquals('Test NPPP', contactToAssert.Name, 'Should return same as response - Test NPPP');
    }

    @isTest
    private static void syncContactToOrdway_Negative() {
        ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
        insert thisApplicationsetting;

        OrdwayLabs__ContractPlanPickerSetting__c opportunitySettingInstance = TestObjectFactory.createContractPlanPickerSetting();
        opportunitySettingInstance.AutomaticallySyncAccount__c = false;
        OpportunitySettingController.saveOpportunitySetting(opportunitySettingInstance);

        Account ordwayAccount = TestObjectFactory.createAccount();
        ordwayAccount.OrdwayCustomerId__c = 'C-00011';
        insert ordwayAccount;

        Contact ordwayContact = TestObjectFactory.createContact();
        ordwayContact.AccountId = ordwayAccount.Id;
        ordwayContact.Sync_to_Ordway__c = true;

        Test.startTest();
        insert ordwayContact;
        Test.stopTest();
        Contact contactToAssert = [SELECT Id, Sync_to_Ordway__c FROM Contact];
        System.assertEquals(true, contactToAssert.Sync_to_Ordway__c, 'Should return true');
    }
}