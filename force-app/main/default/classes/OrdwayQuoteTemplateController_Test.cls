/**
 * @File Name          : OrdwayQuoteTemplateController_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/5/2020        Initial Version
 **/
@isTest
private class OrdwayQuoteTemplateController_Test {
  @isTest
  private static void getOrdwayTemplate() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();

    Integer RESPONSE_CODE = 200;
    String RESPONSE_BODY =
      '[' +
      '{' +
      ' "description" :  "Quotes", ' +
      ' "enabled" : true,' +
      ' "id" : 2762,' +
      ' "name" : "Quote Default",' +
      ' "subject" : "Quotes",' +
      ' "template_id" : "TMPLT-00020"' +
      '}' +
      ']';

    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSE_CODE, RESPONSE_BODY)
    );
    OrdwayQuoteTemplateController.getOrdwayTemplate(null);
    List<QuoteTemplate__c> thisQuoteTemplate = new List<QuoteTemplate__c>(
      [
        SELECT Id, Name, TemplateId__c, Id__c, Active__c, Description__c
        FROM QuoteTemplate__c
        WHERE TemplateId__c = :'TMPLT-00020'
      ]
    );

    if (thisQuoteTemplate.size() > 0) {
      system.assertEquals(
        'Quotes',
        thisQuoteTemplate[0].Description__c,
        'should be Quotes as from the response body'
      );
      system.assertEquals(
        true,
        thisQuoteTemplate[0].Active__c,
        'should be true as from the response body'
      );
      system.assertEquals(
        'Quote Default',
        thisQuoteTemplate[0].Name,
        'should be Quote Default as from the response body'
      );
      system.assertEquals(
        '2762',
        thisQuoteTemplate[0].Id__c,
        'should be 2762 as from the response body'
      );
      List<QuoteTemplate__c> quoteTemplateListToAssert = OrdwayQuoteTemplateController.getOrdwayQuoteTemplate(
        null
      );
      system.assertEquals(
        1,
        quoteTemplateListToAssert.size(),
        'should be 1 as from the response body'
      );
    }
    Test.stopTest();
  }

  @isTest
  private static void getOrdwayTemplate_Negative() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();

    Integer RESPONSE_CODE = 200;
    String RESPONSE_BODY =
      '[' +
      '{' +
      ' "description" :  "Quotes", ' +
      ' "enabled" : true,' +
      ' "name" : "Quote Default",' +
      ' "subject" : "Quotes",' +
      '}' +
      ']';

    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSE_CODE, RESPONSE_BODY)
    );
    OrdwayQuoteTemplateController.getOrdwayTemplate(null);
    List<QuoteTemplate__c> quoteTemplateListToAssert = OrdwayQuoteTemplateController.getOrdwayQuoteTemplate(
      null
    );
    system.assertEquals(
      0,
      quoteTemplateListToAssert.size(),
      'should be 0 as it required field is missing upon creation'
    );
    Test.stopTest();
  }

  @isTest
  private static void getOrdwayTemplate_401() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();
    Integer RESPONSE_CODE = 401;
    String RESPONSE_BODY = 'Connection error';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSE_CODE, RESPONSE_BODY)
    );
    OrdwayQuoteTemplateController.getOrdwayTemplate(null);
    List<QuoteTemplate__c> quoteTemplateListToAssert = OrdwayQuoteTemplateController.getOrdwayQuoteTemplate(
      null
    );
    system.assertEquals(
      0,
      quoteTemplateListToAssert.size(),
      'should be 0 as it is a connection error'
    );
    Test.stopTest();
  }

  @isTest
  private static void getOrdwayTemplate_400() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();
    Integer RESPONSE_CODE = 400;
    String RESPONSE_BODY = '{"details": "Internal server error"}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSE_CODE, RESPONSE_BODY)
    );
    OrdwayQuoteTemplateController.getOrdwayTemplate(null);
    List<QuoteTemplate__c> quoteTemplateListToAssert = OrdwayQuoteTemplateController.getOrdwayQuoteTemplate(
      null
    );
    system.assertEquals(
      0,
      quoteTemplateListToAssert.size(),
      'should be 0 as it is a internal server error'
    );
    Test.stopTest();
  }

  @isTest
  private static void getOrdwayTemplate_500() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();
    Integer RESPONSE_CODE = 500;
    String RESPONSE_BODY = 'Bad request';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSE_CODE, RESPONSE_BODY)
    );
    OrdwayQuoteTemplateController.getOrdwayTemplate(null);
    List<QuoteTemplate__c> quoteTemplateListToAssert = OrdwayQuoteTemplateController.getOrdwayQuoteTemplate(
      null
    );
    system.assertEquals(
      0,
      quoteTemplateListToAssert.size(),
      'should be 0 as it is a bad request'
    );
    Test.stopTest();
  }
}