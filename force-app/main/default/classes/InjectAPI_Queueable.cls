/**
 * @description       : Update Salesforce Id at Subscription & Subscription Charge Level In Ordway
 * @author            : Ordway Labs
 * @group             :
 * @last modified on  : 04-17-2021
 * @last modified by  : 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   08-15-2020      Initial Version
 **/
public with sharing class InjectAPI_Queueable implements Queueable, Database.AllowsCallouts {
  
  public String requestObjectType;
  public Set<String> requestObjectIds;
  public String xEntityId;
  static MetadataService.DynamicReference dynamicReference;

  //TODO: Worth moving this to metadata configuration in future
  static Map<String, String> externalIdFieldMap = new  Map<String, String>{ 
                                                                          'Contact'        => 'OrdwayLabs__OrdwayContactID__c',
                                                                          'OrdwayContract' => 'OrdwayLabs__OrdwaySubscriptionID__c',
                                                                          'Usage'          => 'OrdwayLabs__ExternalID__c',
                                                                          'OrdwayProduct'  => 'OrdwayLabs__OrdwayProductId__c',
                                                                          'Plan'           => 'OrdwayLabs__PlanId__c'};
  
  static Map<String, String> fieldValueMap = new  Map<String, String>{ 
                                                                    'Contact'        => OrdwayHelper.SOBJECT_FIELD_ID,
                                                                    'OrdwayContract' => OrdwayHelper.SOBJECT_FIELD_ID,
                                                                    'Usage'          => OrdwayHelper.SOBJECT_FIELD_ID,
                                                                    'OrdwayProduct'  => 'OrdwayLabs__Product2Id__c',
                                                                    'Plan'           => OrdwayHelper.SOBJECT_FIELD_ID};

  static Map<String, String> model = new  Map<String, String>{ 
                                                              'Contact'        => OrdwayHelper.MODEL_CONTACTS,
                                                              'OrdwayContract' => OrdwayHelper.MODEL_SUBSCRIPTIONS,
                                                              'Usage'          => OrdwayHelper.MODEL_USAGE,
                                                              'OrdwayProduct'  => OrdwayHelper.MODEL_ORDWAY_PRODUCT,
                                                              'Plan'           => OrdwayHelper.MODEL_ORDWAY_PLAN};

  /**
   * @description Queueable to Update Salesforce Id at Subscription & Subscription Charge Level In Ordway
   * @param requestObjectType Sobject Type
   * @param requestObjectIds Set of Sobject Ids
   * @param xEntityId
   **/
  public InjectAPI_Queueable( String requestObjectType, Set<String> requestObjectIds, String xEntityId ) {
    this.requestObjectType = requestObjectType;
    this.requestObjectIds = requestObjectIds;
    this.xEntityId = xEntityId;
  }

  public void execute(QueueableContext context) {

    ApplicationLog.StartLoggingContext( 'InjectAPI_Queueable', 'InjectAPI_Queueable => execute() ' + requestObjectType );
    dynamicReference = MetadataService.dynamicMap.get(requestObjectType);

    try {
      String parentId;

      String parentQuery =
        'SELECT '+externalIdFieldMap.get(requestObjectType)+','+fieldValueMap.get(requestObjectType)+' FROM ' +
        dynamicReference.containerObjectName +
        ' WHERE Id IN: requestObjectIds';

      List<SObject> parentSObjects = Database.query(parentQuery);
      Map<String, List<SObject>> childSobjectsMap = new Map<String, List<SObject>>();

      if(requestObjectType == 'OrdwayContract' || requestObjectType == 'Plan') {
        String childQuery =
        'SELECT Id, ' +
        dynamicReference.childPrimaryExternalReference +
        ' ,' +
        dynamicReference.childPrimaryRelationshipField +
        ' FROM ' +
        dynamicReference.childObjectName +
        ' WHERE ' +
        dynamicReference.childPrimaryRelationshipField +
        ' IN: requestObjectIds';

        List<SObject> childsObject = Database.query(childQuery);

        for (SObject thisChildSObject : childsObject) {
          parentId = String.valueOf( thisChildSObject.get( dynamicReference.childPrimaryRelationshipField ) );
          if (!childSobjectsMap.containsKey(parentId)) {
            childSobjectsMap.put(parentId, new List<SObject>());
          }
          childSobjectsMap.get(parentId).add(thisChildSObject);
        }
      }

      invokeInjectAPI( parentSObjects, childSobjectsMap, requestObjectType, xEntityId );
      //Adding FinishLoggingContext here will throw Uncommitted Work Pending In Progress Error
    } catch (Exception ex) {
      ApplicationLog.Error(ex);
      ApplicationLog.FinishLoggingContext();
    }
  }

  /**
   * @description Method to invoke Inject API
   * @param parentSObjects List of Parent Sobjects
   * @param childSobjectsMap Map of Child Sobjects
   * @param requestObjectType Object Type
   * @param xEntityId Entity Id
   **/
  public static void invokeInjectAPI( List<SObject> parentSObjects, Map<String, List<SObject>> childSobjectsMap, String requestObjectType, String xEntityId ) {

    ApplicationLog.StartLoggingContext( 'InjectAPI_Queueable', 'InjectAPI_Queueable => invokeInjectAPI() ' );
    try {

      xTransaction thisInjectAPITransaction = new xTransaction( parentSObjects, childSobjectsMap, requestObjectType );
      HttpResponse thisResponse = OrdwayService.injectRecordId( JSON.serializePretty(thisInjectAPITransaction), xEntityId );

      if(thisResponse.getStatusCode() == 200
      && requestObjectType == 'OrdwayContract') {
        updateSyncStatus(parentSObjects, childSobjectsMap);
      }
      ApplicationLog.FinishLoggingContext();
    } catch (Exception thisException) {
      ApplicationLog.Error(thisException);
      ApplicationLog.FinishLoggingContext();
    }
  }

  private static void updateSyncStatus(List<SObject> parentSObjects, Map<String, List<SObject>> childSobjectsMap) {

    List<OrdwayContract__c> contractList = new List<OrdwayContract__c>();
    List<ContractLineItem__c> cliList = new List<ContractLineItem__c>();

    for (SObject thisParent : parentSObjects) {
      OrdwayContract__c thisContract = new OrdwayContract__c();
      thisContract.put('Id', thisParent.Id);
      thisContract.put('OrdwayLabs__SyncStatus__c', OrdwayHelper.SYNC_STATE_IN_SYNC);
      contractList.add(thisContract);

      for (SObject thisChild : childSobjectsMap.get(thisParent.Id)) {
        //Updating Child Id - So External ID will act as Identifer always
          ContractLineItem__c thisCLI = new ContractLineItem__c();
          thisCLI.put('OrdwayLabs__SubscriptionID_LineID__c', thisChild.Id);
          thisCLI.put('Id', thisChild.Id);
          cliList.add(thisCLI);
      }
    }

    OrdwayContractTriggerHelper.runOnce = false;
    if (Schema.sObjectType.OrdwayContract__c.isUpdateable()) {
      update contractList;
    }
    
    if (Schema.sObjectType.ContractLineItem__c.isUpdateable()) {
      update cliList;
    }
  }

  //Inject API Payload Structure
  public class xTransaction {
    //Transaction is a reserved Keyword, so adding X to it
    List<Object> transactions = new List<Object>();

    public xTransaction(
      List<SObject> parentSObjects,
      Map<String, List<SObject>> childSobjectsMap,
      String requestObjectType
    ) {
      for (SObject thisParent : parentSObjects) {
        List<String> ListofParentUniqueIds = String.valueOf( thisParent.get(externalIdFieldMap.get(requestObjectType)) ).split(':');
        Transactions thisTransactions = new Transactions();
        thisTransactions.model_name   = model.get(requestObjectType);
        thisTransactions.field_id     = ListofParentUniqueIds[ ListofParentUniqueIds.size() - 1 ];
        thisTransactions.api_name     = OrdwayHelper.SALESFORCE_ID;
        thisTransactions.field_value  = String.valueOf(thisParent.get(fieldValueMap.get(requestObjectType)));

        thisTransactions.children = new List<Children>();
        Map<String, Object> childValueAsMap = new Map<String, Object>();

          if (childSobjectsMap.containsKey(thisTransactions.field_value)) {
            for ( SObject thisChild : childSobjectsMap.get( thisTransactions.field_value ) ) {
              Children thisChildren = new Children();
              List<String> ListofChildUniqueIds = String.valueOf( thisChild.get( dynamicReference.childPrimaryExternalReference ) ).split(':');
              
              thisChildren.field_id = ListofChildUniqueIds[ ListofChildUniqueIds.size() - 1 ];

              if(requestObjectType == 'Plan') {
                thisChildren.api_name = OrdwayHelper.PLANCHARGE_EXTERNAL_ID;
                thisChildren.model_name = OrdwayHelper.MODEL_CHARGES;
                thisChildren.custom_field = true;
              }
              else {
                thisChildren.api_name = OrdwayHelper.CHARGE_EXTERNAL_ID;
                thisChildren.model_name = OrdwayHelper.MODEL_SUBSCRIPTION_CHARGES;
              }
              
              thisChildren.field_value = String.valueOf( thisChild.get(OrdwayHelper.SOBJECT_FIELD_ID) );

              if (String.isNotBlank(thisChildren.field_id)) {
                thisTransactions.children.add(thisChildren);
              }
            }
          }

          childValueAsMap = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(thisTransactions));
          if(!dynamicReference.hasChildRelationship) {
            childValueAsMap.remove('children');
          }

          transactions.add(JSON.deserializeUntyped(JSON.serialize(childValueAsMap)));
      }
    }
  }

  public class Transactions {
    public String model_name;
    public String field_id;
    public String api_name;
    public String field_value;
    public Boolean custom_field;
    public List<Children> children;
    Transactions() {
      this.custom_field = true;
    }
  }

  public class Children {
    public String model_name;
    public String field_id;
    public String api_name;
    public String field_value;
    public Boolean custom_field;
    Children() {
      this.custom_field = false; // We are updating standard field on Subscription Charge
    }
  }
}