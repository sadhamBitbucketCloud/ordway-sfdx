public with sharing class PlanPickerHelper {

	public static	Map<String, String> parentToChildRelationshipMap = new Map<String, String>{'Opportunity' => 'OpportunityLineItem', 'OrdwayLabs__Quote__c' => 'OrdwayLabs__QuoteLineItem__c'};
	public static Map<String, String> parentRelationshipFieldMap   = new Map<String, String>{'OpportunityLineItem' => 'OpportunityId', 'OrdwayLabs__QuoteLineItem__c' => 'OrdwayLabs__Quote__c'};

  public static	Map<String, String> lineItemToChildTierRelationshipMap = new Map<String, String>{'OpportunityLineItem' => 'OrdwayLabs__OpportunityLineItemTier__c', 'OrdwayLabs__QuoteLineItem__c' => 'OrdwayLabs__QuoteLineItemTier__c'};

  public static Map<String, String> lineItemToChildTierRelationshipFieldMap   = new Map<String, String>{'OrdwayLabs__OpportunityLineItemTier__c' => 'OrdwayLabs__OpportunityProduct__c', 'OrdwayLabs__QuoteLineItemTier__c' => 'OrdwayLabs__QuoteLineItem__c'};

  public static Map<String, Map<String, String>> fieldAPINameToPickListLabelMap = new  Map<String, Map<String, String>>();

	public static Map<String, List<SObject>> getLineItemsByPlanChargeId(String parentRecordId, Set<Id> lineitemIds) {
		Map<String, List<SObject>> oliByPlanChargeIdMap = new Map<String, List<SObject>>();

    for (SObject thisLineItem : getChildLineItems(parentRecordId).values()) {
			thisLineItem.put('OrdwayLabs__BillingPeriod__c', !String.isBlank(getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__BillingPeriod__c')) ? getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__BillingPeriod__c') : 'N/A');
			thisLineItem.put('OrdwayLabs__UnitofMeasure__c', !String.isBlank(getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__UnitofMeasure__c')) ? getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__UnitofMeasure__c') : 'N/A');
			thisLineItem.put('OrdwayLabs__IncludedUnits__c', !String.isBlank(getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__IncludedUnits__c')) ? getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__IncludedUnits__c') : 'N/A');
			lineitemIds.add(Id.valueOf(String.valueOf(thisLineItem.get('Id'))));
			String ordwayPlanId 	= getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__OrdwayPlanId__c');
			String ordwayChargeId =	getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__OrdwayChargeId__c');

			if (ordwayPlanId != null && ordwayChargeId != null) {
				String key = ordwayPlanId + '_' + ordwayChargeId;
				if(!oliByPlanChargeIdMap.containsKey(key)){
					oliByPlanChargeIdMap.put(key, new List<SObject>());
				}
				oliByPlanChargeIdMap.get(key).add(thisLineItem);
      }
    }
    return oliByPlanChargeIdMap;
  }

	public static String getSObjectfieldValueAsString(SObject thisSObject, String fieldAPIName){
		return thisSObject.get(fieldAPIName) != null ? String.valueOf(thisSObject.get(fieldAPIName)) : null;
	}

  public static Map<Id, SObject> getChildLineItems(String parentRecordId){
    String childSObjectAPIName;
    String parentRelationshipField;
    if(parentToChildRelationshipMap.containsKey(String.valueOf(Id.valueOf(parentRecordId).getSObjectType()))){
      childSObjectAPIName = parentToChildRelationshipMap.get(String.valueOf(Id.valueOf(parentRecordId).getSObjectType()));
    }

    if(parentRelationshipFieldMap.containsKey(childSObjectAPIName)){
      parentRelationshipField = parentRelationshipFieldMap.get(childSObjectAPIName);
    }

    Map<Id, SObject> childLineItems = new Map<Id, SObject>((List<SObject>) Database.query(getChildLineItemQueryString(childSObjectAPIName, parentRelationshipField)));
    return childLineItems;
  }

  public static String getChildLineItemQueryString(String childAPIName, String parentRelationshipFieldName){
    SObjectType childSobjectType = ((SObject)Type.forName(childAPIName).newInstance()).getSObjectType();
    return 'SELECT ' +String.join(SObjectHelper.getAllFields(childSobjectType),',') +' FROM '+childAPIName+' WHERE '+parentRelationshipFieldName+' =:parentRecordId';
	}
	
  public static ContractPlanPickerSetting__c getOpportunityCustomSetting() {
    return ContractPlanPickerSetting__c.getValues(OrdwayHelper.OPPORTUNITY_SETTING_NAME) == null ? new ContractPlanPickerSetting__c() : ContractPlanPickerSetting__c.getValues(OrdwayHelper.OPPORTUNITY_SETTING_NAME);
	}

	public static Map<Id, SObject> getParentRecord(String parentRecordId){
		Map<Id, SObject> thisSObjectMap;
			thisSObjectMap = new Map<Id, SObject>((List<SObject>) Database.query('SELECT ' +String.join(SObjectHelper.getAllFields(Id.valueOf(parentRecordId).getSObjectType()),',') +' FROM '+ String.valueOf(Id.valueOf(parentRecordId).getSObjectType())+' WHERE Id =:parentRecordId'));
			return thisSObjectMap;
	}

	public static void onLoadDefaultValues(SObject thisSObject) {
		thisSObject.put('OrdwayLabs__BillingPeriod__c', 'N/A');
		thisSObject.put('OrdwayLabs__OrdwayListPrice__c', 0);
		thisSObject.put('OrdwayLabs__UnitofMeasure__c', 'N/A');
		thisSObject.put('OrdwayLabs__IncludedUnits__c', 'N/A');
		thisSObject.put('OrdwayLabs__TierJSON__c', '');
	}

public static Map<String, RequiredFieldWrapper> getPlanPickerWrapper(String parentObjectAPIName, String currencyISO){
		Map<String, RequiredFieldWrapper> requiredFieldWrapperMap = new Map<String, RequiredFieldWrapper>();
		String childSObjectAPIName;
    if(parentToChildRelationshipMap.containsKey(parentObjectAPIName)){
        childSObjectAPIName = parentToChildRelationshipMap.get(parentObjectAPIName);
    }
    
    Schema.DescribeSObjectResult sObjectDescribe = Schema.getGlobalDescribe().get(childSObjectAPIName).getDescribe();
    
    if(MetadataService.getPlanPickerByObjectAPIName(childSObjectAPIName) != null){
      List<PlanPickerMetadataWrapper> thisPlanPickerConfiguration = (List<PlanPickerMetadataWrapper>)JSON.deserialize(MetadataService.getPlanPickerByObjectAPIName(childSObjectAPIName).OrdwayLabs__Configuration__c, List<PlanPickerMetadataWrapper>.class);

      for(PlanPickerMetadataWrapper thisPlanPickerMetadata : thisPlanPickerConfiguration){
        if(thisPlanPickerMetadata.Visible){
          RequiredFieldWrapper thisFieldWrapper;
          if(thisPlanPickerMetadata.Edit){
            thisFieldWrapper = constructEditableFieldWrapper(getFieldDescribeFromMetadata(thisPlanPickerMetadata, sObjectDescribe), currencyISO);
          }
          else{
            thisFieldWrapper = constructReadOnlyFieldWrapper(getFieldDescribeFromMetadata(thisPlanPickerMetadata, sObjectDescribe), currencyISO);
          }
          
          if(thisFieldWrapper != null){
            requiredFieldWrapperMap.put(thisFieldWrapper.Name, thisFieldWrapper);
          }
        }
      }
    }
		return requiredFieldWrapperMap;
	}

	public static DescribeFieldResult getFieldDescribeFromMetadata(PlanPickerMetadataWrapper thisMetadata,Schema.DescribeSObjectResult sObjectDescribe){
		return sObjectDescribe.fields.getMap().get(thisMetadata.Field.toLowerCase().trim()).getDescribe();
	}
	
	public static Map<String, String> componentOutputTypeMap = new Map<String, String>{
																																											'CURRENCY'  => 'lightning:formattedNumber',
																																											'DOUBLE'    => 'lightning:formattedNumber',
																																											'PERCENT'   => 'lightning:formattedNumber',
																																											'INTEGER'   => 'lightning:formattedNumber',
																																											'LONG'      => 'lightning:formattedNumber',
																																											'DATE'      => 'lightning:formattedDateTime'
																																										};
	
	public static Map<String, String> numberStyleMap = new Map<String, String>{
																																						'CURRENCY'  => 'currency',
																																						'PERCENT'   => 'percent-fixed'
																																					};

	public static RequiredFieldWrapper constructReadOnlyFieldWrapper(DescribeFieldResult fieldDescribe, String currencyISO){
		String thisFieldType = String.ValueOf(fieldDescribe.getType());
		if (fieldDescribe.isCreateable() 
			&& fieldDescribe.isCustom() 
			&& !fieldDescribe.isAutoNumber() 
			&& !thisFieldType.contains('REFERENCE') 
			&& thisFieldType != 'BOOLEAN') {
			RequiredFieldWrapper thisWrapper = new RequiredFieldWrapper();
			ComponentAttributes thisComponentAttributes = new ComponentAttributes();

			thisWrapper.name = fieldDescribe.getName();
			thisWrapper.label = fieldDescribe.getLabel();

			if(componentOutputTypeMap.containsKey(thisFieldType)){
				thisWrapper.componentType = componentOutputTypeMap.get(thisFieldType);
			} 
			else{
				thisWrapper.componentType = 'lightning:formattedText';
			} 

			if(numberStyleMap.containsKey(thisFieldType)){
				thisComponentAttributes.style = numberStyleMap.get(thisFieldType);
			}

			if(thisFieldType == 'CURRENCY'){
				thisComponentAttributes.currencyCode = currencyISO;
				thisComponentAttributes.currencyDisplayAs = 'symbol';
        thisWrapper.isCurrency = true;
			}

			if(thisFieldType == 'DATE'){
				thisComponentAttributes.year = 'numeric';
				thisComponentAttributes.month = 'numeric';
				thisComponentAttributes.day = 'numeric';
			}

      if (thisFieldType == 'PICKLIST') {
        thisWrapper.isReadOnlyPickListField = true;
        populatePickListFieldLabel(fieldDescribe, thisWrapper);
      }

			thisWrapper.attributeObject = thisComponentAttributes;
			return thisWrapper;
		}
		return null;
  }
	
	public static RequiredFieldWrapper constructEditableFieldWrapper(DescribeFieldResult fieldDescribe, String currencyISO){
		//Read only fields on the plan 
    Set<String> staticFieldsToexclude = new Set<String>{
      'OrdwayLabs__TierJSON__c',
      'OrdwayLabs__Product_Name__c'
    };

		String thisFieldType = String.ValueOf(fieldDescribe.getType());
		if (fieldDescribe.isCreateable() 
			&& (fieldDescribe.isCustom() || fieldDescribe.getName() == 'Discount' || fieldDescribe.getName() == 'Quantity') 
			&& !fieldDescribe.isAutoNumber() 
			&& !staticFieldsToexclude.contains(fieldDescribe.getName()) 
			&& !thisFieldType.contains('REFERENCE')) {
				RequiredFieldWrapper thisWrapper = new RequiredFieldWrapper();
				thisWrapper.name = fieldDescribe.getName();
				thisWrapper.label = fieldDescribe.getLabel();
				populateAttribute(thisFieldType, thisWrapper, fieldDescribe);
				if(thisFieldType == 'CURRENCY'){
					thisWrapper.isCurrency = true;
				}
			return thisWrapper;
		}
    return null;
	}

  /**
   * @description Method to Populate attributes for Dynamic field component
   * @param fieldType Field Type
   * @param thisWrapper Dynamic Field component wrapper
   * @param fieldDescribe DescribeFieldResult for particular field
   **/
  public static void populateAttribute(
    String fieldType,
    RequiredFieldWrapper thisWrapper,
    DescribeFieldResult fieldDescribe
  ) {
    ComponentAttributes thisComponentAttributes = new ComponentAttributes();

    if (componentTypeMap.containsKey(fieldType)) {
      thisWrapper.componentType = componentTypeMap.get(fieldType);
    }

    if (inputTypeMap.containsKey(fieldType)) {
      thisComponentAttributes.type = inputTypeMap.get(fieldType);
    }

    if (inputTypeFormatterMap.containsKey(fieldType)) {
      thisComponentAttributes.formatter = inputTypeFormatterMap.get(fieldType);
    }

    if (inputTypeNumberStepsMap.containsKey(fieldType)) {
      thisComponentAttributes.step = inputTypeNumberStepsMap.get(fieldType);
    }

    thisComponentAttributes.name = thisWrapper.name;
    thisComponentAttributes.label = thisWrapper.name;
    thisComponentAttributes.variant = 'label-hidden';

    if (fieldType == 'PICKLIST') {
      populatePicklistValue(
        fieldDescribe.getPicklistValues(),
        thisComponentAttributes
      );
    }

    if (
      fieldDescribe.getLength() != null &&
      (fieldType == 'STRING' ||
      fieldType == 'TEXTAREA')
    ) {
      thisComponentAttributes.maxlength = String.valueOf(
        fieldDescribe.getLength()
      );
    }

    if (fieldType == 'BOOLEAN') {
      thisComponentAttributes.checked = false;
    }

    if (fieldDescribe.isDefaultedOnCreate()) {
      thisComponentAttributes.value = String.valueOf(
        fieldDescribe.getDefaultValue()
      );
    }
    thisWrapper.attributeObject = thisComponentAttributes;
  }

  /**
   * @description Method to construct picklist options for the Dynamic field if field type is picklist
   * @param pickEntries PicklistEntry of the Picklist field
   * @param thisReqWrapperAttribute Attribute Wrapper of Dynamic Picklist component
   **/
  private static void populatePicklistValue(
    List<Schema.PicklistEntry> pickEntries,
    ComponentAttributes thisReqWrapperAttribute
  ) {
    List<PickListWrapper> pickListWrapperList = new List<PickListWrapper>();

    for (Schema.PicklistEntry thisEntry : pickEntries) {
      if (thisEntry.isActive()) {
        pickListWrapperList.add(
          new PickListWrapper(thisEntry.getLabel(), thisEntry.getValue())
        );
      }
    }
    thisReqWrapperAttribute.options = pickListWrapperList;
  }

  public static Map<String, List<SObject>> getOrdwayPlanLineItemsRecords(String parentRecordId, String pricebookId, Map<String, List<SObject>> lineItemTiersMap, Map<String, List<SObject>> chargeTiersMap, PlanPickerController.InitialWrapper initialWrapper) {
    Map<String, List<SObject>> planMap = new Map<String, List<SObject>>();
    Set<Id> lineItemIds = new Set<Id>();

    Map<String, List<SObject>> lineItemByPlanChargeIdMap = getLineItemsByPlanChargeId(parentRecordId, lineItemIds);
    
    Map<String, ContractLineItem__c> cliByPlanChargeIdMap = new Map<String, ContractLineItem__c>();
  
    ContractPlanPickerSetting__c thisSetting = getOpportunityCustomSetting();
  
    Map<Id, SObject> parentObjectMap = getParentRecord(parentRecordId);
    String childSObjectAPIName;
    String parentRelationshipField;
    String lineItemTierAPIName;
    String lineItemTierToLineItemRelationShipName;
    
    if(parentToChildRelationshipMap.containsKey(String.valueOf(Id.valueOf(parentRecordId).getSObjectType()))){
      childSObjectAPIName = parentToChildRelationshipMap.get(String.valueOf(Id.valueOf(parentRecordId).getSObjectType()));
    }
    
    if(lineItemToChildTierRelationshipMap.containsKey(childSObjectAPIName)){
      lineItemTierAPIName = lineItemToChildTierRelationshipMap.get(childSObjectAPIName);
    }

    if(parentRelationshipFieldMap.containsKey(childSObjectAPIName)){
      parentRelationshipField = parentRelationshipFieldMap.get(childSObjectAPIName);
    }

    if(lineItemToChildTierRelationshipFieldMap.containsKey(lineItemTierAPIName)){
      lineItemTierToLineItemRelationShipName = lineItemToChildTierRelationshipFieldMap.get(lineItemTierAPIName);
      initialWrapper.lineItemTierRelationShipAPIName  = lineItemTierToLineItemRelationShipName;
    }
  
    SObjectType childSobjectType = ((SObject) Type.forName(childSObjectAPIName).newInstance()).getSObjectType();
    SObjectType lineItemTierSobjectType;
    try {
      lineItemTierSobjectType = ((SObject) Type.forName(lineItemTierAPIName).newInstance()).getSObjectType();
    }
    catch (Exception ex) {
      throw new AuraHandledException('Enable Quote Line Item Tier Object. Go to Ordway Settings -> New Features -> Enable Quote Line Item Tier');
    }

    SObject parentSObject = parentObjectMap.get(Id.valueOf(parentRecordId));
  
    Map<String, Object> fieldsToValue = parentSObject.getPopulatedFieldsAsMap();
  
    Map<String, PricebookEntry> pricebookEntryMap = PriceBookEntryController.getPricebookEntryMap(pricebookId, String.valueOf(fieldsToValue.get('CurrencyIsoCode')));
  
    String entityId = OrdwayService.getEntityId(parentRecordId);
    Set<Id> chargeIds = new Set<Id>();

    List<Plan__c> planList = getOrdwayPlanRecords(entityId);
    for(Plan__c thisOrdwayPlan : planList){
      if(thisOrdwayPlan.Charges__r.size() > 0){
        for(Charge__c thisCharge : thisOrdwayPlan.Charges__r){
          chargeIds.add(thisCharge.Id);
        }
      }
    }
  
    if(!chargeIds.isEmpty()){
      for(ChargeTier__c thisTier: Database.query('SELECT '+String.join(
        SObjectHelper.getAllFields(
          Schema.ChargeTier__c.getSObjectType()
        ),','
      ) +', Charge__r.OrdwayLabs__ChargeID__c'  +' FROM OrdwayLabs__ChargeTier__c WHERE OrdwayLabs__Charge__c IN:chargeIds ORDER BY OrdwayLabs__Tier__c ASC')){

        if(!chargeTiersMap.containskey(thisTier.Charge__r.OrdwayLabs__ChargeID__c)){
          chargeTiersMap.put(thisTier.Charge__r.OrdwayLabs__ChargeID__c, new List<SObject>{});
        }
        constructLineItemChargeTier(lineItemTierSobjectType, chargeTiersMap, thisTier);
      }
    }
   
    for (Plan__c thisOrdwayPlan : planList) {
    
      String planKey = thisOrdwayPlan.PlanId__c + '_' + thisOrdwayPlan.Name;
      
      for (Charge__c thisCharge : thisOrdwayPlan.Charges__r ) {
        
        if (!OrdwayHelper.isValidId(thisCharge.Product__c)) {
          continue;
        }

        //Only if the Salesforce Id Matches
        if (pricebookEntryMap.get(thisCharge.Product__c) != null) {
          if (!planMap.containsKey(planKey)) {
            planMap.put(planKey, new List<SObject>{});
          }
  
          //If this Charge is already present, Add Existing OLI
          if (lineItemByPlanChargeIdMap.containsKey(thisOrdwayPlan.PlanId__c +'_' +thisCharge.ChargeID__c)) {
            planMap.get(planKey).addAll(lineItemByPlanChargeIdMap.get(thisOrdwayPlan.PlanId__c +'_' +thisCharge.ChargeID__c));
          }
  
          //Load Defaults
          SObject thisLineItem = (SObject) childSobjectType.newSObject(null, true);
          
          if(childSObjectAPIName == 'OpportunityLineItem'){
            thisLineItem.put('UnitPrice', 0);
            thisLineItem.put('Quantity', 1);
            thisLineItem.put('Discount', 0);
            thisLineItem.put('PricebookEntryId', pricebookEntryMap.get(thisCharge.Product__c).Id);
            PlanPickerHelper.onLoadDefaultValues(thisLineItem);          
          }
          else{
            thisLineItem.put('OrdwayLabs__Product2Id__c', thisCharge.Product__c);
            thisLineItem.put('OrdwayLabs__PriceBookEntryId__c',pricebookEntryMap.get(thisCharge.Product__c).Id);
            thisLineItem.put('Name', thisCharge.Name.capitalize());
            thisLineItem.put('OrdwayLabs__Quantity__c', 1);
            thisLineItem.put('OrdwayLabs__Discount__c', 0);
          }
          ApplicationLog.Info('427 beforethisLineIteme' + thisLineItem);

          SObjectFieldMappingHelper.dynamicSobjectMapping(thisCharge, thisLineItem, true);
          ApplicationLog.Info('427 thisLineIteme' + thisLineItem);

  
          thisLineItem.put('OrdwayLabs__OrdwayPlanId__c', thisOrdwayPlan.PlanId__c);
          thisLineItem.put(parentRelationshipField, parentRecordId);

          if(childSObjectAPIName == 'OpportunityLineItem'){
            thisLineItem.put('UnitPrice', thisLineItem.get('OrdwayLabs__OrdwayListPrice__c'));
          }
  
          if (thisLineItem.get('OrdwayLabs__ChargeName__c') != null) {
            thisLineItem.put('OrdwayLabs__ChargeName__c', String.valueOf(thisLineItem.get('OrdwayLabs__ChargeName__c')).capitalize());
          }
  
          //If Plan is not active, then skip
          if (false == thisOrdwayPlan.Active__c) {
            planMap.remove(planKey);
            continue;
          }
          planMap.get(planKey).add(thisLineItem);
        }
      }
    }

    if(!lineItemIds.isEmpty()){
      getLineItemChargeTiers(lineItemTierSobjectType, lineItemTierAPIName, lineItemTierToLineItemRelationShipName, lineItemTiersMap, lineItemIds);
    }
    return planMap;
  }  
  
  public static void constructLineItemChargeTier(SObjectType lineItemTierSobjectType, Map<String, List<SObject>> chargeTiersMap, ChargeTier__c chargeTier) {
    SObject thisLineItemTier                            = (SObject) lineItemTierSobjectType.newSObject(null, true);
    thisLineItemTier.put('OrdwayLabs__Tier__c',  chargeTier.OrdwayLabs__Tier__c);
    thisLineItemTier.put('OrdwayLabs__StartingUnit__c', chargeTier.OrdwayLabs__StartingUnit__c);
    thisLineItemTier.put('OrdwayLabs__EndingUnit__c', chargeTier.OrdwayLabs__EndingUnit__c);
    thisLineItemTier.put('OrdwayLabs__PricingType__c', chargeTier.OrdwayLabs__PricingType__c);
    thisLineItemTier.put('OrdwayLabs__ListPrice__c', chargeTier.OrdwayLabs__ListPrice__c);
    chargeTiersMap.get(chargeTier.Charge__r.OrdwayLabs__ChargeID__c).add(thisLineItemTier);
  }

  public static void getLineItemChargeTiers(SObjectType lineItemTierSobjectType, String lineItemTierAPIName, String lineItemRelationShipName, Map<String, List<SObject>> lineItemTiersMap, Set<Id> lineItemIds) {
    String queryString =  'SELECT ' +String.join(SObjectHelper.getAllFields(lineItemTierSobjectType),',') +' FROM '+lineItemTierAPIName+' WHERE '+lineItemRelationShipName+' IN:lineItemIds ORDER BY Tier__c ASC';
    Id lineItemId;
    for(SObject thisLineItemTier : Database.query(queryString)){
      lineItemId = Id.valueOf(String.valueOf(thisLineItemTier.get(lineItemRelationShipName)));
      if(!lineItemTiersMap.containsKey(lineItemId)){
        lineItemTiersMap.put(lineItemId, new List<SObject>());
      }
      lineItemTiersMap.get(lineItemId).add(thisLineItemTier);
    }
  }

  /**
     * @description Method to get Plans From Ordway Plan Object based on Entity Id
     * @param entityId Entity Id
     * @return List<Plan__c> List of Ordway Plans
     **/
    public static List<Plan__c> getOrdwayPlanRecords(String entityId) {
  
      List<Plan__c> ordwayPlanList = new List<Plan__c>();
      String query = 'SELECT Id, Name, OrdwayLabs__PlanId__c, OrdwayLabs__Active__c, OrdwayLabs__PlanDetails__c, (SELECT '+String.join(
        SObjectHelper.getAllFields(
          Schema.Charge__c.getSObjectType()
        ),','
      ) + ' FROM Charges__r ORDER BY OrdwayLabs__ChargeID__c ASC)'
      +' FROM OrdwayLabs__Plan__c'
      +' WHERE OrdwayLabs__EntityID__c = :entityId'
      +' AND OrdwayLabs__PlanId__c != null AND OrdwayLabs__HasCharge__c > 0 ORDER BY OrdwayLabs__PlanId__c DESC';
      if (Schema.sObjectType.Plan__c.isAccessible()) {
        
        ordwayPlanList = Database.query(query);
      }
      return ordwayPlanList;
    }

    public static void populatePickListFieldLabel(DescribeFieldResult fieldDescribe, RequiredFieldWrapper thisWrapper) {
      if(fieldAPINameToPickListLabelMap.isEmpty()
        || !fieldAPINameToPickListLabelMap.containsKey(fieldDescribe.getName())){
          constructPickListFieldLabel(fieldDescribe);
        }
        thisWrapper.pickListFieldValueLabelMap = fieldAPINameToPickListLabelMap.get(fieldDescribe.getName());
    }

    public static void constructPickListFieldLabel(DescribeFieldResult fieldDescribe) {
      Map<String, String> valueToLabelMap = new Map<String, String>();
      for(Schema.PicklistEntry thisEntry : fieldDescribe.getPicklistValues()){
        valueToLabelMap.put(thisEntry.getValue().toUpperCase(), thisEntry.getLabel());
      }
      fieldAPINameToPickListLabelMap.put(fieldDescribe.getName(), valueToLabelMap);
    }

	  /**
   * @description Wrapper data for dynamic field component constructed for Add / Edit Plan page
   **/
  public class RequiredFieldWrapper {
    @AuraEnabled
    public String componentType { get; set; }
    @AuraEnabled
    public String name { get; set; }
    @AuraEnabled
    public String label { get; set; }
    @AuraEnabled
    public Boolean isCurrency { get; set; }
    @AuraEnabled
    public Boolean isReadOnlyPickListField { get; set; }
    @AuraEnabled
    public Map<String, String> pickListFieldValueLabelMap { get; set; }
    @AuraEnabled
    public ComponentAttributes attributeObject { get; set; }
  }

  /**
   * @description Wrapper data for dynamic field component attribute
   **/
  public class ComponentAttributes {
    @AuraEnabled
    public String name { get; set; }
    @AuraEnabled
    public String label { get; set; }
    @AuraEnabled
    public String variant { get; set; }
    @AuraEnabled
    public String placeholder { get; set; }
    @AuraEnabled
    public String type { get; set; }
    @AuraEnabled
    public Boolean checked { get; set; }
    @AuraEnabled
    public String maxlength { get; set; }
    @AuraEnabled
    public Boolean required { get; set; }
    @AuraEnabled
    public String value { get; set; }
    @AuraEnabled
    public String onchange { get; set; }
    @AuraEnabled
    public String formatter { get; set; }
    @AuraEnabled
    public String step { get; set; }
    @AuraEnabled
    public String style { get; set; }
    @AuraEnabled
    public String currencyDisplayAs { get; set; }
    @AuraEnabled
    public String currencyCode { get; set; }
    @AuraEnabled
    public String year { get; set; }
    @AuraEnabled
    public String month { get; set; }
    @AuraEnabled
    public String day { get; set; }
    @AuraEnabled
    public List<PickListWrapper> options { get; set; }
  }

  /**
   * @description Wrapper data for picklist field component
   **/
  public class PickListWrapper {
    @AuraEnabled
    public String label { get; set; }
    @AuraEnabled
    public String value { get; set; }
    PickListWrapper(String pickLabel, String pickValue) {
      this.label = pickLabel;
      this.value = pickValue;
    }
  }

  public class ChargeTierWrapper{
    @AuraEnabled
    public Integer tier {get;set;}
    @AuraEnabled
    public Decimal starting_unit {get;set;}
    @AuraEnabled
    public Decimal ending_unit {get;set;}
    @AuraEnabled
    public Decimal price {get;set;}
    @AuraEnabled
    public String type {get;set;}
    ChargeTierWrapper(ChargeTier__c thisTier){
      tier            = Integer.valueOf(thisTier.Tier__c);
      starting_unit   = thisTier.StartingUnit__c;
      ending_unit     = thisTier.EndingUnit__c;
      price           = thisTier.ListPrice__c;
      type            = thisTier.PricingType__c;
    }
  }

  static Map<String, String> componentTypeMap = OrdwayUtil.componentTypeMap;
  static Map<String, String> inputTypeMap = OrdwayUtil.inputTypeMap;
  static Map<String, String> inputTypeFormatterMap = OrdwayUtil.inputTypeFormatterMap;
  static Map<String, String> inputTypeNumberStepsMap = OrdwayUtil.inputTypeNumberStepsMap;

  
  public class PlanPickerMetadataWrapper{
    @AuraEnabled
    public String MasterLabel {get;set;}
    @AuraEnabled
    public Boolean Edit {get;set;}
    @AuraEnabled
    public String Field {get;set;}
    @AuraEnabled
    public Boolean Visible {get;set;}
    @AuraEnabled
    public Boolean isFormulaField {get;set;}
    @AuraEnabled
    public Boolean isNew {get;set;}
    @AuraEnabled
    public Integer ColumnOrder {get;set;}
  }
}