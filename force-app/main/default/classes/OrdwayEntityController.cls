/**
 * @description       : OrdwayEntityController.cls
 * @author            : Ordway Labs
 * @group             :
 * @last modified on  : 09-19-2020
 * @last modified by  : 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   07-22-2020      Initial Version
 **/
public class OrdwayEntityController {
  public static Set<String> REQUIRED_FIELD_TYPE = new Set<String>{
    'PICKLIST',
    'MULTIPICKLIST',
    'STRING',
    'TEXTAREA'
  };

  /**
   * @description Method to get Ordway entity Setting Initial Wrapper
   * @return ordwayEntitySettingInitialWrapper
   **/
  @AuraEnabled
  public static ordwayEntitySettingInitialWrapper getOrdwayEntitySetting() {
    ordwayEntitySettingInitialWrapper thisInitialWrapper = new ordwayEntitySettingInitialWrapper();
    thisInitialWrapper.isMultiEntityEnabled = OpportunitySettingController.isMultiEntityEnabled()
      ? true
      : OrdwayService.getEntitySetting();
    if (thisInitialWrapper.isMultiEntityEnabled) {
      thisInitialWrapper.ordwayEntities = OrdwayEntityHelper.getOrdwayEntities().values();
    }
    return thisInitialWrapper;
  }

  /**
   * @description Method to upsert Ordway entity rule records
   * @param OrdwayEntityRuleToUpsert Ordway entity rule to upsert
   * @param OrdwayEntityRuleToDelete Ordway entity rule to be deleted
   **/
  @AuraEnabled
  public static void saveOrdwayEntityRuleSetting(
    String OrdwayEntityRuleToUpsert,
    String OrdwayEntityRuleToDelete
  ) {
    if (!OpportunitySettingController.isMultiEntityEnabled()) {
      //Enable Entity In Salesforce
      ContractPlanPickerSetting__c thisSetting = new ContractPlanPickerSetting__c();
      thisSetting.Name = OrdwayHelper.OPPORTUNITY_SETTING_NAME;
      thisSetting.EnableMultiEntity__c = true;
      upsert thisSetting Name;
    }

    List<OrdwayEntityRule__c> ordwayEntityRuleSettingToDelete = OrdwayEntityRule__c.getAll().values();
    List<OrdwayEntityRule__c> ordwayEntityRuleSettingToUpsert;

    if (!String.isEmpty(OrdwayEntityRuleToUpsert)) {
      ordwayEntityRuleSettingToUpsert = (List<OrdwayEntityRule__c>) JSON.deserialize(
        OrdwayEntityRuleToUpsert,
        List<OrdwayEntityRule__c>.class
      );
    }

    try {
      if (
        Schema.sObjectType.OrdwayEntityRule__c.isDeletable() &&
        ordwayEntityRuleSettingToDelete != null &&
        ordwayEntityRuleSettingToDelete.size() > 0
      ) {
        delete ordwayEntityRuleSettingToDelete;
      }

      if (
        ordwayEntityRuleSettingToUpsert != null &&
        ordwayEntityRuleSettingToUpsert.size() > 0
      ) {
        upsert ordwayEntityRuleSettingToUpsert;
      }
    } catch (Exception thisException) {
      throw new AuraHandledException(thisException.getMessage());
    }
  }

  /**
   * @description Method to save Ordway Entity records
   * @param OrdwayEntityToUpsert Ordway entity to upsert
   * @param OrdwayEntityToDelete Ordway entity to delete
   **/
  @AuraEnabled
  public static void saveOrdwayEntitySetting(
    String OrdwayEntityToUpsert,
    String OrdwayEntityToDelete
  ) {
    List<Entity__c> OrdwayEntitySettingToDelete;
    List<Entity__c> OrdwayEntitySettingToUpsert;

    if (!String.isEmpty(OrdwayEntityToDelete)) {
      OrdwayEntitySettingToDelete = (List<Entity__c>) JSON.deserialize(
        OrdwayEntityToDelete,
        List<Entity__c>.class
      );
    }

    if (!String.isEmpty(OrdwayEntityToUpsert)) {
      OrdwayEntitySettingToUpsert = (List<Entity__c>) JSON.deserialize(
        OrdwayEntityToUpsert,
        List<Entity__c>.class
      );
    }

    try {
      if (
        Schema.sObjectType.Entity__c.isDeletable() &&
        OrdwayEntitySettingToDelete != null &&
        !OrdwayEntitySettingToDelete.isEmpty()
      ) {
        delete OrdwayEntitySettingToDelete;
      }

      if (
        OrdwayEntitySettingToUpsert != null &&
        !OrdwayEntitySettingToUpsert.isEmpty()
      ) {
        SObjectAccessDecision decision = Security.stripInaccessible(
          AccessType.UPSERTABLE,
          OrdwayEntitySettingToUpsert
        );
        upsert (List<Entity__c>) decision.getRecords();
      }
    } catch (Exception thisException) {
      throw new AuraHandledException(thisException.getMessage());
    }
  }

  /**
   * @description Method to get Ordway Entity Rule Configuration Initial Wrapper
   * @return InitialWrapper Ordway Entity Rule Configuration Initial Wrapper
   **/
  @AuraEnabled
  public static InitialWrapper getOrdwayEntityRuleConfigurationInitialWrapper() {
    InitialWrapper thisInitialWrapper = new InitialWrapper();

    thisInitialWrapper.ordwayEntities = new List<PickListWrapper>();
    populateOrdwayEntitiesMap(thisInitialWrapper.ordwayEntities);
    if (
      thisInitialWrapper.ordwayEntities != null &&
      !thisInitialWrapper.ordwayEntities.isEmpty()
    ) {
      thisInitialWrapper.ordwayEntityRule = OrdwayEntityHelper.getEntityRule();
      thisInitialWrapper.opportunityFieldMap = new List<PickListWrapper>();
      getSobjectFieldsWrapper(
        thisInitialWrapper.opportunityFieldMap,
        Schema.Opportunity.getSObjectType()
      );
      thisInitialWrapper.accountFieldMap = new List<PickListWrapper>();
      getSobjectFieldsWrapper(
        thisInitialWrapper.accountFieldMap,
        Schema.Account.getSObjectType()
      );
    }
    return thisInitialWrapper;
  }

  /**
   * @description Method to construct Ordway Entity Options
   * @param ordwayEntitiesWrapper Ordway Entity Options
   **/
  public static void populateOrdwayEntitiesMap(
    List<PickListWrapper> ordwayEntitiesWrapper
  ) {
    for (Entity__c thisOrdwayEntity : OrdwayEntityHelper.getOrdwayEntities().values()) {
      ordwayEntitiesWrapper.add(
        new PickListWrapper(thisOrdwayEntity.Name, thisOrdwayEntity.EntityID__c)
      );
    }
  }

  /**
   * @description Method to construct Object Fields Options for specified Object
   * @param thisSObjectFieldWrapper Object Fields Options Wrapper
   * @param thisSObjectType Sobject Type whose fields need to be retrieved
   **/
  public static void getSobjectFieldsWrapper(
    List<PickListWrapper> thisSObjectFieldWrapper,
    Schema.SObjectType thisSObjectType
  ) {
    for (
      Schema.SObjectField thisField : SObjectHelper.getFieldsMap(
          thisSObjectType
        )
        .values()
    ) {
      Schema.DescribeFieldResult thisFieldDescribe = thisField.getDescribe();
      if (
        REQUIRED_FIELD_TYPE.contains(
          String.valueOf(thisFieldDescribe.getType())
        )
      ) {
        thisSObjectFieldWrapper.add(
          new PickListWrapper(
            thisFieldDescribe.getLabel(),
            thisFieldDescribe.getName()
          )
        );
      }
    }
  }

  /**
   * Ordway entity Setting Initial Wrapper Class
   **/
  public class ordwayEntitySettingInitialWrapper {
    @AuraEnabled
    public Boolean isMultiEntityEnabled;
    @AuraEnabled
    public List<Entity__c> ordwayEntities;
  }

  /**
   * Initial Wrapper class
   **/
  public class InitialWrapper {
    @AuraEnabled
    public List<PickListWrapper> ordwayEntities;
    @AuraEnabled
    public List<OrdwayEntityRule__c> ordwayEntityRule;
    @AuraEnabled
    public List<PickListWrapper> opportunityFieldMap;
    @AuraEnabled
    public List<PickListWrapper> accountFieldMap;
  }

  /**
   * Picklist options Wrapper class
   **/
  public class PickListWrapper {
    @AuraEnabled
    public String label { get; set; }
    @AuraEnabled
    public String value { get; set; }
    PickListWrapper(String pickLabel, String pickValue) {
      this.label = pickLabel;
      this.value = pickValue;
    }
  }
}