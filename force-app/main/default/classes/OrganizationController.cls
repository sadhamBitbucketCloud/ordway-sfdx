/**
 * @File Name          : OrganizationController.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    9/29/2019   Ordway Labs     Initial Version
 **/
public with sharing class OrganizationController {
  /**
   * @description Method to Check whether the instance is Sandbox or Production
   * @return  true or false
   **/
  public static boolean isSandbox() {
    Organization thisOrganization;
    if (Schema.sObjectType.Organization.isAccessible()) {
      thisOrganization = [SELECT IsSandbox FROM Organization LIMIT 1];
    }
    return thisOrganization.IsSandbox;
  }

  /**
   * @description Method to Get the Organization Name
   * @return  true or false
   **/
  public static string getOrganizationName() {
    Organization thisOrganization;
    if (Schema.sObjectType.Organization.isAccessible()) {
      thisOrganization = [SELECT Name FROM Organization LIMIT 1];
    }
    return thisOrganization.Name;
  }

  /**
   * @description Method to Get the Organization Id
   * @return  true or false
   **/
  public static string getOrganizationId() {
    Organization thisOrganization;
    if (Schema.sObjectType.Organization.isAccessible()) {
      thisOrganization = [SELECT Id, Name FROM Organization LIMIT 1];
    }
    return thisOrganization.Id;
  }
}