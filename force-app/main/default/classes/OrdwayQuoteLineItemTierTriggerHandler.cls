public with sharing class OrdwayQuoteLineItemTierTriggerHandler extends TriggerHandler.TriggerHandlerBase {

	public static Boolean deleteTier = true;

	public override void beforeInsert(List<SObject> listNewSObjs) {

		if(deleteTier){
			Set<Id> QuoteLineItemIds = new Set<Id>();

			for(OrdwayLabs__QuoteLineItemTier__c thisQuoteLineItemTier : (List<OrdwayLabs__QuoteLineItemTier__c>) listNewSObjs){
				if(thisQuoteLineItemTier.OrdwayLabs__QuoteLineItem__c != null){
					QuoteLineItemIds.add(thisQuoteLineItemTier.OrdwayLabs__QuoteLineItem__c);
				}
				
				if(!QuoteLineItemIds.isEmpty()){
					TierController.deleteAllRelatedTiers(QuoteLineItemIds, 'OrdwayLabs__QuoteLineItemTier__c', 'OrdwayLabs__QuoteLineItem__c');
				}
			}
		}

	}

	public override void beforeDelete(Map<Id, SObject> mapOldSObjs) {
		for(OrdwayLabs__QuoteLineItemTier__c thisQuoteLineItemTier : (List<OrdwayLabs__QuoteLineItemTier__c>) mapOldSObjs.values()){
			if(TierController.validateOnDelete){
				TierController.validateBeforeTierDelete(thisQuoteLineItemTier);
			}
		}
	}
}