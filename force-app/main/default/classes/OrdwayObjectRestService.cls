/**
 * @File Name          : OrdwayObjectRestService.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 12-27-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/8/2020        Initial Version
 **/

@RestResource(urlMapping='/oobject/*')
global with sharing class OrdwayObjectRestService {
  @HttpPut
  global static String doPut() {

    RestResponse response = RestContext.response;

    try {
      ApplicationLog.StartLoggingContext( 'OrdwayObjectRestService', 'REMOTE CALLIN => ' + RestContext.request.requestURI );

      //Turning Triggers Off, so it won't make a callout back to Ordway
      OrdwayContractTriggerHelper.runOnce = false;
      QuoteTriggerHelper_Callout.runOnce = false;
      ContactTriggerHandler.runOnce = false;
      AccountTriggerHandler.runOnce = false;

      OrdwayHelper.SOURCE thisSource;

      Map<String, Object> responseMap = new Map<String, Object>();

      String requestObjectString = (RestContext.request.requestURI.substring( RestContext.request.requestURI.lastIndexOf('/') + 1 )).substringBefore('__c');
      responseMap = (Map<String, Object>) JSON.deserializeUntyped(RestContext.request.requestBody.toString());

      String source = (String) responseMap.get('source');
      String xEntitiyId = responseMap.containsKey('X-Entity-Id') ? (String) responseMap.get('X-Entity-Id') : null;
      
      thisSource = source == 'sf' ? OrdwayHelper.SOURCE.SALESFORCE : OrdwayHelper.SOURCE.ORDWAY;

      ApplicationLog.Info('Event Source => ' + source + '; xEntitiyId => ' + xEntitiyId);
      ApplicationLog.Info( JSON.serializePretty(JSON.deserializeUntyped(RestContext.request.requestBody.toString())));

      //Remove all objects except result
      for(String key : responseMap.keySet()) {
        if(key != 'result') {
          responseMap.remove(key);
        }
      }

      if (RestContext.request.requestURI.contains('Customer__c')) {
        OrdwayCustomerController.manageOrdwayAccounts(responseMap);
      } else if ( MetadataService.dynamicObjectMappingAPINames.contains(requestObjectString)) {
        
        OrdwayObjectSyncHelper.processResponse(requestObjectString, responseMap, thisSource, OrdwayHelper.ACTION.WEBHOOK, xEntitiyId);
      }

      ApplicationLog.FinishLoggingContext();

      response.addHeader('Content-Type', 'application/json'); 
      response.statusCode = 200;
      response.responseBody = Blob.valueOf('Record Processed Successfully');
      return JSON.serialize(response);
    }
    catch(Exception ex) {
      response.addHeader('Content-Type', 'application/json'); 
      response.statusCode = 400;
      response.responseBody = Blob.valueOf(ex.getMessage());
      return JSON.serialize(response);
    }
  }

  @HttpDelete
  global static String doDelete() {

    return '';
  }
}