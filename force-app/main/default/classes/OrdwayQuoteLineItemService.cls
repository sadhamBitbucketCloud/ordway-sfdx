/**
 * @File Name          : OrdwayQuoteLineItemService.cls
 * @Description        : Ordway QuoteLine Item Service
 * @Author             : 
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-17-2021
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    5/15/2020        Initial Version
 **/
public class OrdwayQuoteLineItemService {
  /**
   * @description Method to Sync List of Quote Line Item to respective OLI
   * @param quoteIds  Set of Quote Ids whose Quotelineitem need to be synced to OLI
   * @param opportunityIds  Set of Opportunity Ids whose Opportunitylineitem has to be synced with respective QuoteLineItem
   */
  public static void syncQuoteLineItemsToOpportunityLineItems( Set<Id> quoteIds, Set<Id> opportunityIds ) {

    ApplicationLog.StartLoggingContext( 'OrdwayQuoteLineItemService', 'syncQuoteLineItemsToOpportunityLineItems()' );

    Map<Id, QuoteLineItem__c> quoteLineItemMap = getQuoteLineItemMapByQuoteIds(
      quoteIds
    );

    Map<Id, OpportunityLineItem> opportunityLineItemMap = OpportunityLineItemService.getOpportunityLineItemMap(
      opportunityIds
    );

    //Synced OpportunityLineItem Records which needs to be updated
    List<OpportunityLineItem> syncedOpportunityLineItemToUpdate = new List<OpportunityLineItem>();
    //QLI Id => New OpportunitylineItem used to Update OLI in In respective QLI
    Map<Id, OpportunityLineItem> syncedOpportunityLineItemMapToInsert = new Map<Id, OpportunityLineItem>();

    List<QuoteLineItem__c> QLIList = new List<QuoteLineItem__c>();
    OpportunityLineItem thisOpportunityLineItemToSync;

    for (QuoteLineItem__c thisQuoteLineItem : quoteLineItemMap.values()) {
      //Sync QLI to OLI if relate OLI is present
      if (
        thisQuoteLineItem.OpportunityLineItemId__c != null &&
        !opportunityLineItemMap.isEmpty() &&
        opportunityLineItemMap.containsKey( Id.valueOf(thisQuoteLineItem.OpportunityLineItemId__c) )
      ) {
        thisOpportunityLineItemToSync = opportunityLineItemMap.get(
          Id.valueOf(thisQuoteLineItem.OpportunityLineItemId__c)
        );
        populateQuoteLineItemValuesToOpportunityLineItem(
          thisQuoteLineItem,
          thisOpportunityLineItemToSync
        );
        syncedOpportunityLineItemToUpdate.add(thisOpportunityLineItemToSync);
        QLIList.add(thisQuoteLineItem);
      }

      //Create New OLI for QLI if relate OLI is not present
      if (thisQuoteLineItem.OpportunityLineItemId__c == null) {
        syncedOpportunityLineItemMapToInsert.put(
          thisQuoteLineItem.Id,
          createOpportunityLineItemFromQLI(
            thisQuoteLineItem,
            thisQuoteLineItem.Quote__r.Opportunity__c
          )
        );
      }
      //Remove Opportunity Line Item from the Map so that at the end the remaining unrelate OLI can be deleted
      if (thisQuoteLineItem.OpportunityLineItemId__c != null) {
        opportunityLineItemMap.remove(
          Id.valueOf(thisQuoteLineItem.OpportunityLineItemId__c)
        );
      }
    }

    try {
      if (!syncedOpportunityLineItemToUpdate.isEmpty()) {
        SObjectAccessDecision securityDecision = Security.stripInaccessible(
          AccessType.UPDATABLE,
          syncedOpportunityLineItemToUpdate
        );
        update securityDecision.getRecords();
      }

      if (!syncedOpportunityLineItemMapToInsert.isEmpty() && Schema.sObjectType.OpportunityLineItem.isCreateable() ) {
        //Note: Do Not Use Security InAccessible, Issue with Pass By Reference
        insert syncedOpportunityLineItemMapToInsert.values();
        populateOpportunityLineItemIdInQLI(
          syncedOpportunityLineItemMapToInsert,
          quoteLineItemMap,
          QLIList
        );
      }

      TierController.addOLITiers(quoteLineItemMap.values());

      if (Schema.sObjectType.OpportunityLineItem.isDeletable()) {
        delete opportunityLineItemMap.values();
      }
    } catch (Exception e) {
      ApplicationLog.Error(e);
      ApplicationLog.FinishLoggingContext();
    }
  }

  /**
   * @description Method to update new OpportunitylineItem In respective QLI
   * @param syncedOpportunityLineItemMapToInsert QLI Id to new OLI record
   * @param quoteLineItemMap  Map of QLI records
   */
  public static void populateOpportunityLineItemIdInQLI(
    Map<Id, OpportunityLineItem> syncedOpportunityLineItemMapToInsert,
    Map<Id, QuoteLineItem__c> quoteLineItemMap,
    List<QuoteLineItem__c> QLIList
  ) {
    ApplicationLog.StartLoggingContext(
      'OrdwayQuoteLineItemService',
      'populateOpportunityLineItemIdInQLI()'
    );
    List<QuoteLineItem__c> quoteLineItemListToUpdate = new List<QuoteLineItem__c>();

    for (Id thisQLIId : syncedOpportunityLineItemMapToInsert.keySet()) {
      QuoteLineItem__c thisQLIToUpdate = quoteLineItemMap.get(thisQLIId);
      thisQLIToUpdate.OpportunityLineItemId__c = String.valueOf( syncedOpportunityLineItemMapToInsert.get(thisQLIId).Id );
      quoteLineItemListToUpdate.add(thisQLIToUpdate);
    }

    try {
      SObjectAccessDecision securityDecision = Security.stripInaccessible(
        AccessType.UPDATABLE,
        quoteLineItemListToUpdate
      );
      update securityDecision.getRecords();
      quoteLineItemListToUpdate.addAll(QLIList);
    } catch (Exception e) {
      ApplicationLog.Error(e);
      ApplicationLog.FinishLoggingContext();
    }
  }

  /**
   * @description  Create Quote Line Item for newly inserted Quote
   * @param opportunityIdToQuoteIdMap Map of Opportunity Id to Quote Id for which Quoteline Item needs to be created from respective Opportunities OLI
   */
  public static void createQLIForNewQuote(
    Map<Id, Id> opportunityIdToQuoteIdMap
  ) {
    ApplicationLog.StartLoggingContext(
      'OrdwayQuoteLineItemService',
      'createQLIForNewQuote()'
    );
    List<QuoteLineItem__c> quoteLineItemListToInsert = new List<QuoteLineItem__c>();
    Map<Id, OpportunityLineItem>  oLIMap = OpportunityService.getOpportunityLineItemMap(opportunityIdToQuoteIdMap.keySet());

    for (
      OpportunityLineItem thisOpportunityLineItem : oLIMap.values()) {
      quoteLineItemListToInsert.add(
        createQuoteLineItemFromOLI(
          thisOpportunityLineItem,
          opportunityIdToQuoteIdMap.get(thisOpportunityLineItem.OpportunityId)
        )
      );
    }

    try {
       if (!quoteLineItemListToInsert.isEmpty() && Schema.sObjectType.QuoteLineItem__c.isCreateable()) {
        insert quoteLineItemListToInsert;
        TierController.addQLITiers(quoteLineItemListToInsert);
      }
    } catch (Exception e) {
      ApplicationLog.Error(e);
      ApplicationLog.FinishLoggingContext();
    }
  }

  /**
   * @description  Delete Quote Line Item When Price Book is changed
   * @param Set<Id> Set of quoteLineItemIds To Delete
   **/
  public static void deleteQuoteLineItem(Set<Id> quoteLineItemIdsToDelete) {
    //False Positive - As Quote Line Item is Protected, CheckMarx scanner will flag this as an issue
    List<QuoteLineItem__c> qList = new List<QuoteLineItem__c>([SELECT Id FROM QuoteLineItem__c WHERE Quote__c IN :quoteLineItemIdsToDelete]);
    if ( QuoteLineItem__c.sObjectType.getDescribe().isDeletable() ) {
      delete qList;
    }
  }

  /**
   * @description Create new Quote Line Item from OLI and dynamically populate OLI values to new QLI
   * @param ordwayQuoteId Ordway Quote Id for which QLI needs to be created
   * @param thisOpportunityLineItem  OpportunityLineItem record whose values needs to be populated to Quote
   * @return QuoteLineItem__c New QLI created from OLI
   */
  public static QuoteLineItem__c createQuoteLineItemFromOLI(
    OpportunityLineItem thisOpportunityLineItem,
    Id ordwayQuoteId
  ) {
    QuoteLineItem__c thisQuoteLineItem = new QuoteLineItem__c();

    thisQuoteLineItem.put('OrdwayLabs__Quote__c', ordwayQuoteId);
    thisQuoteLineItem.put('OrdwayLabs__OpportunityLineItemId__c', String.valueOf(thisOpportunityLineItem.Id));
    thisQuoteLineItem.put('OrdwayLabs__PriceBookEntryId__c', String.valueOf(thisOpportunityLineItem.PricebookEntryId) );
    populateOpportunityLineItemValuesToQuoteLineItem( thisQuoteLineItem, thisOpportunityLineItem );
    return thisQuoteLineItem;
  }

  /**
   * @description  Create new OpportunityLineItem from QLI and dynamically populate QLI values to new OLI
   * @param opportunityId  Opportunity record Id for which OLI needs to be created
   * @param thisQuoteLineItem Quote LineItem record whose values needs to be populated to OLI
   * @return OpportunityLineItem New OLI created from QLI
   */
  public static OpportunityLineItem createOpportunityLineItemFromQLI(
    QuoteLineItem__c thisQuoteLineItem,
    Id opportunityId
  ) {
    OpportunityLineItem thisOpportunityLineItem = new OpportunityLineItem();
    thisOpportunityLineItem.OpportunityId = opportunityId;
    thisOpportunityLineItem.PricebookEntryId = Id.valueOf(
      thisQuoteLineItem.PriceBookEntryId__c
    );
    populateQuoteLineItemValuesToOpportunityLineItem(
      thisQuoteLineItem,
      thisOpportunityLineItem
    );

    //Unit Price is a key datapoint in order to create OLI successfully, so if it's not present in mapping, then this needs to be set here
    if(thisOpportunityLineItem.UnitPrice == null
    && thisQuoteLineItem.OrdwayListPrice__c != null) {
      thisOpportunityLineItem.UnitPrice = thisQuoteLineItem.OrdwayListPrice__c;
    }
    return thisOpportunityLineItem;
  }

  /**
   * @description Method to Dynamically populate OLI values to QLI
   * @param thisQuoteLineItem Ordway QuoteLineItem which needs to be updated with QuoteLineItem values
   * @param thisOpportunityLineItem OpportunityLineItem whose values needs to be populated to QuoteLineItem
   */
  public static void populateOpportunityLineItemValuesToQuoteLineItem(
    QuoteLineItem__c thisQuoteLineItem,
    OpportunityLineItem thisOpportunityLineItem
  ) {
    thisQuoteLineItem = (QuoteLineItem__c) SObjectFieldMappingHelper.dynamicSobjectMapping(
      thisOpportunityLineItem,
      thisQuoteLineItem,
      true
    );
  }

  /**
   * @description Method to Dynamically populate QLI values to OLI
   * @param thisQuoteLineItem Ordway QuoteLineItem whose values needs to be populated to OpportunityLineItem
   * @param thisOpportunityLineItem OpportunityLineItem record which needs to be updated with QuoteLineItem values
   */
  public static void populateQuoteLineItemValuesToOpportunityLineItem(
    QuoteLineItem__c thisQuoteLineItem,
    OpportunityLineItem thisOpportunityLineItem
  ) {
    thisOpportunityLineItem = (OpportunityLineItem) SObjectFieldMappingHelper.dynamicSobjectMapping(
      thisQuoteLineItem,
      thisOpportunityLineItem,
      true
    );
  }

  /**
   * getQuoteLineItemMapByQuoteIds  Method to get QuotelineItem by Quotes Ids
   * @param  quoteIds Set of Quote Id whose QuotelineItem needs to be retrieved
   * @return Map of QuoteLineItem__c
   */
  public static Map<Id, QuoteLineItem__c> getQuoteLineItemMapByQuoteIds(
    Set<Id> quoteIds
  ) {
    Map<Id, QuoteLineItem__c> quoteLineItemMap = new Map<Id, QuoteLineItem__c>();

    if (Schema.sObjectType.QuoteLineItem__c.isAccessible()) {
      quoteLineItemMap = new Map<Id, QuoteLineItem__c>(
        (List<QuoteLineItem__c>) Database.query(
          'SELECT ' +
          String.join(
            SObjectHelper.getAllFields(
              Schema.QuoteLineItem__c.getSObjectType()
            ),
            ','
          ) +
          ',OrdwayLabs__Quote__r.OrdwayLabs__Opportunity__c FROM OrdwayLabs__QuoteLineItem__c WHERE OrdwayLabs__Quote__c IN: quoteIds'
        )
      );
    }
    return quoteLineItemMap;
  }

  // QuoteLineItem Id => OrdwayLabs__QuoteLineItemTier__c
  public static Map<Id, List<SObject>> getOrdwayQuoteLineItemTiers(Set<Id> quoteLineItemIds){
    Map<Id, List<SObject>> quoteLineItemTierMap = new Map<Id, List<OrdwayLabs__QuoteLineItemTier__c>>();

    for(OrdwayLabs__QuoteLineItemTier__c thisTier :  Database.query('SELECT ' +String.join(SObjectHelper.getAllFields(Schema.OrdwayLabs__QuoteLineItemTier__c.getSObjectType()),',') +
                                ' FROM OrdwayLabs__QuoteLineItemTier__c WHERE OrdwayLabs__QuoteLineItem__c IN:quoteLineItemIds ORDER BY Tier__c ASC ')){
      if(!quoteLineItemTierMap.containsKey(thisTier.OrdwayLabs__QuoteLineItem__c)){
        quoteLineItemTierMap.put(thisTier.OrdwayLabs__QuoteLineItem__c, new List<OrdwayLabs__QuoteLineItemTier__c>());
      }
      quoteLineItemTierMap.get(thisTier.OrdwayLabs__QuoteLineItem__c).add(thisTier);
    }
    return quoteLineItemTierMap;
  }
}