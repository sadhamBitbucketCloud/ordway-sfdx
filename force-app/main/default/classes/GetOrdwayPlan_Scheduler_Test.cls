/**
 * @File Name          : GetOrdwayPlan_Scheduler_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    4/25/2020        Initial Version
 **/
@isTest
private class GetOrdwayPlan_Scheduler_Test {
  @isTest
  private static void getOrdwayPlanSchedulerTest() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;
    Test.startTest();
    SchedulableContext testsc = null;
    Test.setMock(HttpCalloutMock.class, new MockAddPlan_Test(null));
    GetOrdwayPlan_Scheduler getPlanScheduler = new GetOrdwayPlan_Scheduler();
    getPlanScheduler.execute(testsc);
    System.assertEquals(null, testsc, 'Should return null');
    Test.stopTest();
  }
}