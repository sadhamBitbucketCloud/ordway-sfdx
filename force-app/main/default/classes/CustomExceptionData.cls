/**
 * @File Name          : CustomExceptionData.cls
 * @Description        : Custom Exception Handler
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    9/29/2019   Ordway Labs     Initial Version
 **/
public with sharing class CustomExceptionData {
  public String name;
  public String message;
  public Integer code;

  /**
   * @description Method to create custom exception message for Exception thrown
   * @param name Exception Type
   * @param message Exception Message
   * @param code Exception Code
   **/
  public CustomExceptionData(String name, String message, Integer code) {
    this.name = name;
    this.message = message;
    this.code = code;
  }

  /**
   * @description Method to create custom exception message for Exception thrown
   * @param name Exception Type
   * @param message Exception Message
   **/
  public CustomExceptionData(String name, String message) {
    this.name = name;
    this.message = message;
  }
}