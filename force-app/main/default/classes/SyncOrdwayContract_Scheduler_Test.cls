/**
 * @File Name          : syncOrdwayContract_Scheduler_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    31/01/2020   Ordway Labs     Initial Version
 **/

@isTest
private class SyncOrdwayContract_Scheduler_Test {
  @isTest
  static void test_SyncOrdwayContractScheduler() {
    Test.startTest();
    SchedulableContext testsc = null;
    SyncOrdwayContract_Scheduler testsyncBatch = new SyncOrdwayContract_Scheduler();
    testsyncBatch.execute(testsc);
    System.assertEquals(null, testsc, 'Should return null');
    Test.stopTest();
  }
}