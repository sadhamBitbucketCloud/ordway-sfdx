/**
 * @File Name          : OrdwayObjectController.cls
 * @Description        :
 * @Author             : 
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 12-26-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/9/2020        Initial Version
 **/

public with sharing class OrdwayObjectController {
  public static Integer getOrdwayObjects(
    Integer pageNumber,
    string tableName,
    String qString,
    String xEntityId
  ) {
    Map<String, Object> responseMap = new Map<String, Object>();
    Integer totalCount = 0;

    HttpResponse thisResponse = OrdwayService.getOrdwayObjects(
      tableName,
      pageNumber,
      qString,
      xEntityId
    );

    if (thisResponse != null && thisResponse.getStatusCode() == 200) {
      responseMap = (Map<String, Object>) JSON.deserializeUntyped(
        thisResponse.getBody()
      );

      if (responseMap.containsKey('total_count')) {
        totalCount = (Integer) responseMap.get('total_count');
      }

      //Remove all objects except result
      for(String key : responseMap.keySet()) {
        if(key != 'result') {
          responseMap.remove(key);
        }
      }
      
      if (tableName == 'Account') {
        OrdwayCustomerController.manageOrdwayAccounts(responseMap);
      }
      else {
        OrdwayObjectSyncHelper.processResponse(
          tableName,
          responseMap,
          OrdwayHelper.SOURCE.SALESFORCE,
          OrdwayHelper.ACTION.BULKPROCESS,
          xEntityId
        );
      }
    } else {
      return 0;
    }
    List<Object> newList = new List<Object>();
    if(responseMap != null && responseMap.get('result') != null){
      return getNextPageNumber(
        pageNumber,
        totalCount,
        ((List<Object>) responseMap.get('result')).size()
      );
    }
    return 0;
  }

  private static Integer getNextPageNumber(
    Integer pageNumber,
    Integer totalCount,
    Integer resultSize
  ) {
    //By default Ordway returns 20 records in a page,
    //So if totalcount is less than default size, return 0, which won't make further call
    if (totalCount > 20 && resultSize != 0) {
      pageNumber = pageNumber + 1;
      return pageNumber;
    }
    return 0;
  }
}