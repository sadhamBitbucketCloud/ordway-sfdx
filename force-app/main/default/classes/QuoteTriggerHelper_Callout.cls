/**
 * @File Name          : QuoteTriggerHelper_Callout.cls
 * @Description        : Quote Trigger Callout Helper
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/3/2020        Initial Version
 **/

public class QuoteTriggerHelper_Callout extends TriggerHandler.TriggerHandlerBase {
  //To recursively execute the code in the same transaction, set this variable to true
  //Set it to false on after update
  public static Boolean runOnce = true;

  public override void beforeUpdate(
    Map<Id, SObject> mapOldSObjs,
    Map<Id, SObject> mapNewSObjs
  ) {
    //Won't Support Bulkification, So Take Only The First Record
    Quote__c thisQuote = (Quote__c) mapNewSObjs.values()[0];

    if (canSync(thisQuote)) {
      thisQuote.SyncStatus__c = OrdwayHelper.SYNC_STATE_SYNC_IN_PROGRESS;
    }
  }

  public override void afterUpdate(
    Map<Id, SObject> mapOldSObjs,
    Map<Id, SObject> mapNewSObjs
  ) {
    //Won't Support Bulkification, So Take Only The First Record
    if (canSync((Quote__c) mapNewSObjs.values()[0])) {
      OrdwayQuoteController.aSyncToOrdway(
        new List<Id>(mapNewSObjs.keySet())[0]
      );
    }

    runOnce = false;
  }

  private static Boolean canSync(Quote__c thisQuote) {
    if (
      (thisQuote.Status__c == OrdwayHelper.ORDWAY_QUOTE_DRAFT ||
      thisQuote.Status__c == OrdwayHelper.ORDWAY_QUOTE_NOTLINKED ||
      thisQuote.Status__c == OrdwayHelper.ORDWAY_QUOTE_OUTFORACCEPTANCE) &&
      thisQuote.SyncStatus__c != OrdwayHelper.SYNC_STATE_DO_NOT_SEND &&
      thisQuote.TotalQuantity__c != 0 &&
      thisQuote.Account__c != null
    ) {
      return true;
    }
    return false;
  }
}