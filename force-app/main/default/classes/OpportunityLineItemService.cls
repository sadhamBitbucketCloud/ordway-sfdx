/**
 * @File Name          : OpportunityLineItemService.cls
 * @Description        : OpportunityLineItem Service Class
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    27/10/2019   Ordway Labs     Initial Version
 **/

public with sharing class OpportunityLineItemService {
  /**
   * @description Method to get OpportunitylineItem
   * @param recordIds Set of opportunity Id's
   * @return Map<Id, OpportunityLineItem> Map of OpportunitylineItem
   **/
  public static Map<Id, OpportunityLineItem> getOpportunityLineItemMap(
    Set<Id> recordIds
  ) {
    Map<Id, OpportunityLineItem> oliMap = new Map<Id, OpportunityLineItem>();
    if (Schema.sObjectType.OpportunityLineItem.isAccessible()) {
      oliMap = new Map<Id, OpportunityLineItem>(
        (List<OpportunityLineItem>) Database.query(
          'SELECT ' +
          String.join(
            SObjectHelper.getAllFields(
              Schema.OpportunityLineItem.getSObjectType()
            ),
            ','
          ) +
          ',Opportunity.StageName,Opportunity.OrdwayLabs__OrdwayOpportunityType__c,Opportunity.OrdwayLabs__OrdwayContractStatus__c,Opportunity.OrdwayLabs__OrdwayContract__c FROM OpportunityLineItem WHERE Id IN: recordIds OR OpportunityId IN: recordIds'
        )
      );
    }
    return oliMap;
  }

  /**
   * @description Method to check wheather ordway information are updated
   * @param thisOpportunityLineItem New OpportunityLineItem
   * @param OldOpportunityLineItem Old OpportunityLineItem
   * @return Boolean true or false
   **/
  public static Boolean isOrdwayInformationChanged(
    SObject thisOpportunityLineItem,
    SObject OldOpportunityLineItem
  ) {
    Boolean isChanged = false;
    if (
      MetadataService.fieldMappingMetadataMap != null &&
      MetadataService.fieldMappingMetadataMap.containsKey(
        'OpportunityLineItem_ContractLineItem'
      ) &&
      MetadataService.fieldMappingMetadataMap.get(
          'OpportunityLineItem_ContractLineItem'
        )
        .FieldMappingJSON__c != null
    ) {
      for (
        OrdwayFieldMappingParser.MappingParser thisMapping : OrdwayFieldMappingParser.parse(
          MetadataService.fieldMappingMetadataMap.get(
              'OpportunityLineItem_ContractLineItem'
            )
            .FieldMappingJSON__c
        )
      ) {
        if (
          thisOpportunityLineItem.get(thisMapping.sourceFieldAPIName) !=
          OldOpportunityLineItem.get(thisMapping.sourceFieldAPIName)
        ) {
          isChanged = true;
          break;
        }
      }
    }
    return isChanged;
  }

  /**
   * @description Method to get synchronizable OpportunitylineItem
   * @param opportunityLineItemIds Set of opportunity Id's
   * @return Map<Id, OpportunityLineItem> Map of ContractId to OpportunitylineItem
   **/
  public static Map<Id, OpportunityLineItem> getSynchronizableOpportunityLineItemMap(
    Set<Id> opportunityLineItemIds
  ) {
    Map<Id, OpportunityLineItem> opportunityLineItemMap = new Map<Id, OpportunityLineItem>();

    for (
      OpportunityLineItem thisOpportunityLineItem : getOpportunityLineItemMap(
          opportunityLineItemIds
        )
        .values()
    ) {
      if (
        OpportunityOrdwayContractSyncController.isSynchronizable(
          thisOpportunityLineItem.Opportunity.StageName,
          thisOpportunityLineItem.Opportunity.OrdwayOpportunityType__c,
          thisOpportunityLineItem.Opportunity.OrdwayContractStatus__c
        )
      ) {
        opportunityLineItemMap.put(
          thisOpportunityLineItem.ContractLineItem__c,
          thisOpportunityLineItem
        );
      }
    }
    return opportunityLineItemMap;
  }

  /**
   * @description Method to create Ordway ContractLineitem
   * @param opportunityLineItemIds OpportunityLineItem Ids
   **/
  public static void createOrdwayContractLineItem(
    Set<Id> opportunityLineItemIds
  ) {
    //Contract id To Opportunitylineitem Map
    Map<Id, OpportunityLineItem> opportunityLineItemMap = new Map<Id, OpportunityLineItem>();
    Map<Id, ContractLineItem__c> ordwayContractLineItemMapToInsert = new Map<Id, ContractLineItem__c>();

    for (
      OpportunityLineItem thisOpportunityLineItem : getOpportunityLineItemMap(
          opportunityLineItemIds
        )
        .values()
    ) {
      if (
        OpportunityOrdwayContractSyncController.isSynchronizable(
          thisOpportunityLineItem.Opportunity.StageName,
          thisOpportunityLineItem.Opportunity.OrdwayOpportunityType__c,
          thisOpportunityLineItem.Opportunity.OrdwayContractStatus__c
        )
      ) {
        opportunityLineItemMap.put(
          thisOpportunityLineItem.Id,
          thisOpportunityLineItem
        );
        ContractLineItem__c thisContractLineItem = (ContractLineItem__c) ContractLineItem__c.sObjectType.newSObject(
          null,
          true
        );
        if (
          SObjectHelper.isFieldCreatable(
            SObjectType.ContractLineItem__c,
            Schema.ContractLineItem__c.OrdwayContract__c
          )
        ) {
          thisContractLineItem.OrdwayContract__c = thisOpportunityLineItem.Opportunity.OrdwayContract__c;
        }
        ordwayContractLineItemMapToInsert.put(
          thisOpportunityLineItem.Id,
          (ContractLineItem__c) SObjectFieldMappingHelper.dynamicSobjectMapping(
            thisOpportunityLineItem,
            thisContractLineItem,
            true
          )
        );
      }
    }
    if (Schema.sObjectType.ContractLineItem__c.isCreateable()) {
      insert ordwayContractLineItemMapToInsert.values();
    }

    //Method to populate ContractLineItem Id for Opportunitylineitem
    ContractLineItemService.populateContractLineItemId(
      ordwayContractLineItemMapToInsert,
      opportunityLineItemMap
    );

    if (Schema.sObjectType.OpportunityLineItem.isUpdateable()) {
      update opportunityLineItemMap.values();
    }
  }

  /**
   * @description Method to Sync Ordway Contract Line Item With Opportunity Line Item
   * @param recordIds OpportunityLineItem Ids
   * @param contractIds Contract Ids Ids
   **/
  public static void syncOrdwayContractLineItem( Set<Id> recordIds, Set<Id> contractIds ) {
    //Contract id To Opportunitylineitem Map
    Map<Id, OpportunityLineItem> opportunityLineItemMap = getOpportunityLineItemMap( recordIds );
    if (!opportunityLineItemMap.isEmpty()) {
      ContractLineItemService.updateOrdwayContractLineItem( opportunityLineItemMap, contractIds );
    }
  }

  /**
   * @description Method to delete Synced Ordway ContractLineitem
   * @param opportunityOldMap OpportunityLineItem Old Map
   * @param opportunityMap OpportunityLineItem New Map
   **/
  public static void deleteSyncedContractLineitem(
    Map<Id, OpportunityLineItem> opportunityOldMap,
    Map<Id, Opportunity> opportunityMap
  ) {
    Set<Id> contractLineItemToDelete = new Set<Id>();
    for (
      OpportunityLineItem thisOpportunityLineItem : opportunityOldMap.values()
    ) {
      if (
        OpportunityOrdwayContractSyncController.isSynchronizable(
          opportunityMap.get(thisOpportunityLineItem.OpportunityId).StageName,
          opportunityMap.get(thisOpportunityLineItem.OpportunityId)
            .OrdwayOpportunityType__c,
          opportunityMap.get(thisOpportunityLineItem.OpportunityId)
            .OrdwayContractStatus__c
        )
      ) {
        contractLineItemToDelete.add(
          thisOpportunityLineItem.ContractLineItem__c
        );
      }
    }
    if (!contractLineItemToDelete.isEmpty()) {
      ContractLineItemService.deleteOrdwayContractLineItem(
        contractLineItemToDelete
      );
    }
  }

  /**
   * @description Method to get Contract Line Item Ids from Opportunity Line Item Ids
   * @param opportunityId Set of Opportunity Ids
   * @return Set<Id> Set of Contract Line Item Ids
   **/
  public static Set<Id> getContractLineIdFromOLI(Set<Id> opportunityId) {
    Set<Id> contractLineIds = new Set<Id>();
    for (
      OpportunityLineItem thisOLI : getOpportunityLineItemMap(opportunityId)
        .values()
    ) {
      if (thisOLI.ContractLineItem__c != null) {
        contractLineIds.add(thisOLI.ContractLineItem__c);
      }
    }
    return contractLineIds;
  }

  /**
   * @description Method to update Opportunity Line Item
   * @param oliList Opportunity Line Item to Update
   **/
  public static void updateOLI(List<OpportunityLineItem> oliList) {
    if (Schema.sObjectType.OpportunityLineItem.isUpdateable()) {
      update oliList;
    }
  }
}