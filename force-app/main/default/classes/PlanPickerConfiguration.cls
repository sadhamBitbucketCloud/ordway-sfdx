public class PlanPickerConfiguration {

  @AuraEnabled
  public static InitialWrapper getInitialWrapper(String objectApiName) {
    try {
      if(objectApiName != null){
        InitialWrapper thisInitialWrapper = new InitialWrapper();
        thisInitialWrapper.planPickerMetadataList = getPlanPicker(objectApiName);
        thisInitialWrapper.thisObjectFields = getSobjectFields(MetadataService.objectAPINameMap.get(objectApiName));
        ApplicationLog.StartLoggingContext('Plan Picker Configuration', 'Get Initial Wrapper');
        ApplicationLog.Info(''+thisInitialWrapper);
        ApplicationLog.FinishLoggingContext();
        return thisInitialWrapper;
      }
      return null;
    } catch (Exception e) {
        throw new AuraHandledException('Error ' + e.getMessage());
    }
  }
  
  /**
   * @description Method to Get Plan Picker Custom Metadata
   * @return List<PlanPicker__mdt> Get Plan Picker Custom Metadata
   **/
  @AuraEnabled
  public static List<PlanPicker__mdt> getPlanPicker(String objectApiName) {
    return [
      SELECT Id, DeveloperName, MasterLabel, Configuration__c
            FROM PlanPicker__mdt
            WHERE DeveloperName =: SObjectFieldMappingController.getUniqueMetadataKeyValue(objectApiName)
    ];
  }

  @AuraEnabled
  public static String deployPlanPickerMetadata(String configurationJSON, String objectName) {
    Id deploymentJobId;
    try {
      if(configurationJSON != null){
        String deploymentStatus;
        String key = SObjectFieldMappingController.getUniqueMetadataKeyValue(objectName);
        Metadata.CustomMetadata customMetadata = new Metadata.CustomMetadata();
        customMetadata.fullName = 'OrdwayLabs__PlanPicker.OrdwayLabs__'+key;
        customMetadata.label = key;
        customMetadata.values.add(createMetaDataValue('OrdwayLabs__Configuration__c', configurationJSON));
      
          Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
          mdContainer.addMetadata(customMetadata);
          DeployOrdwayObjectFieldMappingMetaData callback = new DeployOrdwayObjectFieldMappingMetaData();
          deploymentJobId = Metadata.Operations.enqueueDeployment(mdContainer, callback);
          return MetadataService.getMetadataDeploymentURL(deploymentJobId);
      }
      return null;
      } catch (Exception thisException) {
        throw new AuraHandledException(thisException.getMessage());
      }
    }

  /**
   * @description  Method to Create Custom MetadataValue
   * @param  fieldName Metadata Field API Name
   * @param  fieldName Metadata Field Value
   * @return  Custom MetadataValue
   */
  public static Metadata.CustomMetadataValue createMetaDataValue(String fieldName, String fieldValue) {
    Metadata.CustomMetadataValue targetField = new Metadata.CustomMetadataValue();
    targetField.field = fieldName;
    targetField.value = fieldValue;
    return targetField;
  }

  /**
   * @description Method to get Fields for specified object
   * @param objectApiName Object API Name
   * @return thisObjectFields Picklist wrapper
   **/
  @AuraEnabled(cacheable=true)
  public static ObjectFields getSobjectFields(String objectApiName) {

    ObjectFields thisObjectFields = new ObjectFields();
    thisObjectFields.objectFieldsList = fetchObjectLabelValue(objectApiName);
    return thisObjectFields;
  }
  
  /**
   * @description Method to construct the picklist wrapper with Object Field Name & label
   * @param objectApiName Object API Name
   * @return List<ObjectFieldEntryWrapper> List of Picklist Entry wrapper
   **/
    @AuraEnabled(cacheable=true)
    public static List<ObjectFieldEntryWrapper> fetchObjectLabelValue(String objectApiName) {
      try {
        List<ObjectFieldEntryWrapper> thisObjectFieldEntryWrapperList = new List<ObjectFieldEntryWrapper>();
        if(objectApiName != null){
          for (FieldDefinition thisFieldDefinition : SObjectFieldMappingHelper.getFieldDefinition(objectApiName)) {   
            if(!thisFieldDefinition.DataType.contains('Lookup')
              && !thisFieldDefinition.DataType.contains('Master-Detail')
              && !thisFieldDefinition.DataType.contains('Date')
              && !thisFieldDefinition.DataType.contains('Formula')
              && !thisFieldDefinition.DataType.contains('Long Text Area')){
                thisObjectFieldEntryWrapperList.add(new ObjectFieldEntryWrapper(thisFieldDefinition.MasterLabel, 
                thisFieldDefinition.QualifiedApiName));
            }
          }
        }
        return thisObjectFieldEntryWrapperList;
      } catch (Exception e) {
          throw new AuraHandledException('Error ' + e.getMessage());
        }
    }   
    
  /**
   * @description Initial wrapper class for Plan Picker Configuration 
   **/
  public class InitialWrapper {
    @AuraEnabled
    public List<PlanPicker__mdt> planPickerMetadataList;
    @AuraEnabled
    public ObjectFields thisObjectFields;
  }

  /**
   * @description Picklist Wrapper class
   **/
  public class ObjectFields {
    @AuraEnabled
    public List<ObjectFieldEntryWrapper> objectFieldsList;
  }

  /**
   * @description Picklist Entry Wrapper class
   **/
  public class ObjectFieldEntryWrapper {
    @AuraEnabled
    public String label;
    @AuraEnabled
    public string value;

    public ObjectFieldEntryWrapper(String objectEntryLabel, String objectEntryApiName) {
      this.label = objectEntryLabel;
      this.value = objectEntryApiName;
    }
  }
}