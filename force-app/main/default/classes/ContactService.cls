/**
 * @File Name          : ContactService.cls
 * @Description        : Contact Service Class
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 11-18-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    10/12/2019   Ordway Labs     Initial Version
 **/
public with sharing class ContactService {

  public static void populateExternalId(Contact thisContact) {
    if(thisContact.EntityID__c != null) {
      if(thisContact.ExternalID__c != null && thisContact.OrdwayContactID__c == null) {
        thisContact.OrdwayContactID__c = thisContact.ExternalID__c.substringAfter(
          ':'
        );
      }

      if(thisContact.OrdwayContactID__c == null || thisContact.OrdwayContactID__c == '') {
        thisContact.OrdwayContactID__c = thisContact.ExternalID__c;
      }
      thisContact.ExternalID__c = thisContact.OrdwayContactID__c != null ? thisContact.EntityID__c + ':' +thisContact.OrdwayContactID__c : null;
    }
    else {
      if (thisContact.ExternalID__c != null) {
        thisContact.OrdwayContactID__c = thisContact.ExternalID__c;
      }
      if (
        thisContact.ExternalID__c == null &&
        thisContact.OrdwayContactID__c != null
      ) {
        thisContact.ExternalID__c = thisContact.OrdwayContactID__c;
      }
    }
  }
  /**
   * @description Method to get Map of Contact records
   * @param recordIds Set of Contact Ids
   * @return Map<Id, Contact> Map of Contact records
   **/
  public static Map<Id, Contact> getContact(Set<Id> recordIds) {
    Map<Id, Contact> contactMap = new Map<Id, Contact>();

    if (Schema.sObjectType.Contact.isAccessible() && !recordIds.isEmpty()) {
      contactMap = new Map<Id, Contact>(
        [
          SELECT
            Id,
            MailingStreet,
            MailingState,
            MailingCity,
            MailingCountry,
            MailingPostalCode,
            Email
          FROM Contact
          WHERE Id IN :recordIds
        ]
      );
    }
    return contactMap;
  }

  public static List<Contact> getContactListToSync(Map<Id, Contact> contactNewMap){
    List<Contact> contactListToSync = new List<Contact>();
    for(Contact thisContact : [SELECT Id, Sync_to_Ordway__c, AccountId,
                                            Account.OrdwayCustomerId__c, EntityID__c
                                            FROM Contact
                                            WHERE Id IN: contactNewMap.values()]){  
      if(thisContact.Sync_to_Ordway__c
      && thisContact.AccountId != null
      && thisContact.Account.OrdwayCustomerId__c != null
      && ((OpportunitySettingController.isMultiEntityEnabled() && thisContact.EntityID__c != null)
          || !OpportunitySettingController.isMultiEntityEnabled())){
            contactListToSync.add(contactNewMap.get(thisContact.Id));
            break;//Sync only the first contact
      }
    }
    return contactListToSync;
  }

  public static void enqueueContactSyncQueueable(Map<Id, Contact> contactMap){
      ContactSync_Queueable contactSyncQueueable = new ContactSync_Queueable(contactMap);
        system.enqueueJob(contactSyncQueueable);
  }
}