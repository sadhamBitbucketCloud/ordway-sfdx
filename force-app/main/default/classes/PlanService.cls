public inherited sharing class PlanService {

    public static void populateOrdwayEntity(Plan__c plan){

      if(!OpportunitySettingController.isMultiEntityEnabled()) { return; } //If Multi Entity Not Enabled Do Nothing
      //On Bulk Import or Webhook, EntityID will be present, populate lookup field automatically
      if(plan.OrdwayEntity__c == null 
        && plan.EntityId__c != null
        && OrdwayEntityHelper.entityMapByEntityId.containsKey(plan.EntityID__c)){
          plan.OrdwayEntity__c = OrdwayEntityHelper.entityMapByEntityId.get(plan.EntityID__c);
      }
      //If Ordway Entity is blank, populate default entity if exist 
      if( plan.OrdwayEntity__c == null 
      && OrdwayEntityHelper.defaultEntity != null) {
        plan.OrdwayEntity__c = OrdwayEntityHelper.defaultEntity.Id;
      }
    }
  
    public static void populateEntity(Plan__c plan) {

      if(plan.OrdwayEntity__c == null && plan.EntityId__c != null && OrdwayEntityHelper.entityMapByEntityId.get(plan.EntityId__c) != null) { 
        plan.OrdwayEntity__c = OrdwayEntityHelper.entityMapByEntityId.get(plan.EntityId__c);
      }
  
      if(plan.OrdwayEntity__c == null && plan.EntityId__c == null) { clearEntity(plan); return; }
  
      plan.EntityName__c = OrdwayEntityHelper.entityMapById.get(plan.OrdwayEntity__c).Name;
      plan.EntityID__c = OrdwayEntityHelper.entityMapById.get(plan.OrdwayEntity__c).EntityID__c;
    }
  
    public static void populatePlanId(Plan__c plan) {
      if(plan.ExternalID__c == null) { return; }
      plan.PlanId__c = plan.ExternalID__c.contains(':') ? plan.ExternalID__c.substringAfterLast(':') : plan.ExternalID__c;
      if(plan.EntityId__c != null) { plan.ExternalID__c = plan.EntityId__c+':'+plan.PlanId__c; }
    }
  
      public static void clearEntity(Plan__c plan){
      plan.EntityName__c = null;
      plan.EntityID__c = null;
    }
  }