/**
 * @File Name          : OrdwayPlanController_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-27-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    17/04/2021        Ordway Labs        Initial Version
 **/
@isTest
private class OrdwayPlanController_Test {
 
  @isTest
  private static void getOrdwayPlan_WithEntityId() {

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c         = true;
    insert newSetting;

    Entity__c thisEntity   = TestObjectFactory.createOrdwayEntity();
    thisEntity.Name        = 'Test Entity';
    insert thisEntity; 

    thisEntity.EntityID__c = thisEntity.Id;
    update thisEntity;

    Entity__c thisEntityNew   = TestObjectFactory.createOrdwayEntity();
    thisEntityNew.Name        = 'Test Entity New';
    insert thisEntityNew; 

    thisEntityNew.EntityID__c = thisEntityNew.Id;
    update thisEntityNew;

    Map<Id, Entity__c> entityMap = OrdwayEntityHelper.entityMapById;

    Product2 thisProduct2 = TestObjectFactory.createProduct();
    insert thisProduct2;

    Plan__c thisOrdwayPlan   = TestObjectFactory.createOrdwayPlan();
    thisOrdwayPlan.PlanId__c = thisProduct2.Id;
    thisOrdwayPlan.Name      = 'Test Plan';
    thisOrdwayPlan.EntityID__c = thisEntity.EntityID__c;
    insert thisOrdwayPlan;

    ContractPlanPickerSetting__c newSettings = TestObjectFactory.createContractPlanPickerSetting();
    newSettings.EnableMultiEntity__c         = true;
    insert newSettings;

    Product2 thisProduct2New = TestObjectFactory.createProduct();
    insert thisProduct2New;

    Plan__c thisOrdwayPlanNew        = TestObjectFactory.createOrdwayPlan();
    thisOrdwayPlanNew.PlanId__c      = thisProduct2New.Id;
    thisOrdwayPlanNew.Name           = 'Test Plan New';
    thisOrdwayPlanNew.EntityID__c    = thisEntityNew.EntityID__c;
    insert thisOrdwayPlanNew;

    Test.startTest();
    List<Plan__c> thisPlanList = OrdwayPlanController.getOrdwayPlan(thisEntityNew.Id);
    Plan__c plan               = OrdwayPlanController.getPlan(thisOrdwayPlanNew.Id);
    Test.stopTest();
    System.assertEquals(1, thisPlanList.size());
    System.assertEquals('Test Plan New', thisPlanList[0].Name);
  }  

  @isTest  
  private static void syncToOrdway() {

    ProductTriggerHandler.createOrdwayProduct = false;
    Product2 thisProduct = TestObjectFactory.createProduct();
    thisProduct.OrdwayProduct__c = true;
    insert thisProduct;

    Plan__c thisPlan        = TestObjectFactory.createOrdwayPlan();
    insert thisPlan;

    OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.Product2Id__c    = thisProduct.Id;
    thisOrdwayProduct.OrdwayProductID__c = 'P-00071';
    insert thisOrdwayProduct;

    Charge__c thisCharge       = TestObjectFactory.createCharge();
    thisCharge.Name            = 'Test Charge';
    thisCharge.OrdwayPlan__c   =  thisPlan.Id;
    thisCharge.Product__c = thisProduct.Id;
    insert thisCharge;

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.planString;
    
    RESPONSEBODY = RESPONSEBODY.replace('CHARGEID', String.valueOf(thisCharge.Id));
    RESPONSEBODY = RESPONSEBODY.replace('PRODUCTID', String.valueOf(thisProduct.Id));
    RESPONSEBODY = RESPONSEBODY.replace('ORDWAYPLANID', String.valueOf(thisPlan.Id));

    Test.setMock(HttpCalloutMock.class, new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY));
    OrdwayPlanController.syncToOrdway(thisPlan.Id);

    Test.stopTest();
    Plan__c planToAssert = [SELECT Id, Name, PlanId__c FROM Plan__c];
    List<Charge__c> chargeToAssert = [SELECT Id, Name FROM Charge__c];
    system.assertEquals('AY Plan on SF', planToAssert.Name, 'should return same name as response');
    system.assertEquals('CN - AY', chargeToAssert[0].Name, 'should return same name as response');
    system.assertEquals('PLN-00269', planToAssert.PlanId__c, 'should return same PlanId as response');
  }

  @isTest  
  private static void syncToOrdway_Negative() {

    ProductTriggerHandler.createOrdwayProduct = false;
    Product2 thisProduct = TestObjectFactory.createProduct();
    thisProduct.OrdwayProduct__c = true;
    insert thisProduct;

    Plan__c thisPlan        = TestObjectFactory.createOrdwayPlan();
    thisPlan.PlanDetails__c = thisProduct.Id;
    insert thisPlan;

    OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.Product2Id__c    = thisProduct.Id;
    thisOrdwayProduct.OrdwayProductID__c = 'P-00071';
    insert thisOrdwayProduct;

    Charge__c thisCharge       = TestObjectFactory.createCharge();
    thisCharge.Name            = 'Test Charge';
    thisCharge.OrdwayPlan__c   =  thisPlan.Id;
    thisCharge.Product__c = thisProduct.Id;
    insert thisCharge;

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.planString;
    
    RESPONSEBODY = RESPONSEBODY.replace('CHARGEID', String.valueOf(thisCharge.Id));
    RESPONSEBODY = RESPONSEBODY.replace('PRODUCTID', String.valueOf(thisProduct.Id));
    Test.setMock(HttpCalloutMock.class, new OrdwayService_Mock(RESPONSECODE, 
                            RESPONSEBODY.replace('ORDWAYPLANID', String.valueOf(thisPlan.Id))));
    try {
      OrdwayPlanController.syncToOrdway('INVALIDID');
    } catch (Exception ex) {
      system.assertNotEquals(null, ex.getMessage());
    }      
    Test.stopTest();
  }

  @isTest  
  private static void syncToOrdway_400() {

    ProductTriggerHandler.createOrdwayProduct = false;
    Product2 thisProduct = TestObjectFactory.createProduct();
    thisProduct.OrdwayProduct__c = true;
    insert thisProduct;

    Plan__c thisPlan        = TestObjectFactory.createOrdwayPlan();
    thisPlan.PlanDetails__c = thisProduct.Id;
    insert thisPlan;

    OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.Product2Id__c    = thisProduct.Id;
    thisOrdwayProduct.OrdwayProductID__c = 'P-00071';
    insert thisOrdwayProduct;

    Charge__c thisCharge       = TestObjectFactory.createCharge();
    thisCharge.Name            = 'Test Charge';
    thisCharge.OrdwayPlan__c   =  thisPlan.Id;
    thisCharge.Product__c = thisProduct.Id;
    insert thisCharge;

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();
    Integer RESPONSECODE = 400;
    String RESPONSEBODY = '{ "errors":{"details": "Bad Request"}}';
    try {
      OrdwayPlanController.syncToOrdway(thisPlan.Id);
    } catch (Exception ex) {
      system.assertNotEquals(null, ex.getMessage());
    }      
    Test.stopTest();
  }

  @isTest  
  private static void syncToOrdway_500() {

    ProductTriggerHandler.createOrdwayProduct = false;
    Product2 thisProduct = TestObjectFactory.createProduct();
    thisProduct.OrdwayProduct__c = true;
    insert thisProduct;

    Plan__c thisPlan        = TestObjectFactory.createOrdwayPlan();
    thisPlan.PlanDetails__c = thisProduct.Id;
    insert thisPlan;

    OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.Product2Id__c    = thisProduct.Id;
    thisOrdwayProduct.OrdwayProductID__c = 'P-00071';
    insert thisOrdwayProduct;

    Charge__c thisCharge       = TestObjectFactory.createCharge();
    thisCharge.Name            = 'Test Charge';
    thisCharge.OrdwayPlan__c   =  thisPlan.Id;
    thisCharge.Product__c = thisProduct.Id;
    insert thisCharge;

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();
    Integer RESPONSECODE = 500;
    String RESPONSEBODY = '{ "errors":{"details": "Internal Server Error"}}';
    try {
      OrdwayPlanController.syncToOrdway(thisPlan.Id);
    } catch (Exception ex) {
      system.assertNotEquals(null, ex.getMessage());
    }      
    Test.stopTest();
  }

  @isTest  
  private static void getInitialWrapper() {

    Product2 thisProduct2 = TestObjectFactory.createProduct();
    insert thisProduct2;

    Plan__c thisPlan        = TestObjectFactory.createOrdwayPlan();
    thisPlan.PlanDetails__c = thisProduct2.Id;
    thisPlan.PlanId__c      = 'TestPlan Id';  
    insert thisPlan;

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Test.startTest();
    OrdwayPlanController.InitialWrapper thisInitialWrapper = new OrdwayPlanController.InitialWrapper();
    thisInitialWrapper = OrdwayPlanController.getInitialWrapper(thisPlan.Id);
    Test.stopTest();
    
    System.assertEquals('TestPlan Id', thisInitialWrapper.thisPlan.PlanId__c, 'Should return TestPlan Id');
    System.assertNotEquals(null, thisInitialWrapper.namedCredentials);
    //System.assertEquals(true, thisInitialWrapper.isMultiEntityEnabled);
    }

    @isTest  
    private static void getInitialWrapper_Negative() {

      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
      insert thisOrdwayProduct;

      ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
      insert thisApplicationsetting;

      Test.startTest();
      try{
      OrdwayPlanController.InitialWrapper thisInitialWrapper = new OrdwayPlanController.InitialWrapper();
      thisInitialWrapper            = OrdwayPlanController.getInitialWrapper(null);
      } catch (Exception ex) {
        system.assertNotEquals(null, ex.getMessage());
      } 
      Test.stopTest();
    }
}