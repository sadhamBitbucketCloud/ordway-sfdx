@isTest
public class PlanPickerController_Test {
	@isTest
  public static void getInitialWrapper(){

		ProductTriggerHandler.createOrdwayProduct = false;

		 //Insert Product
		 Product2 newProduct = TestObjectFactory.createProduct();
		 insert newProduct;
 
		 //Insert Pricebook
		 Pricebook2 newPricebook = TestObjectFactory.createPricebook();
		 insert newPricebook;
 
		 //Insert Standard PricebookEntry
		 PricebookEntry newPBEStandard = new PricebookEntry();
		 newPBEStandard.Product2Id = newProduct.Id;
		 newPBEStandard.Pricebook2Id = Test.getStandardPricebookId();
		 PricebookEntry newPBEStd = TestObjectFactory.createPriceBookEntry(
			 newPBEStandard
		 );
		 insert newPBEStd;
 
		 PricebookEntry newPBEntry = new PricebookEntry();
		 newPBEntry.Product2Id = newProduct.Id;
		 newPBEntry.Pricebook2Id = newPricebook.Id;
		 PricebookEntry newPBE = TestObjectFactory.createPriceBookEntry(newPBEntry);
		 insert newPBE;

		Plan__c thisPlan        = TestObjectFactory.createOrdwayPlan();
    thisPlan.PlanDetails__c = newProduct.Id;
		thisPlan.PlanId__c  = 'PLN-00002';
		thisPlan.Name = '2 Test SE plan - 2104';
    insert thisPlan;

    OrdwayProduct__c thisOrdwayProduct  = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.OrdwayProductID__c     = 'P-00075';
    thisOrdwayProduct.Product2Id__c     = newProduct.Id;
    insert thisOrdwayProduct;

    Charge__c thisCharge       = TestObjectFactory.createCharge();
    thisCharge.Name            = '2.2 Test SE Charge - 2104';
    thisCharge.OrdwayPlan__c   =  thisPlan.Id;
		thisCharge.ChargeID__c = 'CHG-00002';
		thisCharge.ProductID__c = 'P-00002';
		thisCharge.PricingModel__c = 'Tiered';
    thisCharge.Product__c      = newProduct.Id;
    insert thisCharge;

		Charge__c thisChargeNew       = TestObjectFactory.createCharge();
    thisChargeNew.Name            = '2.2 Test SE Charge - 2105';
    thisChargeNew.OrdwayPlan__c   =  thisPlan.Id;
		thisChargeNew.ChargeID__c = 'CHG-00003';
		thisChargeNew.ProductID__c = 'P-00002';
		thisChargeNew.PricingModel__c = 'Tiered';
    thisChargeNew.Product__c      = newProduct.Id;
    insert thisChargeNew;

		//Insert Opportunity
    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.ServiceStartDate__c = system.today();
    newOpportunity.AutoRenew__c = false;
		newOpportunity.Pricebook2Id = newPricebook.Id;
    insert newOpportunity;

		OpportunityLineItem thisOLI = TestObjectFactory.createOpportunityLineItem();
    thisOLI.OpportunityId       = newOpportunity.Id;
    insert thisOLI; 

		OpportunityLineItem thisOLI1 = TestObjectFactory.createOpportunityLineItem();
    thisOLI1.OpportunityId       = newOpportunity.Id;
    insert thisOLI1; 

		OrdwayOppLineItemTierTriggerHandler.deleteTier = false;

    OpportunityLineItemTier__c thisOLITier = TestObjectFactory.createOpportunityLineItemTier();
    thisOLITier.StartingUnit__c            = 123;
    thisOLITier.OpportunityProduct__c      = thisOLI.Id; 
    insert thisOLITier;

		OpportunityLineItemTier__c thisOLITier1 = TestObjectFactory.createOpportunityLineItemTier();
    thisOLITier1.StartingUnit__c            = 123;
    thisOLITier1.OpportunityProduct__c      = thisOLI.Id; 
    insert thisOLITier1;

		OpportunityLineItemTier__c thisOLITier2 = TestObjectFactory.createOpportunityLineItemTier();			
    thisOLITier2.StartingUnit__c            = 123;
    thisOLITier2.OpportunityProduct__c      = thisOLI1.Id; 
    insert thisOLITier2;

		OpportunityLineItemTier__c thisOLITier3 = TestObjectFactory.createOpportunityLineItemTier();
    thisOLITier3.StartingUnit__c            = 123;
    thisOLITier3.OpportunityProduct__c      = thisOLI1.Id; 
    insert thisOLITier3;

		PlanPickerController.getInitialWrapper(newOpportunity.Id, newPricebook.Id);

		String lineItemList = ' [ '+
    ' { '+
        ' "IsDeleted": false, '+
        ' "Discount": 0, '+
        ' "OrdwayLabs__PricingModel__c": "Volume", '+
        ' "OrdwayLabs__UnitofMeasure__c": "N/A", '+
        ' "Quantity": 1, '+
        ' "OrdwayLabs__OrdwayProductId__c": "P-00002", '+
        ' "OrdwayLabs__ChargeType__c": "Recurring", '+
        ' "OrdwayLabs__OrdwayChargeId__c": "CHG-00002", '+
        ' "OrdwayLabs__OrdwayEffectivePrice__c": 0, '+
        ' "OrdwayLabs__PricingType__c": "per_unit", '+
        ' "OrdwayLabs__IncludedUnits__c": "N/A", '+
        ' "OrdwayLabs__TierJSON__c": "", '+
        ' "UnitPrice": 0, '+
        ' "OrdwayLabs__OrdwayListPrice__c": 0, '+
        ' "OpportunityId": "OPPID", '+
        ' "PricebookEntryId": "PBEID", '+
        ' "OrdwayLabs__ChargeName__c": "2.1 Test SE Charge - 2104", '+
        ' "OrdwayLabs__OrdwayPlanId__c": "PLN-00002", '+
        ' "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0, '+
        ' "OrdwayLabs__OrdwayDiscountedContractValue__c": 0, '+
        ' "OrdwayLabs__OrdwayUndiscountedEffectivePrice__c": 0, '+
        ' "OrdwayLabs__BillingPeriod__c": "Monthly", '+
        ' "isChargeSelected": true, '+
        ' "dynamicChargeId": "DYNAMIC_CHARGE_1", '+
        ' "OrdwayLabs__PlanBundleID__c": "PLN-00002_3" '+
    ' }, '+
    ' { '+
        ' "IsDeleted": false, '+
        ' "Discount": 0, '+
        ' "OrdwayLabs__PricingModel__c": "Tiered", '+
        ' "OrdwayLabs__UnitofMeasure__c": "N/A", '+
        ' "Quantity": 1, '+
        ' "OrdwayLabs__OrdwayProductId__c": "P-00002", '+
        ' "OrdwayLabs__ChargeType__c": "Recurring", '+
        ' "OrdwayLabs__OrdwayChargeId__c": "CHG-00003", '+
        ' "OrdwayLabs__OrdwayEffectivePrice__c": 0, '+
        ' "OrdwayLabs__PricingType__c": "per_unit", '+
        ' "OrdwayLabs__IncludedUnits__c": "N/A", '+
        ' "OrdwayLabs__TierJSON__c": "", '+
        ' "UnitPrice": 0, '+
        ' "OrdwayLabs__OrdwayListPrice__c": 0, '+
        ' "OpportunityId": "OPPID", '+
        ' "PricebookEntryId": "PBEID", '+
        ' "OrdwayLabs__ChargeName__c": "2.2 Test SE Charge - 2104", '+
        ' "OrdwayLabs__OrdwayPlanId__c": "PLN-00002", '+
        ' "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0, '+
        ' "OrdwayLabs__OrdwayDiscountedContractValue__c": 0, '+
        ' "OrdwayLabs__OrdwayUndiscountedEffectivePrice__c": 0, '+
        ' "OrdwayLabs__BillingPeriod__c": "Monthly", '+
        ' "isChargeSelected": true, '+
        ' "dynamicChargeId": "DYNAMIC_CHARGE_2", '+
        ' "OrdwayLabs__PlanBundleID__c": "PLN-00002_3" '+
    ' } '+
' ] ';

lineItemList = lineItemList.replaceAll('OPPID', newOpportunity.Id);
 lineItemList = lineItemList.replaceAll('PBEID', newPBE.Id);

	String lineItemTierString = ' { '+
	' "OLIID": [ '+
			' { '+
					' "Id": "thisOLITier", '+
					' "IsDeleted": false, '+
					' "Name": "OLIT-01627", '+
					' "OrdwayLabs__EndingUnit__c": 1, '+
					' "OrdwayLabs__ListPrice__c": 10, '+
					' "OrdwayLabs__OpportunityProduct__c": "OLIID", '+
					' "OrdwayLabs__PricingType__c": "Per unit", '+
					' "OrdwayLabs__StartingUnit__c": 0, '+
					' "OrdwayLabs__Tier__c": 1 '+
			' }, '+
			' { '+
					' "Id": "thisOLITier1", '+
					' "IsDeleted": false, '+
					' "Name": "OLIT-01628", '+
					' "OrdwayLabs__EndingUnit__c": 2, '+
					' "OrdwayLabs__ListPrice__c": 20, '+
					' "OrdwayLabs__OpportunityProduct__c": "OLIID", '+
					' "OrdwayLabs__PricingType__c": "Per unit", '+
					' "OrdwayLabs__StartingUnit__c": 1, '+
					' "OrdwayLabs__Tier__c": 2 '+
			' } '+
	' ], '+
	' "OLIID1": [ '+
			' { '+
					' "Id": "thisOLITier2", '+
					' "IsDeleted": false, '+
					' "Name": "OLIT-01632", '+
					' "OrdwayLabs__EndingUnit__c": 1, '+
					' "OrdwayLabs__ListPrice__c": 10, '+
					' "OrdwayLabs__OpportunityProduct__c": "OLIID1", '+
					' "OrdwayLabs__PricingType__c": "Per unit", '+
					' "OrdwayLabs__StartingUnit__c": 0, '+
					' "OrdwayLabs__Tier__c": 1 '+
			' }, '+
			' { '+
					' "Id": "thisOLITier3", '+
					' "IsDeleted": false, '+
					' "Name": "OLIT-01633", '+
					' "OrdwayLabs__EndingUnit__c": 2, '+
					' "OrdwayLabs__ListPrice__c": 10, '+
					' "OrdwayLabs__OpportunityProduct__c": "OLIID1", '+
					' "OrdwayLabs__PricingType__c": "Per unit", '+
					' "OrdwayLabs__StartingUnit__c": 1, '+
					' "OrdwayLabs__Tier__c": 2 '+
			' } '+
	' ] '+
' } ';

lineItemTierString = lineItemTierString.replaceAll('OLIID', thisOLI.Id);
lineItemTierString = lineItemTierString.replaceAll('OLIID1', thisOLI1.Id);
lineItemTierString = lineItemTierString.replaceAll('thisOLITier', thisOLITier.Id);
lineItemTierString = lineItemTierString.replaceAll('thisOLITier1', thisOLITier1.Id);
lineItemTierString = lineItemTierString.replaceAll('thisOLITier2', thisOLITier2.Id);
lineItemTierString = lineItemTierString.replaceAll('thisOLITier3', thisOLITier3.Id);

String chargeTierMap = ' { '+
' "CHG-00002": [ '+
		' { '+
				' "OrdwayLabs__Tier__c": 1, '+
				' "IsDeleted": false, '+
				' "OrdwayLabs__ListPrice__c": 10, '+
				' "OrdwayLabs__EndingUnit__c": 1, '+
				' "OrdwayLabs__PricingType__c": "Per unit", '+
				' "OrdwayLabs__StartingUnit__c": 0 '+
		' }, '+
		' { '+
				' "OrdwayLabs__Tier__c": 2, '+
				' "IsDeleted": false, '+
				' "OrdwayLabs__ListPrice__c": 20, '+
				' "OrdwayLabs__EndingUnit__c": null, '+
				' "OrdwayLabs__PricingType__c": "Per unit", '+
				' "OrdwayLabs__StartingUnit__c": 1 '+
		' } '+
	' ], '+
	' "CHG-00003": [ '+
		' { '+
				' "OrdwayLabs__Tier__c": 1, '+
				' "IsDeleted": false, '+
				' "OrdwayLabs__ListPrice__c": 10, '+
				' "OrdwayLabs__EndingUnit__c": 1, '+
				' "OrdwayLabs__PricingType__c": "Per unit", '+
				' "OrdwayLabs__StartingUnit__c": 0 '+
		' }, '+
		' { '+
				' "OrdwayLabs__Tier__c": 2, '+
				' "IsDeleted": false, '+
				' "OrdwayLabs__ListPrice__c": 20, '+
				' "OrdwayLabs__EndingUnit__c": 2, '+
				' "OrdwayLabs__PricingType__c": "Flat fee", '+
				' "OrdwayLabs__StartingUnit__c": 1 '+
		' } '+
	' ] '+
	' } ';

	List<String> listToDelete = new List<String>();
	listToDelete.add(thisOLI1.Id);
	PlanPickerController.updatePlans(lineItemList, '[]', newOpportunity.Id, '[]', lineItemTierString, chargeTierMap, false, 'OrdwayLabs__OpportunityProduct__c');
	TierController.validateOnDelete = false;
	PlanPickerController.deleteLineItemsTierByLineItemIds(listToDelete, 'OrdwayLabs__OpportunityLineItemTier__c', 'OrdwayLabs__OpportunityProduct__c');
	PlanPickerController.deleteLineItems(listToDelete, 'OpportunityLineItem');
	system.assertNotEquals(null, listToDelete);

	}
}