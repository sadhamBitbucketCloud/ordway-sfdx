/**
 * @File Name          : OrdwayContractService_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    1/15/2020   Ordway Labs     Initial Version
 **/
@isTest
private class OrdwayContractService_Test {
  @isTest
  static void test_OrdwayContractService() {
    OpportunitySettingController.saveOpportunitySetting(
      TestObjectFactory.createContractPlanPickerSetting()
    );
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    //Opportunity_OrdwayContract Object Field Mapping Metadata
    String opportunity_OrdwayContractFieldMapping =
      '[{"fieldAPIName":"Account__c","fieldDataTypeKey":"Lookup(Account)","fieldLabel":"Account","sourceFieldAPIName":"AccountId"},' +
      '{"fieldAPIName":"AutoRenew__c","fieldDataTypeKey":"Checkbox","fieldLabel":"Auto Renew","sourceFieldAPIName":"IsPrivate"},' +
      '{"fieldAPIName":"BillingContact__c","fieldDataTypeKey":"Lookup(Contact)","fieldLabel":"Billing Contact","sourceFieldAPIName":"BillingContact__c"},' +
      '{"fieldAPIName":"BillingStartDate__c","fieldDataTypeKey":"Date","fieldLabel":"Billing Start Date","sourceFieldAPIName":"BillingStartDate__c"},' +
      '{"fieldAPIName":"ContractEffectiveDate__c","fieldDataTypeKey":"Date","fieldLabel":"Contract Effective Date","sourceFieldAPIName":"ContractEffectiveDate__c"},' +
      '{"fieldAPIName":"ContractTerm__c","fieldDataTypeKey":"Picklist","fieldLabel":"Contract Term","sourceFieldAPIName":"ContractTerm__c"},' +
      '{"fieldAPIName":"OrdwaySubscriptionID__c","fieldDataTypeKey":"Text","fieldLabel":"Ordway Subscription ID","sourceFieldAPIName":"OrdwaySubscriptionID__c"},' +
      '{"fieldAPIName":"RenewalTerm__c","fieldDataTypeKey":"Picklist","fieldLabel":"Renewal Term","sourceFieldAPIName":"RenewalTerm__c"},' +
      '{"fieldAPIName":"ServiceStartDate__c","fieldDataTypeKey":"Date","fieldLabel":"Service Start Date","sourceFieldAPIName":"ServiceStartDate__c"}]';

    MetadataService.fieldMappingMetadataMap.put(
      'Opportunity_OrdwayContract',
      TestObjectFactory.createObjectFieldMappingMetadata(
        'Opportunity_OrdwayContract',
        'Opportunity',
        'OrdwayContract__c',
        opportunity_OrdwayContractFieldMapping
      )
    );
    //OpportunityLineItem_ContractLineItem Object Field Mapping Metadata
    String opportunityLineItem_ContractLineItemFieldMapping =
      '[{"fieldAPIName":"BillingPeriod__c","fieldDataTypeKey":"Picklist","fieldLabel":"Billing Period","sourceFieldAPIName":"BillingPeriod__c"},' +
      '{"fieldAPIName":"ChargeType__c","fieldDataTypeKey":"Picklist","fieldLabel":"Charge Type","sourceFieldAPIName":"ChargeType__c"},' +
      '{"fieldAPIName":"Name","fieldDataTypeKey":"Text","fieldLabel":"Contract Line Item Name","sourceFieldAPIName":"Name"},' +
      '{"fieldAPIName":"Discount__c","fieldDataTypeKey":"Percent","fieldLabel":"Discount","sourceFieldAPIName":"Discount"},' +
      '{"fieldAPIName":"OrdwayChargeId__c","fieldDataTypeKey":"Text","fieldLabel":"Ordway Charge Id","sourceFieldAPIName":"OrdwayChargeId__c"},' +
      '{"fieldAPIName":"OrdwayDiscountedContractValue__c","fieldDataTypeKey":"Number","fieldLabel":"Ordway Discounted Contract Value","sourceFieldAPIName":"OrdwayDiscountedContractValue__c"},' +
      '{"fieldAPIName":"OrdwayEffectivePrice__c","fieldDataTypeKey":"Number","fieldLabel":"Ordway Effective Price","sourceFieldAPIName":"OrdwayEffectivePrice__c"},' +
      '{"fieldAPIName":"OrdwayListPrice__c","fieldDataTypeKey":"Number","fieldLabel":"Ordway List Price","sourceFieldAPIName":"OrdwayListPrice__c"},' +
      '{"fieldAPIName":"OrdwayPlanId__c","fieldDataTypeKey":"Text","fieldLabel":"Ordway Plan Id","sourceFieldAPIName":"OrdwayPlanId__c"},' +
      '{"fieldAPIName":"OrdwayProductId__c","fieldDataTypeKey":"Text","fieldLabel":"Ordway Product Id","sourceFieldAPIName":"OrdwayProductId__c"},' +
      '{"fieldAPIName":"OrdwayUndiscountedContractValue__c","fieldDataTypeKey":"Number","fieldLabel":"Ordway Undiscounted Contract Value","sourceFieldAPIName":"OrdwayUndiscountedContractValue__c"},' +
      '{"fieldAPIName":"Quantity__c","fieldDataTypeKey":"Number","fieldLabel":"Quantity","sourceFieldAPIName":"Quantity"}]';
    MetadataService.fieldMappingMetadataMap.put(
      'OpportunityLineItem_ContractLineItem',
      TestObjectFactory.createObjectFieldMappingMetadata(
        'OpportunityLineItem_ContractLineItem',
        'OpportunityLineItem',
        'ContractLineItem__c',
        opportunityLineItem_ContractLineItemFieldMapping
      )
    );

    Contact ordwayContact = TestObjectFactory.createContact();
    insert ordwayContact;

    Account ordwayAccount = TestObjectFactory.createAccount();
    insert ordwayAccount;

    Map<Id, OrdwayContract__c> newOrdwayContractMap = new Map<Id, OrdwayContract__c>();
    Map<Id, OrdwayContract__c> oldOrdwayContractMap = new Map<Id, OrdwayContract__c>();

    OrdwayContract__c ordwayContract = new OrdwayContract__c();
    ordwayContract.BillingContact__c = ordwayContact.Id;
    ordwayContract.Account__c = ordwayAccount.Id;
    ordwayContract = TestObjectFactory.createOrdwayContract(ordwayContract);
    insert ordwayContract;

    Opportunity ordwayOpportunity = TestObjectFactory.createOpportunity();
    ordwayOpportunity.BillingContact__c = ordwayContact.Id;
    ordwayOpportunity.OrdwayOpportunityType__c = 'Upsell/Cross-sell';
    ordwayOpportunity.OrdwayContract__c = ordwayContract.Id;
    insert ordwayOpportunity;

    oldOrdwayContractMap.put(ordwayContract.Id, ordwayContract);
    ordwayContract.CustomerId__c = '123456';
    ordwayContract.OrdwaySubscriptionID__c = 'S-12344';
    update ordwayContract;
    ordwayContract.SyncedOpportunityId__c = ordwayOpportunity.Id;
    newOrdwayContractMap.put(ordwayContract.Id, ordwayContract);

    OpportunityLineItem ordwayOpportunityLineItem = new OpportunityLineItem();
    ordwayOpportunityLineItem.OpportunityId = ordwayOpportunity.Id;
    ordwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem(
      ordwayOpportunityLineItem
    );
    insert ordwayOpportunityLineItem;

    Test.startTest();
    OrdwayContractService.createOrdwayContract(
      new Set<Id>{ ordwayOpportunity.Id },
      new Map<Id, Opportunity>{ ordwayOpportunity.Id => ordwayOpportunity }
    );

    OrdwayContractService.populateBillingAddress(
      ordwayContract,
      new Map<Id, Contact>{ ordwayContact.Id => ordwayContact }
    );
    System.assertequals(
      ordwayContact.MailingCity,
      ordwayContract.BillingCity__c,
      'should return Contact Mailing city'
    );

    OrdwayContractService.updateOpportunity(
      newOrdwayContractMap,
      oldOrdwayContractMap
    );
    Opportunity thisOpportunity = [
      SELECT Id, OrdwayContract__c, OrdwayLabs__OrdwaySubscriptionID__c
      FROM Opportunity
      WHERE Id = :ordwayOpportunity.Id
    ];
    System.assertNotequals(
      null,
      thisOpportunity.OrdwayContract__c,
      'Ordway Contract should be created'
    );
    System.assertequals(
      ordwayContract.OrdwaySubscriptionID__c,
      thisOpportunity.OrdwayLabs__OrdwaySubscriptionID__c,
      'Should return S-12344'
    );

    Test.stopTest();
  }

  @isTest
  private static void populateSubscriptionId() {
    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    OrdwayEntityRule__c thisRule = TestObjectFactory.createOrdwayEntityRule();
    thisRule.EntityID__c = 'TEST';
    thisRule.Object__c = 'Opportunity';
    thisRule.Field__c = 'Name';
    insert thisRule;

    Contact ordwayContact = TestObjectFactory.createContact();
    insert ordwayContact;

    Account ordwayAccount = TestObjectFactory.createAccount();
    insert ordwayAccount;

    OpportunitySettingController.saveOpportunitySetting(
      TestObjectFactory.createContractPlanPickerSetting()
    );

    OrdwayContract__c ordwayContract = TestObjectFactory.createOrdwayContract();
    ordwayContract.BillingContact__c = ordwayContact.Id;
    ordwayContract.Account__c = ordwayAccount.Id;
    ordwayContract.OrdwaySubscriptionID__c = null;
    ordwayContract.EntityID__c = 'TEST';
    ordwayContract.ExternalId__c = 'TEST:S-0006';
    insert ordwayContract;
    OrdwayContract__c thisContract = [
      SELECT Id, OrdwaySubscriptionID__c
      FROM OrdwayContract__c
    ];
    system.assertEquals(
      'S-0006',
      thisContract.OrdwaySubscriptionID__c,
      'Should return same substring of External Id After :'
    );
  }

  @isTest
  private static void populateBillingAddress() {
    Contact ordwayContact = TestObjectFactory.createContact();
    insert ordwayContact;

    Account ordwayAccount = TestObjectFactory.createAccount();
    insert ordwayAccount;

    OpportunitySettingController.saveOpportunitySetting(
      TestObjectFactory.createContractPlanPickerSetting()
    );

    OrdwayContract__c ordwayContract = TestObjectFactory.createOrdwayContract();
    ordwayContract.BillingContact__c = null;
    ordwayContract.OrdwayContractStatus__c = 'Active';
    ordwayContract.Account__c = ordwayAccount.Id;
    ordwayContract.VersionType__c = 'Renewal';
    insert ordwayContract;
    ordwayContract.SyncStatus__c = 'Not Synced';
    update ordwayContract;
    OrdwayContract__c thisContract = [
      SELECT Id, BillingCountry__c
      FROM OrdwayContract__c
    ];
    system.assertEquals(
      null,
      thisContract.BillingCountry__c,
      'Should return null'
    );
  }
}