/**
 * @File Name          : OrdwayQuoteService.cls
 * @Description        :
 * @Author             : 
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 12-23-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/16/2020        Initial Version
 **/
public class OrdwayQuoteService {

   /**
   * @description Method to set the Default Renewal term for the Quote record
   * @param thisQuote Quote record
   **/
  public static void defaultRenewalTerm(Quote__c thisQuote) {
    if (thisQuote.ContractTerm__c == '-1' || thisQuote.ContractTerm__c == 'Evergreen') {
      thisQuote.RenewalTerm__c = null;
    }
  }

  /**
   * @description Populate Ordway Quote Id
   * @param thisQuote Ordway Quote
   **/
  public static void populateQuoteId(Quote__c thisQuote) {
    if (thisQuote.EntityID__c != null) {
      if (thisQuote.ExternalID__c != null) {
        thisQuote.OrdwayQuoteId__c = thisQuote.ExternalID__c.substringAfter(
          ':'
        );
        if (
          thisQuote.OrdwayQuoteId__c == null ||
          thisQuote.OrdwayQuoteId__c == ''
        ) {
          thisQuote.OrdwayQuoteId__c = thisQuote.ExternalID__c;
        }
        thisQuote.ExternalID__c =
          thisQuote.EntityID__c +
          ':' +
          thisQuote.OrdwayQuoteId__c;
      }
    } else {
      if (thisQuote.ExternalID__c != null) {
        thisQuote.OrdwayQuoteId__c = thisQuote.ExternalID__c;
      }
      if (
        thisQuote.ExternalID__c == null &&
        thisQuote.OrdwayQuoteId__c != null
      ) {
        thisQuote.ExternalID__c = thisQuote.OrdwayQuoteId__c;
      }
    }
  }

  /**
   * @description Method to Check whether Pricebook Removed
   * @param thisQuote Quote Object
   * @param oldQuoteMap Old Quote Map
   * @return Boolean True or false
   **/
  public static Boolean hasPricebookRemoved(
    Quote__c thisQuote,
    Map<Id, Quote__c> oldQuoteMap
  ) {
    return thisQuote.PriceBook2Id__c == null &&
      oldQuoteMap.get(thisQuote.Id).PriceBook2Id__c != null
      ? true
      : false;
  }

  /**
   * @description Method to whether Currency is changedgetOrdwayQuoteMap
   * @param thisQuote Quote Object
   * @param oldQuoteMap Old Quote Map
   * @return Boolean True or false
   **/
  public static Boolean isCurrencyChanged(
    Quote__c thisQuote,
    Map<Id, Quote__c> oldQuoteMap
  ) {
    return thisQuote.get('CurrencyIsoCode') !=
      oldQuoteMap.get(thisQuote.Id).get('CurrencyIsoCode')
      ? true
      : false;
  }

  /**
   * @description Method to populate default Quote Template
   * @param thisQuote Quote Object
   **/
  public static void populateDefaultQuoteTemplate(Quote__c thisQuote) {

    if(!OrdwayQuoteTemplateController.defaultQuoteTemplateMap.isEmpty()) {

      if(OpportunitySettingController.isMultiEntityEnabled()
      && thisQuote.EntityId__c != null
      && OrdwayQuoteTemplateController.defaultQuoteTemplateMap.get(thisQuote.EntityId__c) != null) {
        thisQuote.QuoteTemplate__c = OrdwayQuoteTemplateController.defaultQuoteTemplateMap.get(thisQuote.EntityId__c);
      }
      else if(!OpportunitySettingController.isMultiEntityEnabled()
      && !OrdwayQuoteTemplateController.defaultQuoteTemplateMap.isEmpty()) {
        thisQuote.QuoteTemplate__c = OrdwayQuoteTemplateController.defaultQuoteTemplateMap.values()[0];
      }
    }
  }

  /**
   * @description Method to Update Primary Checkbox
   * @param  syncedQuoteIds Synced Quote Id
   * @param  opportunityIds Set of Opportunity Ids
   */
  public static void updateSyncing(
    Set<Id> syncedQuoteIds,
    Set<Id> opportunityIds
  ) {
    List<Quote__c> quoteList = new List<Quote__c>();

    if (Schema.sObjectType.Quote__c.isAccessible()) {
      for (Quote__c thisQuote : [
        SELECT Id, IsPrimary__c
        FROM Quote__c
        WHERE Opportunity__c IN :opportunityIds
      ]) {
        thisQuote.IsPrimary__c = syncedQuoteIds.contains(thisQuote.Id)
          ? true
          : false;
        quoteList.add(thisQuote);
      }
    }

    QuoteTriggerHandler.runOnce = false; //Turn The Trigger Off
    QuoteTriggerHelper_Callout.runOnce = false;

    SObjectAccessDecision securityDecision = Security.stripInaccessible(
      AccessType.UPDATABLE,
      quoteList
    );
    update securityDecision.getRecords();
  }

  public static void processQuotePayload(String responseString) {

    //Turn Trigger Off - Do Not Make Any Calls To Ordway
    QuoteTriggerHelper_Callout.runOnce = false;

    Map<String, list<sObject>> sObjectMapToReturn = processQuoteResponse( responseString );
    
    if ( sObjectMapToReturn.containsKey(OrdwayHelper.SOBJECT_LIST_TO_UPDATE) ) {
      SObjectAccessDecision securityDecision = Security.stripInaccessible( AccessType.UPDATABLE, sObjectMapToReturn.get(OrdwayHelper.SOBJECT_LIST_TO_UPDATE) );
      update securityDecision.getRecords();
    }

    if (sObjectMapToReturn.containsKey(OrdwayHelper.SOBJECT_LIST_TO_DELETE) && Schema.sObjectType.QuoteLineItem__c.isDeletable() ) {
      delete sObjectMapToReturn.get(OrdwayHelper.SOBJECT_LIST_TO_DELETE);
    }
  }

  /**
   * @description Method to process Quote Http Response
   * @param responseString Http response body
   * @return Map<String, list<sObject>> Processed Quote Http Response Map
   **/
  public static Map<String, list<sObject>> processQuoteResponse(
    String responseString
  ) {
    Id ordwayQuoteId;
    Map<String, list<sObject>> sObjectMapToReturn = new Map<String, list<sObject>>();
    Map<Id, OrdwayLabs__QuoteLineItem__c> ordwayQuoteLineItemMap = new Map<Id, OrdwayLabs__QuoteLineItem__c>();

    Map<String, Object> thisProductObjectMap;
    SObject sObjectRecord;

    Map<String, Object> thisDeserializedMap = (Map<String, Object>) JSON.deserializeUntyped(
      responseString
    );
    sObjectRecord = OrdwayHelper.getSObjectRecord(
      thisDeserializedMap,
      Schema.sObjectType.Quote__c,
      new Quote__c()
    );
    sObjectRecord.put('SyncStatus__c', OrdwayHelper.SYNC_STATE_IN_SYNC);
    if (sObjectRecord.Id != null) {
      ordwayQuoteId = sObjectRecord.Id;
      OrdwayContractService.populateSObjectDMLMap(
        sObjectMapToReturn,
        OrdwayHelper.SOBJECT_LIST_TO_UPDATE,
        sObjectRecord
      );
    }

    ordwayQuoteLineItemMap = OrdwayQuoteLineItemService.getQuoteLineItemMapByQuoteIds(
      new Set<Id>{ ordwayQuoteId }
    );

    if (thisDeserializedMap.get('plans') != null) {
      for (
        Object thisProductObject : (List<Object>) thisDeserializedMap.get(
          'plans'
        )
      ) {
        thisProductObjectMap = (Map<String, Object>) thisProductObject;
        sObjectRecord = OrdwayHelper.getSObjectRecord(
          thisProductObjectMap,
          Schema.sObjectType.QuoteLineItem__c,
          new QuoteLineItem__c()
        );

        if (
          sObjectRecord.Id != null &&
          OrdwayHelper.isValidId(sObjectRecord.Id) &&
          ordwayQuoteLineItemMap.containsKey(sObjectRecord.Id)
        ) {
          //Record Exists In Salesforce And A Record Exists in Ordway With Salesforce Id
          ordwayQuoteLineItemMap.remove(sObjectRecord.Id);
          OrdwayContractService.populateSObjectDMLMap(
            sObjectMapToReturn,
            OrdwayHelper.SOBJECT_LIST_TO_UPDATE,
            sObjectRecord
          );
        }
      }

      if (
        !ordwayQuoteLineItemMap.isEmpty() &&
        Schema.sObjectType.QuoteLineItem__c.isDeletable()
      ) {
        sObjectMapToReturn.put(
          OrdwayHelper.SOBJECT_LIST_TO_DELETE,
          ordwayQuoteLineItemMap.values()
        );
      }
    }
    return sObjectMapToReturn;
  }

  /**
   * @description Sync Quote to Ordway
   * @param  thisOrdwayQuote thisOrdwayQuote Ordway Contract
   * @return HTTP Response
   */
  public static HttpResponse syncQuoteToOrdway(Quote__c thisOrdwayQuote) {
    HttpResponse thisResponse = OrdwayService.syncQuoteToOrdway(
      thisOrdwayQuote.Id
    );
    if (
      thisResponse.getStatusCode() >= 200 &&
      thisResponse.getStatusCode() <= 299
    ) {
      OrdwayHelper.getSObjectRecord(
        OrdwayHelper.getDeserializedUntyped(thisResponse),
        Schema.sObjectType.Quote__c,
        thisOrdwayQuote
      );
      setLastSyncDate(thisOrdwayQuote, OrdwayHelper.SYNC_STATE_IN_SYNC);
    } else {
      setLastSyncDate(thisOrdwayQuote, OrdwayHelper.SYNC_STATE_SYNC_FAILED);
    }

    QuoteTriggerHelper_Callout.runOnce = false;
    SObjectAccessDecision securityDecision = Security.stripInaccessible(
      AccessType.UPDATABLE,
      new List<Quote__c>{ thisOrdwayQuote }
    );
    upsert securityDecision.getRecords();

    return thisResponse;
  }

  /**
   * @description Method to populate Last Sync Date
   * @param thisQuote Ordway Quote
   * @param syncStatus Ordway Sync Status
   **/
  private static void setLastSyncDate(Quote__c thisQuote, String syncStatus) {
    thisQuote.OrdwayLastSyncDate__c = System.now();
    thisQuote.SyncStatus__c = syncStatus;
  }

  /**
   * @description  Method to Sync Quote and QuoteLineItem with Opportunity and Opportunity Line Item
   * @param quoteTriggerNewMap Quote Trigger New Map
   * @param opportunityIdToBeSynced Set of Opportunity Ids which has to be synced with respective Quote
   */
  public static void syncQuoteToOpportunity( Set<Id> completedQuoteIds, Map<Id, OrdwayLabs__Quote__c> quoteTriggerNewMap, Set<Id> opportunityIdToBeSynced ) {

    ApplicationLog.StartLoggingContext( 'OrdwayQuoteService', 'syncQuoteToOpportunity()' );
    //Synced Opportunity Records which needs to be updated
    Map<Id, Opportunity> syncedOpportunitiesMapToUpdate = OpportunityService.getOpportunityMap(
      opportunityIdToBeSynced
    );
    Opportunity thisOpportunityToSync;

    for (Id thisCompletedQuoteId : completedQuoteIds) {
      thisOpportunityToSync = syncedOpportunitiesMapToUpdate.get( quoteTriggerNewMap.get(thisCompletedQuoteId).Opportunity__c );
      thisOpportunityToSync.SyncedQuoteId__c = thisCompletedQuoteId;
      populateQuoteValuesToOpportunity(
        quoteTriggerNewMap.get(thisCompletedQuoteId),
        thisOpportunityToSync
      );
      Map<String, Object> oppMap = new Map<String, Object>( thisOpportunityToSync.getPopulatedFieldsAsMap() );
      oppMap.remove('SyncedQuoteId');
      syncedOpportunitiesMapToUpdate.put( thisOpportunityToSync.Id, (Opportunity) JSON.deserialize( JSON.serialize( oppMap ), Opportunity.class ) );
    }

    try {
      OpportunityTriggerHandler.syncedOpportunityIds.addAll( opportunityIdToBeSynced );

      SObjectAccessDecision securityDecision = Security.stripInaccessible(
        AccessType.UPDATABLE,
        syncedOpportunitiesMapToUpdate.values()
      );

      update securityDecision.getRecords();
      
      OrdwayQuoteLineItemService.syncQuoteLineItemsToOpportunityLineItems(
        completedQuoteIds,
        opportunityIdToBeSynced
      );
    } catch (Exception e) {
      ApplicationLog.Error(e);
      ApplicationLog.FinishLoggingContext();
    }
  }

  /**
   * @description  Method to Dynamically populate Quote values to Opportunity
   * @param thisOrdwayQuote Ordway Quote whose values needs to be populated to Opportunity
   * @param thisOpportunity Opportunity record which needs to be updated with Quote values
   */
  public static void populateQuoteValuesToOpportunity(
    OrdwayLabs__Quote__c thisOrdwayQuote,
    Opportunity thisOpportunity
  ) {
    thisOpportunity = (Opportunity) SObjectFieldMappingHelper.dynamicSobjectMapping(
      thisOrdwayQuote,
      thisOpportunity,
      true
    );
  }

  /**
   *  @description  Method to Dynamically populate Opportunity values to Ordway Quote
   * @param thisOrdwayQuote Ordway Quote which needs to be updated with opportunity values
   * @param thisOpportunity Opportunity record whose values needs to be populated to Quote
   */
  public static void populateOpportunityValuesToQuote(
    OrdwayLabs__Quote__c thisOrdwayQuote,
    Opportunity thisOpportunity
  ) {
    thisOrdwayQuote = (OrdwayLabs__Quote__c) SObjectFieldMappingHelper.dynamicSobjectMapping(
      thisOpportunity,
      thisOrdwayQuote,
      false
    );
  }

  /**
   * @description  Method to get Ordway Quote Map
   * @param  quoteIds Set of Quote Id's
   * @return Map of Ordway Quote
   */
  public static Map<Id, OrdwayLabs__Quote__c> getOrdwayQuoteMap(
    Set<Id> quoteIds
  ) {
    Map<Id, OrdwayLabs__Quote__c> thisOrdwayQuoteMap = new Map<Id, OrdwayLabs__Quote__c>();
    if (Schema.sObjectType.OrdwayLabs__Quote__c.isAccessible()) {
      thisOrdwayQuoteMap = new Map<Id, OrdwayLabs__Quote__c>(
        (List<OrdwayLabs__Quote__c>) Database.query(
          'SELECT ' +
          String.join(
            SObjectHelper.getAllFields(
              Schema.OrdwayLabs__Quote__c.getSObjectType()
            ),
            ','
          ) +
          ' FROM OrdwayLabs__Quote__c WHERE Id IN:quoteIds'
        )
      );
    }
    return thisOrdwayQuoteMap;
  }

  /**
   * @description  Method to delete Ordway Quotes
   * @param  opportunityIds Set of opportunity Id's
   */
  public static void deleteOrdwayQuotes(Set<Id> opportunityIds) {
    //False Positive - As Quote is Protected, CheckMarx scanner will flag this as an issue
    List<Quote__c> qList = new List<Quote__c>([SELECT Id FROM Quote__c WHERE Opportunity__c IN : opportunityIds]);
    if ( Quote__c.sObjectType.getDescribe().isDeletable() ) {
      delete qList;
    }
  }
}