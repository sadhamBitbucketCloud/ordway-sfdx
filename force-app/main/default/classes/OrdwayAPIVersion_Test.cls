/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 09-19-2020
 * @last modified by  : 
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   09-19-2020      Initial Version
**/
@isTest
private class OrdwayAPIVersion_Test {
  @isTest
  private static void OrdwayAPIVersion_Test() {
    Test.startTest();
    Map<String, String> responseMap = OrdwayAPIVersion.doGet();
    Test.stopTest();
    System.assertNotEquals(null, responseMap, 'should not return null');
  }
}