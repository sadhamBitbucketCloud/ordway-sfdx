/**
 * @File Name          : PostInstallPackage.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    9/28/2019   Ordway Labs     Initial Version
 **/
global with sharing class PostInstallPackage implements InstallHandler {

  global void onInstall(InstallContext context) {
    //If package is being installed for the first time, then create the custom setting with the default values
    if (context.previousVersion() == null) {
      createApplicationSetting();
      createContractPlanPickerSetting();
      createLoadLeanSettings();
    } else {
      if(OrdwayAPIVersion.packageVersion().get('Version') == '3.0.0') {
        updatePlan(); //Populate External ID field - New Field Introduced in v2.9.0
        updateQLILineId(); //Ordway Line Id - New Field to be populated
      }
    }
  }

  @TestVisible
  private static void updateQLILineId() {
    //Field Required for Tiers to work
    if(OpportunitySettingController.isQuotesEnabled()) {
      List<QuoteLineItem__c> qList = new List<QuoteLineItem__c>();
      for(QuoteLineItem__c qRecord : [SELECT Id, ExternalID__c, OrdwayLineId__c FROM QuoteLineItem__c WHERE ExternalID__c = null]) {
        qRecord.ExternalID__c = qRecord.Id;
        qRecord.OrdwayLineId__c = qRecord.Id;
        qList.add(qRecord);
      }
      update qList;
    }
  }

  @TestVisible
  private static void updatePlan() {
    List<Plan__c> plans = new List<Plan__c>();

    for(Plan__c p : [SELECT Id, ExternalID__c, PlanId__c, EntityID__c FROM Plan__c WHERE ExternalID__c = null]) {
      if( String.isNotBlank(p.EntityID__c) && String.isNotBlank(p.PlanId__c) ) {
        p.put(Schema.Plan__c.ExternalID__c, p.EntityID__c+':'+p.PlanId__c);
        plans.add(p);
      }
      else if(String.isNotBlank(p.PlanId__c)) {
        p.put(Schema.Plan__c.ExternalID__c, p.PlanId__c);
        plans.add(p);
      }
    }
    Update plans;
  }

  /**
   * createApplicationSetting Create Application Setting With Default Values On Install
   */
  @TestVisible
  public static void createApplicationSetting() {
    //Is Sandbox or Production
    String installingOrganization = OrganizationController.isSandbox()
      ? OrdwayHelper.ORDWAY_SANDBOX
      : OrdwayHelper.ORDWAY_PRODUCTION;

    ApplicationSetting__c applicationSetting = new ApplicationSetting__c(
      Name = OrdwayHelper.ORDWAY_APPLICATION_SETTING,
      OrdwayContactURL__c = OrdwayHelper.ORDWAY_CONTACT_URL,
      Company__c = OrganizationController.getOrganizationName(),
      Environment__c = installingOrganization,
      Token__c = 'Enter your token'
    );

    if (ApplicationSetting__c.sObjectType.getDescribe().isCreateable()) {
      insert applicationSetting;
    }
  }

  /**
   * createContractPlanPickerSetting Create Custom Setting Record With Default Values
   */
  @TestVisible
  public static void createContractPlanPickerSetting() {
    ContractPlanPickerSetting__c planPickerSetting = new ContractPlanPickerSetting__c(
      AutomaticallyCreateSubscription__c = false,
      LinkSubscriptionAutomatically__c = false,
      UseOrdwayPrice__c = false,
      UseOrdwayPricingCalculation__c = false,
      Trigger__c = OrdwayHelper.SETTING_TRIGGER,
      LastModifiedBy__c = UserInfo.getUserId(),
      Name = OrdwayHelper.OPPORTUNITY_SETTING_NAME,
      OpportunityStage__c = OrdwayHelper.DEFAULT_OPPORTUNITY_STAGE,
      EnableQuotes__c = false,
      AutoSyncQuotefromOrdway__c = false,
      SyncQuoteAutomatically__c = false
    );

    if (ContractPlanPickerSetting__c.sObjectType.getDescribe().isCreateable()) {
      insert planPickerSetting;
    }
  }

  @TestVisible
  private static void createLoadLeanSettings() {
    LoadLeanSettings__c ll = new LoadLeanSettings__c();
    ll.PlanPicker__c = true;
    ll.SetupOwnerId = UserInfo.getOrganizationId();
    insert ll;
  }
}