/**
 * @File Name          : OpportunityLineItemTriggerHandler_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   :
 * @Last Modified On   : 5/25/2020, 9:19:24 PM
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    12/31/2019   Ordway Labs     Initial Version
 **/
@isTest
private class OpportunityLineItemTriggerHandler_Test {
  @isTest
  private static void Test_OpportunityLineItemTriggerHandler() {
    Opportunity ordwayOpportunity = TestObjectFactory.createOpportunity();
    insert ordwayOpportunity;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    insert newSetting;

    OpportunityLineItem ordwayOpportunityLineItem = new OpportunityLineItem();
    ordwayOpportunityLineItem.OpportunityId = ordwayOpportunity.Id;
    ordwayOpportunityLineItem.UnitPrice = 100;
    ordwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem(
      ordwayOpportunityLineItem
    );
    insert ordwayOpportunityLineItem;

    Test.startTest();
    ordwayOpportunityLineItem.DiscountedContractValue__c = 2;
    ordwayOpportunityLineItem.ChargeType__c = 'One Time';
    update ordwayOpportunityLineItem;
    delete ordwayOpportunityLineItem;
    Test.stopTest();

    System.assertEquals(100, ordwayOpportunityLineItem.UnitPrice);
  }
}