/**
 * @File Name          : OrganizationController_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    12/31/2019   Ordway Labs     Initial Version
 **/
@isTest
private class OrganizationController_Test {
  @isTest
  private static void Test_OrgController() {
    Test.startTest();
    OrganizationController.isSandbox();

    System.assertEquals(
      OrganizationController.getOrganizationId(),
      [SELECT Id FROM Organization]
      .Id,
      'Should return the Organization Id of the Salesforce instance'
    );

    System.assertEquals(
      OrganizationController.getOrganizationName(),
      [SELECT Name FROM Organization]
      .Name,
      'Should return the Organization Name of the Salesforce instance'
    );

    Test.stopTest();
  }
}