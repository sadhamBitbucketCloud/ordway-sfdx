/**
 * @File Name          : DeleteApplicationLog_Scheduler.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 03-16-2021
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 *         3/13/2021        Ordway Labs            Initial Version
 **/
global with sharing class DeleteApplicationLog_Scheduler implements Schedulable {
  global void execute(SchedulableContext sc) {
    ApplicationLog.deleteLogs();                                                                         
  }
}