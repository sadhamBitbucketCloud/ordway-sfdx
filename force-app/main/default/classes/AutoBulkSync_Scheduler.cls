/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 12-10-2020
 * @last modified by  : 
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   12-10-2020      Initial Version
**/
global with sharing class AutoBulkSync_Scheduler implements Schedulable{
    global void execute(SchedulableContext sc) {
        List<BulkSyncConfiguration__c> thisBulkSyncRecords = new List<BulkSyncConfiguration__c>([SELECT Entity__c, Filter__c,
                                                                                                        Object__c, OrderNumber__c 
                                                                                                        FROM BulkSyncConfiguration__c
                                                                                                        ORDER BY OrderNumber__c, Entity__c ASC]);
        if(!thisBulkSyncRecords.isEmpty()){
            Database.executebatch(new GetOrdwayObject_Batch(0, thisBulkSyncRecords[0].Object__c, GetOrdwayObject_Batch.getFilterString(Integer.valueOf(thisBulkSyncRecords[0].Filter__c)), thisBulkSyncRecords[0].Entity__c, thisBulkSyncRecords));
        }                                                                                          
    }
}