/**
 * @File Name          : GetOrdwayPlan_Scheduler.cls
 * @Description        : Scheduler class for GetOrdwayPlan_Batch Batch
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    3/7/2020   Ordway Labs     Initial Version
 **/

global with sharing class GetOrdwayPlan_Scheduler implements Schedulable {
  /**
   * @description Scheduler class for GetOrdwayPlan_Batch Batch
   **/
  global void execute(SchedulableContext sc) {
    Database.executebatch(new GetOrdwayPlan_Batch(1, null));
  }
}