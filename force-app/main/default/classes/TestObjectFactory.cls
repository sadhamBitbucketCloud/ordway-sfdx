/**
 * @File Name          : TestObjectFactory.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-18-2021
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    10/13/2019   Ordway Labs     Initial Version
 **/
@isTest
public class TestObjectFactory {
  private static Map<String, SObject> mapTestsObjects = new Map<String, SObject>();
  private static Map<String, SObject> mapDefaultValuesBySObjectName = new Map<String, SObject>();
  private static Map<String, Map<String, Object>> mapTestDefaults = new Map<String, Map<String, Object>>{
    'User' => new Map<String, Object>{
      'Alias' => 'standt',
      'FirstName' => 'Test User{!autonum}',
      'LastName' => 'Tester',
      'Email' => 'testuser{!autonum}@test.com',
      'UserName' => OrganizationController.getOrganizationId() +
      '-testuser{!autonum}@test.com.testing',
      'EmailEncodingKey' => 'UTF-8',
      'LanguageLocaleKey' => 'en_US',
      'LocaleSidKey' => 'en_US',
      'TimeZoneSidKey' => 'Europe/London',
      'DigestFrequency' => null
    },
    'Contact' => new Map<String, Object>{
      'FirstName' => 'Contact{!autonum}',
      'LastName' => 'Test',
      'Email' => 'testcontact{!autonum}@test.com', // Validation rule Email or phone
      'Phone' => '0123456789{!autonum}',
      'MailingStreet' => '123 Test Street',
      'MailingCity' => 'Test City',
      'MailingState' => 'Test State',
      'MailingPostalCode' => 'TE3 3ST',
      'MailingCountry' => 'United Kingdom'
    },
    'Account' => new Map<String, Object>{
      OrdwayHelper.SOBJECT_FIELD_NAME => 'TestingCountry{!autonum}',
      'BillingCountry' => 'United Kingdom',
      'ShippingCountry' => 'United Kingdom',
      'BillingStreet' => '123 Test Street',
      'BillingState' => 'Test State',
      'BillingCity' => 'Test City',
      'BillingPostalCode' => 'TE3 3ST'
    },
    'Pricebook2' => new Map<String, Object>{
      'IsActive' => true,
      OrdwayHelper.SOBJECT_FIELD_NAME => 'Ordway Pricebook {!autonum}'
    },
    'Product2' => new Map<String, Object>{ 'isActive' => true },
    'PricebookEntry' => new Map<String, Object>{
      'isActive' => true,
      'UnitPrice' => 900
    },
    'Opportunity' => new Map<String, Object>{
      OrdwayHelper.SOBJECT_FIELD_NAME => 'Temp',
      'CloseDate' => Date.today().addDays(120)
    },
    'OpportunityLineItem' => new Map<String, Object>{ 'Quantity' => 1 },
    'OrdwayLabs__ApplicationSetting__c' => new Map<String, Object>{
      OrdwayHelper.SOBJECT_FIELD_NAME => OrdwayHelper.ORDWAY_APPLICATION_SETTING,
      'OrdwayContactURL__c' => OrdwayHelper.ORDWAY_CONTACT_URL,
      'Company__c' => OrganizationController.getOrganizationName(),
      'Environment__c' => OrdwayHelper.ORDWAY_SANDBOX,
      'Email__c' => UserInfo.getUserEmail(),
      'Token__c' => 'Cloudiate'
    },
    'OrdwayLabs__ContractPlanPickerSetting__c' => new Map<String, Object>{
      'AutomaticallyCreateSubscription__c' => true,
      'LinkSubscriptionAutomatically__c' => false,
      'AutoSyncQuotefromOrdway__c' => false,
      'SyncQuoteAutomatically__c' => false,
      'AutomaticallyActivateContract__c' => false,
      'UseOrdwayPrice__c' => true,
      'AutomaticallySyncAccount__c' => true,
      'AutomaticallySyncContact__c' => true,
      'UseOrdwayPricingCalculation__c' => true,
      'Trigger__c' => OrdwayHelper.SETTING_TRIGGER,
      'LastModifiedBy__c' => UserInfo.getUserId(),
      OrdwayHelper.SOBJECT_FIELD_NAME => OrdwayHelper.OPPORTUNITY_SETTING_NAME,
      'OpportunityStage__c' => OrdwayHelper.DEFAULT_OPPORTUNITY_STAGE,
      'PriceBook2Id__c' => Test.getStandardPriceBookId()
    },
    'OrdwayLabs__LoadLeanSettings__c' => new Map<String, Object>{
      'CreateApplicationLogSynchronously__c' => true,
      'DisableTriggers__c' => true,
      'DisableValidationRules__c' => true,
      'AllLogs__c' => true,
      'SetUpOwnerId' => UserInfo.getProfileId()
    },
    'OrdwayLabs__BulkSyncConfiguration__c' => new Map<String, Object>{
      'Filter__c' => '10',
      'OrderNumber__c' => 1.0,
      'Object__c' => 'Account'
    },
    'OrdwayLabs__PlanPicker__mdt'   => new Map<String, Object>{
      'MasterLabel'                 => 'Label',
      'DeveloperName'               => 'DeveloperName'
    },
     'OrdwayLabs__OrdwayContract__c' => new Map<String, Object>{
      'ContractEffectiveDate__c' => system.today(),
      'ServiceStartDate__c' => system.today(),
      'BillingStartDate__c' => system.today(),
      'OrdwaySubscriptionID__c' => 'Sub-123'
    },
    'OrdwayLabs__ContractLineItem__c' => new Map<String, Object>{
      OrdwayHelper.SOBJECT_FIELD_NAME => 'Test'
    },
    'OrdwayLabs__OrdwayProduct__c'  => new Map<String, Object>{
    },
    'OrdwayLabs__ContractLineItemTier__c' => new Map<String, Object>{
      'StartingUnit__c' => 5  
    },
    'OrdwayLabs__QuoteTemplate__c' => new Map<String, Object>{
      OrdwayHelper.SOBJECT_FIELD_NAME => 'Test',
      'Description__c' => 'Test Description',
      'Active__c' => true
    },
    'OrdwayLabs__Quote__c' => new Map<String, Object>{
      OrdwayHelper.SOBJECT_FIELD_NAME => 'Test Quote',
      'QuoteDate__c' => system.today(),
      'ExpiryDate__c' => system.today().addMonths(1)
    },
    'OrdwayLabs__QuoteLineItem__c' => new Map<String, Object>{
    },
    'OrdwayLabs__QuoteLineItemTier__c' => new Map<String, Object>{
      'OrdwayLabs__ListPrice__c' => 5,
      'OrdwayLabs__StartingUnit__c' => 5,
      'OrdwayLabs__EndingUnit__c' => 10
    },
    'OrdwayLabs__Charge__c' => new Map<String, Object>{
      'Name'              => 'Test',
      'PricingModel__c'   =>'Per Unit',
      'ChargeType__c'     => 'Recurring'
    },
    'OrdwayLabs__ChargeTier__c' => new Map<String, Object>{
      'StartingUnit__c' => 123
    },
    'OrdwayLabs__OpportunityLineItemTier__c' => new Map<String, Object>{
      'StartingUnit__c' => 123
    },
    'OrdwayLabs__Invoice__c' => new Map<String, Object>{
      OrdwayHelper.SOBJECT_FIELD_NAME => 'Test Invoice'
    },
    'OrdwayLabs__Payment__c' => new Map<String, Object>{
      OrdwayHelper.SOBJECT_FIELD_NAME => 'Test Payment'
    },
    'OrdwayLabs__Entity__c' => new Map<String, Object>{
      OrdwayHelper.SOBJECT_FIELD_NAME => '0'
    },
    'OrdwayEntityRule__c' => new Map<String, Object>{
      OrdwayHelper.SOBJECT_FIELD_NAME => '0'
    },
    'OrdwayLabs__InvoiceLineItem__c' => new Map<String, Object>{
      OrdwayHelper.SOBJECT_FIELD_NAME => 'Test Invoice Line Item'
    },
    'OrdwayLabs__OrdwayUsage__c' => new Map<String, Object>{
      OrdwayHelper.SOBJECT_FIELD_NAME => 'Test Usage'
    },
    'OrdwayLabs__Plan__c' => new Map<String, Object>{
      'OrdwayLabs__PlanId__c' => 'PLN-00008',
      OrdwayHelper.SOBJECT_FIELD_NAME => 'volume-test',
      'OrdwayLabs__Active__c' => true,
      'OrdwayLabs__PlanDetails__c' => '{' +
      '"has_sf_id": true,' +
      '"custom_fields": {' +
      '  "has_sf_id": true' +
      '},' +
      '"public_url": "",' +
      '"charges": [{' +
      '"salesforce_id": "testPriceBookEntrySalesforceId",' +
      '"test-cust": "",' +
      '"custom_fields": {' +
      '"salesforce_id": "testPriceBookEntrySalesforceId",' +
      '"test-cust": ""' +
      '},' +
      '"included_units": "0.0",' +
      '"unit_of_measure": "Users",' +
      '"billing_day": "Align with Billing Period",' +
      '"billing_period_start_alignment": "Service Start Date",' +
      '"billing_period": "Monthly",' +
      '"list_price": "25.00",' +
      '"pricing_model": "Per unit",' +
      '"refund_units_on_cancel": false,' +
      '"prepaid_units_expiry": "contract_term_expiry",' +
      '"refill_prepaid_units_for": "contract_renewal_refill",' +
      ' "refill_qty": 0,' +
      ' "auto_refill_prepayment_units": false,' +
      ' "prepaid_units_enable": false,' +
      ' "prepayment_amount": 0.0,' +
      ' "type": "Usage based",' +
      ' "description": "",' +
      ' "name": "usage",' +
      ' "product_id": "P-00001",' +
      ' "id": "CHG-00003"' +
      '}],' +
      '"updated_date": "2019-12-05T17:43:39.813Z",' +
      '"created_date": "2019-12-05T17:43:39.813Z",' +
      '"updated_by": "jason.toups@ordwaylabs.com",' +
      ' "created_by": "jason.toups@ordwaylabs.com",' +
      ' "description": "",' +
      ' "status": "Active",' +
      ' "name": "Usage",' +
      ' "id": "PLN-00003"' +
      '}'
    }
  };

  private static Integer testAutoNum {
    get {
      if (testAutoNum == null) {
        testAutoNum = 0;
      }
      return testAutoNum++;
    }
    set;
  }

  public static Boolean isInsert = false;

  public static SObject createSObject(SObject values) {
    return TestObjectFactory.createSObject(values, false);
  }

  public static User createUser(String profileName) {
    return TestObjectFactory.createUser(new User(), profileName);
  }

  public static User createUser(User theUser, String profileName) {
    theUser = (User) TestObjectFactory.mergeValuesWithDefaults(theUser);

    if (
      !theUser.getPopulatedFieldsAsMap().containsKey('ProfileId') ||
      String.isNotBlank(profileName)
    ) {
      SObject testProfile = TestObjectFactory.getTestSObject(
        'User',
        profileName
      );
      if (testProfile == null) {
        testProfile = [SELECT Id, Name FROM Profile WHERE Name = :profileName];
        addTestSObject(testProfile);
      }
      theUser.put('ProfileId', testProfile.Id);
    }

    return (User) TestObjectFactory.createSObject(theUser, true);
  }

  public static Pricebook2 createPricebook() {
    return TestObjectFactory.createPricebook(new Pricebook2());
  }

  public static Pricebook2 createPricebook(Pricebook2 priceBookValues) {
    priceBookValues = (Pricebook2) TestObjectFactory.mergeValuesWithDefaults(
      priceBookValues
    );
    return (Pricebook2) TestObjectFactory.createSObject(priceBookValues, true);
  }

  public static Product2 createProduct() {
    return TestObjectFactory.createProduct(new Product2());
  }

  public static Product2 createProduct(Product2 productValues) {
    return (Product2) TestObjectFactory.createSObject(productValues, false);
  }

  public static PricebookEntry createPriceBookEntry() {
    return TestObjectFactory.createPriceBookEntry(
      new PricebookEntry(),
      'TestDefault'
    );
  }

  public static PricebookEntry createPriceBookEntry(
    PricebookEntry priceBookEntryValues
  ) {
    return TestObjectFactory.createPriceBookEntry(
      priceBookEntryValues,
      'TestDefault'
    );
  }

  public static PricebookEntry createPriceBookEntry(
    PricebookEntry priceBookEntryValues,
    String testName
  ) {
    priceBookEntryValues = (PricebookEntry) TestObjectFactory.mergeValuesWithDefaults(
      priceBookEntryValues
    );

    Set<String> popedFields = priceBookEntryValues.getPopulatedFieldsAsMap()
      .keySet();
    if (!popedFields.contains('Pricebook2Id')) {
      SObject testPriceBook = TestObjectFactory.getTestSObject(
        'PriceBook2',
        'TestDefault'
      );
      if (testPriceBook == null) {
        testPriceBook = TestObjectFactory.createPriceBook(
          new PriceBook2(Name = 'TestDefault')
        );
        insert testPriceBook;
      }
      priceBookEntryValues.Pricebook2Id = testPriceBook.Id;
    }
    if (!popedFields.contains('Product2Id')) {
      SObject testPriceProduct = TestObjectFactory.getTestSObject(
        'Product2',
        'TestDefault'
      );
      if (testPriceProduct == null) {
        testPriceProduct = TestObjectFactory.createProduct(
          new Product2(Name = 'TestDefault')
        );
        insert testPriceProduct;

        insert TestObjectFactory.createPriceBookEntry(
          new PriceBookEntry(
            Pricebook2Id = Test.getStandardPricebookId(),
            Product2Id = testPriceProduct.Id
          )
        );
      }
      priceBookEntryValues.Product2Id = testPriceProduct.Id;
    }

    return (PricebookEntry) TestObjectFactory.createSObject(
      priceBookEntryValues,
      testName,
      true
    );
  }

  public static List<PricebookEntry> createPriceBookEntries(
    PricebookEntry pricebookEntryValues,
    Integer numberOf
  ) {
    return TestObjectFactory.createPriceBookEntries(
      pricebookEntryValues,
      'TestDefault',
      numberOf
    );
  }

  public static List<PricebookEntry> createPriceBookEntries(
    PricebookEntry pricebookEntryValues,
    String testName,
    Integer numberOf
  ) {
    List<PricebookEntry> result = new List<PricebookEntry>();
    for (Integer i = 0; i < numberOf; ++i) {
      PricebookEntry pricebookEntryUserSetValues = pricebookEntryValues.clone();
      result.add(
        TestObjectFactory.createPriceBookEntry(
          pricebookEntryUserSetValues,
          testName + i
        )
      );
    }
    return result;
  }

  public static Contact createContact() {
    return TestObjectFactory.createContact(new Contact());
  }
  public static Contact createContact(Contact contactValues) {
    contactValues = (Contact) TestObjectFactory.mergeValuesWithDefaults(
      contactValues
    );
    return (Contact) TestObjectFactory.createSObject(
      contactValues,
      contactValues.FirstName,
      true
    );
  }

  public static Account createAccount() {
    return TestObjectFactory.createAccount(new Account());
  }

  public static Account createAccount(Account accountValues) {
    accountValues = (Account) TestObjectFactory.mergeValuesWithDefaults(
      accountValues
    );
    return (Account) TestObjectFactory.createSObject(accountValues, true);
  }

  public static Opportunity createOpportunity() {
    return TestObjectFactory.createOpportunity(new Opportunity());
  }

  public static Opportunity createOpportunity(Opportunity opportunityValues) {
    opportunityValues = (Opportunity) TestObjectFactory.mergeValuesWithDefaults(
      opportunityValues
    );

    Set<String> popedFields = opportunityValues.getPopulatedFieldsAsMap()
      .keySet();

    if (!popedFields.contains('StageName')) {
      opportunityValues.Stagename = TestObjectFactory.getFirstPicklistValue(
        Opportunity.getSObjectType(),
        'StageName'
      );
    }

    if (!popedFields.contains('AccountId')) {
      SObject testAccount = TestObjectFactory.getTestSObject(
        'Account',
        'TestDefault'
      );
      if (testAccount == null) {
        testAccount = TestObjectFactory.createAccount(
          new Account(Name = 'TestDefault')
        );
        insert testAccount;
      }
      opportunityValues.AccountId = testAccount.Id;
    }

    return (Opportunity) TestObjectFactory.createSObject(
      opportunityValues,
      true
    );
  }

  public static OpportunityLineItem createOpportunityLineItem() {
    return TestObjectFactory.createOpportunityLineItem(
      new OpportunityLineItem()
    );
  }

  public static OpportunityLineItem createOpportunityLineItem(
    OpportunityLineItem opportunityLineItemValues
  ) {
    opportunityLineItemValues = (OpportunityLineItem) TestObjectFactory.mergeValuesWithDefaults(
      opportunityLineItemValues
    );

    Set<String> popedFields = opportunityLineItemValues.getPopulatedFieldsAsMap()
      .keySet();

    if (!popedFields.contains('OpportunityId')) {
      SObject testOpportunity = TestObjectFactory.getTestSObject(
        'Opportunity',
        'TestDefault'
      );
      if (testOpportunity == null) {
        testOpportunity = TestObjectFactory.createOpportunity(
          new Opportunity(Name = 'TestDefault')
        );
        insert testOpportunity;
      }
      opportunityLineItemValues.OpportunityId = testOpportunity.Id;
    }
    if (!popedFields.contains('PricebookEntryId')) {
      SObject testPricebookEntry = TestObjectFactory.getTestSObject(
        'PricebookEntry',
        'TestDefault'
      );
      if (testPricebookEntry == null) {
        testPricebookEntry = TestObjectFactory.createPriceBookEntry();
        insert testPricebookEntry;
      }
      opportunityLineItemValues.PriceBookEntryID = testPricebookEntry.Id;
    }

    return (OpportunityLineItem) TestObjectFactory.createSObject(
      opportunityLineItemValues,
      true
    );
  }

  public static List<OpportunityLineItem> createOpportunityLineItems(
    OpportunityLineItem opportunityLineItemValues,
    Integer numberOf
  ) {
    List<SObject> lstPriceBookEntries = TestObjectFactory.createPriceBookEntries(
      new PriceBookEntry(),
      'TestDefault',
      numberOf
    );
    insert lstPriceBookEntries;

    List<OpportunityLineItem> result = new List<OpportunityLineItem>();
    for (Integer i = 0; i < numberOf; ++i) {
      OpportunityLineItem opportunityLineItemUserSetValues = opportunityLineItemValues.clone();
      opportunityLineItemUserSetValues.PriceBookEntryId = lstPriceBookEntries[i]
        .Id;
      result.add(
        TestObjectFactory.createOpportunityLineItem(
          opportunityLineItemUserSetValues
        )
      );
    }
    return result;
  }

  /* CREATE CUSTOM OBJECT RECORDS => CREATE CUSTOM OBJECT RECORDS */

  public static OrdwayLabs__Plan__c createOrdwayPlan() {
    return TestObjectFactory.createOrdwayPlan(new OrdwayLabs__Plan__c());
  }
  public static OrdwayLabs__Plan__c createOrdwayPlan(
    OrdwayLabs__Plan__c ordwayPlan
  ) {
    ordwayPlan = (OrdwayLabs__Plan__c) TestObjectFactory.mergeValuesWithDefaults(
      ordwayPlan
    );
    return (OrdwayLabs__Plan__c) TestObjectFactory.createSObject(
      ordwayPlan,
      true
    );
  }

  public static OrdwayLabs__Invoice__c createOrdwayInvoice() {
    return TestObjectFactory.createOrdwayInvoice(new OrdwayLabs__Invoice__c());
  }
  public static OrdwayLabs__Invoice__c createOrdwayInvoice(
    OrdwayLabs__Invoice__c ordwayInvoice
  ) {
    ordwayInvoice = (OrdwayLabs__Invoice__c) TestObjectFactory.mergeValuesWithDefaults(
      ordwayInvoice
    );
    return (OrdwayLabs__Invoice__c) TestObjectFactory.createSObject(
      ordwayInvoice,
      true
    );
  }

  public static OrdwayLabs__Payment__c createOrdwayPayment() {
    return TestObjectFactory.createOrdwayPayment(new OrdwayLabs__Payment__c());
  }
  public static OrdwayLabs__Payment__c createOrdwayPayment(
    OrdwayLabs__Payment__c ordwayPayment
  ) {
    ordwayPayment = (OrdwayLabs__Payment__c) TestObjectFactory.mergeValuesWithDefaults(
      ordwayPayment
    );
    return (OrdwayLabs__Payment__c) TestObjectFactory.createSObject(
      ordwayPayment,
      true
    );
  }

  public static OrdwayLabs__Entity__c createOrdwayEntity() {
    return TestObjectFactory.createOrdwayEntity(new OrdwayLabs__Entity__c());
  }
  public static OrdwayLabs__Entity__c createOrdwayEntity(
    OrdwayLabs__Entity__c ordwayEntity
  ) {
    ordwayEntity = (OrdwayLabs__Entity__c) TestObjectFactory.mergeValuesWithDefaults(
      ordwayEntity
    );
    return (OrdwayLabs__Entity__c) TestObjectFactory.createSObject(
      ordwayEntity,
      true
    );
  }

  public static OrdwayLabs__OrdwayEntityRule__c createOrdwayEntityRule() {
    return TestObjectFactory.createOrdwayEntityRule(
      new OrdwayLabs__OrdwayEntityRule__c()
    );
  }
  public static OrdwayLabs__OrdwayEntityRule__c createOrdwayEntityRule(
    OrdwayLabs__OrdwayEntityRule__c ordwayEntityRule
  ) {
    ordwayEntityRule = (OrdwayLabs__OrdwayEntityRule__c) TestObjectFactory.mergeValuesWithDefaults(
      ordwayEntityRule
    );
    return (OrdwayLabs__OrdwayEntityRule__c) TestObjectFactory.createSObject(
      ordwayEntityRule,
      true
    );
  }

  public static OrdwayLabs__OrdwayUsage__c createOrdwayUsage() {
    return TestObjectFactory.createOrdwayUsage(
      new OrdwayLabs__OrdwayUsage__c()
    );
  }
  public static OrdwayLabs__OrdwayUsage__c createOrdwayUsage(
    OrdwayLabs__OrdwayUsage__c ordwayUsage
  ) {
    ordwayUsage = (OrdwayLabs__OrdwayUsage__c) TestObjectFactory.mergeValuesWithDefaults(
      ordwayUsage
    );
    return (OrdwayLabs__OrdwayUsage__c) TestObjectFactory.createSObject(
      ordwayUsage,
      true
    );
  }
 
  public static OrdwayLabs__InvoiceLineItem__c createOrdwayInvoiceLineItem() {
    return TestObjectFactory.createOrdwayInvoiceLineItem(
      new OrdwayLabs__InvoiceLineItem__c()
    );
  }
  public static OrdwayLabs__InvoiceLineItem__c createOrdwayInvoiceLineItem(
    OrdwayLabs__InvoiceLineItem__c ordwayInvoiceLineItem
  ) {
    ordwayInvoiceLineItem = (OrdwayLabs__InvoiceLineItem__c) TestObjectFactory.mergeValuesWithDefaults(
      ordwayInvoiceLineItem
    );
    return (OrdwayLabs__InvoiceLineItem__c) TestObjectFactory.createSObject(
      ordwayInvoiceLineItem,
      true
    );
  }

  public static OrdwayContract__c createOrdwayContract() {
    return TestObjectFactory.createOrdwayContract(new OrdwayContract__c());
  }
  public static OrdwayContract__c createOrdwayContract(
    OrdwayContract__c ordwayContractValues
  ) {
    ordwayContractValues = (OrdwayContract__c) TestObjectFactory.mergeValuesWithDefaults(
      ordwayContractValues
    );
    return (OrdwayContract__c) TestObjectFactory.createSObject(
      ordwayContractValues,
      true
    );
  }

  public static ContractLineItem__c createContractLineItem() {
    return TestObjectFactory.createContractLineItem(new ContractLineItem__c());
  }

  public static ContractLineItem__c createContractLineItem(
    ContractLineItem__c contractLineItemValues
  ) {
    contractLineItemValues = (ContractLineItem__c) TestObjectFactory.mergeValuesWithDefaults(
      contractLineItemValues
    );
    return (ContractLineItem__c) TestObjectFactory.createSObject(
      contractLineItemValues,
      true
    );
  }

  public static ContractLineItemTier__c createContractLineItemTier() {
    return TestObjectFactory.createContractLineItemTier(new ContractLineItemTier__c());
  }

  public static ContractLineItemTier__c createContractLineItemTier(
    ContractLineItemTier__c contractLineItemTierValues
  ) {
    contractLineItemTierValues = (ContractLineItemTier__c) TestObjectFactory.mergeValuesWithDefaults(
      contractLineItemTierValues
    );
    return (ContractLineItemTier__c) TestObjectFactory.createSObject(
      contractLineItemTierValues,
      true
    );
  }

  public static OrdwayProduct__c createOrdwayProduct() {
    return TestObjectFactory.createOrdwayProduct(new OrdwayProduct__c());
  }

  public static OrdwayProduct__c createOrdwayProduct(
    OrdwayProduct__c ordwayProductValues
  ) {
    ordwayProductValues = (OrdwayProduct__c) TestObjectFactory.mergeValuesWithDefaults(
      ordwayProductValues
    );
    return (OrdwayProduct__c) TestObjectFactory.createSObject(
      ordwayProductValues,
      true
    );
  }

  public static Quote__c createQuote() {
    return TestObjectFactory.createQuote(new Quote__c());
  }

  public static Quote__c createQuote(Quote__c quoteValues) {
    quoteValues = (Quote__c) TestObjectFactory.mergeValuesWithDefaults(
      quoteValues
    );
    return (Quote__c) TestObjectFactory.createSObject(quoteValues, true);
  }

  public static QuoteLineItem__c createQuoteLineItem() {
    return TestObjectFactory.createQuoteLineItem(new QuoteLineItem__c());
  }

  public static QuoteLineItem__c createQuoteLineItem(QuoteLineItem__c quoteLineItemValues) {
    quoteLineItemValues = (QuoteLineItem__c) TestObjectFactory.mergeValuesWithDefaults(
      quoteLineItemValues
    );
    return (QuoteLineItem__c) TestObjectFactory.createSObject(quoteLineItemValues, true);
  }

  public static QuoteLineItemTier__c createQuoteLineItemTier() {
    return TestObjectFactory.createQuoteLineItemTier(new QuoteLineItemTier__c());
  }

  public static QuoteLineItemTier__c createQuoteLineItemTier(QuoteLineItemTier__c quoteLineItemTierValues) {
    quoteLineItemTierValues = (QuoteLineItemTier__c) TestObjectFactory.mergeValuesWithDefaults(
      quoteLineItemTierValues
    );
    return (QuoteLineItemTier__c) TestObjectFactory.createSObject(quoteLineItemTierValues, true);
  }

  public static Charge__c createCharge() {
    return TestObjectFactory.createCharge(new Charge__c());
  }

  public static Charge__c createCharge(Charge__c chargeValues) {
    chargeValues = (Charge__c) TestObjectFactory.mergeValuesWithDefaults(
      chargeValues
    );
    return (Charge__c) TestObjectFactory.createSObject(chargeValues, true);
  }

  public static ChargeTier__c createChargeTier() {
    return TestObjectFactory.createChargeTier(new ChargeTier__c());
  }

  public static ChargeTier__c createChargeTier(ChargeTier__c chargeTierValues) {
    chargeTierValues = (ChargeTier__c) TestObjectFactory.mergeValuesWithDefaults(
      chargeTierValues
    );
    return (ChargeTier__c) TestObjectFactory.createSObject(chargeTierValues, true);
  }

  public static OpportunityLineItemTier__c createOpportunityLineItemTier() {
    return TestObjectFactory.createOpportunityLineItemTier(new OpportunityLineItemTier__c());
  }

  public static OpportunityLineItemTier__c createOpportunityLineItemTier(OpportunityLineItemTier__c opportunityLineItemTierValues) {
    opportunityLineItemTierValues = (OpportunityLineItemTier__c) TestObjectFactory.mergeValuesWithDefaults(
      opportunityLineItemTierValues
    );
    return (OpportunityLineItemTier__c) TestObjectFactory.createSObject(opportunityLineItemTierValues, true);
  }

  public static QuoteTemplate__c createQuoteTemplate() {
    return TestObjectFactory.createQuoteTemplate(new QuoteTemplate__c());
  }

  public static QuoteTemplate__c createQuoteTemplate(
    QuoteTemplate__c quoteTemplateValues
  ) {
    quoteTemplateValues = (QuoteTemplate__c) TestObjectFactory.mergeValuesWithDefaults(
      quoteTemplateValues
    );
    return (QuoteTemplate__c) TestObjectFactory.createSObject(
      quoteTemplateValues,
      true
    );
  }

  /* CREATE CUSTOM SETTING => CREATE APPLICATION SETTING */
  public static ApplicationSetting__c createApplicationSetting() {
    return TestObjectFactory.createApplicationSetting(
      new ApplicationSetting__c()
    );
  }

  public static ApplicationSetting__c createApplicationSetting(
    ApplicationSetting__c recordValues
  ) {
    recordValues = (ApplicationSetting__c) TestObjectFactory.mergeValuesWithDefaults(
      recordValues
    );
    return (ApplicationSetting__c) TestObjectFactory.createSObject(
      recordValues,
      true
    );
  }

  /* CREATE CUSTOM SETTING => CONTRACT PLAN PICKER SETTING */
  public static ContractPlanPickerSetting__c createContractPlanPickerSetting() {
    return TestObjectFactory.createContractPlanPickerSetting(
      new ContractPlanPickerSetting__c()
    );
  }

  public static ContractPlanPickerSetting__c createContractPlanPickerSetting(
    ContractPlanPickerSetting__c recordValues
  ) {
    recordValues = (ContractPlanPickerSetting__c) TestObjectFactory.mergeValuesWithDefaults(
      recordValues
    );
    return (ContractPlanPickerSetting__c) TestObjectFactory.createSObject(
      recordValues,
      true
    );
  }

  /* CREATE CUSTOM SETTING => Bulk Sync Configuration */
  public static BulkSyncConfiguration__c createBulkSyncConfigurationSetting() {
    return TestObjectFactory.createBulkSyncConfigurationSetting(
      new BulkSyncConfiguration__c()
    );
  }

  public static BulkSyncConfiguration__c createBulkSyncConfigurationSetting(
    BulkSyncConfiguration__c recordValues
  ) {
    recordValues = (BulkSyncConfiguration__c) TestObjectFactory.mergeValuesWithDefaults(
      recordValues
    );
    return (BulkSyncConfiguration__c) TestObjectFactory.createSObject(
      recordValues,
      true
    );
  }

  /* CREATE CUSTOM SETTING => LOAD LEAN SETTINGS */
  public static LoadLeanSettings__c createLoadLeanSetting() {
    return TestObjectFactory.createLoadLeanSetting(new LoadLeanSettings__c());
  }

  public static LoadLeanSettings__c createLoadLeanSetting(
    LoadLeanSettings__c recordValues
  ) {
    recordValues = (LoadLeanSettings__c) TestObjectFactory.mergeValuesWithDefaults(
      recordValues
    );
    return (LoadLeanSettings__c) TestObjectFactory.createSObject(
      recordValues,
      true
    );
  }

  public static PlanPicker__mdt createPlanPickerMetadata(String label, String Name) {
    PlanPicker__mdt PlanPicker      = new PlanPicker__mdt();
    PlanPicker.MasterLabel          = label;
    PlanPicker.DeveloperName        = Name;
    return PlanPicker;
  }

   public static ObjectFieldMapping__mdt createObjectFieldMappingMetadata(
    String label,
    String sourceObjectName,
    String targetObjectName,
    String fieldMappingJSON
  ) {
    ObjectFieldMapping__mdt objectFieldMapping = new ObjectFieldMapping__mdt();
    objectFieldMapping.MasterLabel = label;
    objectFieldMapping.DeveloperName = label;
    objectFieldMapping.SourceObject__c = sourceObjectName;
    objectFieldMapping.TargetObject__c = targetObjectName;
    objectFieldMapping.WhereIsThisUsed__c = 'Opportunity & Ordway Contract Field Mapping';
    objectFieldMapping.FieldMappingJSON__c = fieldMappingJSON;
    return objectFieldMapping;
  }

  private static Set<DisplayType> setStringFieldTypes = new Set<DisplayType>{
    DisplayType.address,
    DisplayType.String,
    DisplayType.Phone,
    DisplayType.TextArea
  };

  private static Set<DisplayType> setNumberFieldTypes = new Set<DisplayType>{
    DisplayType.Currency,
    DisplayType.Double,
    DisplayType.Integer,
    DisplayType.Percent
  };

  private static final Map<String, String> firstPicklistValue = new Map<String, String>();
  public static String getFirstPicklistValue(
    SObjectType sObjType,
    String fieldName
  ) {
    DescribeFieldResult fieldDescribe = sObjType.getDescribe()
      .fields.getMap()
      .get(fieldName)
      .getDescribe();
    return TestObjectFactory.getFirstPicklistValue(sObjType, fieldDescribe);
  }

  public static String getFirstPicklistValue(
    SObjectType sObjType,
    Schema.DescribeFieldResult fieldDescribe
  ) {
    String key = sObjType + '.' + fieldDescribe.getName();

    if (!firstPicklistValue.containsKey(key)) {
      List<Schema.PicklistEntry> entries = fieldDescribe.getPicklistValues();
      String value = entries.size() > 0 ? entries[0].getValue() : null;
      firstPicklistValue.put(key, value);
    }

    return firstPicklistValue.get(key);
  }

  private static SObject getDefault(Id theRecordTypeId, SObjectType sObjType) {
    String sObjName = sObjType.getDescribe().getName();
    SObject defaults = TestObjectFactory.mapDefaultValuesBySObjectName.get(
      sObjName
    );

    if (defaults == null) {
      defaults = sObjType.newSObject(theRecordTypeId, true);
      TestObjectFactory.mapDefaultValuesBySObjectName.put(sObjName, defaults);
      Map<String, Object> mapDefaultsValues = TestObjectFactory.mapTestDefaults.get(
        sObjName
      );

      if (mapDefaultsValues != null) {
        for (String fieldName : mapDefaultsValues.keySet()) {
          defaults.put(fieldName, mapDefaultsValues.get(fieldName));
        }
      }
    }
    return defaults.clone();
  }

  private static SObject mergeValuesWithDefaults(SObject theSobj) {
    SObjectType sObjType = theSobj.getSObjectType();

    Id theRecordTypeId = null;
    if (
      sObjType.getDescribe().fields.getMap().keySet().contains('recordtypeid')
    ) {
      theRecordTypeId = (Id) theSobj.get('recordtypeid');
    }

    SObject merged = TestObjectFactory.getDefault(theRecordTypeId, sObjType);

    Map<String, Object> mapPopedValues = theSobj.getPopulatedFieldsAsMap();

    if (mapPopedValues != null && !mapPopedValues.isEmpty()) {
      Map<String, SObjectField> sObjFields = sObjType.getDescribe()
        .fields.getMap();

      for (String fieldName : mapPopedValues.keySet()) {
        DescribeFieldResult fieldDescribe = sObjFields.get(fieldName)
          .getDescribe();

        if (fieldDescribe.isCreateable()) {
          merged.put(fieldName, mapPopedValues.get(fieldName));
        }
      }
    }

    return merged;
  }

  private static void addTestSObject(SObject sObj) {
    TestObjectFactory.addTestSObject(
      sObj,
      (String) sObj.get(OrdwayHelper.SOBJECT_FIELD_NAME)
    );
  }

  private static void addTestSObject(SObject sObj, String testName) {
    mapTestsObjects.put(
      sObj.getSObjectType().getDescribe().getName() + testName,
      sObj
    );
  }

  public static SObject getTestSObject(String sObj, String name) {
    return mapTestsObjects.get(sObj + name);
  }

  private static SObject createSObject(
    SObject values,
    Boolean alreadyMergedDefaults
  ) {
    return TestObjectFactory.createSObject(values, null, alreadyMergedDefaults);
  }
  private static SObject createSObject(
    SObject values,
    String testName,
    Boolean alreadyMergedDefaults
  ) {
    if (!alreadyMergedDefaults) {
      values = TestObjectFactory.mergeValuesWithDefaults(values);
    }

    Integer num = TestObjectFactory.testAutoNum;
    // Populate values set by test
    Map<String, Object> mapValues = values.getPopulatedFieldsAsMap();
    for (String fieldName : mapValues.keySet()) {
      String theVal = String.valueOf(mapValues.get(fieldName));
      if (String.isNotBlank(theVal) && theVal.contains('{!autonum}')) {
        theVal = theVal.replace('{!autonum}', num + '');
        values.put(fieldName, theVal);
      }
    }
    if (testName != null && testName.contains('{!autonum')) {
      testName = testName.replace('{!autonum}', num + '');
    }

    SObjectType sObjType = values.getSObjectType();

    // Auto fill required fiels which had no default values...
    Map<String, SObjectField> sObjFields = sObjType.getDescribe()
      .fields.getMap();
    for (String fieldName : sObjFields.keySet()) {
      // Skip auto pop for fields which has been manually set.
      if (mapValues.keySet().contains(fieldName))
        continue;

      DescribeFieldResult fieldDescribe = sObjFields.get(fieldName)
        .getDescribe();

      // if fields is required, and is editable, and is blank....
      if (
        !fieldDescribe.isNillable() &&
        fieldDescribe.isCreateable() &&
        values.get(fieldName) == null
      ) {
        DisplayType fieldType = fieldDescribe.getType();
        if (TestObjectFactory.setStringFieldTypes.contains(fieldType)) {
          // special handle for CompoundName field on contacts and leads
          if (
            fieldName.equalsIgnoreCase(OrdwayHelper.SOBJECT_FIELD_NAME) &&
            (sObjType == Schema.Lead.getSObjectType() ||
            sObjType == Schema.Contact.getSObjectType())
          ) {
            if (values.get('LastName') == null) {
              values.put('LastName', 'Testing_' + num);
            }
          } else {
            values.put(fieldName, 'Testing_' + num);
          }
        } else if (fieldType == DisplayType.Picklist) {
          values.put(
            fieldName,
            TestObjectFactory.getFirstPicklistValue(sObjType, fieldDescribe)
          );
        } else if (TestObjectFactory.setNumberFieldTypes.contains(fieldType)) {
          values.put(fieldName, Decimal.valueOf(num));
        } else if (fieldType == DisplayType.Email) {
          values.put(fieldName, 'testing' + num + '@ordway.com');
        } else if (fieldType == DisplayType.url) {
          values.put(fieldName, 'https://www.ordwaylabs' + num + '.com');
        } else if (fieldType == DisplayType.Date) {
          values.put(fieldName, Date.today().addDays(num));
        } else if (fieldType == DisplayType.DateTime) {
          values.put(fieldName, Datetime.now().addDays(num));
        }
      }
    }

    if (String.isNotBlank(testName)) {
      TestObjectFactory.addTestSObject(values, testName);
    } else {
      TestObjectFactory.addTestSObject(values);
    }

    if (isInsert) {
      insert values;
    }

    return values;
  }
}