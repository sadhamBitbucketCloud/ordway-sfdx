/**
 * @File Name          : OrdwayContractController.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 12-23-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    25/10/2019   Ordway Labs     Initial Version
 **/
public with sharing class OrdwayContractController {
  /**
   * @description Method To check wheather the user has the Ordway permission set assigned to the user
   * @return Boolean true or false
   **/
  @AuraEnabled
  public static Boolean isPermissionSetAssigned() {
    return ApplicationSettingController.isOrdwayPermissionAssignedToUser();
  }

  @AuraEnabled
  public static Map<String, Boolean> getButtonPermission(){
    try {
      Map<String, Boolean> permissionMap = new Map<String, Boolean>();

      permissionMap.put('SyncContract', !FeatureManagement.checkPermission('ContractSynctoUpdatefromOrdway'));
      permissionMap.put('ActivateContract', !FeatureManagement.checkPermission('ContractActivateContract'));
      permissionMap.put('CancelContract', !FeatureManagement.checkPermission('ContractCancelContract'));
      permissionMap.put('ChangeRenewal', !FeatureManagement.checkPermission('ContractCreateChangeRenewalOpportunity'));
      permissionMap.put('ViewInOrdway', !FeatureManagement.checkPermission('ViewinOrdway'));
      return permissionMap;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * @description Method To check Ordway Contract Status
   * @param ordwayContractId
   * @return OrdwayContract__c
   **/
  @AuraEnabled
  public static OrdwayContract__c checkOrdwayStatus(String ordwayContractId) {
    try {
      OrdwayContract__c thisOrdwayContract = new OrdwayContract__c();
      if (Schema.sObjectType.OrdwayContract__c.isAccessible()) {
        thisOrdwayContract = OrdwayContractService.getOrdwayContract(
            new Set<Id>{ ordwayContractId }
          )
          .values()[0];
      }
      return thisOrdwayContract;
    } catch (Exception thisException) {
      AuraHandledException ex = new AuraHandledException(
        thisException.getMessage()
      );
      ex.setMessage(thisException.getMessage());
      throw ex;
    }
  }

  /**
   * @description Method to sync Ordway Contract to Ordway - Sync to Ordway Button
   * @param contractId Ordway Contract Id
   * @return String
   **/
  @AuraEnabled
  public static String syncToOrdway(String contractId) {
    try {
      HttpResponse thisResponse = OrdwayContractService.syncContractToOrdway(
        getContract(contractId)
      );

      if (thisResponse.getStatusCode() == 200) {
        if(OpportunitySettingController.isAutoActivateContract()) {
          System.enqueueJob(new ActivateContractQueueable(contractId));
        }

        return contractId;
      }
      String errorMessage = OrdwayHelper.getResponseStatusMessage(thisResponse);
      AuraHandledException ex = new AuraHandledException(errorMessage);
      ex.setMessage(errorMessage);
      throw ex;
    } catch (Exception thisException) {
      AuraHandledException ex = new AuraHandledException(
        thisException.getMessage()
      );
      ex.setMessage(thisException.getMessage());
      throw ex;
    }
  }

  /**
   * @description Asynchronously Sync Ordway contract to Ordway
   * @param contractId Contract Record Id
   **/
  @future(callout=true)
  public static void aSyncToOrdway(Id contractId) {
    String doNotSend = OrdwayHelper.SYNC_STATE_DO_NOT_SEND;
    OrdwayContract__c thisContract = getContract(contractId);
   
    OrdwayContractService.syncContractToOrdway(thisContract);
    OrdwayContractPlatformEventHelper.publishOrdwayContractAsyncEvent(
      new List<OrdwayContract__c>{ thisContract }
    );

    if(OpportunitySettingController.isAutoActivateContract()
      && System.isFuture()) {
      System.enqueueJob(new ActivateContractQueueable(contractId));
    }
  }

  /**
   * @description Method to invoke from platform event
   * @param contractId Contract Record Id
   * @param aToken Token for the Request
   **/
  @future(callout=true)
  public static void updateContractFromOrdway(Id contractId, String aToken) {
    HttpResponse thisResponse = OrdwayService.getOrdwaySubscription(
      getContract(contractId).OrdwaySubscriptionId__c,
      null,
      contractId
    );

    if (thisResponse.getStatusCode() == 200) {
      OrdwayContractService.processContract(thisResponse.getBody());
    }
  }

  /**
  * @description Method to update Contract from Ordway (Invoked from component Update from Ordway -> Confirm button)
                 When automation is turned OFF
  * @param contractId Contract Record Id
  * @return String contract Id or Error Message
  **/
  @AuraEnabled
  public static String updateOrdwayContract(String contractId) {
    try {
      HttpResponse thisResponse = OrdwayService.getOrdwaySubscription(
        getContract(contractId).OrdwaySubscriptionId__c,
        null,
        contractId
      );
      
      OrdwayContractTriggerHelper.runOnce = false;
      OrdwayContract__c thisContract = getContract(contractId);

      if (thisResponse.getStatusCode() == 200) {
        List<Object> responseList = new List<Object>();
        responseList.add((Object)JSON.deserializeUntyped(thisResponse.getBody()));
        OrdwayObjectSyncHelper.processResponse('OrdwayContract', new Map<String, Object>{ 'result' => JSON.deserializeUntyped(JSON.serialize(responseList))}, OrdwayHelper.ACTION.RESPONSE, thisContract.EntityId__c);
        return contractId;
      }

      String errorMessage = OrdwayHelper.getResponseStatusMessage(thisResponse);
      AuraHandledException ex = new AuraHandledException(errorMessage);
      ex.setMessage(errorMessage);
      throw ex;
    } catch (Exception thisException) {
      AuraHandledException ex = new AuraHandledException(
        thisException.getMessage()
      );
      ex.setMessage(thisException.getMessage());
      throw ex;
    }
  }

  /**
   * @description Method to Get Ordway Contract - Field Comparison
   * @param contractId Contract Record Id
   * @param sObjectList Sobject List for which Field Comparison wrapper needs to be constructed
   * @return Map<String, Map<String, Object>>
   **/
  @AuraEnabled
  public static Map<String, Map<String, Object>> getOrdwayContract(
    Id contractId,
    List<sObject> sObjectList,
    Map<String, String> lineItemToTierStringMap
  ) {
    try {
      HttpResponse thisResponse = OrdwayService.getOrdwaySubscription(
        getContract(contractId).OrdwaySubscriptionId__c,
        null,
        contractId
      );

      if (thisResponse.getStatusCode() == 200) {
        populateLineItemToTierMap(lineItemToTierStringMap, thisResponse.getBody());
        Set<String> dmlKeyStringSet = new Set<String>{
          OrdwayHelper.SOBJECT_LIST_TO_INSERT,
          OrdwayHelper.SOBJECT_LIST_TO_UPDATE
        };
        Map<String, Map<String, Object>> fieldValueAsMap = new Map<String, Map<String, Object>>();

        Map<String, list<sObject>> sObjectMapToReturn = OrdwayContractService.getOrdwayContract(
          thisResponse.getBody()
        );
        for (String thisDMLKey : dmlKeyStringSet) {
          if (sObjectMapToReturn.containsKey(thisDMLKey)) {
            for (SObject thisObject : sObjectMapToReturn.get(thisDMLKey)) {
              if (thisObject.Id != null) {
                fieldValueAsMap.put(thisObject.Id, new Map<String, Object>{});
                fieldValueAsMap.get(thisObject.Id)
                  .putAll(thisObject.getPopulatedFieldsAsMap());
              } else if (
                String.valueOf(
                  thisObject.get('OrdwayLabs__SubscriptionID_LineID__c')
                ) != null
              ) {
                fieldValueAsMap.put(
                  String.valueOf(
                    thisObject.get('OrdwayLabs__SubscriptionID_LineID__c')
                  ),
                  new Map<String, Object>{}
                );
                fieldValueAsMap.get(
                    String.valueOf(
                      thisObject.get('OrdwayLabs__SubscriptionID_LineID__c')
                    )
                  )
                  .putAll(thisObject.getPopulatedFieldsAsMap());
                sObjectList.add(thisObject);
              }
            }
          }
        }
        return fieldValueAsMap;
      }
      String errorMessage = OrdwayHelper.getResponseStatusMessage(thisResponse);
      AuraHandledException ex = new AuraHandledException(errorMessage);
      ex.setMessage(errorMessage);
      throw ex;
    } catch (Exception thisException) {
      AuraHandledException ex = new AuraHandledException(
        thisException.getMessage()
      );
      ex.setMessage(thisException.getMessage());
      throw ex;
    }
  }

  public static void populateLineItemToTierMap(Map<String, String> lineItemToTierStringMap, String jsonResponse) {
    if(String.isNotBlank(jsonResponse)){
      Map<String, Object> ordwayResponse = (Map<String, Object>)JSON.deserializeUntyped(jsonResponse);
      if(ordwayResponse.containsKey('plans')){
        for(Object thisPlan : (List<Object>)ordwayResponse.get('plans')){
          Map<String, Object> deserializedPlan = (Map<String, Object>)thisPlan;
          if(deserializedPlan.containsKey('Id')
            && deserializedPlan.containsKey('tiers')){
            lineItemToTierStringMap.put((String)deserializedPlan.get('Id'), JSON.serialize((List<Object>)deserializedPlan.get('tiers')) );
            
          }
        }
      }
    }

  }

  /**
   * @description Method to get Ordway Contract
   * @param contractId Ordway Contract Id
   * @return OrdwayContract__c Ordway Contract record
   **/
  private static OrdwayContract__c getContract(Id contractId) {
    if (Schema.sObjectType.OrdwayContract__c.isAccessible()) {
      return [
        SELECT
          Id,
          Account__c,
          OrdwaySubscriptionID__c,
          VersionType__c,
          OrdwayContractStatus__c,
          ContractEffectiveDate__c,
          BillingStartDate__c,
          ServiceStartDate__c,
          SyncStatus__c,
          EntityId__c
        FROM OrdwayContract__c
        WHERE Id = :contractId
      ];
    }
    return null;
  }

  /**
   * @description Method to Activate Subscription
   * @param ordwayContractString Ordway Contract JSON string
   * @return String Contract Id or Error Message
   **/
  @AuraEnabled
  public static String activateSubscription(String ordwayContractString) {
    OrdwayLabs__OrdwayContract__c thisOrdwayContract = (OrdwayLabs__OrdwayContract__c) JSON.deserialize(
      ordwayContractString,
      OrdwayLabs__OrdwayContract__c.class
    );

    try {
      HttpResponse thisResponse =  activateSubscription(thisOrdwayContract);
      if (
      thisResponse.getStatusCode() >= 200 &&
      thisResponse.getStatusCode() <= 299
    ) {
      return thisOrdwayContract.Id;
    }

      String errorMessage = OrdwayHelper.getResponseStatusMessage(thisResponse);
      AuraHandledException ex = new AuraHandledException(errorMessage);
      ex.setMessage(errorMessage);
      throw ex;
    } catch (Exception thisException) {
      AuraHandledException ex = new AuraHandledException(
        thisException.getMessage()
      );
      ex.setMessage(thisException.getMessage());
      throw ex;
    }
  }

  public static HttpResponse activateSubscription(OrdwayContract__c thisOrdwayContract) {

    HttpResponse thisResponse = OrdwayService.activateSubscription( thisOrdwayContract );
    if (
      thisResponse.getStatusCode() >= 200 &&
      thisResponse.getStatusCode() <= 299
    ) {
      Map<String, Object> responseMap = OrdwayHelper.getDeserializedUntyped( thisResponse );

      if (
        String.isNotBlank( String.valueOf( responseMap.get('OrdwayLabs__OrdwayContractStatus__c') ) ) &&
        SObjectHelper.isFieldUpdatable( SObjectType.OrdwayContract__c, Schema.OrdwayContract__c.OrdwayContractStatus__c )
      ) {
        thisOrdwayContract.OrdwayContractStatus__c = String.valueOf( responseMap.get('OrdwayLabs__OrdwayContractStatus__c') );
      }
      thisOrdwayContract.put('OrdwayLabs__SyncStatus__c', OrdwayHelper.SYNC_STATE_IN_SYNC);
      OrdwayContractTriggerHelper.runOnce = false;

      if (Schema.sObjectType.OrdwayContract__c.isUpdateable()) {
        update thisOrdwayContract;
      }
    }
    return thisResponse;
  }

  /**
   * @description  Method to Cancel Subscription
   * @param ordwayContractString Ordway Contract JSON string
   * @return String Contract Id or Error Message
   **/
  @AuraEnabled
  public static String cancelSubscription(String ordwayContractString) {

    OrdwayLabs__OrdwayContract__c thisOrdwayContract = (OrdwayLabs__OrdwayContract__c) JSON.deserialize( ordwayContractString, OrdwayLabs__OrdwayContract__c.class );

    try {
      HttpResponse thisResponse = cancelSubscription( thisOrdwayContract );
      if (
        thisResponse.getStatusCode() >= 200 &&
        thisResponse.getStatusCode() <= 299
      ) {
        return thisOrdwayContract.Id;
      }
      String errorMessage = OrdwayHelper.getResponseStatusMessage(thisResponse);
      AuraHandledException ex = new AuraHandledException(errorMessage);
      ex.setMessage(errorMessage);
      throw ex;
    } catch (Exception thisException) {
      AuraHandledException ex = new AuraHandledException(
        thisException.getMessage()
      );
      ex.setMessage(thisException.getMessage());
      throw ex;
    }
  }

  public static HttpResponse cancelSubscription(OrdwayContract__c thisOrdwayContract) {

    HttpResponse thisResponse = OrdwayService.cancelSubscription( thisOrdwayContract );

    if (
      thisResponse.getStatusCode() >= 200 &&
      thisResponse.getStatusCode() <= 299
    ) {
        Map<String, Object> responseMap = OrdwayHelper.getDeserializedUntyped( thisResponse );

        if (
          String.isNotBlank( String.valueOf( responseMap.get('OrdwayLabs__OrdwayContractStatus__c') ) ) &&
          SObjectHelper.isFieldUpdatable( SObjectType.OrdwayContract__c, Schema.OrdwayContract__c.OrdwayContractStatus__c )
        ) {
          thisOrdwayContract.OrdwayContractStatus__c = String.valueOf( responseMap.get('OrdwayLabs__OrdwayContractStatus__c') );
        }
        thisOrdwayContract.SyncStatus__c = OrdwayHelper.SYNC_STATE_IN_SYNC;
        OrdwayContractTriggerHelper.runOnce = false;

        if (Schema.sObjectType.OrdwayContract__c.isUpdateable()) {
          update thisOrdwayContract;
        }
    }
    return thisResponse;
  }

  /**
   * @description Method to get Named Credentials End point URL
   * @return String Named Credentials End point URL
   **/
  @AuraEnabled
  public static String getNamedCredential() {
    try {
      return OrdwayService.getNamedCredentialURL();
    } catch (Exception e) {
      throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  /**
   * @description Method to get Last Contract Date
   * @return Date
   **/
  @AuraEnabled
  public static Date getContractLastDate(Date contractEffectiveDate) {
    try {
      ApplicationLog.StartLoggingContext('OrdwayContractController', 'getContractLastDate()');
      ApplicationLog.Info(('Date Entered =>'+contractEffectiveDate));
      ApplicationLog.Info(('Last Active Date =>'+contractEffectiveDate.addDays(-1)));
      ApplicationLog.FinishLoggingContext();
      return contractEffectiveDate.addDays(-1);
    } catch (Exception e) {
      throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

}