/**
 * @File Name          : AddEditPlanQuoteController.cls
 * @Description        :
 * @Author             : 
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 12-29-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/15/2020        Initial Version
 **/
public with sharing class AddEditPlanQuoteController {
  /**
   * @description Method to get Quote Line Items for Add Edit Plan Picker
   * @param quoteId Quote Id
   * @param pricebookId Pricebook Id
   * @return Map<String, List<QuoteLineItem__c>> Map of Quote Line Items
   **/
  @AuraEnabled
  public static Map<String, List<QuoteLineItem__c>> getOrdwayQLI(
    String quoteId,
    String pricebookId
  ) {
    ApplicationLog.StartLoggingContext(
      'AddEditPlanQuoteController',
      'getOrdwayQLI()'
    );

    Map<String, List<QuoteLineItem__c>> planMap = new Map<String, List<QuoteLineItem__c>>();
    Map<String, QuoteLineItem__c> qliByPlanChargeIdMap = getQLIByPlanChargeId(
      quoteId
    );

    ApplicationLog.Info('QLI By Plan Charge Id' + qliByPlanChargeIdMap);

    ContractPlanPickerSetting__c thisSetting = getOpportunityCustomSetting();

    Map<Id, Quote__c> quoteMap = OrdwayQuoteService.getOrdwayQuoteMap(
      new Set<Id>{ quoteId }
    );
    Quote__c thisQuote = quoteMap.get(quoteId);
    Map<String, Object> fieldsToValue = thisQuote.getPopulatedFieldsAsMap();

    Map<String, PricebookEntry> pricebookEntryMap = PriceBookEntryController.getPricebookEntryMap(
      pricebookId,
      String.valueOf(fieldsToValue.get('CurrencyIsoCode'))
    );

    ApplicationLog.Info('Pricebook Entry' + pricebookEntryMap);

    String entityId = OrdwayService.getEntityId(quoteId);
    for (
      Plan__c thisOrdwayPlan : OrdwayPlanController.getOrdwayPlan(entityId)
    ) {
      String planKey = thisOrdwayPlan.PlanId__c + '_' + thisOrdwayPlan.Name;
      Map<String, Object> responseMap = new Map<String, Object>();

      if(String.isNotBlank(thisOrdwayPlan.PlanDetails__c)) {
        responseMap = (Map<String, Object>) JSON.deserializeUntyped(
          thisOrdwayPlan.PlanDetails__c
        );
      }
      else {
        continue;
      }

      for (String s : responseMap.keySet()) {
        //Each Plan Has More Than One Charge
        if (s == 'charges') {
          //Get Each Charge
          for (
            Object thisStr : (List<Object>) JSON.deserializeUntyped(
              JSON.serialize(responseMap.get(s))
            )
          ) {
            //Get Each Charge In Key Value Pair
            Map<String, Object> chargeObjectMap = new Map<String, Object>();
            chargeObjectMap = (Map<String, Object>) JSON.deserializeUntyped(
              JSON.serialize(thisStr)
            );

            if (
              !OrdwayHelper.isValidId(String.valueOf(chargeObjectMap.get('salesforce_id')))
            ) {
              continue;
            }
            //Only if the Salesforce Id Matches
            if (
              pricebookEntryMap.get(
                (Id) chargeObjectMap.get('salesforce_id')
              ) != null
            ) {
              if (!planMap.containsKey(planKey)) {
                planMap.put(planKey, new List<QuoteLineItem__c>{});
              }
              //If this Charge is already present, Add Existing QLI
              if (
                qliByPlanChargeIdMap.containsKey(
                  thisOrdwayPlan.PlanId__c +
                  '_' +
                  String.valueOf(chargeObjectMap.get('id'))
                )
              ) {
                planMap.get(planKey)
                  .add(
                    qliByPlanChargeIdMap.get(
                      thisOrdwayPlan.PlanId__c +
                      '_' +
                      String.valueOf(chargeObjectMap.get('id'))
                    )
                  );
                continue; //Skips to the next iteration of the loop
              }
              //Load Defaults
              QuoteLineItem__c thisQuoteLineItem = (QuoteLineItem__c) QuoteLineItem__c.sObjectType.newSObject(
                null,
                true
              );
              thisQuoteLineItem.Product2Id__c = (Id) chargeObjectMap.get(
                'salesforce_id'
              );
              thisQuoteLineItem.PriceBookEntryId__c = pricebookEntryMap.get(
                  (Id) chargeObjectMap.get('salesforce_id')
                )
                .Id;
              thisQuoteLineItem.OrdwayPlanId__c = thisOrdwayPlan.PlanId__c;
              thisQuoteLineItem.Quote__c = quoteId;
              thisQuoteLineItem.Name = String.valueOf(
                  chargeObjectMap.get('name')
                )
                .capitalize();
              AddEditPlanHelper.onLoadDefaultValues(thisQuoteLineItem);

              //Construct Quote Line Item
              if (
                chargeObjectMap.get('billing_period') != null &&
                chargeObjectMap.get('billing_period') != ''
              ) {
                thisQuoteLineItem.BillingPeriod__c = String.valueOf(
                  chargeObjectMap.get('billing_period')
                );
              }

              if (chargeObjectMap.get('type') != null) {
                thisQuoteLineItem.ChargeType__c = String.valueOf(
                  chargeObjectMap.get('type')
                );
              }

              if (chargeObjectMap.get('id') != null) {
                thisQuoteLineItem.OrdwayChargeId__c = String.valueOf(
                  chargeObjectMap.get('id')
                );
              }

              if (chargeObjectMap.get('product_id') != null) {
                thisQuoteLineItem.OrdwayProductId__c = String.valueOf(
                  chargeObjectMap.get('product_id')
                );
              }

              if (chargeObjectMap.get('pricing_model') != null) {
                thisQuoteLineItem.PricingModel__c = String.valueOf(
                  chargeObjectMap.get('pricing_model')
                );
              }

              if (chargeObjectMap.get('list_price') != null) {
                thisQuoteLineItem.OrdwayListPrice__c = thisSetting.UseOrdwayPrice__c
                  ? Decimal.valueOf(
                      String.valueOf(chargeObjectMap.get('list_price'))
                    )
                  : pricebookEntryMap.get(
                        (Id) chargeObjectMap.get('salesforce_id')
                      )
                      .UnitPrice;
              }

              if (chargeObjectMap.get('name') != null) {
                thisQuoteLineItem.ChargeName__c = String.valueOf(
                    chargeObjectMap.get('name')
                  )
                  .capitalize();
              }

              if (
                chargeObjectMap.get('unit_of_measure') != null &&
                chargeObjectMap.get('unit_of_measure') != ''
              ) {
                thisQuoteLineItem.UnitofMeasure__c = String.valueOf(
                  chargeObjectMap.get('unit_of_measure')
                );
              }

              if (
                chargeObjectMap.get('included_units') != null &&
                chargeObjectMap.get('included_units') != ''
              ) {
                thisQuoteLineItem.IncludedUnits__c = String.valueOf(
                  chargeObjectMap.get('included_units')
                );
              }

              if (UserInfo.isMultiCurrencyOrganization()) {
                thisQuoteLineItem.put(
                  'CurrencyIsoCode',
                  fieldsToValue.get('CurrencyIsoCode')
                );
              }

              if (chargeObjectMap.get('tiers') != null) {
                thisQuoteLineItem.TierJSON__c = JSON.serializePretty(
                  chargeObjectMap.get('tiers')
                );
              }
              //If Plan is not active, then skip
              if (false == thisOrdwayPlan.Active__c) {
                planMap.remove(planKey);
                continue;
              }

              ApplicationLog.Info(' Quote Line Item ' + thisQuoteLineItem);
              planMap.get(planKey).add(thisQuoteLineItem);
            }
          }
        }
      }
    }
    ApplicationLog.FinishLoggingContext();
    return planMap;
  }

  /**
   * @description Add and delete Quote line items
   * @param qliList Quote Line item records to upsert
   * @param quoteLineItemToDeleteIds Quote Line item records to be deleted
   **/
  @AuraEnabled
  public static void addPlanToQuote(
    String qliList,
    String quoteLineItemToDeleteIds
  ) {
    ApplicationLog.StartLoggingContext(
      'AddEditPlanQuoteController',
      'addPlanToQuote()'
    );

    //Delete OLI
    AddEditPlanHelper.deleteQLI(
      (List<String>) JSON.deserialize(
        quoteLineItemToDeleteIds,
        List<String>.class
      )
    );
    List<QuoteLineItem__c> quoteLineItemToInsert = new List<QuoteLineItem__c>();

    try {
      quoteLineItemToInsert = (List<QuoteLineItem__c>) JSON.deserialize(
        qliList,
        List<QuoteLineItem__c>.class
      );
      Map<String, Object> tierMap = new Map<String, Object>();

      //Loop through Quote Line Items
      for (QuoteLineItem__c thisQLI : quoteLineItemToInsert) {
        AddEditPlanHelper.populateQuoteDefaultValues(thisQLI);

        if (
          thisQLI.TierJSON__c != null &&
          thisQLI.TierJSON__c != 'null' &&
          thisQLI.TierJSON__c != ''
        ) {
          Decimal listPrice = 0;
          for (
            Object thisTier : (List<Object>) JSON.deserializeUntyped(
              thisQLI.TierJSON__c
            )
          ) {
            tierMap = (Map<String, Object>) JSON.deserializeUntyped(
              JSON.serialize(thisTier)
            );
            //VOLUME
            if (thisQLI.PricingModel__c == OrdwayHelper.VOLUME) {
              AddEditPlanHelper.calculateQuoteVolumePricing(thisQLI, tierMap);
              continue;
            }
            //TIERED
            if (thisQLI.PricingModel__c == OrdwayHelper.TIERED) {
              listPrice = AddEditPlanHelper.calculateQuoteTieredPricing(
                thisQLI,
                tierMap,
                listPrice
              );
            }
          }
        }
      }
      SObjectAccessDecision securityDecision = Security.stripInaccessible(
        AccessType.CREATABLE,
        quoteLineItemToInsert
      );
      upsert securityDecision.getRecords();
    } catch (Exception e) {
      AuraHandledException ex = new AuraHandledException(e.getMessage());
      ex.setMessage(e.getMessage());
      throw ex;
    }
  }

  /**
   * @description Get all existing Quote Line Items
   * @param quoteId Quote Id
   * @return Map of Quote line item records
   */
  public static Map<String, QuoteLineItem__c> getQLIByPlanChargeId(Id quoteId) {
    Map<String, QuoteLineItem__c> qliByPlanChargeIdMap = new Map<String, QuoteLineItem__c>();

    for (
      QuoteLineItem__c thisQLI : getQuoteLineItemMap(new Set<Id>{ quoteId })
        .values()
    ) {
      thisQLI.BillingPeriod__c = thisQLI.BillingPeriod__c != null &&
        thisQLI.BillingPeriod__c != ''
        ? thisQLI.BillingPeriod__c
        : 'N/A';
      thisQLI.UnitofMeasure__c = thisQLI.UnitofMeasure__c != null &&
        thisQLI.UnitofMeasure__c != ''
        ? thisQLI.UnitofMeasure__c
        : 'N/A';
      thisQLI.IncludedUnits__c = thisQLI.IncludedUnits__c != null &&
        thisQLI.IncludedUnits__c != ''
        ? thisQLI.IncludedUnits__c
        : 'N/A';

      if (
        thisQLI.OrdwayPlanId__c != null &&
        thisQLI.OrdwayChargeId__c != null
      ) {
        String key = thisQLI.OrdwayPlanId__c + '_' + thisQLI.OrdwayChargeId__c;
        if (qliByPlanChargeIdMap.get(key) == null) {
          qliByPlanChargeIdMap.put(
            thisQLI.OrdwayPlanId__c +
            '_' +
            thisQLI.OrdwayChargeId__c,
            thisQLI
          );
        }
      }
    }
    return qliByPlanChargeIdMap;
  }

  /**
   * @description Initial Wrapper data for Add Edit Plan picker
   * @param quoteId Quote Id
   * @param pricebookId Pricebook Id
   * @return AddEditPlanHelper.InitialWrapper Initial Wrapper
   **/
  @AuraEnabled
  public static AddEditPlanHelper.InitialWrapper getInitialWrapper(
    String quoteId,
    String pricebookId
  ) {
    Schema.FieldSet dynamicFieldSet = Schema.SObjectType.QuoteLineItem__c.fieldSets.getMap()
      .get(OrdwayHelper.ORDWAY_PLAN_PICKER_DYNAMIC_FIELDSET_APINAME);

    Schema.FieldSet readOnlyFieldSet = Schema.SObjectType.QuoteLineItem__c.fieldSets.getMap()
      .get(OrdwayHelper.ORDWAY_PLAN_PICKER_READ_ONLY_FIELDSET_APINAME);

    AddEditPlanHelper.InitialWrapper thisInitialWrapper = new AddEditPlanHelper.InitialWrapper();
    thisInitialWrapper.qliMap = getOrdwayQLI(quoteId, pricebookId);
    thisInitialWrapper.thisOrdwayQuote   = getOrdwayQuote(quoteId);
    thisInitialWrapper.requiredPlanObjectWrapper = AddEditPlanHelper.getDynamicFieldsWrapper(
      dynamicFieldSet
    );
    thisInitialWrapper.currencyISO = AddEditPlanHelper.getCurrencyIsoCode(
      quoteId
    );
    thisInitialWrapper.readOnlyPlanObjectWrapper = AddEditPlanHelper.getReadOnlyFieldsWrapper(readOnlyFieldSet, thisInitialWrapper.currencyISO);
    return thisInitialWrapper;
  }

  /**
   * @description Method to get Ordway Quote record by Record Id
   * @param recordId Quote Id
   * @return Ordway Quote
  **/
  private static OrdwayLabs__Quote__c getOrdwayQuote(Id recordId) {
    if (Schema.sObjectType.OrdwayLabs__Quote__c.isAccessible()) {
      return [
        SELECT
          Id,
          Name,
          OrdwayLabs__TotalQuantity__c
        FROM OrdwayLabs__Quote__c
        WHERE Id = :recordId
      ];
    }
    return null;
  }

  /**
   * @description Method to get Contract Plan picker custom setting
   * @return ContractPlanPickerSetting__c
   **/
  @AuraEnabled
  public static ContractPlanPickerSetting__c getOpportunityCustomSetting() {
    return ContractPlanPickerSetting__c.getValues(
        OrdwayHelper.OPPORTUNITY_SETTING_NAME
      ) == null
      ? new ContractPlanPickerSetting__c()
      : ContractPlanPickerSetting__c.getValues(
          OrdwayHelper.OPPORTUNITY_SETTING_NAME
        );
  }

  /**
   * getQuoteLineItemMap : Method to get Quote Line Item
   * @param  quoteIds:  Set of quote Id's
   * @return Map of Quote Line Items
   */
  public static Map<Id, QuoteLineItem__c> getQuoteLineItemMap(
    Set<Id> quoteIds
  ) {
    Map<Id, QuoteLineItem__c> qliMap = new Map<Id, QuoteLineItem__c>();
    if (Schema.sObjectType.QuoteLineItem__c.isAccessible()) {
      qliMap = new Map<Id, QuoteLineItem__c>(
        (List<QuoteLineItem__c>) Database.query(
          'SELECT ' +
          String.join(
            SObjectHelper.getAllFields(
              Schema.QuoteLineItem__c.getSObjectType()
            ),
            ','
          ) +
          ' FROM OrdwayLabs__QuoteLineItem__c WHERE OrdwayLabs__Quote__c IN: quoteIds'
        )
      );
    }
    return qliMap;
  }
}