/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 09-19-2020
 * @last modified by  : 
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   09-02-2020      Initial Version
**/
@isTest
private class ChangeRenewalOpportunity_Test {
  @isTest
  static void getOpenOpportunities() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    insert newSetting;

    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;

    Contact billingContact = TestObjectFactory.createContact();
    billingContact.AccountId = newAccount.Id;
    billingContact.FirstName = 'OrdwayLab';
    billingContact.Email = 'Ordway@test.com';
    insert billingContact;

    OrdwayContract__c ordwayContract = new OrdwayContract__c();
    ordwayContract.BillingContact__c = newContact.Id;
    ordwayContract.Account__c = newAccount.Id;
    ordwayContract = TestObjectFactory.createOrdwayContract(ordwayContract);
    insert ordwayContract;

    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.BillingContact__c = billingContact.Id;
    newOpportunity.OrdwayContract__c = ordwayContract.Id;
    newOpportunity.OrdwayOpportunityType__c = 'Renewal';
    insert newOpportunity;

    Test.startTest();
    List<Opportunity> opportunityListToAssert = ChangeRenewalOpportunity.getOpenOpportunities(
      ordwayContract.Id,
      true
    );
    system.assertEquals(
      1,
      opportunityListToAssert.size(),
      'should return only one opportunity'
    );
    Test.stopTest();
  }

  @isTest
  static void getOpenOpportunities_Bulk() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    insert newSetting;

    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;

    Contact billingContact = TestObjectFactory.createContact();
    billingContact.AccountId = newAccount.Id;
    billingContact.FirstName = 'OrdwayLab';
    billingContact.Email = 'Ordway@test.com';
    insert billingContact;

    OrdwayContract__c ordwayContract = new OrdwayContract__c();
    ordwayContract.BillingContact__c = newContact.Id;
    ordwayContract.Account__c = newAccount.Id;
    ordwayContract = TestObjectFactory.createOrdwayContract(ordwayContract);
    insert ordwayContract;

    List<Opportunity> opportunityListToinsert = new List<Opportunity>();
    for (Integer i = 0; i < 201; i++) {
      Opportunity newOpportunity = TestObjectFactory.createOpportunity();
      newOpportunity.BillingContact__c = billingContact.Id;
      newOpportunity.OrdwayContract__c = ordwayContract.Id;
      newOpportunity.OrdwayOpportunityType__c = 'Renewal';
      opportunityListToinsert.add(newOpportunity);
    }

    insert opportunityListToinsert;

    Test.startTest();
    List<Opportunity> opportunityListToAssert = ChangeRenewalOpportunity.getOpenOpportunities(
      ordwayContract.Id,
      true
    );
    system.assertEquals(
      201,
      opportunityListToAssert.size(),
      'should return 201 opportunities'
    );
    Test.stopTest();
  }

  @isTest
  private static void getOpportunity() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    insert newSetting;

    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;

    Contact billingContact = TestObjectFactory.createContact();
    billingContact.AccountId = newAccount.Id;
    billingContact.FirstName = 'OrdwayLab';
    billingContact.Email = 'Ordway@test.com';
    insert billingContact;

    OrdwayContract__c ordwayContract = new OrdwayContract__c();
    ordwayContract.BillingContact__c = newContact.Id;
    ordwayContract.Account__c = newAccount.Id;
    ordwayContract = TestObjectFactory.createOrdwayContract(ordwayContract);
    insert ordwayContract;

    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.BillingContact__c = billingContact.Id;
    newOpportunity.OrdwayContract__c = ordwayContract.Id;
    newOpportunity.OrdwayOpportunityType__c = 'Renewal';
    insert newOpportunity;

    Test.startTest();
    Opportunity opportunityToAssert = ChangeRenewalOpportunity.getOpportunity(
      newOpportunity.Id,
      null
    );
    System.assertEquals(
      'Renewal',
      opportunityToAssert.OrdwayOpportunityType__c,
      'should retrun Renewal'
    );
    Test.stopTest();
  }

  @isTest
  private static void getContract() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    insert newSetting;

    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;

    Contact billingContact = TestObjectFactory.createContact();
    billingContact.AccountId = newAccount.Id;
    billingContact.FirstName = 'OrdwayLab';
    billingContact.Email = 'Ordway@test.com';
    insert billingContact;

    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.BillingContact__c = billingContact.Id;
    newOpportunity.OrdwayOpportunityType__c = 'Renewal';
    insert newOpportunity;

    OrdwayContract__c ordwayContract = new OrdwayContract__c();
    ordwayContract.BillingContact__c = newContact.Id;
    ordwayContract.Account__c = newAccount.Id;
    ordwayContract.SyncedOpportunityId__c = newOpportunity.Id;
    ordwayContract = TestObjectFactory.createOrdwayContract(ordwayContract);
    insert ordwayContract;

    Test.startTest();
    Opportunity opportunityToAssert = ChangeRenewalOpportunity.getOpportunity(
      ordwayContract.Id,
      'Renewal'
    );
    System.assertEquals(
      'Renewal',
      opportunityToAssert.OrdwayOpportunityType__c,
      'should retrun Renewal'
    );
    //ChangeRenewalOpportunity.getPageLayoutFields(true);
    Test.stopTest();
  }
}