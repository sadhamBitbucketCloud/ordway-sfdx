/**
 * @File Name          : OrdwayContractService.cls
 * @Description        : Ordway Contract Service
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 12-31-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    10/12/2019   Ordway Labs     Initial Version
 **/
public with sharing class OrdwayContractService {
  /**
   * @description Method to Populate Subscription Id in Ordway Contract Id
   * @param thisContract Ordway Contract record
   **/
  public static void populateSubscriptionId(OrdwayContract__c thisContract) {
    if (thisContract.EntityID__c != null) {
      if (thisContract.ExternalID__c != null) {
        thisContract.OrdwaySubscriptionID__c = thisContract.ExternalID__c.substringAfter(
          ':'
        );
        if (
          thisContract.OrdwaySubscriptionID__c == null ||
          thisContract.OrdwaySubscriptionID__c == ''
        ) {
          thisContract.OrdwaySubscriptionID__c = thisContract.ExternalID__c;
        }
        thisContract.ExternalID__c =
          thisContract.EntityID__c +
          ':' +
          thisContract.OrdwaySubscriptionID__c;
      }
    } else {
      if (thisContract.ExternalID__c != null) {
        thisContract.OrdwaySubscriptionID__c = thisContract.ExternalID__c;
      }
      if (
        thisContract.ExternalID__c == null &&
        thisContract.OrdwaySubscriptionID__c != null
      ) {
        thisContract.ExternalID__c = thisContract.OrdwaySubscriptionID__c;
      }
    }
  }

  /**
   * @description Method to Check whther Opportunity is in sync with contract already
   * @param con Ordway Contract object
   * @param oldMap Ordway Contract oldMap
   * @return Boolean true or false
   **/
  public static Boolean isNewOpportunitySync(
    OrdwayContract__c con,
    Map<Id, OrdwayContract__c> oldMap
  ) {
    return con.SyncedOpportunityId__c != null &&
      oldMap.get(con.Id).SyncedOpportunityId__c != con.SyncedOpportunityId__c
      ? true
      : false;
  }

  /**
   * @description Method to Populate version type in ordway contract
   * @param thisContract Contract Object
   * @param oldMap Previous Version of Ordway Contract
   **/
  public static void populateVersionType(
    OrdwayContract__c thisContract,
    Map<Id, OrdwayContract__c> oldMap
  ) {
    //Renewal & Change won't be in DRAFT
    if (thisContract.OrdwayContractStatus__c == OrdwayHelper.DRAFT) {
      thisContract.VersionType__c = OrdwayHelper.VERSION_TYPE_NEW;
    } else if (
      thisContract.VersionType__c == OrdwayHelper.VERSION_TYPE_RENEWAL &&
      oldMap.get(thisContract.Id).VersionType__c !=
      OrdwayHelper.VERSION_TYPE_RENEWAL
    ) {
      //On Close Winning Opportunity - Type will be set to RENEWAL
      thisContract.VersionType__c = OrdwayHelper.VERSION_TYPE_RENEWAL;
    } else if (
      oldMap.get(thisContract.Id).VersionType__c ==
      OrdwayHelper.VERSION_TYPE_RENEWAL &&
      (oldMap.get(thisContract.Id).SyncStatus__c ==
      OrdwayHelper.SYNC_STATE_SYNC_IN_PROGRESS ||
      oldMap.get(thisContract.Id).SyncStatus__c ==
      OrdwayHelper.SYNC_STATE_NOT_IN_SYNC) &&
      thisContract.SyncStatus__c == OrdwayHelper.SYNC_STATE_NOT_IN_SYNC
    ) {
      //If RENEWAL sync fails, Version Type will remain RENEWAL
      thisContract.VersionType__c = OrdwayHelper.VERSION_TYPE_RENEWAL;
    } else if (thisContract.OrdwayContractStatus__c == OrdwayHelper.ACTIVE) {
      thisContract.VersionType__c = OrdwayHelper.VERSION_TYPE_CHANGE;
    }
  }

  /**
   * @description Method to populate Default Renewal Term if Contract Term is Evergreen
   * @param thisContract ordway contract description
   **/
  public static void defaultRenewalTerm(OrdwayContract__c thisContract) {
    if (thisContract.ContractTerm__c == '-1' || thisContract.ContractTerm__c == 'Evergreen') {
      thisContract.RenewalTerm__c = null;
    }
  }

  public static void populateShippingContact(OrdwayContract__c thisContract) {
    if(thisContract.ShippingContactSameasBillingContact__c
    && thisContract.BillingContact__c != null) {
      thisContract.ShippingContact__c = thisContract.BillingContact__c;
    }
  }

  /**
   * @description Populate Billing Address When the Billing contact is changed
   * @param thisContract Ordway contract
   * @param contactMap Contact Map
   **/
  public static void populateBillingAddress(
    OrdwayContract__c thisContract,
    Map<Id, Contact> contactMap
  ) {
    if (
      thisContract.BillingContact__c != null &&
      contactMap.get(thisContract.BillingContact__c) != null
    ) {
      Contact billingContact = contactMap.get(thisContract.BillingContact__c);
      thisContract.BillingStreet__c = billingContact.MailingStreet;
      thisContract.BillingCity__c = billingContact.MailingCity;
      thisContract.BillingState__c = billingContact.MailingState;
      thisContract.BillingPostalCode__c = billingContact.MailingPostalCode;
      thisContract.BillingCountry__c = billingContact.MailingCountry;
    } else if (thisContract.BillingContact__c == null) {
      thisContract.BillingStreet__c = null;
      thisContract.BillingCity__c = null;
      thisContract.BillingState__c = null;
      thisContract.BillingPostalCode__c = null;
      thisContract.BillingCountry__c = null;
    }
  }

  /**
   * @description Method to Update the Opportunity that is in sycn with the Contract with the most recent values
   * @param newOrdwayContractMap newOrdwayContractMap Map of New Ordway Contract Objects
   * @param oldOrdwayContractMap oldOrdwayContractMap Map of Old Ordway Contract Objects
   **/
  public static void updateOpportunity(
    Map<Id, OrdwayContract__c> newOrdwayContractMap,
    Map<Id, OrdwayContract__c> oldOrdwayContractMap
  ) {
    List<Opportunity> oppToUpdate = new List<Opportunity>();
    for (OrdwayContract__c thisOrdwayContract : newOrdwayContractMap.values()) {
      if (thisOrdwayContract.SyncedOpportunityId__c != null) {
        Opportunity thisOpp = new Opportunity(
          Id = thisOrdwayContract.SyncedOpportunityId__c
        );
        ChangeRenewalOpportunity.mapContractToOpportunity(
          thisOpp,
          thisOrdwayContract
        );
        oppToUpdate.add(thisOpp);
      }
    }

    //As we are just updating the values on Opportunity to be in sync with Contract, there is no need to run the trigger
    OpportunityTriggerHandler.runOnce = false;
    if (Schema.sObjectType.Opportunity.isUpdateable()) {
      update oppToUpdate;
    }
  }

  /**
   * @description Update Syncing Checkbox on Opportunity
   * @param  syncedOppIds syncedOppIds Opportunity that syncs with Contract
   * @param  contractIds  contractIds set of contracts Id that is syncing with Opportunity
   */
  public static void updateSyncing(Set<Id> syncedOppIds, Set<Id> contractIds) {
    List<Opportunity> oppList = new List<Opportunity>();

    if (Schema.sObjectType.Opportunity.isAccessible()) {
      for (Opportunity thisOpportunity : [
        SELECT Id, OrdwayContract__c, IsSyncing__c
        FROM Opportunity
        WHERE OrdwayContract__c IN :contractIds
      ]) {
        thisOpportunity.IsSyncing__c = syncedOppIds.contains(thisOpportunity.Id)
          ? true
          : false;
        oppList.add(thisOpportunity);
      }
    }

    OpportunityTriggerHandler.runOnce = false; //Turn The Trigger Off
    SObjectAccessDecision securityDecision = Security.stripInaccessible(
      AccessType.UPDATABLE,
      oppList
    );
    update securityDecision.getRecords();
  }

  /**
   * @description  Method to create ordway contract for Opportunity (Create Ordway Contract button on Opportunity or through trigger if automation is turned ON)
   * @param synchronizableOpportunityIds  Set of Opportunity Id's
   * @param opportunityNewMap Opportunity Map for which Ordway contract need to be created
   */
  public static void createOrdwayContract(
    Set<Id> synchronizableOpportunityIds,
    Map<Id, Opportunity> opportunityNewMap
  ) {
    ApplicationLog.StartLoggingContext(
      'OrdwayContractService',
      'createOrdwayContract()'
    );
    Map<Id, OrdwayContract__c> ordwayContractMapToInsert = new Map<Id, OrdwayContract__c>();

    for (Id thisOpportunityId : synchronizableOpportunityIds) {
      if (opportunityNewMap.containsKey(thisOpportunityId)) {
        OrdwayContract__c thisOrdwayContract = (OrdwayContract__c) OrdwayContract__c.sObjectType.newSObject(
          null,
          true
        );

        thisOrdwayContract.SyncedOpportunityId__c = thisOpportunityId; //Map Opportunity Id to Synced Opportunity Id field on Ordway Contract
        ordwayContractMapToInsert.put(
          opportunityNewMap.get(thisOpportunityId).Id,
          (OrdwayContract__c) SObjectFieldMappingHelper.dynamicSobjectMapping(
            opportunityNewMap.get(thisOpportunityId),
            thisOrdwayContract,
            true
          )
        );

        populateVersionType(
          thisOrdwayContract,
          thisOrdwayContract.SyncedOpportunityId__c,
          opportunityNewMap
        );
      }
    }

    try {
      if (Schema.sObjectType.OrdwayContract__c.isCreateable()) {
        insert ordwayContractMapToInsert.values();
      }

      //Populate Ordway contract Id - Do not remove this query, added this to fix few issues - Investigate
      if (Schema.sObjectType.Opportunity.isAccessible()) {
        populateOrdwayContractId(
          ordwayContractMapToInsert,
          new Map<Id, Opportunity>(
            [
              SELECT Id, OrdwayContract__c, IsSyncing__c
              FROM Opportunity
              WHERE Id IN :opportunityNewMap.keySet()
            ]
          )
        );
      }

      //Create Contractlineitem
      if (Schema.sObjectType.Opportunity.isAccessible()) {
        ContractLineItemService.createOrdwayContractLineitem(
          ordwayContractMapToInsert.keySet(),
          new Map<Id, Opportunity>(
            [
              SELECT Id, OrdwayContract__c
              FROM Opportunity
              WHERE Id IN :opportunityNewMap.keySet()
            ]
          )
        );
      }
    } catch (Exception e) {
      ApplicationLog.Error(e);
      ApplicationLog.FinishLoggingContext();
    }
  }

  /**
   * @description Populate version type when an Opportunity syncs with Contract
   * @param  thisOrdwayContract thisOrdwayContract Ordway Contract Object
   * @param  thisOpportunityId  thisOpportunityId Id of the Opportunity that syncs with Contract
   * @param  opportunityNewMap  opportunityNewMap New Opportunity Map
   */

  private static void populateVersionType(
    OrdwayContract__c thisOrdwayContract,
    Id thisOpportunityId,
    Map<Id, Opportunity> opportunityNewMap
  ) {
    //Default Version Type
    thisOrdwayContract.VersionType__c = OrdwayHelper.VERSION_TYPE_NEW;

    if (
      opportunityNewMap.get(thisOpportunityId).OrdwayOpportunityType__c != null
    ) {
      if (
        OrdwayHelper.ORDWAY_OPPORTUNITY_TYPE_UPSELL ==
        opportunityNewMap.get(thisOpportunityId).OrdwayOpportunityType__c &&
        thisOrdwayContract.OrdwayContractStatus__c == OrdwayHelper.ACTIVE
      ) {
        thisOrdwayContract.VersionType__c = OrdwayHelper.VERSION_TYPE_CHANGE;
      }
      if (
        OrdwayHelper.ORDWAY_OPPORTUNITY_TYPE_RENEWAL ==
        opportunityNewMap.get(thisOpportunityId).OrdwayOpportunityType__c
      ) {
        thisOrdwayContract.VersionType__c = OrdwayHelper.VERSION_TYPE_RENEWAL;
      }
    }
  }

  /**
   * @description Method to populate Ordway ContractId for Opportunity
   * @param opportunityToOrdwayContractMap Opportunity Id to Ordway Contract Map
   * @param synchronizableOpportunityMap Opportunity Map for which Ordway contract Id need to be populated
   */
  public static void populateOrdwayContractId(
    Map<Id, OrdwayContract__c> opportunityToOrdwayContractMap,
    Map<Id, Opportunity> opportunityNewMap
  ) {
    List<Opportunity> opportunityListToUpdate = new List<Opportunity>();

    for (Opportunity thisOpportunity : opportunityNewMap.values()) {
      if (
        opportunityToOrdwayContractMap.containsKey(thisOpportunity.Id) &&
        SObjectHelper.isFieldUpdatable(
          SObjectType.Opportunity,
          Schema.Opportunity.OrdwayContract__c
        )
      ) {
        thisOpportunity.OrdwayContract__c = opportunityToOrdwayContractMap.get(
            thisOpportunity.Id
          )
          .Id;
        thisOpportunity.IsSyncing__c = true;
        opportunityListToUpdate.add(thisOpportunity);
      }
    }

    if (Opportunity.SObjectType.getDescribe().isUpdateable()) {
      OpportunityTriggerHandler.runOnce = false; //Turn The Trigger Off
      update opportunityListToUpdate;
    }
  }

  /**
   * @description Method to Update ordway contract from Opportunity
   * @param opportunityIds Set of Opportunity Id's
   * @param opportunityNewMap Opportunity Map for which has to be synced to Ordway contract
   */
  public static void updateOrdwayContract(
    Map<Id, Opportunity> ordwayContractToOpportunityMap,
    Map<Id, Opportunity> opportunityNewMap
  ) {
    Map<Id, OrdwayContract__c> ordwayContractMap = getOrdwayContract(
      ordwayContractToOpportunityMap.keySet()
    );

    for (OrdwayContract__c thisOrdwayContract : ordwayContractMap.values()) {
      thisOrdwayContract.SyncedOpportunityId__c = ordwayContractToOpportunityMap.get( thisOrdwayContract.Id ) .Id; //Map Opportunity Id
      populateVersionType( thisOrdwayContract, thisOrdwayContract.SyncedOpportunityId__c, opportunityNewMap );

      thisOrdwayContract = (OrdwayContract__c) SObjectFieldMappingHelper.dynamicSobjectMapping(
        ordwayContractToOpportunityMap.get(thisOrdwayContract.Id),
        thisOrdwayContract,
        true
      );
      thisOrdwayContract.SyncStatus__c = OrdwayHelper.SYNC_STATE_NOT_IN_SYNC;
    }

    if (Schema.sObjectType.OrdwayContract__c.isUpdateable()) {
      update ordwayContractMap.values();
    }
    OpportunityLineItemService.syncOrdwayContractLineItem( opportunityNewMap.keySet(), ordwayContractMap.keyset() );
  }

  /**
   * @description If Customer Id on the contract Changes, update the account with the Customer Id
   * @param contractList Ordway Contract List
   **/
  public static void updateCustomerId(List<OrdwayContract__c> contractList) {
    Map<Id, Account> accountMap = new Map<Id, Account>();

    for (OrdwayContract__c thisContract : contractList) {
      Account newAccount = new Account(
        Id = thisContract.Account__c,
        OrdwayCustomerId__c = thisContract.CustomerId__c
      );
      accountMap.put(thisContract.Account__c, newAccount);
    }

    SObjectAccessDecision securityDecision = Security.stripInaccessible(
      AccessType.UPDATABLE,
      accountMap.values()
    );
    update securityDecision.getRecords();
  }

  public static string processContract(String jsonBody) {
    string contractId;
    Map<String, list<sObject>> sObjectMapToReturn = getOrdwayContract( jsonBody );
    OrdwayContractTriggerHelper.runOnce = false;

    if ( sObjectMapToReturn.containsKey(OrdwayHelper.SOBJECT_LIST_TO_INSERT) ) {
      SObjectAccessDecision securityDecision = Security.stripInaccessible( AccessType.CREATABLE, sObjectMapToReturn.get(OrdwayHelper.SOBJECT_LIST_TO_INSERT) );
      insert securityDecision.getRecords();

      contractId = String.valueOf( securityDecision.getRecords() .get(0) .get('OrdwayLabs__OrdwayContract__c') );
    }

    if ( sObjectMapToReturn.containsKey(OrdwayHelper.SOBJECT_LIST_TO_UPDATE) ) {
      SObjectAccessDecision securityDecision = Security.stripInaccessible( AccessType.UPDATABLE, sObjectMapToReturn.get(OrdwayHelper.SOBJECT_LIST_TO_UPDATE) );
      update securityDecision.getRecords();
    }

    if ( sObjectMapToReturn.containsKey(OrdwayHelper.SOBJECT_LIST_TO_DELETE) && Schema.sObjectType.ContractLineItem__c.isDeletable() ) {
      delete sObjectMapToReturn.get(OrdwayHelper.SOBJECT_LIST_TO_DELETE);
    }
    return contractId;
  }

  /**
   * @description Method to get Contract from Ordway
   * @param  Contract Object
   */
  public static Map<String, list<sObject>> getOrdwayContract(String jsonBody) {
    Id ordwayContractId;
    String ordwaySubscriptionId;

    Map<String, list<sObject>> sObjectMapToReturn = new Map<String, list<sObject>>();
    Map<Id, OrdwayLabs__ContractLineItem__c> ordwayContractLineItemMap = new Map<Id, OrdwayLabs__ContractLineItem__c>();
    Map<String, Id> ordwayContractLineItemExternalIdToIdMap = new Map<String, Id>();

    Map<String, Object> thisProductObjectMap;
    SObject sObjectRecord;

    Map<String, Object> thisDeserializedMap = (Map<String, Object>) JSON.deserializeUntyped(
      jsonBody
    );
    sObjectRecord = OrdwayHelper.getSObjectRecord(
      thisDeserializedMap,
      Schema.sObjectType.OrdwayContract__c,
      new OrdwayContract__c()
    );

    if (sObjectRecord.Id != null) {
      sObjectRecord.put(
        'OrdwayLabs__SyncStatus__c',
        OrdwayHelper.SYNC_STATE_IN_SYNC
      );
      ordwayContractId = sObjectRecord.Id;
      ordwaySubscriptionId = String.valueOf(
        sObjectRecord.get('OrdwayLabs__ExternalID__c')
      );
      populateSObjectDMLMap(
        sObjectMapToReturn,
        OrdwayHelper.SOBJECT_LIST_TO_UPDATE,
        sObjectRecord
      );
    }

    populateOrdwayContractLineItemMap(
      new Set<Id>{ ordwayContractId },
      ordwayContractLineItemMap,
      ordwayContractLineItemExternalIdToIdMap
    );

    for (
      Object thisProductObject : (List<Object>) thisDeserializedMap.get('plans')
    ) {
      thisProductObjectMap = (Map<String, Object>) thisProductObject;
      sObjectRecord = OrdwayHelper.getSObjectRecord(
        thisProductObjectMap,
        Schema.sObjectType.ContractLineItem__c,
        new ContractLineItem__c()
      );

      if (
        sObjectRecord.Id != null && !OrdwayHelper.isValidId(sObjectRecord.Id)
      ) {
        if (
          !ordwayContractLineItemExternalIdToIdMap.isEmpty() &&
          ordwayContractLineItemExternalIdToIdMap.containsKey(
            ordwaySubscriptionId +
            ':' +
            sObjectRecord.Id
          )
        ) {
          //Ordway Charge Synced With Salesforce, And Not Synced Backwards, So Salesforce Record Id will Not be Present in Ordway - Update Existing Record
          ordwayContractLineItemMap.remove(
            ordwayContractLineItemExternalIdToIdMap.get(
              ordwaySubscriptionId +
              ':' +
              sObjectRecord.Id
            )
          );
          sObjectRecord.put(
            OrdwayHelper.SOBJECT_FIELD_ID,
            ordwayContractLineItemExternalIdToIdMap.get(
              ordwaySubscriptionId +
              ':' +
              sObjectRecord.Id
            )
          );
          populateSObjectDMLMap(
            sObjectMapToReturn,
            OrdwayHelper.SOBJECT_LIST_TO_UPDATE,
            sObjectRecord
          );
        } else {
          //Line item exists in Ordway but not in Salesforce, Create Ordway Charge in Salesforce
          sObjectRecord.put(
            'OrdwayLabs__SubscriptionID_LineID__c',
            ordwaySubscriptionId +
            ':' +
            sObjectRecord.Id
          );
          sObjectRecord.put('OrdwayLabs__OrdwayContract__c', ordwayContractId);
          sObjectRecord.put(OrdwayHelper.SOBJECT_FIELD_ID, null);
          populateSObjectDMLMap(
            sObjectMapToReturn,
            OrdwayHelper.SOBJECT_LIST_TO_INSERT,
            sObjectRecord
          );
        }
      } else if (
        sObjectRecord.Id != null && OrdwayHelper.isValidId(sObjectRecord.Id) &&
        !ordwayContractLineItemExternalIdToIdMap.isEmpty() &&
        ordwayContractLineItemExternalIdToIdMap.containsKey(sObjectRecord.Id)
      ) {
        //Created Contract Line Item In Salesforce
        //Synced With Ordway
        //Delete Contract Line Item In Salesforce
        //Update From Ordway - Should NOT Create a New Contract Line Item if a Record Already Exists in Salesforce For The Same Record In Ordway
        ordwayContractLineItemMap.remove(
          ordwayContractLineItemExternalIdToIdMap.get(sObjectRecord.Id)
        );
        sObjectRecord.put(
          OrdwayHelper.SOBJECT_FIELD_ID,
          ordwayContractLineItemExternalIdToIdMap.get(sObjectRecord.Id)
        );
        sObjectRecord.put(
          'OrdwayLabs__SubscriptionID_LineID__c',
          sObjectRecord.Id
        ); //Update - The Scenario Below Will Have Wrong Salesforce Id, So Update Here To Correct It
        populateSObjectDMLMap(
          sObjectMapToReturn,
          OrdwayHelper.SOBJECT_LIST_TO_UPDATE,
          sObjectRecord
        );
      } else if (
        sObjectRecord.Id != null && OrdwayHelper.isValidId(sObjectRecord.Id) &&
        ordwayContractLineItemMap.containsKey(sObjectRecord.Id)
      ) {
        //Record Exists In Salesforce And A Record Exists in Ordway With Salesforce Id
        ordwayContractLineItemMap.remove(sObjectRecord.Id);
        sObjectRecord.put(
          'OrdwayLabs__SubscriptionID_LineID__c',
          sObjectRecord.Id
        ); //Update - The Scenario Below Will Have Wrong Salesforce Id, So Update Here To Correct It
        populateSObjectDMLMap(
          sObjectMapToReturn,
          OrdwayHelper.SOBJECT_LIST_TO_UPDATE,
          sObjectRecord
        );
      } else if (
        sObjectRecord.Id != null && OrdwayHelper.isValidId(sObjectRecord.Id) &&
        !ordwayContractLineItemMap.containsKey(sObjectRecord.Id)
      ) {
        //Created Contract Line Item In Salesforce
        //Synced With Ordway
        //Delete Contract Line Item In Salesforce
        //Update From Ordway - Should Create Deleted Record Back (This Happens When Auto Sync From Salesforce To Ordway is False)
        sObjectRecord.put(
          'OrdwayLabs__SubscriptionID_LineID__c',
          sObjectRecord.Id
        );
        sObjectRecord.put('OrdwayLabs__OrdwayContract__c', ordwayContractId);
        sObjectRecord.put(OrdwayHelper.SOBJECT_FIELD_ID, null);
        populateSObjectDMLMap(
          sObjectMapToReturn,
          OrdwayHelper.SOBJECT_LIST_TO_INSERT,
          sObjectRecord
        );
      }
    }

    if (
      !ordwayContractLineItemMap.isEmpty() &&
      Schema.sObjectType.OrdwayLabs__ContractLineItem__c.isDeletable()
    ) {
      sObjectMapToReturn.put(
        OrdwayHelper.SOBJECT_LIST_TO_DELETE,
        ordwayContractLineItemMap.values()
      );
    }

    return sObjectMapToReturn;
  }

  /**
   * @description Method to populat Sobject DML Map
   * @param sobjectMap Sobject Map to be processed
   * @param dmlKey field API name to be used as key
   * @param thisSobject Sobject to be processed
   **/
  public static void populateSObjectDMLMap(
    Map<String, list<sObject>> sobjectMap,
    String dmlKey,
    SObject thisSobject
  ) {
    if (!sobjectMap.containsKey(dmlKey)) {
      sobjectMap.put(dmlKey, new List<sObject>());
    }
    sobjectMap.get(dmlKey).add(thisSobject);
  }

  /**
   * populateOrdwayContractLineItemMap Method to get all Ordway ContractLineitem Map and Populate External Id to Ordway ContractLineitem Id Map based on Set of Ordway Contract Id
   * @param  ordwayContractLineItemMap  Ordway ContractLineitem Map
   * @param  ordwayContractLineItemExternalIdToIdMap  Ordway External Id to Ordway ContractLineitem Id Map
   * @param  ordwayContractIds Set of Ordway Contract Ids
   */
  private static void populateOrdwayContractLineItemMap(
    Set<Id> ordwayContractIds,
    Map<Id, OrdwayLabs__ContractLineItem__c> ordwayContractLineItemMap,
    Map<String, Id> ordwayContractLineItemExternalIdToIdMap
  ) {
    if (Schema.sObjectType.ContractLineItem__c.isAccessible()) {
      for (OrdwayLabs__ContractLineItem__c thisOrdwayContractLineItem : [
        SELECT
          Id,
          OrdwayLabs__SubscriptionID_LineID__c,
          OrdwayLabs__OrdwayContract__c
        FROM OrdwayLabs__ContractLineItem__c
        WHERE OrdwayLabs__OrdwayContract__c IN :ordwayContractIds
      ]) {
        ordwayContractLineItemMap.put(
          thisOrdwayContractLineItem.Id,
          thisOrdwayContractLineItem
        );
        if (
          !String.isEmpty(
            thisOrdwayContractLineItem.OrdwayLabs__SubscriptionID_LineID__c
          )
        ) {
          ordwayContractLineItemExternalIdToIdMap.put(
            thisOrdwayContractLineItem.OrdwayLabs__SubscriptionID_LineID__c,
            thisOrdwayContractLineItem.Id
          );
        }
      }
    }
  }

  /**
   * @description Method to Sync Contract to Ordway
   * @param  thisContract  Ordway Contract
   * @return HTTP Response
   */
  public static HttpResponse syncContractToOrdway(
    OrdwayContract__c thisContract
  ) {
    HttpResponse thisResponse = OrdwayService.sendToOrdway(thisContract);

    if (
      thisResponse.getStatusCode() >= 200 &&
      thisResponse.getStatusCode() <= 299
    ) {
      OrdwayHelper.getSObjectRecord(
        OrdwayHelper.getDeserializedUntyped(thisResponse),
        Schema.sObjectType.OrdwayContract__c,
        thisContract
      );
      setLastSyncDate(thisContract, OrdwayHelper.SYNC_STATE_IN_SYNC);
    } else {
      setLastSyncDate(thisContract, OrdwayHelper.SYNC_STATE_NOT_IN_SYNC);
    }

    OrdwayContractTriggerHelper.runOnce = false;
    SObjectAccessDecision securityDecision = Security.stripInaccessible(
      AccessType.UPDATABLE,
      new List<OrdwayContract__c>{ thisContract }
    );
    OrdwayContractTriggerHelper.runOnce = false;
    upsert securityDecision.getRecords();

    return thisResponse;
  }

  private static void setLastSyncDate(
    OrdwayContract__c thisContract,
    String syncStatus
  ) {
    thisContract.OrdwayLastSyncDate__c = System.now();
    thisContract.SyncStatus__c = syncStatus;
  }

  /**
   * @description Method to get Ordway contract Map
   * @param ordwayContractIds  Set of Ordway contract Id's
   * @return synchronizableOpportunityMap : Map of Ordway contracts
   */
  public static Map<Id, OrdwayContract__c> getOrdwayContract(
    Set<Id> ordwayContractIds
  ) {
    Map<Id, OrdwayContract__c> thisOrdwayContractMap = new Map<Id, OrdwayContract__c>();
    if (Schema.sObjectType.OrdwayContract__c.isAccessible()) {
      thisOrdwayContractMap = new Map<Id, OrdwayContract__c>(
        (List<OrdwayContract__c>) Database.query(
          'SELECT ' +
          String.join(
            SObjectHelper.getAllFields(
              Schema.OrdwayContract__c.getSObjectType()
            ),
            ','
          ) +
          ' FROM OrdwayContract__c WHERE Id IN:ordwayContractIds'
        )
      );
    }
    return thisOrdwayContractMap;
  }
}