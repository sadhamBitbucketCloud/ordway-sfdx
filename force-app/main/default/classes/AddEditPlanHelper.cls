/**
 * @File Name          : AddEditPlanHelper.cls
 * @Description        : Add / Edit Plan Component Controller
 * @Author             : Ordway Labs
 * @Last Modified By   : 
 * @Last Modified On   : 04-17-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    4/20/2020        Initial Version
 **/
public with sharing class AddEditPlanHelper {
  /**
   * @description  Method to delete Opportunity Line Items
   * @param lineItemId List of Opportunity Line Items Ids
   **/
  public static void deleteOLI(List<String> lineItemId) {
    if (
      Schema.sObjectType.OpportunityLineItem.isDeletable() &&
      !lineItemId.isEmpty()
    ) {
      delete [SELECT Id FROM OpportunityLineItem WHERE ID IN :lineItemId];
    }
  }

  /**
   * @description Get Currency ISO code for Opportunity & Quote Dynamically
   * @param sObjectId Id of SObject
   * @return String Currency ISO Code
   **/
  public static String getCurrencyIsoCode(String sObjectId) {
    if (UserInfo.isMultiCurrencyOrganization()) {
      sObject thisSObject = Database.query(String.escapeSingleQuotes(
        'SELECT Id ,CurrencyIsoCode FROM ' +
        Id.valueOf(sObjectId).getSObjectType() +
        ' WHERE Id = :sObjectId LIMIT 1'
      ));
      return String.valueOf(thisSObject.get(OrdwayHelper.CURRENCY_ISO_CODE));
    }
    return UserInfo.getDefaultCurrency();
  }

  /**
   * @description Method to delete Quote Line Items
   * @param lineItemId List of Quote Line Items Ids
   **/
  public static void deleteQLI(List<String> lineItemId) {
    if (
      Schema.sObjectType.QuoteLineItem__c.isDeletable() && !lineItemId.isEmpty()
    ) {
      delete [SELECT Id FROM QuoteLineItem__c WHERE ID IN :lineItemId];
    }
  }

  /**
   * @description Default Values - Replace N/A with null as N/A is not a valid picklist value
   * @param thisOLI Opportunity Line Item Object
   **/
  public static void populateDefaultValues(OpportunityLineItem thisOLI) {
    thisOLI.BillingPeriod__c = thisOLI.BillingPeriod__c == 'N/A'
      ? null
      : thisOLI.BillingPeriod__c; // N/A Not a Valid Picklist
    thisOLI.IncludedUnits__c = thisOLI.IncludedUnits__c == 'N/A'
      ? null
      : thisOLI.IncludedUnits__c;
    thisOLI.UnitofMeasure__c = thisOLI.UnitofMeasure__c == 'N/A'
      ? null
      : thisOLI.UnitofMeasure__c;
  }

  /**
   * @description Default Values - Replace N/A with null as N/A is not a valid picklist value
   * @param thisOLI Quote Line Item Object
   */
  public static void populateQuoteDefaultValues(QuoteLineItem__c thisQLI) {
    thisQLI.BillingPeriod__c = thisQLI.BillingPeriod__c == 'N/A'
      ? null
      : thisQLI.BillingPeriod__c; // N/A Not a Valid Picklist
    thisQLI.IncludedUnits__c = thisQLI.IncludedUnits__c == 'N/A'
      ? null
      : thisQLI.IncludedUnits__c;
    thisQLI.UnitofMeasure__c = thisQLI.UnitofMeasure__c == 'N/A'
      ? null
      : thisQLI.UnitofMeasure__c;
  }

  /**
   * @description On Add Plan Picker Init - Set the default values for Opportunity Line Item
   * @param thisLineItem Opportunity Line Item Object
   */
  public static void onLoadDefaultValues(OpportunityLineItem thisLineItem) {
    thisLineItem.BillingPeriod__c = 'N/A';
    thisLineItem.OrdwayListPrice__c = 0;
    thisLineItem.UnitofMeasure__c = 'N/A';
    thisLineItem.IncludedUnits__c = 'N/A';
    thisLineItem.TierJSON__c = '';
    thisLineItem.Quantity = 1;
    thisLineItem.Discount = 0;
  }

  /**
   * @description On Add Plan Picker Init - Set the default values for Quote Line Item
   * @param thisLineItem Quote Line Item Object
   */
  public static void onLoadDefaultValues(QuoteLineItem__c thisQuoteLineItem) {
    thisQuoteLineItem.BillingPeriod__c = 'N/A';
    thisQuoteLineItem.OrdwayListPrice__c = 0;
    thisQuoteLineItem.UnitofMeasure__c = 'N/A';
    thisQuoteLineItem.IncludedUnits__c = 'N/A';
    thisQuoteLineItem.TierJSON__c = '';
    thisQuoteLineItem.Quantity__c = 1;
    thisQuoteLineItem.Discount__c = 0;
  }

  /**
   * @description Calculate Volume Pricing For Opportunity Line Item
   * @param thisOLI Opportunity Line Item Object
   * @param tierMap Ordway Tier Values
   */
  public static void calculateVolumePricing(
    OpportunityLineItem thisOLI,
    Map<String, Object> tierMap
  ) {
    Integer startingUnit = Integer.ValueOf(tierMap.get('starting_unit'));
    String typeZ = String.valueOf(tierMap.get('type'));
    Decimal lPrice = Decimal.valueOf(String.valueOf(tierMap.get('price')));

    ContractPlanPickerSetting__c thisSetting = AddEditPlanOrdwayController.getOpportunityCustomSetting();

    if (
      thisOLI.Quantity > startingUnit &&
      (tierMap.get('ending_unit') == '' || tierMap.get('ending_unit') == null ||
      (tierMap.get('ending_unit') != '' &&
      thisOLI.Quantity <= Integer.ValueOf(tierMap.get('ending_unit')))) &&
      SObjectHelper.isFieldCreatable(
        SObjectType.OpportunityLineItem,
        Schema.OpportunityLineItem.OrdwayListPrice__c
      )
    ) {
      if (typeZ != OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        thisOLI.OrdwayListPrice__c = thisSetting.UseOrdwayPrice__c ? lPrice : 0;
      } else if (typeZ == OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        if (
          SObjectHelper.isFieldCreatable(
            SObjectType.OpportunityLineItem,
            Schema.OpportunityLineItem.UnitPrice
          )
        ) {
          thisOLI.UnitPrice = thisSetting.UseOrdwayPrice__c
            ? lPrice / thisOLI.Quantity
            : 0;
        }
        if (
          SObjectHelper.isFieldCreatable(
            SObjectType.OpportunityLineItem,
            Schema.OpportunityLineItem.OrdwayListPrice__c
          )
        ) {
          thisOLI.OrdwayListPrice__c = thisSetting.UseOrdwayPrice__c
            ? lPrice
            : 0;
        }
      }
    }
  }

  /**
   * @description Calculate Volume Pricing For Opportunity Line Item
   * @param thisOLI Opportunity Line Item Object
   * @param tierMap Ordway Tier Values
   */
  public static Decimal calculateTieredPricing(
    OpportunityLineItem thisOLI,
    Map<String, Object> tierMap,
    Decimal listPrice
  ) {
    Integer startingUnit = Integer.ValueOf(tierMap.get('starting_unit'));
    String typeZ = String.valueOf(tierMap.get('type'));
    Decimal lPrice = Decimal.valueOf(String.valueOf(tierMap.get('price')));
   
    if (
      thisOLI.Quantity > startingUnit &&
      tierMap.get('ending_unit') != '' && tierMap.get('ending_unit') != null &&
      thisOLI.Quantity >= Integer.ValueOf(tierMap.get('ending_unit'))
    ) {
      // NOT EQ FLAT FEE
      if (typeZ != OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice =
          listPrice +
          (Integer.ValueOf(tierMap.get('ending_unit')) - startingUnit) * lPrice;
      } else if (typeZ == OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice = listPrice + lPrice;
      }
    } else if (
      thisOLI.Quantity > startingUnit &&
      (tierMap.get('ending_unit') == '' || tierMap.get('ending_unit') == null ||
      (tierMap.get('ending_unit') != '' &&
      thisOLI.Quantity < Integer.ValueOf(tierMap.get('ending_unit'))))
    ) {
      // NOT EQ FLAT FEE
      if (typeZ != OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice = listPrice + (thisOLI.Quantity - startingUnit) * lPrice;
      } else if (typeZ == OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice = listPrice + lPrice;
      }
    }
    if (
      SObjectHelper.isFieldCreatable(
        SObjectType.OpportunityLineItem,
        Schema.OpportunityLineItem.OrdwayListPrice__c
      )
    ) {
      thisOLI.OrdwayListPrice__c = listPrice;
    }
    return listPrice;
  }

  /**
   * @description Calculate Volume Pricing For Quote Line Item
   * @param thisOLI Quote Line Item Object
   * @param tierMap Ordway Tier Values
   */
  public static void calculateQuoteVolumePricing(
    QuoteLineItem__c thisQLI,
    Map<String, Object> tierMap
  ) {
    Integer startingUnit = Integer.ValueOf(tierMap.get('starting_unit'));
    String typeZ = String.valueOf(tierMap.get('type'));
    Decimal lPrice = Decimal.valueOf(String.valueOf(tierMap.get('price')));
    ContractPlanPickerSetting__c thisSetting = AddEditPlanOrdwayController.getOpportunityCustomSetting();

    if (
      thisQLI.Quantity__c > startingUnit &&
      (tierMap.get('ending_unit') == '' || tierMap.get('ending_unit') == null ||
      (tierMap.get('ending_unit') != '' &&
      thisQLI.Quantity__c <= Integer.ValueOf(tierMap.get('ending_unit')))) &&
      SObjectHelper.isFieldCreatable(
        SObjectType.QuoteLineItem__c,
        Schema.QuoteLineItem__c.OrdwayListPrice__c
      )
    ) {
      if (typeZ != OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        thisQLI.OrdwayListPrice__c = thisSetting.UseOrdwayPrice__c ? lPrice : 0;
      } else if (typeZ == OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        if (
          SObjectHelper.isFieldCreatable(
            SObjectType.QuoteLineItem__c,
            Schema.QuoteLineItem__c.OrdwayListPrice__c
          )
        ) {
          thisQLI.OrdwayListPrice__c = thisSetting.UseOrdwayPrice__c
            ? lPrice
            : 0;
        }
      }
    }
  }

  /**
   * @description Calculate Volume Pricing For Quote Line Item
   * @param thisQLI Quote Line Item Object
   * @param tierMap Ordway Tier Values
   */
  public static Decimal calculateQuoteTieredPricing(
    QuoteLineItem__c thisQLI,
    Map<String, Object> tierMap,
    Decimal listPrice
  ) {
    Integer startingUnit = Integer.ValueOf(tierMap.get('starting_unit'));
    String typeZ = String.valueOf(tierMap.get('type'));
    Decimal lPrice = Decimal.valueOf(String.valueOf(tierMap.get('price')));
   
    if (
      thisQLI.Quantity__c > startingUnit &&
      tierMap.get('ending_unit') != '' && tierMap.get('ending_unit') != null &&
      thisQLI.Quantity__c >= Integer.ValueOf(tierMap.get('ending_unit'))
    ) {
      // NOT EQ FLAT FEE
      if (typeZ != OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice =
          listPrice +
          (Integer.ValueOf(tierMap.get('ending_unit')) - startingUnit) * lPrice;
      } else if (typeZ == OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice = listPrice + lPrice;
      }
    } else if (
      thisQLI.Quantity__c > startingUnit &&
      (tierMap.get('ending_unit') == '' || tierMap.get('ending_unit') == null ||
      (tierMap.get('ending_unit') != '' &&
      thisQLI.Quantity__c < Integer.ValueOf(tierMap.get('ending_unit'))))
    ) {
      // NOT EQ FLAT FEE
      if (typeZ != OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice = listPrice + (thisQLI.Quantity__c - startingUnit) * lPrice;
      } else if (typeZ == OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice = listPrice + lPrice;
      }
    }
    if (
      SObjectHelper.isFieldCreatable(
        SObjectType.QuoteLineItem__c,
        Schema.QuoteLineItem__c.OrdwayListPrice__c
      )
    ) {
      thisQLI.OrdwayListPrice__c = listPrice;
    }
    return listPrice;
  }

  /**
   * @description Method to get Read Only fields component and data displayed in Add/Edit plan picker
   * @param readOnlyFieldSet Field Set based on which Read only Field component and data is constructed
   * @return Map<String, RequiredFieldWrapper> Map of Read only Field component and data
   **/
  public static Map<String, RequiredFieldWrapper> getReadOnlyFieldsWrapper(
    Schema.FieldSet readOnlyFieldSet, String currencyISO
  ) {
    
    Map<String, RequiredFieldWrapper> ReadOnlyFieldWrapperMap = new Map<String, RequiredFieldWrapper>();

    for (Schema.FieldSetMember thisFieldMember : readOnlyFieldSet.getFields()) {
      DescribeFieldResult fieldDescribe = thisFieldMember.getSObjectField()
        .getDescribe();
      String thisFieldType = String.ValueOf(fieldDescribe.getType());

      Map<String, String> componentOutputTypeMap = new Map<String, String>{
                                                              'CURRENCY'  => 'lightning:formattedNumber',
                                                              'DOUBLE'    => 'lightning:formattedNumber',
                                                              'PERCENT'   => 'lightning:formattedNumber',
                                                              'INTEGER'   => 'lightning:formattedNumber',
                                                              'LONG'      => 'lightning:formattedNumber',
                                                              'DATE'      => 'lightning:formattedDateTime'
                                                            };

      Map<String, String> numberStyleMap = new Map<String, String>{
                                                'CURRENCY'  => 'currency',
                                                'PERCENT'   => 'percent-fixed'
                                              };
        
                                                  
      if (
        fieldDescribe.isCreateable() &&
        fieldDescribe.isCustom() &&
        !fieldDescribe.isAutoNumber() &&
        !thisFieldType.contains('REFERENCE') &&
        thisFieldType != 'BOOLEAN'
      ) {
        RequiredFieldWrapper thisWrapper = new RequiredFieldWrapper();
        ComponentAttributes thisComponentAttributes = new ComponentAttributes();

        thisWrapper.name = fieldDescribe.getName();
        thisWrapper.label = fieldDescribe.getLabel();

        if(componentOutputTypeMap.containsKey(thisFieldType)){
          thisWrapper.componentType = componentOutputTypeMap.get(thisFieldType);
        } 
        else{
          thisWrapper.componentType = 'lightning:formattedText';
        } 

        if(numberStyleMap.containsKey(thisFieldType)){
          thisComponentAttributes.style = numberStyleMap.get(thisFieldType);
        }

        if(thisFieldType == 'CURRENCY'){
          thisComponentAttributes.currencyCode = currencyISO;
          thisComponentAttributes.currencyDisplayAs = 'symbol';
        }

        if(thisFieldType == 'DATE'){
          thisComponentAttributes.year = 'numeric';
          thisComponentAttributes.month = 'numeric';
          thisComponentAttributes.day = 'numeric';
        }

        thisWrapper.attributeObject = thisComponentAttributes;

        ReadOnlyFieldWrapperMap.put(thisWrapper.name, thisWrapper);
      }
    }
    return ReadOnlyFieldWrapperMap;
  }

  /**
   * @description Method to get Dynamic fields component and data displayed in Add/Edit plan picker
   * @param dynamicFieldSet Field Set based on which Dynamic Field component and data is constructed
   * @return Map<String, RequiredFieldWrapper> Map of Dynamic Field component and data
   **/
  public static Map<String, RequiredFieldWrapper> getDynamicFieldsWrapper(
    Schema.FieldSet dynamicFieldSet
  ) {
    //Read only fields on the plan picker
    Set<String> staticFieldsToexclude = new Set<String>{
      'OrdwayLabs__ChargeName__c',
      'OrdwayLabs__TierJSON__c',
      'OrdwayLabs__ChargeType__c',
      'OrdwayLabs__PricingModel__c',
      'OrdwayLabs__Product_Name__c'
    };

    Map<String, RequiredFieldWrapper> RequiredFieldWrapperMap = new Map<String, RequiredFieldWrapper>();

    for (Schema.FieldSetMember thisFieldMember : dynamicFieldSet.getFields()) {
      DescribeFieldResult fieldDescribe = thisFieldMember.getSObjectField()
        .getDescribe();
      String thisFieldType = String.ValueOf(fieldDescribe.getType());
      if (
        fieldDescribe.isCreateable() &&
        (fieldDescribe.isCustom() || fieldDescribe.getName() == 'Discount' || fieldDescribe.getName() == 'Quantity') &&
        !fieldDescribe.isAutoNumber() &&
        !staticFieldsToexclude.contains(fieldDescribe.getName()) &&
        !thisFieldType.contains('REFERENCE')
      ) {
        RequiredFieldWrapper thisWrapper = new RequiredFieldWrapper();
        thisWrapper.name = fieldDescribe.getName();
        thisWrapper.label = fieldDescribe.getLabel();
        populateAttribute(thisFieldType, thisWrapper, fieldDescribe);
        RequiredFieldWrapperMap.put(thisWrapper.name, thisWrapper);
        if(thisFieldType == 'CURRENCY'){
          thisWrapper.isCurrency = true;
        }
      }
    }
    return RequiredFieldWrapperMap;
  }

  /**
   * @description Method to Populate attributes for Dynamic field component
   * @param fieldType Field Type
   * @param thisWrapper Dynamic Field component wrapper
   * @param fieldDescribe DescribeFieldResult for particular field
   **/
  public static void populateAttribute(
    String fieldType,
    RequiredFieldWrapper thisWrapper,
    DescribeFieldResult fieldDescribe
  ) {
    ComponentAttributes thisComponentAttributes = new ComponentAttributes();

    if (componentTypeMap.containsKey(fieldType)) {
      thisWrapper.componentType = componentTypeMap.get(fieldType);
    }

    if (inputTypeMap.containsKey(fieldType)) {
      thisComponentAttributes.type = inputTypeMap.get(fieldType);
    }

    if (inputTypeFormatterMap.containsKey(fieldType)) {
      thisComponentAttributes.formatter = inputTypeFormatterMap.get(fieldType);
    }

    if (inputTypeNumberStepsMap.containsKey(fieldType)) {
      thisComponentAttributes.step = inputTypeNumberStepsMap.get(fieldType);
    }

    thisComponentAttributes.name = thisWrapper.name;
    thisComponentAttributes.label = thisWrapper.name;
    thisComponentAttributes.variant = 'label-hidden';

    if (fieldType == 'PICKLIST') {
      populatePicklistValue(
        fieldDescribe.getPicklistValues(),
        thisComponentAttributes
      );
    }

    if (
      fieldDescribe.getLength() != null &&
      (fieldType == 'STRING' ||
      fieldType == 'TEXTAREA')
    ) {
      thisComponentAttributes.maxlength = String.valueOf(
        fieldDescribe.getLength()
      );
    }

    if (fieldType == 'BOOLEAN') {
      thisComponentAttributes.checked = false;
    }

    if (fieldDescribe.isDefaultedOnCreate()) {
      thisComponentAttributes.value = String.valueOf(
        fieldDescribe.getDefaultValue()
      );
    }
    thisWrapper.attributeObject = thisComponentAttributes;
  }

  /**
   * @description Method to construct picklist options for the Dynamic field if field type is picklist
   * @param pickEntries PicklistEntry of the Picklist field
   * @param thisReqWrapperAttribute Attribute Wrapper of Dynamic Picklist component
   **/
  private static void populatePicklistValue(
    List<Schema.PicklistEntry> pickEntries,
    ComponentAttributes thisReqWrapperAttribute
  ) {
    List<PickListWrapper> pickListWrapperList = new List<PickListWrapper>();

    for (Schema.PicklistEntry thisEntry : pickEntries) {
      if (thisEntry.isActive()) {
        pickListWrapperList.add(
          new PickListWrapper(thisEntry.getLabel(), thisEntry.getValue())
        );
      }
    }
    thisReqWrapperAttribute.options = pickListWrapperList;
  }

  /**
   * @description Initial Wrapper data for Add / Edit Plan component invoked from component's Init function
   **/
  public class InitialWrapper {
    @AuraEnabled
    public Map<String, List<QuoteLineItem__c>> qliMap;
    @AuraEnabled
    public Map<String, List<OpportunityLineItem>> oliMap;
    @AuraEnabled
    public Map<String, RequiredFieldWrapper> requiredPlanObjectWrapper;
    @AuraEnabled
    public Map<String, RequiredFieldWrapper> readOnlyPlanObjectWrapper;
    @AuraEnabled
    public String salesforceBaseURL;
    @AuraEnabled
    public String currencyISO;
    @AuraEnabled
    public Opportunity thisOpportunity;
    @AuraEnabled
    public OrdwayLabs__Quote__c thisOrdwayQuote;
  }

  /**
   * @description Wrapper data for dynamic field component constructed for Add / Edit Plan page
   **/
  public class RequiredFieldWrapper {
    @AuraEnabled
    public String componentType { get; set; }
    @AuraEnabled
    public String name { get; set; }
    @AuraEnabled
    public String label { get; set; }
    @AuraEnabled
    public Boolean isCurrency { get; set; }
    @AuraEnabled
    public ComponentAttributes attributeObject { get; set; }
  }

  /**
   * @description Wrapper data for dynamic field component attribute
   **/
  public class ComponentAttributes {
    @AuraEnabled
    public String name { get; set; }
    @AuraEnabled
    public String label { get; set; }
    @AuraEnabled
    public String variant { get; set; }
    @AuraEnabled
    public String placeholder { get; set; }
    @AuraEnabled
    public String type { get; set; }
    @AuraEnabled
    public Boolean checked { get; set; }
    @AuraEnabled
    public String maxlength { get; set; }
    @AuraEnabled
    public Boolean required { get; set; }
    @AuraEnabled
    public String value { get; set; }
    @AuraEnabled
    public String onchange { get; set; }
    @AuraEnabled
    public String formatter { get; set; }
    @AuraEnabled
    public String step { get; set; }
    @AuraEnabled
    public String style { get; set; }
    @AuraEnabled
    public String currencyDisplayAs { get; set; }
    @AuraEnabled
    public String currencyCode { get; set; }
    @AuraEnabled
    public String year { get; set; }
    @AuraEnabled
    public String month { get; set; }
    @AuraEnabled
    public String day { get; set; }
    @AuraEnabled
    public List<PickListWrapper> options { get; set; }
  }

  /**
   * @description Wrapper data for picklist field component
   **/
  public class PickListWrapper {
    @AuraEnabled
    public String label { get; set; }
    @AuraEnabled
    public String value { get; set; }
    PickListWrapper(String pickLabel, String pickValue) {
      this.label = pickLabel;
      this.value = pickValue;
    }
  }

  static Map<String, String> componentTypeMap = OrdwayUtil.componentTypeMap;
  static Map<String, String> inputTypeMap = OrdwayUtil.inputTypeMap;
  static Map<String, String> inputTypeFormatterMap = OrdwayUtil.inputTypeFormatterMap;
  static Map<String, String> inputTypeNumberStepsMap = OrdwayUtil.inputTypeNumberStepsMap;
}