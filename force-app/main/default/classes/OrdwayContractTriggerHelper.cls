/**
 * @File Name          : OrdwayContractTriggerHelper.cls
 * @Description        : Ordway Contract Trigger Helper
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/8/2020        Initial Version
 **/
public with sharing class OrdwayContractTriggerHelper extends TriggerHandler.TriggerHandlerBase {
  //To recursively execute the code in the same transaction, set this variable to true
  //Set it to false on after update
  public static Boolean runOnce = true;

  public override void beforeUpdate(
    Map<Id, SObject> mapOldSObjs,
    Map<Id, SObject> mapNewSObjs
  ) {
    //Won't Support Bulkification, So Take Only The First Record
    if (canSync((OrdwayContract__c) mapNewSObjs.values()[0])) {
      OrdwayContract__c thisContract = (OrdwayContract__c) mapNewSObjs.values()[0];
      thisContract.SyncStatus__c = OrdwayHelper.SYNC_STATE_SYNC_IN_PROGRESS;
    }
  }

  public override void afterUpdate(
    Map<Id, SObject> mapOldSObjs,
    Map<Id, SObject> mapNewSObjs
  ) {
    //Won't Support Bulkification, So Take Only The First Record
    if (canSync((OrdwayContract__c) mapNewSObjs.values()[0])) {
      OrdwayContractController.aSyncToOrdway(
        new List<Id>(mapNewSObjs.keySet())[0]
      );
    }

    runOnce = false;
  }

  private static Boolean canSync(OrdwayContract__c thisContract) {
    if (
      thisContract.SyncStatus__c != OrdwayHelper.SYNC_STATE_DO_NOT_SEND &&
      thisContract.TotalContractQuantity__c != 0 &&
      thisContract.ContractEffectiveDate__c != null &&
      ((!thisContract.DeferStartDate__c &&
      thisContract.BillingStartDate__c != null &&
      thisContract.ServiceStartDate__c != null) 
      || thisContract.DeferStartDate__c) &&
      thisContract.Account__c != null
    ) {
      return true;
    }
    return false;
  }
}