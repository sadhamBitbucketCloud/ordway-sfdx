/**
 * @File Name          : OpportunitySettingController_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 10-14-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    12/31/2019   Ordway Labs     Initial Version
 **/
@isTest
private class OpportunitySettingController_Test {
  @isTest
  static void Test_OpportunitySetting() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();

    OpportunitySettingController.saveOpportunitySetting(
      TestObjectFactory.createContractPlanPickerSetting()
    );

    Pricebook2 newPB = TestObjectFactory.createPricebook();
    insert newPB;

    System.assertEquals(
      OrdwayHelper.OPPORTUNITY_SETTING_NAME,
      OpportunitySettingController.getOpportunitySetting()
        .get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
        .Name,
      'Should return the name of the record'
    );

    System.assertEquals(
      OrdwayHelper.DEFAULT_OPPORTUNITY_STAGE,
      OpportunitySettingController.getOpportunitySyncStage(),
      'Should return the Opportunity Sync Stage'
    );

    System.assertEquals(
      true,
      OpportunitySettingController.isAutomaticSyncEnabled(),
      'Should be equal to true'
    );

    System.assertEquals(
      false,
      OpportunitySettingController.isAutoActivateContract(),
      'Should be equal to false'
    );

    System.assertEquals(
      false,
      OpportunitySettingController.isAutomaticLinkEnabled(),
      'Should be equal to false'
    );

    System.assertEquals(
      true,
      OpportunitySettingController.useOrdwayCalculation(),
      'Should be equal to true'
    );

    System.assertEquals(
      false,
      OpportunitySettingController.syncQuoteAutomatically(),
      'Should be equal to false'
    );

    System.assertEquals(
      false,
      OpportunitySettingController.isAutoSyncFromOrdway(),
      'Should be equal to true'
    );

    System.assertEquals(
      Test.getStandardPricebookId(),
      OpportunitySettingController.getOrdwayPriceBookId(),
      'Should return the standard pricebook Id'
    );

    System.assert(
      OpportunitySettingController.getOpportunityStagewithoutRecordType()
        .size() > 0,
      'Return value should not be blank'
    );

    System.assert(
      OpportunitySettingController.getOrdwaySubscriptionStatus().size() > 0,
      'Return value should not be blank'
    );

    OpportunitySettingController.getExistingPricebook();
    //OpportunitySettingController.getOpportunitySettingWrappper();

    Test.stopTest();
  }

  @isTest
  static void Test_OpportunitySettingSave() {
    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.OpportunityStage__c = null;

    try {
      OpportunitySettingController.saveOpportunitySetting(newSetting);
    } catch (Exception thisException) {
      Map<String, Object> mapExceptionMessage = (Map<String, Object>) JSON.deserializeUntyped(
        thisException.getMessage()
      );
      System.assertEquals(
        mapExceptionMessage.get('name'),
        'System.DmlException',
        'Should throw DML Exception'
      );

      System.assertEquals(
        mapExceptionMessage.get('message'),
        'Upsert failed. First exception on row 0; first error: REQUIRED_FIELD_MISSING, Required fields are missing: [OrdwayLabs__OpportunityStage__c]: [OrdwayLabs__OpportunityStage__c]',
        'DML Exception Message'
      );
    }
  }

  @isTest
  static void OpportunitySettingWrapperTest() {
    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.OpportunityStage__c = null;

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = '{"payload":{"quotes":false},"success":true}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    OpportunitySettingController.OpportunitySettingWrappper newOpportunitySettingWrappper = new OpportunitySettingController.OpportunitySettingWrappper();
    newOpportunitySettingWrappper = OpportunitySettingController.getOpportunitySettingWrappper();
    Test.stopTest();
    system.assertEquals(
      false,
      newOpportunitySettingWrappper.hasQuoteLicense,
      'should return false as response'
    );
  }

  @isTest
  static void OpportunitySettingWrapperQuoteEnabledTest() {
    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.OpportunityStage__c = null;

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = '{"payload":{"quotes":true},"success":true}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    OpportunitySettingController.OpportunitySettingWrappper newOpportunitySettingWrappper = new OpportunitySettingController.OpportunitySettingWrappper();
    newOpportunitySettingWrappper = OpportunitySettingController.getOpportunitySettingWrappper();
    Test.stopTest();
    system.assertEquals(
      true,
      newOpportunitySettingWrappper.hasQuoteLicense,
      'should return true as response'
    );
  }

  @isTest
  static void hasQuoteLicense() {
    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.OpportunityStage__c = null;

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = '{"payload":{"quotes":true},"success":true}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    Boolean hasQuoteLicense = OpportunitySettingController.hasQuoteLicense();
    Test.stopTest();
    system.assertEquals(
      true,
      hasQuoteLicense,
      'should return true as response'
    );
  }

  @isTest
  static void isAutoSyncQuoteFromOrdway() {
    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.OpportunityStage__c = null;
    Test.startTest();
    Boolean isAutoSyncQuoteFromOrdway = OpportunitySettingController.isAutoSyncQuoteFromOrdway();
    Test.stopTest();
    system.assertEquals(
      false,
      isAutoSyncQuoteFromOrdway,
      'should return false'
    );
  }

  @isTest
  static void isAutoSyncAccountToOrdwayTrue() {
    OpportunitySettingController.saveOpportunitySetting(
      TestObjectFactory.createContractPlanPickerSetting()
    );
    Test.startTest();
    Boolean isAutoSyncAccountToOrdway = OpportunitySettingController.isAutoSyncAccountToOrdway();
    Test.stopTest();
    system.assertEquals(
      true,
      isAutoSyncAccountToOrdway,
      'should return true'
    );
  }

  @isTest
  static void isAutoSyncContactToOrdwayTrue() {
    OpportunitySettingController.saveOpportunitySetting(
      TestObjectFactory.createContractPlanPickerSetting()
    );
    Test.startTest();
    Boolean isAutoSyncContactToOrdway = OpportunitySettingController.isAutoSyncContactToOrdway();
    Test.stopTest();
    system.assertEquals(
      true,
      isAutoSyncContactToOrdway,
      'should return true'
    );
  }

  @isTest
  static void isAutoSyncAccountToOrdwayFalse() {
    Test.startTest();
    Boolean isAutoSyncAccountToOrdway = OpportunitySettingController.isAutoSyncAccountToOrdway();
    Test.stopTest();
    system.assertEquals(
      false,
      isAutoSyncAccountToOrdway,
      'should return false'
    );
  }

  @isTest
  static void isAutoSyncContactToOrdwayFalse() {
    Test.startTest();
    Boolean isAutoSyncContactToOrdway = OpportunitySettingController.isAutoSyncContactToOrdway();
    Test.stopTest();
    system.assertEquals(
      false,
      isAutoSyncContactToOrdway,
      'should return false'
    );
  }

  @isTest
  static void activateQuote() {
    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.OpportunityStage__c = null;
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    System.assert(thisApplicationsetting.Name != null);

    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = '{"payload":{"quotes":true},"success":true}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    try {
      OpportunitySettingController.activateQuote();
    } catch (Exception ex) {
    }
    Test.stopTest();
  }
}