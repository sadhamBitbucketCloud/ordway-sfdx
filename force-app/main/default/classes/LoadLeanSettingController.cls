/**
 * @File Name          : LoadLeanSettingController.cls
 * @Description        : Lead custom setting Controller class
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    11/24/2019   Ordway Labs     Initial Version
 **/
public with sharing class LoadLeanSettingController {
  /**
   * @description Method to check whether Trigger is disabled for the current user
   * @return Boolean true or false
   **/
  public static Boolean isTriggerDisabled() {
    return LoadLeanSettings__c.getInstance(UserInfo.getUserId())
      .DisableTriggers__c;
  }

  /**
   * @description Method to check whether Application Error Log needs to be created Synchronously for the current user
   * @return Boolean true or false
   **/
  public static Boolean isSyncLogging() {
    return LoadLeanSettings__c.getInstance(UserInfo.getUserId())
      .CreateApplicationLogSynchronously__c;
  }

  /**
   * @description Method to check whether all type of Application logs is enabled for the current user
   * @return Boolean true or false
   **/
  public static Boolean allLogs() {
    return LoadLeanSettings__c.getInstance(UserInfo.getUserId()).AllLogs__c;
  }

  /**
   * @description Method to get Change/Renewal Layout Configuration
   * @param isRenewal variable to indicate whether the Opportunity type is Renewal or not
   * @return String
   **/
  public static String getOpportunityPageLayoutName(Boolean isRenewal) {
    LoadLeanSettings__c llsUser = LoadLeanSettings__c.getInstance(
      UserInfo.getUserId()
    );
    return llsUser.OrdwayLabs__Details__c != null
      ? getOpportunityPageLayoutName(llsUser.OrdwayLabs__Details__c, isRenewal)
      : null;
  }

  /**
   * @description Method to get Change/Renewal Layout Configuration
   * @param Details Page Layout Configuration in JSON Format
   * @param isRenewal variable to indicate whether the Opportunity type is Renewal or not
   * @return String Oppirtunity Page layout name
   **/
  private static String getOpportunityPageLayoutName(
    String Details,
    Boolean isRenewal
  ) {
    List<Object> thisSettings = (List<Object>) JSON.deserializeUntyped(Details);
    if (!thisSettings.isEmpty()) {
      for (Object thisObject : thisSettings) {
        Map<String, Object> thisSettingObject = (Map<String, Object>) thisObject;

        string ORDWAY_SETTING_TYPE_CHANGE = OrdwayHelper.ORDWAY_OPPORTUNITY_TYPE_CHANGE;
        string ORDWAY_SETTING_TYPE_RENEWAl = OrdwayHelper.ORDWAY_OPPORTUNITY_TYPE_RENEWAL;

        if (
          String.valueOf(thisSettingObject.get('type')) != null &&
          !String.isEmpty(String.valueOf(thisSettingObject.get('type'))) &&
          String.valueOf(thisSettingObject.get('layout')) != null &&
          !String.isEmpty(String.valueOf(thisSettingObject.get('layout')))
        ) {
          if (
            isRenewal &&
            String.valueOf(thisSettingObject.get('type')) ==
            ORDWAY_SETTING_TYPE_RENEWAl
          ) {
            return String.valueOf(thisSettingObject.get('layout'));
          } else if (
            !isRenewal &&
            String.valueOf(thisSettingObject.get('type')) ==
            ORDWAY_SETTING_TYPE_CHANGE
          ) {
            return String.valueOf(thisSettingObject.get('layout'));
          }
        }
      }
    }
    return null;
  }
  /**
   * @description Method to check whether the plan picker 2.0 is enabled for the current user
   * @return Boolean true or false
   **/
  public static Boolean isPlanPicker2Enabled() {
    return LoadLeanSettings__c.getOrgDefaults().PlanPicker__c; //Org Wide Setting Only
  }
}