/**
 * @File Name          : SObjectHelper.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 12-29-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    10/13/2019   Ordway Labs     Initial Version
 **/
public with sharing class SObjectHelper {
  /**
   * @description Method to validate the current user has access to populate the value in the field
   * @param sObjectDescription Sobject Description
   * @param fieldName Field API Name
   * @return Boolean True or false
   **/
  public static Boolean isFieldCreatable(
    Schema.DescribeSObjectResult sObjectDescription,
    Schema.SObjectField fieldName
  ) {
    Schema.DescribeFieldResult fieldDescribe = getFieldDescribe(
      sObjectDescription,
      fieldName
    );
    return fieldDescribe != null && fieldDescribe.isCreateable();
  }

  /**
   * @description Method to validate the current user has access to populate the value in the field
   * @param sObjectDescription Sobject Description
   * @param fieldName Field API Nam
   * @return Boolean True or false
   **/
  public static Boolean isFieldCreatable(
    Schema.DescribeSObjectResult sObjectDescription,
    String fieldName
  ) {
    Schema.DescribeFieldResult fieldDescribe = getFieldDescribe(
      sObjectDescription,
      fieldName
    );
    return fieldDescribe != null && fieldDescribe.isCreateable();
  }

  /**
   * @description Method to validate the current user has access to change the value in the field
   * @param sObjectDescription Sobject Description
   * @param fieldName Field API Nam
   * @return Boolean True or false
   **/
  public static Boolean isFieldUpdatable(
    Schema.DescribeSObjectResult sObjectDescription,
    Schema.SObjectField fieldName
  ) {
    Schema.DescribeFieldResult fieldDescribe = getFieldDescribe(
      sObjectDescription,
      fieldName
    );
    return fieldDescribe != null && fieldDescribe.isUpdateable();
  }

  /**
   * @description Method to validate the current user has access to change the value in the field
   * @param sObjectDescription Sobject Description
   * @param fieldName Field API Nam
   * @return Boolean True or false
   **/
  public static Boolean isFieldUpdatable(
    Schema.DescribeSObjectResult sObjectDescription,
    String fieldName
  ) {
    Schema.DescribeFieldResult fieldDescribe = getFieldDescribe(
      sObjectDescription,
      fieldName
    );
    return fieldDescribe != null && fieldDescribe.isUpdateable();
  }

  /**
   * @description Method to check whether the Multi currency is enabled in the current org
   * @return Boolean true or false
   **/
  public static Boolean isMultiCurrencyOrg() {
    return UserInfo.isMultiCurrencyOrganization() ? true : false;
  }

  /**
   * @description Method to get List of all field in the Sobject
   * @param thisSObjectType Sobject type whose field needs to be retrieved
   * @return List<String> List of all field in the Sobject
   **/
  public static List<String> getAllFields(Schema.SObjectType thisSObjectType) {
    return getAllFields(thisSObjectType.getDescribe());
  }

  /**
   * @description Method to get List of all field in the Sobject
   * @param thisSObjectDescribe Sobject Description
   * @return List<String> List of all field in the Sobject
   **/
  public static List<String> getAllFields(
    Schema.DescribeSObjectResult thisSObjectDescribe
  ) {
    Set<String> allFields = new Set<String>();
    Boolean isQuotesEnabled = OpportunitySettingController.isQuotesEnabled();

    for (String fieldName : getFieldsMap(thisSObjectDescribe).keySet()) {
      Schema.DescribeFieldResult fieldDescribe = getFieldDescribe(
        thisSObjectDescribe,
        fieldName
      );

      if (
        fieldDescribe != null &&
        fieldDescribe.getName() != 'LastViewedDate' &&
        fieldDescribe.getName() != 'LastReferencedDate' &&
        fieldDescribe.getName() != 'SystemModstamp' &&
        fieldDescribe.getName() != 'Product_Name__c' &&
        fieldDescribe.getName() != 'OrdwayLabs__SyncedQuoteId__c' ||
        (isQuotesEnabled &&
        fieldDescribe.getName() == 'OrdwayLabs__SyncedQuoteId__c')
      ) {
        allFields.add(fieldDescribe.getName());
      }
    }
    return new List<String>(allFields);
  }

  static Map<String, Schema.DescribeFieldResult> describeFieldResultMap = new Map<String, Schema.DescribeFieldResult>();

  /**
   * @description Method to get Sobject Field Description
   * @param sObjectDescription Sobject Description
   * @param fieldName Field API Name
   * @return Schema.DescribeFieldResult
   **/
  public static Schema.DescribeFieldResult getFieldDescribe(
    Schema.DescribeSObjectResult sObjectDescription,
    Schema.SObjectField fieldName
  ) {
    return getFieldDescribe(sObjectDescription, String.valueOf(fieldName));
  }

  /**
   * @description Method to get Sobject Field Description
   * @param sObjectDescription Sobject Description
   * @param fieldName Field API Name
   * @return Schema.DescribeFieldResult
   **/
  public static Schema.DescribeFieldResult getFieldDescribe(
    Schema.DescribeSObjectResult sObjectDescription,
    String fieldName
  ) {
    String cacheKey =
      sObjectDescription.getName().toUpperCase() +
      String.valueOf(fieldName).toUpperCase();

    Schema.DescribeFieldResult fieldDescribe;
    if (!describeFieldResultMap.containsKey(cacheKey)) {
      Map<String, Schema.SObjectField> fieldMap = getFieldsMap(
        sObjectDescription
      );

      fieldDescribe = (fieldMap == null || fieldMap.isEmpty() ||
        !fieldMap.containsKey(fieldName))
        ? null
        : fieldMap.get(fieldName).getDescribe();
      describeFieldResultMap.put(cacheKey, fieldDescribe);
    } else {
      fieldDescribe = describeFieldResultMap.get(cacheKey);
    }
    return fieldDescribe;
  }

  static Map<String, Map<String, Schema.SObjectField>> sobjFieldsMap = new Map<String, Map<String, Schema.SObjectField>>();

  /**
   * @description Method to get Fields of a Sobject as Map
   * @param thisSObjectType Sobject Type whose Field needs to be retrieved
   * @return Map<String, Schema.SObjectField>
   **/
  public static Map<String, Schema.SObjectField> getFieldsMap(
    Schema.SObjectType thisSObjectType
  ) {
    return getFieldsMap(thisSObjectType.getDescribe());
  }

  /**
   * @description Method to get Fields of a Sobject as Map
   * @param thisSObjectType Sobject Type whose Field needs to be retrieved
   * @return Map<String, Schema.SObjectField>
   **/
  public static Map<String, Schema.SObjectField> getFieldsMap(
    Schema.DescribeSObjectResult sObjectDescription
  ) {
    String cacheKey = sObjectDescription.getName().toUpperCase();

    if (!sobjFieldsMap.containsKey(cacheKey)) {
      sobjFieldsMap.put(cacheKey, sObjectDescription.fields.getMap());
    }
    return sobjFieldsMap.get(cacheKey);
  }

  /**
   * @description Method To get Map of all Sobject field Type for a Sobject
   * @param sObjectName sObject API Name
   * @return Map<String, String> Map of all Sobject field Type
   **/
  public static Map<String, String> getSobjectFieldType(String sObjectName) {
    return getSobjectFieldType(
      ((SObject) Type.forName(sObjectName).newInstance()).getSObjectType()
    );
  }

  /**
   * @description Method To get Map of all Sobject field label for a Sobject
   * @param sObjectName sObject API Name
   * @return Map<String, String> Map of all Sobject field label
   **/
  public static Map<String, String> getSobjectFieldLabel(String sObjectName) {
    return getSobjectFieldLabel(
      ((SObject) Type.forName(sObjectName).newInstance()).getSObjectType()
    );
  }

  /**
   * @description Method To get Map of all Sobject field Type for a Sobject
   * @param thisSObjectType Sobject Type whose fields type needs to be retrieved
   * @return Map<String, String> Map of all Sobject field Types
   **/
  public static Map<String, String> getSobjectFieldType(
    SObjectType thisSObjectType
  ) {
    Map<String, String> fieldDataTypeMap = new Map<String, String>();
    Map<String, SObjectField> sObjFields = thisSObjectType.getDescribe()
      .fields.getMap();

    for (String fieldName : sObjFields.keySet()) {
      DescribeFieldResult fieldDescribe = sObjFields.get(fieldName)
        .getDescribe();
      DisplayType fieldType = fieldDescribe.getType();
      fieldDataTypeMap.put(fieldName, String.valueOf(fieldType));
    }
    return fieldDataTypeMap;
  }

  /**
   * @description Method To get Map of all Sobject field label for a Sobject
   * @param thisSObjectType Sobject Type whose fields label needs to be retrieved
   * @return Map<String, String> Map of all Sobject field labels
   **/
  public static Map<String, String> getSobjectFieldLabel(
    SObjectType thisSObjectType
  ) {
    Map<String, String> fieldDataLabelMap = new Map<String, String>();
    Map<String, SObjectField> sObjFields = thisSObjectType.getDescribe()
      .fields.getMap();

    for (String fieldName : sObjFields.keySet()) {
      DescribeFieldResult fieldDescribe = sObjFields.get(fieldName)
        .getDescribe();

      if (
        !fieldDescribe.isCustom() &&
        fieldName != OrdwayHelper.SOBJECT_FIELD_NAME &&
        fieldName != OrdwayHelper.SOBJECT_FIELD_ID
      )
        continue; //Exclude Standard Fields Except Name, Id
      fieldDataLabelMap.put(
        fieldName,
        String.valueOf(fieldDescribe.getLabel())
      );
    }
    return fieldDataLabelMap;
  }

  /**
   * @description Method to check whether the value is update in Sobject
   * @param fieldToCheck SObject Field which needs to be checked
   * @param sRecord Sobject Record
   * @param sObjectMap Sobject Map in which value needs to be compared
   * @return Boolean true or false
   **/
  public static Boolean isChanged(
    Schema.sObjectField fieldToCheck,
    sObject sRecord,
    Map<Id, SObject> sObjectMap
  ) {
    return sRecord.get(fieldToCheck) != null &&
      sRecord.get(fieldToCheck) != sObjectMap.get(sRecord.Id).get(fieldToCheck)
      ? true
      : false;
  }

  /**
   * @description Method to get Set of Sobject Ids from List of sobject where the specified Field is populated
   * @param lstSObjects List of Sobject List to be validated
   * @param fieldName Field API Name whose value needs to be validated in SObject record
   * @return Set<Id> Set of Sobject records which have passed the Field validation
   **/
  public static Set<Id> getKeyValues(
    List<SObject> lstSObjects,
    Schema.sObjectField fieldName
  ) {
    Set<Id> valueSet = new Set<Id>();

    for (SObject thisObject : lstSObjects) {
      if (thisObject.get(fieldName) != null) {
        valueSet.add((Id) thisObject.get(fieldName));
      }
    }
    return valueSet;
  }

  public static Map<Id, Id> getMapValues(
    List<SObject> lstSObjects,
    Schema.sObjectField keyFieldName,
    Schema.sObjectField valueFieldName
  ) {
    Map<Id, Id> valueMap = new Map<Id, Id>();

    for (SObject thisObject : lstSObjects) {
      if (thisObject.get(keyFieldName) != null) {
        valueMap.put(
          (Id) thisObject.get(keyFieldName),
          (Id) thisObject.get(valueFieldName)
        );
      }
    }
    return valueMap;
  }

  public static Map<String, String> getMapStringValues(
    List<SObject> lstSObjects,
    Schema.sObjectField keyFieldName,
    Schema.sObjectField valueFieldName
  ) {
    Map<String, String> valueMap = new Map<String, String>();

    for (SObject thisObject : lstSObjects) {
      if (
        thisObject.get(keyFieldName) != null &&
        thisObject.get(valueFieldName) != null
      ) {
        valueMap.put(
          (String) thisObject.get(keyFieldName),
          (String) thisObject.get(valueFieldName)
        );
      }
    }
    return valueMap;
  }
}