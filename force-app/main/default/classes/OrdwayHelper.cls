/**
 * @File Name          : OrdwayHelper.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 01-05-2021
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    9/28/2019   Ordway Labs     Initial Version
 **/
public with sharing class OrdwayHelper {
  public static final String ORDWAY = 'ordway';

  //Custom Settings
  public static final string ORDWAY_APPLICATION_SETTING = 'Ordway_Authentication_Details';

  public enum ACTION {
    BULKPROCESS,
    WEBHOOK,
    RESPONSE
  }
  public enum SOURCE {
    SALESFORCE,
    ORDWAY
  }

  public static final string ORDWAY_PRODUCTION = 'OrdwayProduction';
  public static final string ORDWAY_SANDBOX = 'OrdwaySandbox';
  public static final string ORDWAY_STAGING = 'OrdwayStaging';
  public static final string ORDWAY_DEVELOPER = 'OrdwayDeveloper';

  public static final string ORDWAY_OPPORTUNITY = 'Opportunity';
  public static final string ORDWAY_ACCOUNT = 'Account';

  public static final string ORDWAY_CONTACT_URL = 'https://www.ordwaylabs.com/contact/';

  public static final string OPPORTUNITY_SETTING_NAME = 'Master';
  public static final string NOT_LINKED_TO_ORDWAY = 'Not Linked To Ordway';
  public static final string DRAFT = 'Draft';
  public static final string ACTIVE = 'Active';
  public static final string CANCELLED = 'Cancelled';
  public static final string ISTRUE = 'YES';
  public static final string ISFALSE = 'NO';

  public static final string SETTING_TRIGGER = 'On Create Change';
  public static final string DEFAULT_OPPORTUNITY_STAGE = 'Closed Won';
  public static final string ORDWAY_SIX_MONTHS_CONTRACT_TERM = '6';
  public static final string OPPORTUNITY_ORDWAY_NEW_BUSINESS_TYPE = 'New Business';

  public static final String VERSION_TYPE_NEW = 'New';
  public static final String VERSION_TYPE_CHANGE = 'Change';
  public static final String VERSION_TYPE_RENEWAL = 'Renewal';
  public static final String VERSION_TYPE_RENEW = 'Renew';

  //Interim Type - Not a valid version type (Change in draft version Update will be used)
  //After Successful Sync & Before Activation
  public static final String VERSION_TYPE_UPDATE = 'Update';

  public static final String SF_FLAT_FEE = 'flat_fee';
  public static final String SF_PER_UNIT = 'per_unit';

  public static final String OW_PRICING_TYPE_FLAT_FEE = 'Flat fee';
  public static final String OW_PRICING_TYPE_PER_UNIT = 'Per unit';

  public static final String VOLUME = 'Volume';
  public static final String PRICING_MODEL_PER_UNIT = 'Per Unit';
  public static final String TIERED = 'Tiered';

  public static final String SYNC_STATE_IN_SYNC = 'In Sync';
  public static final String SYNC_STATE_SYNC_IN_PROGRESS = 'Sync In Progress';
  public static final String SYNC_STATE_DO_NOT_SEND = 'Do Not Sync';
  public static final String SYNC_STATE_NOT_IN_SYNC = 'Not In Sync';
  public static final String SYNC_STATE_SYNC_FAILED = 'Sync Failed';
  public static final String SYNC_STATE_NOT_SYNCED = 'Not Synced';

  public static final String ORDWAY_OPPORTUNITY_TYPE_RENEWAL = 'Renewal';
  public static final String ORDWAY_OPPORTUNITY_TYPE_CHANGE = 'Change';
  public static final String ORDWAY_OPPORTUNITY_TYPE_UPSELL = 'Upsell/Cross-sell';

  //Field Set API Name
  public static final String ORDWAY_PLAN_PICKER_DYNAMIC_FIELDSET_APINAME = 'OrdwayLabs__PlanPicker';
  public static final String ORDWAY_PLAN_PICKER_READ_ONLY_FIELDSET_APINAME = 'OrdwayLabs__PlanPickerReadOnlyFields';

  //Field API Name
  public static final String ORDWAY_OPPORTUNITY_PRICEBOOK2ID = 'Pricebook2Id';

  //Permission Set Name
  public static final String ORDWAY_CPQ_ADMIN = 'Ordway_CPQ_Admin';
  public static final String ORDWAY_CPQ_USER = 'Ordway_CPQ_User';
  public static final String ORDWAY_CPQ_MULTI_ENTITY = 'Ordway_CPQ_Multi_Entity';

  //Dummy Token
  public static final String AUTOMATED_PROCESS = 'Automated Process';
  public static final String OPPORTUNITY_LAYOUT = 'Opportunity-OrdwayLabs__Ordway Opportunity Layout';

  //Namespace
  public static final String ORDWAY_NAMESPACE = 'OrdwayLabs__';

  //Ordway Quote
  public static final String ORDWAY_QUOTECOMPLETED = 'Completed';
  public static final String ORDWAY_QUOTE_ACCEPTED = 'Accepted';
  public static final String ORDWAY_QUOTE_CANCELLED = 'Cancelled';
  public static final String ORDWAY_QUOTE_CONVERTED = 'Converted';
  public static final String ORDWAY_QUOTE_NOTLINKED = 'Not Linked To Ordway';
  public static final String ORDWAY_QUOTE_OUTFORACCEPTANCE = 'Out_for_acceptance';
  public static final String ORDWAY_QUOTE_DRAFT = 'Draft';

  //Update From Ordway Sobject Map Keys
  public static final String SOBJECT_LIST_TO_INSERT = 'SOBJECT_LIST_TO_INSERT';
  public static final String SOBJECT_LIST_TO_UPDATE = 'SOBJECT_LIST_TO_UPDATE';
  public static final String SOBJECT_LIST_TO_DELETE = 'SOBJECT_LIST_TO_DELETE';

  //Ordway Entity field API Names
  public static final String SOBJECT_FIELD_ENTITYID = 'OrdwayLabs__EntityID__c';
  public static final String SOBJECT_FIELD_ENTITYNAME = 'OrdwayLabs__EntityName__c';
  public static final String SOBJECT_FIELD_ACCOUNT = 'OrdwayLabs__Account__c';
  public static final String SOBJECT_CUSTOM_ACCOUNT = 'Account__c';
  public static final String SOBJECT_FIELD_STDACCOUNT = 'AccountId';
  public static final String SOBJECT_FIELD_ID = 'Id';
  public static final String SOBJECT_FIELD_NAME = 'Name';

  //Currency Iso code Field API Name
  public static final String CURRENCY_ISO_CODE = 'CurrencyIsoCode';

  //Inject ID API
  public static final String SALESFORCE_ID              = 'salesforce_id';
  public static final String CHARGE_EXTERNAL_ID         = 'charge_external_id'; //subscription charge id
  public static final String MODEL_SUBSCRIPTIONS        = 'subscriptions';
  public static final String MODEL_SUBSCRIPTION_CHARGES = 'subscription_charges';
  public static final String MODEL_CONTACTS             = 'contacts';
  public static final String MODEL_USAGE                = 'usage';
  public static final String MODEL_ORDWAY_PRODUCT       = 'product';
  public static final String MODEL_ORDWAY_PLAN          = 'plan';
  public static final String MODEL_CHARGES              = 'charges';
  public static final String PLANCHARGE_EXTERNAL_ID     = 'salesforce_charge_id';

  /**
   * @description Method to get Response Status Message
   * @param thisCalloutResponse Http Response from which Status Message to be retrieved
   * @return String Response Status Message
   **/
  public static String getResponseStatusMessage(
    HttpResponse thisCalloutResponse
  ) {
    String thisMessage;
    if (
      thisCalloutResponse.getStatusCode() >= 200 &&
      thisCalloutResponse.getStatusCode() <= 299
    ) {
      thisMessage = 'SUCCESS';
    } else if (thisCalloutResponse.getStatusCode() == 500) {
      thisMessage =
        thisCalloutResponse.getStatusCode() + ': Ordway Internal Server Error';
    } else if (thisCalloutResponse.getStatusCode() == 503) {
      thisMessage =
        thisCalloutResponse.getStatusCode() + ': Service Unavailable';
    } else if (thisCalloutResponse.getStatusCode() == 522) {
      thisMessage =
        thisCalloutResponse.getStatusCode() +
        ': A network connection to the origin server could not established, please try again in few minutes';
    } else if (thisCalloutResponse.getStatusCode() == 401) {
      thisMessage =
        thisCalloutResponse.getStatusCode() +
        ': There is a connection error. Please check that you are connected to both Salesforce and Ordway.';
    } else if (thisCalloutResponse.getStatusCode() == 302) {
      thisMessage =
        thisCalloutResponse.getStatusCode() +
        ': Please check the mappings in Ordway or contact the administrator';
    } else {
      Map<String, Object> errorMap = getDeserializedUntyped(
        thisCalloutResponse
      );
      
      if(errorMap.containsKey('status') && errorMap.containsKey('details')) {
        thisMessage = errorMap.get('status')+' : '+errorMap.get('details');
      }
      else if(errorMap.containsKey('status') && errorMap.containsKey('error')) {
        thisMessage = errorMap.get('status')+' : '+errorMap.get('error');
      }else {
        thisMessage = thisCalloutResponse.getBody();
      }
    }
    return thisMessage;
  }

  /**
   * @description Method to get Response Status Message
   * @param thisCalloutResponse Http Response from which Status Message to be retrieved
   * @return String Response Status Message
   **/
  public static Map<String, Object> getDeserializedUntyped(
    HttpResponse thisCalloutResponse
  ) {
    Map<String, Object> responseMap = new Map<String, Object>();
    responseMap = (Map<String, Object>) JSON.deserializeUntyped(
      thisCalloutResponse.getBody()
    );
    return responseMap;
  }

  /**
   * @description Method to populate values from response
   * @param responseMap response Map
   * @param sObjectDescription DescribeSObjectResult
   * @param thisRecord Sobject record which needs to be processed
   * @return sObject processed Sobject records
   **/
  public static sObject getSObjectRecord(
    Map<String, Object> responseMap,
    Schema.DescribeSObjectResult sObjectDescription,
    sObject thisRecord
  ) {
    Map<String, String> fieldDataTypeMap = SObjectHelper.getSobjectFieldType(
      sObjectDescription.getName()
    );

    for (String thisString : responseMap.keySet()) {
      if (
        responseMap.get(thisString) != null &&
        (SObjectHelper.isFieldCreatable(sObjectDescription, thisString) ||
        SObjectHelper.isFieldUpdatable(sObjectDescription, thisString))
      ) {
        SObjectFieldMappingHelper.populateSObjectFieldValue(
          fieldDataTypeMap,
          thisRecord,
          thisString.toLowerCase(),
          String.valueOf(responseMap.get(thisString))
        );
        continue;
      }

      if (thisString == 'Id') {
        //Do not check Is Valid Id here
        thisRecord.put('Id', String.valueOf(responseMap.get(thisString)));
      }
    }
    return thisRecord;
  }

  /**
   * @description Method to validate whether the Id is valid or not
   * @param strId Record ID to be validated
   * @return Boolean true or false
   **/
  public static Boolean isValidId(String strId) {
    if(strId == null) { return false;}
    
    String recId = String.escapeSingleQuotes(strId);
    Boolean isValid = (recId.length() == 15 || recId.length() == 18) && Pattern.matches('^[a-zA-Z0-9]*$', recId) ? true : false;
    return isValid;
  }
}