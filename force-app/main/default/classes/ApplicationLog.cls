/**
 * @File Name          : ApplicationLog.cls
 * @Description        : Apex class for controlling Application Log
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 03-16-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    11/24/2019   Ordway Labs     Initial Version
 **/

public with sharing class ApplicationLog {
  public enum LoggingLevel {
    ERROR,
    WARN,
    INFO,
    DEBUG,
    FINE
  }

  private static ApplicationLog instance;

  private ApplicationLog__c errorLog;
  private List<LoggingContext> loggingContextList;
  private List<LogLine> logLinesList;

  private Integer contextDepth;
  private Map<String, Integer> contextCallNumber;

  /**
   * @description Private contructor so this class should not be created on its own, use provided static functions instead.
   * @param className Apex class name
   * @param functionName Function Name
   **/
  private ApplicationLog(String className, String functionName) {
    errorLog = new ApplicationLog__c(
      StartedAt__c = Datetime.now(),
      InitialClass__c = className,
      InitialFunction__c = functionName,
      UserId__c = UserInfo.getUserId()
    );
    loggingContextList = new List<LoggingContext>();
    logLinesList = new List<LogLine>();
    contextDepth = 0;
    contextCallNumber = new Map<String, Integer>();
  }

  /**
   * @description Method Invoked from Apex class function to start Application logging context
   * @param className Apex class name
   * @param functionName Function Name
   **/
  public static ApplicationLog StartLoggingContext(
    String className,
    String functionName
  ) {
    if (ApplicationLog.instance == null) {
      ApplicationLog.instance = new ApplicationLog(className, functionName);
    }

    LoggingContext context = ApplicationLog.instance.addContext(
      className,
      functionName
    );
    return ApplicationLog.instance;
  }

  /**
   * @description Method Invoked from DeleteApplicationLog_Scheduler to delete the records older than 90 days
   **/
  public static void deleteLogs(){
    List<ApplicationLog__c> applicationLogToDelete = new List<ApplicationLog__c>([SELECT Id, CreatedDate
                                                                  FROM ApplicationLog__c
                                                                  WHERE CreatedDate <=: System.today().addDays(-90)
                                                                  AND HasError__c = false
                                                                  LIMIT 10000]);
    delete applicationLogToDelete;                                       
    if(!applicationLogToDelete.isEmpty()){
      Database.emptyRecycleBin(applicationLogToDelete);
    }
  }

  /**
   * @description Method Invoked from Apex class function to Finish Application logging Context
   **/
  public static ApplicationLog FinishLoggingContext() {
    if (ApplicationLog.instance == null)
      return null;

    LoggingContext context = ApplicationLog.instance.popContext();

    if (ApplicationLog.instance.isStartingContext()) {
      ApplicationLog.instance.commitLog();
    }

    return ApplicationLog.instance;
  }

  /**
   * @description Method Commit the Application logging instance
   **/
  public static ApplicationLog CommitNow() {
    if (ApplicationLog.instance == null)
      return null;

    ApplicationLog.instance.commitLog();
    return ApplicationLog.instance;
  }

  /**
   * @description Method to process Exception and add Error line in Currrent Application Log Instance
   * @param message Exception Message
   * @param Exception Exception thrown
   * @param autoEndContext Boolean variable to detrimine auto ending context after error
   **/
  public static ApplicationLog Error(
    String message,
    Exception e,
    Boolean autoEndContext
  ) {
    if (ApplicationLog.instance == null)
      return null;

    ApplicationLog.instance.addErrorLine(message, e);
    if (autoEndContext) {
      ApplicationLog.instance.addLogLine(
        new InfoLogLine('>> Auto ending context after error.')
      );
      //If Errored, end the logging context
      ApplicationLog.CommitNow();
    }
    return ApplicationLog.instance;
  }

  /**
   * @description Method to process Exception and add Error line in Currrent Application Log Instance
   * @param message Exception Message
   * @param Exception Exception thrown
   **/
  public static ApplicationLog Error(String message, Exception e) {
    return ApplicationLog.Error(message, e, true);
  }

  /**
   * @description Method to process Exception and add Error line in Currrent Application Log Instance
   * @param Exception Exception thrown
   **/
  public static ApplicationLog Error(Exception e) {
    return ApplicationLog.Error(e.getMessage(), e, true);
  }

  /**
   * @description Method to process Exception and add Error line in Currrent Application Log Instance
   * @param message Error Message
   **/
  public static ApplicationLog Error(String message) {
    return ApplicationLog.Error(message, null, false);
  }

  /**
   * @description Method to add Warning Log Line in Currrent Application Log Instance
   * @param message Warning Message
   **/
  public static ApplicationLog Warn(String message) {
    if (ApplicationLog.instance == null)
      return null;
    return ApplicationLog.instance.addLogLine(new WarnLogLine(message));
  }

  /**
   * @description Method to add Information Log Line in Currrent Application Log Instance
   * @param message Information Message
   **/
  public static ApplicationLog Info(String message) {
    if (ApplicationLog.instance == null)
      return null;
    return ApplicationLog.instance.addLogLine(new InfoLogLine(message));
  }

  /**
   * @description Method to add Debug Log Line in Currrent Application Log Instance
   * @param message Debug Message
   **/
  public static ApplicationLog Debug(String message) {
    if (ApplicationLog.instance == null)
      return null;
    return ApplicationLog.instance.addLogLine(new DebugLogLine(message));
  }

  /**
   * @description Method to add Fine Log Line in Currrent Application Log Instance
   * @param message Message
   **/
  public static ApplicationLog Fine(String message) {
    if (ApplicationLog.instance == null)
      return null;
    return ApplicationLog.instance.addLogLine(new FineLogLine(message));
  }

  /**
   * @description Method to get number of Log Line added in Currrent Application Log Instance
   * @param message Message
   **/
  public Integer size() {
    return logLinesList.size();
  }

  /**
   * @description Method to add Application Logging Context
   * @param className Apex class name
   * @param functionName Function Name
   **/
  public LoggingContext addContext(String className, String functionName) {
    ++contextDepth;
    LoggingContext newContext = new LoggingContext(className, functionName);
    loggingContextList.add(newContext);
    String contextName = newContext.name();
    Integer callNum = contextCallNumber.get(contextName);
    if (callNum == null) {
      callNum = 0;
    }
    contextCallNumber.put(contextName, ++callNum);
    newContext.callNum = callNum;

    addLogLine(new ContextLogLine(newContext, true));
    return newContext;
  }

  /**
   * @description Method to Remove Application Logging Context
   **/
  public LoggingContext popContext() {
    --contextDepth;
    LoggingContext logContext = loggingContextList[contextDepth];
    String contextName = logContext.name();
    contextCallNumber.put(contextName, logContext.callNum - 1);

    addLogLine(new ContextLogLine(logContext, false));
    return logContext;
  }

  /**
   * @description Method to Add Record Id in Application Log due to which the exception was thrown
   * @param recordId Record Id to be added to Application Log
   **/
  public static void addRecordId(Id recordId) {
    if (ApplicationLog.instance != null) {
      ApplicationLog.instance.addErrorLogRecordId(recordId);
    }
  }

  /**
   * @description Method to Record Id in Error Application Log due to which the exception was thrown
   * @param recordId Record Id to be added to Error Application Log
   **/
  private void addErrorLogRecordId(Id recordId) {
    if (
      errorLog.RecordId__c == null &&
      SObjectHelper.isFieldCreatable(
        SObjectType.ApplicationLog__c,
        Schema.ApplicationLog__c.RecordId__c
      )
    ) {
      errorLog.RecordId__c = recordId;
    }
  }

  public static void flagAsError() {
    if (ApplicationLog.instance != null) {
      ApplicationLog.instance.flagError();
    }
  }


  /**
   * @description Method to Add Record Id in Application Log due to which the exception was thrown
   * @param recordId Record Id to be added to Application Log
   **/
  public void flagError() {
    errorLog.HasError__c = true;
  }

  /**
   * @description Method to add Error line in Application Log
   * @param message Error Message
   * @param Exception Exception to be processed and logged in Application Log
   **/
  public ApplicationLog addErrorLine(String message, Exception e) {
    if (!errorLog.HasError__c) {
      if (
        SObjectHelper.isFieldCreatable(
          SObjectType.ApplicationLog__c,
          Schema.ApplicationLog__c.HasError__c
        )
      ) {
        errorLog.HasError__c = true;
      }
      if (
        SObjectHelper.isFieldCreatable(
          SObjectType.ApplicationLog__c,
          Schema.ApplicationLog__c.FirstErrorMessage__c
        )
      ) {
        errorLog.FirstErrorMessage__c = message.abbreviate(250);
      }
      if (
        SObjectHelper.isFieldCreatable(
          SObjectType.ApplicationLog__c,
          Schema.ApplicationLog__c.FirstErrorLogLineNumber__c
        )
      ) {
        errorLog.FirstErrorLogLineNumber__c = this.size() + 1;
      }
      String exceptionJson = 'null';
      if (e != null) {
        exceptionJson =
          '{"message":' +
          (e.getMessage() == null ? null : '"' + e.getMessage() + '"') +
          ',' +
          '"cause":' +
          (e.getCause() == null ? null : '"' + e.getCause() + '"') +
          ',' +
          '"lineNumber":' +
          e.getLineNumber() +
          ',' +
          '"stackTrace":' +
          (e.getStackTraceString() == null
            ? null
            : '"' + e.getStackTraceString() + '"') +
          ',' +
          '"type":' +
          (e.getTypeName() == null ? null : '"' + e.getTypeName() + '"') +
          '}';
      }

      if (
        SObjectHelper.isFieldCreatable(
          SObjectType.ApplicationLog__c,
          Schema.ApplicationLog__c.FirstErrorExceptionJson__c
        )
      ) {
        errorLog.FirstErrorExceptionJson__c = exceptionJson;
      }
    }
    return this.addLogLine(new ErrorLogLine(message, e));
  }

  /**
   * @description Method to add Log line in Application Log
   **/
  public ApplicationLog addLogLine(LogLine newLine) {
    logLinesList.add(newLine);
    return this;
  }

  /**
   * @description Method to check whether the current Log Context is Starting Context
   **/
  public Boolean isStartingContext() {
    return contextDepth == 0;
  }

  /**
   * @description Method to Commit the Current Application Log
   **/
  public ApplicationLog__c commitLog() {
    if (
      SObjectHelper.isFieldCreatable(
        SObjectType.ApplicationLog__c,
        Schema.ApplicationLog__c.FinishedAt__c
      )
    ) {
      errorLog.FinishedAt__c = Datetime.now();
    }

    String fullBody = this.buildBody();
    if (
      SObjectHelper.isFieldCreatable(
        SObjectType.ApplicationLog__c,
        Schema.ApplicationLog__c.Body__c
      )
    ) {
      errorLog.Body__c = fullBody.abbreviate(131050);
    }

    if(errorLog.HasError__c) {
      if (Schema.sObjectType.ApplicationLog__c.isCreateable()) {
        insert errorLog;
      }
    }
    else if (LoadLeanSettingController.allLogs()){

      if(LoadLeanSettingController.isSyncLogging()
      || Limits.getQueueableJobs() == 1
      || System.isBatch()
      || System.isQueueable()
      || System.isFuture()) {

        if (Schema.sObjectType.ApplicationLog__c.isCreateable()) {
          insert errorLog;
        }
      }
      else if (!LoadLeanSettingController.isSyncLogging()){
        System.enqueueJob(new AsyncLogInsert(errorLog));
      }
    }

    this.flush();
    return errorLog;
  }

  /**
   * @description Asynchronously insert the Application Log
   **/
  public class AsyncLogInsert implements Queueable {
    ApplicationLog__C theLog;
    AsyncLogInsert(ApplicationLog__C theLog) {
      this.theLog = theLog;
    }
    public void execute(QueueableContext context) {
      if (Schema.sObjectType.ApplicationLog__c.isCreateable()) {
        insert theLog;
      }
    }
  }

  /**
   * @description Method to construct Full Body of Application Log
   **/
  public String buildBody() {
    String body = '';
    Integer lineNum = 1;
    for (LogLine nextlogLine : logLinesList) {
      body += '[' + (lineNum++) + ']  ' + nextlogLine.toString();
    }
    return body;
  }

  /**
   * @description Method to clear the current Application Log Instance
   **/
  public void flush() {
    ApplicationLog.instance = null;
  }

  /*
   * @description  Used for saving a level of logging context
   */
  public class LoggingContext {
    String contextClass;
    String contextFunction;
    Integer callNum;
    public LoggingContext(String className, String functionName) {
      contextClass = className;
      contextFunction = functionName;
    }
    public String name() {
      return contextClass + '.' + contextFunction;
    }
  }

  /*
   * @description  Details for a line of log
   */
  public abstract class LogLine {
    protected String level;
    protected Datetime logDatetime;
    protected String message;

    protected LogLine() {
      logDatetime = Datetime.now();
    }
    public LogLine(ApplicationLog.LoggingLevel logLevel, String message) {
      logDatetime = Datetime.now();
      this.level = logLevel.name();
      this.message = message;
    }
    public override String toString() {
      return logDatetime +
        ' ' +
        logDatetime.millisecond() +
        ' => ' +
        level +
        ' => ' +
        message +
        '\n';
    }
  }

  /*
   * @description  Details for a Error line of log
   */
  public class ErrorLogLine extends LogLine {
    public ErrorLogLine(String message) {
      this.level = ApplicationLog.LoggingLevel.ERROR.name();
      this.message = message + ' <<Exception>> NULL';
    }
    public ErrorLogLine(String message, Exception e) {
      this.level = ApplicationLog.LoggingLevel.ERROR.name();
      this.message = message + ' <<Exception>>';

      if (e != null) {
        message +=
          '\nmessage: ' +
          e.getMessage() +
          '\ncause: ' +
          e.getCause() +
          '\nstackTrace: ' +
          e.getStackTraceString() +
          '\nlineNumber: ' +
          e.getLineNumber() +
          '\ntype: ' +
          e.getTypeName();
      } else {
        message += null;
      }
    }
  }

  /*
   * @description  Details for a Warning line of log
   */
  public class WarnLogLine extends LogLine {
    public WarnLogLine(String message) {
      this.level = ApplicationLog.LoggingLevel.WARN.name();
      this.message = message;
    }
  }

  /*
   * @description  Details for a Information line of log
   */
  public class InfoLogLine extends LogLine {
    public InfoLogLine(String message) {
      this.level = ApplicationLog.LoggingLevel.INFO.name();
      this.message = message;
    }
  }

  /*
   * @description  Details for a Debug line of log
   */
  public class DebugLogLine extends LogLine {
    public DebugLogLine(String message) {
      this.level = ApplicationLog.LoggingLevel.DEBUG.name();
      this.message = message;
    }
  }

  /*
   * @description  Details for a Fine line of log
   */
  public class FineLogLine extends LogLine {
    public FineLogLine(String message) {
      this.level = ApplicationLog.LoggingLevel.FINE.name();
      this.message = message;
    }
  }

  /*
   * @description  Details for a Context Log line of Appliation log
   */
  public class ContextLogLine extends LogLine {
    public ContextLogLine(LoggingContext logContext, Boolean isStart) {
      this.level = 'CONTEXT ' + (isStart ? 'START' : 'END') + '';
      this.message =
        logContext.contextClass +
        '.' +
        logContext.contextFunction +
        ' <<Call Number>>' +
        logContext.callNum;
    }
  }
}