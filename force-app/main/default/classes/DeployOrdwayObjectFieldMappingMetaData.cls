/**
 * @File Name          : DeployOrdwayObjectFieldMappingMetaData.cls
 * @Description        : Ordway Object Field Mapping MetaData deployment helper
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    10/11/2019   Ordway Labs     Initial Version
 **/
public with sharing class DeployOrdwayObjectFieldMappingMetaData implements Metadata.DeployCallback {
  /**
   * @description Method invoked after Deployment process is completed
   * @param result DeployResult
   * @param context DeployCallbackContext
   **/
  public void handleResult(
    Metadata.DeployResult result,
    Metadata.DeployCallbackContext context
  ) {
    if (result.status == Metadata.DeployStatus.Succeeded) {
      //Do Nothing
    } else {
      //Do Nothing
    }
  }

  /**
   * @description Method to Deploy Ordway Field Mapping Metadata record
   * @param thisFieldMappingMetadata Ordway Field Mapping Metadata record to be deployed
   * @return Id Deployment Metadata Operations Job Id
   **/
  public static Id createUpdateMetadata(
    Metadata.CustomMetadata thisFieldMappingMetadata
  ) {
    Id jobId = null;
    Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
    mdContainer.addMetadata(thisFieldMappingMetadata);
    DeployOrdwayObjectFieldMappingMetaData callback = new DeployOrdwayObjectFieldMappingMetaData();
    if (!Test.isRunningTest()) {
      jobId = Metadata.Operations.enqueueDeployment(mdContainer, callback);
    }
    return jobId;
  }
}