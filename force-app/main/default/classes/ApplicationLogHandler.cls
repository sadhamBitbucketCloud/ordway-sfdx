/**
 * @File Name          : ApplicationLogHandler.cls
 * @Description        : Application log Trigger Handler
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    11/24/2019   Ordway Labs     Initial Version
 **/
public with sharing class ApplicationLogHandler extends TriggerHandler.TriggerHandlerBase {
  public override void beforeInsert(List<SObject> lstNewSObjs) {
    for (
      ApplicationLog__c thisApplicationLog : (List<ApplicationLog__c>) lstNewSObjs
    ) {
      if (thisApplicationLog.RecordId__c != null) {
        Id thisRecordId = thisApplicationLog.RecordId__c;
        if (
          SObjectHelper.isFieldCreatable(
            SObjectType.ApplicationLog__c,
            Schema.ApplicationLog__c.SObjectType__c
          )
        ) {
          thisApplicationLog.SObjectType__c = String.valueOf(
            thisRecordId.getSObjectType()
          );
        }
      }
    }
  }
}