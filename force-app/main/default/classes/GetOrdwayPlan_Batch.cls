/**
 * @File Name          : GetOrdwayPlan_Batch.cls
 * @Description        : Get Ordway Plan Batch
 * @Author             : 
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    3/7/2020   Ordway Labs     Initial Version
 **/
global with sharing class GetOrdwayPlan_Batch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
  Integer pageNumber = 0;
  Boolean hasMoreRecords = false;
  String xEntityId;

  /**
   * @description Batch class to get Ordway Plan from Ordway
   * @param pNumber Page Number of response
   * @param xEntityId Entity Id
   **/
  global GetOrdwayPlan_Batch(Integer pNumber, String xEntityId) {
    this.pageNumber = pNumber;
    this.xEntityId = xEntityId;
  }

  global GetOrdwayPlan_Batch(Integer pNumber) {
    this.pageNumber = pNumber;
    this.xEntityId = xEntityId;
  }

  global List<sObject> start(Database.BatchableContext BC) {
    Plan__c thisPlan = new Plan__c();
    return new List<Plan__c>{ thisPlan };
  }

  global void execute(Database.BatchableContext BC, List<sObject> sObjectList) {
    hasMoreRecords = OrdwayPlanController.getOrdwayPlan(pageNumber, xEntityId);
    pageNumber += 1; //Increment the Page Number
  }

  global void finish(Database.BatchableContext BC) {
    if (Test.isRunningTest()) {
      hasMoreRecords = false;
    }
    if (hasMoreRecords) {
      Database.executeBatch(new GetOrdwayPlan_Batch(pageNumber, xEntityId));
    }
  }
}