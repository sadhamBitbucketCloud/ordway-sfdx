/**
 * @File Name          : TierController_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-25-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0     3/18/2021        Ordway Labs             Initial Version
 **/
@isTest
private class TierController_Test {
 @isTest
 private static void getInitialWrapper_ChargeTier() {

  Plan__c thisPlan = TestObjectFactory.createOrdwayPlan();
  insert thisPlan;

  Charge__c thisCharge       = TestObjectFactory.createCharge();
  thisCharge.Name            = 'Test Charge';
  thisCharge.PricingModel__c = 'Per Unit';
  thisCharge.ChargeType__c   = 'Recurring';
  thisCharge.OrdwayPlan__c   =  thisPlan.Id;
  insert thisCharge;

  ChargeTier__c thisChargeTier   = TestObjectFactory.createChargeTier();
  thisChargeTier.Charge__c       = thisCharge.Id;
  thisChargeTier.StartingUnit__c = 123;
  thisChargeTier.EndingUnit__c   = 456;
  thisChargeTier.ListPrice__c    = 1234;
  thisChargeTier.PricingType__c  = 'Per unit';
  insert thisChargeTier; 

  Test.startTest();
  TierController.InitialWrapper thisInitialWrapper = new TierController.InitialWrapper();
  thisInitialWrapper = TierController.getInitialWrapper(thisCharge.Id, 'OrdwayLabs__Charge__c');
  List<Sobject> tierList = thisInitialWrapper.tierList;
  Sobject parentRecord = thisInitialWrapper.thissObject;
  Test.stopTest();
  System.assertEquals(1, tierList.Size(), 'Should return 1 Charge Tier as we are inserting 1 tier record');
  System.assertEquals(123, tierList[0].get('OrdwayLabs__StartingUnit__c'), 'Should return 123');
  System.assertEquals('Test Charge', parentRecord.get('Name'), 'Should return Test Charge');
}

@isTest
private static void getInitialWrapper_ChargeTierNegative() {

  Plan__c thisPlan = TestObjectFactory.createOrdwayPlan() ;
  insert thisPlan;

  Charge__c thisCharge       = TestObjectFactory.createCharge();
  thisCharge.Name            = 'Test';
  thisCharge.PricingModel__c = 'Per Unit';
  thisCharge.ChargeType__c   = 'Recurring';
  thisCharge.OrdwayPlan__c   =  thisPlan.Id;
  insert thisCharge;

  ChargeTier__c thisChargeTier   = TestObjectFactory.createChargeTier();
  thisChargeTier.Charge__c       = thisCharge.Id;
  thisChargeTier.StartingUnit__c = 123;
  thisChargeTier.EndingUnit__c   = 456;
  thisChargeTier.ListPrice__c    = 1234;
  thisChargeTier.PricingType__c  = 'Per unit';
  insert thisChargeTier; 

  Test.startTest();
  try{
  TierController.InitialWrapper thisInitialWrapper = new TierController.InitialWrapper();
  thisInitialWrapper = TierController.getInitialWrapper(thisCharge.Id, null);
  List<SObject> tierList = thisInitialWrapper.tierList;
  Sobject parentRecord = thisInitialWrapper.thissObject;
  } catch (Exception ex) {
    system.assertNotEquals(null, ex.getMessage());
     }
  Test.stopTest();
}

 @isTest
 private static void getInitialWrapper_OpportunityLineItemTier() {

  Contact thisOrdwayContact = TestObjectFactory.createContact();
  insert thisOrdwayContact;
   
  Opportunity thisOrdwayOpportunity       = TestObjectFactory.createOpportunity();
  thisOrdwayOpportunity.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayOpportunity;
        
  OpportunityLineItem thisOrdwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem();
  thisOrdwayOpportunityLineItem.OpportunityId       = thisOrdwayOpportunity.Id;
  insert thisOrdwayOpportunityLineItem; 

  OpportunityLineItemTier__c thisOLITier = TestObjectFactory.createOpportunityLineItemTier();
  thisOLITier.StartingUnit__c            = 123;
  thisOLITier.OpportunityProduct__c      = thisOrdwayOpportunityLineItem.Id; 
  insert thisOLITier;

  Test.startTest();
  TierController.InitialWrapper thisInitialWrapper = new TierController.InitialWrapper();
  thisInitialWrapper = TierController.getInitialWrapper(thisOrdwayOpportunityLineItem.Id, 'OpportunityLineItem');
  List<Sobject> tierList = thisInitialWrapper.tierList;
  Sobject parentRecord = thisInitialWrapper.thissObject;
  Test.stopTest();
  System.assertEquals(1, tierList.size(), 'Should return 1 OpportunityLineItemTier as we are inserting 1 tier record');
  System.assertEquals(123, tierList[0].get('OrdwayLabs__StartingUnit__c'), 'Should return 123');
 }

 
 @isTest
 private static void getInitialWrapper_OpportunityLineItemTierNegative() {

  Contact thisOrdwayContact = TestObjectFactory.createContact();
  insert thisOrdwayContact;
   
  Opportunity thisOrdwayOpportunity       = TestObjectFactory.createOpportunity();
  thisOrdwayOpportunity.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayOpportunity;
        
  OpportunityLineItem thisOrdwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem();
  thisOrdwayOpportunityLineItem.OpportunityId       = thisOrdwayOpportunity.Id;
  insert thisOrdwayOpportunityLineItem; 

  OpportunityLineItemTier__c thisOLITier   = TestObjectFactory.createOpportunityLineItemTier();
  thisOLITier.OpportunityProduct__c        = thisOrdwayOpportunityLineItem.Id; 
  insert thisOLITier;

  Test.startTest();
  try{
  TierController.InitialWrapper thisInitialWrapper = new TierController.InitialWrapper();
  thisInitialWrapper = TierController.getInitialWrapper(thisOrdwayOpportunityLineItem.Id, null);
  List<SObject> tierList = thisInitialWrapper.tierList;
  Sobject parentRecord = thisInitialWrapper.thissObject;
  } catch (Exception ex) {
      system.assertNotEquals(null, ex.getMessage());
    }
  Test.stopTest();
 }

 @isTest
 private static void getInitialWrapper_QuoteLineItemTier() {
   
  Account thisAccount = TestObjectFactory.createAccount();
  insert thisAccount;

  Opportunity thisOpportunity = TestObjectFactory.createOpportunity();
  insert thisOpportunity;

  Quote__c thisQuote       = TestObjectFactory.createQuote();
  thisQuote.Account__c     = thisAccount.Id;
  thisQuote.Opportunity__c = thisOpportunity.Id;
  insert thisQuote;

  QuoteLineItem__c thisQLI = TestObjectFactory.createQuoteLineItem();
  thisQLI.Name             = 'Test QLI';
  thisQLI.Quote__c         = thisQuote.Id;
  insert thisQLI;
    
  QuoteLineItemTier__c thisQLITier = TestObjectFactory.createQuoteLineItemTier();
  thisQLITier.QuoteLineItem__c     = thisQLI.Id;
  thisQLITier.StartingUnit__c      = 123;
  insert thisQLITier;

  Test.startTest();
  TierController.InitialWrapper thisInitialWrapper = new TierController.InitialWrapper();
  thisInitialWrapper = TierController.getInitialWrapper(thisQLI.Id, 'OrdwayLabs__QuoteLineItem__c');
  List<SObject> tierList = thisInitialWrapper.tierList;
  SObject parentRecord = thisInitialWrapper.thissobject;
  Test.stopTest();
  System.assertEquals(1, tierList.size(), 'Should return 1 QuoteLineItemTier as we are inserting 1 tier record');
  System.assertEquals(123, tierList[0].get('OrdwayLabs__StartingUnit__c'), 'Should return 123');
  System.assertEquals('Test QLI', parentRecord.get('Name'), 'Should return Test QLI');
}

  @isTest
  private static void getInitialWrapper_QuoteLineItemTierNegative() {
     
  Account thisAccount = TestObjectFactory.createAccount();
  insert thisAccount;
  
  Opportunity thisOpportunity = TestObjectFactory.createOpportunity();
  insert thisOpportunity;
  
  Quote__c thisQuote       = TestObjectFactory.createQuote();
  thisQuote.Account__c     = thisAccount.Id;
  thisQuote.Opportunity__c = thisOpportunity.Id;
  insert thisQuote;
  
  QuoteLineItem__c thisQLI = TestObjectFactory.createQuoteLineItem();
  thisQLI.Quote__c         = thisQuote.Id;
  insert thisQLI;
      
  QuoteLineItemTier__c thisQLITier = TestObjectFactory.createQuoteLineItemTier();
  thisQLITier.QuoteLineItem__c     = thisQLI.Id;
  insert thisQLITier;
  
  Test.startTest();
  try{
  TierController.InitialWrapper thisInitialWrapper = new TierController.InitialWrapper();
  thisInitialWrapper = TierController.getInitialWrapper(thisQLI.Id, null);
  List<SObject> tierList = thisInitialWrapper.tierList;
  SObject parentRecord = thisInitialWrapper.thissobject;
  } catch (Exception ex) {
      system.assertNotEquals(null, ex.getMessage());
      }
  Test.stopTest();
  }
  

 @isTest
 private static void getInitialWrapper_ContractLineItemTier() {

  Contact thisOrdwayContact = TestObjectFactory.createContact();
  insert thisOrdwayContact;

  Opportunity thisOrdwayOpportunity       = TestObjectFactory.createOpportunity();
  thisOrdwayOpportunity.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayOpportunity;

  OpportunityLineItem thisOrdwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem();
  thisOrdwayOpportunityLineItem.OpportunityId       = thisOrdwayOpportunity.Id;
  insert thisOrdwayOpportunityLineItem;

  OrdwayContract__c thisOrdwayContract = TestObjectFactory.createOrdwayContract();
  thisOrdwayContract.BillingContact__c = thisOrdwayContact.Id; 
  insert thisOrdwayContract;

  ContractLineItem__c thisOrdwayContractLineItem = TestObjectFactory.createContractLineItem();
  thisOrdwayContractLineItem.Name                ='Test CLI';
  thisOrdwayContractLineItem.OrdwayContract__c   = thisOrdwayContract.Id;
  insert thisOrdwayContractLineItem;

  ContractLineItemTier__c thisCLITier = TestObjectFactory.createContractLineItemTier();
  thisCLITier.StartingUnit__c         = 123;
  thisCLITier.ContractLineItem__c     = thisOrdwayContractLineItem.Id;
  insert thisCLITier;

  Test.startTest();
  TierController.InitialWrapper thisInitialWrapper = new TierController.InitialWrapper();
  thisInitialWrapper = TierController.getInitialWrapper(thisOrdwayContractLineItem.Id, 'OrdwayLabs__ContractLineItem__c');
  List<SObject> tierList = thisInitialWrapper.tierList;
  SObject parentRecord = thisInitialWrapper.thissobject;
  Test.stopTest();
  System.assertEquals(1, tierList.size(), 'Should return 1 ContractLineItemTier as we are inserting 1 tier record');
  System.assertEquals(123, tierList[0].get('OrdwayLabs__StartingUnit__c'), 'Should return 123');
  System.assertEquals('Test CLI', parentRecord.get('Name'), 'Should return Test CLI');
 }

 @isTest
 private static void getInitialWrapper_ContractLineItemTierNegative() {

  Contact thisOrdwayContact = TestObjectFactory.createContact();
  insert thisOrdwayContact;

  Opportunity thisOrdwayOpportunity       = TestObjectFactory.createOpportunity();
  thisOrdwayOpportunity.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayOpportunity;

  OpportunityLineItem thisOrdwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem();
  thisOrdwayOpportunityLineItem.OpportunityId       = thisOrdwayOpportunity.Id;
  insert thisOrdwayOpportunityLineItem;

  OrdwayContract__c thisOrdwayContract = TestObjectFactory.createOrdwayContract();
  thisOrdwayContract.BillingContact__c = thisOrdwayContact.Id; 
  insert thisOrdwayContract;

  ContractLineItem__c thisOrdwayContractLineItem = TestObjectFactory.createContractLineItem();
  thisOrdwayContractLineItem.OrdwayContract__c   = thisOrdwayContract.Id;
  insert thisOrdwayContractLineItem;

  ContractLineItemTier__c thisCLITier = TestObjectFactory.createContractLineItemTier();
  thisCLITier.ContractLineItem__c     = thisOrdwayContractLineItem.Id;
  insert thisCLITier;

  Test.startTest();
  try{
  TierController.InitialWrapper thisInitialWrapper = new TierController.InitialWrapper();
  thisInitialWrapper = TierController.getInitialWrapper(thisOrdwayContractLineItem.Id, null);
  List<SObject> tierList = thisInitialWrapper.tierList;
  SObject parentRecord = thisInitialWrapper.thissobject;
  } catch (Exception ex) {
      system.assertNotEquals(null, ex.getMessage());
    }
  Test.stopTest();
}

 @isTest
 private static void upsertChargeTier(){

  Plan__c thisPlan = TestObjectFactory.createOrdwayPlan() ;
  insert thisPlan;

  Charge__c thisCharge       = TestObjectFactory.createCharge();
  thisCharge.Name            = 'Test';
  thisCharge.PricingModel__c = 'Per Unit';
  thisCharge.ChargeType__c   = 'Recurring';
  thisCharge.OrdwayPlan__c   =  thisPlan.Id;
  insert thisCharge;

  OrdwayChargeTierTriggerHandler.deleteTier = false;
  ChargeTier__c thisChargeTier  = TestObjectFactory.createChargeTier();
  thisChargeTier.Charge__c      = thisCharge.Id;
  thisChargeTier.StartingUnit__c = 6667;
  thisChargeTier.PricingType__c = 'Per unit';
  insert thisChargeTier; 

  ChargeTier__c thisChargeTierToDelete  = TestObjectFactory.createChargeTier();
  thisChargeTierToDelete.Charge__c      = thisCharge.Id;
  thisChargeTierToDelete.PricingType__c = 'Per unit';
  insert thisChargeTierToDelete; 

  String stringToUpsert = OrdwayService_Mock.chargeTier.replace('CHARGETIER', thisChargeTier.Id);
  stringToUpsert        = stringToUpsert.replace('CHARGEID', thisCharge.Id);
  String stringToDelete = OrdwayService_Mock.chargeTierToDelete.replace('CHARGETIERTODELETE', thisChargeTierToDelete.Id);
  stringToDelete        = stringToDelete.replace('CHARGEID', thisCharge.Id);

  Test.startTest();
  TierController.upsertTiers(stringToUpsert, stringToDelete, 'OrdwayLabs__Charge__c', thisCharge.Id);
  Test.stopTest();
  System.assertEquals(2, [SELECT Id FROM ChargeTier__c].Size());
  ChargeTier__c thisTierToAssert = [SELECT Id, EndingUnit__c 
                                                  FROM ChargeTier__c 
                                                  WHERE Id = :thisChargeTier.Id];
  System.assertEquals(1, thisTierToAssert.EndingUnit__c, 'Should return 1');
}

 @isTest
 private static void upsertChargeTierNegative(){

  Plan__c thisPlan = TestObjectFactory.createOrdwayPlan() ;
  insert thisPlan;

  Charge__c thisCharge       = TestObjectFactory.createCharge();
  thisCharge.Name            = 'Test';
  thisCharge.PricingModel__c = 'Per Unit';
  thisCharge.ChargeType__c   = 'Recurring';
  thisCharge.OrdwayPlan__c   =  thisPlan.Id;
  insert thisCharge;

  ChargeTier__c thisChargeTier  = TestObjectFactory.createChargeTier();
  thisChargeTier.Charge__c      = thisCharge.Id;
  thisChargeTier.PricingType__c = 'Per unit';
  insert thisChargeTier; 

  ChargeTier__c thisChargeTierToDelete  = TestObjectFactory.createChargeTier();
  thisChargeTierToDelete.Charge__c      = thisCharge.Id;
  thisChargeTierToDelete.PricingType__c = 'Per unit';
  insert thisChargeTierToDelete; 

  String stringToUpsert = OrdwayService_Mock.chargeTier.replace('CHARGETIER', thisChargeTier.Id);
  stringToUpsert        = stringToUpsert.replace('CHARGEID', 'Negative Testing');
  String stringToDelete = OrdwayService_Mock.chargeTierToDelete.replace('CHARGETIERTODELETE', thisChargeTierToDelete.Id);
  stringToDelete        = stringToDelete.replace('CHARGEID', 'Negative Testing');

  Test.startTest();
  try{
  TierController.upsertTiers(stringToUpsert, stringToDelete, 'OrdwayLabs__Charge__c', thisCharge.Id);
  } catch (Exception ex) {
    system.assertNotEquals(null, ex.getMessage());
  }
  Test.stopTest();
}

 @isTest
 private static void upsertContractLineItemTier_VolumePricing(){
  Contact thisOrdwayContact = TestObjectFactory.createContact();
  insert thisOrdwayContact;

  Opportunity thisOrdwayOpportunity       = TestObjectFactory.createOpportunity();
  thisOrdwayOpportunity.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayOpportunity;

  OpportunityLineItem thisOrdwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem();
  thisOrdwayOpportunityLineItem.OpportunityId       = thisOrdwayOpportunity.Id;
  insert thisOrdwayOpportunityLineItem;

  OrdwayContract__c thisOrdwayContract = TestObjectFactory.createOrdwayContract();
  thisOrdwayContract.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayContract;

  ContractLineItem__c thisOrdwayContractLineItem = TestObjectFactory.createContractLineItem();
  thisOrdwayContractLineItem.OrdwayContract__c   = thisOrdwayContract.Id;
  thisOrdwayContractLineItem.PricingModel__c     = 'Volume';
  thisOrdwayContractLineItem.Quantity__c         = 6;
  insert thisOrdwayContractLineItem;

  OrdwayContractLineItemTierTriggerHandler.deleteTier = false;

  ContractLineItemTier__c thisCLITier = TestObjectFactory.createContractLineItemTier();
  thisCLITier.ContractLineItem__c     = thisOrdwayContractLineItem.Id; 
  thisCLITier.ListPrice__c            = 123;
  thisCLITier.StartingUnit__c         = 345;
  insert thisCLITier;

  ContractLineItemTier__c thisCLITierToDelete = TestObjectFactory.createContractLineItemTier();
  thisCLITierToDelete.ContractLineItem__c     = thisOrdwayContractLineItem.Id;
  insert thisCLITierToDelete;

  String stringToUpsert = OrdwayService_Mock.contractLineItemTier.replace('CONTRACTLINEITEMTIER', thisCLITier.Id);
  stringToUpsert = stringToUpsert.replace('CLIID', thisOrdwayContractLineItem.Id);  
  String stringToDelete = OrdwayService_Mock.contractLineItemTierToDelete.replace('cLItierToDelete', thisCLITierToDelete.Id);
  stringToDelete = stringToDelete.replace('CLIID', thisOrdwayContractLineItem.Id);

  Test.startTest();
  TierController.upsertTiers(stringToUpsert, stringToDelete, 'OrdwayLabs__ContractLineItem__c', thisOrdwayContractLineItem.Id);
  Test.stopTest();
  System.assertEquals(2, [SELECT Id FROM ContractLineItemTier__c].Size());
  ContractLineItemTier__c thisTierToAssert = [SELECT Id, EndingUnit__c, StartingUnit__c
                                                    FROM ContractLineItemTier__c 
                                                    WHERE Id = :thisCLITier.Id];
  System.assertEquals(100, thisTierToAssert.EndingUnit__c, 'Should return 100');
  System.assertEquals(0, thisTierToAssert.StartingUnit__c, 'Should return 0');
}


@isTest
private static void upsertContractLineItemTier_VolumePricingNegative(){
  Contact thisOrdwayContact = TestObjectFactory.createContact();
  insert thisOrdwayContact;

  Opportunity thisOrdwayOpportunity       = TestObjectFactory.createOpportunity();
  thisOrdwayOpportunity.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayOpportunity;

  OpportunityLineItem thisOrdwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem();
  thisOrdwayOpportunityLineItem.OpportunityId       = thisOrdwayOpportunity.Id;
  insert thisOrdwayOpportunityLineItem;

  OrdwayContract__c thisOrdwayContract = TestObjectFactory.createOrdwayContract();
  thisOrdwayContract.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayContract;

  ContractLineItem__c thisOrdwayContractLineItem = TestObjectFactory.createContractLineItem();
  thisOrdwayContractLineItem.OrdwayContract__c   = thisOrdwayContract.Id;
  thisOrdwayContractLineItem.PricingModel__c     = 'Volume';
  thisOrdwayContractLineItem.Quantity__c         = null; 
  insert thisOrdwayContractLineItem;

  ContractLineItemTier__c thisCLITier = TestObjectFactory.createContractLineItemTier();
  thisCLITier.ListPrice__c            = 123;
  thisCLITier.ContractLineItem__c     = thisOrdwayContractLineItem.Id; 
  insert thisCLITier;

  ContractLineItemTier__c thisCLITierToDelete = TestObjectFactory.createContractLineItemTier();
  thisCLITierToDelete.ContractLineItem__c     = thisOrdwayContractLineItem.Id;
  insert thisCLITierToDelete;

  String stringToUpsert = OrdwayService_Mock.contractLineItemTier.replace('CONTRACTLINEITEMTIER', thisCLITier.Id);
  stringToUpsert = stringToUpsert.replace('CLIID', 'Negative Testing');  
  String stringToDelete = OrdwayService_Mock.contractLineItemTierToDelete.replace('cLItierToDelete', thisCLITierToDelete.Id);
  stringToDelete = stringToDelete.replace('CLIID', 'Negative Testing');

  Test.startTest();
  try{
    TierController.upsertTiers(stringToUpsert, stringToDelete, 'OrdwayLabs__ContractLineItem__c', thisOrdwayContractLineItem.Id);
    } catch (Exception ex) {
      system.assertNotEquals(null, ex.getMessage());
      }
  Test.stopTest();
}

@isTest
private static void upsertContractLineItemTier_TieredPricing(){
  Contact thisOrdwayContact = TestObjectFactory.createContact();
  insert thisOrdwayContact;

  Opportunity thisOrdwayOpportunity       = TestObjectFactory.createOpportunity();
  thisOrdwayOpportunity.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayOpportunity;

  OpportunityLineItem thisOrdwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem();
  thisOrdwayOpportunityLineItem.OpportunityId       = thisOrdwayOpportunity.Id;
  insert thisOrdwayOpportunityLineItem;

  OrdwayContract__c thisOrdwayContract = TestObjectFactory.createOrdwayContract();
  thisOrdwayContract.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayContract;
 
  ContractLineItem__c thisOrdwayContractLineItem = TestObjectFactory.createContractLineItem();
  thisOrdwayContractLineItem.OrdwayContract__c   = thisOrdwayContract.Id;
  thisOrdwayContractLineItem.PricingModel__c     = 'Tiered';
  thisOrdwayContractLineItem.Quantity__c         = 6;
  insert thisOrdwayContractLineItem;
  Test.startTest();
  OrdwayContractLineItemTierTriggerHandler.deleteTier = false;
           
  ContractLineItemTier__c thisCLITier = TestObjectFactory.createContractLineItemTier();
  thisCLITier.ContractLineItem__c     = thisOrdwayContractLineItem.Id; 
  insert thisCLITier;

  ContractLineItemTier__c thisCLITierToDelete = TestObjectFactory.createContractLineItemTier();
  thisCLITierToDelete.ContractLineItem__c     = thisOrdwayContractLineItem.Id;
  insert thisCLITierToDelete;

  String stringToUpsert = OrdwayService_Mock.contractLineItemTier.replace('CONTRACTLINEITEMTIER', thisCLITier.Id);
  stringToUpsert        = stringToUpsert.replace('CLIID', thisOrdwayContractLineItem.Id);  
  String stringToDelete = OrdwayService_Mock.contractLineItemTierToDelete.replace('cLItierToDelete', thisCLITierToDelete.Id);
  stringToDelete        = stringToDelete.replace('CLIID', thisOrdwayContractLineItem.Id);
  
  TierController.upsertTiers(stringToUpsert, stringToDelete, 'OrdwayLabs__ContractLineItem__c', thisOrdwayContractLineItem.Id); 
   Test.stopTest();
  System.assertEquals(2, [SELECT Id FROM ContractLineItemTier__c].Size());
  ContractLineItemTier__c thisTierToAssert = [SELECT Id, EndingUnit__c 
                                                    FROM ContractLineItemTier__c 
                                                    WHERE Id = :thisCLITier.Id];
  System.assertEquals(100, thisTierToAssert.EndingUnit__c, 'Should return 100');
  
}

@isTest
private static void upsertContractLineItemTier_TieredPricingNegative(){
  Contact thisOrdwayContact = TestObjectFactory.createContact();
  insert thisOrdwayContact;

  Opportunity thisOrdwayOpportunity       = TestObjectFactory.createOpportunity();
  thisOrdwayOpportunity.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayOpportunity;

  OpportunityLineItem thisOrdwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem();
  thisOrdwayOpportunityLineItem.OpportunityId       = thisOrdwayOpportunity.Id;
  insert thisOrdwayOpportunityLineItem;

  OrdwayContract__c thisOrdwayContract = TestObjectFactory.createOrdwayContract();
  thisOrdwayContract.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayContract;

  ContractLineItem__c thisOrdwayContractLineItem = TestObjectFactory.createContractLineItem();
  thisOrdwayContractLineItem.OrdwayContract__c   = thisOrdwayContract.Id;
  thisOrdwayContractLineItem.PricingModel__c     = 'Tiered';
  thisOrdwayContractLineItem.Quantity__c         = 6;
  insert thisOrdwayContractLineItem;

  ContractLineItemTier__c thisCLITier = TestObjectFactory.createContractLineItemTier();
  thisCLITier.ContractLineItem__c     = thisOrdwayContractLineItem.Id;
  thisCLITier.PricingType__c          = 'Per unit';
  thisCLITier.StartingUnit__c         = 3; 
  insert thisCLITier;

  ContractLineItemTier__c thisCLITierToDelete = TestObjectFactory.createContractLineItemTier();
  thisCLITierToDelete.ContractLineItem__c     = thisOrdwayContractLineItem.Id;
  insert thisCLITierToDelete;

  String stringToUpsert = OrdwayService_Mock.contractLineItemTier.replace('CONTRACTLINEITEMTIER', thisCLITier.Id);
  stringToUpsert        = stringToUpsert.replace('CLIID', 'Negative Testing');  
  String stringToDelete = OrdwayService_Mock.contractLineItemTierToDelete.replace('cLItierToDelete', thisCLITierToDelete.Id);
  stringToDelete        = stringToDelete.replace('CLIID','Negative Testing');

  Test.startTest();
  try{
  TierController.upsertTiers(stringToUpsert, stringToDelete, 'OrdwayLabs__ContractLineItem__c', thisOrdwayContractLineItem.Id);
  } catch (exception ex){
      System.assertNotEquals(null, ex.getMessage());
    }
  Test.stopTest();
 }

 @isTest
private static void upsertContractLineItemTier_VolumePricing_FlatFee(){
  Contact thisOrdwayContact = TestObjectFactory.createContact();
  insert thisOrdwayContact;

  Opportunity thisOrdwayOpportunity       = TestObjectFactory.createOpportunity();
  thisOrdwayOpportunity.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayOpportunity;

  OpportunityLineItem thisOrdwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem();
  thisOrdwayOpportunityLineItem.OpportunityId       = thisOrdwayOpportunity.Id;
  insert thisOrdwayOpportunityLineItem;

  OrdwayContract__c thisOrdwayContract = TestObjectFactory.createOrdwayContract();
  thisOrdwayContract.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayContract;

  ContractLineItem__c thisOrdwayContractLineItem = TestObjectFactory.createContractLineItem();
  thisOrdwayContractLineItem.OrdwayContract__c   = thisOrdwayContract.Id;
  thisOrdwayContractLineItem.PricingModel__c     = 'Volume';
  thisOrdwayContractLineItem.Quantity__c         = 6;
  insert thisOrdwayContractLineItem;

  OrdwayContractLineItemTierTriggerHandler.deleteTier = false;

  ContractLineItemTier__c thisCLITier = TestObjectFactory.createContractLineItemTier();
  thisCLITier.ListPrice__c            = 123;
  thisCLITier.StartingUnit__c         = 4;
  thisCLITier.EndingUnit__c           = 6; 
  thisCLITier.PricingType__c          = 'Flat fee';
  thisCLITier.ContractLineItem__c     = thisOrdwayContractLineItem.Id; 
  insert thisCLITier;

  ContractLineItemTier__c thisCLITierToDelete = TestObjectFactory.createContractLineItemTier();
  thisCLITierToDelete.ContractLineItem__c     = thisOrdwayContractLineItem.Id;
  thisCLITierToDelete.StartingUnit__c         = 4;
  thisCLITierToDelete.EndingUnit__c           = 6; 
  thisCLITierToDelete.PricingType__c          = 'Flat fee';
  thisCLITier.ListPrice__c                    = 123;
  insert thisCLITierToDelete;

  String stringToUpsert = OrdwayService_Mock.contractLineItemTier.replace('CONTRACTLINEITEMTIER', thisCLITier.Id);
  stringToUpsert = stringToUpsert.replace('CLIID', thisOrdwayContractLineItem.Id);  
  String stringToDelete = OrdwayService_Mock.contractLineItemTierToDelete.replace('cLItierToDelete', thisCLITierToDelete.Id);
  stringToDelete = stringToDelete.replace('CLIID', thisOrdwayContractLineItem.Id);

  Test.startTest();
  TierController.upsertTiers(stringToUpsert, stringToDelete, 'OrdwayLabs__ContractLineItem__c', thisOrdwayContractLineItem.Id);
  Test.stopTest();
  System.assertEquals(2, [SELECT Id FROM ContractLineItemTier__c].Size());
  ContractLineItemTier__c thisTierToAssert = [SELECT Id, EndingUnit__c 
                                                    FROM ContractLineItemTier__c 
                                                    WHERE Id = :thisCLITier.Id];
  System.assertEquals(100, thisTierToAssert.EndingUnit__c, 'Should return 100'); 
 }

 @isTest
private static void upsertContractLineItemTier_TieredPricing_FlatFee(){
  Contact thisOrdwayContact = TestObjectFactory.createContact();
  insert thisOrdwayContact;

  Opportunity thisOrdwayOpportunity       = TestObjectFactory.createOpportunity();
  thisOrdwayOpportunity.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayOpportunity;

  OpportunityLineItem thisOrdwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem();
  thisOrdwayOpportunityLineItem.OpportunityId       = thisOrdwayOpportunity.Id;
  insert thisOrdwayOpportunityLineItem;

  OrdwayContract__c thisOrdwayContract = TestObjectFactory.createOrdwayContract();
  thisOrdwayContract.BillingContact__c = thisOrdwayContact.Id;
  insert thisOrdwayContract;

  ContractLineItem__c thisOrdwayContractLineItem = TestObjectFactory.createContractLineItem();
  thisOrdwayContractLineItem.OrdwayContract__c   = thisOrdwayContract.Id;
  thisOrdwayContractLineItem.PricingModel__c     = 'Tiered';
  thisOrdwayContractLineItem.Quantity__c         = 6;
  insert thisOrdwayContractLineItem;

  OrdwayContractLineItemTierTriggerHandler.deleteTier = false;
            
  ContractLineItemTier__c thisCLITier = TestObjectFactory.createContractLineItemTier();
  thisCLITier.ListPrice__c            = 123;
  thisCLITier.StartingUnit__c         = 4;
  thisCLITier.EndingUnit__c           = 6; 
  thisCLITier.PricingType__c          = 'Flat fee';
  thisCLITier.ContractLineItem__c     = thisOrdwayContractLineItem.Id; 
  insert thisCLITier;

  ContractLineItemTier__c thisCLITierToDelete = TestObjectFactory.createContractLineItemTier();
  thisCLITierToDelete.ListPrice__c            = 123;
  thisCLITierToDelete.ContractLineItem__c     = thisOrdwayContractLineItem.Id;
  thisCLITierToDelete.StartingUnit__c         = 4;
  thisCLITierToDelete.EndingUnit__c           = 6;  
  thisCLITier.PricingType__c                  = 'Flat fee';      
  insert thisCLITierToDelete;

  String stringToUpsert = OrdwayService_Mock.contractLineItemTier.replace('CONTRACTLINEITEMTIER', thisCLITier.Id);
  stringToUpsert        = stringToUpsert.replace('CLIID', thisOrdwayContractLineItem.Id);  
  String stringToDelete = OrdwayService_Mock.contractLineItemTierToDelete.replace('cLItierToDelete', thisCLITierToDelete.Id);
  stringToDelete        = stringToDelete.replace('CLIID', thisOrdwayContractLineItem.Id);

  Test.startTest();
  TierController.upsertTiers(stringToUpsert, stringToDelete, 'OrdwayLabs__ContractLineItem__c', thisOrdwayContractLineItem.Id);
  Test.stopTest();
  System.assertEquals(2, [SELECT Id FROM ContractLineItemTier__c].Size());
  ContractLineItemTier__c thisTierToAssert = [SELECT Id, EndingUnit__c 
                                                    FROM ContractLineItemTier__c 
                                                    WHERE Id = :thisCLITier.Id];
  System.assertEquals(100, thisTierToAssert.EndingUnit__c, 'Should return 100');
  }

  @isTest
  private static void addQLITiers() {
    
    Account thisAccount = TestObjectFactory.createAccount();
    insert thisAccount;
  
    Opportunity thisOpportunity = TestObjectFactory.createOpportunity();
    insert thisOpportunity;

    OpportunityLineItem thisOLI = TestObjectFactory.createOpportunityLineItem();
    thisOLI.OpportunityId       = thisOpportunity.Id;
    insert thisOLI;
    
    OpportunityLineItemTier__c thisOLITier = TestObjectFactory.createOpportunityLineItemTier();
    thisOLITier.StartingUnit__c            = 123;
    thisOLITier.OpportunityProduct__c      = thisOLI.Id; 
    insert thisOLITier;

    Quote__c thisQuote       = TestObjectFactory.createQuote();
    thisQuote.Account__c     = thisAccount.Id;
    thisQuote.Opportunity__c = thisOpportunity.Id;
    insert thisQuote;
  
    QuoteLineItem__c thisQLI = TestObjectFactory.createQuoteLineItem();
    thisQLI.Name             = 'Test QLI';
    thisQLI.Quote__c         = thisQuote.Id;
    thisQLI.OpportunityLineItemId__c = thisOLI.Id;
    insert thisQLI;
      
    QuoteLineItemTier__c thisQLITier = TestObjectFactory.createQuoteLineItemTier();
    thisQLITier.QuoteLineItem__c     = thisQLI.Id;
    thisQLITier.StartingUnit__c      = 123;
    insert thisQLITier;
  
    Test.startTest();
    TierController.InitialWrapper thisInitialWrapper = new TierController.InitialWrapper();
    thisInitialWrapper = TierController.getInitialWrapper(thisQLI.Id, 'OrdwayLabs__QuoteLineItem__c');
    List<SObject> tierList = thisInitialWrapper.tierList;
    SObject parentRecord = thisInitialWrapper.thissobject;
    Test.stopTest();
    System.assertEquals(1, tierList.size(), 'Should return 1 QuoteLineItemTier as we are inserting 1 tier record');
    System.assertEquals(123, tierList[0].get('OrdwayLabs__StartingUnit__c'), 'Should return 123');
    System.assertEquals('Test QLI', parentRecord.get('Name'), 'Should return Test QLI');
  }

  @isTest
  private static void opportunityLineItemTierDeleteException() {

    Contact thisOrdwayContact = TestObjectFactory.createContact();
    insert thisOrdwayContact;
    
    Opportunity thisOrdwayOpportunity       = TestObjectFactory.createOpportunity();
    thisOrdwayOpportunity.BillingContact__c = thisOrdwayContact.Id;
    insert thisOrdwayOpportunity;
    
    Test.startTest();
    OpportunityLineItem thisOrdwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem();
    thisOrdwayOpportunityLineItem.OpportunityId       = thisOrdwayOpportunity.Id;
    insert thisOrdwayOpportunityLineItem; 

    OpportunityLineItemTier__c thisOLITier = TestObjectFactory.createOpportunityLineItemTier();
    thisOLITier.StartingUnit__c            = 123;
    thisOLITier.OpportunityProduct__c      = thisOrdwayOpportunityLineItem.Id; 
    insert thisOLITier;

    try{
      delete thisOLITier;
     } catch (Exception ex){
       system.assertNotEquals(null, ex.getMessage());
    }
    Test.stopTest();
    System.assertEquals(1, [SELECT Id FROM OpportunityLineItemTier__c].SIZE());
  }

  @isTest
  private static void deleteAllRelatedTiers_Charge() {
   
    TierController.validateOnDelete = false;
    Plan__c thisPlan = TestObjectFactory.createOrdwayPlan();
    insert thisPlan;

    Charge__c thisCharge       = TestObjectFactory.createCharge();
    thisCharge.Name            = 'Test Charge';
    thisCharge.OrdwayPlan__c   =  thisPlan.Id;
    insert thisCharge;

    ChargeTier__c thisChargeTier = TestObjectFactory.createChargeTier();
    thisChargeTier.OrdwayLabs__Charge__c     = thisCharge.Id;
    insert thisChargeTier;

    Test.startTest();
      ChargeTier__c thisChargeTierToDelete = TestObjectFactory.createChargeTier();
      thisChargeTierToDelete.OrdwayLabs__Charge__c     = thisCharge.Id;
      insert thisChargeTierToDelete;
    Test.stopTest();
    System.assertEquals(1, [SELECT Id FROM ChargeTier__c].size());
  }

  @isTest
  private static void addOLITiers() {
    Account thisAccount = TestObjectFactory.createAccount();
    insert thisAccount;
  
    Opportunity thisOpportunity = TestObjectFactory.createOpportunity();
    insert thisOpportunity;

    OpportunityLineItem thisOLI = TestObjectFactory.createOpportunityLineItem();
    thisOLI.OpportunityId       = thisOpportunity.Id;
    insert thisOLI;
    
    OpportunityLineItemTier__c thisOLITier = TestObjectFactory.createOpportunityLineItemTier();
    thisOLITier.StartingUnit__c            = 123;
    thisOLITier.OpportunityProduct__c      = thisOLI.Id; 
    insert thisOLITier;

    Quote__c thisQuote       = TestObjectFactory.createQuote();
    thisQuote.Account__c     = thisAccount.Id;
    thisQuote.Opportunity__c = thisOpportunity.Id;
    insert thisQuote;
  
    QuoteLineItem__c thisQLI = TestObjectFactory.createQuoteLineItem();
    thisQLI.Quote__c         = thisQuote.Id;
    thisQLI.OpportunityLineItemId__c = thisOLI.Id;
    insert thisQLI;
      
    QuoteLineItemTier__c thisQLITier = TestObjectFactory.createQuoteLineItemTier();
    thisQLITier.QuoteLineItem__c     = thisQLI.Id;
    thisQLITier.Tier__c = 234;
    thisQLITier.EndingUnit__c = 123;
    thisQLITier.ListPrice__c  = 123;
    thisQLITier.PricingType__c = 'Per unit';
    thisQLITier.StartingUnit__c = 123;
    insert thisQLITier;

    Test.startTest();
    TierController.InitialWrapper thisInitialWrapper = new TierController.InitialWrapper();
    thisInitialWrapper = TierController.getInitialWrapper(thisOLI.Id, 'OpportunityLineItem');
    List<SObject> tierList = thisInitialWrapper.tierList;
    SObject parentRecord = thisInitialWrapper.thissobject;
    Test.stopTest();
    System.assertEquals(1, tierList.size(), 'Should return 1 OpportunityLineItemTier as we are inserting 1 tier record');
    System.assertEquals(123, tierList[0].get('OrdwayLabs__StartingUnit__c'), 'Should return 123');
   // System.assertEquals('Test OLI', parentRecord.get('Name'), 'Should return Test OLI');
 }
}