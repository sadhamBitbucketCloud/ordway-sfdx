public class PlanPickerController {

	@AuraEnabled
	public static InitialWrapper getInitialWrapper(String parentRecordId, String pricebookId){
		ApplicationLog.StartLoggingContext('PlanPickerController','getInitialWrapper()');
		SObjectType parentSobjectType = Id.valueOf(parentRecordId).getSObjectType();
		InitialWrapper thisInitialWrapper               = new InitialWrapper();
		thisInitialWrapper.lineItemMap                  = PlanPickerHelper.getOrdwayPlanLineItemsRecords(parentRecordId, pricebookId, thisInitialWrapper.lineItemTiersMap, thisInitialWrapper.chargeTiersMap, thisInitialWrapper);
		thisInitialWrapper.salesforceBaseURL            = getSalesforceOrgBaseURL();
		thisInitialWrapper.parentObject                 = PlanPickerHelper.getParentRecord(parentRecordId).get(Id.valueOf(parentRecordId));
		thisInitialWrapper.currencyISO 									= AddEditPlanHelper.getCurrencyIsoCode(parentRecordId);
		thisInitialWrapper.planPickerFieldWrapper = PlanPickerHelper.getPlanPickerWrapper(String.valueOf(parentSobjectType), thisInitialWrapper.currencyISO);
		ApplicationLog.FinishLoggingContext();
		return thisInitialWrapper;
	}

	public static String getSalesforceOrgBaseURL() {
    return URL.getSalesforceBaseUrl().toExternalForm();
  }

	@AuraEnabled
  public static void updatePlans(String lineItemList ,String lineItemToDeleteId, String parentRecordId, String lineItemTiersToDeleteId, String lineItemTiersString, String chargeTierMapString, Boolean isSelectedTable, String lineItemTierRelationShipAPIName) {
    ApplicationLog.StartLoggingContext('PlanPickerController', 'updatePlans()');
    try {
			TierController.validateOnDelete = false;

			Map<String, System.Type> classTypeMap = new Map<String, System.Type>{'OpportunityLineItem' => List<OpportunityLineItem>.class,
																																			'OrdwayLabs__QuoteLineItem__c' => List<OrdwayLabs__QuoteLineItem__c>.class};
			
			Map<String, System.Type> tierClassTypeMap = new Map<String, System.Type>{'OpportunityLineItem' =>  Map<String, List<OrdwayLabs__OpportunityLineItemTier__c>>.class,
																																							'OrdwayLabs__QuoteLineItem__c' => Map<String, List<OrdwayLabs__QuoteLineItemTier__c>>.class};
	
			SObjectType parentSobjectType = Id.valueOf(parentRecordId).getSObjectType();
			String childSObjectAPIName;
			String tierLineItemAPIName;

			if(PlanPickerHelper.parentToChildRelationshipMap.containsKey(String.valueOf(parentSobjectType))){
				childSObjectAPIName = PlanPickerHelper.parentToChildRelationshipMap.get(String.valueOf(Id.valueOf(parentRecordId).getSObjectType()));
			}

			if(PlanPickerHelper.lineItemToChildTierRelationshipMap.containsKey(childSObjectAPIName)){
				tierLineItemAPIName = PlanPickerHelper.lineItemToChildTierRelationshipMap.get(childSObjectAPIName);
			}

			Map<String, List<SObject>> lineItemTiers = (Map<String, List<SObject>>) JSON.deserialize(lineItemTiersString , tierClassTypeMap.get(childSObjectAPIName));

			Map<String, List<SObject>> chargeTierMap = (Map<String, List<SObject>>) JSON.deserialize(chargeTierMapString , tierClassTypeMap.get(childSObjectAPIName));

			List<String> lineItemTierIdsToDelete = (List<String>) JSON.deserialize(lineItemTiersToDeleteId,List<String>.class);
			
			if(!lineItemTierIdsToDelete.isEmpty()){
				deleteLineItems(lineItemTierIdsToDelete, tierLineItemAPIName);
			}

			List<Id> lineItemIdsToDelete = (List<Id>) JSON.deserialize(lineItemToDeleteId,List<String>.class);
			if(!lineItemIdsToDelete.isEmpty()){
				deleteLineItemsTierByLineItemIds(lineItemIdsToDelete, tierLineItemAPIName, lineItemTierRelationShipAPIName);
				deleteLineItems(lineItemIdsToDelete, childSObjectAPIName);
			}

			List<Sobject> lineItemsListToUpsert = (List<Sobject>) JSON.deserialize(lineItemList ,classTypeMap.get(childSObjectAPIName));
	
      Map<String, Object> tierMap = new Map<String, Object>();
      ContractPlanPickerSetting__c thisSetting = PlanPickerHelper.getOpportunityCustomSetting();

      //Loop through Opportunity Line Items
      for (SObject thisLineItem : lineItemsListToUpsert) {
				
        populateDefaultValues(thisLineItem);
        //Opportunitylineitem specific logic implementation
				if(childSObjectAPIName == 'OpportunityLineItem'){
					if(thisLineItem.get('Id') == null 
          && thisLineItem.get('OrdwayLabs__ContractLineItem__c') != null){
						thisLineItem.put('TotalPrice', null);
					}
				}

        if(!PlanPickerHelper.getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__PricingModel__c').equals(OrdwayHelper.PRICING_MODEL_PER_UNIT)) {
					Decimal listPrice = 0;
					List<SObject> tierList = new List<SObject>();
					if(isSelectedTable 
						&& lineItemTiers.containsKey(PlanPickerHelper.getSObjectfieldValueAsString(thisLineItem, 'Id'))){
							tierList = lineItemTiers.get(PlanPickerHelper.getSObjectfieldValueAsString(thisLineItem, 'Id'));
					}
					else if(!chargeTierMap.isEmpty() 
						&& chargeTierMap.containsKey(PlanPickerHelper.getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__OrdwayChargeId__c'))){
							tierList = chargeTierMap.get(PlanPickerHelper.getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__OrdwayChargeId__c'));
					}

					if(!tierList.isEmpty()){
						for(SObject thisTier : tierList){
							if(PlanPickerHelper.getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__PricingModel__c') == OrdwayHelper.VOLUME) {
								calculateVolumePricing(thisLineItem, thisTier, childSObjectAPIName == 'OpportunityLineItem');
								continue;
							}

							//TIERED
							if(PlanPickerHelper.getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__PricingModel__c') == OrdwayHelper.TIERED) {
								listPrice = calculateTieredPricing(thisLineItem, thisTier, listPrice, childSObjectAPIName == 'OpportunityLineItem');
							}
						}
					}
        }

				// Update Calculated Tier Price When chargetype is usage based
				if(thisLineItem.get('OrdwayLabs__PricingModel__c') == 'Per Unit'){
					thisLineItem.put('OrdwayLabs__CalculatedTierPrice__c',  null);
				}
      }
      SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.CREATABLE, lineItemsListToUpsert);

      ApplicationLog.Info( 'INFO 1 >>>' + JSON.serializePretty(securityDecision.getRemovedFields()));
      ApplicationLog.Info('INFO 2 >>>' + JSON.serializePretty(securityDecision.getRecords()));

      ApplicationLog.FinishLoggingContext();
			lineItemsListToUpsert = securityDecision.getRecords();
      upsert lineItemsListToUpsert;

			if(isSelectedTable){
				List<SObject> finalListOfLineItemTiers = new List<SObject>();
				for(String lineItemId : lineItemTiers.keySet()){
					if(!lineItemIdsToDelete.contains(lineItemId)){
						finalListOfLineItemTiers.addAll(lineItemTiers.get(lineItemId));
					}
				}
				upsert finalListOfLineItemTiers;
			}
			else{
				if(!chargeTierMap.isEmpty()){
					String chargeId;
					List<SObject> finalListOfNewLineItemTiers = new List<SObject>();
					for(SObject thisLineItem : lineItemsListToUpsert){
						chargeId = String.valueOf(thisLineItem.get('OrdwayLabs__OrdwayChargeId__c'));
						if(chargeTierMap.containsKey(chargeId)){
							for(SObject thisNewLineItemTier : chargeTierMap.get(chargeId)){
								thisNewLineItemTier.put(lineItemTierRelationShipAPIName, thisLineItem.Id);
								finalListOfNewLineItemTiers.add(thisNewLineItemTier);
							}
						}
					}

					upsert finalListOfNewLineItemTiers;
				}
			}
			TierController.validateOnDelete = true;

			//DELETE all Tiers when parent pricing modal is per unit
			List<String> lineItemToDelete = new List<String>();
			for(SObject thisLineItem : lineItemsListToUpsert){
				if(thisLineItem.get('OrdwayLabs__PricingModel__c') == 'Per Unit' ){
					lineItemToDelete.add(thisLineItem.Id);
				}
			}

			if(!lineItemToDelete.isEmpty()){
				deleteLineItemsTierByLineItemIds(lineItemToDelete, tierLineItemAPIName, lineItemTierRelationShipAPIName);
			}

    } catch (Exception e) {
      AuraHandledException ex = new AuraHandledException(e.getMessage());
      ex.setMessage(e.getMessage());
      throw ex;
    }
  }

	public static void deleteLineItems(List<String> lineItemIds, String childSObjectAPIName) {
		SObjectType childSobjectType = ((SObject)Type.forName(childSObjectAPIName).newInstance()).getSObjectType();
		DELETE  Database.query(String.escapeSingleQuotes('SELECT ' +String.join(SObjectHelper.getAllFields(childSobjectType),',') +' FROM '+childSObjectAPIName+' WHERE Id IN :lineItemIds'));
	}	
	
	public static void deleteLineItemsTierByLineItemIds(List<String> lineItemIds, String lineItemTierAPIName, String lineItemRelationShipName) {
		SObjectType childSobjectType = ((SObject)Type.forName(lineItemTierAPIName).newInstance()).getSObjectType();
		String queryString =  'SELECT ' +String.join(SObjectHelper.getAllFields(childSobjectType),',') +' FROM '+lineItemTierAPIName+' WHERE '+lineItemRelationShipName+' IN:lineItemIds';
		DELETE  Database.query(String.escapeSingleQuotes(queryString));
	}	

	public static void populateDefaultValues(SObject thisLineItem) {
		thisLineItem.put('OrdwayLabs__BillingPeriod__c', PlanPickerHelper.getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__BillingPeriod__c') == 'N/A' ?  null : PlanPickerHelper.getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__BillingPeriod__c'));
		thisLineItem.put('OrdwayLabs__IncludedUnits__c', PlanPickerHelper.getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__IncludedUnits__c') == 'N/A' ?  null : PlanPickerHelper.getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__IncludedUnits__c'));
		thisLineItem.put('OrdwayLabs__UnitofMeasure__c', PlanPickerHelper.getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__UnitofMeasure__c') == 'N/A' ?  null : PlanPickerHelper.getSObjectfieldValueAsString(thisLineItem, 'OrdwayLabs__UnitofMeasure__c'));
  }

	public static void calculateVolumePricing(SObject thisLineItem, SObject tierMap, Boolean isOpportunityLineItem) {
		Integer startingUnit = Integer.ValueOf(tierMap.get('OrdwayLabs__StartingUnit__c'));
    String typeZ = String.valueOf(tierMap.get('OrdwayLabs__PricingType__c'));
    Decimal lPrice = Decimal.valueOf(String.valueOf(tierMap.get('OrdwayLabs__ListPrice__c')));

		String quantityFieldAPIName = isOpportunityLineItem ? 'Quantity' : 'OrdwayLabs__Quantity__c';
    if (Integer.ValueOf(thisLineItem.get(quantityFieldAPIName)) > startingUnit
			 && (tierMap.get('OrdwayLabs__EndingUnit__c') == '' || tierMap.get('OrdwayLabs__EndingUnit__c') == null 
			 ||	(tierMap.get('OrdwayLabs__EndingUnit__c') != '' 
			 && Integer.ValueOf(thisLineItem.get(quantityFieldAPIName)) <= Integer.ValueOf(tierMap.get('OrdwayLabs__EndingUnit__c'))))) {

        if (typeZ != OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
          thisLineItem.put('OrdwayLabs__CalculatedTierPrice__c', lPrice != null ? lPrice * Integer.ValueOf(thisLineItem.get(quantityFieldAPIName)) : 0);
          thisLineItem.put('OrdwayLabs__OrdwayListPrice__c', lPrice != null && lPrice != 0 ? lPrice : 0);
        } 
        else if (typeZ == OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
          thisLineItem.put('OrdwayLabs__CalculatedTierPrice__c', lPrice != null ? lPrice : 0);
          thisLineItem.put('OrdwayLabs__OrdwayListPrice__c', lPrice != null && lPrice != 0 ? lPrice/Integer.ValueOf(thisLineItem.get(quantityFieldAPIName)) : 0);
        }
      }
  }
  
  public static Decimal calculateTieredPricing(SObject thisLineItem, SObject tierMap, Decimal listPrice, Boolean isOpportunityLineItem) {
    Integer startingUnit = Integer.ValueOf(tierMap.get('OrdwayLabs__StartingUnit__c'));
    String typeZ = String.valueOf(tierMap.get('OrdwayLabs__PricingType__c'));
    Decimal lPrice = Decimal.valueOf(String.valueOf(tierMap.get('OrdwayLabs__ListPrice__c')));
    String quantityFieldAPIName = isOpportunityLineItem ? 'Quantity' : 'OrdwayLabs__Quantity__c';

    if (Integer.valueOf(thisLineItem.get(quantityFieldAPIName)) > startingUnit 
      &&	tierMap.get('OrdwayLabs__EndingUnit__c') != '' && tierMap.get('OrdwayLabs__EndingUnit__c') != null 
      && Integer.valueOf(thisLineItem.get(quantityFieldAPIName)) >= Integer.ValueOf(tierMap.get('OrdwayLabs__EndingUnit__c'))) {
      // NOT EQ FLAT FEE
      if (typeZ != OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice = listPrice + (Integer.ValueOf(tierMap.get('OrdwayLabs__EndingUnit__c')) - startingUnit) * lPrice;
      } else if (typeZ == OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice = listPrice + lPrice;
      }
    } else if (Integer.valueOf(thisLineItem.get(quantityFieldAPIName)) > startingUnit 
      && (tierMap.get('OrdwayLabs__EndingUnit__c') == '' || tierMap.get('OrdwayLabs__EndingUnit__c') == null || (tierMap.get('OrdwayLabs__EndingUnit__c') != '' &&
      Integer.valueOf(thisLineItem.get(quantityFieldAPIName)) < Integer.ValueOf(tierMap.get('OrdwayLabs__EndingUnit__c'))))) {
      // NOT EQ FLAT FEE

      if (typeZ != OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice = listPrice + (Integer.valueOf(thisLineItem.get(quantityFieldAPIName)) - startingUnit) * lPrice;
      } else if (typeZ == OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice = listPrice + lPrice;
      }
    }
    thisLineItem.put('OrdwayLabs__CalculatedTierPrice__c', listPrice);
    thisLineItem.put('OrdwayLabs__OrdwayListPrice__c', listPrice != null && listPrice != 0 ? listPrice/Integer.ValueOf(thisLineItem.get(quantityFieldAPIName)) : 0);
    return listPrice;
  }

	@AuraEnabled
	public static void triggerUpdateOnParentRecord(String parentRecordId) {
		Map<Id, SObject> parentObjectToUpdate;
		if(String.valueOf(Id.valueOf(parentRecordId).getSObjectType()) == 'OrdwayLabs__Quote__c'
			&& OpportunitySettingController.syncQuoteAutomatically()){
				parentObjectToUpdate = PlanPickerHelper.getParentRecord(parentRecordId);
				SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE, parentObjectToUpdate.values());
				update securityDecision.getRecords();
			}
	}

	public class InitialWrapper {
		@AuraEnabled
		public Map<String, List<SObject>> lineItemMap;
		@AuraEnabled
		public Map<String, PlanPickerHelper.RequiredFieldWrapper> planPickerFieldWrapper;
		@AuraEnabled
		public String salesforceBaseURL;
		@AuraEnabled
		public String currencyISO;
		@AuraEnabled
		public SObject parentObject;
		@AuraEnabled
		public Map<String, List<SObject>> lineItemTiersMap;
		@AuraEnabled
		public Map<String, List<SObject>> chargeTiersMap;
		@AuraEnabled
		public String lineItemTierRelationShipAPIName;

		public InitialWrapper(){
			lineItemTiersMap = new Map<String, List<SObject>>();
			chargeTiersMap   = new Map<String, List<SObject>>();
		}
	}
}