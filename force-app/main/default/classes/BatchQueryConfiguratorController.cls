public with sharing class BatchQueryConfiguratorController {

	@AuraEnabled
	public static FieldWrapper getObjectFieldWrapper(String customSettingUniqueName, String objectAPIName){
		Map<String, String> fieldMap = new Map<String,String>();
		List<PicklistEntryWrapper> fieldList = new  List<PicklistEntryWrapper>();
		Set<String> fieldTypes = new Set<String>{'Text',  'Picklist', 'Date', 'Date/Time', 'Checkbox'};

		FieldWrapper thisWrapper = new FieldWrapper();
		for(FieldDefinition thisDefinition : [SELECT QualifiedApiName,Length,MasterLabel,DurableId,DataType,isCalculated,ExtraTypeInfo, EntityDefinition.DurableId
																														FROM FieldDefinition
																														WHERE EntityDefinition.DurableId =:objectAPIName
																														ORDER BY MasterLabel]){
				if(fieldTypes.contains(SObjectFieldMappingHelper.getTargetFieldDataTypeKey(thisDefinition.DataType))){
						thisWrapper.fieldMap.put(thisDefinition.QualifiedApiName , SObjectFieldMappingHelper.getTargetFieldDataTypeKey(thisDefinition.DataType));
						thisWrapper.fieldList.add(new PicklistEntryWrapper(thisDefinition.MasterLabel, thisDefinition.QualifiedApiName));
				}
			thisWrapper.syncConfigurationSetting = getBulkSyncConfiguration(customSettingUniqueName);
		}
		return thisWrapper;
	}

	public static OrdwayLabs__BulkSyncConfiguration__c getBulkSyncConfiguration(String recordName){
		//TODO: what if the record is not present, we need to create a new instance and return right?
		OrdwayLabs__BulkSyncConfiguration__c thisConfiguration = OrdwayLabs__BulkSyncConfiguration__c.getValues(recordName);
		return thisConfiguration;
	}

	@AuraEnabled
	public static void saveBatchQueryCustomSetting(String customSettingJSONString){
		OrdwayLabs__BulkSyncConfiguration__c thisBatchConfiguration = (BulkSyncConfiguration__c)JSON.deserialize(customSettingJSONString, OrdwayLabs__BulkSyncConfiguration__c.class);
		upsert thisBatchConfiguration;
	}

  //TODO : Move the wrapper schema to Ordway helper, create a method in that class which accepts param label, value and return PicklistEntryWrapper
	public class PicklistEntryWrapper {
		@AuraEnabled
		public String label { get; set; }
		@AuraEnabled
		public String value { get; set; }

		public PicklistEntryWrapper(
			String picklistEntryLabel,
			String picklistEntryApiName
		) {
			this.label = picklistEntryLabel;
			this.value = picklistEntryApiName;
		}
	}

	//TODO : Change the Wrapper name since in future it will be updated
	//TODO: The ObjectFieldWrapper should contain the CUstom setting record
	public class FieldWrapper{
		@AuraEnabled
		public Map<String, String> fieldMap { get; set; }
		@AuraEnabled
		public OrdwayLabs__BulkSyncConfiguration__c syncConfigurationSetting;
		@AuraEnabled
		public List<PicklistEntryWrapper> fieldList { get; set; }
		FieldWrapper(){
			fieldMap = new Map<String, String>();
			fieldList = new List<PicklistEntryWrapper>();
		}
	}
}