/**
 * @File Name          : SyncOrdwayContract_Scheduler.cls
 * @Description        : Scheduler Class for SyncOrdwayContract_Batch
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    12/10/2019   Ordway Labs     Initial Version
 **/
global with sharing class SyncOrdwayContract_Scheduler implements Schedulable {
  global void execute(SchedulableContext sc) {
    SyncOrdwayContract_Batch syncBatch = new SyncOrdwayContract_Batch();
    //Ordway doesn't support bulkified request, so sending it one by one
    Database.executebatch(syncBatch, 1);
  }
}