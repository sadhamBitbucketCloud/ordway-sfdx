/**
 * @File Name          : OrdwayContractPlatformEventHelper.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-22-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    3/19/2020        Initial Version
 **/
public with sharing class OrdwayContractPlatformEventHelper {
  /**
   * @description Method to Publish Ordway Contract Platform Event for Ordway Contract
   * @param thisObjecttList Ordway sObject List
   **/
  public static void publishOrdwayContractAsyncEvent(
    List<SObject> thisObjecttList
  ) {
    List<OrdwayContractAsyncEvent__e> contractPlatformEventList = new List<OrdwayContractAsyncEvent__e>();

    for (SObject thisObject : thisObjecttList) {
      //Initialize the event
      OrdwayContractAsyncEvent__e thisObjectAsyncEvent = new OrdwayContractAsyncEvent__e();

      if (
        SObjectHelper.isFieldCreatable(
          SObjectType.OrdwayContractAsyncEvent__e,
          Schema.OrdwayContractAsyncEvent__e.Status__c
        )
      ) {
        thisObjectAsyncEvent.Status__c = (String) thisObject.get(
          'SyncStatus__c'
        );
      }
      if (
        SObjectHelper.isFieldCreatable(
          SObjectType.OrdwayContractAsyncEvent__e,
          Schema.OrdwayContractAsyncEvent__e.RecordId__c
        )
      ) {
        thisObjectAsyncEvent.RecordId__c = (String) thisObject.get(
          OrdwayHelper.SOBJECT_FIELD_ID
        );
      }
      if (
        SObjectHelper.isFieldCreatable(
          SObjectType.OrdwayContractAsyncEvent__e,
          Schema.OrdwayContractAsyncEvent__e.Message__c
        )
      ) {
        if (
          ((Id) thisObject.get(OrdwayHelper.SOBJECT_FIELD_ID))
            .getSObjectType() == OrdwayContract__c.SObjectType
        ) {
          thisObjectAsyncEvent.Message__c = thisObject.get('SyncStatus__c') ==
            OrdwayHelper.SYNC_STATE_IN_SYNC
            ? System.Label.Ordway_Contract_Async_To_Ordway_Success_Message
            : System.Label.Ordway_Contract_Async_To_Ordway_Error_Message;
        } else if (
          ((Id) thisObject.get(OrdwayHelper.SOBJECT_FIELD_ID))
            .getSObjectType() == Quote__c.SObjectType
        ) {
          thisObjectAsyncEvent.Message__c = thisObject.get('SyncStatus__c') ==
            OrdwayHelper.SYNC_STATE_IN_SYNC
            ? System.Label.Ordway_Quote_Async_To_Ordway_Success_Message
            : System.Label.Ordway_Quote_Async_To_Ordway_Error_Message;
        }
      }
      contractPlatformEventList.add(thisObjectAsyncEvent);
    }
    publishOrdwayContractEvent(contractPlatformEventList);
  }

  /**
   * @description Method to Publish Ordway Contract Platform Event for Ordway Contract
   * @param thisObjecttList Ordway sObject List
   **/
  public static void publishActivateContractEvent(
    List<SObject> thisObjecttList
  ) {
    List<OrdwayContractAsyncEvent__e> contractPlatformEventList = new List<OrdwayContractAsyncEvent__e>();

    for (SObject thisObject : thisObjecttList) {
      //Initialize the event
      OrdwayContractAsyncEvent__e thisObjectAsyncEvent = new OrdwayContractAsyncEvent__e();

      if (
        SObjectHelper.isFieldCreatable(
          SObjectType.OrdwayContractAsyncEvent__e,
          Schema.OrdwayContractAsyncEvent__e.Status__c
        )
      ) {
        thisObjectAsyncEvent.Status__c = (String) thisObject.get(
          'OrdwayLabs__OrdwayContractStatus__c'
        );
      }
      if (
        SObjectHelper.isFieldCreatable(
          SObjectType.OrdwayContractAsyncEvent__e,
          Schema.OrdwayContractAsyncEvent__e.RecordId__c
        )
      ) {
        thisObjectAsyncEvent.RecordId__c = (String) thisObject.get(
          OrdwayHelper.SOBJECT_FIELD_ID
        );
      }
      if (
        SObjectHelper.isFieldCreatable(
          SObjectType.OrdwayContractAsyncEvent__e,
          Schema.OrdwayContractAsyncEvent__e.Message__c
        )
      ) {
        if (
          ((Id) thisObject.get(OrdwayHelper.SOBJECT_FIELD_ID))
            .getSObjectType() == OrdwayContract__c.SObjectType
        ) {
          thisObjectAsyncEvent.Message__c = thisObject.get('OrdwayLabs__OrdwayContractStatus__c') ==
            OrdwayHelper.ACTIVE
            ? 'Contract activated successfully'
            : 'Failed to activate the contract';
            contractPlatformEventList.add(thisObjectAsyncEvent);
        }
      }
    }
    publishOrdwayContractEvent(contractPlatformEventList);
  }

  /**
   * @description Method to Publish Ordway Contract Platform Event
   * @param thisOrdwayContractEvents Ordway Contract Platform Event
   **/
  public static void publishOrdwayContractEvent(
    List<OrdwayContractAsyncEvent__e> thisOrdwayContractEvents
  ) {
    List<Database.SaveResult> publicEventSaveResult = EventBus.publish(
      thisOrdwayContractEvents
    );

    for (Database.SaveResult thisSaveResult : publicEventSaveResult) {
      if (!thisSaveResult.isSuccess()) {
        ApplicationLog.StartLoggingContext(
          'OrdwayContractPlatformEventHelper',
          'publishOrdwayContractEvent()'
        );
        for (Database.Error eventError : thisSaveResult.getErrors()) {
          ApplicationLog.Info(
            'Ordway Contract Platform Event Error>>' +
            eventError.getStatusCode() +
            ' - ' +
            eventError.getMessage()
          );
        }
      }
    }
  }
}