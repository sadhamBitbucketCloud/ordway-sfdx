/**
 * @File Name          : MockAddPlan_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    12/31/2019   Ordway Labs     Initial Version
 **/
@isTest
global class MockAddPlan_Test implements HttpCalloutMock {
  Id productId;
  global MockAddPlan_Test(Id productId) {
    this.productId = productId;
  }

  global HTTPResponse respond(HTTPRequest thisRequest) {
    String endPoint = OrdwayService.getEndPoint(
      MetadataService.GET_ORDWAY_PLAN
    );

    //System.assertEquals('callout:OrdwayLabs__OrdwaySandbox'+endPoint, thisRequest.getEndpoint());
    System.assertEquals('GET', thisRequest.getMethod());

    String jsonString = getAddPlan();

    if (productId != null) {
      jsonString = jsonString.replace(
        '01t2v00000Ax8G8AAJ',
        String.valueOf(productId)
      );
    }
    // Create a fake response
    HttpResponse thisResponse = new HttpResponse();
    thisResponse.setHeader('Content-Type', 'application/json');
    thisResponse.setBody(jsonString);
    thisResponse.setStatusCode(200);
    return thisResponse;
  }

  private static String getAddPlan() {
    return ' { ' +
      ' "plans": [ ' +
      ' { ' +
      ' "id": "PLN-00025", ' +
      ' "name": "Demo Test", ' +
      ' "status": "Active", ' +
      ' "description": "", ' +
      ' "created_by": "kunal.vashist@ordwaylabs.com", ' +
      ' "updated_by": "kunal.vashist@ordwaylabs.com", ' +
      ' "created_date": "2020-06-23T12:56:55.654Z", ' +
      ' "updated_date": "2020-06-23T12:56:55.654Z", ' +
      ' "charges": [ ' +
      ' { ' +
      ' "id": "CHG-000025", ' +
      ' "product_id": "P-00001", ' +
      ' "name": "Demo Charge", ' +
      ' "description": "", ' +
      ' "type": "One time", ' +
      ' "timing": "Initial", ' +
      ' "billing_period": "", ' +
      ' "effective_date": "Contract effective date", ' +
      ' "charge_billing_date": "Contract effective date", ' +
      ' "pricing_model": "Per unit", ' +
      ' "list_price": "125.00", ' +
      ' "custom_fields": { ' +
      ' "salesforce_id": "01tf4000003j743AAA" ' +
      ' }, ' +
      ' "salesforce_id": "01tf4000003j743AAA" ' +
      ' } ' +
      ' ], ' +
      ' "public_url": "", ' +
      ' "custom_fields": {} ' +
      ' }, ' +
      ' { ' +
      ' "id": "NewPlan", ' +
      ' "name": "New Plan", ' +
      ' "status": "Active", ' +
      ' "description": "", ' +
      ' "created_by": "dax@ordwaylabs.com", ' +
      ' "updated_by": "dax@ordwaylabs.com", ' +
      ' "created_date": "2020-05-20T15:25:01.639Z", ' +
      ' "updated_date": "2020-05-20T15:25:01.639Z", ' +
      ' "charges": [ ' +
      ' { ' +
      ' "id": "PrePay1", ' +
      ' "product_id": "Test", ' +
      ' "name": "PrePay1", ' +
      ' "description": "", ' +
      ' "type": "Usage based", ' +
      ' "backcharge_current_period": true, ' +
      ' "prepayment_amount": 1208571.429, ' +
      ' "prepaid_units_enable": true, ' +
      ' "auto_refill_prepayment_units": false, ' +
      ' "refill_qty": 0, ' +
      ' "refill_prepaid_units_for": "contract_renewal_refill", ' +
      ' "prepaid_units_expiry": "contract_term_expiry", ' +
      ' "refund_units_on_cancel": false, ' +
      ' "pricing_model": "Per unit", ' +
      ' "list_price": "0.175", ' +
      ' "billing_period": "Monthly", ' +
      ' "billing_period_start_alignment": "Service Start Date", ' +
      ' "billing_day": "Align with Billing Period", ' +
      ' "unit_of_measure": "", ' +
      ' "included_units": "0.0", ' +
      ' "custom_fields": { ' +
      ' "salesforce_id": "" ' +
      ' }, ' +
      ' "salesforce_id": "" ' +
      ' } ' +
      ' ], ' +
      ' "public_url": "", ' +
      ' "custom_fields": {} ' +
      ' } ' +
      ' ], ' +
      ' "public_url": "", ' +
      ' "custom_fields": {} ' +
      ' } ' +
      ' ], ' +
      ' "total_count": 35 ' +
      ' } ';
  }
}