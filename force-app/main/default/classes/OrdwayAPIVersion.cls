/**
 * @File Name          : OrdwayAPIVersion.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 12-11-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    7/6/2020        Initial Version
 **/
@RestResource(urlMapping='/ordway/version')
global with sharing class OrdwayAPIVersion {
  @HttpGet
  global static Map<String, String> doGet() {
    return packageVersion();
  }

  public static Map<String, String> packageVersion() {
    return new Map<String, String>{'Version' => '3.0.0'};
  }
}