/**
 * @File Name          : ApplicationLog_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    12/31/2019   Ordway Labs     Initial Version
 **/
@isTest
private class ApplicationLog_Test {
  @isTest
  static void ApplicationLog_LogFine() {
    Test.startTest();
    LoadLeanSettings__c llSetting = TestObjectFactory.createLoadLeanSetting();
    insert llSetting;

    ApplicationLog.StartLoggingContext(
      'ApplicationLog_Test',
      'ApplicationLog_LogFine'
    );
    ApplicationLog.fine('Message => Fine');
    ApplicationLog.FinishLoggingContext();
    Test.stopTest();
    System.assert([SELECT Id FROM ApplicationLog__c].size() > 0);
  }

  @isTest
  static void ApplicationLog_LogDebug() {
    Test.startTest();
    LoadLeanSettings__c llSetting = TestObjectFactory.createLoadLeanSetting();
    insert llSetting;

    ApplicationLog.StartLoggingContext(
      'ApplicationLog_Test',
      'ApplicationLog_LogDebug'
    );
    ApplicationLog.Debug('Message => Debug');
    ApplicationLog.FinishLoggingContext();
    Test.stopTest();
    System.assert([SELECT Id FROM ApplicationLog__c].size() > 0);
  }

  @isTest
  static void ApplicationLog_LogInfo() {
    Test.startTest();
    LoadLeanSettings__c llSetting = TestObjectFactory.createLoadLeanSetting();
    insert llSetting;

    ApplicationLog.StartLoggingContext(
      'ApplicationLog_Test',
      'ApplicationLog_LogInfo'
    );
    ApplicationLog.Info('Message => Info');
    ApplicationLog.FinishLoggingContext();
    Test.stopTest();
    System.assert([SELECT Id FROM ApplicationLog__c].size() > 0);
  }

  @isTest
  static void ApplicationLog_LogWarn() {
    Test.startTest();
    LoadLeanSettings__c llSetting = TestObjectFactory.createLoadLeanSetting();
    insert llSetting;

    ApplicationLog.StartLoggingContext(
      'ApplicationLog_Test',
      'ApplicationLog_LogWarn'
    );
    ApplicationLog.Warn('Message => Warn');
    ApplicationLog.FinishLoggingContext();
    Test.stopTest();
    System.assert([SELECT Id FROM ApplicationLog__c].size() > 0);
  }

  @isTest
  static void ApplicationLog_LogError() {
    Test.startTest();
    ApplicationLog.StartLoggingContext(
      'ApplicationLog_Test',
      'ApplicationLog_LogError'
    );
    ApplicationLog.Error('Message => Error');
    ApplicationLog.FinishLoggingContext();
    Test.stopTest();
    System.assert([SELECT Id FROM ApplicationLog__c].size() > 0);
  }

  @isTest
  static void ApplicationLog_LogErrorExceptionEndContext() {
    Test.startTest();
    Contact newContact = TestObjectFactory.createContact();
    newContact.Email = null;
    Exception ex;
    try {
      insert newContact;
    } catch (Exception thisException) {
      ex = thisException;
    }

    ApplicationLog.StartLoggingContext(
      'ApplicationLog_Test',
      'ApplicationLog_LogError'
    );
    ApplicationLog.Error('Message => Error', ex, true);
    ApplicationLog.FinishLoggingContext();
    Test.stopTest();
    System.assert([SELECT Id FROM ApplicationLog__c].size() > 0);
  }

  @isTest
  static void ApplicationLog_LogException() {
    Test.startTest();
    LoadLeanSettings__c llSetting = TestObjectFactory.createLoadLeanSetting();
    insert llSetting;

    Contact newContact = TestObjectFactory.createContact();
    newContact.Email = null;

    ApplicationLog.StartLoggingContext(
      'ApplicationLog_Test',
      'ApplicationLog_LogError'
    );
    try {
      insert newContact;
    } catch (Exception thisException) {
      ApplicationLog.Error(thisException);
    }
    ApplicationLog.FinishLoggingContext();

    Test.stopTest();
    System.assert([SELECT Id FROM ApplicationLog__c].size() > 0);
  }

  @isTest
  static void ApplicationLog_LogRecordIdErrorException() {
    Test.startTest();
    Contact newContact = TestObjectFactory.createContact();
    newContact.Email = null;
    Exception ex;
    try {
      insert newContact;
    } catch (Exception thisException) {
      ex = thisException;
    }

    ApplicationLog.StartLoggingContext(
      'ApplicationLog_Test',
      'ApplicationLog_LogError'
    );
    ApplicationLog.addRecordId(newContact.Id);
    ApplicationLog.Error('Message => Error', ex);
    ApplicationLog.FinishLoggingContext();
    Test.stopTest();
    System.assert([SELECT Id FROM ApplicationLog__c].size() > 0);
    System.assertEquals(
      [SELECT Id, SObjectType__c FROM ApplicationLog__c]
      .SObjectType__c,
      'Contact'
    );

    System.assert(ErrorLogController.getErrorLog(newContact.Id, 5).size() > 0);

    User userWithNoAccess = TestObjectFactory.createUser('Standard User');

    System.runAs(userWithNoAccess) {
      System.assert(
        ErrorLogController.getErrorLog(newContact.Id, 5) == null,
        'This user dont have access to Error Log Object'
      );
    }
  }
}