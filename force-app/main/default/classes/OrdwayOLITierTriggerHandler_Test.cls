/**
 * @File Name          : OrdwayOLITierTriggerHandler_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-04-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    09/04/2021        Initial Version
 **/
@isTest
private class OrdwayOLITierTriggerHandler_Test {
  @isTest
  private static void opportunityLineItemTierDeleteException() {

    Contact thisOrdwayContact = TestObjectFactory.createContact();
    insert thisOrdwayContact;
    
    Opportunity thisOrdwayOpportunity       = TestObjectFactory.createOpportunity();
    thisOrdwayOpportunity.BillingContact__c = thisOrdwayContact.Id;
    insert thisOrdwayOpportunity;
    
    Test.startTest();
    OpportunityLineItem thisOrdwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem();
    thisOrdwayOpportunityLineItem.OpportunityId       = thisOrdwayOpportunity.Id;
    insert thisOrdwayOpportunityLineItem; 

    OpportunityLineItemTier__c thisOLITier = TestObjectFactory.createOpportunityLineItemTier();
    thisOLITier.StartingUnit__c            = 123;
    thisOLITier.OpportunityProduct__c      = thisOrdwayOpportunityLineItem.Id; 
    insert thisOLITier;

    try{
      delete thisOLITier;
     } catch (Exception ex){
       system.assertNotEquals(null, ex.getMessage());
    }
    Test.stopTest();
    System.assertEquals(1, [SELECT Id FROM OpportunityLineItemTier__c].SIZE());
  }
}