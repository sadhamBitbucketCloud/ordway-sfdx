/**
 * @File Name          : SObjectFieldMappingController_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    1/1/2020   Ordway Labs     Initial Version
 **/
@isTest
private class SObjectFieldMappingController_Test {
  @isTest
  private static void Test_MethodOne() {
    Test.startTest();
    SObjectFieldMappingController.getObjectListWrapper();
    SObjectFieldMappingController.objectFieldListWrapper thisWrapper = SObjectFieldMappingController.getObjectFieldsWrapperList(
      'Opportunity',
      'OpportunityLineItem'
    );
    System.assertEquals(
      false,
      thisWrapper.isExistingMapping,
      'Should return false'
    );
    SObjectFieldMappingController.createUpdateOrdwayFieldMappingMetadata(
      'Opportunity',
      'OrdwayContract__c',
      '',
      'Description'
    );
    SObjectFieldMappingController.generateNewTargetObjectFieldWrapperList(
      'OrdwayContract__c',
      new List<SObjectFieldMappingController.targetObjectFieldWrapper>()
    );
    Test.stopTest();
  }

  @isTest
  private static void get_Existing_Object_FieldMapping_Test() {
    String opportunity_OrdwayContractFieldMapping =
      '[{"fieldAPIName":"Account__c","fieldDataTypeKey":"Lookup(Account)","fieldLabel":"Account","sourceFieldAPIName":"AccountId"},' +
      '{"fieldAPIName":"AutoRenew__c","fieldDataTypeKey":"Checkbox","fieldLabel":"Auto Renew","sourceFieldAPIName":"IsPrivate"},' +
      '{"fieldAPIName":"BillingContact__c","fieldDataTypeKey":"Lookup(Contact)","fieldLabel":"Billing Contact","sourceFieldAPIName":"BillingContact__c"},' +
      '{"fieldAPIName":"BillingStartDate__c","fieldDataTypeKey":"Date","fieldLabel":"Billing Start Date","sourceFieldAPIName":"BillingStartDate__c"},' +
      '{"fieldAPIName":"ContractEffectiveDate__c","fieldDataTypeKey":"Date","fieldLabel":"Contract Effective Date","sourceFieldAPIName":"ContractEffectiveDate__c"},' +
      '{"fieldAPIName":"ContractTerm__c","fieldDataTypeKey":"Picklist","fieldLabel":"Contract Term","sourceFieldAPIName":"ContractTerm__c"},' +
      '{"fieldAPIName":"OrdwaySubscriptionID__c","fieldDataTypeKey":"Text","fieldLabel":"Ordway Subscription ID","sourceFieldAPIName":"OrdwaySubscriptionID__c"},' +
      '{"fieldAPIName":"RenewalTerm__c","fieldDataTypeKey":"Picklist","fieldLabel":"Renewal Term","sourceFieldAPIName":"RenewalTerm__c"},' +
      '{"fieldAPIName":"ServiceStartDate__c","fieldDataTypeKey":"Date","fieldLabel":"Service Start Date","sourceFieldAPIName":"ServiceStartDate__c"}]';

    MetadataService.fieldMappingMetadataMap.put(
      'Opportunity_OrdwayContract',
      TestObjectFactory.createObjectFieldMappingMetadata(
        'Opportunity_OrdwayContract',
        'Opportunity',
        'OrdwayContract__c',
        opportunity_OrdwayContractFieldMapping
      )
    );
    Test.startTest();
    SObjectFieldMappingController.objectFieldListWrapper thisWrapper = SObjectFieldMappingController.getObjectFieldsWrapperList(
      'Opportunity',
      'OrdwayContract__c'
    );
    System.assertEquals(
      true,
      thisWrapper.isExistingMapping,
      'Existing Object Mapping should return true'
    );
    Test.stopTest();
  }

  @isTest
  private static void testDeployOrdwayObjectFieldMappingMetaData() {
    Metadata.DeployCallback callback = new DeployOrdwayObjectFieldMappingMetaData();
    Metadata.DeployResult result = new Metadata.DeployResult();
    result.numberComponentErrors = 1;
    Metadata.DeployCallbackContext context = new Metadata.DeployCallbackContext();
    System.assertEquals(
      1,
      result.numberComponentErrors,
      'Sample assert since nothing can be use for assertion'
    );
    callback.handleResult(result, context);
  }
}