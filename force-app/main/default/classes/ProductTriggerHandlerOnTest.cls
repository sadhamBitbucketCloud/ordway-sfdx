@isTest
private class ProductTriggerHandlerOnTest {
 @isTest
 private static void createOrdwayProductOnInsert(){
  Product2 thisProduct2 = TestObjectFactory.createProduct();
  thisProduct2.OrdwayProduct__c = true;
  insert thisProduct2;
  System.assertEquals(1, [Select Id FROM OrdwayProduct__c].size(), 'Should create Ordway Product');
 }
 
 @isTest
 private static void createOrdwayProductOnInsert_Negative(){
  try{
  Product2 thisProduct2 = TestObjectFactory.createProduct();
  thisProduct2.OrdwayProduct__c = false;
  insert thisProduct2;
  } catch (exception ex){
      System.assertEquals(null, ex.getMessage());
    }
 }    
}