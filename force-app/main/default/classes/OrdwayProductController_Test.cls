@isTest
private class OrdwayProductController_Test {
  @isTest  
    private static void syncToOrdway() {
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
      thisOrdwayProduct.OrdwayProductID__c = 'P-00071';
      insert thisOrdwayProduct;

      ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
      insert thisApplicationsetting;

      Test.startTest();
      Integer RESPONSECODE = 200;
      String RESPONSEBODY = ' { "attributes": { '
                                  +' "type": "OrdwayLabs__OrdwayProduct__c", '
                                  +' "url": "/services/data/v51.0/sobjects/OrdwayLabs__OrdwayProduct__c/a0I1F000005UmslUAC" '
                              +' }, '
                              +' "Id": "ORDWAYPRODUCTID", '
                              +' "IsDeleted": false, '
                              +' "Name": "Apr 5 1432 v1", '
                              +' "LastModifiedDate": "2021-04-06T07:22:28.000+0000", '
                              +' "OrdwayLabs__OrdwayProductID__c": "P-00075", '
                              +' "OrdwayLabs__Product2Id__c": "01t1F000008blAsQAI", '
                              +' "OrdwayLabs__Taxable__c": false, '
                              +' "OrdwayLabs__Active__c": true, '
                              +' "OrdwayLabs__Description__c": "Product Description - Demo Product Created in Salesforce" '
                          +' } ';
      Test.setMock(HttpCalloutMock.class, new OrdwayService_Mock(RESPONSECODE, 
                              RESPONSEBODY.replace('ORDWAYPRODUCTID', String.valueOf(thisOrdwayProduct.Id))));
      
      try {
        OrdwayProductController.syncToOrdway(thisOrdwayProduct.Id);
      }
      catch(Exception e) {

      }
      
      Test.stopTest();
      // OrdwayProduct__c oProductToAssert = [SELECT Id, Name, OrdwayProductID__c FROM OrdwayProduct__c];
      // system.assertEquals('Apr 5 1432 v1', oProductToAssert.Name, 'should return same name as response');
      // system.assertEquals('P-00075', oProductToAssert.OrdwayProductID__c, 'should return same OrdwayProductID as response');
    }

    @isTest  
    private static void syncToOrdwayWithoutProductId() {
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
      insert thisOrdwayProduct;

      ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
      insert thisApplicationsetting;

      Test.startTest();
      Integer RESPONSECODE = 200;
      String RESPONSEBODY = ' { "attributes": { '
                                  +' "type": "OrdwayLabs__OrdwayProduct__c", '
                                  +' "url": "/services/data/v51.0/sobjects/OrdwayLabs__OrdwayProduct__c/a0I1F000005UmslUAC" '
                              +' }, '
                              +' "Id": "ORDWAYPRODUCTID", '
                              +' "IsDeleted": false, '
                              +' "Name": "Apr 5 1432 v1", '
                              +' "LastModifiedDate": "2021-04-06T07:22:28.000+0000", '
                              +' "OrdwayLabs__Product2Id__c": "01t1F000008blAsQAI", '
                              +' "OrdwayLabs__Taxable__c": false, '
                              +' "OrdwayLabs__Active__c": true, '
                              +' "OrdwayLabs__Description__c": "Product Description - Demo Product Created in Salesforce" '
                          +' } ';
      Test.setMock(HttpCalloutMock.class, new OrdwayService_Mock(RESPONSECODE, 
                              RESPONSEBODY.replace('ORDWAYPRODUCTID', String.valueOf(thisOrdwayProduct.Id))));

      try {
        OrdwayProductController.syncToOrdway(thisOrdwayProduct.Id);
      }
      catch(Exception e) {

      }
      Test.stopTest();
      // OrdwayProduct__c oProductToAssert = [SELECT Id, Name, OrdwayProductID__c FROM OrdwayProduct__c];
      // system.assertEquals('Apr 5 1432 v1', oProductToAssert.Name, 'should return same name as response');
      // system.assertEquals(null ,oProductToAssert.OrdwayProductID__c, 'should return same OrdwayProductID as response');
    }

    @isTest  
    private static void syncToOrdway_Negative() {
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
      thisOrdwayProduct.OrdwayProductID__c = 'P-00071';
      insert thisOrdwayProduct;

      ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
      insert thisApplicationsetting;

      Test.startTest();
      Integer RESPONSECODE = 200;
      String RESPONSEBODY = ' { "attributes": { '
                                  +' "type": "OrdwayLabs__OrdwayProduct__c", '
                                  +' "url": "/services/data/v51.0/sobjects/OrdwayLabs__OrdwayProduct__c/a0I1F000005UmslUAC" '
                              +' }, '
                              +' "Id": "ORDWAYPRODUCTID", '
                              +' "IsDeleted": false, '
                              +' "Name": "Apr 5 1432 v1", '
                              +' "LastModifiedDate": "2021-04-06T07:22:28.000+0000", '
                              +' "OrdwayLabs__OrdwayProductID__c": "P-00075", '
                              +' "OrdwayLabs__Product2Id__c": "01t1F000008blAsQAI", '
                              +' "OrdwayLabs__Taxable__c": false, '
                              +' "OrdwayLabs__Active__c": true, '
                              +' "OrdwayLabs__Description__c": "Product Description - Demo Product Created in Salesforce" '
                          +' } ';
      Test.setMock(HttpCalloutMock.class, new OrdwayService_Mock(RESPONSECODE, 
                              RESPONSEBODY.replace('ORDWAYPRODUCTID', String.valueOf(thisOrdwayProduct.Id))));
      try {
        OrdwayProductController.syncToOrdway('INVALIDID');
      } catch (Exception ex) {
        system.assertNotEquals(null, ex.getMessage());
      }      
      Test.stopTest();
    }

    @isTest  
    private static void syncToOrdway_400() {
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
      thisOrdwayProduct.OrdwayProductID__c = 'P-00071';
      insert thisOrdwayProduct;

      ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
      insert thisApplicationsetting;

      Test.startTest();
          Integer RESPONSECODE = 400;
          String RESPONSEBODY = '{ "errors":{"details": "Bad Request"}}';
          Test.setMock(
            HttpCalloutMock.class,
            new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
          );
          try {
            OrdwayProductController.syncToOrdway(thisOrdwayProduct.Id);
          } catch (Exception ex) {
            system.assertNotEquals(null, ex.getMessage());
          }

      Test.stopTest();
    }

    @isTest  
    private static void syncToOrdway_500() {
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
      thisOrdwayProduct.OrdwayProductID__c = 'P-00071';
      insert thisOrdwayProduct;

      ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
      insert thisApplicationsetting;

      Test.startTest();
          Integer RESPONSECODE = 500;
          String RESPONSEBODY = '{ "errors":{"details": "Internal Server Error"}}';
          Test.setMock(
            HttpCalloutMock.class,
            new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
          );
          try {
            OrdwayProductController.syncToOrdway(thisOrdwayProduct.Id);
          } catch (Exception ex) {
            system.assertNotEquals(null, ex.getMessage());
          }

      Test.stopTest();
    }

    @isTest  
    private static void getInitialWrapper() {

      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
      thisOrdwayProduct.OrdwayProductID__c = 'P-00071';
      insert thisOrdwayProduct;

      ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
      insert thisApplicationsetting;

      ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
      newSetting.EnableMultiEntity__c = true;
      insert newSetting;
      

      Test.startTest();
      OrdwayProductController.InitialWrapper thisInitialWrapper = new OrdwayProductController.InitialWrapper();
      thisInitialWrapper = OrdwayProductController.getInitialWrapper(thisOrdwayProduct.Id);
      Test.stopTest();
      
      System.assertEquals('P-00071', thisInitialWrapper.thisOrdwayProduct.OrdwayProductID__c, 'Should return P-00071');
      System.assertNotEquals(null, thisInitialWrapper.namedCredentials);
    }

    @isTest  
    private static void getInitialWrapper_Negative() {

      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
      thisOrdwayProduct.OrdwayProductID__c = 'P-00071';
      insert thisOrdwayProduct;

      ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
      insert thisApplicationsetting;

      Test.startTest();
      try{
      OrdwayProductController.InitialWrapper thisInitialWrapper = new OrdwayProductController.InitialWrapper();
      thisInitialWrapper            = OrdwayProductController.getInitialWrapper(null);
      } catch (Exception ex) {
        system.assertNotEquals(null, ex.getMessage());
      } 
      Test.stopTest();
    }
}