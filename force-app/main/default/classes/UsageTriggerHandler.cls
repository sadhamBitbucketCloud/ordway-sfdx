public with sharing class UsageTriggerHandler extends TriggerHandler.TriggerHandlerBase {

  public override void beforeInsert(List<SObject> lstNewSObjs) {

    for (OrdwayUsage__c thisUsage : (List<OrdwayUsage__c>) lstNewSObjs) {
      if(String.isNotBlank(thisUsage.SubscriptionChargeLineID__c) 
        && OrdwayHelper.isValidId(thisUsage.SubscriptionChargeLineID__c) ) {
          thisUsage.OrdwayContractLineItem__c = thisUsage.SubscriptionChargeLineID__c;
        }
    }
  }
}