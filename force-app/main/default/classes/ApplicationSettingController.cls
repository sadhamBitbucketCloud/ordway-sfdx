/**
 * @File Name          : ApplicationSettingController.cls
 * @Description        : Application Setting Controller Class
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 01-09-2021
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    9/29/2019   Ordway Labs     Initial Version
 **/
public with sharing class ApplicationSettingController {
  /**
   * @description Method to get Application Custom Setting
   **/
  public static ApplicationSetting__c applicationSetting {
    get {
      if (applicationSetting == null) {
        applicationSetting = ApplicationSetting__c.getValues(
          OrdwayHelper.ORDWAY_APPLICATION_SETTING
        );
      }
      return applicationSetting;
    }
    private set;
  }

  /**
   * @description Method to update Application Custom Setting
   * @param email Email Value
   * @param company Company value
   * @param token Ordway connection Token
   * @param environment Type of Enviroment
   **/
  @AuraEnabled
  public static void updateApplicationSetting(
    String email,
    String company,
    String token,
    String environment
  ) {
    
      ApplicationSetting__c applicationSetting = new ApplicationSetting__c();
      applicationSetting.Name = OrdwayHelper.ORDWAY_APPLICATION_SETTING;
      applicationSetting.Email__c = email;
      applicationSetting.Company__c = company;
      applicationSetting.Token__c = token;
      applicationSetting.Environment__c = environment;
      applicationSetting.LastTokenValidationDate__c = System.now();
      applicationSetting.LastModifiedBy__c = UserInfo.getUserId();

    try {
        upsert applicationSetting Name;
    } catch (Exception thisException) {
      CustomExceptionData exceptionData = new CustomExceptionData(
        thisException.getTypeName(),
        thisException.getMessage()
      );
      AuraHandledException aE = new AuraHandledException(
        JSON.serialize(exceptionData)
      );
      aE.setMessage(JSON.serialize(exceptionData));
      throw aE;
    }
  }

  /**
   * @description Method to disconnect the current Ordway Connection
   **/
  @AuraEnabled
  public static void disconnect() {
    applicationSetting.Email__c = null;
    applicationSetting.Company__c = null;
    applicationSetting.Token__c = null;
    applicationSetting.LastModifiedBy__c = UserInfo.getUserId();
    applicationSetting.LastTokenValidationDate__c = null;
    try {
        applicationSetting.Name = OrdwayHelper.ORDWAY_APPLICATION_SETTING;
        upsert applicationSetting Name;
    } catch (Exception thisException) {
      CustomExceptionData exceptionData = new CustomExceptionData(
        thisException.getTypeName(),
        thisException.getMessage()
      );
      AuraHandledException aE = new AuraHandledException(
        JSON.serialize(exceptionData)
      );
      aE.setMessage(JSON.serialize(exceptionData));
      throw aE;
    }
  }

  /**
   * @description Method to validate the Ordway Connection
   * @param email Email Value
   * @param company Company value
   * @param token Ordway connection Token
   * @param environment Type of Enviroment
   * @param isTestConnection Boolen variable to indicate whether the Connection is test connection
   * @return string Message based on Response
   **/
  @AuraEnabled
  public static string validateToken(
    String email,
    String company,
    String token,
    String environment,
    Boolean isTestConnection
  ) {
    ApplicationLog.StartLoggingContext(
      'ApplicationSettingController',
      'validateToken()'
    );
    HttpResponse response = OrdwayService.validateToken(
      email,
      company,
      token,
      environment
    );
    if (response.getStatusCode() == 200) {
      if (isTestConnection) {
        ApplicationLog.FinishLoggingContext();
        return 'You are connected successfully';
      } else {
        ApplicationLog.FinishLoggingContext();
        updateApplicationSetting(email, company, token, environment);
        return 'You are connected successfully';
      }
    } else {
      ApplicationLog.FinishLoggingContext();
      return response.getBody();
    }
  }

  /**
   * @description Initial Wrapper Data for Ordway connection Component
   * @return InitialWrapper
   **/
  @AuraEnabled
  public static InitialWrapper getApplicationSetting() {
    InitialWrapper thisInitialWrapper = new InitialWrapper();
    thisInitialWrapper.applicationSetting = applicationSetting;
    thisInitialWrapper.isMultiEntityPermissionAssigned = isMultiEntityPermissionSetAssignedToUser();
    return thisInitialWrapper;
  }

  /**
   * @description Method to Check whether the Ordway Connection Enviroment is Production
   * @return true or false
   **/
  public static Boolean isConnectedToOrdwayProduction() {
    return applicationSetting != null &&
      applicationSetting.Environment__c == OrdwayHelper.ORDWAY_PRODUCTION
      ? true
      : false;
  }

  /**
   * @description Method to Ordway Connection Enviroment
   * @return Ordway Connection Enviroment
   **/
  public static String getInstance() {
    ApplicationSetting__c applicationSetting = ApplicationSetting__c.getValues(OrdwayHelper.ORDWAY_APPLICATION_SETTING);
    return applicationSetting.Environment__c;
  }

  /**
   * @description Method to check whether Ordway CPQ User or Ordway Admin User permission set is assigned to the current user
   * @return Boolean true or false
   **/
  public static Boolean isOrdwayPermissionAssignedToUser() {
    List<PermissionSetAssignment> ordwayPermissionSet = new List<PermissionSetAssignment>();
      ordwayPermissionSet = [
        SELECT Id, Assignee.Name, PermissionSet.Name, PermissionSet.Label
        FROM PermissionSetAssignment
        WHERE
          AssigneeId = :UserInfo.getUserId()
          AND (PermissionSet.Name = :OrdwayHelper.ORDWAY_CPQ_ADMIN
          OR PermissionSet.Name = :OrdwayHelper.ORDWAY_CPQ_USER)
      ];
    return ordwayPermissionSet.size() > 0;
  }

  /**
   * @description Method to Check whether Ordway Multi Entity Permission set is Assigned to the current user
   * @return true or false
   */
  public static Boolean isMultiEntityPermissionSetAssignedToUser() {
    List<PermissionSetAssignment> ordwayPermissionSet = new List<PermissionSetAssignment>();
      ordwayPermissionSet = [
        SELECT Id, Assignee.Name, PermissionSet.Name, PermissionSet.Label
        FROM PermissionSetAssignment
        WHERE
          AssigneeId = :UserInfo.getUserId()
          AND PermissionSet.Name = :OrdwayHelper.ORDWAY_CPQ_MULTI_ENTITY
      ];
    return ordwayPermissionSet.size() > 0;
  }

  /**
   * @description Method to get all Named Credentials related to Ordway
   * @return List<NamedCredential>
   **/
  @AuraEnabled
  public static List<NamedCredential> getOrdwayNamedCredential() {
    List<NamedCredential> namedCredentialList = new List<NamedCredential>();

    namedCredentialList = new List<NamedCredential>(
      [
        SELECT Id, DeveloperName, MasterLabel
        FROM NamedCredential
        WHERE DeveloperName LIKE 'Ordway%'
      ]
    );
    
    return namedCredentialList;
  }

  /**
   * @description Initial Wrapper Data for Ordway connection Component
   **/
  public class InitialWrapper {
    @AuraEnabled
    public ApplicationSetting__c applicationSetting;
    @AuraEnabled
    public Boolean isMultiEntityPermissionAssigned;
  }
}