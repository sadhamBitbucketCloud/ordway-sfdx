/**
 * @File Name          : OpportunityTriggerHandler_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-30-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    1/4/2020   Ordway Labs     Initial Version
 **/
@isTest
private class OpportunityTriggerHandler_Test {
  @isTest
  private static void OpportunityTriggerTest() {
    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    insert newSetting;

    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;

    Contact billingContact = TestObjectFactory.createContact();
    billingContact.AccountId = newAccount.Id;
    billingContact.FirstName = 'OrdwayLab';
    billingContact.Email = 'Ordway@test.com';
    insert billingContact;

    List<Opportunity> opportunityList = new List<Opportunity>();
    for (Integer i = 0; i < 201; i++) {
      Opportunity newOpportunity = TestObjectFactory.createOpportunity();
      opportunityList.add(newOpportunity);
    }

    Test.startTest();
    opportunityList[0].BillingContact__c = newContact.Id;
    opportunityList[1].BillingContact__c = newContact.Id;
    opportunityList[1].BillingCity__c = 'Manual City Updated By User';
    insert opportunityList;

    //Should return the Billing City from contact
    Opportunity insertedOpp = [
      SELECT Id, BillingCity__c
      FROM Opportunity
      WHERE BillingContact__c = :newContact.Id AND BillingCity__c LIKE 'Test%'
    ];
    System.assertEquals(
      'Test City',
      insertedOpp.BillingCity__c,
      'Billing city should be copied over from contact'
    );

    //Should return the Billing City entered by the user manually
    Opportunity thisOpp = [
      SELECT Id, BillingCity__c
      FROM Opportunity
      WHERE BillingContact__c = :newContact.Id AND BillingCity__c LIKE 'Manual%'
    ];
    System.assertEquals(
      'Manual City Updated By User',
      thisOpp.BillingCity__c,
      'Billing city should be NOT copied over from contact'
    );

    //Manually Overwriting the Address Should Not Overwrite the Address From Contact Record
    thisOpp.BillingCity__c = 'Update in Unit Test';
    update thisOpp;

    Opportunity updatedOpp = [
      SELECT Id, BillingCity__c
      FROM Opportunity
      WHERE Id = :thisOpp.Id
    ];
    System.assertEquals(
      'Update in Unit Test',
      updatedOpp.BillingCity__c,
      'Should return the Billing City the user updated manually'
    );
    Test.stopTest();
  }

  /*@isTest
  static void Opportunity_OrdwayContract_SyncTest() {
    Test.startTest();
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    insert newSetting;

    OrdwayContract__c ordwayContract = TestObjectFactory.createOrdwayContract();
    insert ordwayContract;

    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;

    Contact billingContact = TestObjectFactory.createContact();
    billingContact.AccountId = newAccount.Id;
    billingContact.FirstName = 'OrdwayLab';
    billingContact.Email = 'Ordway@test.com';
    insert billingContact;

    List<Opportunity> opportunityList = new List<Opportunity>();
    for (Integer i = 0; i < 50; i++) {
      Opportunity newOpportunity = TestObjectFactory.createOpportunity();
      newOpportunity.BillingContact__c = billingContact.Id;
      opportunityList.add(newOpportunity);
    }

    insert opportunityList;

    Product2 newProduct = TestObjectFactory.createProduct();
    insert newProduct;

    Pricebook2 newPricebook = TestObjectFactory.createPricebook();
    insert newPricebook;

    PricebookEntry newPBEStandard = new PricebookEntry();
    newPBEStandard.Product2Id = newProduct.Id;
    newPBEStandard.Pricebook2Id = Test.getStandardPricebookId();

    PricebookEntry newPBEStd = TestObjectFactory.createPriceBookEntry(
      newPBEStandard
    );
    insert newPBEStd;

    PricebookEntry newPBEntry = new PricebookEntry();
    newPBEntry.Product2Id = newProduct.Id;
    newPBEntry.Pricebook2Id = newPricebook.Id;

    PricebookEntry newPBE = TestObjectFactory.createPriceBookEntry(newPBEntry);
    insert newPBE;

    
    Test.setMock(HttpCalloutMock.class, new MockAddPlan_Test(newProduct.Id));
    Database.executebatch(new GetOrdwayPlan_Batch(1, null));
    Test.stopTest();
    String oliListToInsert =
      '[{' +
      '  "IsDeleted": false,' +
      '  "Discount": "25",' +
      '  "Description": "",' +
      '  "OrdwayLabs__PricingModel__c": "Volume",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "Quantity": "75",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00006",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "[ {\n  \\"tier\\" : 1,\n  \\"starting_unit\\" : 0,\n  \\"ending_unit\\" : 20.0,\n  \\"price\\" : \\"5.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 2,\n  \\"starting_unit\\" : 21.0,\n  \\"ending_unit\\" : 50.0,\n  \\"price\\" : \\"2.5\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 3,\n  \\"starting_unit\\" : 51.0,\n  \\"ending_unit\\" : \\"\\",\n  \\"price\\" : \\"350.0\\",\n  \\"type\\" : \\"Flat fee\\"\n} ]",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 0,' +
      '  "OpportunityId": "testOpportunityId",' +
      '  "PricebookEntryId": "testPricebookEntryId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00006",' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Monthly"' +
      '}, {' +
      '  "IsDeleted": false,' +
      '  "Discount": "10",' +
      '  "Description": "",' +
      '  "OrdwayLabs__PricingModel__c": "Tiered",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "Quantity": "25",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00005",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "[ {\n  \\"tier\\" : 1,\n  \\"starting_unit\\" : 0,\n  \\"ending_unit\\" : 10.0,\n  \\"price\\" : \\"5.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 2,\n  \\"starting_unit\\" : 10.0,\n  \\"ending_unit\\" : 20.0,\n  \\"price\\" : \\"3.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 3,\n  \\"starting_unit\\" : 20.0,\n  \\"ending_unit\\" : \\"\\",\n  \\"price\\" : \\"50.0\\",\n  \\"type\\" : \\"Flat fee\\"\n} ]",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 0,' +
      '  "OpportunityId": "testOpportunityId",' +
      '  "PricebookEntryId": "testPricebookEntryId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00005",' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Monthly"' +
      '}, {' +
      '  "IsDeleted": false,' +
      '  "Discount": "5",' +
      '  "Description": "",' +
      '  "OrdwayLabs__PricingModel__c": "Per unit",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "Quantity": "10",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00004",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 500,' +
      '  "OpportunityId": "testOpportunityId",' +
      '  "PricebookEntryId": "testPricebookEntryId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00004",' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0 ,' +
      '  "OrdwayLabs__BillingPeriod__c": "Annually"' +
      '}]';

    oliListToInsert = oliListToInsert.replaceAll('\n', '').replaceAll('\r', '');
    oliListToInsert = oliListToInsert.replace(
      'testOpportunityId',
      String.valueOf(opportunityList[0].Id)
    );
    oliListToInsert = oliListToInsert.replace(
      'testPricebookEntryId',
      String.valueOf(newPBE.Id)
    );
    AddEditPlanOrdwayController.addPlanToOpportunity(oliListToInsert, '[]'); //Testing One Opportunity

    List<OpportunityLineItem> oppLineList = OpportunityLineItemService.getOpportunityLineItemMap(
        new Set<Id>{ opportunityList[0].Id }
      )
      .values();
    System.assertEquals(
      3,
      oppLineList.size(),
      'Should return 3 Opportunity Line Item'
    );

    for (OpportunityLineItem thisOLI : oppLineList) {
      if (thisOLI.OrdwayLabs__OrdwayChargeId__c == 'CHG-00004') {
        System.assertEquals(
          'Recurring',
          thisOLI.OrdwayLabs__ChargeType__c,
          'Should return Recurring as Charge Type'
        );
        System.assertEquals(
          'Per Unit',
          thisOLI.OrdwayLabs__PricingModel__c,
          'Should return Per Unit as Pricing Model'
        );
        System.assertEquals(
          'per_unit',
          thisOLI.OrdwayLabs__PricingType__c,
          'Should return Per Unit as Pricing Type'
        );
        System.assertEquals(
          'Annually',
          thisOLI.OrdwayLabs__BillingPeriod__c,
          'Should return Annually as Billing Period'
        );
        System.assertEquals(
          'PLN-00004',
          thisOLI.OrdwayLabs__OrdwayPlanId__c,
          'Should return the Plan Id mapped'
        );
        System.assertEquals(
          'P-00001',
          thisOLI.OrdwayLabs__OrdwayProductId__c,
          'Should return the Product Id configured in Ordway'
        );
        System.assertEquals(
          500.00,
          thisOLI.OrdwayLabs__OrdwayListPrice__c,
          'Before Discount'
        ); //Before Discount
        System.assertEquals(
          475.00,
          thisOLI.OrdwayLabs__OrdwayEffectivePrice__c,
          'After Discount'
        ); //After Discount
        System.assertEquals(
          5000.00,
          thisOLI.OrdwayLabs__OrdwayUndiscountedContractValue__c,
          'Before Discount - Undiscounted Contract Value'
        );
        System.assertEquals(
          4750.00,
          thisOLI.OrdwayLabs__OrdwayDiscountedContractValue__c,
          'After Discount - Discounted Contract Value'
        );
        System.assertEquals(
          10.00,
          thisOLI.Quantity,
          'Should return the quantity entered by the user'
        );
        System.assertEquals(
          5.00,
          thisOLI.Discount,
          'Should return the actual discount offered'
        );
        System.assertEquals(
          4750.00,
          thisOLI.TotalPrice,
          'Should return the total price - equals to discounted contract value'
        );
      }

      if (thisOLI.OrdwayLabs__OrdwayChargeId__c == 'CHG-00005') {
        System.assertEquals(
          'Recurring',
          thisOLI.OrdwayLabs__ChargeType__c,
          'Should return Recurring as Charge Type'
        );
        System.assertEquals(
          'Tiered',
          thisOLI.OrdwayLabs__PricingModel__c,
          'Should return Tiered as Pricing Model'
        );
        System.assertEquals(
          'flat_fee',
          thisOLI.OrdwayLabs__PricingType__c,
          'Should return Flat Fee as Pricing Type'
        );
        System.assertEquals(
          'Monthly',
          thisOLI.OrdwayLabs__BillingPeriod__c,
          'Should return Monthly as Billing Period'
        );
        System.assertEquals(
          'PLN-00005',
          thisOLI.OrdwayLabs__OrdwayPlanId__c,
          'Should return the Plan Id mapped'
        );
        System.assertEquals(
          'P-00001',
          thisOLI.OrdwayLabs__OrdwayProductId__c,
          'Should return the Product Id configured in Ordway'
        );
        System.assertEquals(
          130.00,
          thisOLI.OrdwayLabs__OrdwayListPrice__c,
          'Before Discount'
        ); //Before Discount
        System.assertEquals(
          117.00,
          thisOLI.OrdwayLabs__OrdwayEffectivePrice__c,
          'After Discount'
        ); //After Discount
        System.assertEquals(
          1560.00,
          thisOLI.OrdwayLabs__OrdwayUndiscountedContractValue__c,
          'Before Discount - Undiscounted Contract Value'
        );
        System.assertEquals(
          1404.00,
          thisOLI.OrdwayLabs__OrdwayDiscountedContractValue__c,
          'After Discount - Discounted Contract Value'
        );
        System.assertEquals(
          25.00,
          thisOLI.Quantity,
          'Should return the quantity entered by the user'
        );
        System.assertEquals(
          10.00,
          thisOLI.Discount,
          'Should return the actual discount offered'
        );
        System.assertEquals(
          1404.00,
          thisOLI.TotalPrice,
          'Should return the total price - equals to discounted contract value'
        );
      }

      if (thisOLI.OrdwayLabs__OrdwayChargeId__c == 'CHG-00006') {
        System.assertEquals(
          'Recurring',
          thisOLI.OrdwayLabs__ChargeType__c,
          'Should return Recurring as Charge Type'
        );
        System.assertEquals(
          'Volume',
          thisOLI.OrdwayLabs__PricingModel__c,
          'Should return Volume as Pricing Model'
        );
        System.assertEquals(
          'Monthly',
          thisOLI.OrdwayLabs__BillingPeriod__c,
          'Should return Monthly as Billing Period'
        );
        System.assertEquals(
          'PLN-00006',
          thisOLI.OrdwayLabs__OrdwayPlanId__c,
          'Should return the Plan Id mapped'
        );
        System.assertEquals(
          'P-00001',
          thisOLI.OrdwayLabs__OrdwayProductId__c,
          'Should return the Product Id configured in Ordway'
        );
        System.assertEquals(
          350.00,
          thisOLI.OrdwayLabs__OrdwayListPrice__c,
          'Before Discount'
        ); //Before Discount
        System.assertEquals(
          262.50,
          thisOLI.OrdwayLabs__OrdwayEffectivePrice__c,
          'After Discount'
        ); //After Discount
        System.assertEquals(
          4200.00,
          thisOLI.OrdwayLabs__OrdwayUndiscountedContractValue__c,
          'Before Discount - Undiscounted Contract Value'
        );
        System.assertEquals(
          3150.00,
          thisOLI.OrdwayLabs__OrdwayDiscountedContractValue__c,
          'After Discount - Discounted Contract Value'
        );
        System.assertEquals(
          75.00,
          thisOLI.Quantity,
          'Should return the quantity entered by the user'
        );
        System.assertEquals(
          25.00,
          thisOLI.Discount,
          'Should return the actual discount offered'
        );
        System.assertEquals(
          3150.00,
          thisOLI.TotalPrice,
          'Should return the total price - equals to discounted contract value'
        );
      }
    }

    opportunityList.remove(0);
    List<OpportunityLineItem> oppLineItemListToInsert = new List<OpportunityLineItem>();
    //Add Same Products To All Opportunity
    for (Opportunity tOpportunity : opportunityList) {
      for (OpportunityLineItem thisOLI : oppLineList) {
        OpportunityLineItem thisNewOLI = new OpportunityLineItem();
        thisNewOLI = thisOLI.clone();
        thisNewOLI.OpportunityId = tOpportunity.Id;
        thisNewOLI.TotalPrice = null;
        oppLineItemListToInsert.add(thisNewOLI);
      }
    }

    //Bulk Insert Opportunity Line Item
    insert oppLineItemListToInsert;

    OpportunityTriggerHandler.runOnce = true;
    List<Opportunity> oppToCloseWin = new List<Opportunity>();

    for (Opportunity thisOpportunity : opportunityList) {
      thisOpportunity.StageName = 'Closed Won';
      oppToCloseWin.add(thisOpportunity);
    }

    oppToCloseWin[0].OrdwayContract__c = ordwayContract.Id;
    oppToCloseWin[0].ContractTerm__c = '-1';
    OpportunityService.defaultRenewalTerm(oppToCloseWin[0]);
    system.assertEquals(
      null,
      oppToCloseWin[0].RenewalTerm__c,
      'Default renewal term if contract term is -1 is null'
    );
    oppToCloseWin[0].ContractTerm__c = '6';
    oppToCloseWin[0].BillingContact__c = newContact.Id;

    //Bulk Close Win Opportunity
    update oppToCloseWin;

    Opportunity closedWonOpportunity = [
      SELECT Id, StageName, IsSyncing__c, OrdwayContract__c
      FROM Opportunity
      WHERE Id = :opportunityList[0].Id
    ];
    System.assertEquals(
      'Closed Won',
      closedWonOpportunity.StageName,
      'Should return  Closed Won'
    );
    System.assertEquals(
      true,
      closedWonOpportunity.IsSyncing__c,
      'Successful Close Win Should Update Syncing to True'
    );
    System.assertNotEquals(null, closedWonOpportunity.OrdwayContract__c);

    OrdwayContract__c thisOrdwayContract = [
      SELECT Id, SyncedOpportunityId__c
      FROM OrdwayContract__c
      WHERE Id = :closedWonOpportunity.OrdwayContract__c
    ];
    System.assertEquals(
      closedWonOpportunity.Id,
      thisOrdwayContract.SyncedOpportunityId__c,
      'Should return  closed won Opportunity Id'
    );

    List<ContractLineItem__c> contractLineItemList = [
      SELECT Id
      FROM ContractLineItem__c
      WHERE OrdwayContract__c = :thisOrdwayContract.Id
    ];
    System.assertEquals(
      3,
      contractLineItemList.size(),
      'Should return 3 contract line items created from OLI'
    );

    List<ContractLineItem__c> contractLineItemListAll = [
      SELECT Id
      FROM ContractLineItem__c
    ];
    System.assertEquals(
      147,
      contractLineItemListAll.size(),
      'Should return all 147 contract line items created from all OLI'
    );

    List<OpportunityLineItem> oppLineItemList = [
      SELECT Id
      FROM OpportunityLineItem
      WHERE ContractLineItem__c != NULL
    ];
    System.assertEquals(
      147,
      oppLineItemList.size(),
      'All OLI should have contract line item Id populated'
    );

    Opportunity thisRenewalOpp = ChangeRenewalOpportunity.getOpportunity(
      String.valueOf(closedWonOpportunity.OrdwayContract__c),
      OrdwayHelper.ORDWAY_OPPORTUNITY_TYPE_RENEWAL
    );
    thisRenewalOpp.StageName = 'Prospecting';
    thisRenewalOpp.CloseDate = System.today();
    insert thisRenewalOpp;

    ChangeRenewalOpportunity.getOpportunity(
      String.valueOf(thisRenewalOpp.Id),
      OrdwayHelper.ORDWAY_OPPORTUNITY_TYPE_RENEWAL
    );

    Opportunity qRenewalOpp = [
      SELECT Id, HasOpportunityLineItem
      FROM Opportunity
      WHERE Id = :thisRenewalOpp.Id
    ];
    System.assertEquals(
      true,
      qRenewalOpp.HasOpportunityLineItem,
      'OLI added, so should return true'
    );

    //Navigating By Selecting An Open Opportunity
    AddEditPlanOrdwayController.getOrdwayOLI(
      String.valueOf(thisRenewalOpp.Id),
      String.valueOf(newPricebook.Id)
    );
  }*/

  @isTest
  static void Opportunity_Delete_Exception_Test() {
    List<Opportunity> opportunityListToInsert = new List<Opportunity>();
    List<OrdwayContract__c> OrdwayContractListToInsert = new List<OrdwayContract__c>();

    Opportunity newOpportunityWithSubId = TestObjectFactory.createOpportunity();
    newOpportunityWithSubId.OrdwaySubscriptionID__c = 'S-12344';
    newOpportunityWithSubId.OrdwayOpportunityType__c = OrdwayHelper.OPPORTUNITY_ORDWAY_NEW_BUSINESS_TYPE;
    opportunityListToInsert.add(newOpportunityWithSubId);

    Opportunity opportunityInSync = TestObjectFactory.createOpportunity();
    opportunityInSync.OrdwayOpportunityType__c = 'Renewal';

    opportunityListToInsert.add(opportunityInSync);
    insert opportunityListToInsert;

    OrdwayContract__c ordwayContract = TestObjectFactory.createOrdwayContract();
    ordwayContract.SyncedOpportunityId__c = newOpportunityWithSubId.Id;
    OrdwayContractListToInsert.add(ordwayContract);

    OrdwayContract__c syncedContract = TestObjectFactory.createOrdwayContract();
    syncedContract.SyncedOpportunityId__c = opportunityInSync.Id;
    syncedContract.OrdwaySubscriptionID__c = 'S-215';
    OrdwayContractListToInsert.add(syncedContract);

    insert OrdwayContractListToInsert;

    try {
      delete newOpportunityWithSubId;
    } catch (Exception ex) {
      system.assertEquals(
        true,
        ex.getMessage()
          .contains(System.Label.OrdwayLabs.Opportunity_Delete_Message),
        'Opportunity has got Ordway Subscription Id, then it should not be deleted'
      );
    }

    try {
      delete opportunityInSync;
    } catch (Exception ex) {
      system.assertEquals(
        true,
        ex.getMessage()
          .contains(System.Label.OrdwayLabs.Opportunity_Delete_Message),
        'Opportunity is in Sync with Contract, then it should not be deleted'
      );
    }
  }

  @isTest
  static void Opportunity_Delete_CustomPermission_Test() {
    Test.startTest();

    User thisUser = TestObjectFactory.createUser('System Administrator');
    insert thisUser;

    PermissionSet ps = new PermissionSet();
    ps.Name = 'Test';
    ps.Label = 'Test';
    insert ps;
    SetupEntityAccess thisSEA = new SetupEntityAccess();
    thisSEA.ParentId = ps.Id;
    thisSEA.SetupEntityId = [select Id from CustomPermission where DeveloperName = 'CanDeleteOpportunity'][0].Id;
    insert thisSEA;

    PermissionSetAssignment thisPSA = new PermissionSetAssignment();
    thisPSA.AssigneeId = thisUser.Id;
    thisPSA.PermissionSetId = ps.Id;
    insert thisPSA;
    System.runAs(thisUser){

      List<Opportunity> opportunityListToInsert = new List<Opportunity>();
      List<OrdwayContract__c> OrdwayContractListToInsert = new List<OrdwayContract__c>();

      Opportunity newOpportunityWithSubId = TestObjectFactory.createOpportunity();
      newOpportunityWithSubId.OrdwaySubscriptionID__c = 'S-12344';
      newOpportunityWithSubId.OrdwayOpportunityType__c = OrdwayHelper.OPPORTUNITY_ORDWAY_NEW_BUSINESS_TYPE;
      opportunityListToInsert.add(newOpportunityWithSubId);

      Opportunity opportunityInSync = TestObjectFactory.createOpportunity();
      opportunityInSync.OrdwayOpportunityType__c = 'Renewal';

      opportunityListToInsert.add(opportunityInSync);
      insert opportunityListToInsert;

      OrdwayContract__c ordwayContract = TestObjectFactory.createOrdwayContract();
      ordwayContract.SyncedOpportunityId__c = newOpportunityWithSubId.Id;
      OrdwayContractListToInsert.add(ordwayContract);

      OrdwayContract__c syncedContract = TestObjectFactory.createOrdwayContract();
      syncedContract.SyncedOpportunityId__c = opportunityInSync.Id;
      syncedContract.OrdwaySubscriptionID__c = 'S-215';
      OrdwayContractListToInsert.add(syncedContract);

      insert OrdwayContractListToInsert;

      //Insert Quote
      Quote__c newQuote = TestObjectFactory.createQuote();
      newQuote.Opportunity__c = newOpportunityWithSubId.Id;
      insert newQuote;

      delete newOpportunityWithSubId;
      delete opportunityInSync;
      Test.stopTest();
      system.assertEquals(0, [SELECT Id FROM Opportunity].size(), 'should delete all opportunity');
      system.assertEquals(0, [SELECT Id FROM Quote__c].size(), 'should delete all quotes');
    }
  }
}