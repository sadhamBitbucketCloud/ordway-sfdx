/**
 * @File Name          : GetOrdwayObject_Batch.cls
 * @Description        : Ordway Object Bulk Sync Batch
 * @Author             : 
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 12-11-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/9/2020        Initial Version
 **/

global with sharing class GetOrdwayObject_Batch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
  Integer pageNumber = 0;
  Integer nextPageNumber = 0;
  String objectName;
  String queryString;
  String xEntityId;
  List<BulkSyncConfiguration__c> thisBulkSyncRecords = new List<BulkSyncConfiguration__c>();

  //v2.5
  global GetOrdwayObject_Batch(
    Integer pNumber,
    String objectName,
    String qString,
    String xEntityId,
    List<BulkSyncConfiguration__c> thisBulkSyncRecords
  ) {
    this.pageNumber = pNumber;
    this.nextPageNumber = pNumber;
    this.objectName = objectName;
    this.queryString = qString;
    this.xEntityId = xEntityId;
    this.thisBulkSyncRecords = thisBulkSyncRecords;
  }

  /**
   * @description Batch class to retrieve all data of specified Type from Ordway
   * @param pNumber Page Number of response
   * @param objectName Object Name to be retrieved
   * @param qString Query String for the request
   * @param xEntityId Entity Id
   **/
  // > 2.0 (Multi-Entity Implementation)
  global GetOrdwayObject_Batch(
    Integer pNumber,
    String objectName,
    String qString,
    String xEntityId
  ) {
    this.pageNumber = pNumber;
    this.nextPageNumber = pNumber;
    this.objectName = objectName;
    this.queryString = qString;
    this.xEntityId = xEntityId;
  }

  // < v2.0
  global GetOrdwayObject_Batch(
    Integer pNumber,
    String objectName,
    String qString
  ) {
    this.pageNumber = pNumber;
    this.objectName = objectName;
    this.queryString = qString;
    this.xEntityId = xEntityId;
  }

  global List<sObject> start(Database.BatchableContext BC) {
    Plan__c thisPlan = new Plan__c();
    return new List<Plan__c>{ thisPlan };
  }

  global void execute(Database.BatchableContext BC, List<sObject> sObjectList) {
    pageNumber = OrdwayObjectController.getOrdwayObjects(
      pageNumber,
      objectName,
      queryString,
      xEntityId
    );
  }

  global void finish(Database.BatchableContext BC) {
    if (pageNumber != 0 && nextPageNumber != pageNumber) {
      Database.executeBatch(
        new GetOrdwayObject_Batch(
          pageNumber,
          objectName,
          queryString,
          xEntityId,
          thisBulkSyncRecords
        )
      );
    }
    else if(!thisBulkSyncRecords.isEmpty()) {
      this.thisBulkSyncRecords.remove(0);
      if(!thisBulkSyncRecords.isEmpty()) {
        Database.executeBatch(new GetOrdwayObject_Batch(0, thisBulkSyncRecords[0].Object__c, getFilterString(Integer.valueOf(thisBulkSyncRecords[0].Filter__c)), thisBulkSyncRecords[0].Entity__c, thisBulkSyncRecords)); 
      }
    }
    else if (objectName == 'Plan' && queryString == '') {
      objectName = null;
      Database.executeBatch(new GetOrdwayPlan_Batch(1, xEntityId));
    } 
  }

  public static String getFilterString(Integer filter){
      String dateFormatString = 'yyyy-MM-dd';
      DateTime updatedDate = System.today().addDays(-filter);
      String queryString = '&updated_date' + EncodingUtil.urlEncode('>', 'utf-8') +'=' +updatedDate.format(dateFormatString);
      return queryString;
    }
  }