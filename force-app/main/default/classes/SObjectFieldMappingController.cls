/**
 * @File Name          : SObjectFieldMappingController.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    9/11/2019   Ordway Labs     Initial Version
 **/
public class SObjectFieldMappingController {
  /**
   * @description Method to Get Opportunity Custom Setting
   * @return ContractPlanPickerSetting__c
   **/
  @AuraEnabled(cacheable=true)
  public static ContractPlanPickerSetting__c getContractPickerSettings() {
    try {
      return ContractPlanPickerSetting__c.getValues(
          OrdwayHelper.OPPORTUNITY_SETTING_NAME
        ) == null
        ? new ContractPlanPickerSetting__c()
        : ContractPlanPickerSetting__c.getValues(
            OrdwayHelper.OPPORTUNITY_SETTING_NAME
          );
    } catch (Exception e) {
      throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  /**
   * @description Method to get Object Definition
   * @return thisObjectListWrapper List of objectListWrapper
   */
  @AuraEnabled
  public static List<objectListWrapper> getObjectListWrapper() {
    try {
      List<objectListWrapper> thisObjectListWrapper = new List<objectListWrapper>();
      for (
        EntityDefinition thisEntity : SObjectFieldMappingHelper.getObjectDefinition()
      ) {
        thisObjectListWrapper.add(new objectListWrapper(thisEntity));
      }
      return thisObjectListWrapper;
    } catch (Exception e) {
      throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  /**
   * getObjectFieldsWrapperList : Method to get Field definition for Source Object and Target Object
   * @param  selectedSourceObjectName: Source Object API Name
   * @param  selectedTargetObjectName: Target Object API Name
   * @return thisObjectFieldListWrapper : objectFieldList Wrapper
   */
  @AuraEnabled
  public static objectFieldListWrapper getObjectFieldsWrapperList(
    String selectedSourceObjectName,
    String selectedTargetObjectName
  ) {
    try {
      objectFieldListWrapper thisObjectFieldListWrapper;
      Map<String, List<sourceObjectFieldWrapper>> sourceFieldDataTypeMap = new Map<String, List<sourceObjectFieldWrapper>>();
      List<targetObjectFieldWrapper> thisTargetObjectFieldWrapperList = new List<targetObjectFieldWrapper>();
      String metaDatakey = getUniqueMetadataKeyValue(
        selectedSourceObjectName +
        '_to_' +
        selectedTargetObjectName
      );
      if (
        MetadataService.fieldMappingMetadataMap == null ||
        MetadataService.fieldMappingMetadataMap.isEmpty() ||
        !MetadataService.fieldMappingMetadataMap.containsKey(metaDatakey) ||
        (MetadataService.fieldMappingMetadataMap.containsKey(metaDatakey) &&
        MetadataService.fieldMappingMetadataMap.get(metaDatakey)
          .FieldMappingJSON__c == null)
      ) {
        generateSourceObjectFieldWrapperList(
          selectedSourceObjectName,
          sourceFieldDataTypeMap
        );
        generateNewTargetObjectFieldWrapperList(
          selectedTargetObjectName,
          thisTargetObjectFieldWrapperList
        );
        thisObjectFieldListWrapper = new objectFieldListWrapper(
          thisTargetObjectFieldWrapperList,
          sourceFieldDataTypeMap,
          false,
          null
        );
      } else if (
        MetadataService.fieldMappingMetadataMap != null &&
        MetadataService.fieldMappingMetadataMap.containsKey(metaDatakey)
      ) {
        Map<String, targetObjectFieldWrapper> existingFieldMapping = getExistingMapping(
          MetadataService.fieldMappingMetadataMap.get(metaDatakey)
            .FieldMappingJSON__c
        );
        generateSourceObjectFieldWrapperList(
          selectedSourceObjectName,
          sourceFieldDataTypeMap
        );
        generateExistingTargetObjectFieldWrapperList(
          selectedTargetObjectName,
          thisTargetObjectFieldWrapperList,
          existingFieldMapping
        );
        thisObjectFieldListWrapper = new objectFieldListWrapper(
          thisTargetObjectFieldWrapperList,
          sourceFieldDataTypeMap,
          true,
          MetadataService.fieldMappingMetadataMap.get(metaDatakey)
            .WhereIsThisUsed__c
        );
      }
      return thisObjectFieldListWrapper;
    } catch (Exception e) {
      throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  /**
   * @description Method to populate Source object Wrapper
   * @param  sourceObjectName Source Object API Name
   * @param  dataTypeMap Source field Datatype to sourceObjectFieldWrapper Map
   */
  public static void generateSourceObjectFieldWrapperList(
    String sourceObjectName,
    Map<String, List<sourceObjectFieldWrapper>> dataTypeMap
  ) {
    for (
      FieldDefinition thisFieldDefinition : SObjectFieldMappingHelper.getFieldDefinition(
        sourceObjectName
      )
    ) {
      if (thisFieldDefinition.DataType.contains('Lookup')) {
        populateDataTypeMap(
          thisFieldDefinition,
          thisFieldDefinition.DataType,
          dataTypeMap
        );
      } else {
        if (
          SObjectFieldMappingHelper.dataTypeToKeyMap.containsKey(
            SObjectFieldMappingHelper.getTargetFieldDataTypeKey(
              thisFieldDefinition.DataType
            )
          )
        ) {
          for (
            String thisDataTypeKey : SObjectFieldMappingHelper.dataTypeToKeyMap.get(
                SObjectFieldMappingHelper.getTargetFieldDataTypeKey(
                  thisFieldDefinition.DataType
                )
              )
              .split(',')
          ) {
            populateDataTypeMap(
              thisFieldDefinition,
              thisDataTypeKey,
              dataTypeMap
            );
          }
        }
      }
    }
  }

  /**
   * @description  Method to populate DataType Map
   * @param  thisField Source Object Field Definition
   * @param  fieldkey Source Object Field Map unique key
   * @param  thisDataTypeMap Source field Datatype to sourceObjectFieldWrapper Map
   */
  public static void populateDataTypeMap(
    FieldDefinition thisField,
    String fieldkey,
    Map<String, List<sourceObjectFieldWrapper>> thisDataTypeMap
  ) {
    if (!thisDataTypeMap.containsKey(fieldkey)) {
      thisDataTypeMap.put(fieldkey, new List<sourceObjectFieldWrapper>());
    }
    thisDataTypeMap.get(fieldkey)
      .add(
        new sourceObjectFieldWrapper(
          thisField.MasterLabel,
          thisField.QualifiedApiName
        )
      );
  }

  /**
   * @description Method to populate Target object Wrapper
   * @param  targeteObjectName Target Object API Name
   * @param  targetObjectFieldWrapperList Target Object Field Wrapper
   */
  public static void generateNewTargetObjectFieldWrapperList(
    String targeteObjectName,
    List<targetObjectFieldWrapper> targetObjectFieldWrapperList
  ) {
    for (
      FieldDefinition thisFieldDefinition : SObjectFieldMappingHelper.getFieldDefinition(
        targeteObjectName
      )
    ) {
      if (!thisFieldDefinition.isCalculated)
        targetObjectFieldWrapperList.add(
          new targetObjectFieldWrapper(
            SObjectFieldMappingHelper.getTargetFieldDataTypeKey(
              thisFieldDefinition.DataType
            ),
            thisFieldDefinition.MasterLabel,
            thisFieldDefinition.QualifiedApiName,
            null
          )
        );
    }
  }

  /**
   * @description Method to get existing Target object Wrapper
   * @param  targeteObjectName Target Object API Name
   * @param  targetObjectFieldWrapperList Target Object Field Wrapper
   * @param  existingMapping Existing Target Object Field Wrapper Map
   */
  public static void generateExistingTargetObjectFieldWrapperList(
    String targeteObjectName,
    List<targetObjectFieldWrapper> targetObjectFieldWrapperList,
    Map<String, targetObjectFieldWrapper> existingMapping
  ) {
    for (
      FieldDefinition thisFieldDefinition : SObjectFieldMappingHelper.getFieldDefinition(
        targeteObjectName
      )
    )
    if(!thisFieldDefinition.isCalculated){
      {
        if (existingMapping.containsKey(thisFieldDefinition.QualifiedApiName)) {
          targetObjectFieldWrapperList.add(
            existingMapping.get(thisFieldDefinition.QualifiedApiName)
          );
        } else {
          targetObjectFieldWrapperList.add(
            new targetObjectFieldWrapper(
              SObjectFieldMappingHelper.getTargetFieldDataTypeKey(
                thisFieldDefinition.DataType
              ),
              thisFieldDefinition.MasterLabel,
              thisFieldDefinition.QualifiedApiName,
              null
            )
          );
        }
      }
    }
  }

  /**
   * @description Method to get existing Target object Wrapper
   * @param  fieldMappingJSON Existing Field Mapping JSON
   * @return existingMapping Existing Target Object Field Wrapper Map
   */
  public static Map<String, targetObjectFieldWrapper> getExistingMapping(
    String fieldMappingJSON
  ) {
    Map<String, targetObjectFieldWrapper> existingMapping = new Map<String, targetObjectFieldWrapper>();
    for (
      OrdwayFieldMappingParser.MappingParser thisMapping : OrdwayFieldMappingParser.parse(
        fieldMappingJSON
      )
    ) {
      existingMapping.put(
        thisMapping.fieldAPIName,
        new targetObjectFieldWrapper(
          thisMapping.fieldDataTypeKey,
          thisMapping.fieldLabel,
          thisMapping.fieldAPIName,
          thisMapping.sourceFieldAPIName
        )
      );
    }
    return existingMapping;
  }

  /**
   * @description Method to create New Field mapping Metadata
   * @param  sourceObjectName Source Object API Name
   * @param  targeteObjectName Target Object API Name
   * @param  fieldMappingJSON Field Mapping JSON
   */
  @AuraEnabled
  public static String createUpdateOrdwayFieldMappingMetadata(
    String sourceObjectName,
    String targetObjectName,
    String fieldMappingJSON,
    String description
  ) {
    String key = getUniqueMetadataKeyValue(
      sourceObjectName +
      '_to_' +
      targetObjectName
    );
    Id deploymentJobId;

    String deploymentStatus;
    Metadata.CustomMetadata customMetadata = new Metadata.CustomMetadata();
    customMetadata.fullName =
      'OrdwayLabs__ObjectFieldMapping.OrdwayLabs__' + key;
    customMetadata.label = key;
    customMetadata.values.add(
      SObjectFieldMappingHelper.createMetaDataValue(
        'OrdwayLabs__SourceObject__c',
        sourceObjectName
      )
    );
    customMetadata.values.add(
      SObjectFieldMappingHelper.createMetaDataValue(
        'OrdwayLabs__TargetObject__c',
        targetObjectName
      )
    );
    customMetadata.values.add(
      SObjectFieldMappingHelper.createMetaDataValue(
        'OrdwayLabs__FieldMappingJSON__c',
        fieldMappingJSON
      )
    );
    customMetadata.values.add(
      SObjectFieldMappingHelper.createMetaDataValue(
        'OrdwayLabs__WhereIsThisUsed__c',
        description
      )
    );

    deploymentJobId = DeployOrdwayObjectFieldMappingMetaData.createUpdateMetadata(
      customMetadata
    );

    return MetadataService.getMetadataDeploymentURL(deploymentJobId);
  }

  /**
   * getUniqueMetadataKeyValue Method to get unique Object Field Mapping key value
   * @param  metadataKeyValue String from which Unique key to be generated
   */
  public static String getUniqueMetadataKeyValue(String metadataKeyValue) {
    if (metadataKeyValue.contains('__c')) {
      metadataKeyValue = metadataKeyValue.remove('__c');
    }
    if (metadataKeyValue.contains('__')) {
      metadataKeyValue = metadataKeyValue.replaceAll('__', '_');
    }
    if (metadataKeyValue.contains('OrdwayLabs_')) {
      metadataKeyValue = metadataKeyValue.remove('OrdwayLabs_');
    }
    return metadataKeyValue;
  }

  /**
   * Object Field List Wrapper Class
   */
  public class objectFieldListWrapper {
    @AuraEnabled
    public Boolean isExistingMapping { get; set; }
    @AuraEnabled
    public List<targetObjectFieldWrapper> targetObjectFieldWrapperList {
      get;
      set;
    }
    @AuraEnabled
    public Map<String, List<sourceObjectFieldWrapper>> dataTypeFieldMap {
      get;
      set;
    }
    @AuraEnabled
    public String description { get; set; }

    public objectFieldListWrapper(
      List<targetObjectFieldWrapper> targetObjectWrapperList,
      Map<String, List<sourceObjectFieldWrapper>> dataTypeMap,
      Boolean isExisting,
      String thisDescription
    ) {
      isExistingMapping = isExisting;
      targetObjectFieldWrapperList = targetObjectWrapperList;
      dataTypeFieldMap = dataTypeMap;
      description = thisDescription;
    }
  }

  /**
   * Object List Wrapper Class
   */
  public class objectListWrapper {
    @AuraEnabled
    public String objectLabel { get; set; }
    @AuraEnabled
    public String objectAPIName { get; set; }
    public objectListWrapper(EntityDefinition thisObject) {
      objectLabel = thisObject.MasterLabel;
      objectAPIName = thisObject.QualifiedAPIName;
    }
  }

  /**
   * Target Object Field List Wrapper Class
   */
  public class targetObjectFieldWrapper {
    @AuraEnabled
    public String fieldDataTypeKey { get; set; }
    @AuraEnabled
    public String fieldLabel { get; set; }
    @AuraEnabled
    public String fieldAPIName { get; set; }
    @AuraEnabled
    public String sourceFieldAPIName { get; set; }
    public targetObjectFieldWrapper(
      String fieldkey,
      String Label,
      String APIName,
      String sourceAPI
    ) {
      fieldDataTypeKey = fieldkey;
      fieldLabel = Label;
      fieldAPIName = APIName;
      sourceFieldAPIName = sourceAPI;
    }
  }

  /**
   * Source Object Field List Wrapper Class
   */
  public class sourceObjectFieldWrapper {
    @AuraEnabled
    public String fieldLabel { get; set; }
    @AuraEnabled
    public String fieldAPIName { get; set; }
    public sourceObjectFieldWrapper(String Label, String APIName) {
      fieldLabel = Label;
      fieldAPIName = APIName;
    }
  }
}