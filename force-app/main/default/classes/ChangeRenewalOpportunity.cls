/**
 * @File Name          : ChangeRenewalOpportunity.cls
 * @Description        : Change Renewal Opportunity Controller
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    4/11/2020        Initial Version
 **/
public with sharing class ChangeRenewalOpportunity {
  /**
   * @description fetch all open opportunities against the contract
   * @param  contractId  Id of the contract record
   * @param isRenewal Boolean variable to indicate whether Opportunity Type is Renewal type or not
   * @return List<Opportunity> list of all open opportunities
   */
  @AuraEnabled
  public static List<Opportunity> getOpenOpportunities(
    string contractId,
    Boolean isRenewal
  ) {
    Boolean isClosed = false;

    string ORDWAY_OPPORTUNITY_TYPE_RENEWAl = OrdwayHelper.ORDWAY_OPPORTUNITY_TYPE_RENEWAL;
    string ORDWAY_OPPORTUNITY_TYPE_UPSELL = OrdwayHelper.ORDWAY_OPPORTUNITY_TYPE_UPSELL;

    string opportunityType = isRenewal
      ? ORDWAY_OPPORTUNITY_TYPE_RENEWAl
      : ORDWAY_OPPORTUNITY_TYPE_UPSELL;

    List<Opportunity> opportunityList = new List<Opportunity>(
      (List<Opportunity>) Database.query(
        'SELECT ' +
        String.join(
          SObjectHelper.getAllFields(Schema.Opportunity.getSObjectType()),
          ','
        ) +
        ' FROM Opportunity WHERE OrdwayLabs__OrdwayOpportunityType__c =: opportunityType AND IsClosed =: isClosed AND OrdwayLabs__OrdwayContract__c =: contractId'
      )
    );
    SObjectAccessDecision securityDecision = Security.stripInaccessible(
      AccessType.READABLE,
      opportunityList
    );
    return (List<Opportunity>) securityDecision.getRecords();
  }

  /**
   * @description Method to get new Opportunity instance
   * @param  recordId  Opportunity Id or Contract Id - Contract Id when no Open Opportunity exists, Opportunity Id if Open Opportunity exists
   * @param  opportunityType  Change or Renewal
   * @return new Opportunity
   */
  @AuraEnabled
  public static Opportunity getOpportunity(
    string recordId,
    string opportunityType
  ) {
    Boolean isClosed = false;
    //We need opportunityType to pre-populate the fields & perform the logic
    //If Record Type is configured in custom settings, then pass the record type Id to load the defaults
    Opportunity thisCR = (Opportunity) Opportunity.sObjectType.newSObject(
      null,
      true
    );

    if (((Id) recordId).getSObjectType() == Opportunity.getSObjectType()) {
      //If recordId is of type Opportunity, then query the Opportunity Id
      List<Opportunity> opportunityList = new List<Opportunity>(
        (List<Opportunity>) Database.query(
          'SELECT ' +
          String.join(
            SObjectHelper.getAllFields(Schema.Opportunity.getSObjectType()),
            ','
          ) +
          ' FROM Opportunity WHERE IsClosed =: isClosed AND Id =: recordId'
        )
      );
      SObjectAccessDecision securityDecision = Security.stripInaccessible(
        AccessType.READABLE,
        opportunityList
      );
      thisCR = (Opportunity) securityDecision.getRecords()[0];
      return thisCR;
    }

    if (
      ((Id) recordId).getSObjectType() ==
      OrdwayLabs__OrdwayContract__c.getSObjectType()
    ) {
      //If recordId is of type Ordway Contract, then form a new instance and populate the fields
      List<OrdwayContract__c> contractList = new List<OrdwayContract__c>(
        (List<OrdwayContract__c>) Database.query(
          'SELECT ' +
          String.join(
            SObjectHelper.getAllFields(
              Schema.OrdwayContract__c.getSObjectType()
            ),
            ','
          ) +
          ' FROM OrdwayLabs__OrdwayContract__c WHERE Id =: recordId'
        )
      );

      if (contractList[0].SyncedOpportunityId__c != null) {
        Opportunity thisOpportunity;
        if (Schema.sObjectType.Opportunity.isAccessible()) {
          thisOpportunity = [
            SELECT Id, PriceBook2Id
            FROM Opportunity
            WHERE Id = :contractList[0].SyncedOpportunityId__c
          ];
        }
        thisCR.Pricebook2Id = thisOpportunity.Pricebook2Id;
        thisCR.RelatedOpportunity__c = thisOpportunity.Id; //Self Relationship
      }

      mapContractToOpportunity(thisCR, contractList[0]);
      thisCR.OrdwayOpportunityType__c = OrdwayHelper.ORDWAY_OPPORTUNITY_TYPE_UPSELL;
      if (OrdwayHelper.ORDWAY_OPPORTUNITY_TYPE_RENEWAL == opportunityType) {
        thisCR.OrdwayOpportunityType__c = OrdwayHelper.ORDWAY_OPPORTUNITY_TYPE_RENEWAL;
        thisCR.Name =
          thisCR.Name +
          ' ' +
          OrdwayHelper.ORDWAY_OPPORTUNITY_TYPE_RENEWAL;
        thisCR.ServiceStartDate__c = thisCR.ServiceStartDate__c != null
          ? thisCR.ServiceStartDate__c.addMonths(
              Integer.valueOf(thisCR.ContractTerm__c)
            )
          : null;
        thisCR.BillingStartDate__c = thisCR.BillingStartDate__c != null
          ? thisCR.BillingStartDate__c.addMonths(
              Integer.valueOf(thisCR.ContractTerm__c)
            )
          : null;
        thisCR.ContractEffectiveDate__c = thisCR.ContractEffectiveDate__c !=
          null
          ? thisCR.ContractEffectiveDate__c.addMonths(
              Integer.valueOf(thisCR.ContractTerm__c)
            )
          : null;
      }

      return thisCR;
    }

    return null;
  }

  /**
   * @description Method to populate Ordway contract values to Opportunity based on dynamic field mapping configurations
   * @param thisOpportunity Opportunity Record
   * @param thisContract Ordway Contract
   **/
  public static void mapContractToOpportunity(
    Opportunity thisOpportunity,
    OrdwayContract__c thisContract
  ) {
    thisOpportunity.OrdwayContract__c = thisContract.Id;
    //Dynamic Field Mapping
    SObjectFieldMappingHelper.dynamicSobjectMapping(
      thisContract,
      thisOpportunity,
      true
    );
  }
}