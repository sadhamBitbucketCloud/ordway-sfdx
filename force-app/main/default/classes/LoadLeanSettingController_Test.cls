/**
 * @File Name          : LoadLeanSettingController_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    12/31/2019   Ordway Labs     Initial Version
 **/
@IsTest
private class LoadLeanSettingController_Test {
  @IsTest
  private static void loadLeanSetting_ProfileEnabled() {
    Test.startTest();
    LoadLeanSettings__c llSetting = TestObjectFactory.createLoadLeanSetting();
    insert llSetting;

    System.assertEquals(
      true,
      LoadLeanSettingController.isTriggerDisabled(),
      'Trigger disabled for profile'
    );

    System.assertEquals(
      true,
      LoadLeanSettingController.isSyncLogging(),
      'Log error synchronously'
    );

    System.assertEquals(
      true,
      LoadLeanSettingController.allLogs(),
      'Log everything including debug, warning etc.,'
    );
    Test.stopTest();
  }
  @IsTest
  private static void loadLeanSetting_ProfileDisabled() {
    Test.startTest();

    LoadLeanSettings__c userSetting = new LoadLeanSettings__c();
    userSetting.CreateApplicationLogSynchronously__c = false;
    userSetting.DisableTriggers__c = false;
    userSetting.DisableValidationRules__c = false;
    userSetting.AllLogs__c = false;
    userSetting.Details__c =
      '[{' +
      '"type": "Change",' +
      ' "layout": "Opportunity-Ordway Opportunity Layout"' +
      '},' +
      '{' +
      '"type": "Renewal",' +
      '"layout": "Renewal-Ordway Opportunity Layout"' +
      '}' +
      ']';

    LoadLeanSettings__c llSetting = TestObjectFactory.createLoadLeanSetting(
      userSetting
    );
    insert llSetting;

    System.assertEquals(
      false,
      LoadLeanSettingController.isTriggerDisabled(),
      'Trigger enabled for profile'
    );

    System.assertEquals(
      false,
      LoadLeanSettingController.isSyncLogging(),
      'Log error asynchronously'
    );

    System.assertEquals(
      false,
      LoadLeanSettingController.allLogs(),
      'error log only'
    );

    System.assertEquals(
      'Renewal-Ordway Opportunity Layout',
      LoadLeanSettingController.getOpportunityPageLayoutName(true),
      'Should return Renewal-Ordway Opportunity Layout'
    );

    System.assertEquals(
      'Opportunity-Ordway Opportunity Layout',
      LoadLeanSettingController.getOpportunityPageLayoutName(false),
      'Should return Opportunity-Ordway Opportunity Layout'
    );
    Test.stopTest();
  }

  @IsTest
  private static void loadLeanSetting_UserEnabled() {
    Test.startTest();
    LoadLeanSettings__c userSetting = new LoadLeanSettings__c();
    userSetting.SetupOwnerId = UserInfo.getUserId();

    LoadLeanSettings__c llSetting = TestObjectFactory.createLoadLeanSetting(
      userSetting
    );
    insert llSetting;

    System.assertEquals(
      true,
      LoadLeanSettingController.isTriggerDisabled(),
      'Trigger disabled for profile'
    );

    System.assertEquals(
      true,
      LoadLeanSettingController.isSyncLogging(),
      'Log error synchronously'
    );

    System.assertEquals(
      true,
      LoadLeanSettingController.allLogs(),
      'Log everything including debug, warning etc.,'
    );
    Test.stopTest();
  }
  @IsTest
  private static void loadLeanSetting_UserDisabled() {
    Test.startTest();

    LoadLeanSettings__c userSetting = new LoadLeanSettings__c();
    userSetting.SetupOwnerId = UserInfo.getUserId();
    userSetting.CreateApplicationLogSynchronously__c = false;
    userSetting.DisableTriggers__c = false;
    userSetting.DisableValidationRules__c = false;
    userSetting.AllLogs__c = false;

    LoadLeanSettings__c llSetting = TestObjectFactory.createLoadLeanSetting(
      userSetting
    );
    insert llSetting;

    System.assertEquals(
      false,
      LoadLeanSettingController.isTriggerDisabled(),
      'Trigger enabled for profile'
    );

    System.assertEquals(
      false,
      LoadLeanSettingController.isSyncLogging(),
      'Log error asynchronously'
    );

    System.assertEquals(
      false,
      LoadLeanSettingController.allLogs(),
      'error log only'
    );
    Test.stopTest();
  }

  @IsTest
  private static void userEnabledProfileDisabled() {
    Test.startTest();
    LoadLeanSettings__c llSetting = TestObjectFactory.createLoadLeanSetting();
    insert llSetting;

    LoadLeanSettings__c userSetting = new LoadLeanSettings__c();
    userSetting.SetupOwnerId = UserInfo.getUserId();
    userSetting.CreateApplicationLogSynchronously__c = true;
    userSetting.DisableTriggers__c = true;
    userSetting.DisableValidationRules__c = true;
    userSetting.AllLogs__c = true;

    LoadLeanSettings__c llSettingUser = TestObjectFactory.createLoadLeanSetting(
      userSetting
    );
    insert llSettingUser;

    System.assertEquals(
      true,
      LoadLeanSettingController.isTriggerDisabled(),
      'Trigger disabled for profile'
    );

    System.assertEquals(
      true,
      LoadLeanSettingController.isSyncLogging(),
      'Log error synchronously'
    );

    System.assertEquals(
      true,
      LoadLeanSettingController.allLogs(),
      'Log everything including debug, warning etc.,'
    );
    Test.stopTest();
  }

  @IsTest
  private static void userDisabledProfileEnabled() {
    Test.startTest();
    LoadLeanSettings__c llSetting = TestObjectFactory.createLoadLeanSetting();
    insert llSetting;

    LoadLeanSettings__c userSetting = new LoadLeanSettings__c();
    userSetting.SetupOwnerId = UserInfo.getUserId();
    userSetting.CreateApplicationLogSynchronously__c = false;
    userSetting.DisableTriggers__c = false;
    userSetting.DisableValidationRules__c = false;
    userSetting.AllLogs__c = false;

    LoadLeanSettings__c llSettingUser = TestObjectFactory.createLoadLeanSetting(
      userSetting
    );
    insert llSettingUser;

    System.assertEquals(
      false,
      LoadLeanSettingController.isTriggerDisabled(),
      'Trigger Enabled'
    );

    System.assertEquals(
      false,
      LoadLeanSettingController.isSyncLogging(),
      'Log error asynchronously'
    );

    System.assertEquals(
      false,
      LoadLeanSettingController.allLogs(),
      'error log only'
    );
    Test.stopTest();
  }

  @IsTest
  private static void defaultSettings() {
    Test.startTest();

    System.assertEquals(
      false,
      LoadLeanSettingController.isTriggerDisabled(),
      'Trigger Enabled'
    );

    System.assertEquals(
      false,
      LoadLeanSettingController.isSyncLogging(),
      'Log error asynchronously'
    );

    System.assertEquals(
      false,
      LoadLeanSettingController.allLogs(),
      'error log only'
    );
    Test.stopTest();
  }
}