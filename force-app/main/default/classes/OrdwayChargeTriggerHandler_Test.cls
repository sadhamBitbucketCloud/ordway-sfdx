@isTest
private class OrdwayChargeTriggerHandler_Test {
  @isTest
  private static void populateProductId() {

    ProductTriggerHandler.createOrdwayProduct = false;
    Product2 thisProduct = TestObjectFactory.createProduct();
    thisProduct.OrdwayProduct__c = true;
    insert thisProduct;

    Plan__c thisPlan        = TestObjectFactory.createOrdwayPlan();
    thisPlan.PlanDetails__c = thisProduct.Id;
    insert thisPlan;

    OrdwayProduct__c thisOrdwayProduct  = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.OrdwayProductID__c     = 'P-00075';
    thisOrdwayProduct.Product2Id__c     = thisProduct.Id;
    insert thisOrdwayProduct;

    Charge__c thisCharge       = TestObjectFactory.createCharge();
    thisCharge.Name            = 'Test Charge';
    thisCharge.OrdwayPlan__c   =  thisPlan.Id;
    thisCharge.Product__c      = thisProduct.Id;
    insert thisCharge;

    System.assertEquals('P-00075', [SELECT Id, ProductID__c FROM Charge__c].ProductID__c, 'should return same product Id');
  }

  @isTest
  private static void populateChargeId() {

    ProductTriggerHandler.createOrdwayProduct = false;
    Product2 thisProduct = TestObjectFactory.createProduct();
    thisProduct.OrdwayProduct__c = true;
    insert thisProduct;

    Plan__c thisPlan        = TestObjectFactory.createOrdwayPlan();
    thisPlan.PlanDetails__c = thisProduct.Id;
    insert thisPlan;

    OrdwayProduct__c thisOrdwayProduct   = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.OrdwayProductID__c = 'P-00075';
    thisOrdwayProduct.Product2Id__c      = thisProduct.Id;
    insert thisOrdwayProduct;

    Charge__c thisCharge       = TestObjectFactory.createCharge();
    thisCharge.Name            = 'Test Charge';
    thisCharge.OrdwayPlan__c   =  thisPlan.Id;
    thisCharge.Product__c      = thisProduct.Id;
    insert thisCharge;

    thisCharge.ExternalID__c = 'EXTERNEL:C:C-1234';
    update thisCharge;

    System.assertEquals('C-1234', [SELECT Id, ChargeID__c FROM Charge__c].ChargeID__c, 'should return same charge Id');
 }

 @isTest
  private static void populateChargeIdOnUpdate() {

    ProductTriggerHandler.createOrdwayProduct = false;
    Product2 thisProduct = TestObjectFactory.createProduct();
    thisProduct.OrdwayProduct__c = true;
    insert thisProduct;

    Plan__c thisPlan        = TestObjectFactory.createOrdwayPlan();
    thisPlan.PlanDetails__c = thisProduct.Id;
    insert thisPlan;

    OrdwayProduct__c thisOrdwayProduct   = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.OrdwayProductID__c = 'P-00075';
    thisOrdwayProduct.Product2Id__c      = thisProduct.Id;
    insert thisOrdwayProduct;

    Charge__c thisCharge       = TestObjectFactory.createCharge();
    thisCharge.Name            = 'Test Charge';
    thisCharge.OrdwayPlan__c   =  thisPlan.Id;
    thisCharge.Product__c      = thisProduct.Id;
    insert thisCharge;

    thisCharge.ExternalID__c = 'C-1234';
    update thisCharge;

    System.assertEquals('C-1234', [SELECT Id, ChargeID__c FROM Charge__c].ChargeID__c, 'should return same charge Id');
 }

 @isTest
  private static void populateChargeIdOnInsert() {

    ProductTriggerHandler.createOrdwayProduct = false;
    Product2 thisProduct = TestObjectFactory.createProduct();
    thisProduct.OrdwayProduct__c = true;
    insert thisProduct;

    Plan__c thisPlan        = TestObjectFactory.createOrdwayPlan();
    thisPlan.PlanDetails__c = thisProduct.Id;
    insert thisPlan;

    OrdwayProduct__c thisOrdwayProduct   = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.OrdwayProductID__c = 'P-00075';
    thisOrdwayProduct.Product2Id__c      = thisProduct.Id;
    insert thisOrdwayProduct;

    Charge__c thisCharge       = TestObjectFactory.createCharge();
    thisCharge.Name            = 'Test Charge';
    thisCharge.OrdwayPlan__c   =  thisPlan.Id;
    thisCharge.Product__c      = thisProduct.Id;
    thisCharge.ExternalID__c = 'EXTERNEL:C:C-1234';
    insert thisCharge;

    System.assertEquals('C-1234', [SELECT Id, ChargeID__c FROM Charge__c].ChargeID__c, 'should return same charge Id');
 }

 @isTest
  private static void populateChargeIdInsert() {

    ProductTriggerHandler.createOrdwayProduct = false;
    Product2 thisProduct = TestObjectFactory.createProduct();
    thisProduct.OrdwayProduct__c = true;
    insert thisProduct;

    Plan__c thisPlan        = TestObjectFactory.createOrdwayPlan();
    thisPlan.PlanDetails__c = thisProduct.Id;
    insert thisPlan;

    OrdwayProduct__c thisOrdwayProduct   = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.OrdwayProductID__c = 'P-00075';
    thisOrdwayProduct.Product2Id__c      = thisProduct.Id;
    insert thisOrdwayProduct;

    Charge__c thisCharge       = TestObjectFactory.createCharge();
    thisCharge.Name            = 'Test Charge';
    thisCharge.OrdwayPlan__c   =  thisPlan.Id;
    thisCharge.Product__c      = thisProduct.Id;
    thisCharge.ExternalID__c   = 'C-1234';
    insert thisCharge;

    System.assertEquals('C-1234', [SELECT Id, ChargeID__c FROM Charge__c].ChargeID__c, 'should return same charge Id');
 }
  @isTest
  private static void populateProductId_ME() {

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c         = true;
    
    insert newSetting;

    Entity__c thisEntity   = TestObjectFactory.createOrdwayEntity();
    thisEntity.Name        = 'Test Entity';
    insert thisEntity; 

    thisEntity.EntityID__c = thisEntity.Id;
    update thisEntity;
    
    Entity__c thisEntityNew   = TestObjectFactory.createOrdwayEntity();
    thisEntityNew.Name        = 'Test Entity New';
    insert thisEntityNew;
    
    thisEntityNew.EntityID__c = thisEntityNew.Id;
    update thisEntityNew;
    
    ProductTriggerHandler.createOrdwayProduct = false;

    Product2 thisProduct = TestObjectFactory.createProduct();
    thisProduct.OrdwayProduct__c = true;
    insert thisProduct;

    Plan__c thisPlan        = TestObjectFactory.createOrdwayPlan();
    thisPlan.PlanDetails__c = thisProduct.Id;
    thisPlan.EntityID__c = thisEntity.Id;
    thisPlan.OrdwayEntity__c = thisEntity.Id;
    insert thisPlan;

    OrdwayProduct__c thisOrdwayProduct  = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.OrdwayProductID__c = 'P-00071';
    thisOrdwayProduct.Product2Id__c     = thisProduct.Id;
    thisOrdwayProduct.OrdwayEntity__c = thisEntityNew.Id;
    thisOrdwayProduct.EntityID__c = thisEntityNew.Id;
    insert thisOrdwayProduct;

    OrdwayProduct__c thisOrdwayProductNew  = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProductNew.OrdwayProductID__c     = 'P-00075';
    thisOrdwayProductNew.Product2Id__c     = thisProduct.Id;
    thisOrdwayProductNew.EntityID__c = thisEntity.Id;
    thisOrdwayProductNew.OrdwayEntity__c = thisEntity.Id;
    insert thisOrdwayProductNew;

    Charge__c thisCharge = TestObjectFactory.createCharge();
    thisCharge.Name  = 'Test Charge';
    thisCharge.OrdwayPlan__c =  thisPlan.Id;
    thisCharge.Product__c = thisProduct.Id;
    insert thisCharge;

    System.assertEquals('P-00075', [SELECT Id, ProductID__c FROM Charge__c].ProductID__c, 'should return same product Id');
  }}