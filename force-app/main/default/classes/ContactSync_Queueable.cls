/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 11-05-2020
 * @last modified by  : 
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   11-05-2020      Initial Version
**/
public with sharing class ContactSync_Queueable implements Queueable, Database.AllowsCallouts {
  public Map<Id, Contact> contactMap;

  public ContactSync_Queueable(Map<Id, Contact> contactMap) {
    this.contactMap  = contactMap;
  }

  public void execute(QueueableContext context) {
    try {
      List<Contact> contactListToSync = new List<Contact>(ContactService.getContactListToSync(contactMap));
      if(!contactListToSync.isEmpty()){
        HttpResponse thisResponse = OrdwayService.syncContactToOrdway(contactListToSync);
        if (thisResponse != null && thisResponse.getStatusCode() == 200) {
          
          SObject sObjectRecord;
          Map<String, Object> thisDeserializedMap = (Map<String, Object>) JSON.deserializeUntyped(thisResponse.getBody());
          sObjectRecord = OrdwayHelper.getSObjectRecord(thisDeserializedMap, Schema.sObjectType.Contact, new Contact());
          sObjectRecord.put('Id', contactListToSync[0].Id);

          ContactTriggerHandler.runOnce = false; //Turn The Trigger Off
          if (Schema.sObjectType.Contact.isUpdateable()) {
              update sObjectRecord;
          }
        }
      }
    }
    catch (Exception ex) {
      ApplicationLog.Error(ex);
      ApplicationLog.FinishLoggingContext();
    }
  }
}