/**
 * @File Name          : OrdwayContractTriggerHandler.cls
 * @Description        : Ordway Contract Trigger Handler
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 11-12-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    10/12/2019   Ordway Labs     Initial Version
 **/
public with sharing class OrdwayContractTriggerHandler extends TriggerHandler.TriggerHandlerBase {
  public override void beforeInsert(List<SObject> listNewSObjs) {
    for (
      OrdwayContract__c thisOrdwayContract : (List<OrdwayContract__c>) listNewSObjs
    ) {
      OrdwayContractService.populateSubscriptionId(thisOrdwayContract);
    }
  }

  public override void beforeUpdate(
    Map<Id, SObject> mapOldSObjs,
    Map<Id, SObject> mapNewSObjs
  ) {
    Set<Id> contactIds = new Set<Id>();
    Map<Id, Contact> contactMap = new Map<Id, Contact>();

    for (
      OrdwayContract__c thisOrdwayContract : (List<OrdwayContract__c>) mapNewSObjs.values()
    ) {
      if (
        SObjectHelper.isChanged(
          Schema.OrdwayContract__c.BillingContact__c,
          thisOrdwayContract,
          (Map<Id, OrdwayContract__c>) mapOldSObjs
        )
      ) {
        contactIds.add(thisOrdwayContract.BillingContact__c);
      }
    }

    contactMap = ContactService.getContact(contactIds);

    for (
      OrdwayContract__c thisOrdwayContract : (List<OrdwayContract__c>) mapNewSObjs.values()
    ) {
      OrdwayContractService.populateSubscriptionId(thisOrdwayContract);
      OrdwayContractService.populateShippingContact(thisOrdwayContract);
      OrdwayContractService.populateBillingAddress(
        thisOrdwayContract,
        contactMap
      );
      OrdwayContractService.populateVersionType(
        thisOrdwayContract,
        (Map<Id, OrdwayContract__c>) mapOldSObjs
      );
      OrdwayContractService.defaultRenewalTerm(thisOrdwayContract);
    }
  }

  public override void afterUpdate(
    Map<Id, SObject> mapOldSObjs,
    Map<Id, SObject> mapNewSObjs
  ) {
    Set<Id> syncedOppIds = new Set<Id>();
    List<OrdwayContract__c> contractList = new List<OrdwayContract__c>();

    Map<Id, OrdwayContract__c> oldMap = (Map<Id, OrdwayContract__c>) mapOldSObjs;
    Map<Id, OrdwayContract__c> newMap = (Map<Id, OrdwayContract__c>) mapNewSObjs;

    for (OrdwayContract__c thisOrdwayContract : newMap.values()) {
      if (
        OrdwayContractService.isNewOpportunitySync(
          thisOrdwayContract,
          (Map<Id, OrdwayContract__c>) mapOldSObjs
        )
      ) {
        syncedOppIds.add(thisOrdwayContract.SyncedOpportunityId__c);
      }

      if (
        thisOrdwayContract.Account__c != null &&
        SObjectHelper.isChanged(
          Schema.OrdwayContract__c.CustomerId__c,
          thisOrdwayContract,
          (Map<Id, OrdwayContract__c>) mapOldSObjs
        )
      ) {
        contractList.add(thisOrdwayContract);
      }
    }

    if (!contractList.isEmpty()) {
      OrdwayContractService.updateCustomerId(contractList);
    }

    OrdwayContractService.updateOpportunity(
      (Map<Id, OrdwayContract__c>) mapNewSObjs,
      (Map<Id, OrdwayContract__c>) mapOldSObjs
    );

    if (!syncedOppIds.isEmpty()) {
      OrdwayContractService.updateSyncing(syncedOppIds, newMap.keyset());
    }
  }

  public override void beforeDelete(Map<Id, SObject> mapOldSObjs) {
    for (
      OrdwayContract__c thisOrdwayContract : (List<OrdwayContract__c>) mapOldSObjs.values()
    ) {
      if (!FeatureManagement.checkPermission('CanDeleteOpportunity') 
      && thisOrdwayContract.OrdwaySubscriptionID__c != null) {
        thisOrdwayContract.addError(
          System.Label.OrdwayLabs.Ordway_Contract_Delete_Message
        );
      }
    }
  }
}