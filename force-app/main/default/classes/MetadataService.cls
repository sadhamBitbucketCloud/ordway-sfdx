/**
 * @File Name          : MetadataService.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 05-03-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    9/11/2019   Ordway Labs     Initial Version
 **/
public with sharing class MetadataService {
  public static final String GET_ORDWAY_CUSTOMER = 'GETOrdwayCustomer';
  public static final String VALIDATE_TOKEN = 'ValidateToken';

  public static final String GET_ORDWAY_SUBSCRIPTION = 'GETOrdwaySubscription';
  public static final String SEND_CONTRACT_TO_ORDWAY = 'PostContractToOrdway';
  public static final String ACTIVATE_ORDWAY_CONTRACT = 'ActivateOrdwayContract';
  public static final String CANCEL_ORDWAY_CONTRACT = 'CancelOrdwayContract';
  public static final String UNLINK_ORDWAY_CONTRACT = 'UnlinkOrdwayContract';
  public static final String PUT_ORDWAY_CONTRACT_CHANGE = 'Contract_PUT_Change';
  public static final String PUT_ORDWAY_CONTRACT_UPDATE = 'Contract_PUT_Update';
  public static final String PUT_ORDWAY_CONTRACT_RENEW = 'Contract_PUT_Renew';

  public static final String GET_QUOTE = 'Quote_GET';
  public static final String POST_QUOTE = 'Quote_POST';
  public static final String PUT_QUOTE = 'Quote_PUT';
  public static final String POST_ACTIVATE = 'Quote_Activate_Send_POST';
  public static final String POST_CANCEL = 'Quote_Cancel_POST';
  public static final String POST_CONVERT = 'Quote_Convert_POST';
  public static final String GET_QUOTE_TEMPLATE = 'QuoteTemplate_GET';

  public static final String QUOTE_SETTING = 'Quote_Settings_GET';
  public static final String ENTITY_SETTING = 'Entity_Settings_GET';

  public static final String INJECT_ID_API = 'ID_Inject_API_PUT';

  public static final String PUT_CONTACT = 'Contact_PUT';
  public static final String POST_CONTACT = 'Contact_POST';

  public static final String PUT_ACCOUNT = 'Account_PUT';
  public static final String POST_ACCOUNT = 'Account_POST';

  //Product
  public static final String PUT_PRODUCT = 'Product_PUT';
  public static final String POST_PRODUCT = 'Product_POST';

   //Plan
   public static final String PUT_PLAN = 'Plan_PUT';
   public static final String POST_PLAN = 'Plan_POST';
   public static final String GET_ORDWAY_PLAN = 'GETOrdwayPlan'; //Without Reverse Mapping

  @TestVisible
  public static Map<String, ObjectFieldMapping__mdt> fieldMappingMetadataMap {
    get {
      if (fieldMappingMetadataMap == null) {
        fieldMappingMetadataMap = new Map<String, ObjectFieldMapping__mdt>();
        for (ObjectFieldMapping__mdt thisMetadata : [
          SELECT
            SourceObject__c,
            MasterLabel,
            DeveloperName,
            TargetObject__c,
            WhereIsThisUsed__c,
            FieldMappingJSON__c
          FROM ObjectFieldMapping__mdt
          LIMIT 1000
        ]) {
          if(StaticFieldMappingHelper.STATIC_FIELD_MAPPINGS.containsKey(thisMetadata.MasterLabel)){
            List<OrdwayFieldMappingParser.MappingParser> dynamicMapping     = OrdwayFieldMappingParser.parse(thisMetadata.FieldMappingJSON__c);
            List<OrdwayFieldMappingParser.MappingParser> staticFieldMapping = OrdwayFieldMappingParser.parse(StaticFieldMappingHelper.STATIC_FIELD_MAPPINGS.get(thisMetadata.MasterLabel));
            dynamicMapping.addAll(staticFieldMapping);
            thisMetadata.FieldMappingJSON__c = JSON.serialize(dynamicMapping);
          }
          fieldMappingMetadataMap.put(thisMetadata.MasterLabel, thisMetadata);
        }
      }
      return fieldMappingMetadataMap;
    }
    private set;
  }

  @TestVisible
  public static Map<String, String> endPointMapping {
    get {
      if (endPointMapping == null) {
        endPointMapping = new Map<String, String>();
        for (OrdwayEndpointConfiguration__mdt thisMetadata : [
          SELECT MasterLabel, DeveloperName, URL__c
          FROM OrdwayEndpointConfiguration__mdt
          LIMIT 100
        ]) {
          endPointMapping.put(thisMetadata.DeveloperName, thisMetadata.URL__c);
        }
      }
      return endPointMapping;
    }
    private set;
  }

  @TestVisible
  public static List<String> dynamicObjectMappingAPINames {
    get {
      if (dynamicObjectMappingAPINames == null) {
        dynamicObjectMappingAPINames = new List<String>();
        for (DynamicObjectMapping__mdt thisMetadata : [
          SELECT DeveloperName
          FROM DynamicObjectMapping__mdt
          LIMIT 100
        ]) {
          dynamicObjectMappingAPINames.add(thisMetadata.DeveloperName);
        }
      }
      return dynamicObjectMappingAPINames;
    }
    set;
  }

  @TestVisible
  public static Map<String, DynamicReference> dynamicMap {
    get {
      if (dynamicMap == null) {
        dynamicMap = new Map<String, DynamicReference>();
        for (DynamicObjectMapping__mdt thisMetadata : [
          SELECT
            DeveloperName,
            Object__r.QualifiedApiName,
            ExternalID__r.QualifiedApiName,
            PrimaryExternalID__r.QualifiedApiName,
            PrimaryObjectExternalId__r.QualifiedApiName,
            PrimaryRelationship__r.QualifiedApiName,
            SecondaryExternalID__r.QualifiedApiName,
            SecondaryObjectExternalId__r.QualifiedApiName,
            SecondaryRelationship__r.QualifiedApiName,
            PrimaryObject__r.QualifiedApiName,
            SecondaryObject__r.QualifiedApiName,
            ChildExternalID__r.QualifiedApiName,
            ChildObject__r.QualifiedApiName,
            ParentRelationshipField__r.QualifiedApiName,
            IsManyToMany__c,
            JunctionObject__r.QualifiedApiName,
            PrimaryJunctionRelationship__r.QualifiedApiName,
            SecondaryJunctionRelationship__r.QualifiedApiName,
            ExternalID_PrimaryObjectForJO__r.QualifiedApiName,
            PrimaryObjectForJunctionObject__r.QualifiedApiName,
            ChildSecondaryExternalId__r.QualifiedApiName,
            ChildSecondaryObject__r.QualifiedApiName,
            ChildSecondaryObjectExternalId__r.QualifiedApiName,
            ChildSecondaryRelationshipField__r.QualifiedApiName
          FROM DynamicObjectMapping__mdt
          ORDER BY OrderNumber__c ASC
          LIMIT 100
        ]) {
          dynamicMap.put(thisMetadata.DeveloperName, new DynamicReference(thisMetadata));
        }
      }
      return dynamicMap;
    }
    private set;
  }

  @TestVisible
  public static Map<String, DynamicObjectMapping__mdt> dynamicObjectMapping {
    get {
      if (dynamicObjectMapping == null) {
        dynamicObjectMapping = new Map<String, DynamicObjectMapping__mdt>();
        for (DynamicObjectMapping__mdt thisMetadata : [
          SELECT
            DeveloperName,
            Object__r.QualifiedApiName,
            ExternalID__r.QualifiedApiName,
            PrimaryExternalID__r.QualifiedApiName,
            PrimaryObjectExternalId__r.QualifiedApiName,
            PrimaryRelationship__r.QualifiedApiName,
            SecondaryExternalID__r.QualifiedApiName,
            SecondaryObjectExternalId__r.QualifiedApiName,
            SecondaryRelationship__r.QualifiedApiName,
            PrimaryObject__r.QualifiedApiName,
            SecondaryObject__r.QualifiedApiName,
            ChildExternalID__r.QualifiedApiName,
            ChildObject__r.QualifiedApiName,
            ParentRelationshipField__r.QualifiedApiName,
            IsManyToMany__c,
            JunctionObject__r.QualifiedApiName,
            PrimaryJunctionRelationship__r.QualifiedApiName,
            SecondaryJunctionRelationship__r.QualifiedApiName,
            ExternalID_PrimaryObjectForJO__r.QualifiedApiName,
            PrimaryObjectForJunctionObject__r.QualifiedApiName,
            ChildSecondaryExternalId__r.QualifiedApiName,
            ChildSecondaryObject__r.QualifiedApiName,
            ChildSecondaryObjectExternalId__r.QualifiedApiName,
            ChildSecondaryRelationshipField__r.QualifiedApiName
          FROM DynamicObjectMapping__mdt
          LIMIT 100
        ]) {
          dynamicObjectMapping.put(thisMetadata.DeveloperName, thisMetadata);
        }
      }

      return dynamicObjectMapping;
    }
    private set;
  }

  /**
   * @description Method to get SObject Field
   * @param objectName Object API name
   * @param fieldName Field API Name
   * @return Schema.SobjectField
   **/
  public static Schema.SobjectField getFieldName(
    String objectName,
    String fieldName
  ) {
   
    if (objectName != null && fieldName != null) {
      return Schema.getGlobalDescribe()
        .get(objectName)
        .getDescribe()
        .fields.getMap()
        .get(fieldName);
    }
    return null;
  }

  public static Map<String,String> objectAPINameMap {
    get {
      if(objectAPINameMap == null) {
        objectAPINameMap = new Map<String, String>();
        objectAPINameMap.put('OLI', 'OpportunityLineItem');
        objectAPINameMap.put('QLI', 'OrdwayLabs__QuoteLineItem__c');
      }
      return objectAPINameMap;
    }
    set;
  }

  @TestVisible
  public static Map<String, OrdwayLabs__PlanPicker__mdt> planPickerMetadata {
    get {
      if (planPickerMetadata == null) {
        planPickerMetadata = new Map<String, OrdwayLabs__PlanPicker__mdt>();
        for (OrdwayLabs__PlanPicker__mdt thisPlanPicker : [SELECT Id,
                                                              DeveloperName,
                                                              OrdwayLabs__Configuration__c
                                                            FROM OrdwayLabs__PlanPicker__mdt 
                                                            LIMIT 100]) {
            planPickerMetadata.put(SObjectFieldMappingController.getUniqueMetadataKeyValue(objectAPINameMap.get(thisPlanPicker.DeveloperName)), thisPlanPicker);
        }
      }

      return planPickerMetadata;
    }
    private set;
  }

  public static OrdwayLabs__PlanPicker__mdt getPlanPickerByObjectAPIName(String objectAPIName) {
    if(!planPickerMetadata.isEmpty()
      && planPickerMetadata.containsKey(SObjectFieldMappingController.getUniqueMetadataKeyValue(objectApiName))){
      return planPickerMetadata.get(SObjectFieldMappingController.getUniqueMetadataKeyValue(objectApiName));
    }
    return null;
  }

  public static String getMetadataDeploymentURL(Id deploymentJobId) {
    return URL.getSalesforceBaseUrl().toExternalForm()+'/changemgmt/monitorDeploymentsDetails.apexp?retURL=/changemgmt/monitorDeployment.apexp&asyncId='+deploymentJobId;
  }

  public class DynamicReference {
    //Container Object
    public String developerName;
    public String containerObjectName;
    public Schema.SobjectField containerObjectExternalID;

    //Primary Relationship Details
    public String primaryObjectName;
    public Schema.SobjectField primaryExternalReference;
    public Schema.SobjectField primaryRelationshipField;
    public Schema.SobjectField externalReferenceInPrimaryObject;
    
    //Secondary Relationship Details
    public String secondaryObjectName;
    public Schema.SobjectField secondaryExternalReference;
    public Schema.SobjectField secondaryRelationshipField;
    public Schema.SobjectField externalReferenceInSecondaryObject;

    //Child Object Relationship Details
    public Boolean hasChildRelationship;
    public String childObjectName;
    public Schema.SobjectField childPrimaryExternalReference;
    public Schema.SobjectField childPrimaryRelationshipField;

    public String childSecondaryObjectName;
    public Schema.SobjectField childSecondaryRelationshipField;
    public Schema.SobjectField childExternalReferenceInSecondaryObject;
    public Schema.SobjectField childSecondaryExternalReference;

    //Junction Object Details
    public Boolean hasManyToManyRelationship;
    public String junctionObjectName;
    public Schema.SobjectField primaryJunctionRelationshipField;
    public Schema.SobjectField secondaryJunctionRelationshipField;
    public String primaryObjectNameForJO;
    public Schema.SobjectField externalReferenceInPrimaryObjectForJO;
  
    public DynamicReference(DynamicObjectMapping__mdt dynamicObjectMapping){

      //Container Object 
      developerName                       = dynamicObjectMapping.DeveloperName;
      containerObjectName                 = dynamicObjectMapping.Object__r.QualifiedApiName;
      containerObjectExternalID           = getFieldName(containerObjectName, dynamicObjectMapping.ExternalID__r.QualifiedApiName);

      //Primary Relationship Details
      primaryObjectName                   = dynamicObjectMapping.PrimaryObject__r.QualifiedApiName;
      primaryExternalReference            = getFieldName(containerObjectName, dynamicObjectMapping.PrimaryExternalID__r.QualifiedApiName);
      primaryRelationshipField            = getFieldName(containerObjectName, dynamicObjectMapping.PrimaryRelationship__r.QualifiedApiName);
      externalReferenceInPrimaryObject    = getFieldName(primaryObjectName, dynamicObjectMapping.PrimaryObjectExternalId__r.QualifiedApiName);

      //Secondary Relationship Details
      secondaryObjectName                 = dynamicObjectMapping.SecondaryObject__r.QualifiedApiName;
      secondaryExternalReference          = getFieldName(containerObjectName, dynamicObjectMapping.SecondaryExternalID__r.QualifiedApiName);
      secondaryRelationshipField          = getFieldName(containerObjectName, dynamicObjectMapping.SecondaryRelationship__r.QualifiedApiName);
      externalReferenceInSecondaryObject  = getFieldName(secondaryObjectName, dynamicObjectMapping.SecondaryObjectExternalId__r.QualifiedApiName);
      
      //Child Object Relationship Details
      hasChildRelationship                = dynamicObjectMapping.ChildObject__c != null ? true : false;
      childObjectName                     = dynamicObjectMapping.ChildObject__r.QualifiedApiName;
      childPrimaryExternalReference       = getFieldName(childObjectName, dynamicObjectMapping.ChildExternalID__r.QualifiedApiName);
      childPrimaryRelationshipField       = getFieldName(childObjectName, dynamicObjectMapping.ParentRelationshipField__r.QualifiedApiName);

      childSecondaryObjectName            = dynamicObjectMapping.ChildSecondaryObject__r.QualifiedApiName;
      childSecondaryRelationshipField     = getFieldName(childObjectName, dynamicObjectMapping.ChildSecondaryRelationshipField__r.QualifiedApiName);
      childExternalReferenceInSecondaryObject = getFieldName(childSecondaryObjectName, dynamicObjectMapping.ChildSecondaryObjectExternalId__r.QualifiedApiName);
      childSecondaryExternalReference     = getFieldName(childObjectName, dynamicObjectMapping.ChildSecondaryExternalId__r.QualifiedApiName);

      //Junction Object Details
      hasManyToManyRelationship           = dynamicObjectMapping.IsManyToMany__c;
      junctionObjectName                  = dynamicObjectMapping.JunctionObject__r.QualifiedApiName;
      primaryJunctionRelationshipField    = getFieldName(junctionObjectName, dynamicObjectMapping.PrimaryJunctionRelationship__r.QualifiedApiName);
      secondaryJunctionRelationshipField  = getFieldName(junctionObjectName, dynamicObjectMapping.SecondaryJunctionRelationship__r.QualifiedApiName);
      primaryObjectNameForJO              = dynamicObjectMapping.PrimaryObjectForJunctionObject__r.QualifiedApiName;
      externalReferenceInPrimaryObjectForJO = getFieldName(primaryObjectNameForJO, dynamicObjectMapping.ExternalID_PrimaryObjectForJO__r.QualifiedApiName);
    }
  }
}