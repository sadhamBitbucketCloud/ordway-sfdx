/**
 * @File Name          : ApplicationSettingController_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 01-03-2021
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    12/31/2019   Ordway Labs     Initial Version
 **/
@isTest
private class ApplicationSettingController_Test {
  @isTest
  private static void test_ApplicationSetting() {
    Test.startTest();
    ApplicationSetting__c newApplicationSetting = TestObjectFactory.createApplicationSetting();
    insert newApplicationSetting;

    ApplicationSetting__c thisAppSetting = ApplicationSettingController.getApplicationSetting()
      .applicationSetting;
    System.assertEquals(
      thisAppSetting.Name,
      OrdwayHelper.ORDWAY_APPLICATION_SETTING,
      'Should return the custom setting name as inserted'
    );

    ApplicationSettingController.isConnectedToOrdwayProduction();
    ApplicationSettingController.isOrdwayPermissionAssignedToUser();
    ApplicationSettingController.getOrdwayNamedCredential();
    ApplicationSettingController.updateApplicationSetting(
      'np@cloudiate.com',
      'Cloudiate',
      'Cloudiate',
      'OrdwaySandbox'
    );
    ApplicationSettingController.disconnect();
    Test.stopTest();
  }

  @isTest
  private static void test_ApplicationSetting_ValidateToken() {
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new MockValidateTokenCallout_Test());
    String responseStr = ApplicationSettingController.validateToken(
      'np@cloudiate.com',
      'Cloudiate',
      'token',
      'OrdwaySandbox',
      false
    );
    System.assertEquals('You are connected successfully', responseStr);
    Test.stopTest();
  }

  @isTest
  private static void test_ApplicationSettingTestConnection() {
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new MockValidateTokenCallout_Test());
    String responseStr = ApplicationSettingController.validateToken(
      'np@cloudiate.com',
      'Cloudiate',
      'token',
      'OrdwaySandbox',
      true
    );
    System.assertEquals('You are connected successfully', responseStr);
    Test.stopTest();
  }
}