/**
 * @File Name          : OrdwayQLITierTriggerHandler_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-04-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    09/04/2021        Initial Version
 **/
@isTest
private class OrdwayQLITierTriggerHandler_Test {
  @isTest
  private static void quoteLineItemTierDeleteException() {

    Account thisAccount = TestObjectFactory.createAccount();
    insert thisAccount;

    Opportunity thisOpportunity = TestObjectFactory.createOpportunity();
    insert thisOpportunity;

    Quote__c thisQuote       = TestObjectFactory.createQuote();
    thisQuote.Account__c     = thisAccount.Id;
    thisQuote.Opportunity__c = thisOpportunity.Id;
    insert thisQuote;

    Test.startTest();
    QuoteLineItem__c thisQLI = TestObjectFactory.createQuoteLineItem();
    thisQLI.Name             = 'Test QLI';
    thisQLI.Quote__c         = thisQuote.Id;
    insert thisQLI;
      
    QuoteLineItemTier__c thisQLITier = TestObjectFactory.createQuoteLineItemTier();
    thisQLITier.QuoteLineItem__c     = thisQLI.Id;
    thisQLITier.StartingUnit__c      = 123;
    insert thisQLITier;

    try{
      delete thisQLITier;
     } catch (Exception ex){
       system.assertNotEquals(null, ex.getMessage());
    }
    Test.stopTest();
    System.assertEquals(1, [SELECT Id FROM QuoteLineItemTier__c].SIZE());
    }
}