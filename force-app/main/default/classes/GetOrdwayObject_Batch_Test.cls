/**
 * @File Name          : GetOrdwayObject_Batch_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 01-02-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/16/2020        Initial Version
 **/
@isTest
private class GetOrdwayObject_Batch_Test {
  @isTest
  private static void GetOrdwayObjectAccount_200() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Account newAccount = TestObjectFactory.createAccount();
    newAccount.OrdwayCustomerId__c = 'C-00106';
    newAccount.Name = 'ordway.com';
    insert newAccount;

    Account newAccount1 = TestObjectFactory.createAccount();
    newAccount1.OrdwayCustomerId__c = 'C-00107';
    newAccount1.Name = 'Test Account';
    insert newAccount1;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayAccountString;
    RESPONSEBODY = RESPONSEBODY.replace('newAccountId', newAccount.Id);
    RESPONSEBODY = RESPONSEBODY.replace('newAccountId1', newAccount1.Id);
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    Database.executebatch(new GetOrdwayObject_Batch(1, 'Account', '', null));
    Test.stopTest();
    system.assertEquals(
      2,
      [SELECT Id, Name FROM Account].size(),
      'should return 2 accounts as in response'
    );
  }


  @isTest
  private static void GetOrdwayObjectAccount_BulkSync() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    BulkSyncConfiguration__c thisBulkSyncConfig = TestObjectFactory.createBulkSyncConfigurationSetting();
    insert thisBulkSyncConfig;

    Account newAccount = TestObjectFactory.createAccount();
    newAccount.OrdwayCustomerId__c = 'C-00106';
    newAccount.Name = 'ordway.com';
    insert newAccount;

    Account newAccount1 = TestObjectFactory.createAccount();
    newAccount1.OrdwayCustomerId__c = 'C-00107';
    newAccount1.Name = 'Test Account';
    insert newAccount1;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayAccountString;
    RESPONSEBODY = RESPONSEBODY.replace('newAccountId', newAccount.Id);
    RESPONSEBODY = RESPONSEBODY.replace('newAccountId1', newAccount1.Id);
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );

    AutoBulkSync_Scheduler thisScheduler = new AutoBulkSync_Scheduler();
    String sch ='0 48 * * * ?'; 
    System.schedule('Schedule', sch,thisScheduler);
    Test.stopTest();
    system.assertEquals(
      2,
      [SELECT Id, Name FROM Account].size(),
      'should return 2 accounts as in response'
    );
  }

  /* @isTest
    private static void GetOrdwayObjectAccountParentToChild_200() {
        ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
        insert thisApplicationsetting;

        Account newParentAccount = TestObjectFactory.createAccount();
        newParentAccount.OrdwayCustomerId__c = 'C-00106';
        newParentAccount.Name = 'ordwayTest';
        insert newParentAccount;

        Account newAccount = TestObjectFactory.createAccount();
        newAccount.OrdwayCustomerId__c = 'C-00107';
        newAccount.Name = 'ordway.com';
        insert newAccount;

        Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
        Test.startTest();
            Integer RESPONSECODE = 200;
            String RESPONSEBODY = OrdwayService_Mock.ordWayAccountString;
            RESPONSEBODY = RESPONSEBODY.replace('newAccountId1', newParentAccount.Id);
            RESPONSEBODY = RESPONSEBODY.replace('newAccountId', newAccount.Id);
            Test.setMock(HttpCalloutMock.class, new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY.replace('testParentId', String.valueOf('C-00106'))));
            Database.executebatch(new GetOrdwayObject_Batch(1, 'Account', null, null));
        Test.stopTest();
         Account accountToAssert = [SELECT Id, OrdwayCustomerId__c, ParentId FROM Account WHERE OrdwayCustomerId__c = 'C-00107'];
         system.assertEquals(accountToAssert.ParentId, newParentAccount.Id, 'should return same as Parent Id');
        
    }*/

  @isTest
  private static void GetOrdwayObjectAccount_400() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 400;
    String RESPONSEBODY = '{ "errors":{"details": "Bad Request"}}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    try {
      Database.executebatch(
        new GetOrdwayObject_Batch(1, 'Account', null, null)
      );
    } catch (Exception e) {
    }
    Test.stopTest();
    System.assertEquals(
      0,
      [SELECT Id, Name FROM Account].size(),
      'should not return any accounts'
    );
  }

  @isTest
  private static void GetOrdwayObjectAccount_401() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 401;
    String RESPONSEBODY = '{ "errors":{"details": "Unauthorized"}}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    try {
      Database.executebatch(
        new GetOrdwayObject_Batch(1, 'Account', null, null)
      );
    } catch (Exception e) {
    }
    Test.stopTest();
    System.assertEquals(
      0,
      [SELECT Id, Name FROM Account].size(),
      'should not return any accounts'
    );
  }

  @isTest
  private static void GetOrdwayObjectAccount_500() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 500;
    String RESPONSEBODY = '{ "errors":{"details": "Internal Server Error"}}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    try {
      Database.executebatch(
        new GetOrdwayObject_Batch(1, 'Account', null, null)
      );
    } catch (Exception e) {
    }
    Test.stopTest();
    System.assertEquals(
      0,
      [SELECT Id, Name FROM Account].size(),
      'should not return any accounts'
    );
  }

  @isTest
  private static void GetOrdwayObjectInvoice_200() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Map<String, DynamicObjectMapping__mdt> dynamicObjectMapping = MetadataService.dynamicObjectMapping;

    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayInvoiceString;
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    Database.executebatch(new GetOrdwayObject_Batch(1, 'Invoice', null, null));
    Test.stopTest();
    Invoice__c invoiceToAssert = [SELECT Id, Name FROM Invoice__c LIMIT 1];
    system.assert(invoiceToAssert.Name.Contains('INV-0'));
    System.assertEquals(
      3,
      [SELECT Id FROM Invoice__c].size(),
      'should return 3 invoices same as response'
    );
    System.assertEquals(
      28,
      [SELECT Id FROM InvoiceLineItem__c].size(),
      'should return 28 invoice line items same as response'
    );
  }

  @isTest
  private static void GetOrdwayObjectInvoiceWithEntity_200() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    Map<String, DynamicObjectMapping__mdt> dynamicObjectMapping = MetadataService.dynamicObjectMapping;

    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayInvoiceString;
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    Database.executebatch(
      new GetOrdwayObject_Batch(1, 'Invoice', null, thisEntity.EntityID__c)
    );
    Test.stopTest();
    Invoice__c invoiceToAssert = [
      SELECT Id, Name, ExternalID__c
      FROM Invoice__c
      LIMIT 1
    ];
    system.assert(invoiceToAssert.Name.Contains('INV-0'));
    system.assert(invoiceToAssert.ExternalID__c.Contains('TEST'));
    System.assertEquals(
      3,
      [SELECT Id FROM Invoice__c].size(),
      'should return 3 invoices same as response'
    );
    System.assertEquals(
      28,
      [SELECT Id FROM InvoiceLineItem__c].size(),
      'should return 28 invoice line items same as response'
    );
  }

  @isTest
  private static void GetOrdwayObjectPayment_200() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayPaymentString;
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    Database.executebatch(new GetOrdwayObject_Batch(1, 'Payment', null, null));
    Test.stopTest();
    System.assertEquals(
      4,
      [SELECT Id FROM Payment__c].size(),
      'should return 4 payments same as response'
    );
  }

  @isTest
  private static void GetOrdwayObjectPaymentWithEntity_200() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayPaymentString;
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    Database.executebatch(
      new GetOrdwayObject_Batch(1, 'Payment', null, thisEntity.EntityID__c)
    );
    Test.stopTest();
    Payment__c paymentToAssert = [
      SELECT Id, ExternalID__c
      FROM Payment__c
      LIMIT 1
    ];
    system.assert(paymentToAssert.ExternalID__c.Contains('TEST'));
    System.assertEquals(
      4,
      [SELECT Id FROM Payment__c].size(),
      'should return 4 payments same as response'
    );
  }

  @isTest
  private static void GetOrdwayObjectPaymentUpdate_200() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Payment__c newPayment = TestObjectFactory.createOrdwayPayment();
    newPayment.ExternalID__c = 'PMT-00035';
    insert newPayment;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayPaymentString;
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    Database.executebatch(new GetOrdwayObject_Batch(1, 'Payment', null, null));
    Test.stopTest();
    Payment__c paymentToAssert = [
      SELECT Id, CustomerID__c, ExternalID__c
      FROM Payment__c
      WHERE ExternalID__c = :'PMT-00035'
    ];
    system.assertEquals(
      'C-00027',
      paymentToAssert.CustomerID__c,
      'should return same as response'
    );
    system.assertEquals(
      4,
      [SELECT Id FROM Payment__c].size(),
      'should return 4 payments'
    );
  }

  @isTest
  private static void GetOrdwayObjectPayment_400() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;
    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 400;
    String RESPONSEBODY = '{ "errors":{"details": "Bad Request"}}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    try {
      Database.executebatch(
        new GetOrdwayObject_Batch(1, 'Payment', null, null)
      );
    } catch (Exception e) {
    }
    System.assertEquals(
      0,
      [SELECT Id FROM Payment__c].size(),
      'should not return any payments'
    );
    Test.stopTest();
  }

  @isTest
  private static void GetOrdwayObjectPayment_401() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;
    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 401;
    String RESPONSEBODY = '{ "errors":{"details": "Unauthorized"}}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    try {
      Database.executebatch(
        new GetOrdwayObject_Batch(1, 'Payment', null, null)
      );
    } catch (Exception e) {
    }
    System.assertEquals(
      0,
      [SELECT Id FROM Payment__c].size(),
      'should not return any payments'
    );
    Test.stopTest();
  }

  @isTest
  private static void GetOrdwayObjectPayment_500() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;
    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 500;
    String RESPONSEBODY = '{ "errors":{"details": "Internal Server Error"}}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    try {
      Database.executebatch(
        new GetOrdwayObject_Batch(1, 'Payment', null, null)
      );
    } catch (Exception e) {
    }
    System.assertEquals(
      0,
      [SELECT Id FROM Payment__c].size(),
      'should not return any payments'
    );
    Test.stopTest();
  }

  @isTest
  private static void GetOrdwayObjectCredit_200() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayCreditString;
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    Database.executebatch(new GetOrdwayObject_Batch(1, 'Credit', null, null));
    Test.stopTest();
    system.assertEquals(
      3,
      [SELECT Id FROM Credit__c].size(),
      'should return 3 as we have credits in response'
    );
  }

  @isTest
  private static void GetOrdwayObjectCreditWithEntity_200() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayCreditString;
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    Database.executebatch(
      new GetOrdwayObject_Batch(1, 'Credit', null, thisEntity.EntityID__c)
    );
    Test.stopTest();
    Credit__c creditToAssert = [
      SELECT Id, ExternalID__c
      FROM Credit__c
      LIMIT 1
    ];
    system.assert(creditToAssert.ExternalID__c.Contains('TEST'));
    system.assertEquals(
      3,
      [SELECT Id FROM Credit__c].size(),
      'should return 3 as we have credits in response'
    );
  }

  @isTest
  private static void GetOrdwayObjectRefund_200() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayRefundString;
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    Database.executebatch(new GetOrdwayObject_Batch(1, 'Refund', null, null));
    Test.stopTest();
    Refund__c refundToAssert = [SELECT Id, Name FROM Refund__c LIMIT 1];
    system.assert(refundToAssert.Name.Contains('REF-00'));
    system.assertEquals(
      3,
      [SELECT Id FROM Refund__c].size(),
      'should return 3 as we have refunds in response'
    );
  }

  @isTest
  private static void GetOrdwayObjectRefundWithEntity_200() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayRefundString;
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    Database.executebatch(
      new GetOrdwayObject_Batch(1, 'Refund', null, thisEntity.EntityID__c)
    );
    Test.stopTest();
    Refund__c refundToAssert = [
      SELECT Id, Name, ExternalID__c
      FROM Refund__c
      LIMIT 1
    ];
    system.assert(refundToAssert.Name.Contains('REF-00'));
    system.assert(refundToAssert.ExternalID__c.Contains('TEST'));
    system.assertEquals(
      3,
      [SELECT Id FROM Refund__c].size(),
      'should return 3 as we have refunds in response'
    );
  }
}