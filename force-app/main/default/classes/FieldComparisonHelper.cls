/**
 * @File Name          : FieldComparisonHelper.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/27/2020        Initial Version
 **/
public with sharing class FieldComparisonHelper {
  /**
  * @description Method to retrieve Object Data wrapper used in field comparison component
                 which contains which is used for comparing record data in org with the record data in Ordway.
  * @param sObjectlist sObject List for which Comparison Data Wrapper needs to be constructed
  * @param ordwayPopulatedValuesAsMap Ordway Record values populated in Map with record Id as Key
  * @return Map<String, List<objectDataWrapper>> Map of Object Comparison Wrapper
  **/
  public static Map<String, List<objectDataWrapper>> getRecordWrapper(
    List<sObject> sObjectlist,
    Map<String, Map<String, Object>> ordwayPopulatedValuesAsMap
  ) {
    Map<String, String> fieldDataLabelMap = new Map<String, String>();
    Map<String, Map<String, String>> objectFieldDataLabelMap = new Map<String, Map<String, String>>();
    Map<String, List<objectDataWrapper>> objectDataMap = new Map<String, List<objectDataWrapper>>();

    for (sObject thisSObject : sObjectlist) {
      objectDataWrapper thisRecordWrapper = new objectDataWrapper();
      thisRecordWrapper.key = String.valueOf(
        thisSObject.get(OrdwayHelper.SOBJECT_FIELD_NAME)
      );
      thisRecordWrapper.recordId = thisSObject.Id != null
        ? String.valueOf(thisSObject.Id)
        : String.valueOf(
            thisSObject.get('OrdwayLabs__SubscriptionID_LineID__c')
          );
      thisRecordWrapper.value = new List<DataWrapper>();

      Map<String, Object> fieldsToValue = thisSObject.getPopulatedFieldsAsMap();

      String sObjectType = String.valueOf(thisSObject.getSobjectType());

      if (objectFieldDataLabelMap.get(sObjectType) == null) {
        objectFieldDataLabelMap.put(sObjectType, new Map<String, String>{});
        objectFieldDataLabelMap.get(sObjectType)
          .putAll(SObjectHelper.getSobjectFieldLabel(sObjectType));
      }
      fieldDataLabelMap = objectFieldDataLabelMap.get(sObjectType);

      for (String fieldName : fieldsToValue.keySet()) {
        //Skip If It Doesn't Exists
        if (
          fieldDataLabelMap.get(fieldName.toLowerCase()) == null ||
          fieldName.contains('SyncStatus__c')
        )
          continue;

        //We need to map only the fields present in Ordway response
        if (
          ordwayPopulatedValuesAsMap != null &&
          ordwayPopulatedValuesAsMap.get(thisRecordWrapper.recordId) != null &&
          ordwayPopulatedValuesAsMap.get(thisRecordWrapper.recordId)
            .get(fieldName) == null
        )
          continue;
        thisRecordWrapper.value.add(
          new DataWrapper(
            thisRecordWrapper.recordId,
            fieldDataLabelMap.get(fieldName.toLowerCase()),
            fieldName,
            thisSObject.Id != null ? fieldsToValue.get(fieldName) : 'N/A',
            ordwayPopulatedValuesAsMap
          )
        );
      }

      if (objectDataMap.get(sObjectType) == null) {
        objectDataMap.put(sObjectType, new List<objectDataWrapper>{});
      }
      objectDataMap.get(sObjectType).add(thisRecordWrapper);
    }
    return objectDataMap;
  }

  /**
   * @description Object Data wrapper used in field comparison component
   **/
  public class objectDataWrapper {
    @AuraEnabled
    public String key;
    @AuraEnabled
    public String recordId;
    @AuraEnabled
    public List<DataWrapper> value;
    @AuraEnabled
    public OrdwayContract__c ordwayContractObjectToUpdate;
    @AuraEnabled
    public List<ContractLineItem__c> contractLineItemListToUpdate;
  }

  /**
   * @description Object Data wrapper Individual fields data wrapper
   **/
  public class DataWrapper {
    @AuraEnabled
    public String fieldLabel;
    @AuraEnabled
    public String fieldAPIName;
    @AuraEnabled
    public String fieldValueSalesforce;
    @AuraEnabled
    public String fieldValueOrdway;

    DataWrapper(
      String recordId,
      String fieldLabel,
      String fieldAPIName,
      Object fieldValueSalesforce,
      Map<String, Map<String, Object>> ordwayPopulatedValuesAsMap
    ) {
      this.fieldLabel = fieldLabel;
      this.fieldAPIName = fieldAPIName;
      this.fieldValueSalesforce = fieldValueSalesforce != null
        ? getStringFieldValue(fieldValueSalesforce)
        : String.valueOf(fieldValueSalesforce);
      this.fieldValueOrdway = ordwayPopulatedValuesAsMap != null &&
        ordwayPopulatedValuesAsMap.get(recordId) != null &&
        ordwayPopulatedValuesAsMap.get(recordId).get(fieldAPIName) != null
        ? getStringFieldValue(
            ordwayPopulatedValuesAsMap.get(recordId).get(fieldAPIName)
          )
        : 'N/A';

      if(fieldAPIName == 'OrdwayLabs__ExternalID__c') {
        this.fieldValueSalesforce = String.valueOf(fieldValueSalesforce).split(':')[String.valueOf(fieldValueSalesforce).split(':').size() - 1];
      }
    }
  }

  /**
   * @description Method to get the field value as String
   * @param thisObject value which needs to be converted as string
   * @return String String value of the Object
   **/
  private static String getStringFieldValue(Object thisObject) {
    if (thisObject instanceof Date) {
      Date thisDateValue = Date.valueOf(thisObject);
      return DateTime.newInstance(
          thisDateValue.year(),
          thisDateValue.month(),
          thisDateValue.day()
        )
        .format('MM/dd/yyyy');
    } else if (thisObject instanceof Datetime) {
      return DateTime.valueOf(thisObject).format('MM/dd/yyyy');
    } else {
      return String.valueOf(thisObject);
    }
  }
}