/**
 * @File Name          : OpportunitySettingController.cls
 * @Description        : Opportunity Setting Controller
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 10-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    9/29/2019   Ordway Labs     Initial Version
 **/
public with sharing class OpportunitySettingController {
  @TestVisible
  public static Map<String, ContractPlanPickerSetting__c> ordwayConfiguration {
    get {
      if (ordwayConfiguration == null) {
        ordwayConfiguration = new Map<String, ContractPlanPickerSetting__c>();
        ordwayConfiguration = ContractPlanPickerSetting__c.getAll();
      }
      return ordwayConfiguration;
    }
    private set;
  }

  /**
   * @description Method to Get Opportunity Custom Setting
   * @return Map<String, ContractPlanPickerSetting__c> Map opportunity settings
   **/
  @AuraEnabled
  public static Map<String, ContractPlanPickerSetting__c> getOpportunitySetting() {
    return ordwayConfiguration;
  }

  /**
   * @description Method to get Configured Opportunity Sync Stage
   * @return String Configured Opportunity Sync Stage
   **/
  public static String getOpportunitySyncStage() {
    return !ordwayConfiguration.isEmpty()
      ? ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
          .OpportunityStage__c
      : null;
  }

  /**
   * @description Automatically Create Ordway Subscription In Salesforce At The Above Stage
   * @return True or false
   **/
  public static Boolean isAutomaticSyncEnabled() {
    return !ordwayConfiguration.isEmpty()
      ? ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
          .AutomaticallyCreateSubscription__c
      : false;
  }

  /**
   * @description Automatically Create Ordway Subscription In Salesforce At The Above Stage
   * @return True or false
   **/
  public static Boolean isAutoActivateContract() {
    return !ordwayConfiguration.isEmpty()
      ? ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
          .AutomaticallyActivateContract__c
      : false;
  }

  /**
   * @description Automatic Sync Between Salesforce And Ordway Platform
   * @return True or false
   **/
  public static Boolean isAutomaticLinkEnabled() {
    return !ordwayConfiguration.isEmpty()
      ? ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
          .LinkSubscriptionAutomatically__c
      : false;
  }

  /**
   * @description should use Ordway Calculation
   * @return True or false
   **/
  public static Boolean useOrdwayCalculation() {
    return !ordwayConfiguration.isEmpty()
      ? ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
          .UseOrdwayPricingCalculation__c
      : true;
  }

  /**
   * @description Method to get Ordway Pricebook Id
   * @return True or false
   **/
  public static String getOrdwayPriceBookId() {
    return !ordwayConfiguration.isEmpty() &&
      ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
        .PriceBook2Id__c != null
      ? ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
          .PriceBook2Id__c
      : null;
  }

  public static Boolean isAutoSyncAccountToOrdway() {
    return !ordwayConfiguration.isEmpty()
      ? ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
          .AutomaticallySyncAccount__c
      : false;
  }

  public static Boolean isAutoSyncContactToOrdway() {
    return !ordwayConfiguration.isEmpty()
      ? ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
          .AutomaticallySyncContact__c
      : false;
  }

  /**
   * @description Method to check whether Auto Sync from Ordway is enabled
   * @return True or false
   **/
  public static Boolean isAutoSyncFromOrdway() {
    return !ordwayConfiguration.isEmpty()
      ? ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
          .AutoSyncFromOrdway__c
      : false;
  }

  /**
   * @description Method to check whether Ordway Quote feature is enabled
   * @return True or false
   **/
  public static Boolean isQuotesEnabled() {
    return !ordwayConfiguration.isEmpty()
      ? ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
          .EnableQuotes__c
      : false;
  }

  /**
   * @description Method to check whether Auto Sync from Ordway is enabled for Quote
   * @return True or false
   **/
  public static Boolean isAutoSyncQuoteFromOrdway() {
    return !ordwayConfiguration.isEmpty()
      ? ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
          .AutoSyncQuotefromOrdway__c
      : false;
  }

  /**
   * @description Method to check whether Automatic Sync Between Salesforce And Ordway Platform is enabled
   * @return True or false
   **/
  public static Boolean syncQuoteAutomatically() {
    return !ordwayConfiguration.isEmpty()
      ? ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
          .SyncQuoteAutomatically__c
      : false;
  }

  /**
   * @description Method to check whether Multi Entity is enabled
   * @return True or false
   **/
  public static Boolean isMultiEntityEnabled() {
    return !ordwayConfiguration.isEmpty()
      ? ordwayConfiguration.get(OrdwayHelper.OPPORTUNITY_SETTING_NAME)
          .EnableMultiEntity__c
      : false;
  }

  /**
   * @description Method to check whether Org has Ordway Quote License
   * @return True or false
   **/
  @AuraEnabled
  public static Boolean hasQuoteLicense() {
    return isQuotesEnabled() ? true : FeatureAccessController.hasQuoteLicense();
  }

  /**
   * @description Method to Activate Ordway Quote Feature
   **/
  @AuraEnabled
  public static void activateQuote() {

    try {
      if(FeatureAccessController.hasQuoteLicense()) {
        MigrationAssistant.updateQuoteMetadataMapping();
        FeatureAccessController.changeQuoteProtection();
      }
      else {
        throw new FeatureAccessController.FeatureAccessException(
          'Ordway Quote feature not currently licensed. Please contact Ordway support for more details'
        );
      }
    }
    catch (Exception ex) {
      throw new FeatureAccessController.FeatureAccessException(ex.getMessage());
    }
  }

  /**
   *  @description Method to Save Opportunity Settings
   */
  @AuraEnabled
  public static void saveOpportunitySetting(
    OrdwayLabs__ContractPlanPickerSetting__c opportunitySettingInstance
  ) {
    ContractPlanPickerSetting__c newSetting = new ContractPlanPickerSetting__c();
    List<ContractPlanPickerSetting__c> newSettingList = new List<ContractPlanPickerSetting__c>();

    newSetting.AutomaticallyCreateSubscription__c = opportunitySettingInstance.OrdwayLabs__AutomaticallyCreateSubscription__c;
    newSetting.AutomaticallySyncAccount__c = opportunitySettingInstance.OrdwayLabs__AutomaticallySyncAccount__c;
    newSetting.AutomaticallySyncContact__c = opportunitySettingInstance.OrdwayLabs__AutomaticallySyncContact__c;
    newSetting.LinkSubscriptionAutomatically__c = opportunitySettingInstance.OrdwayLabs__LinkSubscriptionAutomatically__c;
    newSetting.UseOrdwayPricingCalculation__c = opportunitySettingInstance.OrdwayLabs__UseOrdwayPricingCalculation__c;
    newSetting.AutoSyncFromOrdway__c = opportunitySettingInstance.OrdwayLabs__AutoSyncFromOrdway__c;
    newSetting.AutomaticallyActivateContract__c = opportunitySettingInstance.OrdwayLabs__AutomaticallyActivateContract__c;
    newSetting.OpportunityStage__c = opportunitySettingInstance.OrdwayLabs__OpportunityStage__c;
    newSetting.PriceBook2Id__c = opportunitySettingInstance.OrdwayLabs__PriceBook2Id__c;
    newSetting.UseOrdwayPrice__c = opportunitySettingInstance.OrdwayLabs__UseOrdwayPrice__c;
    newSetting.SyncQuoteAutomatically__c = opportunitySettingInstance.OrdwayLabs__SyncQuoteAutomatically__c;
    newSetting.AutoSyncQuotefromOrdway__c = opportunitySettingInstance.OrdwayLabs__AutoSyncQuotefromOrdway__c;
    newSetting.EnableQuotes__c = opportunitySettingInstance.OrdwayLabs__EnableQuotes__c;
    newSetting.LastModifiedBy__c = UserInfo.getUserId();
    newSetting.Name = OrdwayHelper.OPPORTUNITY_SETTING_NAME;

    newSettingList.add(newSetting);

    try {
      upsert newSettingList Name;
    } catch (Exception thisException) {
      CustomExceptionData exceptionData = new CustomExceptionData(
        thisException.getTypeName(),
        thisException.getMessage()
      );
      AuraHandledException aE = new AuraHandledException(
        JSON.serialize(exceptionData)
      );
      aE.setMessage(JSON.serialize(exceptionData));
      throw aE;
    }
  }

  /**
   * @description Method to get Opportunity Stage
   * @return Map of Opportunity Stage values
   **/
  @AuraEnabled
  public static Map<String, String> getOpportunityStagewithoutRecordType() {
    Map<String, String> result = new Map<String, String>();
    if (Schema.sObjectType.Opportunity.fields.StageName.isAccessible()) {
      Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
      for (Schema.PicklistEntry stagePicklist : ple) {
        result.put(stagePicklist.getValue(), stagePicklist.getLabel());
      }
    }
    return result;
  }

  /**
   * @description Method to get Ordway Subscription Status
   * @return Ordway Contract Status
   **/
  @AuraEnabled
  public static Map<String, String> getOrdwaySubscriptionStatus() {
    Map<String, String> result = new Map<String, String>();
    if (
      Schema.sObjectType.OrdwayContract__c.fields.OrdwayContractStatus__c.isAccessible()
    ) {
      Schema.DescribeFieldResult fieldResult = OrdwayContract__c.OrdwayContractStatus__c.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
      for (Schema.PicklistEntry stagePicklist : ple) {
        result.put(stagePicklist.getValue(), stagePicklist.getLabel());
      }
    }
    return result;
  }

  /**
   * @description Method to get Existing Price books
   * @return Map of Price books
   **/
  @AuraEnabled
  public static Map<String, String> getExistingPricebook() {
    Map<String, String> priceBookMap = new Map<String, String>();
    if (Schema.sObjectType.Pricebook2.isAccessible()) {
      for (Pricebook2 thisPricebook2 : [
        SELECT Id, Name
        FROM Pricebook2
        WHERE isActive = TRUE
      ]) {
        if (!priceBookMap.containsKey(thisPricebook2.Id)) {
          priceBookMap.put(thisPricebook2.Id, thisPricebook2.Name);
        }
      }
    }
    return priceBookMap;
  }

  /**
   * @description Method to  get OpportunitySettingWrappper
   * @return OpportunitySettingWrappper
   **/
  @AuraEnabled
  public static OpportunitySettingWrappper getOpportunitySettingWrappper() {
    OpportunitySettingWrappper newOpportunitySettingWrappper = new OpportunitySettingWrappper();
    newOpportunitySettingWrappper.stageWithDefaultRecordType = getOpportunityStagewithoutRecordType();
    newOpportunitySettingWrappper.OpportunitySetting = ContractPlanPickerSetting__c.getValues(
        OrdwayHelper.OPPORTUNITY_SETTING_NAME
      ) == null
      ? new ContractPlanPickerSetting__c()
      : ContractPlanPickerSetting__c.getValues(
          OrdwayHelper.OPPORTUNITY_SETTING_NAME
        );
    newOpportunitySettingWrappper.ordwaySubscriptionStatus = getOrdwaySubscriptionStatus();
    newOpportunitySettingWrappper.pricebookMap = getExistingPricebook();
    newOpportunitySettingWrappper.hasQuoteLicense = isQuotesEnabled()
      ? true
      : FeatureAccessController.hasQuoteLicense();
    newOpportunitySettingWrappper.isMultiCurrencyOrganization = UserInfo.isMultiCurrencyOrganization();
    return newOpportunitySettingWrappper;
  }

  /**
   * @description Opportunity Setting Wrappper Class
   **/
  public class OpportunitySettingWrappper {
    @AuraEnabled
    public Map<String, String> stageWithDefaultRecordType { get; set; }
    @AuraEnabled
    public Map<String, String> ordwaySubscriptionStatus { get; set; }
    @AuraEnabled
    public Map<String, String> pricebookMap { get; set; }
    @AuraEnabled
    public ContractPlanPickerSetting__c opportunitySetting { get; set; }
    @AuraEnabled
    public Boolean hasQuoteLicense { get; set; }
    @AuraEnabled
    public Boolean isMultiCurrencyOrganization { get; set; }
  }
}