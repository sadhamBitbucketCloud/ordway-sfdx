/**
   * @File Name          : OrdwayService.cls
   * @Description        :
   * @Author             :
   * @Group              :
   * @Last Modified By   : 
   * @Last Modified On   : 04-17-2021
   * @Modification Log   :
   * Ver       Date            Author                 Modification
   * 1.0    9/28/2019   Ordway Labs     Initial Version
   **/
  public with sharing class OrdwayService {

  /**
  * @description Method to Sync Ordway Account To Ordway
  * @param thisSobject - Account Ordway account to be synced
  * @return HttpResponse
  **/
  public static HttpResponse syncAccountToOrdway( List<SObject> thisSObjectList ) {

    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT PUT/POST => Sync Account');

    HttpRequest thisRequest;
    Account thisAccount = (Account)thisSObjectList[0];
    ApplicationLog.addRecordId(thisAccount.Id);

    if (thisAccount.OrdwayCustomerID__c != null) {
      thisRequest = newRequest( getEndPoint(MetadataService.PUT_ACCOUNT), 'PUT', thisAccount.Id );
    } else {
      thisRequest = newRequest( getEndPoint(MetadataService.POST_ACCOUNT), 'POST', thisAccount.Id );
    }
    thisRequest.setBody(JSON.serializePretty(thisAccount));
    return OrdwayCallout(thisRequest);
  }

    /**
  * @description Method to Sync Ordway Contact To Ordway
  * @param thisOrdwayContact Ordway contact to be synced
  * @return HttpResponse
  **/
  public static HttpResponse syncContactToOrdway(List<SObject> thisSObjectList) {
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT PUT/POST => Sync Contact');

    HttpRequest thisRequest;
    Contact thisContact = (Contact)thisSObjectList[0];
    ApplicationLog.addRecordId(thisContact.Id);

    if (thisContact.OrdwayContactID__c != null) {
      thisRequest = newRequest( getEndPoint(MetadataService.PUT_CONTACT), 'PUT', thisContact.Id );
    } else {
      thisRequest = newRequest( getEndPoint(MetadataService.POST_CONTACT), 'POST', thisContact.Id );
    }
    thisRequest.setBody(JSON.serializePretty(thisContact));
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to get Ordway Quote Setting from Ordway
  **/
  public static HttpResponse getQuoteSetting() {
    HttpRequest thisRequest = newRequest( getEndPoint(MetadataService.QUOTE_SETTING), 'GET', null );
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to get Ordway Entity Setting from Ordway
  **/
  public static Boolean getEntitySetting() {
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT GET => Get Entity Setting');
    HttpRequest thisRequest = newRequest( getEndPoint(MetadataService.ENTITY_SETTING), 'GET', null );
    HttpResponse thisResponse;

    try {
      thisResponse = OrdwayCallout(thisRequest);
      Map<String, Object> entitySettingMap = new Map<String, Object>();
      entitySettingMap = (Map<String, Object>) JSON.deserializeUntyped( thisResponse.getBody() );
      Map<String, Object> planMap = (Map<String, Object>) JSON.deserializeUntyped( JSON.serialize(entitySettingMap.get('payload')) );
      return (Boolean) planMap.get('multi_entity');
    } 
    catch (Exception thisException) {
      throw new AuraHandledException( OrdwayHelper.getResponseStatusMessage(thisResponse));
    }
  }

  /**
  * @description Method to Create Ordway Plan In Ordway
  * @param Id record Id of the Ordway Plan to be created in Ordway
  * @return HttpResponse
  **/
  public static HttpResponse createOrdwayPlan(Id planId) {

    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT POST => Create Ordway Plan');
    ApplicationLog.addRecordId(planId);
    HttpRequest thisRequest = newRequest( getEndPoint(MetadataService.POST_PLAN), 'POST', planId);
    thisRequest.setBody(JSON.serializePretty(OrdwayObject.getPlan(planId)));
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Update Ordway Plan In Ordway
  * @param Id record Id of the Ordway Plan to be updated in Ordway
  * @return HttpResponse
  **/
  public static HttpResponse updateOrdwayPlan(Id planId) {

    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT PUT => Update Ordway Plan');
    ApplicationLog.addRecordId(planId);
    HttpRequest thisRequest = newRequest( getEndPoint(MetadataService.PUT_PLAN), 'PUT', planId);
    thisRequest.setBody(JSON.serializePretty(OrdwayObject.getPlan(planId)));
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Create Ordway Product In Ordway
  * @param Id record Id of the Ordway Product to be created in Ordway
  * @return HttpResponse
  **/
  public static HttpResponse createOrdwayProduct(Id recordId) {

    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT POST => Create Ordway Product');
    ApplicationLog.addRecordId(recordId);
    HttpRequest thisRequest = newRequest( getEndPoint(MetadataService.POST_PRODUCT), 'POST', recordId);
    thisRequest.setBody(JSON.serializePretty(OrdwayObject.getOrdwayProduct(recordId)));
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Update Ordway Product In Ordway
  * @param Id record Id of the Ordway Product to be updated in Ordway
  * @return HttpResponse
  **/
  public static HttpResponse updateOrdwayProduct(Id recordId) {

    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT PUT => Update Ordway Product');
    ApplicationLog.addRecordId(recordId);
    HttpRequest thisRequest = newRequest( getEndPoint(MetadataService.PUT_PRODUCT), 'PUT', recordId);
    thisRequest.setBody(JSON.serializePretty(OrdwayObject.getOrdwayProduct(recordId)));
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Sync Ordway Contract To Ordway
  * @param thisOrdwayContract Ordway contract to be synced
  * @return HttpResponse
  **/
  public static HttpResponse sendToOrdway( OrdwayContract__c thisOrdwayContract ) {

    if( getOrdwayContractAction(thisOrdwayContract) == OrdwayHelper.VERSION_TYPE_UPDATE ) {
      return updateSubscription(thisOrdwayContract);
    }
    else if( getOrdwayContractAction(thisOrdwayContract) == OrdwayHelper.VERSION_TYPE_CHANGE ) {
      return changeSubscription(thisOrdwayContract);
    }
    else if( getOrdwayContractAction(thisOrdwayContract) == OrdwayHelper.VERSION_TYPE_RENEW ) {
      return renewSubscription(thisOrdwayContract);
    }
    else {
      return createSubscription(thisOrdwayContract);
    }
  }

  /**
  * @description Method to Create Ordway Contract
  * @param thisOrdwayContract Ordway Contract to Create in Ordway
  * @return HttpResponse
  **/
  public static HttpResponse createSubscription( OrdwayContract__c thisOrdwayContract ) {
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT POST => Create Subscription');
    ApplicationLog.addRecordId(thisOrdwayContract.Id);
    HttpRequest thisRequest = newRequest( getEndPoint(MetadataService.SEND_CONTRACT_TO_ORDWAY), 'POST', thisOrdwayContract.Id );
    thisRequest.setBody(JSON.serializePretty(OrdwayObject.getContract(thisOrdwayContract.Id)));
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Get Ordway Subscription From Opportunity
  * @param subId Subscription Id
  * @param aToken Authorization Token value
  * @param contractId Ordway contract Id
  * @return HttpResponse
  **/
  public static HttpResponse getOrdwaySubscription( String subId, String aToken, String contractId ) {
    ApplicationLog.StartLoggingContext( 'OrdwayService', 'CALLOUT GET => Get Ordway Subscription' );
    String subscriptionId = EncodingUtil.urlEncode(subId, 'utf-8').replace('+', '%20');

    HttpRequest thisRequest = newRequest( getEndPoint(MetadataService.GET_ORDWAY_SUBSCRIPTION) + subscriptionId, 'GET', contractId );

    if ( UserInfo.getName() == OrdwayHelper.AUTOMATED_PROCESS && aToken != '' && aToken != null ) {
      thisRequest.setHeader('Authorization', 'Bearer ' + aToken);
      thisRequest.setHeader('Session-Id', aToken);
    }
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Activate Ordway Contract
  * @param thisOrdwayContract Ordway contract to be activated
  * @return HttpResponse
  **/
  public static HttpResponse activateSubscription( OrdwayContract__c thisOrdwayContract ) {
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT PUT => Activate Subscription');
    ApplicationLog.addRecordId(thisOrdwayContract.Id);
    String subId = EncodingUtil.urlEncode(thisOrdwayContract.OrdwaySubscriptionID__c, 'utf-8').replace('+', '%20');

    Map<String, Date> jsonBodyMap = new Map<String, Date>{ 'service_start_date' => thisOrdwayContract.ServiceStartDate__c, 'billing_start_date' => thisOrdwayContract.BillingStartDate__c };

    HttpRequest thisRequest = newRequest( getEndPoint(MetadataService.ACTIVATE_ORDWAY_CONTRACT).replace(':subId:', subId), 'PUT', thisOrdwayContract.Id );
    thisRequest.setBody(JSON.serializePretty(jsonBodyMap));

    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Change Ordway Contract
  * @param Id Id of the Ordway Contract to be renewed
  * @return HttpResponse
  **/
  public static HttpResponse changeSubscription( OrdwayContract__c thisOrdwayContract ) {
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT PUT => Change Subscription');
    ApplicationLog.addRecordId(thisOrdwayContract.Id);
    String subId = EncodingUtil.urlEncode(thisOrdwayContract.OrdwaySubscriptionID__c, 'utf-8').replace('+', '%20');

    HttpRequest thisRequest = newRequest( getEndPoint(MetadataService.PUT_ORDWAY_CONTRACT_CHANGE).replace(':subId:', subId), 'PUT', thisOrdwayContract.Id );
    thisRequest.setBody(JSON.serializePretty(OrdwayObject.getContract(thisOrdwayContract.Id)));

    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Update Ordway Contract
  * @param Id Id of the Ordway Contract to be renewed
  * @return HttpResponse
  **/
  public static HttpResponse updateSubscription( OrdwayContract__c thisOrdwayContract ) {
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT PUT => Update Subscription');
    ApplicationLog.addRecordId(thisOrdwayContract.Id);
    String subId = EncodingUtil.urlEncode(thisOrdwayContract.OrdwaySubscriptionID__c, 'utf-8').replace('+', '%20');

    HttpRequest thisRequest = newRequest( getEndPoint(MetadataService.PUT_ORDWAY_CONTRACT_UPDATE).replace(':subId:', subId), 'PUT', thisOrdwayContract.Id );
    thisRequest.setBody(JSON.serializePretty(OrdwayObject.getContract(thisOrdwayContract.Id)));

    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Renew Ordway Contract
  * @param Id Id of the Ordway Contract to be renewed
  * @return HttpResponse
  **/
  public static HttpResponse renewSubscription( OrdwayContract__c thisOrdwayContract ) {
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT PUT => Renew Subscription');
    ApplicationLog.addRecordId(thisOrdwayContract.Id);
    String subId = EncodingUtil.urlEncode(thisOrdwayContract.OrdwaySubscriptionID__c, 'utf-8').replace('+', '%20');

    HttpRequest thisRequest = newRequest( getEndPoint(MetadataService.PUT_ORDWAY_CONTRACT_RENEW).replace(':subId:', subId), 'PUT', thisOrdwayContract.Id );
    thisRequest.setBody(JSON.serializePretty(OrdwayObject.getContract(thisOrdwayContract.Id)));

    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Cancel Ordway Contract
  * @param thisOrdwayContract Ordway contract to be cancelled
  * @return HttpResponse
  **/
  public static HttpResponse cancelSubscription( OrdwayContract__c thisOrdwayContract ) {
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT PUT => Cancel Subscription');
    ApplicationLog.addRecordId(thisOrdwayContract.Id);
    String subId = EncodingUtil.urlEncode(thisOrdwayContract.OrdwaySubscriptionID__c, 'utf-8').replace('+', '%20');

    Map<String, Date> jsonBodyMap = new Map<String, Date>{ 'cancellation_date' => thisOrdwayContract.CancellationDate__c };

    HttpRequest thisRequest = newRequest( getEndPoint(MetadataService.CANCEL_ORDWAY_CONTRACT).replace(':subId:', subId), 'PUT', thisOrdwayContract.Id );
    thisRequest.setBody(JSON.serializePretty(jsonBodyMap));

    return OrdwayCallout(thisRequest);
  }

  public static HttpResponse injectRecordId(
    String requestBody,
    String xEntityId
  ) {
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT PUT => Inject ID');
    HttpRequest thisRequest = newRequest(
      getEndPoint(MetadataService.INJECT_ID_API),
      'PUT',
      xEntityId
    );
    thisRequest.setBody(requestBody);
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Sync Quote to Ordway
  * @param quoteId Ordway Quote Id to be synced
  * @return HttpResponse
  **/
  public static HttpResponse syncQuoteToOrdway(Id quoteId) {
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT PUT/POST => Sync Quote');
    ApplicationLog.addRecordId(quoteId);
    HttpRequest thisRequest;

    Quote__c thisQuote = [SELECT Id, OrdwayQuoteId__c FROM Quote__c WHERE Id =: quoteId];

    if (thisQuote.OrdwayQuoteId__c != null) {
      thisRequest = newRequest(
        getEndPoint(MetadataService.PUT_QUOTE),
        'PUT',
        quoteId
      );
    } else {
      thisRequest = newRequest(
        getEndPoint(MetadataService.POST_QUOTE),
        'POST',
        quoteId
      );
    }

    thisRequest.setBody(JSON.serializePretty(OrdwayObject.getQuote(quoteId)));

    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to get Ordway Quote from Ordway
  * @param quoteId Ordway Quote Id retrieved from Ordway
  * @param aToken Authorization Token Value
  * @return HttpResponse
  **/
  public static HttpResponse getOrdwayQuote(Id quoteId, String ordwayQuoteId, String aToken) {
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT GET => Get Ordway Quote');
    HttpRequest thisRequest = newRequest(
      getEndPoint(MetadataService.GET_QUOTE) + ordwayQuoteId,
      'GET',
      quoteId
    );

    if (
      UserInfo.getName() == OrdwayHelper.AUTOMATED_PROCESS &&
      aToken != '' &&
      aToken != null
    ) {
      thisRequest.setHeader('Authorization', 'Bearer ' + aToken);
      thisRequest.setHeader('Session-Id', aToken);
    }
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Convert Quote
  * @param quoteId Ordway Quote Id to be converted
  * @return HttpResponse
  **/
  public static HttpResponse convertQuote(Id quoteId, String ordwayQuoteId) {
    Map<String, String> jsonBodyMap = new Map<String, String>{
      'OrdwayLabs__OrdwayQuoteId__c' => ordwayQuoteId
    };
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT POST => Convert Quote');
    HttpRequest thisRequest = newRequest(
      getEndPoint(MetadataService.POST_CONVERT),
      'POST',
      quoteId
    );
    thisRequest.setBody(JSON.serializePretty(jsonBodyMap));
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Activate Ordway quote
  * @param quoteId Ordway quote Id to be activated
  * @return HttpResponse
  **/
  public static HttpResponse activateQuote(Id quoteId, String ordwayQuoteId) {
    Map<String, String> jsonBodyMap = new Map<String, String>{
      'OrdwayLabs__OrdwayQuoteId__c' => ordwayQuoteId
    };
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT POST => Activate Quote');
    HttpRequest thisRequest = newRequest(
      getEndPoint(MetadataService.POST_ACTIVATE),
      'POST',
      quoteId
    );
    thisRequest.setBody(JSON.serializePretty(jsonBodyMap));
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to Cancel Ordway quote
  * @param quoteId Ordway quote Id to be Cancelled
  * @return HttpResponse
  **/
  public static HttpResponse cancelQuote(Id quoteId, String ordwayQuoteId) {
    Map<String, String> jsonBodyMap = new Map<String, String>{
      'OrdwayLabs__OrdwayQuoteId__c' => ordwayQuoteId
    };
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT POST => Cancel Quote');
    HttpRequest thisRequest = newRequest(
      getEndPoint(MetadataService.POST_CANCEL),
      'POST',
      quoteId
    );
    thisRequest.setBody(JSON.serializePretty(jsonBodyMap));
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to get Ordway quote template from ordway
  * @param xEntityId Entity Id
  * @return HttpResponse
  **/
  public static HttpResponse getQuoteTemplate(String xEntityId) {
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT GET => Get Quote Template');
    HttpRequest thisRequest = newRequest(
      getEndPoint(MetadataService.GET_QUOTE_TEMPLATE),
      'GET',
      xEntityId
    );
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to get Ordway Plan from ordway
  * @param pageNumber page number for request
  * @param xEntityId Entity Id
  * @return HttpResponse
  **/
  public static HttpResponse getOrdwayPlan(
    Integer pageNumber,
    String xEntityId
  ) {
    ApplicationLog.StartLoggingContext('OrdwayService', 'CALLOUT GET => Get Ordway Plan');
    HttpRequest thisRequest = newRequest(
      getEndPoint(MetadataService.GET_ORDWAY_PLAN) +
      '&page=' +
      pageNumber,
      'GET',
      xEntityId
    );
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to get Ordway Objects from ordway
  * @param objectName Name of the ordway object to be retrieved from Ordway
  * @param pageNumber page number for request
  * @param qString Query String for request
  * @param xEntityId Entity Id
  * @return HttpResponse
  **/
  public static HttpResponse getOrdwayObjects(
    String objectName,
    Integer pageNumber,
    String qString,
    String xEntityId
  ) {
    ApplicationLog.StartLoggingContext(
      'OrdwayService',
      'CALLOUT GET => Bulk Import =>' + objectName
    );
    HttpRequest thisRequest = newRequest(
      getEndPoint(objectName) +
      'page=' +
      pageNumber +
      qString,
      'GET',
      xEntityId
    );
    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to validate Ordway Connection
  * @param email Email value
  * @param company Company value
  * @param token Authorization token
  * @param environment Enviroment value
  * @return HttpResponse
  **/
  public static HttpResponse validateToken(
    String email,
    String company,
    String token,
    String environment
  ) {
    String endPoint =
      getEndPoint(MetadataService.VALIDATE_TOKEN) +
      Datetime.now().format('yyyy-MM-dd');
    ApplicationLog.Info(endPoint);
    HttpRequest thisRequest = new HttpRequest();
    defaultHeader(
      thisRequest,
      email,
      token,
      company,
      endPoint,
      environment,
      null
    );
    thisRequest.setMethod('GET');

    return OrdwayCallout(thisRequest);
  }

  /**
  * @description Method to make a Http callout to ordway
  * @param thisRequest Http Request
  * @return HttpResponse
  **/
  private static HttpResponse OrdwayCallout(HttpRequest thisRequest) {
    HttpResponse thisResponse;
    try {
      ApplicationLog.Info('Request Endpoint => '+thisRequest.getEndpoint());
      ApplicationLog.Info('Request Method => '+thisRequest.getMethod());
      ApplicationLog.Info('Request Body => '+thisRequest.getBody());
      thisResponse = new Http().send(thisRequest);
      ApplicationLog.Info('Status Code => ' + thisResponse.getStatusCode());
      
      if(thisResponse.getStatusCode() >= 300) {
        ApplicationLog.Info(thisResponse.getBody());
        ApplicationLog.flagAsError();
      }
      else {
        ApplicationLog.Info('Response Data => '+ JSON.serializePretty(JSON.deserializeUntyped(thisResponse.getBody())));
      }
      ApplicationLog.FinishLoggingContext();
      return thisResponse;
    } catch (Exception ex) {
      ApplicationLog.Error(ex);
      ApplicationLog.FinishLoggingContext();
      return thisResponse;
    }
  }

  /**
  * @description Method to get Ordway contract action
  * @param thisOrdwayContract Ordway contract
  * @return String
  **/
  private static String getOrdwayContractAction(
    OrdwayContract__c thisOrdwayContract
  ) {
    if (
      thisOrdwayContract.VersionType__c == OrdwayHelper.VERSION_TYPE_NEW &&
      thisOrdwayContract.OrdwayContractStatus__c == OrdwayHelper.DRAFT &&
      thisOrdwayContract.OrdwaySubscriptionID__c != null
    ) {
      return OrdwayHelper.VERSION_TYPE_UPDATE;
    } else if (
      (thisOrdwayContract.VersionType__c == OrdwayHelper.VERSION_TYPE_CHANGE ||
      thisOrdwayContract.VersionType__c == OrdwayHelper.VERSION_TYPE_NEW) &&
      thisOrdwayContract.OrdwayContractStatus__c == OrdwayHelper.ACTIVE
    ) {
      return OrdwayHelper.VERSION_TYPE_CHANGE;
    }
    if (
      thisOrdwayContract.VersionType__c == OrdwayHelper.VERSION_TYPE_RENEWAL
    ) {
      return OrdwayHelper.VERSION_TYPE_RENEW;
    }
    return OrdwayHelper.VERSION_TYPE_NEW;
  }

  /**
  * @description Method to construct new Http Request
  * @param endPoint End point URL for the request
  * @param httpMethod Http Callout Method
  * @param thisSobjectId Sobject record Id
  * @return HttpRequest
  **/
  public static HttpRequest newRequest(
    String endPoint,
    String httpMethod,
    String thisSobjectId
  ) {
    ApplicationSetting__c thisSetting = ApplicationSettingController.applicationSetting;
    HttpRequest thisRequest = new HttpRequest();
    String thisSobjectEntityId = thisSobjectId;

    if (
      thisSobjectId != null && OrdwayHelper.isValidId(thisSobjectId) &&
      OpportunitySettingController.isMultiEntityEnabled()
    ) {
      thisSobjectEntityId = getEntityId(thisSobjectId);
    }

    defaultHeader(
      thisRequest,
      thisSetting.Email__c,
      thisSetting.Token__c,
      thisSetting.Company__c,
      endPoint,
      null,
      thisSobjectEntityId
    );
    thisRequest.setMethod(httpMethod);
    return thisRequest;
  }

  /**
  * @description Method to get End point URL
  * @param endPointName End point name whose endpoint URL needs to be retieved
  * @return String
  **/
  @TestVisible
  private static String getEndPoint(String endPointName) {
    return MetadataService.endPointMapping.get(endPointName);
  }

  /**
  * @description Method to construct Default header for the request
  * @param thisRequest Http Request for which default needs to be constructed
  * @param email Email value
  * @param token Authorization token
  * @param company COmpany value
  * @param endPoint End point URL
  * @param environment Enviroment value
  * @param xEntityId Entity Id
  **/
  @TestVisible
  private static void defaultHeader(
    HttpRequest thisRequest,
    String email,
    String token,
    String company,
    String endPoint,
    String environment,
    String xEntityId
  ) {
    thisRequest.setHeader('Accept', 'application/json');
    thisRequest.setHeader('Content-Type', 'application/json');

    //X-User-Email, X-User-Token, X-User-Company are no longer required
    //All API's except authentication API are built specifically for Salesforce Integration
    //Authentication mechanism for Salesforce API's are based on Salesforce Id
    //Salesforce makes Callout to Ordway along with Session-Id, and Ordway will verify the Session-Id
    if (email != null) {
      thisRequest.setHeader('X-User-Email', email);
    }

    if (token != null) {
      thisRequest.setHeader('X-User-Token', token);
    }

    if (company != null) {
      thisRequest.setHeader('X-User-Company', company);
    }

    if (
      xEntityId != null && OpportunitySettingController.isMultiEntityEnabled()
    ) {
      thisRequest.setHeader('X-Entity-Id', xEntityId);
      ApplicationLog.Info('X-Entity-Id => ' + xEntityId);
    }
    thisRequest.setHeader('Authorization', 'Bearer ' + getSessionId());
    thisRequest.setHeader('Session-Id', getSessionId());
    thisRequest.setTimeout(120000);

    thisRequest.setHeader(
      'X-Salesforce-Url',
      Url.getOrgDomainUrl().toExternalForm()
    );
    thisRequest.setEndpoint(getNamedCredential(environment) + endPoint);
  }

  /**
  * @description Method to retrieve Named credential End point URL
  * @return String  Named credential End point URL
  **/
  public static String getNamedCredentialURL() {

    String connectedInstance = ApplicationSettingController.getInstance();
    
    if(connectedInstance != null) {
      NamedCredential thisNamedCredential = [
        SELECT EndPoint
        FROM NamedCredential
        WHERE DeveloperName = :connectedInstance
      ];

      if(thisNamedCredential != null) {
        return thisNamedCredential.Endpoint;
      }
      else { return null; }
    }
    return null;
  }

  /**
  * @description Method to retrieve Named credential based on Enviroment
  * @return String  Named credential
  **/
  private static String getNamedCredential(String environment) {
    String connectedInstance = environment == null
      ? ApplicationSettingController.getInstance()
      : environment;

    if (connectedInstance == OrdwayHelper.ORDWAY_DEVELOPER) {
      return 'callout:OrdwayLabs__OrdwayDeveloper';
    } else if (connectedInstance == OrdwayHelper.ORDWAY_STAGING) {
      return 'callout:OrdwayLabs__OrdwayStaging';
    } else if (connectedInstance == OrdwayHelper.ORDWAY_PRODUCTION) {
      return 'callout:OrdwayLabs__OrdwayProduction';
    } else if (connectedInstance == OrdwayHelper.ORDWAY_SANDBOX) {
      return 'callout:OrdwayLabs__OrdwaySandbox';
    } else {
      return 'callout:' + connectedInstance;
    }
  }

  /**
  * @description Method to get session Id for making Callout to Ordway
  * @return String Session Id
  **/
  static String getSessionId() {
    PageReference p = Page.GetSessionId;
    String vfContent = Test.isRunningTest()
      ? 'Start_Of_Session_Id' + UserInfo.getSessionId() + 'End_Of_Session_Id'
      : p.getContent().toString();
    Integer startIndex =
      vfContent.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length();
    Integer endIndex = vfContent.indexOf('End_Of_Session_Id');

    return vfContent.substring(startIndex, endIndex);
  }

  /**
  * @description Method to get Ordway entity Id from Sobject
  * @param sObjectId Sobject Id from which Ordway entity Id needs to be retrieved
  * @return String Ordway entity Id
  **/
  public static String getEntityId(String sObjectId) {
    if (OpportunitySettingController.isMultiEntityEnabled()) {
      sObject thisSobject = Database.query(String.escapeSingleQuotes(
        'SELECT Id ,' +
        OrdwayHelper.SOBJECT_FIELD_ENTITYID +
        ' FROM ' +
        Id.valueOf(sObjectId).getSObjectType() +
        ' WHERE Id = :sObjectId LIMIT 1'
      ));

      return thisSobject.get(OrdwayHelper.SOBJECT_FIELD_ENTITYID) == null
        ? null
        : String.valueOf(thisSobject.get(OrdwayHelper.SOBJECT_FIELD_ENTITYID));
    }
    return null;
  }

  public static Map<Id,Entity__c> getEntityMap(Set<Id> entityIds) {
    Map<Id, Entity__c> entityMap = new Map<Id, Entity__c>([SELECT Id, Name, EntityID__c FROM Entity__c WHERE Id IN: entityIds]);
    return entityMap;
  }
}