/**
 * @File Name          : OrdwayObject_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-06-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    03/22/2021        Initial Version
 **/
@isTest
private class OrdwayObject_Test {
 @isTest
 private static void getContract() {

  OrdwayContract__c thisOrdwayContract = TestObjectFactory.createOrdwayContract();
  insert thisOrdwayContract;
 
  ContractLineItem__c thisOrdwayContractLineItem = TestObjectFactory.createContractLineItem();
  thisOrdwayContractLineItem.Name                ='Test CLI';
  thisOrdwayContractLineItem.OrdwayContract__c   = thisOrdwayContract.Id;
  insert thisOrdwayContractLineItem;

  ContractLineItemTier__c thisCLITier = TestObjectFactory.createContractLineItemTier();
  thisCLITier.StartingUnit__c         = 123;
  thisCLITier.ContractLineItem__c     = thisOrdwayContractLineItem.Id;
  insert thisCLITier;

  Test.startTest();
  Map<String, Map<String, Object>> thisContractMap = OrdwayObject.getContract(thisOrdwayContract.Id);
  Test.stopTest(); 
  
  System.AssertEquals(true, thisContractMap.get('contract').get('OrdwayLabs__AutoRenew__c'));
 }

 @isTest
 private static void getContractNegative() {

  OrdwayContract__c thisOrdwayContract = TestObjectFactory.createOrdwayContract();
  insert thisOrdwayContract;
 
  ContractLineItem__c thisOrdwayContractLineItem = TestObjectFactory.createContractLineItem();
  thisOrdwayContractLineItem.Name                ='Test CLI';
  thisOrdwayContractLineItem.OrdwayContract__c   = thisOrdwayContract.Id;
  insert thisOrdwayContractLineItem;

  ContractLineItemTier__c thisCLITier = TestObjectFactory.createContractLineItemTier();
  thisCLITier.ContractLineItem__c     = thisOrdwayContractLineItem.Id;
  insert thisCLITier;

  Test.startTest();
  try{
  OrdwayObject.getContract('Negative Testing');
  } catch (Exception ex) {
    system.assertNotEquals(null, ex.getMessage());
    }
  Test.stopTest();
 }

  @isTest
  private static void getQuote(){
   
    Account thisAccount = TestObjectFactory.createAccount();
    insert thisAccount;
    
    Opportunity thisOpportunity = TestObjectFactory.createOpportunity();
    insert thisOpportunity;
    
    Quote__c thisQuote       = TestObjectFactory.createQuote();
    thisQuote.Account__c     = thisAccount.Id;
    thisQuote.Opportunity__c = thisOpportunity.Id;
    insert thisQuote;
    
    QuoteLineItem__c thisQLI = TestObjectFactory.createQuoteLineItem();
    thisQLI.Name             = 'Test QLI';
    thisQLI.Quote__c         = thisQuote.Id;
    insert thisQLI;
        
    QuoteLineItemTier__c thisQLITier = TestObjectFactory.createQuoteLineItemTier();
    thisQLITier.QuoteLineItem__c     = thisQLI.Id;
    thisQLITier.StartingUnit__c      = 123;
    insert thisQLITier;

    Test.startTest();
    Map<String, Map<String, Object>> thisQuoteMap = OrdwayObject.getQuote(thisQuote.Id);
    Test.stopTest(); 
    
    System.AssertEquals(false, thisQuoteMap.get('quote').get('OrdwayLabs__AutoRenew__c'));
    
  }

  @isTest
  private static void getQuoteNegative(){
   
    Account thisAccount = TestObjectFactory.createAccount();
    insert thisAccount;
    
    Opportunity thisOpportunity = TestObjectFactory.createOpportunity();
    insert thisOpportunity;
    
    Quote__c thisQuote       = TestObjectFactory.createQuote();
    thisQuote.Account__c     = thisAccount.Id;
    thisQuote.Opportunity__c = thisOpportunity.Id;
    insert thisQuote;
    
    QuoteLineItem__c thisQLI = TestObjectFactory.createQuoteLineItem();
    thisQLI.Quote__c         = thisQuote.Id;
    insert thisQLI;
        
    QuoteLineItemTier__c thisQLITier = TestObjectFactory.createQuoteLineItemTier();
    thisQLITier.QuoteLineItem__c     = thisQLI.Id;
    insert thisQLITier;

    Test.startTest();
    try{
        OrdwayObject.getQuote('Negative Testing');
        } catch (Exception ex) {
          system.assertNotEquals(null, ex.getMessage());
          }
    Test.stopTest();  
  }

  @isTest
  private static void getPlan(){

   Plan__c thisPlan = TestObjectFactory.createOrdwayPlan();
   insert thisPlan;
  
   Charge__c thisCharge       = TestObjectFactory.createCharge();
   thisCharge.Name            = 'Test Charge';
   thisCharge.OrdwayPlan__c   =  thisPlan.Id;
   insert thisCharge;
  
   ChargeTier__c thisChargeTier   = TestObjectFactory.createChargeTier();
   thisChargeTier.StartingUnit__c = 123;
   thisChargeTier.Charge__c       = thisCharge.Id;
   insert thisChargeTier; 
  
   Test.startTest();
   Map<String, Object> thisPlanMap =  OrdwayObject.getPlan(thisPlan.Id);
   Test.stopTest(); 
   System.assertNotEquals(null, thisPlanMap);
  }

  @isTest
  private static void getPlanNegative(){

   Plan__c thisPlan = TestObjectFactory.createOrdwayPlan();
   insert thisPlan;
  
   Charge__c thisCharge       = TestObjectFactory.createCharge();
   thisCharge.OrdwayPlan__c   =  thisPlan.Id;
   insert thisCharge;
  
   ChargeTier__c thisChargeTier   = TestObjectFactory.createChargeTier();
   thisChargeTier.Charge__c       = thisCharge.Id;
   insert thisChargeTier; 
  
   Test.startTest();
   try{
    OrdwayObject.getPlan('Negative Testing');
    } catch (Exception ex) {
      system.assertNotEquals(null, ex.getMessage());
      }
   Test.stopTest();  
  }

  @isTest
  private static void getOrdwayProduct(){

   Product2 thisProduct = TestObjectFactory.createProduct();
   thisProduct.Name     = 'TestProduct';
   insert thisProduct;
      
   Pricebook2 thisPricebook = TestObjectFactory.createPricebook();
   insert thisPricebook;
      
   PricebookEntry thisPBEStandard = TestObjectFactory.createPriceBookEntry();
   thisPBEStandard.Product2Id     = thisProduct.Id;
   thisPBEStandard.Pricebook2Id   = Test.getStandardPricebookId();
   insert thisPBEStandard;
      
   PricebookEntry thisPBEntry = new PricebookEntry();
   thisPBEntry.Product2Id     = thisProduct.Id;
   thisPBEntry.Pricebook2Id   = thisPricebook.Id;
      
   PricebookEntry thisPBE = TestObjectFactory.createPriceBookEntry(thisPBEntry);
   insert thisPBE;
       
   Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
   thisOrdwayPlan.PlanDetails__c = thisProduct.Id;
   insert thisOrdwayPlan;

   OrdwayProduct__c thisOrdwayProduct  = TestObjectFactory.createOrdwayProduct();
   thisOrdwayProduct.Product2Id__c        = thisProduct.Id;
   insert thisOrdwayProduct;

   Test.startTest();
   OrdwayProduct__c thisOProduct = OrdwayObject.getOrdwayProduct(thisOrdwayProduct.Id);
   Test.stopTest();
   
   System.assertEquals(false, thisOProduct.Taxable__c);
  }

  @isTest
  private static void getOrdwayProductNegative(){

   Product2 thisProduct = TestObjectFactory.createProduct();
   insert thisProduct;
      
   Pricebook2 thisPricebook = TestObjectFactory.createPricebook();
   insert thisPricebook;
      
   PricebookEntry thisPBEStandard = TestObjectFactory.createPriceBookEntry();
   thisPBEStandard.Product2Id     = thisProduct.Id;
   thisPBEStandard.Pricebook2Id   = Test.getStandardPricebookId();
   insert thisPBEStandard;
      
   PricebookEntry thisPBEntry = new PricebookEntry();
   thisPBEntry.Product2Id     = thisProduct.Id;
   thisPBEntry.Pricebook2Id   = thisPricebook.Id;
      
   PricebookEntry thisPBE = TestObjectFactory.createPriceBookEntry(thisPBEntry);
   insert thisPBE;
       
   Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
   thisOrdwayPlan.PlanDetails__c = thisProduct.Id;
   insert thisOrdwayPlan;

   OrdwayProduct__c thisOrdwayProduct  = TestObjectFactory.createOrdwayProduct();
   thisOrdwayProduct.Product2Id__c        = thisProduct.Id;
   insert thisOrdwayProduct;

   Test.startTest();
   try{
    OrdwayObject.getOrdwayProduct('Negative Testing');
    } catch (Exception ex) {
      system.assertNotEquals(null, ex.getMessage());
      }
   Test.stopTest();
  }
}