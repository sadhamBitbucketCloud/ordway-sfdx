global with sharing class OrdwayAPI {
   
  global static HttpResponse activateSubscription(Id contractId) {
    return OrdwayContractController.activateSubscription([SELECT Id, 
                                                            OrdwaySubscriptionID__c, 
                                                            ServiceStartDate__c, 
                                                            BillingStartDate__c 
                                                          FROM OrdwayContract__c 
                                                          WHERE Id =: contractId]);
  }

  global static HttpResponse cancelSubscription(Id contractId) {
    return OrdwayContractController.cancelSubscription([SELECT Id, 
                                                            OrdwaySubscriptionID__c, 
                                                            CancellationDate__c
                                                          FROM OrdwayContract__c 
                                                          WHERE Id =: contractId]);
  }

  global static String createOrdwayContract(Id opportunityId) {
    return OrdwayButtonController.createOrdwayContract(opportunityId);
  }

  global static String updateOrdwayContract(Id opportunityId) {
    return OrdwayButtonController.updateOrdwayContract(opportunityId);
  }

  global static String convertQuote(Id quoteId) {
    return OrdwayQuoteController.convertQuote(quoteId);
  }

  global class ActivateSubscriptionQueueable implements Queueable {
    Set<Id> contractIds = new Set<Id>();
    global ActivateSubscriptionQueueable(Set<Id> contractIds){
        this.contractIds = contractIds ;  
    }
    global void execute(QueueableContext context) {
      Id recordId = (new List<Id>(contractIds))[0];
      activateSubscription(recordId);
      contractIds.remove(recordId);

      if(!contractIds.isEmpty()) {
        System.enqueueJob(new ActivateSubscriptionQueueable(contractIds));
      }
    }
  }

  global class CancelSubscriptionQueueable implements Queueable {
    Set<Id> contractIds = new Set<Id>();
    global CancelSubscriptionQueueable(Set<Id> contractIds){
        this.contractIds = contractIds ;  
    }
    global void execute(QueueableContext context) {
      Id recordId = (new List<Id>(contractIds))[0];
      cancelSubscription(recordId);
      contractIds.remove(recordId);

      if(!contractIds.isEmpty()) {
        System.enqueueJob(new CancelSubscriptionQueueable(contractIds));
      }
    }
  }

  global class ConvertQuoteQueueable implements Queueable {
    Set<Id> quoteIds = new Set<Id>();
    global ConvertQuoteQueueable(Set<Id> quoteIds){
        this.quoteIds = quoteIds ;  
    }
    global void execute(QueueableContext context) {
      Id recordId = (new List<Id>(quoteIds))[0];
      ConvertQuote(recordId);
      quoteIds.remove(recordId);

      if(!quoteIds.isEmpty()) {
        System.enqueueJob(new ConvertQuoteQueueable(quoteIds));
      }
    }
  }
}