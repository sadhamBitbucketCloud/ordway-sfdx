/**
 * @File Name          : OrdwayQuoteController.cls
 * @Description        :
 * @Author             : 
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 12-23-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/22/2020        Initial Version
 **/
public with sharing class OrdwayQuoteController {
  /**
   * @description Method To check wheather the user has the Ordway permission set assigned
   * @return Boolean true or false
   **/
  @AuraEnabled
  public static Boolean isPermissionSetAssigned() {
    return ApplicationSettingController.isOrdwayPermissionAssignedToUser();
  }

  @AuraEnabled
  public static Map<String, Boolean> getButtonPermission(){
    try {
      Map<String, Boolean> permissionMap = new Map<String, Boolean>();

      permissionMap.put('SyncQuote', !FeatureManagement.checkPermission('QuoteSynctoUpdatefromOrdway'));
      permissionMap.put('ActivateSendQuote', !FeatureManagement.checkPermission('QuoteActivateSend'));
      permissionMap.put('CancelQuote', !FeatureManagement.checkPermission('QuoteCancel'));
      permissionMap.put('ConvertQuote', !FeatureManagement.checkPermission('QuoteConvert'));
      permissionMap.put('TemplatePreviewQuote', !FeatureManagement.checkPermission('QuoteSelectTemplatePreview'));
      permissionMap.put('ViewQuoteInOrdway', !FeatureManagement.checkPermission('ViewinOrdway'));
      return permissionMap;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * @description Method To get Quote Template
   * @param quoteId Ordway Quote Id
   * @return List<QuoteTemplate__c> List of Quote Templates
   **/
  @AuraEnabled
  public static List<QuoteTemplate__c> getQuoteTemplates(String quoteId) {
    //We will query the Quote record to get the entity Id
    return OrdwayQuoteTemplateController.getOrdwayQuoteTemplate(
      OrdwayService.getEntityId(quoteId)
    );
  }

  /**
   * @description Method to check whether user has Edit permission to the Quote record
   * @param quoteId Quote Record Id
   * @return Boolean true or false
   **/
  @AuraEnabled
  public static Boolean hasEditPermission(Id quoteId) {
    if (Schema.sObjectType.Quote__c.isAccessible()) {
      return [
        SELECT Id, UserRecordAccess.HasEditAccess
        FROM Quote__c
        WHERE Id = :quoteId
      ]
      .UserRecordAccess.HasEditAccess;
    }
    return false;
  }

  /**
   * @description Method to get Named Credentials End point URL
   * @return String Named Credentials End point URL
   **/
  @AuraEnabled
  public static String getNamedCredential() {
    try {
      return OrdwayService.getNamedCredentialURL();
    } catch (Exception e) {
      throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  /**
   * @description Method to check whether Ordway Quote feature is enabled
   * @return True or false
   **/
  @AuraEnabled
  public static Boolean hasQuotesEnabled() {
    return OpportunitySettingController.isQuotesEnabled();
  }

  /**
   * @description Method to Update Quote Template Name from Select Template
   * @param quoteId Quote Record Id
   * @param templateId Quote Template Id
   * @return Boolean
   **/
  @AuraEnabled
  public static Boolean updateQuoteTemplate(String quoteId, String templateId) {
    if (Schema.sObjectType.OrdwayLabs__QuoteTemplate__c.isAccessible()) {
      Quote__c thisQuote = new Quote__c(
        Id = quoteId,
        QuoteTemplate__c = templateId
      );
      QuoteTriggerHelper_Callout.runOnce = false;

      if (Schema.sObjectType.Quote__c.isUpdateable()) {
        update thisQuote;
      }
      return true;
    }
    return false;
  }

  /**
   * @description Method to sync Ordway Quote to Ordway - Sync to Ordway Button
   * @param  quoteRecordId: Ordway Quote Id
   */
  @AuraEnabled
  public static String syncToOrdway(String quoteRecordId) {
    Quote__c thisOrdwayQuote;
    if (Schema.sObjectType.Quote__c.isAccessible()) {
      thisOrdwayQuote = [
        SELECT Id, OrdwayLabs__QuoteTemplate__c
        FROM OrdwayLabs__Quote__c
        WHERE Id = :quoteRecordId
      ];
    }

    try {
      HttpResponse thisResponse = OrdwayQuoteService.syncQuoteToOrdway(
        thisOrdwayQuote
      );
      if (thisResponse.getStatusCode() == 200) {
        return quoteRecordId;
      }
      String errorMessage = OrdwayHelper.getResponseStatusMessage(thisResponse);
      AuraHandledException ex = new AuraHandledException(errorMessage);
      ex.setMessage(errorMessage);
      throw ex;
    } catch (Exception thisException) {
      AuraHandledException ex = new AuraHandledException(
        thisException.getMessage()
      );
      ex.setMessage(thisException.getMessage());
      throw ex;
    }
  }

  /**
   * @description Sync to Ordway From Trigger
   * @param  ordwayQuoteId  Quote Record Id
   */
  @future(callout=true)
  public static void aSyncToOrdway(Id ordwayQuoteId) {
    //Publish Ordway Quote Event only if invoked from future context
    Quote__c thisOrdwayQuote = new Quote__c(Id = ordwayQuoteId);
    OrdwayQuoteService.syncQuoteToOrdway(thisOrdwayQuote);
    OrdwayContractPlatformEventHelper.publishOrdwayContractAsyncEvent(
      new List<Quote__c>{ thisOrdwayQuote }
    );
  }

  /**
   * @description Method to Get Ordway Quote - Update from Ordway - Field Comparison Modal
   * @param  ordwayQuoteId Quote Record Id
   * @return
   */
  @AuraEnabled
  public static Map<String, Map<String, Object>> getOrdwayQuote(
    Id quoteId,
    List<sObject> sObjectList,
    Map<String, String> lineItemToTierStringMap
  ) {
    try {
      HttpResponse thisResponse = OrdwayService.getOrdwayQuote(
        quoteId,
        getOrdwayQuoteId(quoteId).OrdwayQuoteId__c,
        null
      );

      if (thisResponse.getStatusCode() == 200) {
        OrdwayContractController.populateLineItemToTierMap(lineItemToTierStringMap, thisResponse.getBody());
        Set<String> dmlKeyStringSet = new Set<String>{
          OrdwayHelper.SOBJECT_LIST_TO_UPDATE
        };
        Map<String, Map<String, Object>> fieldValueAsMap = new Map<String, Map<String, Object>>();
        Map<String, list<sObject>> sObjectMapToReturn = OrdwayQuoteService.processQuoteResponse(
          thisResponse.getBody()
        );

        for (String thisDMLKey : dmlKeyStringSet) {
          if (sObjectMapToReturn.containsKey(thisDMLKey)) {
            for (SObject thisObject : sObjectMapToReturn.get(thisDMLKey)) {
              if (thisObject.Id != null) {
                fieldValueAsMap.put(thisObject.Id, new Map<String, Object>{});
                fieldValueAsMap.get(thisObject.Id)
                  .putAll(thisObject.getPopulatedFieldsAsMap());
              }
            }
          }
        }
        return fieldValueAsMap;
      }
      String errorMessage = OrdwayHelper.getResponseStatusMessage(thisResponse);
      AuraHandledException ex = new AuraHandledException(errorMessage);
      ex.setMessage(errorMessage);
      throw ex;
    } catch (Exception thisException) {
      AuraHandledException ex = new AuraHandledException(
        thisException.getMessage()
      );
      ex.setMessage(thisException.getMessage());
      throw ex;
    }
  }

  /**
   * @description Invoked from component Update from Ordway -> Confirm button
   * When automation is turned OFF
   * @param  quoteId Quote Record Id
   */
  @AuraEnabled
  public static String updateOrdwayQuote(String quoteId) {
    try {
      Quote__c thisQuote = getOrdwayQuoteId(quoteId);
      HttpResponse thisResponse = OrdwayService.getOrdwayQuote(
        quoteId,
        thisQuote.OrdwayQuoteId__c,
        null
      );
      QuoteTriggerHelper_Callout.runOnce = false;
      if (thisResponse.getStatusCode() == 200) {
        List<Object> responseList = new List<Object>();
        responseList.add((Object)JSON.deserializeUntyped(thisResponse.getBody()));
        OrdwayObjectSyncHelper.processResponse('Quote', new Map<String, Object>{ 'result' => JSON.deserializeUntyped(JSON.serialize(responseList))}, OrdwayHelper.ACTION.RESPONSE, thisQuote.EntityId__c);
        return quoteId;
      }

      String errorMessage = OrdwayHelper.getResponseStatusMessage(thisResponse);
      AuraHandledException ex = new AuraHandledException(errorMessage);
      ex.setMessage(errorMessage);
      throw ex;
    } catch (Exception thisException) {
      AuraHandledException ex = new AuraHandledException(
        thisException.getMessage()
      );
      ex.setMessage(thisException.getMessage());
      throw ex;
    }
  }

  /**
   * @description Method to Cancel Quote
   * When automation is turned OFF
   * @param  quoteId Quote Record Id
   */
  @AuraEnabled
  public static String cancelQuote(String quoteId) {
    try {
      HttpResponse thisResponse = OrdwayService.cancelQuote(
        quoteId,
        getOrdwayQuoteId(quoteId).OrdwayQuoteId__c
      );

      if (thisResponse.getStatusCode() == 200) {
        OrdwayQuoteService.processQuotePayload(thisResponse.getBody());
        return quoteId;
      }

      String errorMessage = OrdwayHelper.getResponseStatusMessage(thisResponse);
      AuraHandledException ex = new AuraHandledException(errorMessage);
      ex.setMessage(errorMessage);
      throw ex;
    } catch (Exception thisException) {
      AuraHandledException ex = new AuraHandledException(
        thisException.getMessage()
      );
      ex.setMessage(thisException.getMessage());
      throw ex;
    }
  }

  /**
   * @description Method to Convert Quote
   * When automation is turned OFF
   * @param  quoteId Quote Record Id
   */
  @AuraEnabled
  public static String convertQuote(String quoteId) {
    try {
      HttpResponse thisResponse = OrdwayService.convertQuote(
        quoteId,
        getOrdwayQuoteId(quoteId).OrdwayQuoteId__c
      );

      if (thisResponse.getStatusCode() == 200) {
        OrdwayQuoteService.processQuotePayload(thisResponse.getBody());
        return quoteId;
      }

      String errorMessage = OrdwayHelper.getResponseStatusMessage(thisResponse);
      AuraHandledException ex = new AuraHandledException(errorMessage);
      ex.setMessage(errorMessage);
      throw ex;
    } catch (Exception thisException) {
      AuraHandledException ex = new AuraHandledException(
        thisException.getMessage()
      );
      ex.setMessage(thisException.getMessage());
      throw ex;
    }
  }

  /**
   * @description Method to Activate Quote
   * When automation is turned OFF
   * @param  quoteId Quote Record Id
   */
  @AuraEnabled
  public static String activateQuote(String quoteId) {
    try {
      HttpResponse thisResponse = OrdwayService.activateQuote(
        quoteId,
        getOrdwayQuoteId(quoteId).OrdwayQuoteId__c
      );

      if (thisResponse.getStatusCode() == 200) {
        OrdwayQuoteService.processQuotePayload(thisResponse.getBody());
        return quoteId;
      }

      String errorMessage = OrdwayHelper.getResponseStatusMessage(thisResponse);
      AuraHandledException ex = new AuraHandledException(errorMessage);
      ex.setMessage(errorMessage);
      throw ex;
    } catch (Exception thisException) {
      AuraHandledException ex = new AuraHandledException(
        thisException.getMessage()
      );
      ex.setMessage(thisException.getMessage());
      throw ex;
    }
  }

  /**
   * @description Method to Get Ordway Quote Id
   * @param  quoteId Quote Record Id
   */
  private static Quote__c getOrdwayQuoteId(String quoteId) {
    if (Schema.sObjectType.Quote__c.isAccessible()) {
      return [
        SELECT Id, OrdwayQuoteId__c, EntityId__c
        FROM Quote__c
        WHERE Id = :quoteId OR OrdwayQuoteId__c = :quoteId
        LIMIT 1
      ];
    }
    return null;
  }

  /**
   * @description Method to check whether the new plan picker component to show
   * @return True or false
   **/
  @AuraEnabled
  public static Boolean hasPlanPickerEnabled() {
    return LoadLeanSettingController.isPlanPicker2Enabled();
  }

}