/**
 * @File Name          : SyncOrdwayContract_Batch.cls
 * @Description        : Sync Ordway Contracts to Ordway
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    12/10/2019   Ordway Labs     Initial Version
 **/

global with sharing class SyncOrdwayContract_Batch implements Database.Batchable<sObject>, Database.AllowsCallouts {
  String query;
  Set<Id> contractIdSet;
  String OrdwaySyncStatus = OrdwayHelper.SYNC_STATE_NOT_SYNCED;

  global SyncOrdwayContract_Batch() {
    query = 'SELECT OrdwayLabs__Account__c, Id, OrdwayLabs__VersionType__c, OrdwayLabs__OrdwayContractStatus__c, OrdwayLabs__OrdwaySubscriptionID__c FROM OrdwayLabs__OrdwayContract__c WHERE OrdwayLabs__OrdwayLastSyncDate__c = null OR OrdwayLabs__SyncStatus__c =: OrdwaySyncStatus';
  }

  global SyncOrdwayContract_Batch(Set<Id> contractIds) {
    this.contractIdSet = contractIds;
    query = 'SELECT OrdwayLabs__Account__c, Id, OrdwayLabs__VersionType__c, OrdwayLabs__OrdwayContractStatus__c, OrdwayLabs__OrdwaySubscriptionID__c FROM OrdwayLabs__OrdwayContract__c WHERE ID IN: contractIdSet';
  }

  global SyncOrdwayContract_Batch(String query) {
    this.query = query;
  }

  global Database.QueryLocator start(Database.BatchableContext BC) {
    if (Schema.sObjectType.OrdwayContract__c.isAccessible()) {
      return Database.getQueryLocator(query);
    } else {
      return null;
    }
  }

  global void execute(
    Database.BatchableContext BC,
    List<OrdwayContract__c> ordwayContractList
  ) {
    OrdwayHelper.getResponseStatusMessage(
      OrdwayContractService.syncContractToOrdway(ordwayContractList[0])
    );
  }

  global void finish(Database.BatchableContext BC) {
  }
}