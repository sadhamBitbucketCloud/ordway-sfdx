public inherited sharing class OrdwayProductService {

  public static void populateOrdwayEntity(OrdwayProduct__c oPro){

    if(!OpportunitySettingController.isMultiEntityEnabled()) { return; } //If Multi Entity Not Enabled Do Nothing
    
    //On Bulk Import or Webhook, EntityID will be present, populate lookup field automatically
    if(oPro.OrdwayEntity__c == null 
      && oPro.EntityId__c != null
      && OrdwayEntityHelper.entityMapByEntityId.containsKey(oPro.EntityID__c)){
        oPro.OrdwayEntity__c = OrdwayEntityHelper.entityMapByEntityId.get(oPro.EntityID__c);
    }
    
    //If Ordway Entity is blank, populate default entity if exist 
    if( oPro.OrdwayEntity__c == null 
    && OrdwayEntityHelper.defaultEntity != null) {
      oPro.OrdwayEntity__c = OrdwayEntityHelper.defaultEntity.Id;
    }
  }

  public static void populateEntity(OrdwayProduct__c oPro) {

    if(oPro.OrdwayEntity__c == null && oPro.EntityId__c != null && OrdwayEntityHelper.entityMapByEntityId.get(oPro.EntityId__c) != null) { 
      oPro.OrdwayEntity__c = OrdwayEntityHelper.entityMapByEntityId.get(oPro.EntityId__c);
    }

    if(oPro.OrdwayEntity__c == null && oPro.EntityId__c == null) { clearEntity(oPro); return; }

    oPro.EntityName__c = OrdwayEntityHelper.entityMapById.get(oPro.OrdwayEntity__c).Name;
    oPro.EntityID__c = OrdwayEntityHelper.entityMapById.get(oPro.OrdwayEntity__c).EntityID__c;
  }

  public static void populateOrdwayProductId(OrdwayProduct__c oPro) {

    if(oPro.ExternalID__c == null) { return; }
    oPro.OrdwayProductID__c = oPro.ExternalID__c.contains(':') ? oPro.ExternalID__c.substringAfterLast(':') : oPro.ExternalID__c;
    
    if(oPro.EntityId__c != null) { oPro.ExternalID__c = oPro.EntityId__c+':'+oPro.OrdwayProductID__c; }
  }

  public static Product2 createProduct2(OrdwayProduct__c oPro){

    if(oPro.Product2Id__c == null
      && oPro.OrdwayProductID__c != null) {
        Product2 thisProduct2 = (Product2) SObjectFieldMappingHelper.dynamicSobjectMapping(oPro, new Product2(), true);
        thisProduct2.OrdwayProduct__c = true;
        thisProduct2.IsActive = true;
        return thisProduct2;
      }
    return null;
  }

  public static void createProduct2(List<OrdwayProduct__c> oProList, Map<String, Product2> product2Map){

    ProductTriggerHandler.createOrdwayProduct = false; //Do not trigger Product Trigger
    insert product2Map.values();

    for(OrdwayProduct__c oPro : oProList){
      if(product2Map.get(oPro.OrdwayProductID__c) != null){
        //Map Product2Id from newly created record
        oPro.Product2Id__c = product2Map.get(oPro.OrdwayProductID__c).Id;
      }
    }
  }

  public static void clearEntity(OrdwayProduct__c oPro){
    oPro.EntityName__c = null;
    oPro.EntityID__c = null;
  }

  public static void updateProduct2(Set<Id> product2IdSet){

    List<Product2> product2ToUpdate = new List<Product2>();
    for(Product2 pro2 : [ SELECT Id, OrdwayProduct__c FROM Product2 WHERE Id IN: product2IdSet AND OrdwayProduct__c = false ]){
      pro2.OrdwayProduct__c = true;
      product2ToUpdate.add(pro2);
    }
    ProductTriggerHandler.createOrdwayProduct = false;
    update product2ToUpdate;

  }

  public static List<OrdwayProduct__c> getOrdwayProducts(Set<Id> productIds, String filterField){
		return Database.query('SELECT Id, Product2Id__c FROM OrdwayProduct__c WHERE '+ filterField+ ' =: productIds');
  }
}