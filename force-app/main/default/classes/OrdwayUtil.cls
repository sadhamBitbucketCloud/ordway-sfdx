/**
 * @File Name          : OrdwayUtil.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : Ordway Labs
 * @Last Modified On   : 01-15-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    4/20/2020        Initial Version
 **/

public with sharing class OrdwayUtil {
  public static Map<String, String> componentTypeMap = new Map<String, String>{
    'PICKLIST' => 'lightning:combobox',
    'BOOLEAN' => 'lightning:input',
    'STRING' => 'lightning:input',
    'TEXTAREA' => 'lightning:textarea',
    'DATETIME' => 'lightning:input',
    'DATE' => 'lightning:input',
    'CURRENCY' => 'lightning:input',
    'DOUBLE' => 'lightning:input',
    'PERCENT' => 'lightning:input',
    'EMAIL' => 'lightning:input',
    'INTEGER' => 'lightning:input',
    'LONG' => 'lightning:input',
    'PHONE' => 'lightning:input',
    'TIME' => 'lightning:input',
    'URL' => 'lightning:input'
  };

  public static Map<String, String> inputTypeMap = new Map<String, String>{
    'BOOLEAN' => 'checkbox',
    'STRING' => 'text',
    'DATETIME' => 'datetime',
    'DATE' => 'date',
    'CURRENCY'  => 'number',
    'DOUBLE' => 'number',
    'PERCENT' => 'number',
    'EMAIL' => 'email',
    'INTEGER' => 'number',
    'LONG' => 'number',
    'PHONE' => 'tel',
    'TIME' => 'time',
    'URL' => 'url'
  };

  public static Map<String, String> inputTypeFormatterMap = new Map<String, String>{
    //'CURRENCY'  => 'currency',
    'PERCENT' => 'percent-fixed'
  };

  public static Map<String, String> inputStyleMap = new Map<String, String>{
    'CURRENCY' => 'currency'
  };

  public static Map<String, String> inputTypeNumberStepsMap = new Map<String, String>{
    'CURRENCY'  => '0.01',
    'DOUBLE' => '0.01',
    'PERCENT' => '0.01',
    'INTEGER' => '1',
    'LONG' => '1'
  };
}