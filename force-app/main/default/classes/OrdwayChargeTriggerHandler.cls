public with sharing class OrdwayChargeTriggerHandler extends TriggerHandler.TriggerHandlerBase {
  
  public override void beforeInsert(List<SObject> lstNewSObjs) {

    Map<String, OrdwayProduct__c> ordwayProductMap = new Map<String, OrdwayProduct__c>();
    Set<Id> product2Ids = new Set<Id>();
    Set<Id> ordwayPlanIds = new Set<Id>();
    Map<Id, String> planEntityMap = new Map<Id, String>();

    for(Charge__c thisCharge : (List<Charge__c>) lstNewSObjs){

      //If Product2 ID is Present, then populate the Product ID at Charge Level
      if(thisCharge.ProductID__c == null
      && thisCharge.Product__c != null){
        product2Ids.add(thisCharge.Product__c);
        ordwayPlanIds.add(thisCharge.OrdwayPlan__c);
      }  
      
      //Populate Ordway Product Id from External Id
      if(thisCharge.ExternalID__c != null && thisCharge.ChargeID__c == null && thisCharge.ExternalID__c.contains(':')){
        thisCharge.ChargeID__c = thisCharge.ExternalID__c.substringAfterLast(':');
      }
      else if( thisCharge.ExternalID__c != null && thisCharge.ChargeID__c == null && !thisCharge.ExternalID__c.contains(':') ) {
        thisCharge.ChargeID__c = thisCharge.ExternalID__c;
      }

    }

    if( OpportunitySettingController.isMultiEntityEnabled()) { 

      for(Plan__c thisPlan : [SELECT Id, EntityID__c 
                                  FROM Plan__c 
                                  WHERE Id IN: ordwayPlanIds]){
        planEntityMap.put(thisPlan.Id, thisPlan.EntityID__c);
      }

      for(OrdwayProduct__c thisOrdwayProduct : [SELECT Id, OrdwayProductID__c, 
                                                        Product2Id__c, EntityID__c 
                                                        FROM OrdwayProduct__c 
                                                        WHERE Product2Id__c IN: product2Ids
                                                        AND EntityID__c IN: planEntityMap.values()]){
        ordwayProductMap.put(thisOrdwayProduct.Product2Id__c+':'+thisOrdwayProduct.EntityID__c, thisOrdwayProduct);
      }

      for(Charge__c thisCharge : (List<Charge__c>) lstNewSObjs){
        
        if(thisCharge.ProductID__c == null
        && thisCharge.Product__c != null
        && ordwayProductMap.get(String.valueOf(thisCharge.Product__c+':'+planEntityMap.get(thisCharge.OrdwayPlan__c))) != null){
          thisCharge.ProductID__c = ordwayProductMap.get(String.valueOf(thisCharge.Product__c+':'+planEntityMap.get(thisCharge.OrdwayPlan__c))).OrdwayProductID__c;
        }
      }

    }
    else{
      for(OrdwayProduct__c thisOrdwayProduct : [SELECT Id, OrdwayProductID__c, Product2Id__c 
                                                        FROM OrdwayProduct__c 
                                                        WHERE Product2Id__c IN: product2Ids]){
        ordwayProductMap.put(thisOrdwayProduct.Product2Id__c, thisOrdwayProduct);
      }

      for(Charge__c thisCharge : (List<Charge__c>) lstNewSObjs){
        if(thisCharge.ProductID__c == null
          && thisCharge.Product__c != null
          && ordwayProductMap.get(thisCharge.Product__c) != null){
            thisCharge.ProductID__c = ordwayProductMap.get(thisCharge.Product__c).OrdwayProductID__c;
        }
      }
    }
  }

  public override void beforeUpdate(Map<Id, SObject> mapOldSObjs, Map<Id, SObject> mapNewSObjs) {

    for(Charge__c thisCharge : (List<Charge__c>) mapNewSObjs.values()){

      if(thisCharge.ExternalID__c != null && thisCharge.ChargeID__c == null && thisCharge.ExternalID__c.contains(':')) {
        thisCharge.ChargeID__c = thisCharge.ExternalID__c.substringAfterLast(':');
      }
      else if( thisCharge.ExternalID__c != null && thisCharge.ChargeID__c == null && !thisCharge.ExternalID__c.contains(':') ) {
        thisCharge.ChargeID__c = thisCharge.ExternalID__c;
      }
    }

  }
}