public with sharing class MigrationAssistant {

  @AuraEnabled
	public static MigrationAssistantWrapper getMigrationAssistantWrapper(){
    try {
      MigrationAssistantWrapper assistantWrapper      = new MigrationAssistantWrapper();
      assistantWrapper.isQuoteAssistantEnabled        = quoteAssistant();
      assistantWrapper.isPlanPickerVersionTwoEnabled  = isPlanPickerEnabled();
      return assistantWrapper;
    }
    catch (Exception e) {
      AuraHandledException ex = new AuraHandledException(e.getMessage());
      ex.setMessage(e.getMessage());
      throw ex;
    }
	}

  @AuraEnabled
  public static Boolean quoteAssistant() {
    //If Quotes not enabled, then return true
    if(!OpportunitySettingController.isQuotesEnabled()) {
      return true;
    }

    try {
      SObjectType lineItemTierSobjectType = ((SObject) Type.forName('OrdwayLabs__QuoteLineItemTier__c').newInstance()).getSObjectType();
      return true;
    }
    catch (Exception e){
      return false;
    }
  }

  @AuraEnabled
  public static Boolean isPlanPickerEnabled() {
    return LoadLeanSettingController.isPlanPicker2Enabled();
  }

  @AuraEnabled
  public static Boolean enablePlanPicker() {
    try {
      
      if( LoadLeanSettings__c.getOrgDefaults().Id == null) {
        LoadLeanSettings__c ll = new LoadLeanSettings__c();
        ll.PlanPicker__c = true;
        ll.SetupOwnerId = UserInfo.getOrganizationId();
        insert ll;
        return true;
      }
      else {
        LoadLeanSettings__c ll = LoadLeanSettings__c.getOrgDefaults();
        ll.PlanPicker__c = true;
        update ll;
        return true;
      }
      
    }
    catch (Exception e) {
      AuraHandledException ex = new AuraHandledException(e.getMessage());
      ex.setMessage(e.getMessage());
      throw ex;
    }
  }

  /**
   * @description Method to Activate Ordway Quote Line Item Tier
   **/
  @AuraEnabled
  public static Boolean activateQuoteLineItemTier() {
    try {
      if(FeatureAccessController.hasQuoteLicense()) {
        MigrationAssistant.updateQuoteMetadataMapping();
        FeatureAccessController.changeQuoteProtection();
        return true;
      }
      else {
        throw new FeatureAccessController.FeatureAccessException(
          'Ordway Quote feature not currently licensed. Please contact Ordway support for more details'
        );
      }
    }
    catch (Exception ex) {
      throw new FeatureAccessController.FeatureAccessException(ex.getMessage());
    }
  }

  public static void updateQuoteMetadataMapping() {
    
    Metadata.CustomMetadata dynamicMetadata = new Metadata.CustomMetadata();

    //Custom Metadata API Name + Namespace Prefix + Developer Name
    //No need to provide developer name, as the same will be derived from fullname
    dynamicMetadata.fullName = 'OrdwayLabs__DynamicObjectMapping.OrdwayLabs__Quote';
    dynamicMetadata.Label = 'Quotes';

    Metadata.CustomMetadataValue targetField = new Metadata.CustomMetadataValue();

    targetField = new Metadata.CustomMetadataValue();
    targetField.field = 'OrdwayLabs__ChildExternalID__c'; //Metadata Field
    targetField.value = 'OrdwayLabs__ExternalID__c'; //Field Value
    dynamicMetadata.values.add(targetField);

    Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
    mdContainer.addMetadata(dynamicMetadata);
    DeployOrdwayObjectFieldMappingMetaData callback = new DeployOrdwayObjectFieldMappingMetaData();
    Metadata.Operations.enqueueDeployment(mdContainer, callback);
  }

  public class MigrationAssistantWrapper {
    @AuraEnabled
		public Boolean isQuoteAssistantEnabled;
    @AuraEnabled
		public Boolean isPlanPickerVersionTwoEnabled;
  }
}