/**
 * @File Name          : OpportunityOrdwayContractSyncController.cls
 * @Description        : Opportunity Ordway Contract Sync Controller
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    5/10/2019   Ordway Labs     Initial Version
 **/
public with sharing class OpportunityOrdwayContractSyncController {
  private static List<OpportunityOrdwayContractSyncSetting__mdt> syncSetting {
    get {
      if (syncSetting == null) {
        syncSetting = new List<OpportunityOrdwayContractSyncSetting__mdt>();
        syncSetting = [
          SELECT
            OpportunityOrdwayTypeValue__c,
            OrdwayContractStatusValue__c,
            Sync__c
          FROM OpportunityOrdwayContractSyncSetting__mdt
          LIMIT 100
        ];
      }
      return syncSetting;
    }
    set;
  }

  public static Map<String, List<String>> ordwayTypeContractStatusMap {
    get {
      if (ordwayTypeContractStatusMap == null) {
        ordwayTypeContractStatusMap = new Map<String, List<String>>();
        for (
          OpportunityOrdwayContractSyncSetting__mdt thisSetting : syncSetting
        ) {
          if (thisSetting.Sync__c) {
            if (
              !ordwayTypeContractStatusMap.containsKey(
                thisSetting.OpportunityOrdwayTypeValue__c
              )
            ) {
              ordwayTypeContractStatusMap.put(
                thisSetting.OpportunityOrdwayTypeValue__c,
                new List<String>{}
              );
            }
            ordwayTypeContractStatusMap.get(
                thisSetting.OpportunityOrdwayTypeValue__c
              )
              .add(thisSetting.OrdwayContractStatusValue__c);
          }
        }
      }
      return ordwayTypeContractStatusMap;
    }
    set;
  }

  /**
   * @description Method to check whether the opportunity is Synchronizable (Use if Ordway Contract Id Is Present On Opportunity)
   * @param opportunityStage Opportunity Stage value
   * @param opportunityOrdwayType Opportunity ordway Type value
   * @param opportunityContractStatus Opportunity ordway contract status value
   * @return boolean true or false
   **/
  public static boolean isSynchronizable(
    String opportunityStage,
    String opportunityOrdwayType,
    String opportunityContractStatus
  ) {
    Boolean isSyncable = false;
    if (
      ordwayTypeContractStatusMap.containsKey(opportunityOrdwayType) &&
      ordwayTypeContractStatusMap.get(opportunityOrdwayType)
        .contains(opportunityContractStatus) &&
      isSynchronizable(opportunityStage)
    ) {
      isSyncable = true;
    }
    return isSyncable;
  }

  /**
   * @description Method to check whether the opportunity is Synchronizable (Use if Ordway Contract Id is not present on the Opportunity)
   * @param opportunityStage Opportunity Stage value
   * @return boolean true or false
   **/
  public static boolean isSynchronizable(String opportunityStage) {
    //Check Whether Automation is Turned On and Compare Opportunity Stage
    if (
      OpportunitySettingController.getOpportunitySyncStage() ==
      opportunityStage && OpportunitySettingController.isAutomaticSyncEnabled()
    ) {
      return true;
    }
    return false;
  }

  /**
   * @description Method to get Get OrdwayContractSyncWrapper
   * @return List<OrdwayContractSyncWrapper> List of OrdwayContractSyncWrapper
   **/
  @AuraEnabled
  public static List<OrdwayContractSyncWrapper> getOpportunityOrdwayContractSync() {
    List<OrdwayContractSyncWrapper> newOrdwaySyncList = new List<OrdwayContractSyncWrapper>();
    Map<String, Integer> ordwayTypeMap = new Map<String, Integer>();
    Integer index = 0;

    for (
      OpportunityOrdwayContractSyncSetting__mdt thisContractSync : syncSetting
    ) {
      if (
        !ordwayTypeMap.containsKey(
          thisContractSync.OpportunityOrdwayTypeValue__c
        )
      ) {
        newOrdwaySyncList.add(new OrdwayContractSyncWrapper());
        ordwayTypeMap.put(
          thisContractSync.OpportunityOrdwayTypeValue__c,
          index
        );
        newOrdwaySyncList[
            ordwayTypeMap.get(thisContractSync.OpportunityOrdwayTypeValue__c)
          ]
          .opportunityOrdwayType = thisContractSync.OpportunityOrdwayTypeValue__c;
        index++;
      }

      if (thisContractSync.Sync__c) {
        if (
          thisContractSync.OrdwayContractStatusValue__c ==
          OrdwayHelper.NOT_LINKED_TO_ORDWAY
        ) {
          newOrdwaySyncList[
              ordwayTypeMap.get(thisContractSync.OpportunityOrdwayTypeValue__c)
            ]
            .notLinkedToOrdway = OrdwayHelper.ISTRUE;
        }
        if (
          thisContractSync.OrdwayContractStatusValue__c == OrdwayHelper.DRAFT
        ) {
          newOrdwaySyncList[
              ordwayTypeMap.get(thisContractSync.OpportunityOrdwayTypeValue__c)
            ]
            .draft = OrdwayHelper.ISTRUE;
        }
        if (
          thisContractSync.OrdwayContractStatusValue__c == OrdwayHelper.ACTIVE
        ) {
          newOrdwaySyncList[
              ordwayTypeMap.get(thisContractSync.OpportunityOrdwayTypeValue__c)
            ]
            .active = OrdwayHelper.ISTRUE;
        }
        if (
          thisContractSync.OrdwayContractStatusValue__c ==
          OrdwayHelper.CANCELLED
        )
          newOrdwaySyncList[
              ordwayTypeMap.get(thisContractSync.OpportunityOrdwayTypeValue__c)
            ]
            .cancelled = OrdwayHelper.ISTRUE;
      }
    }
    return newOrdwaySyncList;
  }

  /**
   * @description Ordway Contract Sync Wrapper Class
   **/
  public class OrdwayContractSyncWrapper {
    @AuraEnabled
    public String opportunityOrdwayType;
    @AuraEnabled
    public String notLinkedToOrdway;
    @AuraEnabled
    public String draft;
    @AuraEnabled
    public String active;
    @AuraEnabled
    public String cancelled;

    public OrdwayContractSyncWrapper() {
      notLinkedToOrdway = OrdwayHelper.ISFALSE;
      draft = OrdwayHelper.ISFALSE;
      active = OrdwayHelper.ISFALSE;
      cancelled = OrdwayHelper.ISFALSE;
    }
  }
}