/**
 * @description       :
 * @author            :
 * @group             :
 * @last modified on  : 09-19-2020
 * @last modified by  : 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   08-16-2020      Initial Version
 **/
@isTest
public class InjectAPI_Queueable_Test {
  @isTest
  private static void injectAPIQueueableTest() {
    LoadLeanSettings__c llSetting = TestObjectFactory.createLoadLeanSetting();
    llSetting.CreateApplicationLogSynchronously__c = true;
    insert llSetting;

    Account ordwayAccount = TestObjectFactory.createAccount();
    insert ordwayAccount;

    Contact ordwayContact = TestObjectFactory.createContact();
    insert ordwayContact;

    OpportunitySettingController.saveOpportunitySetting(
      TestObjectFactory.createContractPlanPickerSetting()
    );

    OrdwayContract__c ordwayContract = TestObjectFactory.createOrdwayContract();
    ordwayContract.BillingContact__c = ordwayContact.Id;
    ordwayContract.Account__c = ordwayAccount.Id;
    ordwayContract.OrdwaySubscriptionID__c = 'Sub-123';
    ordwayContract.OrdwayLabs__ExternalID__c = 'Sub-123';

    insert ordwayContract;

    System.assertEquals(
      'Not Synced',
      ordwayContract.SyncStatus__c,
      'Should return Not Synced'
    );

    ContractLineItem__c ordwayContractLineItem = new ContractLineItem__c();
    ordwayContractLineItem.OrdwayLabs__OrdwayContract__c = ordwayContract.Id;
    ordwayContractLineItem.OrdwayLabs__SubscriptionID_LineID__c = 'Sub-123:SCHARID-123';
    ordwayContractLineItem = TestObjectFactory.createContractLineItem(
      ordwayContractLineItem
    );
    insert ordwayContractLineItem;

    System.assertEquals(ordwayAccount.Id, ordwayContract.Account__c);

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;
    ID jobID;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;

    Test.startTest();

    Integer RESPONSE_CODE = 200;

    String RESPONSE_BODY = '{ ' + '"callout" : "success"' + '}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSE_CODE, RESPONSE_BODY)
    );
    InjectAPI_Queueable injectAPIQueueable = new InjectAPI_Queueable(
      'OrdwayContract',
      new Set<String>{ ordwayContract.Id },
      null
    );
    jobID = system.enqueueJob(injectAPIQueueable);
    Test.stopTest();

    AsyncApexJob jobInfo = [
      SELECT Status, NumberOfErrors
      FROM AsyncApexJob
      WHERE Id = :jobID
    ];
    System.assertequals(
      'Completed',
      jobInfo.Status,
      'The Queue should be completed'
    );
  }
}