/**
 * @File Name          : GetOrdwayObjectComponentController.cls
 * @Description        : Get Ordway Object Component Controller
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 01-03-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/14/2020        Initial Version
 **/
public with sharing class GetOrdwayObjectComponentController {
  /**
   * @description Method to execute the GetOrdwayObject_Batch for specified Object and filters from the Component
   * @param pageNumber Page number for the request header
   * @param objectName Object Name to be retrieved
   * @param fromDate   From Date Filter for the Object
   * @param toDate     To Date Filter for the Object
   * @param filter     filter by type
   * @param filterValue filter value
   * @param xEntityId Entity Id
   **/
  @AuraEnabled
  public static void callGetOrdwayObject_Batch(
    Integer pageNumber,
    String objectName,
    Date fromDate,
    Date toDate,
    String filter,
    String filterValue,
    String xEntityId,
    Boolean includeSynced
  ) {
   
    if (objectName == 'QuoteTemplate') {
      OrdwayQuoteTemplateController.getOrdwayTemplate(xEntityId);
    } else {
      //Construct Query String
      String queryString = '';

      if (filter == 'customer_id' || filter == 'id' || filter == 'contact_id' || filter == 'subscription_id' || filter == 'charge_id') {
        filterValue = filterValue.trim();
        queryString = '&' + filter + '=' + EncodingUtil.urlEncode(filterValue, 'utf-8').replace('+', '%20');
      } else if (filter == 'status' || filter == 'refund_status') {
        queryString =
          '&' +
          filter +
          '=' +
          EncodingUtil.urlEncode(filterValue.toLowerCase(), 'utf-8')
            .replace('+', '%20'); //Salesforce encodes space as +, whereas it should be %20
      } else {
        String dateFormatString = 'yyyy-MM-dd';
        Datetime fDate;
        Datetime tDate;

        if (fromDate != null) {
          fDate = Datetime.newInstance(
            fromDate.year(),
            fromDate.month(),
            fromDate.day()
          );
          queryString =
            queryString +
            '&' +
            filter +
            EncodingUtil.urlEncode('>', 'utf-8') +
            '=' +
            fDate.format(dateFormatString);

          if (toDate == null) {
            toDate = System.today();
          }
        }

        if (toDate != null) {
          tDate = Datetime.newInstance(
            toDate.year(),
            toDate.month(),
            toDate.day()
          );
          queryString =
            queryString +
            '&' +
            filter +
            EncodingUtil.urlEncode('<', 'utf-8') +
            '=' +
            tDate.format(dateFormatString);
        }

        //If from date & to date are equal, then just pass created_date=dates
        if (fromDate != null && toDate != null && fromDate == toDate) {
          queryString = '';
          queryString = '&' + filter + '=' + fDate.format(dateFormatString);
        }
      }
      if (objectName == 'OrdwayContract'){
        if(includeSynced) {
          queryString = queryString +'&' + 'include_synced=true';
        }
        else {
          queryString = queryString +'&' + 'include_synced=false';
        }
      }
      GetOrdwayObject_Batch getOrdwayObjectBatch = new GetOrdwayObject_Batch(
        pageNumber,
        objectName,
        queryString,
        xEntityId
      );
      Database.executebatch(getOrdwayObjectBatch);
    }
  }

  /**
   * @description Method to retrieve Dynamic Object Metadata records
   * @return List<DynamicObjectMapping__mdt> List of Dynamic Object Metadata records
   **/
  @AuraEnabled(cacheable=true)
  public static List<DynamicObjectMapping__mdt> getDynamicObjectMetadata() {
    //Using Order Number to Control the Drop-down values in Bulk Sync Custom Page
    return [
      SELECT MasterLabel, DeveloperName, OrderNumber__c
      FROM DynamicObjectMapping__mdt
      WHERE OrderNumber__c != NULL
      ORDER BY OrderNumber__c ASC
      LIMIT 100
    ];
  }

  /**
   * @description Method to get Initial wrapper data for Get Ordway Object Component
   * @return InitialWrapper Initial wrapper data for Get Ordway Object Component
   **/
  @AuraEnabled(cacheable=true)
  public static InitialWrapper getInitialWrapper() {
    try {
      InitialWrapper thisInitialWrapper = new InitialWrapper();
      thisInitialWrapper.dynamicObjectMetadataList = getDynamicObjectMetadata();
      thisInitialWrapper.isQuoteEnabled = OpportunitySettingController.isQuotesEnabled();
      ApplicationSettingController.InitialWrapper applicationSettingWrapper = new ApplicationSettingController.InitialWrapper();
      applicationSettingWrapper = ApplicationSettingController.getApplicationSetting();
      thisInitialWrapper.applicationSetting = applicationSettingWrapper.applicationSetting;
      thisInitialWrapper.isMultiEntityPermissionAssigned = applicationSettingWrapper.isMultiEntityPermissionAssigned;
      thisInitialWrapper.isMultiEntityEnabled = OpportunitySettingController.isMultiEntityEnabled();
      if (thisInitialWrapper.isMultiEntityEnabled) {
        thisInitialWrapper.ordwayEntities = new List<OrdwayEntityController.PickListWrapper>();
        OrdwayEntityController.populateOrdwayEntitiesMap(
          thisInitialWrapper.ordwayEntities
        );
      }
      return thisInitialWrapper;
    } catch (Exception e) {
      throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  /**
   * @description Method to get Picklist wrapper for specified picklist field for the specified object
   * @param objectApiName Object API Name
   * @param picklistFieldApiName Picklist Field API Name
   * @return PicklistWrapper Picklist wrapper
   **/
  @AuraEnabled(cacheable=true)
  public static PicklistWrapper getPicklistWrapper(
    String objectApiName,
    String picklistFieldApiName
  ) {
    PicklistWrapper thisPicklistWrapper = new PicklistWrapper();
    thisPicklistWrapper.PicklistWrapperList = fetchPickListValue(
      objectApiName,
      picklistFieldApiName
    );
    return thisPicklistWrapper;
  }

  /**
   * @description Method to construct the picklist wrapper with Picklist values
   * @param objectApiName Object API Name
   * @param picklistFieldApiName Picklist Field API Name
   * @return List<PicklistEntryWrapper> List of Picklist Entry wrapper
   **/
  @AuraEnabled(cacheable=true)
  public static List<PicklistEntryWrapper> fetchPickListValue(
    String objectApiName,
    String picklistFieldApiName
  ) {
    try {
      Schema.SObjectType globalDescribe = Schema.getGlobalDescribe()
        .get(objectApiName);
      Schema.DescribeSObjectResult sobjectDescribe = globalDescribe.getDescribe();
      List<PicklistEntryWrapper> thisPicklistEntryWrapperList = new List<PicklistEntryWrapper>();
      for (
        Schema.PicklistEntry thisPicklistEntry : sobjectDescribe.fields.getMap()
          .get(picklistFieldApiName)
          .getDescribe()
          .getPicklistValues()
      ) {
        if (thisPicklistEntry.getValue() != OrdwayHelper.NOT_LINKED_TO_ORDWAY) {
          thisPicklistEntryWrapperList.add(
            new PicklistEntryWrapper(
              thisPicklistEntry.getLabel(),
              thisPicklistEntry.getValue()
            )
          );
        }
      }
      return thisPicklistEntryWrapperList;
    } catch (Exception e) {
      throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  /**
   * @description Method to upsert Ordway Bulk Sync Configuration
   * @param bulkSyncConfigurationToUpsert Ordway Bulk Sync Configuration
   * @return List<BulkSyncConfiguration__c> List of Bulk Sync Configuration records
   **/
  @AuraEnabled
  public static List<BulkSyncConfiguration__c> saveBulkSyncConfigurationSetting(String bulkSyncConfigurationToUpsert) {
    List<BulkSyncConfiguration__c> bulkSyncConfigurationRecordsToUpsert;
    if (!String.isEmpty(bulkSyncConfigurationToUpsert)) {
      bulkSyncConfigurationRecordsToUpsert = (List<BulkSyncConfiguration__c>) JSON.deserialize(
        bulkSyncConfigurationToUpsert,
        List<BulkSyncConfiguration__c>.class
      );
    }
    try {
      if (
        bulkSyncConfigurationRecordsToUpsert != null &&
        bulkSyncConfigurationRecordsToUpsert.size() > 0
      ) {
        upsert bulkSyncConfigurationRecordsToUpsert Name;
        return getBulkSyncConfiguration();
      }
      return null;
    } catch (Exception thisException) {
      throw new AuraHandledException(thisException.getMessage());
    }
  }

  /**
   * @description Method to delete Ordway Bulk Sync Configuration Records
   * @param bulkSyncConfigurationToDelete Ordway Bulk Sync Configuration
   **/
  @AuraEnabled
  public static void deleteBulkSyncConfigurationSetting(String bulkSyncConfigurationToDelete) {
    List<BulkSyncConfiguration__c> bulkSyncConfigurationRecordsToDelete;

    if (!String.isEmpty(bulkSyncConfigurationToDelete)) {
      bulkSyncConfigurationRecordsToDelete = (List<BulkSyncConfiguration__c>) JSON.deserialize(
        bulkSyncConfigurationToDelete,
        List<BulkSyncConfiguration__c>.class
      );
    }
    try {
      if (
        Schema.sObjectType.BulkSyncConfiguration__c.isDeletable() &&
        bulkSyncConfigurationRecordsToDelete != null &&
        bulkSyncConfigurationRecordsToDelete.size() > 0
      ) {
        delete bulkSyncConfigurationRecordsToDelete;
      }
    } catch (Exception thisException) {
      throw new AuraHandledException(thisException.getMessage());
    }
  }

 /**
   * @description Method to Get Bulk Sync Configuration Custom Setting
   * @return List<BulkSyncConfiguration__c> Bulk Sync Configuration
   **/
  @AuraEnabled(cacheable=true)
  public static List<BulkSyncConfiguration__c> getBulkSyncConfigurationRecords() {
    return getBulkSyncConfiguration();
  }

  /**
   * @description Method to Get Bulk Sync Configuration Custom Setting
   * @return List<BulkSyncConfiguration__c> Bulk Sync Configuration
   **/
  @AuraEnabled
  public static List<BulkSyncConfiguration__c> getBulkSyncConfiguration() {
    if (Schema.sObjectType.BulkSyncConfiguration__c.isAccessible()) {
      return [
        SELECT Id, Name, Entity__c, Filter__c, Object__c, ObjectLabel__c, OrderNumber__c
        FROM BulkSyncConfiguration__c
        ORDER BY Entity__c, OrderNumber__c ASC
        LIMIT 100
      ];
    }
    return null;
  }

  /**
   * @description Picklist Entry Wrapper class
   **/
  public class PicklistEntryWrapper {
    @AuraEnabled
    public String label { get; set; }
    @AuraEnabled
    public string value { get; set; }

    public PicklistEntryWrapper(
      String picklistEntryLabel,
      String picklistEntryApiName
    ) {
      this.label = picklistEntryLabel;
      this.value = picklistEntryApiName;
    }
  }

  /**
   * @description Picklist Wrapper class
   **/
  public class PicklistWrapper {
    @AuraEnabled
    public List<PicklistEntryWrapper> PicklistWrapperList;
  }

  /**
   * @description Initial wrapper class for Get Ordway Object Component
   **/
  public class InitialWrapper {
    @AuraEnabled
    public List<OrdwayEntityController.PickListWrapper> ordwayEntities;
    @AuraEnabled
    public Boolean isMultiEntityEnabled;
    @AuraEnabled
    public List<DynamicObjectMapping__mdt> dynamicObjectMetadataList;
    @AuraEnabled
    public Boolean isQuoteEnabled;
    @AuraEnabled
    public ApplicationSetting__c applicationSetting;
    @AuraEnabled
    public Boolean isMultiEntityPermissionAssigned;
  }
}