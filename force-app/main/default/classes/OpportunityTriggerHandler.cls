/**
 * @File Name          : OpportunityTriggerHandler.cls
 * @Description        : Opportunity Trigger Handler
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-17-2021
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    10/12/2019   Ordway Labs     Initial Version
 **/
public with sharing class OpportunityTriggerHandler extends TriggerHandler.TriggerHandlerBase {
  static Set<Id> recordIds;
  static Integer recursionCount = 1;

  //To recursively execute the code in the same transaction, set this variable to true
  //Set it to false on after update
  public static Boolean runOnce = true;
  //Opportunity Id which are being updated due to Ordway Quote to Opportunity Sync Process so that we can avoid recursion while sync
  public static Set<Id> syncedOpportunityIds = new Set<Id>();

  public override void beforeInsert(List<SObject> lstNewSObjs) {
    Set<Id> contactIds = new Set<Id>();
    Set<Id> accountIds = new Set<Id>();
    Map<Id, Contact> contactMap = new Map<Id, Contact>();
    Map<Id, Account> accountMap = new Map<Id, Account>();

    for (Opportunity thisOpportunity : (List<Opportunity>) lstNewSObjs) {
      if (thisOpportunity.BillingContact__c != null) {
        contactIds.add(thisOpportunity.BillingContact__c);
      }
      if (thisOpportunity.AccountId != null) {
        accountIds.add(thisOpportunity.AccountId);
      }
    }

    contactMap = ContactService.getContact(contactIds);
    accountMap = AccountService.getAccount(accountIds);

    for (Opportunity thisOpportunity : (List<Opportunity>) lstNewSObjs) {
      OpportunityService.populateBillingAddress(thisOpportunity, contactMap);
      OpportunityService.defaultRenewalTerm(thisOpportunity);
      OpportunityService.defaultPricebook(thisOpportunity);
      OpportunityService.populateOrdwayEntity(thisOpportunity, accountMap);
    }
  }

  public override void beforeUpdate(
    Map<Id, SObject> mapOldSObjs,
    Map<Id, SObject> mapNewSObjs
  ) {
    Set<Id> contactIds = new Set<Id>();
    Set<Id> synchronizableOpportunityIds = new Set<Id>();
    Set<Id> synchronizableOpportunityIdsWithContractId = new Set<Id>();
    Set<Id> accountIds = new Set<Id>();

    //Opportunity Ordway Contract To Be Synced with Opportunity
    Map<Id, Opportunity> ordwayContractToOpportunityMap = new Map<Id, Opportunity>();
    Map<Id, Opportunity> opportunityOldMap = (Map<Id, Opportunity>) mapOldSObjs;
    Map<Id, Contact> contactMap = new Map<Id, Contact>();
    Map<Id, Account> accountMap = new Map<Id, Account>();

    for (
      Opportunity thisOpportunity : (List<Opportunity>) mapNewSObjs.values()
    ) {
      //If Billing Contact is changed
      if (
        SObjectHelper.isChanged(
          Schema.Opportunity.BillingContact__c,
          thisOpportunity,
          (Map<Id, Opportunity>) mapOldSObjs
        )
      ) {
        contactIds.add(thisOpportunity.BillingContact__c);
      }
      if (thisOpportunity.AccountId != null) {
        accountIds.add(thisOpportunity.AccountId);
      }
    }

    contactMap = ContactService.getContact(contactIds);
    accountMap = AccountService.getAccount(accountIds);

    for (
      Opportunity thisOpportunity : (List<Opportunity>) mapNewSObjs.values()
    ) {
      //Populate Billing Address
      OpportunityService.populateBillingAddress(thisOpportunity, contactMap);
      OpportunityService.defaultRenewalTerm(thisOpportunity);
      OpportunityService.populateOrdwayEntity(thisOpportunity, accountMap);
    }
  }

  public override void afterInsert(Map<Id, SObject> mapNewSObjs) {

    ApplicationLog.StartLoggingContext('Opportunity Trigger Handler', 'Create OLI for Change or Renewal Opportunity');

    Map<Id, Opportunity> opportunityNewMap = (Map<Id, Opportunity>) mapNewSObjs;
    Map<Id, Id> relatedOppWithOppIdMap = new Map<Id, Id>();
    List<OpportunityLineItem> oLIListToInsert = new List<OpportunityLineItem>();
    Set<Id> OpportunityIds = new Set<Id>();
    Set<Id> CLIIds = new Set<Id>();
    for(Opportunity thisOpportunity : opportunityNewMap.values()) {
      if(thisOpportunity.RelatedOpportunity__c != null 
      && thisOpportunity.OrdwayContract__c != null 
      && thisOpportunity.Pricebook2Id != null) {
        OpportunityIds.add(thisOpportunity.Id);
      }
    }

    ApplicationLog.Info(' Opportunity Ids =>'+JSON.serializePretty(OpportunityIds));

    if(!OpportunityIds.isEmpty()) {
      for(Opportunity thisOpportunity : [SELECT Id, Pricebook2Id,
                                              RelatedOpportunity__c, 
                                              RelatedOpportunity__r.Pricebook2Id,
                                              OrdwayContract__c 
                                              FROM Opportunity 
                                              WHERE Id IN: OpportunityIds
                                              AND RelatedOpportunity__r.Pricebook2Id != NULL
                                              ]){
        if(thisOpportunity.Pricebook2Id == thisOpportunity.RelatedOpportunity__r.Pricebook2Id){
          relatedOppWithOppIdMap.put(thisOpportunity.RelatedOpportunity__c, thisOpportunity.Id);
        }
      }
      
      ApplicationLog.Info(' Related Opportunity Map =>'+JSON.serializePretty(relatedOppWithOppIdMap));
      ApplicationLog.Info(' OLI =>'+JSON.serializePretty(OpportunityLineItemService.getOpportunityLineItemMap(relatedOppWithOppIdMap.keySet()).values()));

      for(OpportunityLineItem thisOLI : OpportunityLineItemService.getOpportunityLineItemMap(relatedOppWithOppIdMap.keySet()).values()){
        if(thisOLI.ContractLineItem__c != null){
          OpportunityLineItem oppLineClone       = thisOLI.clone(false, true);
          oppLineClone.put('OpportunityId', relatedOppWithOppIdMap.get(thisOLI.OpportunityId));
          oppLineClone.put('TotalPrice', null);
          oLIListToInsert.add(oppLineClone);
          CLIIds.add(oppLineClone.ContractLineItem__c); 
        }
      }
      ApplicationLog.Info(' OLI to Insert =>'+JSON.serializePretty(oLIListToInsert));
      ApplicationLog.FinishLoggingContext();

      if (Schema.sObjectType.OpportunityLineItem.isCreateable()) {
        insert oLIListToInsert;
      }  

      TierController.addOLITiers(oLIListToInsert);
    }                                            
  }

  public override void afterUpdate(
    Map<Id, SObject> mapOldSObjs,
    Map<Id, SObject> mapNewSObjs
  ) {
    Set<Id> contractTermUpdatedOppIds = new Set<Id>();
    Set<Id> synchronizableOpportunityIds = new Set<Id>();
    Set<Id> opportunityIdsToUpdateContract = new Set<Id>();
    Set<Id> ordwayQuoteToBeSynced = new Set<Id>();

    Map<Id, Opportunity> opportunityOldMap = (Map<Id, Opportunity>) mapOldSObjs;
    Map<Id, Opportunity> oppContractIdOpportunityMap = new Map<Id, Opportunity>();

    //Is Contract Term Updated - Then we need to trigger an update on OLI to recalculate the pricing
    for (
      Opportunity thisOpportunity : (List<Opportunity>) mapNewSObjs.values()
    ) {
      if (
        OpportunitySettingController.useOrdwayCalculation() &&
        SObjectHelper.isChanged(
          Schema.Opportunity.ContractTerm__c,
          thisOpportunity,
          (Map<Id, Opportunity>) mapOldSObjs
        )
      ) {
        contractTermUpdatedOppIds.add(thisOpportunity.Id);
      }

      //Create Ordway contract
      if (
        thisOpportunity.OrdwayContract__c == null &&
        OpportunityOrdwayContractSyncController.isSynchronizable(
          thisOpportunity.StageName
        )
      ) {
        synchronizableOpportunityIds.add(thisOpportunity.Id);
      }

      //Update Ordway contract If Opportunity Has Got Contract Id
      if (
        SObjectHelper.isChanged(
          Schema.Opportunity.StageName,
          thisOpportunity,
          (Map<Id, Opportunity>) mapOldSObjs
        ) &&
        thisOpportunity.OrdwayContract__c != null &&
        OpportunityOrdwayContractSyncController.isSynchronizable(
          thisOpportunity.StageName
        )
      ) {
        oppContractIdOpportunityMap.put(
          thisOpportunity.OrdwayContract__c,
          thisOpportunity
        );
        opportunityIdsToUpdateContract.add(thisOpportunity.Id);
      }
    }

    if (!synchronizableOpportunityIds.isEmpty()) {
      //Creates Ordway Contract
      //Creates CLI
      OrdwayContractService.createOrdwayContract(
        synchronizableOpportunityIds,
        (Map<Id, Opportunity>) mapNewSObjs
      );
    }

    if (!oppContractIdOpportunityMap.isEmpty()) {
      //Update Ordway Contract
      //Update CLI
      OrdwayContractService.updateOrdwayContract(
        oppContractIdOpportunityMap,
        (Map<Id, Opportunity>) mapNewSObjs
      );
      //Create CLI for new OLI
      ContractLineItemService.createOrdwayContractLineitem(
        opportunityIdsToUpdateContract,
        (Map<Id, Opportunity>) mapNewSObjs
      );
      //Delete CLI
      ContractLineItemService.deleteCLIOnSync(
        oppContractIdOpportunityMap.keySet(),
        OpportunityLineItemService.getContractLineIdFromOLI(
          opportunityIdsToUpdateContract
        )
      );
    }

    //To Trigger The Pricing Calculation
    if (!contractTermUpdatedOppIds.isEmpty()) {
      OpportunityLineItemService.updateOLI(
        OpportunityLineItemService.getOpportunityLineItemMap( contractTermUpdatedOppIds ) .values()
      );
    }
    runOnce = false;
  }

  public override void beforeDelete(Map<Id, SObject> mapOldSObjs) {
    Set<Id> opportunityIds = new Set<Id>();
    Set<Id> opportunityIdsToDeleteQuote = new Set<Id>();
    if (Schema.sObjectType.OrdwayContract__c.isAccessible()) {
      for (OrdwayContract__c thisContract : [
        SELECT SyncedOpportunityId__c
        FROM OrdwayContract__c
        WHERE SyncedOpportunityId__c IN :mapOldSObjs.keyset()
      ]) {
        opportunityIds.add(thisContract.SyncedOpportunityId__c);
      }
    }

    for (
      Opportunity thisOpportunity : (List<Opportunity>) mapOldSObjs.values()
    ) {
      //If Opportunity has got Ordway Subscription Id, then it should not be deleted
      if (
        !FeatureManagement.checkPermission('CanDeleteOpportunity') &&
        thisOpportunity.OrdwaySubscriptionID__c != null &&
        thisOpportunity.OrdwayOpportunityType__c ==
        OrdwayHelper.OPPORTUNITY_ORDWAY_NEW_BUSINESS_TYPE
      ) {
        thisOpportunity.addError(
          System.Label.OrdwayLabs.Opportunity_Delete_Message
        );
      }

      //If Opportunity is in Sync with Contract, then it should not be deleted
      else if (
        !FeatureManagement.checkPermission('CanDeleteOpportunity') &&
        thisOpportunity.OrdwayOpportunityType__c !=
        OrdwayHelper.OPPORTUNITY_ORDWAY_NEW_BUSINESS_TYPE &&
        opportunityIds.contains(thisOpportunity.Id)
      ) {
        thisOpportunity.addError(
          System.Label.OrdwayLabs.Opportunity_Delete_Message
        );
      }
      else {
        opportunityIdsToDeleteQuote.add(thisOpportunity.Id);
      }

      if(!opportunityIdsToDeleteQuote.isEmpty()){
        OrdwayQuoteService.deleteOrdwayQuotes(opportunityIdsToDeleteQuote);
      }
  
    }
  }

  //Recursive Check
  public static Boolean executeTrigger(Map<Id, sObject> mapNewSObjs) {
    //On Insert Return True
    if (mapNewSObjs == null) {
      return true;
    } else if (
      recordIds != null && recordIds.containsAll(mapNewSObjs.keySet()) &&
      recursionCount > 2
    ) {
      //On Update - Return False For WorkFlow Field Update / Process Builder Update
      return runOnce;
    } else {
      recordIds = new Set<Id>();
      recursionCount = math.mod(recursionCount, 2) != 0 ? 1 : recursionCount;
      recursionCount++;
      recordIds.addAll(mapNewSObjs.keySet());
      return true;
    }
  }
}