/**
 * @File Name          : OrdwayButtonController_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    12/31/2019   Ordway Labs     Initial Version
 **/
@isTest
private class OrdwayButtonController_Test {
  @isTest
  private static void test_OrdwayButtonController() {
    //Opportunity_OrdwayContract Object Field Mapping Metadata
    String opportunity_OrdwayContractFieldMapping =
      '[{"fieldAPIName":"Account__c","fieldDataTypeKey":"Lookup(Account)","fieldLabel":"Account","sourceFieldAPIName":"AccountId"},' +
      '{"fieldAPIName":"AutoRenew__c","fieldDataTypeKey":"Checkbox","fieldLabel":"Auto Renew","sourceFieldAPIName":"IsPrivate"},' +
      '{"fieldAPIName":"BillingContact__c","fieldDataTypeKey":"Lookup(Contact)","fieldLabel":"Billing Contact","sourceFieldAPIName":"BillingContact__c"},' +
      '{"fieldAPIName":"BillingStartDate__c","fieldDataTypeKey":"Date","fieldLabel":"Billing Start Date","sourceFieldAPIName":"BillingStartDate__c"},' +
      '{"fieldAPIName":"ContractEffectiveDate__c","fieldDataTypeKey":"Date","fieldLabel":"Contract Effective Date","sourceFieldAPIName":"ContractEffectiveDate__c"},' +
      '{"fieldAPIName":"ContractTerm__c","fieldDataTypeKey":"Picklist","fieldLabel":"Contract Term","sourceFieldAPIName":"ContractTerm__c"},' +
      '{"fieldAPIName":"OrdwaySubscriptionID__c","fieldDataTypeKey":"Text","fieldLabel":"Ordway Subscription ID","sourceFieldAPIName":"OrdwaySubscriptionID__c"},' +
      '{"fieldAPIName":"RenewalTerm__c","fieldDataTypeKey":"Picklist","fieldLabel":"Renewal Term","sourceFieldAPIName":"RenewalTerm__c"},' +
      '{"fieldAPIName":"ServiceStartDate__c","fieldDataTypeKey":"Date","fieldLabel":"Service Start Date","sourceFieldAPIName":"ServiceStartDate__c"}]';

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    newSetting.OpportunityStage__c = 'Closed Won';
    newSetting.AutomaticallyCreateSubscription__c = false;
    insert newSetting;

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    MetadataService.fieldMappingMetadataMap.put(
      'Opportunity_OrdwayContract',
      TestObjectFactory.createObjectFieldMappingMetadata(
        'Opportunity_OrdwayContract',
        'Opportunity',
        'OrdwayContract__c',
        opportunity_OrdwayContractFieldMapping
      )
    );

    OrdwayContract__c ordwayContract = TestObjectFactory.createOrdwayContract();
    insert ordwayContract;

    Opportunity ordwayOpportunity = TestObjectFactory.createOpportunity();
    ordwayOpportunity.OrdwayContract__c = ordwayContract.Id;
    ordwayOpportunity.StageName = 'Closed Won';
    insert ordwayOpportunity;

    Test.startTest();
    OrdwayButtonController.isPermissionSetAssigned();

    OrdwayButtonController.getDataWrapper(ordwayOpportunity.Id);
    OrdwayButtonController.createOrdwayContract(ordwayOpportunity.Id);
    Opportunity OpportunityToAssert = [
      SELECT Id, OrdwayContract__c, BillingContact__c
      FROM Opportunity
      WHERE Id = :ordwayOpportunity.Id
    ];
    System.assertNotequals(
      null,
      OpportunityToAssert.OrdwayContract__c,
      'Ordway Contract should be created'
    );
    Quote__c quoteToAssert = OrdwayButtonController.getDynamicMappingForQuote(
      OpportunityToAssert.Id
    );
    System.assertNotequals(null, quoteToAssert, 'should not be null');
    try {
      OrdwayButtonController.createOrdwayContract('invalidId');
    } catch (Exception ex) {
      System.assertEquals(
        'Invalid id: invalidId',
        ex.getMessage(),
        'Should return this message : Invalid id: invalidId'
      );
    }
    OrdwayButtonController.updateOrdwayContract(ordwayOpportunity.Id);
    try {
      OrdwayButtonController.updateOrdwayContract('invalidId');
    } catch (Exception ex) {
      System.assertEquals(
        'Invalid id: invalidId',
        ex.getMessage(),
        'Should return this message : Invalid id: invalidId'
      );
    }
    Test.stopTest();
  }

  @isTest
  private static void isQuoteObjectEnabled() {
    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableQuotes__c = true;
    insert newSetting;
    Test.startTest();
    Boolean isEnabled = OrdwayButtonController.isQuoteObjectEnabled();
    Test.stopTest();
    system.assertEquals(true, isEnabled, 'Should return true');
  }
}