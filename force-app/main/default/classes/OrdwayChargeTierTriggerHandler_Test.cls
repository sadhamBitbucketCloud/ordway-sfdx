/**
 * @File Name          : OrdwayChargeTierTriggerHandler_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-25-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    09/04/2021        Initial Version
 **/
@isTest
private class OrdwayChargeTierTriggerHandler_Test {
  @isTest
  private static void deleteAllRelatedTiers_Charge() {
    Test.startTest();
    TierController.validateOnDelete = false;
    Plan__c thisPlan = TestObjectFactory.createOrdwayPlan();
    insert thisPlan;

    Charge__c thisCharge       = TestObjectFactory.createCharge();
    thisCharge.Name            = 'Test Charge';
    thisCharge.OrdwayPlan__c   =  thisPlan.Id;
    insert thisCharge;
    
    OrdwayChargeTierTriggerHandler.deleteTier = true;

    ChargeTier__c thisChargeTier = TestObjectFactory.createChargeTier();
    thisChargeTier.Charge__c     = thisCharge.Id;
    insert thisChargeTier;
    
    TierController.deleteAllRelatedTiers(new Set<Id>{ thisCharge.Id } , 'ChargeTier__c', 'Charge__c');
    Test.stopTest();
    System.assertEquals(0, [SELECT Id FROM ChargeTier__c].size());
  }
}