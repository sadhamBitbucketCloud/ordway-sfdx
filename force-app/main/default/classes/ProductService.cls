public inherited sharing class ProductService {

  public static void createOrdwayProduct(List<Product2> product2List){
    
    List<OrdwayProduct__c> ordwayProductListToInsert = new List<OrdwayProduct__c>();

    for(Product2 thisProduct : product2List) {
      if( thisProduct.OrdwayProduct__c ) {
        //Create Ordway Product - Single Entity
        ordwayProductListToInsert.add(createOrdwayProduct(thisProduct));
      }
    }

    SObjectAccessDecision securityDecision = Security.stripInaccessible( AccessType.CREATABLE, ordwayProductListToInsert );
    insert securityDecision.getRecords();
  }

  public static void upsertOrdwayProduct(Map<Id, Product2> oldProduct2Map, Map<Id, Product2> newProduct2Map, Boolean createOrdwayProduct){

    List<OrdwayProduct__c> ordwayProductList = new List<OrdwayProduct__c>();

    Map<Id, List<OrdwayProduct__c>> ordwayProductMap = getOrdwayProduct(newProduct2Map.keySet());

    for(Product2 thisProduct : newProduct2Map.values()) {
      
      //If Ordway Product exists for a Product2 Record already, then update the values based on the field mapping
      if(ordwayProductMap.get(thisProduct.Id) != null 
        && thisProduct.OrdwayProduct__c 
        && oldProduct2Map.get(thisProduct.Id).OrdwayProduct__c ) {

          for(OrdwayProduct__c oPro : ordwayProductMap.get(thisProduct.Id)){

            OrdwayProduct__c thisOrdwayProduct = (OrdwayProduct__c)populateChangedFields(
                                                                                          thisProduct, 
                                                                                          new OrdwayProduct__c(), 
                                                                                          oldProduct2Map.get(thisProduct.Id),
                                                                                          oPro.Id
                                                                                        );
            if(thisOrdwayProduct != null && thisOrdwayProduct.Id != null){
              ordwayProductList.add(thisOrdwayProduct);
            }
          }
      }

      //If Ordway Product Record doesn't exists, then create a new one
      if(thisProduct.OrdwayProduct__c 
        && !oldProduct2Map.get(thisProduct.Id).OrdwayProduct__c
        && ordwayProductMap.get(thisProduct.Id) == null 
        && createOrdwayProduct){
          ordwayProductList.add(createOrdwayProduct(thisProduct));
      } 
    }
    OrdwayProductTriggerHandler.runOnce = false;
    SObjectAccessDecision securityDecision = Security.stripInaccessible( AccessType.UPSERTABLE, ordwayProductList );
    upsert securityDecision.getRecords();
  }

  public static OrdwayProduct__c createOrdwayProduct(Product2 thisProduct) {
    OrdwayProduct__c thisOrdwayProduct = (OrdwayProduct__c) SObjectFieldMappingHelper.dynamicSobjectMapping(thisProduct, new OrdwayProduct__c(), true);
    thisOrdwayProduct.Product2Id__c = thisProduct.Id;
    return thisOrdwayProduct;
  }

  public static Map<Id, List<OrdwayProduct__c>> getOrdwayProduct( Set<Id> productIds ){

    Map<Id, List<OrdwayProduct__c>> ordwayProductMap = new Map<Id, List<OrdwayProduct__c>>();

    for(OrdwayProduct__c thisOrdwayProduct : OrdwayProductService.getOrdwayProducts(productIds, 'OrdwayLabs__Product2Id__c')){
        if(ordwayProductMap.containsKey(thisOrdwayProduct.Product2Id__c)){
          ordwayProductMap.get(thisOrdwayProduct.Product2Id__c).add(thisOrdwayProduct);
        }
        else{ 
          ordwayProductMap.put(thisOrdwayProduct.Product2Id__c, new List<OrdwayProduct__c>{thisOrdwayProduct});
        }
    }
     return ordwayProductMap;
  }

  public static Sobject populateChangedFields(SObject sourceSObject, SObject targetSobject, Sobject oldValue, Id recordId) {

    Boolean isChanged = false;
    Schema.sObjectType sourceSobjectType = sourceSObject.getSObjectType();
    Schema.sObjectType targetSobjectType = targetSobject.getSObjectType();
    String metaDatakey = SObjectFieldMappingController.getUniqueMetadataKeyValue(string.valueOf(sourceSobjectType) +'_to_' +string.valueOf(targetSobjectType));
    
    if (MetadataService.fieldMappingMetadataMap != null 
    && MetadataService.fieldMappingMetadataMap.containsKey(metaDatakey)
    && MetadataService.fieldMappingMetadataMap.get(metaDatakey).FieldMappingJSON__c != null) {

      for (OrdwayFieldMappingParser.MappingParser thisMapping : OrdwayFieldMappingParser.parse(
                                                                MetadataService.fieldMappingMetadataMap.get(metaDatakey).FieldMappingJSON__c
                                                                                              )) {
        if (oldValue.get(thisMapping.sourceFieldAPIName) != sourceSObject.get(thisMapping.sourceFieldAPIName)) {
          targetSobject.put( thisMapping.fieldAPIName, sourceSObject.get(thisMapping.sourceFieldAPIName) );
          isChanged = true;
        }
      }
    }

    if(isChanged){
      targetSobject.put('Id', recordId);
      return targetSobject;
    }
     return null;
  }  
}