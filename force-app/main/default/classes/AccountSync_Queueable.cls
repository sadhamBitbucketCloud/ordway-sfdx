/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 11-05-2020
 * @last modified by  : 
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   11-05-2020      Initial Version
**/
public with sharing class AccountSync_Queueable implements Queueable, Database.AllowsCallouts {
  public Map<Id, Account> accountMap;

  public AccountSync_Queueable(Map<Id, Account> accountMap) {
    this.accountMap  = accountMap;
  }

  public void execute(QueueableContext context) {
    try {
      List<Account> accountListToSync = new List<Account>(AccountService.getAccountListToSync(accountMap));

      if(!accountListToSync.isEmpty()){

        HttpResponse thisResponse = OrdwayService.syncAccountToOrdway(accountListToSync);
        if (thisResponse !=null && thisResponse.getStatusCode() == 200) {

          SObject sObjectRecord;
          Map<String, Object> thisDeserializedMap = (Map<String, Object>) JSON.deserializeUntyped(thisResponse.getBody());
          sObjectRecord = OrdwayHelper.getSObjectRecord(thisDeserializedMap, Schema.sObjectType.Account, new Account());
          sObjectRecord.put('Id', accountListToSync[0].Id);
          
          AccountTriggerHandler.runOnce = false; //Turn The Trigger Off
          if (Schema.sObjectType.Account.isUpdateable()) {
              update sObjectRecord;
          }
        }
      }
    }
    catch (Exception ex) {
      ApplicationLog.Error(ex);
      ApplicationLog.FinishLoggingContext();
    }
  }
}