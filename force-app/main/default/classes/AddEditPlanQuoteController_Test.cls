/**
 * @File Name          : AddEditPlanQuoteController_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/5/2020        Initial Version
 **/
@isTest
private class AddEditPlanQuoteController_Test {
  @isTest
  static void getOrdwayQLI() {
    ApplicationSetting__c newApplicationSetting = TestObjectFactory.createApplicationSetting();
    insert newApplicationSetting;

    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    Contact newContact = TestObjectFactory.createContact();
    insert newContact;

    Product2 newProduct = TestObjectFactory.createProduct();
    insert newProduct;

    Pricebook2 newPricebook = TestObjectFactory.createPricebook();
    insert newPricebook;

    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.Pricebook2Id = newPricebook.Id;
    insert newOpportunity;

    PricebookEntry newPBEStandard = new PricebookEntry();
    newPBEStandard.Product2Id = newProduct.Id;
    newPBEStandard.Pricebook2Id = Test.getStandardPricebookId();

    PricebookEntry newPBEStd = TestObjectFactory.createPriceBookEntry(
      newPBEStandard
    );
    insert newPBEStd;

    PricebookEntry newPBEntry = new PricebookEntry();
    newPBEntry.Product2Id = newProduct.Id;
    newPBEntry.Pricebook2Id = newPricebook.Id;

    PricebookEntry newPBE = TestObjectFactory.createPriceBookEntry(newPBEntry);
    insert newPBE;

    //Insert Quote
    Quote__c newQuote = TestObjectFactory.createQuote();
    newQuote.Account__c = newAccount.Id;
    newQuote.Opportunity__c = newOpportunity.Id;
    insert newQuote;

    Plan__c newOrdwayPlan = TestObjectFactory.createOrdwayPlan();
    newOrdwayPlan.PlanDetails__c = newOrdwayPlan.PlanDetails__c.replaceAll(
      'testPriceBookEntrySalesforceId',
      String.valueOf(newProduct.Id)
    );
    insert newOrdwayPlan;

    update newOrdwayPlan; //Update Trigger

    Test.startTest();
    String qliListToInsert =
      '[{' +
      '  "IsDeleted": false,' +
      '  "OrdwayLabs__PricingModel__c": "Volume",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "OrdwayLabs__Quantity__c": "75",' +
      '  "OrdwayLabs__Discount__c": "10",' +
      '  "OrdwayLabs__PriceBookEntryId__c": "pricebookEntryId",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00006",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "[ {\n  \\"tier\\" : 1,\n  \\"starting_unit\\" : 0,\n  \\"ending_unit\\" : 20.0,\n  \\"price\\" : \\"5.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 2,\n  \\"starting_unit\\" : 21.0,\n  \\"ending_unit\\" : 50.0,\n  \\"price\\" : \\"2.5\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 3,\n  \\"starting_unit\\" : 51.0,\n  \\"ending_unit\\" : \\"\\",\n  \\"price\\" : \\"350.0\\",\n  \\"type\\" : \\"Flat fee\\"\n} ]",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 0,' +
      '  "OrdwayLabs__Quote__c": "testQuoteId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00006",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Monthly"' +
      '}, {' +
      '  "IsDeleted": false,' +
      '  "OrdwayLabs__PricingModel__c": "Tiered",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "OrdwayLabs__Quantity__c": "25",' +
      '  "OrdwayLabs__Discount__c": "10",' +
      '  "OrdwayLabs__PriceBookEntryId__c": "pricebookEntryId",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00005",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "[ {\n  \\"tier\\" : 1,\n  \\"starting_unit\\" : 0,\n  \\"ending_unit\\" : 10.0,\n  \\"price\\" : \\"5.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 2,\n  \\"starting_unit\\" : 10.0,\n  \\"ending_unit\\" : 20.0,\n  \\"price\\" : \\"3.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 3,\n  \\"starting_unit\\" : 20.0,\n  \\"ending_unit\\" : \\"\\",\n  \\"price\\" : \\"50.0\\",\n  \\"type\\" : \\"Flat fee\\"\n} ]",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 0,' +
      '  "OrdwayLabs__Quote__c": "testQuoteId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00005",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Monthly"' +
      '}, {' +
      '  "IsDeleted": false,' +
      '  "OrdwayLabs__PricingModel__c": "Per unit",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "OrdwayLabs__Quantity__c": "10",' +
      '  "OrdwayLabs__Discount__c": "10",' +
      '  "OrdwayLabs__PriceBookEntryId__c": "pricebookEntryId",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00004",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 500,' +
      '  "OrdwayLabs__Quote__c": "testQuoteId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00004",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Annually"' +
      '}]';

    qliListToInsert = qliListToInsert.replaceAll('\n', '').replaceAll('\r', '');
    qliListToInsert = qliListToInsert.replace(
      'testQuoteId',
      String.valueOf(newQuote.Id)
    );
    qliListToInsert = qliListToInsert.replace(
      'pricebookEntryId',
      String.valueOf(newPBE.Id)
    );
    try {
      AddEditPlanQuoteController.addPlanToQuote(qliListToInsert, '[]');
      AddEditPlanHelper.InitialWrapper thisInitialPlanWrapper = AddEditPlanQuoteController.getInitialWrapper(
        String.valueOf(newQuote.Id),
        String.valueOf(newPricebook.Id)
      );
      System.assertEquals(
        1,
        thisInitialPlanWrapper.qliMap.values().size(),
        'The JSON Contains Only 1 Quote Line Items'
      );
    } catch (Exception e) {
    }
    Test.stopTest();
  }
}