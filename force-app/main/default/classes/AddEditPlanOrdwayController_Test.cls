/**
 * @File Name          : AddEditPlanOrdwayController_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 01-03-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    1/1/2020   Ordway Labs     Initial Version
 **/
@isTest
private class AddEditPlanOrdwayController_Test {
  @isTest
  static void getPlanTest() {
    ApplicationSetting__c newApplicationSetting = TestObjectFactory.createApplicationSetting();
    insert newApplicationSetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    insert newSetting;

    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    Contact newContact = TestObjectFactory.createContact();
    insert newContact;

    Product2 newProduct = TestObjectFactory.createProduct();
    insert newProduct;

    Pricebook2 newPricebook = TestObjectFactory.createPricebook();
    insert newPricebook;

    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.Pricebook2Id = newPricebook.Id;
    insert newOpportunity;

    PricebookEntry newPBEStandard = new PricebookEntry();
    newPBEStandard.Product2Id = newProduct.Id;
    newPBEStandard.Pricebook2Id = Test.getStandardPricebookId();

    PricebookEntry newPBEStd = TestObjectFactory.createPriceBookEntry(
      newPBEStandard
    );
    insert newPBEStd;

    PricebookEntry newPBEntry = new PricebookEntry();
    newPBEntry.Product2Id = newProduct.Id;
    newPBEntry.Pricebook2Id = newPricebook.Id;

    PricebookEntry newPBE = TestObjectFactory.createPriceBookEntry(newPBEntry);
    insert newPBE;

    Plan__c newOrdwayPlan = TestObjectFactory.createOrdwayPlan();
    newOrdwayPlan.PlanDetails__c = newOrdwayPlan.PlanDetails__c.replaceAll(
      'testPriceBookEntrySalesforceId',
      String.valueOf(newProduct.Id)
    );
    insert newOrdwayPlan;

    update newOrdwayPlan; //Update Trigger

    Test.startTest();
    AddEditPlanOrdwayController.DataWrapper thisDataWrapper = AddEditPlanOrdwayController.getDataWrapper(
      String.valueOf(newOpportunity.Id)
    );
    AddEditPlanHelper.InitialWrapper thisInitialPlanWrapper = AddEditPlanOrdwayController.getInitialWrapper(
      String.valueOf(newOpportunity.Id),
      String.valueOf(newPricebook.Id)
    );
    String oliListToInsert = '[{"IsDeleted":false,"Discount":0,"Description":"","OrdwayLabs__PricingModel__c":"Volume","Quantity":1,"OrdwayLabs__OrdwayProductId__c":"P-00001","OrdwayLabs__ChargeType__c":"Recurring","OrdwayLabs__OrdwayChargeId__c":"CHG-00006","OrdwayLabs__OrdwayEffectivePrice__c":0,"OrdwayLabs__PricingType__c":"per_unit","OrdwayLabs__TierJSON__c":"[ {\n  \\"tier\\" : 1,\n  \\"starting_unit\\" : 0,\n  \\"ending_unit\\" : 1000.0,\n  \\"price\\" : \\"1.0\\",\n  \\"type\\" : \\"Per unit\\"\n}]","OrdwayLabs__OrdwayListPrice__c":0,"OpportunityId":"testOpportunityId","PricebookEntryId":"testPricebookEntryId","OrdwayLabs__ChargeName__c":"Recurring Volume","OrdwayLabs__OrdwayPlanId__c":"PLN-00006","OrdwayLabs__OrdwayUndiscountedContractValue__c":0,"OrdwayLabs__OrdwayDiscountedContractValue__c":0,"OrdwayLabs__BillingPeriod__c":"Monthly"}]';
    oliListToInsert = oliListToInsert.replaceAll('\n', '').replaceAll('\r', '');

    oliListToInsert = oliListToInsert.replace(
      'testOpportunityId',
      String.valueOf(newOpportunity.Id)
    );
    oliListToInsert = oliListToInsert.replace(
      'testPricebookEntryId',
      String.valueOf(newPBE.Id)
    );
    AddEditPlanOrdwayController.addPlanToOpportunity(oliListToInsert, '[]');
    Map<String, OpportunityLineItem> opportunityLineItemMapByCharge = AddEditPlanOrdwayController.getOLIByPlanChargeId(
      newOpportunity.Id
    );
    Test.stopTest();
    List<OpportunityLineItem> oliListToAssert = [
      SELECT Id, Name, OpportunityId
      FROM OpportunityLineItem
      WHERE OpportunityId = :newOpportunity.Id
    ];
    System.assertEquals(
      1,
      oliListToAssert.size(),
      'The JSON Contains Only one OpportunityLineItem'
    );
    System.assertEquals(
      1,
      opportunityLineItemMapByCharge.size(),
      'The JSON Contains Only one OpportunityLineItem'
    );
  }

  @isTest
  static void populateAttributeTest() {
    Test.startTest();
    List<Schema.SObjectField> thisOpportunityLineItemFields = new List<Schema.SObjectField>{
      OpportunityLineItem.ChargeName__c,
      OpportunityLineItem.TierJSON__c,
      OpportunityLineItem.ChargeType__c,
      OpportunityLineItem.PricingModel__c,
      OpportunityLineItem.OrdwayListPrice__c,
      OpportunityLineItem.BillingPeriod__c,
      OpportunityLineItem.UnitofMeasure__c,
      OpportunityLineItem.IncludedUnits__c
    };
    AddEditPlanHelper.RequiredFieldWrapper thisReqWrapper = new AddEditPlanHelper.RequiredFieldWrapper();
    for (Schema.SObjectField thisField : thisOpportunityLineItemFields) {
      DescribeFieldResult fieldDescribe = thisField.getDescribe();
      String thisFieldType = String.ValueOf(fieldDescribe.getType());
      thisReqWrapper.name = fieldDescribe.getName();
      thisReqWrapper.label = fieldDescribe.getLabel();
      AddEditPlanHelper.populateAttribute(
        thisFieldType,
        thisReqWrapper,
        fieldDescribe
      );
      System.assertEquals(
        thisReqWrapper.name,
        thisReqWrapper.attributeObject.name,
        'Attribute Name Should be equal to field name passed'
      );
    }
    Test.stopTest();
  }

  @isTest
  private static void getCLIByPlanChargeId() {
    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    newSetting.LinkSubscriptionAutomatically__c = true;
    insert newSetting;

    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    Contact ordwayContact = TestObjectFactory.createContact();
    insert ordwayContact;

    OrdwayContract__c ordwayContract = new OrdwayContract__c();
    ordwayContract.BillingContact__c = ordwayContact.Id;  
    ordwayContract.ContractEffectiveDate__c = system.today();
    ordwayContract.BillingStartDate__c = system.today();
    ordwayContract.ServiceStartDate__c = system.today();
    ordwayContract.Account__c = newAccount.Id;  
    insert ordwayContract;

    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    insert newOpportunity;

    ContractLineItem__c ordwayContractLineItem = new ContractLineItem__c();
    ordwayContractLineItem.OrdwayLabs__OrdwayContract__c = ordwayContract.Id;
    ordwayContractLineItem = TestObjectFactory.createContractLineItem(
      ordwayContractLineItem
    );
    insert ordwayContractLineItem;
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();

    Integer RESPONSE_CODE = 200;
    String RESPONSE_BODY =
      '{ ' +
      ' "customer" : { ' +
      '   "Website" : null,' +
      '   "Name" : "Demo"' +
      '  },' +
      ' "plans" : [ {' +
      '   "OrdwayLabs__ChargeType__c" : "One time",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "1000.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "1000.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Recurring",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "500.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "500.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Usage based",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 5.0,' +
      ' "OrdwayLabs__Quantity__c" : 100.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "950.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "10.00"' +
      '} ],' +
      '"OrdwayLabs__OrdwayMRR__c" : null,' +
      '"OrdwayLabs__OrdwayContractStatus__c" : "Active",' +
      '"OrdwayLabs__AutoRenew__c" : true,' +
      '"OrdwayLabs__RenewalTerm__c" : "12",' +
      '"OrdwayLabs__ContractTerm__c" : "12",' +
      '"OrdwayLabs__ContractEffectiveDate__c" : "2020-03-27",' +
      '"OrdwayLabs__BillingStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__ServiceStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__TotalContractValue__c" : 111520.0,' +
      '"OrdwayLabs__CurrentTermEndDate__c" : "2021-03-23",' +
      '"OrdwayLabs__CurrentTermStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__CustomerId__c" : "C-00001",' +
      '"Id" : "testContractId"' +
      '}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(
        RESPONSE_CODE,
        RESPONSE_BODY.replace(
          'testContractId',
          String.valueOf(ordwayContract.Id)
        )
      )
    );
    Map<String, ContractLineItem__c> thisMap = AddEditPlanOrdwayController.getCLIByPlanChargeId(ordwayContract.Id);
    Test.stopTest();
    system.assertNotEquals(null, thisMap);
  }
}