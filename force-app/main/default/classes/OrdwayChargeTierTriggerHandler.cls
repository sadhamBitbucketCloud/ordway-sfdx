public with sharing class OrdwayChargeTierTriggerHandler extends TriggerHandler.TriggerHandlerBase {	

  public static Boolean deleteTier = true; //Set this to false from manage tier component controller

	public override void beforeInsert(List<SObject> listNewSObjs) {
    if(deleteTier) {
			Set<Id> chargeIds = new Set<Id>();
      for(OrdwayLabs__ChargeTier__c thisChargeTier : (List<OrdwayLabs__ChargeTier__c>) listNewSObjs){
        if(thisChargeTier.OrdwayLabs__Charge__c != null){
          chargeIds.add(thisChargeTier.OrdwayLabs__Charge__c);
        }
        
        if(!chargeIds.isEmpty()){
          TierController.deleteAllRelatedTiers(chargeIds, 'OrdwayLabs__ChargeTier__c', 'OrdwayLabs__Charge__c');
        }
      }
    }
		
	}

	public override void beforeDelete(Map<Id, SObject> mapOldSObjs) {
		for(OrdwayLabs__ChargeTier__c thisChargeTier : (List<OrdwayLabs__ChargeTier__c>) mapOldSObjs.values()){
			if(TierController.validateOnDelete){
			  TierController.addTierDeleteError(thisChargeTier);
			}
		}
	}
}