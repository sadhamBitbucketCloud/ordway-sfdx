/**
 * @File Name          : FeatureAccessController.cls
 * @Description        : Feature Access Controller
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/5/2020        Initial Version
 **/
public with sharing class FeatureAccessController {
  /**
   * @description Method to enable Ordway Quote Feature in the current Org
   **/
  public static void changeQuoteProtection() {
    if (!Test.isRunningTest()) {
      FeatureManagement.changeProtection(
        'OrdwayLabs__Quote__c',
        'CustomObject',
        'Unprotected'
      );
      FeatureManagement.changeProtection(
        'OrdwayLabs__QuoteLineItem__c',
        'CustomObject',
        'Unprotected'
      );
      FeatureManagement.changeProtection(
        'OrdwayLabs__QuoteLineItemTier__c',
        'CustomObject',
        'Unprotected'
      );
    } else {
      throw new FeatureAccessException(
        'Ordway Quote feature not currently licensed. Please contact Ordway support for more details'
      );
    }
  }

  /**
   * @description Method to check whether the current Org has Ordway Quote License
   * @return Boolean true or false based on the Response
   **/
  public static Boolean hasQuoteLicense() {
    HttpResponse thisResponse = OrdwayService.getQuoteSetting();
    if (thisResponse != null && thisResponse.getStatusCode() == 200) {
      Map<String, Object> quoteSettingMap = new Map<String, Object>();
      quoteSettingMap = (Map<String, Object>) JSON.deserializeUntyped(
        thisResponse.getBody()
      );
      Map<String, Object> planMap = (Map<String, Object>) JSON.deserializeUntyped(
        JSON.serialize(quoteSettingMap.get('payload'))
      );
      return (Boolean) planMap.get('quotes');
    } else {
      return false;
    }
  }

  public class FeatureAccessException extends Exception {
  }
}