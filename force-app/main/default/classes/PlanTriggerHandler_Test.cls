/**
 * @File Name          : PlanTriggerHandler_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-25-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    15/02/2021        Initial Version
 **/
@isTest
private class PlanTriggerHandler_Test {
  @isTest
  private static void populateOrdwayEntity() {

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c         = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.Name      = 'Test Entity';
    insert thisEntity;
    
    thisEntity.EntityID__c = thisEntity.Id;
    update thisEntity;

    Map<Id, Entity__c> entityMap = OrdwayEntityHelper.entityMapById;

    Test.startTest();
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      thisOrdwayPlan.EntityID__c    = thisEntity.EntityID__c;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
      insert thisOrdwayProduct;
    Test.stopTest();

    Plan__c ordwayPlan = [SELECT Id, PlanDetails__c, OrdwayEntity__c, Name
                                              FROM Plan__c];
    system.assertEquals(thisEntity.Id, ordwayPlan.OrdwayEntity__c, 'should return same entity');
  }

  @isTest
  private static void populateEntityName(){

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c         = true;
    insert newSetting;

    Entity__c thisEntity   = TestObjectFactory.createOrdwayEntity();
    thisEntity.Name        = 'Test Entity';
    insert thisEntity; 
        
    thisEntity.EntityID__c = thisEntity.Id;
    update thisEntity;

    Map<Id, Entity__c> entityMap = OrdwayEntityHelper.entityMapById;

    Test.startTest();
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan         = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c  = thisProduct2.Id;
      thisOrdwayPlan.OrdwayEntity__c = thisEntity.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct  = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c     = thisProduct2.Id;
      insert thisOrdwayProduct;
    Test.stopTest();

    Plan__c ordwayPlan = [SELECT Id, OrdwayEntity__c, EntityName__c FROM Plan__c];
    system.assertEquals('Test Entity', ordwayPlan.EntityName__c, 'should return same entity name');
    ordwayPlan.OrdwayEntity__c = null;
    ordwayPlan.EntityId__c = null;
    ordwayPlan.EntityName__c = null;
    update ordwayPlan;
    System.assertEquals(null, [SELECT Id, EntityName__c FROM Plan__c].EntityName__c);
  }

  @isTest
  private static void populateOrdwayPlanId() {

    Test.startTest();
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      thisOrdwayPlan.ExternalID__c  = 'EXTERNEL:C:C-1234';
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct   = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c      = thisProduct2.Id;
      thisOrdwayProduct.Name               = 'Test OrdwayProduct';
      insert thisOrdwayProduct;
    Test.stopTest();
    System.assertEquals('C-1234', [SELECT Id, PlanId__c FROM Plan__c].PlanId__c, 
                                                    'should return same ordwayplan Id');
  }

  @isTest
  private static void populateOrdwayPlanIdInsert() {

    Test.startTest();
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan          = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c   = thisProduct2.Id;
      thisOrdwayPlan.ExternalID__c    = 'C-1234';
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct   = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c      = thisProduct2.Id;
      thisOrdwayProduct.Name               = 'Test OrdwayProduct';
      insert thisOrdwayProduct;
    Test.stopTest();
    System.assertEquals('C-1234', [SELECT Id, PlanId__c FROM Plan__c].PlanId__c, 
                                                    'should return same ordwayplan Id');
  }

  @isTest
  private static void populateOrdwayPlan(){

    Test.startTest();
    Product2 thisProduct2 = TestObjectFactory.createProduct();
    insert thisProduct2;

    Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
    thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
    insert thisOrdwayPlan;

    thisOrdwayPlan.ExternalID__c    = 'C-123456';
    update thisOrdwayPlan;

    OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
    thisOrdwayProduct.Name             = 'Test OrdwayProduct';
    insert thisOrdwayProduct;

  Test.stopTest();
  System.assertEquals('C-123456', [SELECT Id, PlanId__c FROM Plan__c].PlanId__c, 
                                                  'should return same ordwayplan Id');
  }

  @isTest
  private static void populateOrdwayPlanIdOnUpate(){

    Test.startTest();
    Product2 thisProduct2 = TestObjectFactory.createProduct();
    insert thisProduct2;

    Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();                                                             
    thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
    insert thisOrdwayPlan;

    thisOrdwayPlan.ExternalID__c    = 'EXTERNEL:C:C-1234';
    update thisOrdwayPlan;

    OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
    thisOrdwayProduct.Name             = 'Test OrdwayProduct';
    insert thisOrdwayProduct;

    
  Test.stopTest();
  System.assertEquals('C-1234', [SELECT Id, PlanId__c FROM Plan__c].PlanId__c, 
                                                  'should return same ordwayplan Id');
  }

  @isTest
  private static void populateChangedEntityName(){

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c         = true;
    insert newSetting;

    Entity__c thisEntity   = TestObjectFactory.createOrdwayEntity();
    thisEntity.Name        = 'Test Entity';
    insert thisEntity; 
        
    thisEntity.EntityID__c = thisEntity.Id;
    update thisEntity;

    Entity__c thisEntityNew   = TestObjectFactory.createOrdwayEntity();
    thisEntityNew.Name        = 'Test Entity New';
    insert thisEntityNew; 
        
    thisEntityNew.EntityID__c = thisEntityNew.Id;
    update thisEntityNew;

    Map<Id, Entity__c> entityMap = OrdwayEntityHelper.entityMapById;

    Test.startTest();
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan            = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c     = thisProduct2.Id;
      thisOrdwayPlan.OrdwayEntity__c = thisEntity.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct  = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c     = thisProduct2.Id;
      insert thisOrdwayProduct;
    Test.stopTest();

    Plan__c ordwayPlan = [SELECT Id, EntityName__c FROM Plan__c];
    system.assertEquals('Test Entity', ordwayPlan.EntityName__c, 'should return same entity name');
    thisOrdwayPlan.OrdwayEntity__c = thisEntityNew.Id;
    update thisOrdwayPlan;
    System.assertEquals('Test Entity New', [SELECT Id, EntityName__c FROM Plan__c].EntityName__c);
  }
}