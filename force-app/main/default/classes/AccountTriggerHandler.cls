/**
 * @description       : Trigger Handler for Account Object Trigger
 * @last modified on  : 12-31-2020
 * @last modified by  : 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   08-12-2020      Initial Version
 **/
public with sharing class AccountTriggerHandler extends TriggerHandler.TriggerHandlerBase {

  static Set<Id> recordIds;

  //To recursively execute the code in the same transaction, set this variable to true
  //Set it to false on after update
  public static Boolean runOnce = true;

  public override void beforeInsert(List<SObject> lstNewSObjs) {
    for (Account thisAccount : (List<Account>) lstNewSObjs) {
      if (OpportunitySettingController.isMultiEntityEnabled()) {
        OrdwayEntityHelper.populateOrdwayEntity(thisAccount);
      }
    }
  }

  public override void beforeUpdate( Map<Id, SObject> mapOldSObjs, Map<Id, SObject> mapNewSObjs ) {

    for (Account thisAccount : (List<Account>) mapNewSObjs.values()) {
      if (OpportunitySettingController.isMultiEntityEnabled()) {
        OrdwayEntityHelper.populateOrdwayEntity(thisAccount);
      }
    }
  }

  public override void afterInsert(Map<Id, SObject> mapNewSObjs) {

    Map<Id, Account> accountNewMap = (Map<Id, Account>) mapNewSObjs;

    if(!accountNewMap.isEmpty()
      && OpportunitySettingController.isAutoSyncAccountToOrdway()
      && !System.isBatch()
      && !System.isQueueable()
      && !System.isFuture() ){
      AccountService.enqueueAccountSyncQueueable(accountNewMap);
    }
  }

  public override void afterUpdate( Map<Id, SObject> mapOldSObjs, Map<Id, SObject> mapNewSObjs ) {

    Map<Id, Account> accountNewMap = (Map<Id, Account>) mapNewSObjs;

    if(!accountNewMap.isEmpty()
      && OpportunitySettingController.isAutoSyncAccountToOrdway()
      && !System.isBatch()
      && !System.isQueueable()
      && !System.isFuture() ){ //Adding Queueable Condition here
      //When a contract syncs automatically (Q1) & activates automatically (Q2),  then below Q should not execute
      AccountService.enqueueAccountSyncQueueable(accountNewMap);
    }
    runOnce = false;
  }

  //Recursive Check
  public static Boolean executeTrigger(Map<Id, sObject> mapNewSObjs) {
    //On Insert Return True
    if (mapNewSObjs == null) {
      return true;
    } else if (
      recordIds != null
      && recordIds.containsAll(mapNewSObjs.keySet())
    ) {
      return runOnce;
    } else {
      recordIds = new Set<Id>();
      recordIds.addAll(mapNewSObjs.keySet());
      return true;
    }
  }
}