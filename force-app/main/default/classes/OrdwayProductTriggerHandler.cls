public with sharing class OrdwayProductTriggerHandler extends TriggerHandler.TriggerHandlerBase {
  
  public static Boolean runOnce = true;
  public static Set<Id> product2IdSet = new Set<Id>();

  public override void beforeInsert(List<SObject> lstNewSObjs) {
    
    OrdwayEntityHelper.getEntities();

    Map<String, Product2> product2Map = new Map<String, Product2>();

    for(OrdwayProduct__c pro : (List<OrdwayProduct__c>) lstNewSObjs) {
      if(pro.Product2Id__c != null){
        product2IdSet.add(pro.Product2Id__c);
      }
      OrdwayProductService.populateOrdwayEntity(pro); //Populate Ordway Entity
      OrdwayProductService.populateEntity(pro); //Populate Entity Id & Entity Name
      OrdwayProductService.populateOrdwayProductId(pro); //Populate Ordway Product Id

      Product2 pro2 = OrdwayProductService.createProduct2(pro);
      if(pro2 != null) { product2Map.put(pro.OrdwayProductID__c, pro2); }
    }

    OrdwayProductService.createProduct2((List<OrdwayProduct__c>) lstNewSObjs, product2Map);

  }

  public override void afterInsert(Map<Id, SObject> mapNewSObjs) {
    if(!product2IdSet.isEmpty()){
      OrdwayProductService.updateProduct2(product2IdSet);
    }
  }
  
  public override void beforeUpdate(Map<Id, SObject> mapOldSObjs, Map<Id, SObject> mapNewSObjs) {

    OrdwayEntityHelper.getEntities();
    
    for(OrdwayProduct__c pro : (List<OrdwayProduct__c>) mapNewSObjs.values()) {
      OrdwayProductService.populateEntity(pro); //Populate Entity Id & Entity Name
      OrdwayProductService.populateOrdwayProductId(pro); //Populate Ordway Product Id
    }

  }

  public override void afterUpdate(Map<Id, SObject> mapOldSObjs, Map<Id, SObject> mapNewSObjs) {
    Map<Id, OrdwayProduct__c> oldMap = (Map<Id, OrdwayProduct__c>) mapOldSObjs;
    Map<Id, OrdwayProduct__c> newMap = (Map<Id, OrdwayProduct__c>) mapNewSObjs;
    Map<Id, String> productIDWithOproductIdMap = new Map<Id, String>();
    List<Charge__c> chargeList = new List<Charge__c>();

    if( !OpportunitySettingController.isMultiEntityEnabled() && runOnce) {
      List<Product2> product2List = new List<Product2>();
      for (OrdwayProduct__c oPro : newMap.values()) {
        if(oPro.Product2Id__c != null){
          product2List.add((Product2) SObjectFieldMappingHelper.dynamicSobjectMapping(oPro, new Product2(Id = oPro.Product2Id__c), true));
        }
      }
      update product2List;  
    }

    //Update Product ID at charge level when Ordway Product ID is updated in Ordway Product
    for (OrdwayProduct__c oPro : newMap.values()) {
      if(oPro.OrdwayProductID__c != null 
        && oPro.Product2Id__c != null 
        && oldMap.get(oPro.Id).OrdwayProductID__c != oPro.OrdwayProductID__c){
          productIDWithOproductIdMap.put(oPro.Product2Id__c, oPro.OrdwayProductID__c);
      }
    }

    for(Charge__c thisCharge : [SELECT Id, Product__c, ProductID__c
                                      FROM Charge__c 
                                      WHERE Product__c IN: productIDWithOproductIdMap.keySet()] ){
      thisCharge.put('OrdwayLabs__ProductID__c', productIDWithOproductIdMap.get(thisCharge.Product__c));
      chargeList.add(thisCharge);
    }
    update chargeList;    
  }
}