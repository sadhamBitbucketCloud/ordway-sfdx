/**
 * @File Name          : PlanPickerConfiguration_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 05-03-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    15/02/2021        Initial Version
 **/
@isTest
public class PlanPickerConfiguration_Test {
  @isTest
  public static void getInitialWrapper_OLI() {
    PlanPicker__mdt thisPlanPicker = TestObjectFactory.createPlanPickerMetadata('OLI', 'OLI');
    Test.startTest();
      PlanPickerConfiguration.InitialWrapper thisInitialWrapper = new PlanPickerConfiguration.InitialWrapper();
      thisInitialWrapper = PlanPickerConfiguration.getInitialWrapper('OLI');
      System.assertEquals(1, thisInitialWrapper.planPickerMetadataList.size());
    Test.stopTest();
  }

  @isTest
  public static void getInitialWrapper_OLINegative() {
    PlanPicker__mdt thisPlanPicker = TestObjectFactory.createPlanPickerMetadata('OLI', 'OLI');
    Test.startTest();
      try {
        PlanPickerConfiguration.InitialWrapper thisInitialWrapper = new PlanPickerConfiguration.InitialWrapper();
        thisInitialWrapper = PlanPickerConfiguration.getInitialWrapper('NEGATIVE');
      } catch (Exception ex) {
        system.assertNotEquals(null, ex.getMessage());
      }
    Test.stopTest();
  }

  @isTest
  public static void deployPlanPickerMetadata_OLI() {
    Test.startTest();
      Metadata.DeployCallback callback = new DeployOrdwayObjectFieldMappingMetaData();
      Metadata.DeployResult result = new Metadata.DeployResult();
      result.numberComponentErrors = 1;
      Metadata.DeployCallbackContext context = new Metadata.DeployCallbackContext();
      System.assertEquals(1, result.numberComponentErrors, 'Sample assert since nothing can be use for assertion');
      callback.handleResult(result, context);
    Test.stopTest();
  }

  @isTest
  public static void deployPlanPickerMetadata_OLINegative() {
    String stringToAssert = OrdwayService_Mock.ordwayPlanPickerMDTStringOLI.replace('OLI_PlanBundleID', 'Negative Testing');
    
    Test.startTest();
      try {
        String deploymentId = PlanPickerConfiguration.deployPlanPickerMetadata(stringToAssert, 'OLI');
      } catch (Exception ex) {
        system.assertNotEquals(null, ex.getMessage());
      }
    Test.stopTest();
  }

  @isTest
  public static void getInitialWrapper_QLI() {
    PlanPicker__mdt thisPlanPicker = TestObjectFactory.createPlanPickerMetadata('QLI', 'QLI');
    Test.startTest();
      PlanPickerConfiguration.InitialWrapper thisInitialWrapper = new PlanPickerConfiguration.InitialWrapper();
      thisInitialWrapper = PlanPickerConfiguration.getInitialWrapper('QLI');
      System.assertNotEquals(null, thisInitialWrapper.planPickerMetadataList);
    Test.stopTest();
  }

  @isTest
  public static void getInitialWrapper_QLINegative() {
    PlanPicker__mdt thisPlanPicker = TestObjectFactory.createPlanPickerMetadata('QLI', 'QLI');
    Test.startTest();
      try {
        PlanPickerConfiguration.InitialWrapper thisInitialWrapper = new PlanPickerConfiguration.InitialWrapper();
        thisInitialWrapper = PlanPickerConfiguration.getInitialWrapper('NEGATIVE');
      } catch (Exception ex) {
        system.assertNotEquals(null, ex.getMessage());
      }
    Test.stopTest();
  }

  @isTest
  public static void deployPlanPickerMetadata_QLINegative() {
    String stringToAssert = OrdwayService_Mock.ordwayPlanPickerMDTStringQLI.replace('BillingPeriod', 'Negative Testing');
    Test.startTest();
      try {
        String deploymentId = PlanPickerConfiguration.deployPlanPickerMetadata(stringToAssert, 'QLI');
      } catch (Exception ex) {
        system.assertNotEquals(null, ex.getMessage());
      }
    Test.stopTest();
  }

}