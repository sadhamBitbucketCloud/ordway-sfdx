/**
 * @File Name          : OrdwayButtonController.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-23-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    12/10/2019   Ordway Labs     Initial Version
 **/
public with sharing class OrdwayButtonController {
  /**
   * @description Method to Method To check wheather the user has the Ordway permission set assigned to the user
   * @return Boolean True Or false
   **/
  @AuraEnabled
  public static Boolean isPermissionSetAssigned() {
    return ApplicationSettingController.isOrdwayPermissionAssignedToUser();
  }

  /**
   * @description Method to Method To check wheather the Ordway quote feature is enabled
   * @return Boolean True Or false
   **/
  @AuraEnabled
  public static Boolean isQuoteObjectEnabled() {
    return OpportunitySettingController.isQuotesEnabled() && FeatureManagement.checkPermission('OpportunityCreateQuote');
  }

  /**
   * @description Method to get Initial Data Wrapper
   * @param opportunityId Opportunity record Id
   * @return AddEditPlanOrdwayController.DataWrapper
   **/
  @AuraEnabled
  public static AddEditPlanOrdwayController.DataWrapper getDataWrapper(
    String opportunityId
  ) {
    return AddEditPlanOrdwayController.getDataWrapper(opportunityId);
  }

  /**
   * @description Method to create ordway contract for Opportunity
   * @param opportunityId Opportunity record Id
   * @return String Opportunity Id or error message
   **/
  @AuraEnabled
  public static String createOrdwayContract(String opportunityId) {
    ApplicationLog.StartLoggingContext(
      'OrdwayButtonController',
      'createOrdwayContract()'
    );
    try {
      Set<Id> thisOpportunitySet = new Set<Id>();
      thisOpportunitySet.add(Id.valueof(opportunityId));
      Map<Id, Opportunity> opportunityMap = OpportunityService.getOpportunityMap(
        thisOpportunitySet
      );
      OrdwayContractService.createOrdwayContract(
        thisOpportunitySet,
        opportunityMap
      );
      return opportunityId;
    } catch (Exception thisException) {
      ApplicationLog.addRecordId(opportunityId);

      ApplicationLog.Error(thisException.getMessage(), thisException, true);
      ApplicationLog.FinishLoggingContext();

      return 'ERROR';
    }
  }

  /**
   * @description Method to update ordway contract for Opportunity
   * @param opportunityId Opportunity record Id
   * @return String Ordway contract Id or error message
   **/
  @AuraEnabled
  public static String updateOrdwayContract(String opportunityId) {
    ApplicationLog.StartLoggingContext( 'OrdwayButtonController', 'updateOrdwayContract()' );
    try {
      Map<Id, Opportunity> opportunityMap = OpportunityService.getOpportunityMap( new Set<Id>{ Id.valueof(opportunityId) } );
      Map<Id, Opportunity> ordwayToOpportunityMap = new Map<Id, Opportunity>();

      for (Opportunity thisOpportunity : opportunityMap.values()) {
        if (thisOpportunity.OrdwayContract__c != null) {
          ordwayToOpportunityMap.put( thisOpportunity.OrdwayContract__c, thisOpportunity );
        }
      }
      if (!ordwayToOpportunityMap.isEmpty()) {
        //On Sync, Create & Update & Delete Line Items
        OrdwayContractService.updateOrdwayContract( ordwayToOpportunityMap, opportunityMap );
        ContractLineItemService.createOrdwayContractLineitem( opportunityMap.keySet(), opportunityMap );
        ContractLineItemService.deleteCLIOnSync( ordwayToOpportunityMap.keySet(), OpportunityLineItemService.getContractLineIdFromOLI( opportunityMap.keySet() ) );
      }
      return opportunityMap.get(Id.valueof(opportunityId)).OrdwayContract__c;
    } catch (Exception thisException) {
      ApplicationLog.addRecordId(opportunityId);

      ApplicationLog.Error(thisException.getMessage(), thisException, true);
      ApplicationLog.FinishLoggingContext();

      return 'ERROR';
    }
  }

  /**
   * @description Method to get Dynamic Field Mapping For Creating Quote From Opportunity - Create Quote Button
   * @param opportunityId Opportunity record Id
   * @return Quote__c Ordway Quote
   **/
  @AuraEnabled
  public static Quote__c getDynamicMappingForQuote(String opportunityId) {
    Set<Id> opportunityIds = new Set<Id>();
    opportunityIds.add(opportunityId);
    Map<Id, Opportunity> opportunityMap = OpportunityService.getOpportunityMap(
      opportunityIds
    );
    Quote__c thisOrdwayQuote = (Quote__c) SObjectFieldMappingHelper.dynamicSobjectMapping(
      opportunityMap.get(opportunityId),
      (Quote__c) Quote__c.sObjectType.newSObject(null, true),
      true
    );
    thisOrdwayQuote.QuoteDate__c = System.today();
    thisOrdwayQuote.ExpiryDate__c = System.today().addDays(15);
    return thisOrdwayQuote;
  }
}