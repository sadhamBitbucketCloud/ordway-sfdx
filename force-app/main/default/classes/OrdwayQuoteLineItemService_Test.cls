/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 09-19-2020
 * @last modified by  : 
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   09-19-2020      Initial Version
**/
@isTest
private class OrdwayQuoteLineItemService_Test {
  @isTest
  private static void deleteQuoteLineItemNegative() {
    try {
      OrdwayQuoteLineItemService.deleteQuoteLineItem(
        new Set<Id>{ 'InvalidId' }
      );
    } catch (Exception ex) {
      System.assertEquals(
        'Invalid id: InvalidId',
        ex.getMessage(),
        'Negative Testing for Deleting QLI'
      );
    }
  }
}