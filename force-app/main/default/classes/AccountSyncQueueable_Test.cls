@isTest
private class AccountSyncQueueable_Test {
    @isTest
    private static void syncAccountToOrdway_Insert() {
        ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
        insert thisApplicationsetting;

        OrdwayLabs__ContractPlanPickerSetting__c opportunitySettingInstance = TestObjectFactory.createContractPlanPickerSetting();
        opportunitySettingInstance.AutomaticallySyncAccount__c = true;
        OpportunitySettingController.saveOpportunitySetting(opportunitySettingInstance);

        Account ordwayAccount = TestObjectFactory.createAccount();
        ordwayAccount.OrdwayCustomerId__c = 'C-00011';
        ordwayAccount.Website = null;
        ordwayAccount.Description = null;
        ordwayAccount.Type = null;

        Test.startTest();

        Integer RESPONSE_CODE = 200;
        String RESPONSE_BODY =' {"Name":"Test NP 1410", '
                            +' "Website":"TestGoogle.com", '
                            +' "OrdwayLabs__OrdwayCustomerId__c":"C-00199", '
                            +' "Description":"Test description", '
                            +' "Type":"business", '
                            +' "BillingCity":"Texas", '
                            +' "BillingState":"", '
                            +' "ParentId":null} ';
        Test.setMock(HttpCalloutMock.class, new OrdwayService_Mock( RESPONSE_CODE, RESPONSE_BODY));
        insert ordwayAccount;

        Test.stopTest();
        Account accountToAssert = [SELECT Id, Website, OrdwayCustomerId__c, Description, Type FROM Account];
        System.assertEquals('C-00199', accountToAssert.OrdwayCustomerId__c, 'Should return same as response - C-00199');
        System.assertEquals('Test description', accountToAssert.Description, 'Should return same as response - Test description');
        System.assertEquals('business', accountToAssert.Type, 'Should return same as response - business');
    }

    @isTest
    private static void syncAccountToOrdway_InsertBulk() {
        ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
        insert thisApplicationsetting;

        OrdwayLabs__ContractPlanPickerSetting__c opportunitySettingInstance = TestObjectFactory.createContractPlanPickerSetting();
        opportunitySettingInstance.AutomaticallySyncAccount__c = true;
        OpportunitySettingController.saveOpportunitySetting(opportunitySettingInstance);

        List<Account> accountToInsert = new List<Account>();
        for(Integer i =0; i<=50;i++){
            Account ordwayAccount = TestObjectFactory.createAccount();
            ordwayAccount.OrdwayCustomerId__c = 'C-00011';
            ordwayAccount.Website = null;
            ordwayAccount.Description = null;
            ordwayAccount.Type = null;
            accountToInsert.add(ordwayAccount);
        }
        insert accountToInsert;

        Test.startTest();

        Integer RESPONSE_CODE = 200;
        String RESPONSE_BODY =' {"Name":"Test NP 1410", '
                            +' "Website":"TestGoogle.com", '
                            +' "OrdwayLabs__OrdwayCustomerId__c":"C-00199", '
                            +' "Description":"Test description", '
                            +' "Type":"business", '
                            +' "BillingCity":"Texas", '
                            +' "BillingState":"", '
                            +' "ParentId":null} ';
        Test.setMock(HttpCalloutMock.class, new OrdwayService_Mock( RESPONSE_CODE, RESPONSE_BODY));
        Test.stopTest();
        system.assertEquals(1, [SELECT Id, Type FROM Account WHERE Type ='business'].size(), 'should return only one account as it is not supported for bulk');
    }


    @isTest
    private static void syncAccountToOrdway_Update() {
        ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
        insert thisApplicationsetting;

        OrdwayLabs__ContractPlanPickerSetting__c opportunitySettingInstance = TestObjectFactory.createContractPlanPickerSetting();
        opportunitySettingInstance.AutomaticallySyncAccount__c = true;
        OpportunitySettingController.saveOpportunitySetting(opportunitySettingInstance);

        Account ordwayAccount = TestObjectFactory.createAccount();
        ordwayAccount.OrdwayCustomerId__c = 'C-00011';
        ordwayAccount.Website = null;
        ordwayAccount.Description = null;
        ordwayAccount.Type = null;
        insert ordwayAccount;

        Test.startTest();
        Integer RESPONSE_CODE = 200;
        String RESPONSE_BODY =' {"Name":"Test NP 1410", '
                            +' "Website":"TestGoogle.com", '
                            +' "OrdwayLabs__OrdwayCustomerId__c":"C-00199", '
                            +' "Description":"Test description", '
                            +' "Type":"business", '
                            +' "BillingCity":"Texas", '
                            +' "BillingState":"", '
                            +' "ParentId":null} ';
        Test.setMock(HttpCalloutMock.class, new OrdwayService_Mock( RESPONSE_CODE, RESPONSE_BODY));
        ordwayAccount.Description = 'Test';
        update ordwayAccount;
        Test.stopTest();
        Account accountToAssert = [SELECT Id, Website, OrdwayCustomerId__c, Description, Type FROM Account];
        System.assertEquals('C-00199', accountToAssert.OrdwayCustomerId__c, 'Should return same as response - C-00199');
        System.assertEquals('Test description', accountToAssert.Description, 'Should return same as response - Test description');
        System.assertEquals('business', accountToAssert.Type, 'Should return same as response - business');
    }

    @isTest
    private static void syncAccountToOrdway_Negative() {
        ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
        insert thisApplicationsetting;

        OrdwayLabs__ContractPlanPickerSetting__c opportunitySettingInstance = TestObjectFactory.createContractPlanPickerSetting();
        opportunitySettingInstance.AutomaticallySyncAccount__c = false;
        OpportunitySettingController.saveOpportunitySetting(opportunitySettingInstance);

        Account ordwayAccount = TestObjectFactory.createAccount();
        ordwayAccount.OrdwayCustomerId__c = 'C-00011';
        ordwayAccount.Website = null;
        ordwayAccount.Description = null;
        ordwayAccount.Type = null;

        Test.startTest();
        insert ordwayAccount;
        Test.stopTest();
        Account accountToAssert = [SELECT Id, Website, OrdwayCustomerId__c, Description, Type FROM Account];
        System.assertEquals('C-00011', accountToAssert.OrdwayCustomerId__c, 'Should return C-00011');
        System.assertEquals(null, accountToAssert.Description, 'Should return null');
        System.assertEquals(null, accountToAssert.Type, 'Should return null');
    }
}