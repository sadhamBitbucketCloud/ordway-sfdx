/**
 * @description       : Ordway Entity Helper
 * @author            : Ordway Labs
 * @group             :
 * @last modified on  : 04-14-2021
 * @last modified by  : 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   07-26-2020      Initial Version
 **/
public with sharing class OrdwayEntityHelper {

  public static Map<String, List<OrdwayEntityRule__c>> entityRuleMap {
    get {
      if (entityRuleMap == null) {
        entityRuleMap = new Map<String, List<OrdwayEntityRule__c>>();
        for (OrdwayEntityRule__c thisRule : getEntityRule()) {
          if (!entityRuleMap.containsKey(thisRule.Object__c)) {
            entityRuleMap.put(thisRule.Object__c, new List<OrdwayEntityRule__c>{});
          }
          entityRuleMap.get(thisRule.Object__c).add(thisRule);
        }
      }
      return entityRuleMap;
    }
    private set;
  }

  public static Map<String, String> entityMap {
    get {
      if (entityMap == null) {
        entityMap = new Map<String, String>();
        for (Entity__c thisEntity : getOrdwayEntities().values()) {
          entityMap.put(thisEntity.EntityID__c, thisEntity.Name);
        }
      }
      return entityMap;
    }
    private set;
  }


  public static Entity__c defaultEntity { get; set; }
  public static Map<String, Id> entityMapByEntityId { get; set; }

  public static Map<Id, Entity__c> entityMapById {
    get {
      if( entityMapById == null ) {

        entityMapById = new Map<Id, Entity__c>();
        entityMapByEntityId = new Map<String, Id>();

        entityMapById = getOrdwayEntities();

        for(Entity__c e : entityMapById.values()) {
          if(e.Default__c) { defaultEntity = e; }

          entityMapByEntityId.put(e.EntityID__c, e.Id);
        }
      }
      return entityMapById;
    }
    private set;
  }

  public static Map<Id, Entity__c> getEntities() {
    return entityMapById;
  }

  /**
   * @description Method to get Ordway Entity Rules
   * @return List<OrdwayEntityRule__c> List of Ordway Entity Rules
   **/
  public static List<OrdwayEntityRule__c> getEntityRule() {
    if (Schema.sObjectType.OrdwayEntityRule__c.isAccessible()) {
      return [
        SELECT Id, Name, EntityID__c, Value__c, Object__c, Field__c
        FROM OrdwayEntityRule__c
        ORDER BY Name ASC
        LIMIT 500
      ];
    }
    return null;
  }

  /**
   * @description Method to get Ordway Entity
   * @return List<OrdwayEntityRule__c> List of Ordway Entity
   **/
  public static Map<Id, Entity__c> getOrdwayEntities() {
    Map<Id, Entity__c> entityMap = new Map<Id, Entity__c>();
    if (Schema.sObjectType.Entity__c.isAccessible()) {
      entityMap = new Map<Id, Entity__c>([SELECT Id, Name, EntityID__c, Default__c FROM Entity__c LIMIT 100]);
    }
    return entityMap;
  }

  /**
   * @description Method to populate the entity Name based on the Entity Id
   * @param  thisSObject Sobject record in which the entity Name Should be populated
   */
  public static void populateOrdwayEntityName(SObject thisSObject) {
    if (
      thisSObject.get(OrdwayHelper.SOBJECT_FIELD_ENTITYID) != null &&
      OpportunitySettingController.isMultiEntityEnabled() &&
      !entityMap.isEmpty() &&
      entityMap.containsKey( String.valueOf(thisSObject.get(OrdwayHelper.SOBJECT_FIELD_ENTITYID)) )
    ) {
      thisSObject.put(
        OrdwayHelper.SOBJECT_FIELD_ENTITYNAME,
        entityMap.get( String.valueOf(thisSObject.get(OrdwayHelper.SOBJECT_FIELD_ENTITYID)) )
      );
    }
  }

  /**
   * @description Method to populate Entity Id
   * @param  thisSObject Sobject record in which the entity Id Should be populated
   */
  public static void populateOrdwayEntity(Sobject thisSObject) {

    thisSObject.put(OrdwayHelper.SOBJECT_FIELD_ENTITYID, null);
    thisSObject.put(OrdwayHelper.SOBJECT_FIELD_ENTITYNAME, null);
    String thisSobjectType = String.valueOf(thisSObject.getSObjectType());

    //Populate Only If Multi Entity Is Enabled
    if ( OpportunitySettingController.isMultiEntityEnabled() 
      && !entityRuleMap.isEmpty() 
      && entityRuleMap.containsKey(thisSobjectType) ) {

      for (OrdwayEntityRule__c thisRule : entityRuleMap.get(thisSobjectType)) {
        if (
          thisSObject.get(thisRule.Field__c) != null &&
          String.valueOf(thisSObject.get(thisRule.Field__c)) .containsIgnoreCase(thisRule.Value__c) ) {

          thisSObject.put( OrdwayHelper.SOBJECT_FIELD_ENTITYID, thisRule.EntityID__c );
        }
      }
    }

    if (thisSObject.get(OrdwayHelper.SOBJECT_FIELD_ENTITYID) != null) {
      populateOrdwayEntityName(thisSObject);
    }
  }
}