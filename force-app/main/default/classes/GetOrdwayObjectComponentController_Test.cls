/**
 * @File Name          : GetOrdwayObjectComponentController_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 01-03-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/19/2020        Initial Version
 **/
@isTest
private class GetOrdwayObjectComponentController_Test {
  @isTest
  private static void callGetOrdwayObject_Plan() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Product2 thisProduct2 = TestObjectFactory.createProduct();
    insert thisProduct2;

    Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
    thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
    insert thisOrdwayPlan;

    OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
    insert thisOrdwayProduct;

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new MockAddPlan_Test(thisOrdwayPlan.Id));
    GetOrdwayObjectComponentController.callGetOrdwayObject_Batch(
      1,
      'Plan',
      null,
      null,
      null,
      null,
      null,
      false
    );
    GetOrdwayObjectComponentController.callGetOrdwayObject_Batch(
      1,
      'QuoteTemplate',
      null,
      null,
      null,
      null,
      null,
      false
    );
    Test.stopTest();
    System.assertEquals(
      3,
      [SELECT Id FROM Plan__c].size(),
      'Mock response returns three Plans Only'
    );
  }

  @isTest
  private static void callGetOrdwayObject_AccountWithCustomerId() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Account newAccount = TestObjectFactory.createAccount();
    newAccount.OrdwayCustomerId__c = 'C-00106';
    newAccount.Name = 'ordway.com';
    insert newAccount;

    Account newAccount1 = TestObjectFactory.createAccount();
    newAccount1.OrdwayCustomerId__c = 'C-00107';
    newAccount1.Name = 'Test Account';
    insert newAccount1;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayAccountString;
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    GetOrdwayObjectComponentController.callGetOrdwayObject_Batch(
      1,
      'Account',
      null,
      null,
      'customer_id',
      'C-00106',
      null,
      false
    );
    GetOrdwayObjectComponentController.getInitialWrapper();
    Test.stopTest();
    system.assertEquals(
      2,
      [SELECT Id FROM Account].size(),
      'should return 2 account as in response'
    );
  }

  @isTest
  private static void callGetOrdwayObject_AccountWithUpdatedDate() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Account newAccount = TestObjectFactory.createAccount();
    newAccount.OrdwayCustomerId__c = 'C-00106';
    newAccount.Name = 'ordway.com';
    insert newAccount;

    Account newAccount1 = TestObjectFactory.createAccount();
    newAccount1.OrdwayCustomerId__c = 'C-00107';
    newAccount1.Name = 'Test Account';
    insert newAccount1;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayAccountString;
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    GetOrdwayObjectComponentController.callGetOrdwayObject_Batch(
      1,
      'Account',
      System.today().addMonths(-1),
      null,
      'updated_date',
      null,
      null,
      false
    );
    Test.stopTest();
    system.assertEquals(
      2,
      [SELECT Id, Name FROM Account].size(),
      'should return 2 account as in response'
    );
  }

  @isTest
  private static void fetchPickListValue() {
    Test.startTest();
    GetOrdwayObjectComponentController.PicklistWrapper thisPEWrapper = new GetOrdwayObjectComponentController.PicklistWrapper();
    thisPEWrapper = GetOrdwayObjectComponentController.getPicklistWrapper(
      'OrdwayLabs__OrdwayContract__c',
      'OrdwayLabs__OrdwayContractStatus__c'
    );
    Test.stopTest();
    system.assertNotEquals(null, thisPEWrapper, 'should not return null');
  }

  @isTest
  private static void getBulkSyncConfigurationRecords() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    BulkSyncConfiguration__c thisBulkSyncConfig = TestObjectFactory.createBulkSyncConfigurationSetting();
    insert thisBulkSyncConfig;

    Test.startTest();
    List<BulkSyncConfiguration__c> bulkSyncConfigToAssert = GetOrdwayObjectComponentController.getBulkSyncConfigurationRecords();
    Test.stopTest();
    system.assertEquals(1, bulkSyncConfigToAssert.size(), 'should return 1 as we are inserting a record');
  }

  @isTest
  private static void saveBulkSyncConfigurationSetting() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Test.startTest();
    String recordToInsert = ' [ '+
                                ' { '+
                                    ' "Name": "Account", '+
                                    ' "OrdwayLabs__Filter__c": "7", '+
                                    ' "OrdwayLabs__Object__c": "Account", '+
                                    ' "OrdwayLabs__ObjectLabel__c": "Accounts", '+
                                    ' "OrdwayLabs__OrderNumber__c": 1 '+
                                ' } '+
                            ' ] ';

    List<BulkSyncConfiguration__c> bulkSyncConfigToAssert = GetOrdwayObjectComponentController.saveBulkSyncConfigurationSetting(recordToInsert);
    Test.stopTest();
    system.assertEquals(1, bulkSyncConfigToAssert.size(), 'should return 1 as we are inserting a record');

    String recordToDelete = ' [ '+
                              ' { '+
                                  ' "Id":"recordId",'+
                                  ' "Name": "Account", '+
                                  ' "OrdwayLabs__Filter__c": "7", '+
                                  ' "OrdwayLabs__Object__c": "Account", '+
                                  ' "OrdwayLabs__ObjectLabel__c": "Accounts", '+
                                  ' "OrdwayLabs__OrderNumber__c": 1 '+
                              ' } '+
                            ' ] ';

    GetOrdwayObjectComponentController.deleteBulkSyncConfigurationSetting(recordToDelete.replace('recordId',String.valueOf(bulkSyncConfigToAssert[0].Id)));
    system.assertEquals(0, [SELECT Id FROM BulkSyncConfiguration__c].size(), 'should return null');
  }

  @isTest
  private static void saveBulkSyncConfigurationSetting_Negative() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;
    
    Test.startTest();
    try{
      List<BulkSyncConfiguration__c> bulkSyncConfigToAssert = GetOrdwayObjectComponentController.saveBulkSyncConfigurationSetting(null);
    }
    catch(Exception ex){
      system.assertNotEquals(null, ex.getMessage());
    }
    Test.stopTest();
  }

  @isTest
  private static void deleteBulkSyncConfigurationSetting_Negative() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;
    
    Test.startTest();
    try{
      GetOrdwayObjectComponentController.deleteBulkSyncConfigurationSetting(null);
    }
    catch(Exception ex){
      system.assertNotEquals(null, ex.getMessage());
    }
    Test.stopTest();
  }

  @isTest
  private static void fetchPickListValueNegative() {
    Test.startTest();
    GetOrdwayObjectComponentController.PicklistWrapper thisPEWrapper = new GetOrdwayObjectComponentController.PicklistWrapper();
    try {
      thisPEWrapper = GetOrdwayObjectComponentController.getPicklistWrapper(
        'null',
        'OrdwayLabs__OrdwayContractStatus__c'
      );
    } catch (Exception e) {
      system.assertEquals(
        'Script-thrown exception',
        e.getMessage(),
        'should be error'
      );
    }
    Test.stopTest();
  }

  @isTest
  private static void multiEntity() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = new Entity__c();
    thisEntity.Name = '0';
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    OrdwayEntityRule__c thisRule = new OrdwayEntityRule__c();
    thisRule.EntityID__c = 'TEST';
    thisRule.Name = '0';
    thisRule.Object__c = 'Opportunity';
    thisRule.Field__c = 'Name';
    insert thisRule;

    Account newAccount = TestObjectFactory.createAccount();
    newAccount.OrdwayCustomerId__c = 'C-00106';
    newAccount.Name = 'ordway.com';
    insert newAccount;

    Account newAccount1 = TestObjectFactory.createAccount();
    newAccount1.OrdwayCustomerId__c = 'C-00107';
    newAccount1.Name = 'Test Account';
    insert newAccount1;

    Map<String, DynamicObjectMapping__mdt> thisMDTMap = MetadataService.dynamicObjectMapping;
    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayAccountString;
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    GetOrdwayObjectComponentController.callGetOrdwayObject_Batch(
      1,
      'Account',
      null,
      null,
      'customer_id',
      'C-00106',
      'TEST',
      false
    );
    GetOrdwayObjectComponentController.getInitialWrapper();
    Test.stopTest();
    system.assertEquals(
      2,
      [SELECT Id FROM Account].size(),
      'should return 2 account as in response'
    );
  }
}