public with sharing class OrdwayOppLineItemTierTriggerHandler extends TriggerHandler.TriggerHandlerBase {
	
	public static Boolean deleteTier = true;

	public override void beforeInsert(List<SObject> listNewSObjs) {
		if(deleteTier){
			Set<Id> opportunityLineItemIds = new Set<Id>();
			for(OrdwayLabs__OpportunityLineItemTier__c thisOpportunityLineItemTier : (List<OrdwayLabs__OpportunityLineItemTier__c>) listNewSObjs){
				if(thisOpportunityLineItemTier.OrdwayLabs__OpportunityProduct__c != null){
					opportunityLineItemIds.add(thisOpportunityLineItemTier.OrdwayLabs__OpportunityProduct__c);
				}
				
				if(!opportunityLineItemIds.isEmpty()){
					TierController.deleteAllRelatedTiers(opportunityLineItemIds, 'OrdwayLabs__OpportunityLineItemTier__c', 'OrdwayLabs__OpportunityProduct__c');
				}
			}
		}
	}

	public override void beforeDelete(Map<Id, SObject> mapOldSObjs) {
		for(OrdwayLabs__OpportunityLineItemTier__c thisOpportunityLineItemTier : (List<OrdwayLabs__OpportunityLineItemTier__c>) mapOldSObjs.values()){
			if(TierController.validateOnDelete){
				TierController.validateBeforeTierDelete(thisOpportunityLineItemTier);
			}
		}
	}
}