/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 04-25-2021
 * @last modified by  : 
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   09-01-2020      Initial Version
**/
@isTest
private class OrdwayEntityController_Test {
  @isTest
  private static void getOrdwayEntitySetting() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    OrdwayEntityRule__c thisRule = TestObjectFactory.createOrdwayEntityRule();
    thisRule.EntityID__c = 'TEST';
    thisRule.Object__c = 'Opportunity';
    thisRule.Field__c = 'Name';
    insert thisRule;
    Test.startTest();
    OrdwayEntityController.ordwayEntitySettingInitialWrapper thisInitialWrapper = new OrdwayEntityController.ordwayEntitySettingInitialWrapper();
    thisInitialWrapper = OrdwayEntityController.getOrdwayEntitySetting();
    Test.stopTest();
    System.assertEquals(
      1,
      thisInitialWrapper.ordwayEntities.size(),
      'Should return 1 Entity'
    );
  }

  @isTest
  private static void saveOrdwayEntitySetting() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    Entity__c thisEntityNew = TestObjectFactory.createOrdwayEntity();
    thisEntityNew.Name = '1';
    thisEntityNew.EntityID__c = 'TESTNEW';
    insert thisEntityNew;

    OrdwayEntityRule__c thisRule = TestObjectFactory.createOrdwayEntityRule();
    thisRule.EntityID__c = 'TEST';
    thisRule.Object__c = 'Opportunity';
    thisRule.Field__c = 'Name';
    insert thisRule;

    String OrdwayEntityToUpsert = '[{"Id":"thisEntityId","Name":"TEST1","OrdwayLabs__EntityID__c":"TEST1"}]';
    String OrdwayEntityToDelete = '[{"Id":"thisEntityNew","Name":"TESTNEW","OrdwayLabs__EntityID__c":"TESTNEW"}]';

    Test.startTest();
    OrdwayEntityController.saveOrdwayEntitySetting(
      OrdwayEntityToUpsert.replace(
        'thisEntityId',
        String.valueOf(thisEntity.Id)
      ),
      OrdwayEntityToDelete.replace(
        'thisEntityNew',
        String.valueOf(thisEntityNew.Id)
      )
    );
    Test.stopTest();
    Entity__c thisEntityToAssert = [
      SELECT Id, Name
      FROM Entity__c
      WHERE Id = :thisEntity.Id
    ];
    system.assertEquals(
      'TEST1',
      thisEntityToAssert.Name,
      'Should return updated entity name'
    );
  }

  @isTest
  private static void saveOrdwayEntityRuleSetting() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    Entity__c thisEntityNew = TestObjectFactory.createOrdwayEntity();
    thisEntityNew.Name = '1';
    thisEntityNew.EntityID__c = 'TESTNEW';
    insert thisEntityNew;

    OrdwayEntityRule__c thisRule = TestObjectFactory.createOrdwayEntityRule();
    thisRule.EntityID__c = 'TEST';
    thisRule.Object__c = 'Opportunity';
    thisRule.Field__c = 'Name';
    insert thisRule;

    OrdwayEntityRule__c thisNewRule = TestObjectFactory.createOrdwayEntityRule();
    thisNewRule.EntityID__c = 'TESTNew';
    thisNewRule.Name = '1';
    thisNewRule.Object__c = 'Opportunity';
    thisNewRule.Field__c = 'Name';
    insert thisNewRule;

    String OrdwayEntityRuleToUpsert = '[{"Name":"0","OrdwayLabs__EntityID__c":"Test","OrdwayLabs__Value__c":"TestNew","OrdwayLabs__Object__c":"Opportunity","OrdwayLabs__Field__c":"Name"}]';
    String OrdwayEntityRuleToDelete = '[{"Name":"1","OrdwayLabs__EntityID__c":"TESTNew","OrdwayLabs__Value__c":"TESTNew","OrdwayLabs__Object__c":"Opportunity","OrdwayLabs__Field__c":"Name"}]';

    Test.startTest();
    OrdwayEntityController.saveOrdwayEntityRuleSetting(
      OrdwayEntityRuleToUpsert,
      OrdwayEntityRuleToDelete
    );
    Test.stopTest();
    OrdwayEntityRule__c thisRuleToAssert = [
      SELECT Id, value__c
      FROM OrdwayEntityRule__c
      WHERE Name = '0'
    ];
    system.assertEquals(
      'TestNew',
      thisRuleToAssert.value__c,
      'Should return updated entity rule value'
    );
  }

  @isTest
  private static void saveOrdwayEntitySettingNegative() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    Entity__c thisEntityNew = TestObjectFactory.createOrdwayEntity();
    thisEntityNew.Name = '1';
    thisEntityNew.EntityID__c = 'TESTNEW';
    insert thisEntityNew;

    OrdwayEntityRule__c thisRule = TestObjectFactory.createOrdwayEntityRule();
    thisRule.EntityID__c = 'TEST';
    thisRule.Object__c = 'Opportunity';
    thisRule.Field__c = 'Name';
    insert thisRule;
    String OrdwayEntityToUpsert = '[{"Id":"thisEntityId","Name":"TEST1","OrdwayLabs__EntityID__c":"TEST1"}]';
    String OrdwayEntityToDelete = null;

    Test.startTest();

    try {
      OrdwayEntityController.saveOrdwayEntitySetting(
        OrdwayEntityToUpsert,
        OrdwayEntityToDelete
      );
    } catch (Exception ex) {
    }
    Entity__c thisEntityToAssert = [
      SELECT Id, Name
      FROM Entity__c
      WHERE Id = :thisEntity.Id
    ];
    system.assertEquals(
      '0',
      thisEntityToAssert.Name,
      'Should return same entity name'
    );
    Test.stopTest();
  }

  @isTest
  private static void getOrdwayEntityRuleConfigurationInitialWrapper() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    OrdwayEntityRule__c thisRule = TestObjectFactory.createOrdwayEntityRule();
    thisRule.EntityID__c = 'TEST';
    thisRule.Object__c = 'Opportunity';
    thisRule.Field__c = 'Name';
    insert thisRule;

    Test.startTest();
    OrdwayEntityController.InitialWrapper thisInitialWrapper = new OrdwayEntityController.InitialWrapper();
    thisInitialWrapper = OrdwayEntityController.getOrdwayEntityRuleConfigurationInitialWrapper();
    system.assertNotEquals(null, thisInitialWrapper);
    Test.stopTest();
  }

  @isTest
  private static void populateOrdwayEntity_Account() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    OrdwayEntityRule__c thisRule = TestObjectFactory.createOrdwayEntityRule();
    thisRule.EntityID__c = 'TEST';
    thisRule.Object__c = 'Account';
    thisRule.Field__c = 'Name';
    thisRule.Value__c = 'TEST';
    insert thisRule;

    Test.startTest();
    Account newAccount = TestObjectFactory.createAccount();
    newAccount.EntityID__c = null;
    newAccount.EntityName__c = null;
    insert newAccount;
    newAccount.Description = 'Test';
    update newAccount;
    Test.stopTest();

    Account accountToAssert = [
      SELECT Id, Name, EntityID__c, OrdwayCustomerId__c, EntityName__c
      FROM Account
    ];
    system.assertEquals(
      '0',
      accountToAssert.EntityName__c,
      'Should return entity name 0'
    );
    system.assertEquals(
      'TEST',
      accountToAssert.EntityID__c,
      'Should return entity value TEST'
    );
  }

  @isTest
  private static void populateOrdwayEntity_Opportunity() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    OrdwayEntityRule__c thisRule = TestObjectFactory.createOrdwayEntityRule();
    thisRule.EntityID__c = 'TEST';
    thisRule.Object__c = 'Account';
    thisRule.Field__c = 'Name';
    thisRule.Value__c = 'TEST';
    insert thisRule;

    OrdwayEntityRule__c thisRuleOpp = TestObjectFactory.createOrdwayEntityRule();
    thisRuleOpp.EntityID__c = 'TEST';
    thisRuleOpp.Name = '0';
    thisRuleOpp.Object__c = 'Opportunity';
    thisRuleOpp.Field__c = 'Name';
    thisRuleOpp.Value__c = 'TEST';
    insert thisRuleOpp;

    Account newAccount = TestObjectFactory.createAccount();
    newAccount.EntityID__c = null;
    newAccount.EntityName__c = null;
    insert newAccount;

    Test.startTest();
    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.AccountId = newAccount.Id;
    newOpportunity.EntityID__c = null;
    newOpportunity.EntityName__c = null;
    insert newOpportunity;
    Test.stopTest();

    Opportunity opportunityToAssert = [
      SELECT Id, Name, EntityID__c, EntityName__c
      FROM Opportunity
    ];
    system.assertEquals(
      '0',
      opportunityToAssert.EntityName__c,
      'Should return entity name 0'
    );
    system.assertEquals(
      'TEST',
      opportunityToAssert.EntityID__c,
      'Should return entity value TEST'
    );
    opportunityToAssert.EntityID__c = null;
    opportunityToAssert.EntityName__c = null;
    update opportunityToAssert;
    Opportunity thisOpportunity = [
      SELECT Id, Name, EntityID__c, EntityName__c
      FROM Opportunity
    ];
    system.assertEquals(
      '0',
      thisOpportunity.EntityName__c,
      'Should return entity name 0'
    );
    system.assertEquals(
      'TEST',
      thisOpportunity.EntityID__c,
      'Should return entity value TEST'
    );
  }

  @isTest
  private static void getOrdwayQuoteTemplate() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    OrdwayEntityRule__c thisRule = TestObjectFactory.createOrdwayEntityRule();
    thisRule.EntityID__c = 'TEST';
    thisRule.Object__c = 'Opportunity';
    thisRule.Field__c = 'Name';
    insert thisRule;

    QuoteTemplate__c thisQT = new QuoteTemplate__c();
    thisQT.Name = 'Quote Template';
    thisQT.Active__c = true;
    thisQT.TemplateId__c = 'TMPLT-00020';
    insert thisQT;

    QuoteTemplate__c thisQTWithEntity = new QuoteTemplate__c();
    thisQTWithEntity.Name = 'Quote Template';
    thisQTWithEntity.Active__c = true;
    thisQTWithEntity.TemplateId__c = 'TMPLT-00021';
    thisQTWithEntity.EntityID__c = 'TEST';
    thisQTWithEntity.EntityName__c = '0';
    insert thisQTWithEntity;

    Test.startTest();
    List<QuoteTemplate__c> QTListToAssert = OrdwayQuoteTemplateController.getOrdwayQuoteTemplate(
      'TEST'
    );
    system.assertEquals(
      1,
      QTListToAssert.Size(),
      'Should return one QT as it is entity specific'
    );
    Test.stopTest();
  }

  @isTest
  private static void getOrdwayPlan() {
    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c         = true;
    insert newSetting;

    Entity__c thisEntity   = TestObjectFactory.createOrdwayEntity();
    thisEntity.Name        = 'Test Entity';
    insert thisEntity; 

    thisEntity.EntityID__c = thisEntity.Id;
    update thisEntity;

    Entity__c thisEntityNew   = TestObjectFactory.createOrdwayEntity();
    thisEntityNew.Name        = 'Test Entity New';
    insert thisEntityNew; 

    thisEntityNew.EntityID__c = thisEntityNew.Id;
    update thisEntityNew;

    Map<Id, Entity__c> entityMap = OrdwayEntityHelper.entityMapById;

    Product2 thisProduct2 = TestObjectFactory.createProduct();
    insert thisProduct2;

    Plan__c thisPlan        = TestObjectFactory.createOrdwayPlan();
    thisPlan.Name           = 'Test Plan';
    thisPlan.PlanId__c      = thisProduct2.Id;  
    thisPlan.PlanDetails__c = thisProduct2.Id;
    thisPlan.EntityID__c    = thisEntity.Id;
    thisPlan.Active__c      = true;
    insert thisPlan;

    ContractPlanPickerSetting__c newSettings = TestObjectFactory.createContractPlanPickerSetting();
    newSettings.EnableMultiEntity__c         = true;
    insert newSettings;

    Product2 thisProduct2New = TestObjectFactory.createProduct();
    insert thisProduct2New;

    Plan__c thisOrdwayPlanNew        = TestObjectFactory.createOrdwayPlan();
    thisOrdwayPlanNew.Name           = 'Test Plan New';
    thisOrdwayPlanNew.PlanId__c      = thisProduct2New.Id;  
    thisOrdwayPlanNew.PlanDetails__c = thisProduct2New.Id;
    thisOrdwayPlanNew.EntityID__c    = thisEntityNew.Id;
    thisOrdwayPlanNew.Active__c      = true;
    thisOrdwayPlanNew.OrdwayEntity__c = thisEntityNew.Id;
    insert thisOrdwayPlanNew;

    Test.startTest();
    List<Plan__c> thisPlanList = OrdwayPlanController.getOrdwayPlan(thisEntityNew.Id);
    Test.stopTest();
    System.assertEquals(1, thisPlanList.size());
    System.assertEquals('Test Plan New', thisPlanList[0].Name);
  }

  @isTest
  private static void getEntitySetting_200() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    OrdwayEntityRule__c thisRule = TestObjectFactory.createOrdwayEntityRule();
    thisRule.EntityID__c = 'TEST'; 
    thisRule.Object__c = 'Opportunity';
    thisRule.Field__c = 'Name';
    insert thisRule;

    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = '{"payload":{"multi_entity":true},"success":true}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    Boolean hasEntity = OrdwayService.getEntitySetting();
    Test.stopTest();
    system.assertEquals(true, hasEntity, 'should return true');
  }

  @isTest
  private static void getEntitySetting_Negative() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    OrdwayEntityRule__c thisRule = TestObjectFactory.createOrdwayEntityRule();
    thisRule.EntityID__c = 'TEST';
    thisRule.Object__c = 'Opportunity';
    thisRule.Field__c = 'Name';
    insert thisRule;

    OrdwayEntityRule__c eRule = [SELECT Id, Field__c FROM OrdwayEntityRule__c WHERE Id =: thisRule.Id];
    System.assertEquals(eRule.Field__c, 'Name');

    Test.startTest();
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = '{"payload":{"multi_entity":"true"},"success":true}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(RESPONSECODE, RESPONSEBODY)
    );
    try {
      Boolean hasEntity = OrdwayService.getEntitySetting();
    } catch (Exception ex) {
    }
    Test.stopTest();
  }

  @isTest
  private static void getEntityId() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'TEST';
    insert thisEntity;

    OrdwayEntityRule__c thisRule = TestObjectFactory.createOrdwayEntityRule();
    thisRule.EntityID__c = 'TEST';
    thisRule.Object__c = 'Account';
    thisRule.Field__c = 'Name';
    thisRule.Value__c = 'TEST';
    insert thisRule;

    Test.startTest();
    Account newAccount = TestObjectFactory.createAccount();
    newAccount.EntityID__c = null;
    newAccount.EntityName__c = null;
    insert newAccount;
    String entityName = OrdwayService.getEntityId(newAccount.Id);
    system.assertEquals('TEST', entityName, 'Should return Entity name TEST');
    Test.stopTest();
  }
}