/**
 * @File Name          : OrdwayCustomerController.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-21-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/9/2020        Initial Version
 **/
public with sharing class OrdwayCustomerController {

  public static void manageOrdwayAccounts(Map<String, Object> responseMap) {

    ApplicationLog.StartLoggingContext(
      'OrdwayCustomerController',
      'manageOrdwayAccounts()'
    );

    Savepoint sp = Database.setSavepoint();

    try {
      Map<String, Object> recordMap = new Map<String, Object>();
      List<Account> accList = new List<Account>();

      for (Object thisObject : responseMap.values()) {
        for (
          Object thisRecord : (List<Object>) JSON.deserializeUntyped(
            JSON.serialize(thisObject)
          )
        ) {
          recordMap = (Map<String, Object>) JSON.deserializeUntyped(
            JSON.serialize(thisRecord)
          );

          //Skip if Salesforce Id is not present on Ordway customer record
          if (recordMap.get('Id') == null || recordMap.get('Id') == '') {
            continue;
          }

          accList.add(
            (Account) OrdwayHelper.getSObjectRecord(
              recordMap,
              Schema.getGlobalDescribe().get('Account').getDescribe(),
              new Account()
            )
          );
        }
      }

      SObjectAccessDecision sDecision = Security.stripInaccessible(
        AccessType.UPDATABLE,
        accList
      );
      update (List<Account>) sDecision.getRecords();

      ApplicationLog.FinishLoggingContext();
    } catch (Exception ex) {
      Database.rollback(sp);
      ApplicationLog.Error(ex);
      ApplicationLog.FinishLoggingContext();
    }
  }
}