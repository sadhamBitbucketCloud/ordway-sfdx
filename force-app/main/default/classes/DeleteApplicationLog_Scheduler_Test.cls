/**
 * @File Name          : DeleteApplicationLog_Scheduler_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 03-16-2021
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 *         3/13/2021        Ordway Labs            Initial Version
 **/
@isTest
public class DeleteApplicationLog_Scheduler_Test {
  @isTest
  public static void deleteApplicationLog_Scheduler(){
    ApplicationLog__c thisApplicationLog = new ApplicationLog__c();
    insert thisApplicationLog;
    Test.startTest();
    SchedulableContext testsc = null;
    DeleteApplicationLog_Scheduler deleteApplicationLogScheduler = new DeleteApplicationLog_Scheduler();
    deleteApplicationLogScheduler.execute(testsc);
    Test.stopTest();
    System.assertEquals(1, [SELECT Id, Name FROM ApplicationLog__c].Size());
  }

  @isTest
  public static void deleteLogs(){
    ApplicationLog__c thisApplicationLog = new ApplicationLog__c();
    insert thisApplicationLog;
    Test.startTest();
    ApplicationLog.deleteLogs();
    Test.stopTest();
    System.assertEquals(1, [SELECT Id, Name FROM ApplicationLog__c].Size());
 }
}