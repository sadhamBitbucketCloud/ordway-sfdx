/**
 * @File Name          : OrdwayFieldMappingParser.cls
 * @Description        : Ordway FieldMapping Parser
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    10/11/2019   Ordway Labs     Initial Version
 **/
public with sharing class OrdwayFieldMappingParser {
  //JSON Parser structure for Dynamic field mapping record
  public class MappingParser {
    public String fieldAPIName;
    public String fieldDataTypeKey;
    public String fieldLabel;
    public String sourceFieldAPIName;
  }

  public static List<MappingParser> parse(String json) {
    return (List<MappingParser>) System.JSON.deserialize(
      json,
      List<MappingParser>.class
    );
  }
}