/**
 * @description       :
 * @author            :
 * @group             :
 * @last modified on  : 12-14-2020
 * @last modified by  : 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-25-2020      Initial Version
**/
public with sharing class ContactTriggerHandler extends TriggerHandler.TriggerHandlerBase{
  static Set<Id> recordIds;

  //To recursively execute the code in the same transaction, set this variable to true
  //Set it to false on after update
  public static Boolean runOnce = true;

  public override void beforeInsert(List<SObject> lstSObjs) {
    for(Contact thisContact : (List<Contact>)lstSObjs) {
      ContactService.populateExternalId(thisContact);
    }
  }

  public override void afterInsert(Map<Id, SObject> mapNewSObjs) {
      Map<Id, Contact> contactNewMap = (Map<Id, Contact>) mapNewSObjs;
      if(!contactNewMap.isEmpty()
      && OpportunitySettingController.isAutoSyncContactToOrdway()
      && !System.isBatch()
      && !System.isQueueable()
      && !System.isFuture() ){
          ContactService.enqueueContactSyncQueueable(contactNewMap);
      }
  }

  public override void beforeUpdate(Map<Id, SObject> mapOldSObjs, Map<Id, SObject> mapNewSObjs) {
    Map<Id, Contact> contactNewMap = (Map<Id, Contact>) mapNewSObjs;
    for(Contact thisContact : contactNewMap.values()) {
      ContactService.populateExternalId(thisContact);
    }
  }

  public override void afterUpdate(Map<Id, SObject> mapOldSObjs, Map<Id, SObject> mapNewSObjs) {
      Map<Id, Contact> contactNewMap = (Map<Id, Contact>) mapNewSObjs;
      if(!contactNewMap.isEmpty()
      && OpportunitySettingController.isAutoSyncContactToOrdway()
      && !System.isBatch()
      && !System.isQueueable()
      && !System.isFuture() ){
          ContactService.enqueueContactSyncQueueable(contactNewMap);
      }
      runOnce = false;
  }

   //Recursive Check
   public static Boolean executeTrigger(Map<Id, sObject> mapNewSObjs) {
    //On Insert Return True
    if (mapNewSObjs == null) {
      return true;
    } else if (
      recordIds != null
      && recordIds.containsAll(mapNewSObjs.keySet())
    ) {
      return runOnce;
    } else {
      recordIds = new Set<Id>();
      recordIds.addAll(mapNewSObjs.keySet());
      return true;
    }
  }
}