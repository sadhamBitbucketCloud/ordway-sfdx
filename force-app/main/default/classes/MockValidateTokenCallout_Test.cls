/**
 * @File Name          : MockValidateTokenCallout_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    12/31/2019   Ordway Labs     Initial Version
 **/
@isTest
global class MockValidateTokenCallout_Test implements HttpCalloutMock {
  global HTTPResponse respond(HTTPRequest thisRequest) {
    String endPoint =
      OrdwayService.getEndPoint(MetadataService.VALIDATE_TOKEN) +
      Datetime.now().format('yyyy-MM-dd');

    System.assertEquals(
      'callout:OrdwayLabs__OrdwaySandbox' + endPoint,
      thisRequest.getEndpoint()
    );
    System.assertEquals('GET', thisRequest.getMethod());

    // Create a fake response
    HttpResponse thisResponse = new HttpResponse();
    thisResponse.setHeader('Content-Type', 'application/json');
    thisResponse.setStatusCode(200);
    return thisResponse;
  }
}