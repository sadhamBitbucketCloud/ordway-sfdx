/**
 * @File Name          : OrdwayCLITierTriggerHandler_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-27-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    09/04/2021        Initial Version
 **/
@isTest
private class OrdwayCLITierTriggerHandler_Test {
  @isTest
  private static void deleteAllRelatedTiers_CLITier() {

    Test.startTest();
    TierController.validateOnDelete = false;        
    Contact thisOrdwayContact = TestObjectFactory.createContact();
    insert thisOrdwayContact;

    OrdwayContract__c thisOrdwayContract = TestObjectFactory.createOrdwayContract();
    thisOrdwayContract.BillingContact__c = thisOrdwayContact.Id; 
    insert thisOrdwayContract;

    ContractLineItem__c thisOrdwayContractLineItem = TestObjectFactory.createContractLineItem();
    thisOrdwayContractLineItem.Name                ='Test CLI';
    thisOrdwayContractLineItem.OrdwayContract__c   = thisOrdwayContract.Id;
    insert thisOrdwayContractLineItem;

    ContractLineItemTier__c thisCLITier = TestObjectFactory.createContractLineItemTier();
    thisCLITier.StartingUnit__c         = 123;
    thisCLITier.ContractLineItem__c     = thisOrdwayContractLineItem.Id;
    insert thisCLITier;

    ContractLineItemTier__c thisCLITierNew = TestObjectFactory.createContractLineItemTier();
    thisCLITierNew.StartingUnit__c         = 123;
    thisCLITierNew.ContractLineItem__c     = thisOrdwayContractLineItem.Id;
    insert thisCLITierNew;

    OrdwayContractLineItemTierTriggerHandler.deleteTier = true;

    Test.stopTest();

    System.assertEquals(1, [SELECT Id FROM ContractLineItemTier__c].size());
  }
}