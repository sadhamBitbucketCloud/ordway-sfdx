public with sharing class TierController {

  public static Map<String, String> tierParentMap {
    get {
      if( tierParentMap == null ) {
        tierParentMap = new Map<String, String>{'OrdwayLabs__Charge__c' => 'OrdwayLabs__ChargeTier__c',
                                'OpportunityLineItem' => 'OrdwayLabs__OpportunityLineItemTier__c',
                                'OrdwayLabs__QuoteLineItem__c' => 'OrdwayLabs__QuoteLineItemTier__c',
                                'OrdwayLabs__ContractLineItem__c' => 'OrdwayLabs__ContractLineItemTier__c'};
      }
      return tierParentMap;
    }
    set;
  }
  //Boolean variable to skip the delete validation 
  public static Boolean validateOnDelete       = true;
  public static Set<Id> tierObjectIdsAllowedToDelete = new Set<Id>();

  @AuraEnabled
  public static InitialWrapper getInitialWrapper(Id parentId, string sObjectType) {
    try {
        InitialWrapper thisInitialWrapper = new InitialWrapper();
        thisInitialWrapper.tierList = getTiers(parentId, sObjectType);
        thisInitialWrapper.thissObject = getSObject(parentId, sObjectType);
        return thisInitialWrapper;
      } catch (Exception e) {
        throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  @AuraEnabled
  public static Sobject getSObject(Id parentId, string sObjectType) {
    return Database.query('SELECT Id, Name, PricingModel__c FROM '+ sObjectType +' WHERE Id' +' =: parentId');
  }

  @AuraEnabled
  public static List<Sobject> getTiers(Id parentId, string sObjectType) {
   
    Map<String, Schema.SObjectType> thisGD = Schema.getGlobalDescribe();
    Schema.SobjectType thisOT = thisGD.get(tierParentMap.get(sObjectType));
    String parentObject = sObjectType == 'OpportunityLineItem' ? 'OrdwayLabs__OpportunityProduct__c' : sObjectType;
    String parentName;
    if(parentObject == 'OpportunityLineItem'){
          parentName = 'OpportunityLineItem.Name';
    }
    else{
        parentName = parentObject.replace('__c', '__r')+'.Name';     
    }
    return Database.query(
          'SELECT ' +String.join(SObjectHelper.getAllFields(thisOT),',') +','+ parentName + ' FROM '
          +  tierParentMap.get(sObjectType) +' WHERE ' + parentObject 
          +' =: parentId ORDER BY OrdwayLabs__Tier__c ASC'
    );
  }

  @AuraEnabled
  public static void upsertTiers(String tiersToUpsert, string tiersToDelete, string sObjectType, Id parentId) {
    List<Sobject> tierList;
    List<Sobject> tierListToDelete;
    Map<String, System.Type> classTypeMap = new Map<String, System.Type>{'OrdwayLabs__Charge__c' => List<ChargeTier__c>.class,
                                                    'OpportunityLineItem' => List<OpportunityLineItemTier__c>.class,
                                                    'OrdwayLabs__QuoteLineItem__c' => List<QuoteLineItemTier__c>.class,
                                                    'OrdwayLabs__ContractLineItem__c' => List<ContractLineItemTier__c>.class};

    if (!String.isEmpty(tiersToUpsert)) {
      tierList = (List<Sobject>) JSON.deserialize(tiersToUpsert, classTypeMap.get(sObjectType));
    }
    if (tiersToDelete != null) {
        tierListToDelete = (List<Sobject>) JSON.deserialize(tiersToDelete, classTypeMap.get(sObjectType));
    }
    try {
      if (tierList != null && tierList.size() > 0) {
        OrdwayChargeTierTriggerHandler.deleteTier = false;
        OrdwayContractLineItemTierTriggerHandler.deleteTier = false;
        OrdwayOppLineItemTierTriggerHandler.deleteTier = false;
        OrdwayQuoteLineItemTierTriggerHandler.deleteTier = false;
        upsert tierList;

        if(sObjectType != 'OrdwayLabs__Charge__c'){
          Sobject parentRecord = getParentRecord(sObjectType, parentId);
          Decimal listPrice = 0;

          for(sObject thisTier : tierList){
            if (parentRecord.get('OrdwayLabs__PricingModel__c') == OrdwayHelper.VOLUME) {
              listPrice = calculateVolumePricing(thisTier, parentRecord, listPrice, sObjectType);
            }
            if (parentRecord.get('OrdwayLabs__PricingModel__c') == OrdwayHelper.TIERED) {
              listPrice = calculateTieredPricing(thisTier, parentRecord, listPrice, sObjectType);
            }
          }
          parentRecord.put('OrdwayLabs__CalculatedTierPrice__c', listPrice);
          update parentRecord;
        }
      }
      if ((Charge__c.sObjectType.getDescribe().isDeletable() 
        || OpportunityLineItem.sObjectType.getDescribe().isDeletable()
        || QuoteLineItem__c.sObjectType.getDescribe().isDeletable()
        || ContractLineItem__c.sObjectType.getDescribe().isDeletable()) 
        && tierListToDelete != null 
        && tierListToDelete.size() > 0) {
        validateOnDelete = false;
        delete tierListToDelete;
      }
    } catch (Exception thisException) {
      throw new AuraHandledException(thisException.getMessage());
    }
  }

  public static Sobject getParentRecord(String sObjectType, Id parentId){
    
  String quantity = sObjectType == 'OpportunityLineItem' ? ',Quantity' : ',OrdwayLabs__Quantity__c';
    return Database.query(
    'SELECT Id , OrdwayLabs__PricingModel__c '+quantity+ ' FROM '+  sObjectType + ' WHERE  Id =\''+parentId+'\'');
  }

  public static Decimal calculateVolumePricing(sObject thisTier, SObject parentRecord, Decimal listPrice, String sObjectType) {

    Integer startingUnit = Integer.ValueOf(thisTier.get('OrdwayLabs__StartingUnit__c'));
    Integer endingUnit = Integer.ValueOf(thisTier.get('OrdwayLabs__EndingUnit__c'));
    String typeZ = String.valueOf(thisTier.get('OrdwayLabs__PricingType__c'));
    Decimal lPrice = (Decimal)thisTier.get('OrdwayLabs__ListPrice__c');
    Integer  quantity = sObjectType == 'OpportunityLineItem' ? Integer.ValueOf(parentRecord.get('Quantity')) 
                                                            : Integer.ValueOf(parentRecord.get('OrdwayLabs__Quantity__c'));
                       
    if (quantity > startingUnit && 
      (endingUnit == null || quantity <= endingUnit)) {
      if (typeZ == OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice =  lPrice;
        parentRecord.put('OrdwayLabs__OrdwayListPrice__c', lPrice != null && lPrice != 0 ? lPrice : 0);
      } else if (typeZ != OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice = lPrice * quantity;
        parentRecord.put('OrdwayLabs__OrdwayListPrice__c', lPrice != null && lPrice != 0 ? lPrice/quantity : 0);
      }
    }
    return listPrice;
  }

  public static Decimal calculateTieredPricing(sObject thisTier, SObject parentRecord, Decimal listPrice, String sObjectType) {
    
    Integer startingUnit = Integer.ValueOf(thisTier.get('OrdwayLabs__StartingUnit__c'));
    Integer endingUnit = Integer.ValueOf(thisTier.get('OrdwayLabs__EndingUnit__c'));
    String typeZ = String.valueOf(thisTier.get('OrdwayLabs__PricingType__c'));
    Decimal lPrice = (Decimal)thisTier.get('OrdwayLabs__ListPrice__c');
    Integer  quantity = sObjectType == 'OpportunityLineItem' ? Integer.ValueOf(parentRecord.get('Quantity')) 
                                                            : Integer.ValueOf(parentRecord.get('OrdwayLabs__Quantity__c'));

    if (quantity > startingUnit 
        && endingUnit != null 
        && quantity >= endingUnit) {
      // NOT EQ FLAT FEE
      if (typeZ == OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice = listPrice + lPrice;
      } else if (typeZ != OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
          listPrice = listPrice + (endingUnit - startingUnit) * lPrice;
        }
    } else if (quantity > startingUnit 
        && (quantity < endingUnit || endingUnit == null)) {
      // NOT EQ FLAT FEE
      if (typeZ == OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {
        listPrice = listPrice + lPrice;
      } else if (typeZ != OrdwayHelper.OW_PRICING_TYPE_FLAT_FEE) {  
        listPrice = listPrice + (quantity - startingUnit) * lPrice;
      }
    }
    return listPrice;
  }

  public static void validateBeforeTierDelete(SObject thisTierToValidate) {
		if(!tierObjectIdsAllowedToDelete.contains(Id.valueOf(String.valueOf(thisTierToValidate.get(OrdwayHelper.SOBJECT_FIELD_ID))))){
			addTierDeleteError(thisTierToValidate);
		}
	}

	public static void addTierDeleteError(SObject tierSObject) {
		tierSObject.addError('Please edit the record, then delete the required rows');
	}

  public static void deleteAllRelatedTiers(Set<Id> parentLineItemIds, String tierObjectAPIName, String lineItemLookUpFieldAPIName) {
    List<SObject> tiersToDelete = Database.query('SELECT Id, '+lineItemLookUpFieldAPIName+' FROM '+tierObjectAPIName+' WHERE '+lineItemLookUpFieldAPIName+' IN: parentLineItemIds');
    if(!tiersToDelete.isEmpty()){
      validateOnDelete = false;
      SObjectType r = ((SObject)Type.forName('Schema',tierObjectAPIName).newInstance()).getSObjectType();
      if(r.getDescribe().isDeletable()){
        DELETE tiersToDelete;
        Database.emptyRecycleBin(tiersToDelete);
      }
      
      validateOnDelete = true;
    }
  }

  public static void addQLITiers(List<QuoteLineItem__c> QLIList){
    Map<Id, Id> sourceTargetIdMap = new Map<Id, Id>();  
    List<QuoteLineItemTier__c> tiers = new List<QuoteLineItemTier__c>();

    for(QuoteLineItem__c thisQLI : QLIList){
      sourceTargetIdMap.put(thisQLI.OpportunityLineItemId__c, thisQLI.Id);
    }

    for(OpportunityLineItemTier__c oTier : [SELECT Id, OpportunityProduct__c, Tier__c,
                                                      EndingUnit__c, ListPrice__c, 
                                                      PricingType__c, StartingUnit__c
                                                      FROM OpportunityLineItemTier__c 
                                                      WHERE OpportunityProduct__c 
                                                      IN: sourceTargetIdMap.keySet()]){
      QuoteLineItemTier__c qTier = new QuoteLineItemTier__c();
      qTier.QuoteLineItem__c = sourceTargetIdMap.get(oTier.OpportunityProduct__c);
      qTier.Tier__c = oTier.Tier__c;
      qTier.EndingUnit__c = oTier.EndingUnit__c;
      qTier.StartingUnit__c = oTier.StartingUnit__c;
      qTier.ListPrice__c = oTier.ListPrice__c;
      qTier.PricingType__c = oTier.PricingType__c;
      tiers.add(qTier);
    }
    SObjectAccessDecision securityDecision = Security.stripInaccessible( AccessType.CREATABLE, tiers );
    insert securityDecision.getRecords();
  }

  public static void addOLITiers(List<QuoteLineItem__c> QLIList){
    
    Map<Id, Id> sourceTargetIdMap = new Map<Id, Id>();  
    List<OpportunityLineItemTier__c> tiers = new List<OpportunityLineItemTier__c>();

    for(QuoteLineItem__c thisQLI : QLIList){
      sourceTargetIdMap.put(thisQLI.Id, thisQLI.OpportunityLineItemId__c);
    }

    for(QuoteLineItemTier__c qTier : [SELECT Id, QuoteLineItem__c, Tier__c,
                                                  EndingUnit__c, ListPrice__c, 
                                                  PricingType__c, StartingUnit__c
                                                  FROM QuoteLineItemTier__c 
                                                  WHERE QuoteLineItem__c 
                                                  IN: sourceTargetIdMap.keySet()]){
                                                  
      OpportunityLineItemTier__c oTier = new OpportunityLineItemTier__c();
      oTier.OpportunityProduct__c = sourceTargetIdMap.get(qTier.QuoteLineItem__c);
      oTier.Tier__c = qTier.Tier__c;
      oTier.EndingUnit__c = qTier.EndingUnit__c;
      oTier.StartingUnit__c = qTier.StartingUnit__c;
      oTier.ListPrice__c = qTier.ListPrice__c;
      oTier.PricingType__c = qTier.PricingType__c;
      tiers.add(oTier);
    }
    SObjectAccessDecision securityDecision = Security.stripInaccessible( AccessType.CREATABLE, tiers );
    insert securityDecision.getRecords();
  }

  public static void addCLITiers(List<OpportunityLineItem> OLIList){
    Map<Id, Id> sourceTargetIdMap = new Map<Id, Id>();  
    List<ContractLineItemTier__c> CLITierListToInsert = new List<ContractLineItemTier__c>();

    for(OpportunityLineItem thisOLI : OLIList){
      sourceTargetIdMap.put(thisOLI.Id, thisOLI.ContractLineItem__c);
    }
    
    for(OpportunityLineItemTier__c oTier : [SELECT Id, OpportunityProduct__c, Tier__c,
                                                  EndingUnit__c, ListPrice__c, 
                                                  PricingType__c, StartingUnit__c
                                                  FROM OpportunityLineItemTier__c 
                                                  WHERE OpportunityProduct__c 
                                                  IN: sourceTargetIdMap.keySet()]){
      ContractLineItemTier__c cTier = new ContractLineItemTier__c();
      
      if( sourceTargetIdMap.get(oTier.OpportunityProduct__c) == null) { continue; }

      cTier.ContractLineItem__c = sourceTargetIdMap.get(oTier.OpportunityProduct__c);
      cTier.Tier__c = oTier.Tier__c;
      cTier.EndingUnit__c = oTier.EndingUnit__c;
      cTier.StartingUnit__c = oTier.StartingUnit__c;
      cTier.ListPrice__c = oTier.ListPrice__c;
      cTier.PricingType__c = oTier.PricingType__c;
      CLITierListToInsert.add(cTier);
    }
    SObjectAccessDecision securityDecision = Security.stripInaccessible( AccessType.CREATABLE, CLITierListToInsert );
    insert securityDecision.getRecords();
  }

  public static void addOLITiers(List<OpportunityLineItem> OLIList){
    Map<Id, Id> sourceTargetIdMap = new Map<Id, Id>();  
    List<OpportunityLineItemTier__c> tiers = new List<OpportunityLineItemTier__c>();

    for(OpportunityLineItem thisOLI : OLIList){
      sourceTargetIdMap.put(thisOLI.ContractLineItem__c, thisOLI.Id);
    }

    for(ContractLineItemTier__c cTier : [SELECT Id, Tier__c, ContractLineItem__c,
                                                  EndingUnit__c, ListPrice__c, 
                                                  PricingType__c, StartingUnit__c
                                                  FROM ContractLineItemTier__c 
                                                  WHERE ContractLineItem__c IN: sourceTargetIdMap.keySet()]){

      OpportunityLineItemTier__c oTier = new OpportunityLineItemTier__c();
      oTier.OpportunityProduct__c = sourceTargetIdMap.get(cTier.ContractLineItem__c);
      oTier.Tier__c = cTier.Tier__c;
      oTier.EndingUnit__c = cTier.EndingUnit__c;
      oTier.StartingUnit__c = cTier.StartingUnit__c;
      oTier.ListPrice__c = cTier.ListPrice__c;
      oTier.PricingType__c = cTier.PricingType__c;
      tiers.add(oTier);
    }

    SObjectAccessDecision securityDecision = Security.stripInaccessible( AccessType.CREATABLE, tiers );
    insert securityDecision.getRecords();
  }

  public class InitialWrapper {
    @AuraEnabled
    public List<Sobject> tierList;
    @AuraEnabled
    public Sobject thissObject;
  }
  
}