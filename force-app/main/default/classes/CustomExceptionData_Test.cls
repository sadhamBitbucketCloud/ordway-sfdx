/**
 * @File Name          : CustomExceptionData_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    02/05/2020   Ordway Labs     Initial Version
 **/
@istest
private class CustomExceptionData_Test {
  @istest
  static void primary() {
    Test.startTest();
    CustomExceptionData e = new CustomExceptionData(
      'Error',
      'This is message',
      500
    );
    System.assertequals('Error', e.name, 'Should return Error');
    System.assertequals(
      'This is message',
      e.message,
      'should return This is message'
    );
    System.assertequals(500, e.code, 'should return 500');

    CustomExceptionData thisException = new CustomExceptionData(
      'Error',
      'This is message'
    );
    System.assertequals('Error', thisException.name, 'Should return Error');
    System.assertequals(
      'This is message',
      thisException.message,
      'should return This is message'
    );
    Test.stopTest();
  }
}