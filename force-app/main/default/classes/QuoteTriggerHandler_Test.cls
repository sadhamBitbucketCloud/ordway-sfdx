/**
 * @File Name          : QuoteTriggerHandler_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 10-24-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/5/2020        Initial Version
 **/
@isTest
private class QuoteTriggerHandler_Test {
  @isTest
  private static void populateOpportunityValuesToQuoteTest() {
    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;

    //Insert Opportunity
    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.ServiceStartDate__c = system.today();
    newOpportunity.AutoRenew__c = false;
    insert newOpportunity;

    //Insert Quote
    Quote__c newQuote = TestObjectFactory.createQuote();
    newQuote.Account__c = newAccount.Id;
    newQuote.Opportunity__c = newOpportunity.Id;
    insert newQuote;
    Quote__c thisQuote = [
      SELECT Id, ServiceStartDate__c, AutoRenew__c, Opportunity__c, Account__c
      FROM Quote__c
    ];
    system.assertNotEquals(
      null,
      thisQuote.ServiceStartDate__c,
      'It should have ServiceStartDate Populated'
    );
    system.assertNotEquals(
      null,
      thisQuote.AutoRenew__c,
      'It should have AutoRenew Populated'
    );
    system.assertNotEquals(
      null,
      thisQuote.Opportunity__c,
      'It should have Opportunity Populated'
    );
    system.assertNotEquals(
      null,
      thisQuote.Account__c,
      'It should have Account Populated'
    );
  }

  @isTest
  private static void populateOpportunityValuesToQuoteTest_Bulk() {
    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;

    Test.startTest();
    List<Opportunity> opportunityList = new List<Opportunity>();
    for (Integer i = 0; i < 110; i++) {
      Opportunity newOpportunity = TestObjectFactory.createOpportunity();
      newOpportunity.ServiceStartDate__c = system.today();
      newOpportunity.AutoRenew__c = false;
      opportunityList.add(newOpportunity);
    }
    insert opportunityList;

    List<Quote__c> quoteList = new List<Quote__c>();
    for (Integer i = 0; i < opportunityList.size(); i++) {
      Quote__c newQuote = TestObjectFactory.createQuote();
      newQuote.Account__c = newAccount.Id;
      newQuote.Opportunity__c = opportunityList[i].Id;
      quoteList.add(newQuote);
    }
    insert quoteList;
    Test.stopTest();
    system.assertEquals(110, [SELECT Id FROM Quote__c].size(), 'should be 110');
  }

  @isTest
  private static void hasQuoteStatusChangedToCompletedTest() {
    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;

    //Insert Opportunity
    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.ServiceStartDate__c = system.today();
    newOpportunity.AutoRenew__c = false;
    insert newOpportunity;

    //Insert Product
    Product2 newProduct = TestObjectFactory.createProduct();
    insert newProduct;

    //Insert Pricebook
    Pricebook2 newPricebook = TestObjectFactory.createPricebook();
    insert newPricebook;

    //Insert Standard PricebookEntry
    PricebookEntry newPBEStandard = new PricebookEntry();
    newPBEStandard.Product2Id = newProduct.Id;
    newPBEStandard.Pricebook2Id = Test.getStandardPricebookId();
    PricebookEntry newPBEStd = TestObjectFactory.createPriceBookEntry(
      newPBEStandard
    );
    insert newPBEStd;

    //Insert PricebookEntry
    PricebookEntry newPBEntry = new PricebookEntry();
    newPBEntry.Product2Id = newProduct.Id;
    newPBEntry.Pricebook2Id = newPricebook.Id;

    PricebookEntry newPBE = TestObjectFactory.createPriceBookEntry(newPBEntry);
    insert newPBE;

    Test.startTest();
    String oliListToInsert =
      '[{' +
      '  "IsDeleted": false,' +
      '  "Discount": "25",' +
      '  "Description": "",' +
      '  "OrdwayLabs__PricingModel__c": "Volume",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "Quantity": "75",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00006",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "[ {\n  \\"tier\\" : 1,\n  \\"starting_unit\\" : 0,\n  \\"ending_unit\\" : 20.0,\n  \\"price\\" : \\"5.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 2,\n  \\"starting_unit\\" : 21.0,\n  \\"ending_unit\\" : 50.0,\n  \\"price\\" : \\"2.5\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 3,\n  \\"starting_unit\\" : 51.0,\n  \\"ending_unit\\" : \\"\\",\n  \\"price\\" : \\"350.0\\",\n  \\"type\\" : \\"Flat fee\\"\n} ]",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 0,' +
      '  "OpportunityId": "testOpportunityId",' +
      '  "PricebookEntryId": "testPricebookEntryId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00006",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Monthly"' +
      '}, {' +
      '  "IsDeleted": false,' +
      '  "Discount": "10",' +
      '  "Description": "",' +
      '  "OrdwayLabs__PricingModel__c": "Tiered",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "Quantity": "25",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00005",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "[ {\n  \\"tier\\" : 1,\n  \\"starting_unit\\" : 0,\n  \\"ending_unit\\" : 10.0,\n  \\"price\\" : \\"5.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 2,\n  \\"starting_unit\\" : 10.0,\n  \\"ending_unit\\" : 20.0,\n  \\"price\\" : \\"3.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 3,\n  \\"starting_unit\\" : 20.0,\n  \\"ending_unit\\" : \\"\\",\n  \\"price\\" : \\"50.0\\",\n  \\"type\\" : \\"Flat fee\\"\n} ]",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 0,' +
      '  "OpportunityId": "testOpportunityId",' +
      '  "PricebookEntryId": "testPricebookEntryId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00005",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Monthly"' +
      '}, {' +
      '  "IsDeleted": false,' +
      '  "Discount": "5",' +
      '  "Description": "",' +
      '  "OrdwayLabs__PricingModel__c": "Per unit",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "Quantity": "10",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00004",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 500,' +
      '  "OpportunityId": "testOpportunityId",' +
      '  "PricebookEntryId": "testPricebookEntryId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00004",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Annually"' +
      '}]';

    oliListToInsert = oliListToInsert.replaceAll('\n', '').replaceAll('\r', '');
    oliListToInsert = oliListToInsert.replace(
      'testOpportunityId',
      String.valueOf(newOpportunity.Id)
    );
    oliListToInsert = oliListToInsert.replace(
      'testPricebookEntryId',
      String.valueOf(newPBE.Id)
    );
    AddEditPlanOrdwayController.addPlanToOpportunity(oliListToInsert, '[]');

    //Insert Quote
    Quote__c newQuote = TestObjectFactory.createQuote();
    newQuote.Account__c = newAccount.Id;
    newQuote.Opportunity__c = newOpportunity.Id;
    insert newQuote;
    Quote__c thisQuote = [
      SELECT Id, AutoRenew__c, ServiceStartDate__c, Status__c
      FROM Quote__c
    ];
    thisQuote.Status__c = 'Completed';
    Test.stopTest();
    try {
      update thisQuote;
      system.assertEquals(
        3,
        [SELECT Id FROM QuoteLineItem__c].size(),
        'Should be same as opportunity line item'
      );
      system.assertNotEquals(
        null,
        thisQuote.AutoRenew__c,
        'should be same as opportunity'
      );
      system.assertNotEquals(
        null,
        thisQuote.ServiceStartDate__c,
        'should be same as opportunity'
      );
    } catch (Exception e) {
    }
  }

  @isTest
  private static void hasPricebookChangedTest() {
    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;

    //Insert Opportunity
    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.ServiceStartDate__c = system.today();
    newOpportunity.AutoRenew__c = false;
    insert newOpportunity;

    //Insert Product
    Product2 newProduct = TestObjectFactory.createProduct();
    insert newProduct;

    //Insert Pricebook
    Pricebook2 newPricebook = TestObjectFactory.createPricebook();
    insert newPricebook;

    //Insert Standard PricebookEntry
    PricebookEntry newPBEStandard = new PricebookEntry();
    newPBEStandard.Product2Id = newProduct.Id;
    newPBEStandard.Pricebook2Id = Test.getStandardPricebookId();
    PricebookEntry newPBEStd = TestObjectFactory.createPriceBookEntry(
      newPBEStandard
    );
    insert newPBEStd;

    //Insert PricebookEntry
    PricebookEntry newPBEntry = new PricebookEntry();
    newPBEntry.Product2Id = newProduct.Id;
    newPBEntry.Pricebook2Id = newPricebook.Id;

    PricebookEntry newPBE = TestObjectFactory.createPriceBookEntry(newPBEntry);
    insert newPBE;

    Test.startTest();
    String oliListToInsert =
      '[{' +
      '  "IsDeleted": false,' +
      '  "Discount": "25",' +
      '  "Description": "",' +
      '  "OrdwayLabs__PricingModel__c": "Volume",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "Quantity": "75",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00006",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "[ {\n  \\"tier\\" : 1,\n  \\"starting_unit\\" : 0,\n  \\"ending_unit\\" : 20.0,\n  \\"price\\" : \\"5.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 2,\n  \\"starting_unit\\" : 21.0,\n  \\"ending_unit\\" : 50.0,\n  \\"price\\" : \\"2.5\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 3,\n  \\"starting_unit\\" : 51.0,\n  \\"ending_unit\\" : \\"\\",\n  \\"price\\" : \\"350.0\\",\n  \\"type\\" : \\"Flat fee\\"\n} ]",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 0,' +
      '  "OpportunityId": "testOpportunityId",' +
      '  "PricebookEntryId": "testPricebookEntryId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00006",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Monthly"' +
      '}, {' +
      '  "IsDeleted": false,' +
      '  "Discount": "10",' +
      '  "Description": "",' +
      '  "OrdwayLabs__PricingModel__c": "Tiered",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "Quantity": "25",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00005",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "[ {\n  \\"tier\\" : 1,\n  \\"starting_unit\\" : 0,\n  \\"ending_unit\\" : 10.0,\n  \\"price\\" : \\"5.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 2,\n  \\"starting_unit\\" : 10.0,\n  \\"ending_unit\\" : 20.0,\n  \\"price\\" : \\"3.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 3,\n  \\"starting_unit\\" : 20.0,\n  \\"ending_unit\\" : \\"\\",\n  \\"price\\" : \\"50.0\\",\n  \\"type\\" : \\"Flat fee\\"\n} ]",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 0,' +
      '  "OpportunityId": "testOpportunityId",' +
      '  "PricebookEntryId": "testPricebookEntryId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00005",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Monthly"' +
      '}, {' +
      '  "IsDeleted": false,' +
      '  "Discount": "5",' +
      '  "Description": "",' +
      '  "OrdwayLabs__PricingModel__c": "Per unit",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "Quantity": "10",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00004",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 500,' +
      '  "OpportunityId": "testOpportunityId",' +
      '  "PricebookEntryId": "testPricebookEntryId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00004",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Annually"' +
      '}]';

    oliListToInsert = oliListToInsert.replaceAll('\n', '').replaceAll('\r', '');
    oliListToInsert = oliListToInsert.replace(
      'testOpportunityId',
      String.valueOf(newOpportunity.Id)
    );
    oliListToInsert = oliListToInsert.replace(
      'testPricebookEntryId',
      String.valueOf(newPBE.Id)
    );
    AddEditPlanOrdwayController.addPlanToOpportunity(oliListToInsert, '[]');

    //Insert Quote
    Quote__c newQuote = TestObjectFactory.createQuote();
    newQuote.Account__c = newAccount.Id;
    newQuote.Opportunity__c = newOpportunity.Id;
    insert newQuote;

    Test.stopTest();
    List<QuoteLineItem__c> QLIListToAssert = new List<QuoteLineItem__c>(
      [SELECT Id FROM QuoteLineItem__c]
    );
    if (QLIListToAssert.size() > 0) {
      system.assertEquals(
        3,
        QLIListToAssert.size(),
        'Should be same as opportunity line item'
      );
    }
    Quote__c thisQuote = [SELECT Id, PriceBook2Id__c FROM Quote__c];
    thisQuote.PriceBook2Id__c = null;
    update thisQuote;
    system.assertEquals(
      0,
      [SELECT Id FROM QuoteLineItem__c].size(),
      'should return 0 as it should delete all quote line item'
    );
  }

  @isTest
  private static void populateOpportunityLineItemIdInQLI() {
    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    newSetting.LinkSubscriptionAutomatically__c = true;
    newSetting.EnableQuotes__c = true;
    insert newSetting;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;

    //Insert Opportunity
    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.ServiceStartDate__c = system.today();
    newOpportunity.SyncedQuoteId__c = null;
    newOpportunity.AutoRenew__c = false;
    insert newOpportunity;

    //Insert Product
    Product2 newProduct = TestObjectFactory.createProduct();
    insert newProduct;

    //Insert Pricebook
    Pricebook2 newPricebook = TestObjectFactory.createPricebook();
    insert newPricebook;

    //Insert Standard PricebookEntry
    PricebookEntry newPBEStandard = new PricebookEntry();
    newPBEStandard.Product2Id = newProduct.Id;
    newPBEStandard.Pricebook2Id = Test.getStandardPricebookId();
    PricebookEntry newPBEStd = TestObjectFactory.createPriceBookEntry(
      newPBEStandard
    );
    insert newPBEStd;

    //Insert PricebookEntry
    PricebookEntry newPBEntry = new PricebookEntry();
    newPBEntry.Product2Id = newProduct.Id;
    newPBEntry.Pricebook2Id = newPricebook.Id;

    PricebookEntry newPBE = TestObjectFactory.createPriceBookEntry(newPBEntry);
    insert newPBE;

    //Insert Quote
    Quote__c newQuote = TestObjectFactory.createQuote();
    newQuote.Account__c = newAccount.Id;
    newQuote.Opportunity__c = newOpportunity.Id;
    insert newQuote;

    Test.startTest();
    String qliListToInsert =
      '[{' +
      '  "IsDeleted": false,' +
      '  "OrdwayLabs__PricingModel__c": "Volume",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "OrdwayLabs__Quantity__c": "75",' +
      '  "OrdwayLabs__Discount__c": "10",' +
      '  "OrdwayLabs__PriceBookEntryId__c": "pricebookEntryId",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00006",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "[ {\n  \\"tier\\" : 1,\n  \\"starting_unit\\" : 0,\n  \\"ending_unit\\" : 20.0,\n  \\"price\\" : \\"5.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 2,\n  \\"starting_unit\\" : 21.0,\n  \\"ending_unit\\" : 50.0,\n  \\"price\\" : \\"2.5\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 3,\n  \\"starting_unit\\" : 51.0,\n  \\"ending_unit\\" : \\"\\",\n  \\"price\\" : \\"350.0\\",\n  \\"type\\" : \\"Flat fee\\"\n} ]",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 0,' +
      '  "OrdwayLabs__Quote__c": "testQuoteId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00006",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Monthly"' +
      '}, {' +
      '  "IsDeleted": false,' +
      '  "OrdwayLabs__PricingModel__c": "Tiered",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "OrdwayLabs__Quantity__c": "25",' +
      '  "OrdwayLabs__Discount__c": "10",' +
      '  "OrdwayLabs__PriceBookEntryId__c": "pricebookEntryId",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00005",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "[ {\n  \\"tier\\" : 1,\n  \\"starting_unit\\" : 0,\n  \\"ending_unit\\" : 10.0,\n  \\"price\\" : \\"5.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 2,\n  \\"starting_unit\\" : 10.0,\n  \\"ending_unit\\" : 20.0,\n  \\"price\\" : \\"3.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 3,\n  \\"starting_unit\\" : 20.0,\n  \\"ending_unit\\" : \\"\\",\n  \\"price\\" : \\"50.0\\",\n  \\"type\\" : \\"Flat fee\\"\n} ]",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 0,' +
      '  "OrdwayLabs__Quote__c": "testQuoteId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00005",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Monthly"' +
      '}, {' +
      '  "IsDeleted": false,' +
      '  "OrdwayLabs__PricingModel__c": "Per unit",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "OrdwayLabs__Quantity__c": "10",' +
      '  "OrdwayLabs__Discount__c": "10",' +
      '  "OrdwayLabs__PriceBookEntryId__c": "pricebookEntryId",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00004",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 500,' +
      '  "OrdwayLabs__Quote__c": "testQuoteId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00004",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Annually"' +
      '}]';

    qliListToInsert = qliListToInsert.replaceAll('\n', '').replaceAll('\r', '');
    qliListToInsert = qliListToInsert.replace(
      'testQuoteId',
      String.valueOf(newQuote.Id)
    );
    qliListToInsert = qliListToInsert.replace(
      'pricebookEntryId',
      String.valueOf(newPBE.Id)
    );
    Test.stopTest();
    try {
      AddEditPlanQuoteController.addPlanToQuote(qliListToInsert, '[]');
      Quote__c thisQuote = [SELECT Id, Opportunity__c, PriceBook2Id__c FROM Quote__c];
      thisQuote.Status__c = 'Completed';
      update thisQuote;
      Opportunity syncedOpportunity = [
        SELECT Id, Name, SyncedQuoteId__c
        FROM Opportunity
        WHERE Id = :thisQuote.Opportunity__c
      ];
      system.assertEquals(
        newQuote.Id,
        syncedOpportunity.SyncedQuoteId__c,
        'Once Status is Complete synced Quotes should be populated in Opportunity'
      );
    } catch (Exception e) {
    }
  }

  @isTest
  private static void getOrdwayQuoteMap() {
    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;

    //Insert Opportunity
    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.ServiceStartDate__c = system.today();
    newOpportunity.AutoRenew__c = false;
    insert newOpportunity;

    Test.startTest();
    List<Quote__c> quoteList = new List<Quote__c>();
    Set<Id> quoteIds = new Set<Id>();
    for (Integer i = 0; i < 201; i++) {
      Quote__c newQuote = TestObjectFactory.createQuote();
      newQuote.Account__c = newAccount.Id;
      newQuote.Opportunity__c = newOpportunity.Id;
      quoteList.add(newQuote);
    }
    insert quoteList;

    List<Quote__c> insertedQuoteList = [SELECT Id FROM Quote__c];
    Set<Id> insertedQuoteIds = (new Map<Id, Quote__c>(insertedQuoteList))
      .keySet();
    Map<Id, OrdwayLabs__Quote__c> thisOrdwayQuoteMap = OrdwayQuoteService.getOrdwayQuoteMap(
      insertedQuoteIds
    );
    Test.stopTest();
    system.assertEquals(
      201,
      thisOrdwayQuoteMap.size(),
      'should return same as quote'
    );
  }

  @isTest
  private static void auto_aSyncToOrdway_Test() {
    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.Name = 'Master';
    newSetting.LinkSubscriptionAutomatically__c = true;
    insert newSetting;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;
    //Insert Opportunity
    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.ServiceStartDate__c = system.today();
    newOpportunity.AutoRenew__c = false;
    insert newOpportunity;

    //Insert Quote
    Quote__c newQuote = TestObjectFactory.createQuote();
    newQuote.Account__c = newAccount.Id;
    newQuote.Opportunity__c = newOpportunity.Id;
    newQuote.Status__c = OrdwayHelper.ORDWAY_QUOTE_OUTFORACCEPTANCE;
    insert newQuote;

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();
    update newQuote;
    Integer RESPONSECODE = 200;
    String RESPONSEBODY = OrdwayService_Mock.ordWayQuoteString;
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(
        RESPONSECODE,
        RESPONSEBODY.replace('testQuoteId', String.valueOf(newQuote.Id))
      )
    );
    OrdwayQuoteController.aSyncToOrdway(newQuote.Id);
    Test.stopTest();
    Opportunity thisOpp = [
      SELECT Id, ServiceStartDate__c
      FROM Opportunity
      WHERE Id = :newOpportunity.Id
    ];
    System.assertEquals(system.today(), thisOpp.ServiceStartDate__c);
  }

  @isTest
  private static void QuoteTriggerHelper_Callout() {
    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.SyncQuoteAutomatically__c = true;
    insert newSetting;

    //Insert account
    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    //Insert contact
    Contact newContact = TestObjectFactory.createContact();
    newContact.AccountId = newAccount.Id;
    insert newContact;

    //Insert Opportunity
    Opportunity newOpportunity = TestObjectFactory.createOpportunity();
    newOpportunity.ServiceStartDate__c = system.today();
    newOpportunity.AutoRenew__c = false;
    insert newOpportunity;

    //Insert Product
    Product2 newProduct = TestObjectFactory.createProduct();
    insert newProduct;

    //Insert Pricebook
    Pricebook2 newPricebook = TestObjectFactory.createPricebook();
    insert newPricebook;

    //Insert Standard PricebookEntry
    PricebookEntry newPBEStandard = new PricebookEntry();
    newPBEStandard.Product2Id = newProduct.Id;
    newPBEStandard.Pricebook2Id = Test.getStandardPricebookId();
    PricebookEntry newPBEStd = TestObjectFactory.createPriceBookEntry(
      newPBEStandard
    );
    insert newPBEStd;

    //Insert PricebookEntry
    PricebookEntry newPBEntry = new PricebookEntry();
    newPBEntry.Product2Id = newProduct.Id;
    newPBEntry.Pricebook2Id = newPricebook.Id;

    PricebookEntry newPBE = TestObjectFactory.createPriceBookEntry(newPBEntry);
    insert newPBE;

    //Insert Quote
    Quote__c newQuote = TestObjectFactory.createQuote();
    newQuote.Account__c = newAccount.Id;
    newQuote.Opportunity__c = newOpportunity.Id;
    insert newQuote;

    Test.startTest();
    String qliListToInsert =
      '[{' +
      '  "IsDeleted": false,' +
      '  "OrdwayLabs__PricingModel__c": "Volume",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "OrdwayLabs__Quantity__c": "75",' +
      '  "OrdwayLabs__Discount__c": "10",' +
      '  "OrdwayLabs__PriceBookEntryId__c": "pricebookEntryId",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00006",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "[ {\n  \\"tier\\" : 1,\n  \\"starting_unit\\" : 0,\n  \\"ending_unit\\" : 20.0,\n  \\"price\\" : \\"5.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 2,\n  \\"starting_unit\\" : 21.0,\n  \\"ending_unit\\" : 50.0,\n  \\"price\\" : \\"2.5\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 3,\n  \\"starting_unit\\" : 51.0,\n  \\"ending_unit\\" : \\"\\",\n  \\"price\\" : \\"350.0\\",\n  \\"type\\" : \\"Flat fee\\"\n} ]",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 0,' +
      '  "OrdwayLabs__Quote__c": "testQuoteId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00006",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Monthly"' +
      '}, {' +
      '  "IsDeleted": false,' +
      '  "OrdwayLabs__PricingModel__c": "Tiered",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "OrdwayLabs__Quantity__c": "25",' +
      '  "OrdwayLabs__Discount__c": "10",' +
      '  "OrdwayLabs__PriceBookEntryId__c": "pricebookEntryId",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00005",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "[ {\n  \\"tier\\" : 1,\n  \\"starting_unit\\" : 0,\n  \\"ending_unit\\" : 10.0,\n  \\"price\\" : \\"5.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 2,\n  \\"starting_unit\\" : 10.0,\n  \\"ending_unit\\" : 20.0,\n  \\"price\\" : \\"3.0\\",\n  \\"type\\" : \\"Per unit\\"\n}, {\n  \\"tier\\" : 3,\n  \\"starting_unit\\" : 20.0,\n  \\"ending_unit\\" : \\"\\",\n  \\"price\\" : \\"50.0\\",\n  \\"type\\" : \\"Flat fee\\"\n} ]",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 0,' +
      '  "OrdwayLabs__Quote__c": "testQuoteId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00005",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Monthly"' +
      '}, {' +
      '  "IsDeleted": false,' +
      '  "OrdwayLabs__PricingModel__c": "Per unit",' +
      '  "OrdwayLabs__UnitofMeasure__c": "N/A",' +
      '  "OrdwayLabs__Quantity__c": "10",' +
      '  "OrdwayLabs__Discount__c": "10",' +
      '  "OrdwayLabs__PriceBookEntryId__c": "pricebookEntryId",' +
      '  "OrdwayLabs__OrdwayProductId__c": "P-00001",' +
      '  "OrdwayLabs__ChargeType__c": "Recurring",' +
      '  "OrdwayLabs__OrdwayChargeId__c": "CHG-00004",' +
      '  "OrdwayLabs__OrdwayEffectivePrice__c": 0,' +
      '  "OrdwayLabs__PricingType__c": "per_unit",' +
      '  "OrdwayLabs__IncludedUnits__c": "N/A",' +
      '  "OrdwayLabs__TierJSON__c": "",' +
      '  "OrdwayLabs__OrdwayListPrice__c": 500,' +
      '  "OrdwayLabs__Quote__c": "testQuoteId",' +
      '  "OrdwayLabs__ChargeName__c": "Implementation Pro",' +
      '  "OrdwayLabs__OrdwayPlanId__c": "PLN-00004",' +
      '  "OrdwayLabs__OrdwayUndiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__OrdwayDiscountedContractValue__c": 0,' +
      '  "OrdwayLabs__BillingPeriod__c": "Annually"' +
      '}]';

    qliListToInsert = qliListToInsert.replaceAll('\n', '').replaceAll('\r', '');
    qliListToInsert = qliListToInsert.replace(
      'testQuoteId',
      String.valueOf(newQuote.Id)
    );
    qliListToInsert = qliListToInsert.replace(
      'pricebookEntryId',
      String.valueOf(newPBE.Id)
    );
    Test.stopTest();
    try {
      AddEditPlanQuoteController.addPlanToQuote(qliListToInsert, '[]');
      newQuote.Status__c = 'Draft';
      newQuote.SyncStatus__c = 'In Sync';
      update newQuote;

      ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
      insert thisApplicationsetting;

      Integer RESPONSECODE = 200;
      String RESPONSEBODY = OrdwayService_Mock.ordWayQuoteString;
      Test.setMock(
        HttpCalloutMock.class,
        new OrdwayService_Mock(
          RESPONSECODE,
          RESPONSEBODY.replace('testQuoteId', String.valueOf(newQuote.Id))
        )
      );

      Quote__c syncedQuote = [
        SELECT Id, Status__c, OrdwayLastSyncDate__c, SyncStatus__c
        FROM Quote__c
        WHERE Id = :newQuote.Id
      ];
      system.assertEquals(
        System.today(),
        syncedQuote.OrdwayLastSyncDate__c.date(),
        'Last Sync Status Should be Today'
      );
    } catch (Exception e) {
    }
  }
}