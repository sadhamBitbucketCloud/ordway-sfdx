/**
 * @File Name          : OpportunityLineItemService_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    02/1/2020   Ordway Labs     Initial Version
 **/

@isTest
private class OpportunityLineItemService_Test {
  @isTest
  private static void testOpportunityLineItemService() {
    OpportunitySettingController.saveOpportunitySetting(
      TestObjectFactory.createContractPlanPickerSetting()
    );
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    //Opportunity_OrdwayContract Object Field Mapping Metadata
    String opportunity_OrdwayContractFieldMapping =
      '[{"fieldAPIName":"Account__c","fieldDataTypeKey":"Lookup(Account)","fieldLabel":"Account","sourceFieldAPIName":"AccountId"},' +
      '{"fieldAPIName":"AutoRenew__c","fieldDataTypeKey":"Checkbox","fieldLabel":"Auto Renew","sourceFieldAPIName":"IsPrivate"},' +
      '{"fieldAPIName":"BillingContact__c","fieldDataTypeKey":"Lookup(Contact)","fieldLabel":"Billing Contact","sourceFieldAPIName":"BillingContact__c"},' +
      '{"fieldAPIName":"BillingStartDate__c","fieldDataTypeKey":"Date","fieldLabel":"Billing Start Date","sourceFieldAPIName":"BillingStartDate__c"},' +
      '{"fieldAPIName":"ContractEffectiveDate__c","fieldDataTypeKey":"Date","fieldLabel":"Contract Effective Date","sourceFieldAPIName":"ContractEffectiveDate__c"},' +
      '{"fieldAPIName":"ContractTerm__c","fieldDataTypeKey":"Picklist","fieldLabel":"Contract Term","sourceFieldAPIName":"ContractTerm__c"},' +
      '{"fieldAPIName":"OrdwaySubscriptionID__c","fieldDataTypeKey":"Text","fieldLabel":"Ordway Subscription ID","sourceFieldAPIName":"OrdwaySubscriptionID__c"},' +
      '{"fieldAPIName":"RenewalTerm__c","fieldDataTypeKey":"Picklist","fieldLabel":"Renewal Term","sourceFieldAPIName":"RenewalTerm__c"},' +
      '{"fieldAPIName":"ServiceStartDate__c","fieldDataTypeKey":"Date","fieldLabel":"Service Start Date","sourceFieldAPIName":"ServiceStartDate__c"}]';

    MetadataService.fieldMappingMetadataMap.put(
      'Opportunity_OrdwayContract',
      TestObjectFactory.createObjectFieldMappingMetadata(
        'Opportunity_OrdwayContract',
        'Opportunity',
        'OrdwayContract__c',
        opportunity_OrdwayContractFieldMapping
      )
    );
    //OpportunityLineItem_ContractLineItem Object Field Mapping Metadata
    String opportunityLineItem_ContractLineItemFieldMapping =
      '[{"fieldAPIName":"BillingPeriod__c","fieldDataTypeKey":"Picklist","fieldLabel":"Billing Period","sourceFieldAPIName":"BillingPeriod__c"},' +
      '{"fieldAPIName":"ChargeType__c","fieldDataTypeKey":"Picklist","fieldLabel":"Charge Type","sourceFieldAPIName":"ChargeType__c"},' +
      '{"fieldAPIName":"Name","fieldDataTypeKey":"Text","fieldLabel":"Contract Line Item Name","sourceFieldAPIName":"Name"},' +
      '{"fieldAPIName":"Discount__c","fieldDataTypeKey":"Percent","fieldLabel":"Discount","sourceFieldAPIName":"Discount"},' +
      '{"fieldAPIName":"OrdwayChargeId__c","fieldDataTypeKey":"Text","fieldLabel":"Ordway Charge Id","sourceFieldAPIName":"OrdwayChargeId__c"},' +
      '{"fieldAPIName":"OrdwayDiscountedContractValue__c","fieldDataTypeKey":"Number","fieldLabel":"Ordway Discounted Contract Value","sourceFieldAPIName":"OrdwayDiscountedContractValue__c"},' +
      '{"fieldAPIName":"OrdwayEffectivePrice__c","fieldDataTypeKey":"Number","fieldLabel":"Ordway Effective Price","sourceFieldAPIName":"OrdwayEffectivePrice__c"},' +
      '{"fieldAPIName":"OrdwayListPrice__c","fieldDataTypeKey":"Number","fieldLabel":"Ordway List Price","sourceFieldAPIName":"OrdwayListPrice__c"},' +
      '{"fieldAPIName":"OrdwayPlanId__c","fieldDataTypeKey":"Text","fieldLabel":"Ordway Plan Id","sourceFieldAPIName":"OrdwayPlanId__c"},' +
      '{"fieldAPIName":"OrdwayProductId__c","fieldDataTypeKey":"Text","fieldLabel":"Ordway Product Id","sourceFieldAPIName":"OrdwayProductId__c"},' +
      '{"fieldAPIName":"OrdwayUndiscountedContractValue__c","fieldDataTypeKey":"Number","fieldLabel":"Ordway Undiscounted Contract Value","sourceFieldAPIName":"OrdwayUndiscountedContractValue__c"},' +
      '{"fieldAPIName":"Quantity__c","fieldDataTypeKey":"Number","fieldLabel":"Quantity","sourceFieldAPIName":"Quantity"}]';
    MetadataService.fieldMappingMetadataMap.put(
      'OpportunityLineItem_ContractLineItem',
      TestObjectFactory.createObjectFieldMappingMetadata(
        'OpportunityLineItem_ContractLineItem',
        'OpportunityLineItem',
        'ContractLineItem__c',
        opportunityLineItem_ContractLineItemFieldMapping
      )
    );

    Contact ordwayContact = TestObjectFactory.createContact();
    insert ordwayContact;

    Opportunity ordwayOpportunity = TestObjectFactory.createOpportunity();
    ordwayOpportunity.BillingContact__c = ordwayContact.Id;
    ordwayOpportunity.OrdwayOpportunityType__c = 'New Business';
    insert ordwayOpportunity;

    OpportunityLineItem ordwayOpportunityLineItem = new OpportunityLineItem();
    ordwayOpportunityLineItem.OpportunityId = ordwayOpportunity.Id;
    ordwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem(
      ordwayOpportunityLineItem
    );
    insert ordwayOpportunityLineItem;

    OrdwayContract__c OrdwayContract = new OrdwayContract__c();
    OrdwayContract.BillingContact__c = ordwayContact.Id;
    OrdwayContract = TestObjectFactory.createOrdwayContract(OrdwayContract);
    insert OrdwayContract;

    ContractLineItem__c ordwayContractLineItem = new ContractLineItem__c();
    ordwayContractLineItem.OrdwayLabs__OrdwayContract__c = OrdwayContract.Id;
    ordwayContractLineItem = TestObjectFactory.createContractLineItem(
      ordwayContractLineItem
    );
    insert ordwayContractLineItem;

    Test.startTest();
    Map<Id, OpportunityLineItem> thisOpportunityLineItemMap = OpportunityLineItemService.getOpportunityLineItemMap(
      new Set<Id>{ ordwayOpportunityLineItem.Id }
    );
    System.assertEquals(
      ordwayOpportunityLineItem.Id,
      thisOpportunityLineItemMap.get(ordwayOpportunityLineItem.Id).Id,
      'Should return same Id in Map'
    );
    System.assertEquals(
      false,
      OpportunityLineItemService.isOrdwayInformationChanged(
        ordwayOpportunityLineItem,
        ordwayOpportunityLineItem
      )
    );

    OpportunityLineItemService.getSynchronizableOpportunityLineItemMap(
      new Set<Id>{ ordwayOpportunityLineItem.Id }
    );

    OpportunityLineItemService.createOrdwayContractLineItem(
      new Set<Id>{ ordwayOpportunityLineItem.Id }
    );
    List<ContractLineItem__c> contractLineItemList = [
      SELECT Id
      FROM ContractLineItem__c
      WHERE OrdwayContract__c = :OrdwayContract.Id
    ];
    System.assertequals(1, contractLineItemList.size(), 'Should return 1');

    OpportunityLineItemService.syncOrdwayContractLineItem(
      new Set<Id>{ ordwayOpportunityLineItem.Id },
      new Set<Id>{ OrdwayContract.Id }
    );
    update ordwayOpportunity;
    delete ordwayOpportunityLineItem;
    Test.stopTest();
  }

  @isTest
  private static void getContractLineIdFromOLI() {
    OpportunitySettingController.saveOpportunitySetting(
      TestObjectFactory.createContractPlanPickerSetting()
    );
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Contact ordwayContact = TestObjectFactory.createContact();
    insert ordwayContact;

    Opportunity ordwayOpportunity = TestObjectFactory.createOpportunity();
    ordwayOpportunity.BillingContact__c = ordwayContact.Id;
    insert ordwayOpportunity;

    Set<Id> oppIds = new Set<Id>();
    oppIds.add(ordwayOpportunity.Id);

    OpportunityLineItem ordwayOpportunityLineItem = new OpportunityLineItem();
    ordwayOpportunityLineItem.OpportunityId = ordwayOpportunity.Id;
    ordwayOpportunityLineItem = TestObjectFactory.createOpportunityLineItem(
      ordwayOpportunityLineItem
    );
    insert ordwayOpportunityLineItem;
    List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
    oliList.add(ordwayOpportunityLineItem);

    OrdwayContract__c OrdwayContract = new OrdwayContract__c();
    OrdwayContract.BillingContact__c = ordwayContact.Id;
    OrdwayContract = TestObjectFactory.createOrdwayContract(OrdwayContract);
    insert OrdwayContract;

    ContractLineItem__c ordwayContractLineItem = new ContractLineItem__c();
    ordwayContractLineItem.OrdwayLabs__OrdwayContract__c = OrdwayContract.Id;
    ordwayContractLineItem = TestObjectFactory.createContractLineItem(
      ordwayContractLineItem
    );
    insert ordwayContractLineItem;

    Test.startTest();

    Set<Id> contractLineIds = OpportunityLineItemService.getContractLineIdFromOLI(
      oppIds
    );
    OpportunityLineItemService.updateOLI(oliList);
    system.assertEquals(0, contractLineIds.size(), 'should return 0');
    Test.stopTest();
  }
}