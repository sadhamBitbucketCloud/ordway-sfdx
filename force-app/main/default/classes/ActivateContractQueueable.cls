/**
 * @description       :
 * @author            :
 * @group             :
 * @last modified on  : 09-22-2020
 * @last modified by  : 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-18-2020      Initial Version
**/
public with sharing class ActivateContractQueueable implements Queueable, Database.AllowsCallouts{
  Id contractId;
  public ActivateContractQueueable(Id contractId) {
    this.contractId = contractId;
  }

  public void execute(QueueableContext context) {
    List<OrdwayContract__c> thisContractList = OrdwayContractService.getOrdwayContract(new Set<Id>{contractId}).values();

    if(!thisContractList.isEmpty() && thisContractList[0].OrdwayContractStatus__c == OrdwayHelper.DRAFT
    && thisContractList[0].OrdwaySubscriptionId__c != null ) {
      OrdwayContractController.activateSubscription(thisContractList[0]);
      OrdwayContractPlatformEventHelper.publishActivateContractEvent(
        new List<OrdwayContract__c>{ thisContractList[0] }
      );
    }
  }
}