/**
 * @File Name          : OrdwayObjectRestService_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 11-08-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/25/2020        Initial Version
 **/
@isTest
private class OrdwayObjectRestService_Test {
  @isTest
  private static void doPutInvoice() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Test.startTest();

    String REQUEST_BODY =
      '{"result" : [{' +
      ' "OrdwayLabs__ExternalID__c": "INV-00025",' +
      ' "OrdwayLabs__CustomerID__c": "C-00016", ' +
      ' "attributes": { ' +
      ' "type": "OrdwayLabs__Invoice__c" ' +
      ' }, ' +
      ' "line_items": [' +
      ' {' +
      '  "Name": "Bronze Package",' +
      ' "OrdwayLabs__InvoiceID_LineID__c": "1",' +
      ' "attributes": {' +
      '    "type": "OrdwayLabs__InvoiceLineItem__c"' +
      ' }' +
      '}' +
      ']' +
      '}], "source" : "ordway"}';

    RestRequest thisRequest = new RestRequest();
    RestResponse thisResponse = new RestResponse();
    thisRequest.requestURI = '/Invoice__c';
    thisRequest.httpMethod = 'PUT';
    thisRequest.requestBody = Blob.valueOf(REQUEST_BODY);
    RestContext.request = thisRequest;
    RestContext.response = thisResponse;
    OrdwayObjectRestService.doPut();
    Invoice__c thisInv = [
      SELECT Id, ExternalID__c, CustomerID__c
      FROM Invoice__c
    ];
    system.assertEquals(
      thisInv.ExternalID__c,
      'INV-00025',
      'should be same as request body'
    );
    system.assertEquals(
      thisInv.CustomerID__c,
      'C-00016',
      'should be same as request body'
    );
    InvoiceLineItem__c thisInvLineItem = [
      SELECT Id, Name, InvoiceID_LineID__c, Invoice__c
      FROM InvoiceLineItem__c
    ];
    system.assertEquals(
      thisInvLineItem.Name,
      'Bronze Package',
      'should return Bronze Package'
    );
    system.assertEquals(
      thisInvLineItem.InvoiceID_LineID__c,
      'INV-00025:1',
      'should be same as request body'
    );
    system.assertEquals(
      thisInvLineItem.Invoice__c,
      thisInv.Id,
      'should be same as Invoice'
    );
    Test.stopTest();
  }

  @isTest
  private static void doPutInvoiceChange() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Invoice__c newInvoice = TestObjectFactory.createOrdwayInvoice();
    newInvoice.ExternalID__c = 'INV-00025';
    insert newInvoice;

    InvoiceLineItem__c newInvoiceLineItem = TestObjectFactory.createOrdwayInvoiceLineItem();
    newInvoiceLineItem.Invoice__c = newInvoice.Id;
    newInvoiceLineItem.Name = 'Bronze Package';
    newInvoiceLineItem.InvoiceID_LineID__c = 'INV-00025:2';
    insert newInvoiceLineItem;

    Test.startTest();

    String REQUEST_BODY =
      '{"result" : [{' +
      ' "OrdwayLabs__ExternalID__c": "INV-00025",' +
      ' "OrdwayLabs__CustomerID__c": "C-00016", ' +
      ' "attributes": { ' +
      ' "type": "OrdwayLabs__Invoice__c" ' +
      ' }, ' +
      ' "line_items": [' +
      ' {' +
      '  "Name": "Silver Package",' +
      ' "OrdwayLabs__InvoiceID_LineID__c": "2",' +
      ' "attributes": {' +
      '    "type": "OrdwayLabs__InvoiceLineItem__c"' +
      ' }' +
      '}' +
      ']' +
      '}], "source" : "ordway"}';

    RestRequest thisRequest = new RestRequest();
    RestResponse thisResponse = new RestResponse();
    thisRequest.requestURI = '/Invoice__c';
    thisRequest.httpMethod = 'PUT';
    thisRequest.requestBody = Blob.valueOf(REQUEST_BODY);
    RestContext.request = thisRequest;
    RestContext.response = thisResponse;
    OrdwayObjectRestService.doPut();
    InvoiceLineItem__c thisInvLineItem = [
      SELECT Id, Name, InvoiceID_LineID__c, Invoice__c
      FROM InvoiceLineItem__c
    ];
    system.assertEquals(
      thisInvLineItem.Name,
      'Silver Package',
      'should return Silver Package as we updated the name in request'
    );
    Test.stopTest();
  }
  @isTest
  private static void doPutCustomer() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.EntityID__c = 'SOHONA';
    insert thisEntity;

    OrdwayEntityRule__c thisRule = TestObjectFactory.createOrdwayEntityRule();
    thisRule.EntityID__c = 'SOHONA';
    thisRule.Object__c = 'Account'; 
    thisRule.Field__c = 'Name';
    thisRule.Value__c = 'SOHONA';
    insert thisRule;

    Account newAccount = TestObjectFactory.createAccount();
    thisRule.EntityID__c = null;
    insert newAccount;

    Test.startTest();
    
    String REQUEST_BODY =+' { '
    +' "result": [ '
        +' { '
            +' "Name": "Test DEMO", '
            +' "Website": null, '
            +' "OrdwayLabs__OrdwayCustomerId__c": "C-00194", '
            +' "ParentId": null, '
            +' "Id":"TestAccountid",'
            +' "Description": null, '
            +' "BillingCity": "Due on Receipt", '
            +' "BillingStreet": "1st Day Of The Month", '
            +' "attributes": { '
                +' "type": "Account" '
            +' } '
        +' } '
    +' ], '
    +' "source": "ordway", '
    +' "X-Entity-Id": "SOHONA" '
+' } ';

    RestRequest thisRequest = new RestRequest();
    RestResponse thisResponse = new RestResponse();
    thisRequest.requestURI = '/Customer__c';
    thisRequest.httpMethod = 'PUT';
    thisRequest.requestBody = Blob.valueOf(REQUEST_BODY.replace('TestAccountid', String.valueOf(newAccount  .Id) ));
    RestContext.request = thisRequest;
    RestContext.response = thisResponse;
    OrdwayObjectRestService.doPut();
    Test.stopTest();
    Account thisAccount = [SELECT Id, OrdwayCustomerId__c, EntityID__c, Name FROM Account];
    system.assertEquals('C-00194', thisAccount.OrdwayCustomerId__c, 'should return same account id on customerId');
  }

  @isTest
  private static void doPutContact() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    Test.startTest();

    String REQUEST_BODY =  +' {"result":[ '
    +' { '
      +' "FirstName":"Test A - 1909 New", '
      +' "LastName":"WorkBench", '
      +' "AccountId":"TestAccountid", '
      +' "OrdwayLabs__ExternalID__c":"C-001234", '
      +' "attributes":{ '
        +' "type":"Contact" '
      +' } '
    +' }], '
      +' "source":"ordway" '
    +' } ';

    RestRequest thisRequest = new RestRequest();
    RestResponse thisResponse = new RestResponse();
    thisRequest.requestURI = '/Contact';
    thisRequest.httpMethod = 'PUT';
    thisRequest.requestBody = Blob.valueOf(REQUEST_BODY.replace('TestAccountid', String.valueOf(newAccount.Id) ));
    RestContext.request = thisRequest;
    RestContext.response = thisResponse;
    OrdwayObjectRestService.doPut();
    Test.stopTest();
    Contact thisContact = [SELECT Id, OrdwayContactID__c FROM Contact];
    system.assertEquals('C-001234', thisContact.OrdwayContactID__c, 'should return C-001234');
  }

  @isTest
  private static void doPutContactChange() {
    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    Account newAccount = TestObjectFactory.createAccount();
    insert newAccount;

    Contact newContact = TestObjectFactory.createcontact();
    newContact.ExternalID__c = 'C-001234';
    insert newContact;
    ContactTriggerHandler.runOnce = true;
    Test.startTest();

    String REQUEST_BODY =  +' {"result":[ '
    +' { '
      +' "FirstName":"Test A - 1909 New", '
      +' "LastName":"WorkBench", '
      +' "AccountId":"TestAccountid", '
      +' "OrdwayLabs__ExternalID__c":"C-001234", '
      +' "attributes":{ '
        +' "type":"Contact" '
      +' } '
    +' }], '
      +' "source":"ordway" '
    +' } ';

    RestRequest thisRequest = new RestRequest();
    RestResponse thisResponse = new RestResponse();
    thisRequest.requestURI = '/Contact';
    thisRequest.httpMethod = 'PUT';
    thisRequest.requestBody = Blob.valueOf(REQUEST_BODY.replace('TestAccountid', String.valueOf(newAccount.Id) ));
    RestContext.request = thisRequest;
    RestContext.response = thisResponse;
    ContactTriggerHandler.runOnce = true;
    OrdwayObjectRestService.doPut();
    Test.stopTest();
    Contact thisContact = [SELECT Id, FirstName, LastName, OrdwayContactID__c, ExternalID__c FROM Contact];
    system.assertEquals('Test A - 1909 New', thisContact.FirstName, 'should return Test A - 1909 New');
    system.assertEquals('WorkBench', thisContact.LastName, 'should return WorkBench');
  }
}