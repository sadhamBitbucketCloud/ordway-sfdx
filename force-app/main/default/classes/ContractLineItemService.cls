/**
 * @File Name          : ContractLineItemService.cls
 * @Description        : Contract Line Item Service Class
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-03-2021
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    10/26/2019   Ordway Labs     Initial Version
 **/
public with sharing class ContractLineItemService {
  /**
   * @description  Method to create ordway Contract line item for Ordway Contract
   * @param opportunityIds Opportunity Ids
   * @param opportunityNewMap Opportunity Trigger New Map
   */
  public static void createOrdwayContractLineitem( Set<Id> opportunityIds, Map<Id, Opportunity> opportunityNewMap ) {

    Map<Id, OpportunityLineItem> opportunityLineItemMap = OpportunityService.getOpportunityLineItemMap( opportunityIds );
    Map<Id, ContractLineItem__c> ordwayContractLineItemMapToInsert = new Map<Id, ContractLineItem__c>();
    Set<Id> OLIIds = new Set<Id>();

    for ( OpportunityLineItem thisOpportunityLineItem : opportunityLineItemMap.values() ) {
      if ( opportunityNewMap.containsKey(thisOpportunityLineItem.OpportunityId) 
            && thisOpportunityLineItem.ContractLineItem__c == null ) {

        ContractLineItem__c thisContractLineItem = (ContractLineItem__c) ContractLineItem__c.sObjectType.newSObject( null, true );

        if ( SObjectHelper.isFieldCreatable( SObjectType.ContractLineItem__c, Schema.ContractLineItem__c.OrdwayContract__c ) ) {
          thisContractLineItem.OrdwayContract__c = opportunityNewMap.get( thisOpportunityLineItem.OpportunityId ) .OrdwayContract__c;
        }

        ordwayContractLineItemMapToInsert.put(
          thisOpportunityLineItem.Id,
          (ContractLineItem__c) SObjectFieldMappingHelper.dynamicSobjectMapping( thisOpportunityLineItem, thisContractLineItem, true )
        );
        OLIIds.add(thisOpportunityLineItem.Id);
      }
    }

    if (!ordwayContractLineItemMapToInsert.isEmpty()) {
      try {
        if (Schema.sObjectType.ContractLineItem__c.isCreateable()) {
          insert ordwayContractLineItemMapToInsert.values();
        }
        //populate ContractLineItem Id
        populateContractLineItemId(
          ordwayContractLineItemMapToInsert,
          opportunityLineItemMap
        );

        if (Schema.sObjectType.OpportunityLineItem.isUpdateable()) {
          update opportunityLineItemMap.values();
        }
        TierController.addCLITiers(opportunityLineItemMap.values());
      } catch (Exception thisException) {
        ApplicationLog.Error(thisException);
      }
    }
  }

  /**
   * @description Method to populate Contract Line Item Id for Opportunitylineitem
   * @param opportunityToContractLineitemMap Opportunitylineitem Id to Contractlineitem Map
   * @param opportunityLineItemMap  Opportunitylineitem Map for which ContractLineItem Id need to be populated
   */
  public static void populateContractLineItemId(
    Map<Id, ContractLineItem__c> opportunityToContractLineitemMap,
    Map<Id, OpportunityLineItem> opportunityLineItemMap
  ) {
    for (
      OpportunityLineItem thisOpportunityLineItem : opportunityLineItemMap.values()
    ) {
      if (
        opportunityToContractLineitemMap.containsKey(thisOpportunityLineItem.Id)
      ) {
        if (
          sObjectHelper.isFieldCreatable(
            SObjectType.OpportunityLineItem,
            Schema.OpportunityLineItem.ContractLineItem__c
          ) ||
          sObjectHelper.isFieldUpdatable(
            SObjectType.OpportunityLineItem,
            Schema.OpportunityLineItem.ContractLineItem__c
          )
        ) {
          thisOpportunityLineItem.ContractLineItem__c = opportunityToContractLineitemMap.get(
              thisOpportunityLineItem.Id
            )
            .Id;
        }
      }
    }
  }

  /**
   * @description Method to get ordway Contract Line Item
   * @param  recordIds Ordway Contract LineItem Id's
   * @return Map of Ordway Contract Line Item
   */
  public static Map<Id, ContractLineItem__c> getOrdwayContractLineItemMapById(
    Set<Id> recordIds
  ) {
    Map<Id, ContractLineItem__c> contractLineItemMap = new Map<Id, ContractLineItem__c>();

    if (Schema.sObjectType.ContractLineItem__c.isAccessible()) {
      contractLineItemMap = new Map<Id, ContractLineItem__c>(
        (List<ContractLineItem__c>) Database.query(
          'SELECT ' +
          String.join(
            SObjectHelper.getAllFields(
              Schema.ContractLineItem__c.getSObjectType()
            ),
            ','
          ) +
          ' FROM ContractLineItem__c WHERE OrdwayContract__c IN: recordIds'
        )
      );
    }
    return contractLineItemMap;
  }

  /**
   * @description  Method to update ordway contract for Opportunity
   * @param opportunityLineItemMap Opportunity Line Item Map
   * @param contractIds Ordway contract Ids
   */
  public static void updateOrdwayContractLineItem( Map<Id, OpportunityLineItem> opportunityLineItemMap, Set<Id> contractIds ) {
    
    Map<Id, ContractLineItem__c> ordwayContractLineitemMap = getOrdwayContractLineItemMapById( contractIds );
    Map<Id, OpportunityLineItem> oliMap = new Map<Id, OpportunityLineItem>();

    for (OpportunityLineItem thisOLI : opportunityLineItemMap.values()) {
      oliMap.put(thisOLI.ContractLineItem__c, thisOLI);
    }

    for ( ContractLineItem__c thisContractLineItem : ordwayContractLineitemMap.values() ) {
      if (oliMap.get(thisContractLineItem.Id) != null) {
        thisContractLineItem = (ContractLineItem__c) SObjectFieldMappingHelper.dynamicSobjectMapping(
          oliMap.get(thisContractLineItem.Id),
          thisContractLineItem,
          true
        );
      }
    }

    try {
      if (Schema.sObjectType.ContractLineItem__c.isUpdateable()) {
        update ordwayContractLineitemMap.values();
      }
      TierController.addCLITiers(opportunityLineItemMap.values());
    } catch (Exception thisException) {
      ApplicationLog.Error(thisException);
    }
  }

  /**
   * @description Method to delete Contract Line Items
   * @param ordwayContractIds Set of Ordway Contract Ids
   * @param CLIIds Set of Ordway Contract Line Item Ids
   **/
  public static void deleteCLIOnSync(
    Set<Id> ordwayContractIds,
    Set<Id> CLIIds
  ) {
    Set<Id> recIdsToDelete = new Set<Id>();
    if (!CLIIds.isEmpty() && !ordwayContractIds.isEmpty()) {
      for (
        Id thisId : getOrdwayContractLineItemMapById(ordwayContractIds).keyset()
      ) {
        if (!CLIIds.contains(thisId)) {
          recIdsToDelete.add(thisId);
        }
      }

      if (!recIdsToDelete.isEmpty()) {
        deleteOrdwayContractLineItem(recIdsToDelete);
      }
    }
  }

  /**
   * @description Method to delete ordway contractLineitem
   * @param ordwayContractLineItemIds Set of ordway contract Line Item Id's
   */
  public static void deleteOrdwayContractLineItem(
    Set<Id> ordwayContractLineItemIds
  ) {
    List<ContractLineItem__c> ordwayContractItemList = new List<ContractLineItem__c>();

    if (Schema.sObjectType.ContractLineItem__c.isAccessible()) {
      ordwayContractItemList = new List<ContractLineItem__c>(
        [
          SELECT Id
          FROM ContractLineItem__c
          WHERE Id IN :ordwayContractLineItemIds
        ]
      );
    }

    if (ContractLineItem__c.sObjectType.getDescribe().isDeletable()) {
      try {
        delete ordwayContractItemList;
      } catch (Exception thisException) {
        ApplicationLog.Error(thisException);
      }
    }
  }

  // ContractLineItem Id => OrdwayLabs__ContractLineItemTier__c
  public static Map<Id, List<SObject>> getOrdwayContractLineItemTiers(Set<Id> contractLineItemIds){
    Map<Id, List<SObject>> contractLineItemTierMap = new Map<Id, List<OrdwayLabs__ContractLineItemTier__c>>();

    for(OrdwayLabs__ContractLineItemTier__c thisTier :  Database.query('SELECT ' +String.join(SObjectHelper.getAllFields(Schema.OrdwayLabs__ContractLineItemTier__c.getSObjectType()),',') +
                                ' FROM OrdwayLabs__ContractLineItemTier__c WHERE OrdwayLabs__ContractLineItem__c IN:contractLineItemIds ORDER BY Tier__c ASC ')){
      if(!contractLineItemTierMap.containsKey(thisTier.OrdwayLabs__ContractLineItem__c)){
        contractLineItemTierMap.put(thisTier.OrdwayLabs__ContractLineItem__c, new List<OrdwayLabs__ContractLineItemTier__c>());
      }
      contractLineItemTierMap.get(thisTier.OrdwayLabs__ContractLineItem__c).add(thisTier);
    }
    return contractLineItemTierMap;
  }

}