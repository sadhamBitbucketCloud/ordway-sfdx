/**
 * @File Name          : OrdwayPlanController.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-13-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    3/7/2020   Ordway Labs     Initial Version
 **/
public with sharing class OrdwayPlanController {
  /**
   * @description Method to get Ordway Plans from Ordway
   * @param pageNumber Page number for request
   * @param xEntityId Entity Id
   * @return Boolean is plans empty true or false
   **/
  public static Boolean getOrdwayPlan(Integer pageNumber, String xEntityId) {
    Map<String, Object> responseMap = new Map<String, Object>();
    HttpResponse thisResponse = OrdwayService.getOrdwayPlan(
      pageNumber,
      xEntityId
    );
    List<Plan__c> planList = new List<Plan__c>();
    Map<String, Object> planMap = new Map<String, Object>();
    planMap = (Map<String, Object>) JSON.deserializeUntyped(
      thisResponse.getBody()
    );
    

    if (thisResponse.getStatusCode() == 200) {
      for (
        Object thisObject : (List<Object>) JSON.deserializeUntyped(
          JSON.serialize(planMap.get('plans'))
        )
      ) {
        responseMap = (Map<String, Object>) JSON.deserializeUntyped(
          JSON.serialize(thisObject) 
        );
        Plan__c newPlan = new Plan__c();
        newPlan.PlanId__c = String.valueOf(responseMap.get('id'));
        newPlan.Name = String.valueOf(responseMap.get('name'));
        newPlan.Active__c = String.valueOf(responseMap.get('status')) ==
          'Active'
          ? true
          : false;
        newPlan.PlanDetails__c = JSON.serializePretty(thisObject);
        newPlan.EntityID__c = xEntityId;
        OrdwayEntityHelper.populateOrdwayEntityName(newPlan);
        newPlan.OrdwayLastSyncDate__c = System.now();
        planList.add(newPlan);
      }
    }

    //PlanId Unique External Id
    SObjectAccessDecision decision = Security.stripInaccessible(
      AccessType.UPSERTABLE,
      planList
    );
    upsert (List<Plan__c>) decision.getRecords() PlanId__c;

    return planList.isEmpty() ? false : true; //Return 0 if the list is empty, means there are no more records
}

  /**
   * @description Method to get Plans From Ordway Plan Object based on Entity Id
   * @param entityId Entity Id
   * @return List<Plan__c> List of Ordway Plans
   **/
  public static List<Plan__c> getOrdwayPlan(String entityId) {
    List<Plan__c> ordwayPlanList = new List<Plan__c>();

    if (Schema.sObjectType.Plan__c.isAccessible()) {
      ordwayPlanList = new List<Plan__c>(
        [
          SELECT Id, Name, PlanId__c, Active__c, PlanDetails__c
          FROM Plan__c
          WHERE EntityID__c = :entityId
          ORDER BY PlanId__c DESC
        ]
      );
    }
    return ordwayPlanList;
  }

  @AuraEnabled
  public static void syncToOrdway(String recordId){

    try {
      Plan__c thisPlan = [SELECT Id, PlanId__c, EntityId__c FROM Plan__c WHERE Id =: recordId];

      HttpResponse thisResponse = syncPlan(recordId);
      ApplicationLog.StartLoggingContext('OrdwayPlanController', 'Sync to Ordway => Handle Response');

      if ( thisResponse.getStatusCode() >= 200 && thisResponse.getStatusCode() <= 299 ) {
        List<Object> responseList = new List<Object>();
        responseList.add((Object)JSON.deserializeUntyped(thisResponse.getBody()));
        OrdwayObjectSyncHelper.processRecords( 'Plan', OrdwayObjectSyncHelper.getSObjectRecords('Plan', new Map<String, Object>{ 'result' => JSON.deserializeUntyped(JSON.serialize(responseList))}, OrdwayHelper.ACTION.RESPONSE, thisPlan.EntityId__c), thisPlan.EntityId__c);
      } 
      else {
        String errorMessage = OrdwayHelper.getResponseStatusMessage(thisResponse);
        AuraHandledException ex = new AuraHandledException(errorMessage);
        ex.setMessage(errorMessage);
        throw ex;
      }
    } catch (Exception e) {
      throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  public static HttpResponse syncPlan(Id planId) {

    Plan__c thisPlan = [SELECT Id, PlanId__c FROM Plan__c WHERE Id =: planId];

    if( thisPlan.PlanId__c != null) {
      return OrdwayService.updateOrdwayPlan(planId);
    }
    return OrdwayService.createOrdwayPlan(planId);
  }

  public static Plan__c getPlan(Id planId) {
    return [SELECT Id, Name, PlanId__c, Active__c, OrdwayEntity__c, HasCharge__c FROM Plan__c WHERE Id =: planId];
  }

  @AuraEnabled
  public static InitialWrapper getInitialWrapper(Id planId) {
    try {
        InitialWrapper thisInitialWrapper = new InitialWrapper();
        thisInitialWrapper.thisPlan = getPlan(planId);
        thisInitialWrapper.namedCredentials = getNamedCredential();
        thisInitialWrapper.isMultiEntityEnabled = OpportunitySettingController.isMultiEntityEnabled();
        return thisInitialWrapper;
      } catch (Exception e) {
        throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  @AuraEnabled(cacheable=true)
  public static String getNamedCredential() {
    try {
      return OrdwayService.getNamedCredentialURL();
    } catch (Exception e) {
      throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  public class InitialWrapper {
    @AuraEnabled
    public Plan__c thisPlan;
    @AuraEnabled
    public String namedCredentials;
    @AuraEnabled
    public Boolean isMultiEntityEnabled;
  }
}