/**
 * @File Name          : OpportunityService.cls
 * @Description        : Opportuntit Service Class
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    10/12/2019   Ordway Labs     Initial Version
 **/
public with sharing class OpportunityService {
  /**
   * @description Method to set the Default Renewal term for the opportunity record
   * @param thisOpportunity Opportunity record
   **/
  public static void defaultRenewalTerm(Opportunity thisOpportunity) {
    if (thisOpportunity.ContractTerm__c == '-1' || thisOpportunity.ContractTerm__c == 'Evergreen') {
      thisOpportunity.RenewalTerm__c = null;
    }
  }

  /**
   * @description Method to set the Default Pricebook for the opportunity record
   * @param thisOpportunity Opportunity record
   **/
  public static void defaultPricebook(Opportunity thisOpportunity) {
    if (
      thisOpportunity.Pricebook2Id == null &&
      OpportunitySettingController.getOrdwayPriceBookId() != null &&
      !Test.isRunningTest()
    ) {
      thisOpportunity.Pricebook2Id = OpportunitySettingController.getOrdwayPriceBookId();
    }
  }

  /**
   * @description Method to populate Ordway Entity Id and Entity name based on Account and Ordway Entity Configurations
   * @param thisOpportunity Opportunity record
   * @param accountMap Map of Account Records
   **/
  public static void populateOrdwayEntity(
    Opportunity thisOpportunity,
    Map<Id, Account> accountMap
  ) {
    OrdwayEntityHelper.populateOrdwayEntity(thisOpportunity);
    if (
      thisOpportunity.OrdwayLabs__EntityID__c == null &&
      thisOpportunity.OrdwayLabs__EntityName__c == null &&
      thisOpportunity.AccountId != null &&
      accountMap.containskey(thisOpportunity.AccountId) &&
      accountMap.get(thisOpportunity.AccountId).OrdwayLabs__EntityID__c !=
      null &&
      accountMap.get(thisOpportunity.AccountId).OrdwayLabs__EntityName__c !=
      null &&
      OpportunitySettingController.isMultiEntityEnabled()
    ) {
      thisOpportunity.OrdwayLabs__EntityID__c = accountMap.get(
          thisOpportunity.AccountId
        )
        .OrdwayLabs__EntityID__c;
      thisOpportunity.OrdwayLabs__EntityName__c = accountMap.get(
          thisOpportunity.AccountId
        )
        .OrdwayLabs__EntityName__c;
    }
  }

  /**
   * @description Method to Populate Billing address from Billing contact
   * @param thisOpportunity Opportunity record
   * @param contactMap Map of Contact records
   **/
  public static void populateBillingAddress(
    Opportunity thisOpportunity,
    Map<Id, Contact> contactMap
  ) {
    //If Billing Contact is Populated on Insert & Custom Billing Address Values Present - Do Not Overwrite
    //If Billing Contact is Populated on Insert & No Custom Billing Address Values Present - Auto-Populate
    //If Billing Contact is Changed, then update billing address
    //If Billing Address is Changed, then do not overwrite

    if (
      thisOpportunity.BillingContact__c != null &&
      contactMap.get(thisOpportunity.BillingContact__c) != null
    ) {
      Contact billingContact = contactMap.get(
        thisOpportunity.BillingContact__c
      );

      if (
        (Trigger.isInsert &&
        thisOpportunity.BillingStreet__c == null &&
        thisOpportunity.BillingCity__c == null &&
        thisOpportunity.BillingState__c == null &&
        thisOpportunity.BillingPostalCode__c == null &&
        thisOpportunity.BillingCountry__c == null &&
        thisOpportunity.BillingEmail__c == null) || Trigger.isUpdate
      ) {
        thisOpportunity.BillingStreet__c = billingContact.MailingStreet;
        thisOpportunity.BillingCity__c = billingContact.MailingCity;
        thisOpportunity.BillingState__c = billingContact.MailingState;
        thisOpportunity.BillingPostalCode__c = billingContact.MailingPostalCode;
        thisOpportunity.BillingCountry__c = billingContact.MailingCountry;
        thisOpportunity.BillingEmail__c = billingContact.Email;
      }
    } else if (thisOpportunity.BillingContact__c == null) {
      thisOpportunity.BillingStreet__c = null;
      thisOpportunity.BillingCity__c = null;
      thisOpportunity.BillingState__c = null;
      thisOpportunity.BillingPostalCode__c = null;
      thisOpportunity.BillingCountry__c = null;
      thisOpportunity.BillingEmail__c = null;
    }
  }

  /**
   * @description Method to get OpportunitylineItem Map
   * @param opportunityIds Set of opportunity Id's
   * @return Map<Id, OpportunityLineItem> Map of OpportunitylineItem
   **/
  public static Map<Id, OpportunityLineItem> getOpportunityLineItemMap(
    Set<Id> opportunityIds
  ) {
    Map<Id, OpportunityLineItem> thisOpportunityLineItemMap = new Map<Id, OpportunityLineItem>();
    if (Schema.sObjectType.OpportunityLineItem.isAccessible()) {
      thisOpportunityLineItemMap = new Map<Id, OpportunityLineItem>(
        (List<OpportunityLineItem>) Database.query(
          'SELECT ' +
          String.join(
            SObjectHelper.getAllFields(
              Schema.OpportunityLineItem.getSObjectType()
            ),
            ','
          ) +
          ' FROM OpportunityLineItem WHERE OpportunityId IN:opportunityIds'
        )
      );
    }
    return thisOpportunityLineItemMap;
  }

  /**
   * @description Method to get Opportunity Map
   * @param opportunityIds Set of opportunity Id's
   * @return Map<Id, Opportunity> Map of Opportunity
   **/
  public static Map<Id, Opportunity> getOpportunityMap(Set<Id> opportunityIds) {
    Map<Id, Opportunity> thisOpportunityMap = new Map<Id, Opportunity>();
    if (Schema.sObjectType.Opportunity.isAccessible()) {
      thisOpportunityMap = new Map<Id, Opportunity>(
        (List<Opportunity>) Database.query(
          'SELECT ' +
          String.join(
            SObjectHelper.getAllFields(Schema.Opportunity.getSObjectType()),
            ','
          ) +
          ' FROM Opportunity WHERE Id IN:opportunityIds'
        )
      );
    }
    return thisOpportunityMap;
  }
}