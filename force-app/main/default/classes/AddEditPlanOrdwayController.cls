/**
 * @File Name          : AddEditPlanOrdwayController.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 01-03-2021
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    17/10/2019   Ordway Labs     Initial Version
 **/
public with sharing class AddEditPlanOrdwayController {
  /**
   * @description Method to get Opportunity line items for Add Edit Plan Picker
   * @param opportunityId Opportunity Id
   * @param pricebookId Pricebook Id
   * @return Map<String, List<OpportunityLineItem>> Map of Opportuntity Line Items
   **/
  @AuraEnabled
  public static Map<String, List<OpportunityLineItem>> getOrdwayOLI(
    String opportunityId,
    String pricebookId
  ) {
    ApplicationLog.StartLoggingContext(
      'AddEditPlanOrdwayController',
      'getOrdwayOLI()'
    );

    Map<String, List<OpportunityLineItem>> planMap = new Map<String, List<OpportunityLineItem>>();
    Map<String, OpportunityLineItem> oliByPlanChargeIdMap = getOLIByPlanChargeId(
      opportunityId
    );
    Map<String, ContractLineItem__c> cliByPlanChargeIdMap = new Map<String, ContractLineItem__c>();

    ApplicationLog.Info('OLI By Plan Charge Id' + oliByPlanChargeIdMap);

    ContractPlanPickerSetting__c thisSetting = getOpportunityCustomSetting();

    Map<Id, Opportunity> opportunityMap = OpportunityService.getOpportunityMap(
      new Set<Id>{ opportunityId }
    );
    Opportunity thisOpportunity = opportunityMap.get(opportunityId);

    Map<String, Object> fieldsToValue = thisOpportunity.getPopulatedFieldsAsMap();

    Map<String, PricebookEntry> pricebookEntryMap = PriceBookEntryController.getPricebookEntryMap(
      pricebookId,
      String.valueOf(fieldsToValue.get('CurrencyIsoCode'))
    );

    ApplicationLog.Info('Pricebook Entry' + pricebookEntryMap);

    if (
      thisOpportunity.OrdwayContract__c != null &&
      !thisOpportunity.HasOpportunityLineItem
    ) {
      cliByPlanChargeIdMap = getCLIByPlanChargeId(
        thisOpportunity.OrdwayContract__c
      );
    }

    String entityId = OrdwayService.getEntityId(opportunityId);
    for (
      Plan__c thisOrdwayPlan : OrdwayPlanController.getOrdwayPlan(entityId)
    ) {
      String planKey = thisOrdwayPlan.PlanId__c + '_' + thisOrdwayPlan.Name;

      Map<String, Object> responseMap = new Map<String, Object>();

      if(String.isNotBlank(thisOrdwayPlan.PlanDetails__c)) {
        responseMap = (Map<String, Object>) JSON.deserializeUntyped(
          thisOrdwayPlan.PlanDetails__c
        );
      }
      else {
        continue;
      }

      for (String s : responseMap.keySet()) {
        //Each Plan Has More Than One Charge
        if (s == 'charges') {
          //Get Each Charge
          for (
            Object thisStr : (List<Object>) JSON.deserializeUntyped(
              JSON.serialize(responseMap.get(s))
            )
          ) {
            //Get Each Charge In Key Value Pair
            Map<String, Object> chargeObjectMap = new Map<String, Object>();
            chargeObjectMap = (Map<String, Object>) JSON.deserializeUntyped(
              JSON.serialize(thisStr)
            );

            if (
              !OrdwayHelper.isValidId(String.valueOf(chargeObjectMap.get('salesforce_id')))
            ) {
              continue;
            }
            //Only if the Salesforce Id Matches
            if (
              pricebookEntryMap.get(
                (Id) chargeObjectMap.get('salesforce_id')
              ) != null
            ) {
              if (!planMap.containsKey(planKey)) {
                planMap.put(planKey, new List<OpportunityLineItem>{});
              }

              //If this Charge is already present, Add Existing OLI
              if (
                oliByPlanChargeIdMap.containsKey(
                  thisOrdwayPlan.PlanId__c +
                  '_' +
                  String.valueOf(chargeObjectMap.get('id'))
                )
              ) {
                planMap.get(planKey)
                  .add(
                    oliByPlanChargeIdMap.get(
                      thisOrdwayPlan.PlanId__c +
                      '_' +
                      String.valueOf(chargeObjectMap.get('id'))
                    )
                  );
                continue; //Skips to the next iteration of the loop
              }

              //Load Defaults
              OpportunityLineItem thisLineItem = (OpportunityLineItem) OpportunityLineItem.sObjectType.newSObject(
                null,
                true
              );

              thisLineItem.PricebookEntryId = pricebookEntryMap.get(
                  (Id) chargeObjectMap.get('salesforce_id')
                )
                .Id;
              thisLineItem.OrdwayPlanId__c = thisOrdwayPlan.PlanId__c;
              thisLineItem.OpportunityId = opportunityId;
              thisLineItem.UnitPrice = 0;

              AddEditPlanHelper.onLoadDefaultValues(thisLineItem);

              //Construct Opportunity Line Item
              if (
                chargeObjectMap.get('billing_period') != null &&
                chargeObjectMap.get('billing_period') != ''
              ) {
                thisLineItem.BillingPeriod__c = String.valueOf(
                  chargeObjectMap.get('billing_period')
                );
              }

              if (chargeObjectMap.get('type') != null) {
                thisLineItem.ChargeType__c = String.valueOf(
                  chargeObjectMap.get('type')
                );
              }

              if (chargeObjectMap.get('id') != null) {
                thisLineItem.OrdwayChargeId__c = String.valueOf(
                  chargeObjectMap.get('id')
                );
              }

              if (chargeObjectMap.get('product_id') != null) {
                thisLineItem.OrdwayProductId__c = String.valueOf(
                  chargeObjectMap.get('product_id')
                );
              }

              if (chargeObjectMap.get('pricing_model') != null) {
                thisLineItem.PricingModel__c = String.valueOf(
                  chargeObjectMap.get('pricing_model')
                );
              }

              if (chargeObjectMap.get('list_price') != null) {
                thisLineItem.OrdwayListPrice__c = thisSetting.UseOrdwayPrice__c
                  ? Decimal.valueOf(
                      String.valueOf(chargeObjectMap.get('list_price'))
                    )
                  : pricebookEntryMap.get(
                        (Id) chargeObjectMap.get('salesforce_id')
                      )
                      .UnitPrice;
              }

              thisLineItem.UnitPrice = thisLineItem.OrdwayListPrice__c;
              if (chargeObjectMap.get('name') != null) {
                thisLineItem.ChargeName__c = String.valueOf(
                    chargeObjectMap.get('name')
                  )
                  .capitalize();
              }

              if (
                chargeObjectMap.get('unit_of_measure') != null &&
                chargeObjectMap.get('unit_of_measure') != ''
              ) {
                thisLineItem.UnitofMeasure__c = String.valueOf(
                  chargeObjectMap.get('unit_of_measure')
                );
              }

              if (
                chargeObjectMap.get('included_units') != null &&
                chargeObjectMap.get('included_units') != ''
              ) {
                thisLineItem.IncludedUnits__c = String.valueOf(
                  chargeObjectMap.get('included_units')
                );
              }

              if (chargeObjectMap.get('description') != null) {
                thisLineItem.Description = String.valueOf(
                  chargeObjectMap.get('description')
                );
              }

              if (chargeObjectMap.get('tiers') != null) {
                thisLineItem.TierJSON__c = JSON.serializePretty(
                  chargeObjectMap.get('tiers')
                );
              }

              //If Contract Line Item, Map it here
              //Check whether Opportunity has Contract Id Populated
              //Check whether Opportunity has Line Items
              //If Has Line Items, then do nothing
              //If No line items, then construct the line item using contract line item

              //If the plan exists on CLI, then add it to OLI and continue
              if (
                thisOpportunity.OrdwayContract__c != null &&
                !thisOpportunity.HasOpportunityLineItem &&
                cliByPlanChargeIdMap.containsKey(
                  thisOrdwayPlan.PlanId__c +
                  '_' +
                  String.valueOf(
                    chargeObjectMap.get(OrdwayHelper.SOBJECT_FIELD_ID)
                  )
                )
              ) {
                planMap.get(planKey)
                  .add(
                    getOpportunityLineItem(
                      thisLineItem,
                      cliByPlanChargeIdMap.get(
                        thisOrdwayPlan.PlanId__c +
                        '_' +
                        String.valueOf(
                          chargeObjectMap.get(OrdwayHelper.SOBJECT_FIELD_ID)
                        )
                      )
                    )
                  );
                continue;
              }

              //If Plan is not active, then skip
              if (false == thisOrdwayPlan.Active__c) {
                planMap.remove(planKey);
                continue;
              }

              ApplicationLog.Info(' Opportunity Line Item ' + thisLineItem);
              planMap.get(planKey).add(thisLineItem);
            }
          }
        }
      }
    }
    ApplicationLog.Info(' Plan Map ' + JSON.serializePretty(planMap));
    ApplicationLog.FinishLoggingContext();
    return planMap;
  }

  /**
   * @description Add and delete Opportunity line items
   * @param oliList Opportunity Line item records to upsert
   * @param opportunityLineItemToDeleteIds Opportunity Line item records to be deleted
   **/
  @AuraEnabled
  public static void addPlanToOpportunity(
    String oliList,
    String opportunityLineItemToDeleteIds
  ) {
    ApplicationLog.StartLoggingContext(
      'AddEditPlanOrdwayController',
      'addPlanToOpportunity()'
    );

    try {
      //Delete OLI
      AddEditPlanHelper.deleteOLI(
        (List<String>) JSON.deserialize(
          opportunityLineItemToDeleteIds,
          List<String>.class
        )
      );

      List<OpportunityLineItem> opportunityLineItemToInsert = (List<OpportunityLineItem>) JSON.deserialize(
        oliList,
        List<OpportunityLineItem>.class
      );

      Map<String, Object> tierMap = new Map<String, Object>();
      ContractPlanPickerSetting__c thisSetting = getOpportunityCustomSetting();

      //Loop through Opportunity Line Items
      for (OpportunityLineItem thisOLI : opportunityLineItemToInsert) {
        AddEditPlanHelper.populateDefaultValues(thisOLI);
        
        if(thisOLI.Id == null 
          && thisOLI.OrdwayLabs__ContractLineItem__c != null){
          thisOLI.OrdwayLabs__ContractLineItem__c = null; 
          thisOLI.TotalPrice = null;
        }

        if (
          thisOLI.TierJSON__c != null &&
          thisOLI.TierJSON__c != 'null' &&
          thisOLI.TierJSON__c != ''
        ) {
          Decimal listPrice = 0;

          for (
            Object thisTier : (List<Object>) JSON.deserializeUntyped(
              thisOLI.TierJSON__c
            )
          ) {
            tierMap = (Map<String, Object>) JSON.deserializeUntyped(
              JSON.serialize(thisTier)
            );

            //VOLUME
            if (thisOLI.PricingModel__c == OrdwayHelper.VOLUME) {
              AddEditPlanHelper.calculateVolumePricing(thisOLI, tierMap);
              continue;
            }

            //TIERED
            if (thisOLI.PricingModel__c == OrdwayHelper.TIERED) {
              listPrice = AddEditPlanHelper.calculateTieredPricing(
                thisOLI,
                tierMap,
                listPrice
              );
            }
          }
        }
      }
      SObjectAccessDecision securityDecision = Security.stripInaccessible(
        AccessType.CREATABLE,
        opportunityLineItemToInsert
      );
      ApplicationLog.Info(
        'INFO 1 >>>' + JSON.serializePretty(securityDecision.getRemovedFields())
      );
      ApplicationLog.Info(
        'INFO 2 >>>' + JSON.serializePretty(securityDecision.getRecords())
      );
      ApplicationLog.FinishLoggingContext();
      upsert securityDecision.getRecords();
    } catch (Exception e) {
      AuraHandledException ex = new AuraHandledException(e.getMessage());
      ex.setMessage(e.getMessage());
      throw ex;
    }
  }

  /**
   * @description Method to populate Opportunity Line Item values from Contract line item based on Dynamic field mapping configurations
   * @param thisOLI Opportunity Line Item record
   * @param thisCLI Contract Line Item record
   * @return OpportunityLineItem Opportunity line item record with values populated from Contract line item
   **/
  private static OpportunityLineItem getOpportunityLineItem(
    OpportunityLineItem thisOLI,
    ContractLineItem__c thisCLI
  ) {
    thisOLI.ContractLineItem__c = thisCLI.Id;
    //Dynamic Field Mapping
    SObjectFieldMappingHelper.dynamicSobjectMapping(thisCLI, thisOLI, true);
    return thisOLI;
  }

  /**
   * @description Get all existing Contract Line Items and add it to map
   * @param contractId Id of the Contract
   * @return Map of contract line item records
   */
  @TestVisible
  private static Map<String, ContractLineItem__c> getCLIByPlanChargeId(
    Id contractId
  ) {
    Map<String, ContractLineItem__c> cliByPlanChargeIdMap = new Map<String, ContractLineItem__c>();

    for (
      ContractLineItem__c thisCLI : ContractLineItemService.getOrdwayContractLineItemMapById(
          new Set<Id>{ contractId }
        )
        .values()
    ) {
      thisCLI.BillingPeriod__c = thisCLI.BillingPeriod__c != null &&
        thisCLI.BillingPeriod__c != ''
        ? thisCLI.BillingPeriod__c
        : 'N/A';
      thisCLI.UnitofMeasure__c = thisCLI.UnitofMeasure__c != null &&
        thisCLI.UnitofMeasure__c != ''
        ? thisCLI.UnitofMeasure__c
        : 'N/A';
      thisCLI.IncludedUnits__c = thisCLI.IncludedUnits__c != null &&
        thisCLI.IncludedUnits__c != ''
        ? thisCLI.IncludedUnits__c
        : 'N/A';

      if (
        thisCLI.OrdwayPlanId__c != null &&
        thisCLI.OrdwayChargeId__c != null
      ) {
        String key = thisCLI.OrdwayPlanId__c + '_' + thisCLI.OrdwayChargeId__c;
        if (cliByPlanChargeIdMap.get(key) == null) {
          cliByPlanChargeIdMap.put(
            thisCLI.OrdwayPlanId__c +
            '_' +
            thisCLI.OrdwayChargeId__c,
            thisCLI
          );
        }
      }
    }
    return cliByPlanChargeIdMap;
  }

  /**
   * @description Get all existing Opportunity Line Items
   * @param opportunityId Opportunity Id
   * @return Map of Opportunity line item records
   */
  public static Map<String, OpportunityLineItem> getOLIByPlanChargeId(
    Id opportunityId
  ) {
    Map<String, OpportunityLineItem> oliByPlanChargeIdMap = new Map<String, OpportunityLineItem>();

    for (
      OpportunityLineItem thisOLI : OpportunityLineItemService.getOpportunityLineItemMap(
          new Set<Id>{ opportunityId }
        )
        .values()
    ) {
      thisOLI.BillingPeriod__c = thisOLI.BillingPeriod__c != null &&
        thisOLI.BillingPeriod__c != ''
        ? thisOLI.BillingPeriod__c
        : 'N/A';
      thisOLI.UnitofMeasure__c = thisOLI.UnitofMeasure__c != null &&
        thisOLI.UnitofMeasure__c != ''
        ? thisOLI.UnitofMeasure__c
        : 'N/A';
      thisOLI.IncludedUnits__c = thisOLI.IncludedUnits__c != null &&
        thisOLI.IncludedUnits__c != ''
        ? thisOLI.IncludedUnits__c
        : 'N/A';

      if (
        thisOLI.OrdwayPlanId__c != null &&
        thisOLI.OrdwayChargeId__c != null
      ) {
        String key = thisOLI.OrdwayPlanId__c + '_' + thisOLI.OrdwayChargeId__c;
        if (oliByPlanChargeIdMap.get(key) == null) {
          oliByPlanChargeIdMap.put(
            thisOLI.OrdwayPlanId__c +
            '_' +
            thisOLI.OrdwayChargeId__c,
            thisOLI
          );
        }
      }
    }
    return oliByPlanChargeIdMap;
  }

  /**
   * @description Initial Wrapper data for Add Edit Plan picker
   * @param opportunityId Opportunity Id
   * @param pricebookId Pricebook Id
   * @return AddEditPlanHelper.InitialWrapper Initial Wrapper
   **/
  @AuraEnabled
  public static AddEditPlanHelper.InitialWrapper getInitialWrapper(
    String opportunityId,
    String pricebookId
  ) {
    Schema.FieldSet dynamicFieldSet = Schema.SObjectType.OpportunityLineItem.fieldSets.getMap()
      .get(OrdwayHelper.ORDWAY_PLAN_PICKER_DYNAMIC_FIELDSET_APINAME);

    Schema.FieldSet readOnlyFieldSet = Schema.SObjectType.OpportunityLineItem.fieldSets.getMap()
    .get(OrdwayHelper.ORDWAY_PLAN_PICKER_READ_ONLY_FIELDSET_APINAME);

    AddEditPlanHelper.InitialWrapper thisInitialWrapper = new AddEditPlanHelper.InitialWrapper();
    thisInitialWrapper.oliMap = getOrdwayOLI(opportunityId, pricebookId);
    thisInitialWrapper.salesforceBaseURL = getSalesforceOrgBaseURL();
    thisInitialWrapper.thisOpportunity   = getOpportunity(opportunityId);
    thisInitialWrapper.requiredPlanObjectWrapper = AddEditPlanHelper.getDynamicFieldsWrapper(
      dynamicFieldSet
    );
    thisInitialWrapper.currencyISO = AddEditPlanHelper.getCurrencyIsoCode(
      opportunityId
    );

    thisInitialWrapper.readOnlyPlanObjectWrapper = AddEditPlanHelper.getReadOnlyFieldsWrapper(readOnlyFieldSet, thisInitialWrapper.currencyISO);
    return thisInitialWrapper;
  }

  /**
   * @description Method to get Contract Plan picker custom setting
   * @return ContractPlanPickerSetting__c
   **/
  @AuraEnabled
  public static ContractPlanPickerSetting__c getOpportunityCustomSetting() {
    return ContractPlanPickerSetting__c.getValues(
        OrdwayHelper.OPPORTUNITY_SETTING_NAME
      ) == null
      ? new ContractPlanPickerSetting__c()
      : ContractPlanPickerSetting__c.getValues(
          OrdwayHelper.OPPORTUNITY_SETTING_NAME
        );
  }

  /**
   * @description Method to get Salesforce Org Domain URL
   * @return String Salesforce Org Domain URL
   **/
  @AuraEnabled
  public static String getSalesforceOrgBaseURL() {
    return URL.getSalesforceBaseUrl().toExternalForm();
  }

  /**
   * @description Method to get Wrapper Data containing Opportunity Custom Setting, Pricebook information and Opportunity data
   * @param opportunityId Opportunity Id
   * @return DataWrapper Wrapper class
   **/
  @AuraEnabled
  public static DataWrapper getDataWrapper(String opportunityId) {
    DataWrapper thisDataWrapper = new DataWrapper();
    thisDataWrapper.isUseOrdwayPricebook = getOpportunityCustomSetting();
    thisDataWrapper.existingPricebook = OpportunitySettingController.getExistingPricebook();
    thisDataWrapper.currentOpportunity = getOpportunity(opportunityId);
    thisDataWrapper.isNewPlanPickerToShow = LoadLeanSettingController.isPlanPicker2Enabled();
    return thisDataWrapper;
  }

   /**
   * @description Method to get Opportunity record by Record Id
   * @param recordId Opportunity Id
   * @return Opportunity
   **/
  private static Opportunity getOpportunity(Id recordId) {
    if (Schema.sObjectType.Opportunity.isAccessible()) {
      return [
        SELECT
          Id,
          Name,
          OrdwayContract__c,
          Pricebook2Id,
          HasOpportunityLineItem,
          TotalOpportunityQuantity,
          OrdwayContractStatus__c,
          OrdwayOpportunityType__c
        FROM Opportunity
        WHERE Id = :recordId
      ];
    }
    return null;
  }

  /**
   * @description Wrapper data for Add / Edit Plan
   **/
  public class DataWrapper {
    @AuraEnabled
    public ContractPlanPickerSetting__c isUseOrdwayPricebook { get; set; }
    @AuraEnabled
    public Map<String, String> existingPricebook { get; set; }
    @AuraEnabled
    public Opportunity currentOpportunity { get; set; }
    @AuraEnabled
    public Boolean isNewPlanPickerToShow {get; set;} 
  }
}