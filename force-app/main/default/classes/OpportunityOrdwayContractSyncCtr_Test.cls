/**
 * @File Name          : OpportunityOrdwayContractSyncCtr_Test.cls
 * @Description        :
 * @Author             : Ordway Lab
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    31/01/2020   Ordway Labs     Initial Version
 **/

@isTest
private class OpportunityOrdwayContractSyncCtr_Test {
  @isTest
  private static void test_OpportunityOrdwayContractSync() {
    ApplicationSetting__c newApplicationSetting = TestObjectFactory.createApplicationSetting();
    insert newApplicationSetting;

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    insert newSetting;

    Test.startTest();
    List<OpportunityOrdwayContractSyncController.OrdwayContractSyncWrapper> contractWrapper = OpportunityOrdwayContractSyncController.getOpportunityOrdwayContractSync();
    system.assertEquals(3, contractWrapper.size());
    Test.stopTest();
  }
}