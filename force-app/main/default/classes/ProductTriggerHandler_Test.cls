/**
 * @File Name          : ProductTriggerHandler_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-09-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    03/22/2021        Initial Version
 **/
@isTest
private class ProductTriggerHandler_Test {

  @isTest
  private static void createOrdwayProduct_Bulk(){

    Test.startTest();
      List<Product2> thisProductList = new List<Product2>();
      for (Integer i = 0; i < 110; i++) {
        Product2 thisProduct         = TestObjectFactory.createProduct();
        thisProduct.OrdwayProduct__c = True;
        thisProductList.add(thisProduct);
      }
      insert thisProductList;
    Test.stopTest();
    system.assertEquals(110, [SELECT Id FROM OrdwayProduct__c].size(), 'should be 110');
  }

  @isTest
  private static void createOrdwayProduct_BulkNegative(){

    Test.startTest();
      try{
      List<Product2> thisProductList = new List<Product2>();
      for (Integer i = 0; i < 110; i++) {
        Product2 thisProduct = TestObjectFactory.createProduct();
        thisProductList.add(thisProduct);
      }
      insert thisProductList;
      } catch (Exception ex) {
        system.assertNotEquals(null, ex.getMessage());
        }
    Test.stopTest();
    system.assertEquals(0, [SELECT Id FROM OrdwayProduct__c].size(), 'should not create Ordway Product Record');
  }

  @isTest
  private static void createOrdwayProductOnUpdate(){

    Product2 thisProduct = TestObjectFactory.createProduct();
    insert thisProduct;

    Test.startTest();
      thisProduct.OrdwayProduct__c = true;
      update thisProduct;
    Test.stopTest(); 
    System.assertEquals(1, [SELECT Id FROM OrdwayProduct__c].size(), 'should create Ordway Product Record');
  }

  @isTest
  private static void createOrdwayProductOnUpdateBulk(){

    List<Product2> productList = new List<Product2>();
    List<Product2> productListToUpdate = new List<Product2>();
    for (Integer i = 0; i < 110; i++) {
      Product2 thisProduct = TestObjectFactory.createProduct();
      productList.add(thisProduct);
    }
    insert productList;
    Test.startTest();
      for(Product2 thisProduct : productList){
        thisProduct.OrdwayProduct__c = true;
        productListToUpdate.add(thisProduct);
      }
      update productListToUpdate;
    Test.stopTest();
    system.assertEquals(110, [SELECT Id FROM OrdwayProduct__c].size(), 'should be 110');
  }

  @isTest
  private static void populateChangedFieldsToOrdwayProduct(){
    
    ProductTriggerHandler.createOrdwayProduct = false;
    Product2 thisProduct = TestObjectFactory.createProduct();
    thisProduct.OrdwayProduct__c = true;
    insert thisProduct;

    Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
    thisOrdwayPlan.PlanDetails__c = thisProduct.Id;
    insert thisOrdwayPlan;

    OrdwayProduct__c thisOrdwayProduct  = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.Product2Id__c        = thisProduct.Id;
    insert thisOrdwayProduct;

    ObjectFieldMapping__mdt thisObjectFieldMappingMdt = TestObjectFactory.createObjectFieldMappingMetadata(
                                                                                    'Test' ,
                                                                                    'Product2',
                                                                                    'OrdwayProduct__c',
                                                                                    'OrdwayService_Mock.fieldMappingJson'  
                                                                                    );

    Test.startTest();
      thisProduct.Name = 'Test Product';
      update thisProduct;
    Test.stopTest(); 
    OrdwayProduct__c OrdwayProduct = [SELECT Id, Product2Id__c, Name
                                            FROM OrdwayProduct__c 
                                            WHERE Product2Id__c =: thisProduct.Id];
    System.assertEquals(thisProduct.Name, OrdwayProduct.Name);
  }

  @isTest
  private static void populateChangedFieldsToOrdwayProduct_Bulk(){

    List<Product2> productList               = new List<Product2>();
    List<Product2> productListToUpdate       = new List<Product2>();
    List<OrdwayProduct__c> ordwayProductList = new List<OrdwayProduct__c>();
    ObjectFieldMapping__mdt thisObjectFieldMappingMdt = TestObjectFactory.createObjectFieldMappingMetadata(
                                                                                    'Test' ,
                                                                                    'Product2',
                                                                                    'OrdwayProduct__c',
                                                                                    'OrdwayService_Mock.fieldMappingJson'  
                                                                                    );
    
    for (Integer i = 0; i < 110; i++) {
        ProductTriggerHandler.createOrdwayProduct = false; 
        Product2 thisProduct = TestObjectFactory.createProduct();
        thisProduct.OrdwayProduct__c = true;
        productList.add(thisProduct);    
    }
    insert productList;
    for(Product2 thisProduct : productList ){
        OrdwayProduct__c thisOrdwayProduct  = TestObjectFactory.createOrdwayProduct();
        thisOrdwayProduct.Product2Id__c     = thisProduct.Id;
        ordwayProductList.add(thisOrdwayProduct);
    } 
    insert ordwayProductList;

    for(Product2 thisProduct : productList){
        thisProduct.Name = 'Test Product';
        productListToUpdate.add(thisProduct);
    }
    Test.startTest();
    update productListToUpdate;
    Test.stopTest();
    OrdwayProduct__c OrdwayProduct = [SELECT Id, Product2Id__c, Name
                                              FROM OrdwayProduct__c 
                                              WHERE Product2Id__c =: productListToUpdate[1].Id];
    System.assertEquals(110, [SELECT Id FROM OrdwayProduct__c].size(), 'should be 110');
    System.assertEquals('Test Product', productListToUpdate[1].Name);
  }
}