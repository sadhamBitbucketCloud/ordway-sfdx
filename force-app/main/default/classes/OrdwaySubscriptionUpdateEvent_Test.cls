/**
 * @File Name          : OrdwaySubscriptionUpdateEvent_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    4/5/2020        Initial Version
 **/

@isTest
private class OrdwaySubscriptionUpdateEvent_Test {
  @isTest
  static void test_OrdwayContractUpdateEvent() {
    OrdwayContract__c ordwayContract = new OrdwayContract__c();
    ordwayContract = TestObjectFactory.createOrdwayContract(ordwayContract);
    ordwayContract.CustomerId__c = '123456';
    ordwayContract.OrdwaySubscriptionID__c = 'S-12344';
    insert ordwayContract;

    OrdwaySubscriptionUpdate__e thisEvent = new OrdwaySubscriptionUpdate__e();
    thisEvent.OrdwaySubscriptionID__c = ordwayContract.OrdwaySubscriptionID__c;
    thisEvent.RecordId__c = ordwayContract.Id;

    Test.startTest();
    Database.SaveResult sr = EventBus.publish(thisEvent);
    Test.stopTest();
    System.assertEquals(true, sr.isSuccess());
  }

  @isTest
  static void test_OrdwayContractUpdateEventAutoEnabled() {
    ContractPlanPickerSetting__c thisContractPlanPicker = new ContractPlanPickerSetting__c();
    thisContractPlanPicker.AutoSyncFromOrdway__c = true;
    OpportunitySettingController.saveOpportunitySetting(
      TestObjectFactory.createContractPlanPickerSetting(thisContractPlanPicker)
    );

    OrdwayContract__c ordwayContract = TestObjectFactory.createOrdwayContract();
    ordwayContract.OrdwaySubscriptionID__c = 'Sub-123';

    insert ordwayContract;

    ApplicationSetting__c thisApplicationsetting = TestObjectFactory.createApplicationSetting();
    insert thisApplicationsetting;

    OrdwaySubscriptionUpdate__e thisEvent = new OrdwaySubscriptionUpdate__e();
    thisEvent.OrdwaySubscriptionID__c = ordwayContract.OrdwaySubscriptionID__c;
    thisEvent.RecordId__c = ordwayContract.Id;

    Test.startTest();
    Integer RESPONSE_CODE = 200;
    String RESPONSE_BODY =
      '{ ' +
      ' "customer" : { ' +
      '   "Website" : null,' +
      '   "Name" : "Demo"' +
      '  },' +
      ' "plans" : [ {' +
      '   "OrdwayLabs__ChargeType__c" : "One time",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "1000.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "1000.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Recurring",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 0.0,' +
      ' "OrdwayLabs__Quantity__c" : 1.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "500.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "500.00"' +
      '}, {' +
      ' "OrdwayLabs__ChargeType__c" : "Usage based",' +
      ' "OrdwayLabs__OrdwayChargeId__c" : "",' +
      ' "OrdwayLabs__TierJSON__c" : null,' +
      ' "OrdwayLabs__PricingModel__c" : "per unit",' +
      ' "OrdwayLabs__Discount__c" : 5.0,' +
      ' "OrdwayLabs__Quantity__c" : 100.0,' +
      ' "OrdwayLabs__OrdwayEffectivePrice__c" : "950.00",' +
      ' "OrdwayLabs__OrdwayListPrice__c" : "10.00"' +
      '} ],' +
      '"OrdwayLabs__OrdwayMRR__c" : null,' +
      '"OrdwayLabs__OrdwayContractStatus__c" : "Active",' +
      '"OrdwayLabs__AutoRenew__c" : true,' +
      '"OrdwayLabs__RenewalTerm__c" : "12",' +
      '"OrdwayLabs__ContractTerm__c" : "12",' +
      '"OrdwayLabs__ContractEffectiveDate__c" : "2020-03-27",' +
      '"OrdwayLabs__BillingStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__ServiceStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__TotalContractValue__c" : 111520.0,' +
      '"OrdwayLabs__CurrentTermEndDate__c" : "2021-03-23",' +
      '"OrdwayLabs__CurrentTermStartDate__c" : "2020-03-24",' +
      '"OrdwayLabs__CustomerId__c" : "C-00001",' +
      '"Id" : "testContractId"' +
      '}';
    Test.setMock(
      HttpCalloutMock.class,
      new OrdwayService_Mock(
        RESPONSE_CODE,
        RESPONSE_BODY.replace(
          'testContractId',
          String.valueOf(ordwayContract.Id)
        )
      )
    );
    Database.SaveResult sr = EventBus.publish(thisEvent);
    Test.stopTest();
    System.assertEquals(true, sr.isSuccess());
  }
}