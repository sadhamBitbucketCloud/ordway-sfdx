public class OrdwayProductController {

  @AuraEnabled
  public static void syncToOrdway(String recordId){

    try {

      OrdwayProduct__c thisProduct = [SELECT Id, OrdwayProductID__c FROM OrdwayProduct__c WHERE Id =: recordId];

      HttpResponse thisResponse = syncProduct(recordId);

      if ( thisResponse.getStatusCode() >= 200 && thisResponse.getStatusCode() <= 299 ) {
   
        OrdwayHelper.getSObjectRecord( OrdwayHelper.getDeserializedUntyped(thisResponse), Schema.sObjectType.OrdwayProduct__c, thisProduct);
        thisProduct.OrdwayLastSyncDate__c = System.now();
        SObjectAccessDecision securityDecision = Security.stripInaccessible( AccessType.UPDATABLE, new List<OrdwayProduct__c>{ thisProduct });
        upsert securityDecision.getRecords();
      } 
      else {
        String errorMessage = OrdwayHelper.getResponseStatusMessage(thisResponse);
        AuraHandledException ex = new AuraHandledException(errorMessage);
        ex.setMessage(errorMessage);
        throw ex;
      }
    } catch (Exception e) {
      throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  public static HttpResponse syncProduct(Id recordId) {

    OrdwayProduct__c thisProduct = [SELECT Id, OrdwayProductID__c FROM OrdwayProduct__c WHERE Id =: recordId];

    if( thisProduct.OrdwayProductID__c != null) {
      return OrdwayService.updateOrdwayProduct(recordId);
    }
    return OrdwayService.createOrdwayProduct(recordId);
  }

  public static OrdwayProduct__c getOrdwayProduct(Id ordwayProductId) {
    return [SELECT Id, OrdwayProductID__c, OrdwayEntity__c FROM OrdwayProduct__c WHERE Id =: ordwayProductId];
  }

  @AuraEnabled
  public static InitialWrapper getInitialWrapper(Id ordwayProductId) {
    try {
        InitialWrapper thisInitialWrapper = new InitialWrapper();
        thisInitialWrapper.thisOrdwayProduct = getOrdwayProduct(ordwayProductId);
        thisInitialWrapper.namedCredentials = getNamedCredential();
        thisInitialWrapper.isMultiEntityEnabled = OpportunitySettingController.isMultiEntityEnabled();
        return thisInitialWrapper;
      } catch (Exception e) {
        throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  @AuraEnabled(cacheable=true)
  public static String getNamedCredential() {
    try {
      return OrdwayService.getNamedCredentialURL();
    } catch (Exception e) {
      throw new AuraHandledException('Error ' + e.getMessage());
    }
  }

  public class InitialWrapper {
    @AuraEnabled
    public OrdwayProduct__c thisOrdwayProduct;
    @AuraEnabled
    public String namedCredentials;
    @AuraEnabled
    public Boolean isMultiEntityEnabled;
  }

}