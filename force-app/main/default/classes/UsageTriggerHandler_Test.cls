@isTest
private class UsageTriggerHandler_Test {
	@isTest
    static void beforeInsert(){
        //Insert Account
        Account newAccount = TestObjectFactory.createAccount();
        insert newAccount;
       	//Insert contact
        Contact ordwayContact = TestObjectFactory.createContact();
        ordwayContact.AccountId = newAccount.Id;
        insert ordwayContact;
        // Insert Contract
        OrdwayContract__c ordwayContract = TestObjectFactory.createOrdwayContract();
        ordwayContract.BillingContact__c = ordwayContact.Id;  
        ordwayContract.ContractEffectiveDate__c = system.today();
        ordwayContract.BillingStartDate__c = system.today();
        ordwayContract.ServiceStartDate__c = system.today();
        ordwayContract.Account__c = newAccount.Id;  
        insert ordwayContract;
        // Insert Contract Line Item
        ContractLineItem__c ordwayCLI = TestObjectFactory.createContractLineItem();
        ordwayCLI.OrdwayContract__c = ordwayContract.Id;
        insert ordwayCLI;
        
        // Insert Usage
        OrdwayUsage__c ordwayUsage = TestObjectFactory.createOrdwayUsage();
        ordwayUsage.Account__c = newAccount.Id;
        ordwayUsage.OrdwayContract__c = ordwayContract.Id;
        ordwayUsage.SubscriptionChargeLineID__c = ordwayCLI.Id;
        insert ordwayUsage;
        OrdwayUsage__c ordwayUsageToAssert = [SELECT Id, OrdwayContractLineItem__c FROM OrdwayUsage__c];
        system.assertEquals(ordwayCLI.Id, ordwayUsageToAssert.OrdwayContractLineItem__c, 'Should return same as SubscriptionChargeLineID');
    }
}