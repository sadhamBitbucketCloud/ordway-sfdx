/**
 * @File Name          : OrdwayProductTriggerHandler_Test.cls
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-22-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    03/22/2021        Initial Version
 **/
@isTest
private class OrdwayProductTriggerHandler_Test {
  @isTest
  private static void populateOrdwayProductId() {

    Test.startTest();
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct   = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c      = thisProduct2.Id;
      thisOrdwayProduct.Name               = 'Test OrdwayProduct';
      thisOrdwayProduct.ExternalID__c      = 'EXTERNEL:C:C-1234';
      insert thisOrdwayProduct;
    Test.stopTest();
    System.assertEquals('C-1234', [SELECT Id, OrdwayProductID__c FROM OrdwayProduct__c].OrdwayProductID__c, 
                                                    'should return same ordwayproduct Id');
  }

  @isTest
  private static void populateOrdwayProductIdInsert() {

    Test.startTest();
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct   = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c      = thisProduct2.Id;
      thisOrdwayProduct.Name               = 'Test OrdwayProduct';
      thisOrdwayProduct.ExternalID__c      = 'C-1234';
      insert thisOrdwayProduct;
    Test.stopTest();
    System.assertEquals('C-1234', [SELECT Id, OrdwayProductID__c FROM OrdwayProduct__c].OrdwayProductID__c, 
                                                    'should return same ordwayproduct Id');
  }

  @isTest
  private static void populateOrdwayProduct(){

    Test.startTest();
    Product2 thisProduct2 = TestObjectFactory.createProduct();
    insert thisProduct2;

    Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
    thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
    insert thisOrdwayPlan;

    OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
    thisOrdwayProduct.Name             = 'Test OrdwayProduct';
    insert thisOrdwayProduct;

    thisOrdwayProduct.ExternalID__c    = 'C-123456';
    update thisOrdwayProduct;
  Test.stopTest();
  System.assertEquals('C-123456', [SELECT Id, OrdwayProductID__c FROM OrdwayProduct__c].OrdwayProductID__c, 
                                                  'should return same ordwayproduct Id');
  }

  @isTest
  private static void populateOrdwayProductIdUpate(){

    Test.startTest();
    Product2 thisProduct2 = TestObjectFactory.createProduct();
    insert thisProduct2;

    Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();                                                             
    thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
    insert thisOrdwayPlan;

    OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
    thisOrdwayProduct.Name             = 'Test OrdwayProduct';
    insert thisOrdwayProduct;

    thisOrdwayProduct.ExternalID__c    = 'EXTERNEL:C:C-1234';
    update thisOrdwayProduct;
  Test.stopTest();
  System.assertEquals('C-1234', [SELECT Id, OrdwayProductID__c FROM OrdwayProduct__c].OrdwayProductID__c, 
                                                  'should return same ordwayproduct Id');
  }
  
  @isTest
  private static void populateOrdwayEntity() {
 
    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c         = true;
    insert newSetting;

    Entity__c thisEntity = TestObjectFactory.createOrdwayEntity();
    thisEntity.Name      = 'Test Entity';
    insert thisEntity;
    
    thisEntity.EntityID__c = thisEntity.Id;
    update thisEntity;

    Map<Id, Entity__c> entityMap = OrdwayEntityHelper.entityMapById;

    Test.startTest();
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c    = thisProduct2.Id;
      thisOrdwayProduct.EntityId__c      = thisEntity.EntityID__c;
      insert thisOrdwayProduct;
    Test.stopTest();

    OrdwayProduct__c ordwayProduct = [SELECT Id, Product2Id__c, OrdwayEntity__c, Name
                                              FROM OrdwayProduct__c];
    system.assertEquals(thisEntity.Id, ordwayProduct.OrdwayEntity__c, 'should return same entity');
  }

  @isTest
  private static void populateEntityName(){

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c         = true;
    insert newSetting;

    Entity__c thisEntity   = TestObjectFactory.createOrdwayEntity();
    thisEntity.Name        = 'Test Entity';
    insert thisEntity; 
        
    thisEntity.EntityID__c = thisEntity.Id;
    update thisEntity;

    Map<Id, Entity__c> entityMap = OrdwayEntityHelper.entityMapById;

    Test.startTest();
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct  = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c     = thisProduct2.Id;
      thisOrdwayProduct.OrdwayEntity__c   = thisEntity.Id;
      insert thisOrdwayProduct;
    Test.stopTest();

    OrdwayProduct__c ordwayProduct = [SELECT Id, EntityName__c FROM OrdwayProduct__c];
    system.assertEquals('Test Entity', ordwayProduct.EntityName__c, 'should return same entity name');
    thisOrdwayProduct.OrdwayEntity__c = null;
    thisOrdwayProduct.EntityId__c = null;
    update thisOrdwayProduct;
    System.assertEquals(null, [SELECT Id, EntityName__c FROM OrdwayProduct__c].EntityName__c);
  }

  @isTest
  private static void checkBlankEntity(){

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c         = true;
    insert newSetting;

    Test.startTest();
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;
      try{
        OrdwayProduct__c thisOrdwayProduct  = TestObjectFactory.createOrdwayProduct();
        thisOrdwayProduct.Product2Id__c     = thisProduct2.Id;
        insert thisOrdwayProduct;
      } catch (Exception ex) {
        system.assertNotEquals(null, ex.getMessage());
        }
    Test.stopTest(); 
  }

  @isTest
  private static void createProduct(){

    Test.startTest();
      OrdwayProduct__c thisOrdwayProduct   = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.OrdwayProductID__c = 'Test OrdwayProduct';
      thisOrdwayProduct.Name               = 'Test OrdwayProduct';
      ProductTriggerHandler.createOrdwayProduct = false;
      insert thisOrdwayProduct;
    Test.stopTest();

    System.assertEquals(1, [SELECT Id FROM Product2 ].size());
    System.assertEquals([SELECT Id FROM Product2 ].Id, [SELECT Product2Id__c FROM OrdwayProduct__c].Product2Id__c);
  }

  @isTest
  private static void populateChangedEntityName(){

    ContractPlanPickerSetting__c newSetting = TestObjectFactory.createContractPlanPickerSetting();
    newSetting.EnableMultiEntity__c         = true;
    insert newSetting;

    Entity__c thisEntity   = TestObjectFactory.createOrdwayEntity();
    thisEntity.Name        = 'Test Entity';
    insert thisEntity; 
        
    thisEntity.EntityID__c = thisEntity.Id;
    update thisEntity;

    Entity__c thisEntityNew   = TestObjectFactory.createOrdwayEntity();
    thisEntityNew.Name        = 'Test Entity New';
    insert thisEntityNew; 
        
    thisEntityNew.EntityID__c = thisEntityNew.Id;
    update thisEntityNew;

    Test.startTest();
      Product2 thisProduct2 = TestObjectFactory.createProduct();
      insert thisProduct2;

      Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
      thisOrdwayPlan.PlanDetails__c = thisProduct2.Id;
      insert thisOrdwayPlan;

      OrdwayProduct__c thisOrdwayProduct  = TestObjectFactory.createOrdwayProduct();
      thisOrdwayProduct.Product2Id__c     = thisProduct2.Id;
      thisOrdwayProduct.OrdwayEntity__c   = thisEntity.Id;
      insert thisOrdwayProduct;
    Test.stopTest();

    OrdwayProduct__c ordwayProduct = [SELECT Id, EntityName__c FROM OrdwayProduct__c];
    system.assertEquals('Test Entity', ordwayProduct.EntityName__c, 'should return same entity name');
    thisOrdwayProduct.OrdwayEntity__c = thisEntityNew.Id;
    update thisOrdwayProduct;
    System.assertEquals('Test Entity New', [SELECT Id, EntityName__c FROM OrdwayProduct__c].EntityName__c);
  }

  @isTest
  private static void populateChangedFieldsToProduct(){

    ProductTriggerHandler.createOrdwayProduct = false;

    Product2 thisProduct         = TestObjectFactory.createProduct();
    thisProduct.OrdwayProduct__c = true;
    insert thisProduct;

    Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
    thisOrdwayPlan.PlanDetails__c = thisProduct.Id;
    insert thisOrdwayPlan;

    OrdwayProduct__c thisOrdwayProduct   = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.Product2Id__c      = thisProduct.Id;
    thisOrdwayProduct.OrdwayProductID__c = thisProduct.Id;
    insert thisOrdwayProduct;

    ObjectFieldMapping__mdt thisObjectFieldMappingMdt = TestObjectFactory.createObjectFieldMappingMetadata(
                                                                              'Test' ,
                                                                              'OrdwayProduct__c',
                                                                              'Product2',
                                                                              'OrdwayService_Mock.fieldMappingJson'  
                                                                            );

    Test.startTest();
      thisOrdwayProduct.Name = 'Test OrdwayProduct';
      update thisOrdwayProduct;
    Test.stopTest(); 
      Product2 product = [SELECT Id, Name FROM Product2 ];
      System.assertEquals('Test OrdwayProduct', product.Name);
  }

  @isTest
  private static void updateProductIdAtCharge(){

    Product2 thisProduct = TestObjectFactory.createProduct();
    insert thisProduct;

    Plan__c thisOrdwayPlan        = TestObjectFactory.createOrdwayPlan();
    thisOrdwayPlan.PlanDetails__c = thisProduct.Id;
    insert thisOrdwayPlan;

    OrdwayProduct__c thisOrdwayProduct   = TestObjectFactory.createOrdwayProduct();
    thisOrdwayProduct.OrdwayProductID__c = 'Test';
    thisOrdwayProduct.Product2Id__c      = thisProduct.Id;
    insert thisOrdwayProduct;

    Charge__c thisCharge       = TestObjectFactory.createCharge();
    thisCharge.Name            = 'Test Charge';
    thisCharge.Product__c      = thisProduct.Id;
    thisCharge.OrdwayPlan__c   = thisOrdwayPlan.Id;
    insert thisCharge;

    Test.startTest();
      thisOrdwayProduct.OrdwayProductID__c = 'Test OrdwayProduct';
      update thisOrdwayProduct;
    Test.stopTest(); 
      Charge__c charge = [ SELECT Id, ProductID__c FROM Charge__c];
      System.assertEquals('Test OrdwayProduct', charge.ProductID__c);
  }
}