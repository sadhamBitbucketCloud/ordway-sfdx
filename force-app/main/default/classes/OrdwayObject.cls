public inherited sharing class OrdwayObject {
  
   // OrdwayObject.getContract('a0G0t0000041ybdEAA');
  public static Map<String, Schema.SObjectType> thisGD = Schema.getGlobalDescribe();
  public static Set<Id> lineItemIds = new Set<Id>();
  
  public static Map<String, Map<String, Object>>  getContract(Id contractId){

    Map<Id, List<ContractLineItemTier__c>> tierMap = new Map<Id, List<ContractLineItemTier__c>>();

    Schema.SobjectType thisObjectType = thisGD.get('OrdwayLabs__OrdwayContract__c');

    OrdwayContract__c thisContract = Database.query('SELECT Account__r.Id, Account__r.Name, ' +String.join(SObjectHelper.getAllFields(thisObjectType),',') +
                                                    ' FROM OrdwayLabs__OrdwayContract__c WHERE Id =: contractId ');

    thisObjectType = thisGD.get('OrdwayLabs__ContractLineItem__c'); 

    Map<Id, ContractLineItem__c> thisList = new Map<Id, ContractLineItem__c>((List<ContractLineItem__c>)Database.query(
                                      'SELECT ' +String.join(SObjectHelper.getAllFields(thisObjectType),',') +
                                      ' FROM OrdwayLabs__ContractLineItem__c WHERE OrdwayLabs__OrdwayContract__c =: contractId '));

    lineItemIds = thisList.keySet();
    thisObjectType = thisGD.get('OrdwayLabs__ContractLineItemTier__c');
    
    for(ContractLineItemTier__c thisTier : [SELECT Tier__c, StartingUnit__c, EndingUnit__c, PricingType__c, ListPrice__c, ContractLineItem__c 
                                                              FROM ContractLineItemTier__c 
                                                              WHERE ContractLineItem__c IN: lineItemIds
                                                              ORDER BY Tier__c ASC]) {
      if(!tierMap.containsKey(thisTier.ContractLineItem__c)) {
        tierMap.put(thisTier.ContractLineItem__c, new List<ContractLineItemTier__c>{});
      }
      tierMap.get(thisTier.ContractLineItem__c).add(thisTier);
    }
    
    return getContract(thisContract, thisList.values(), tierMap);
  }   

  private static Map<String, Map<String, Object>>  getContract(OrdwayContract__c thisContract, List<ContractLineItem__c> thisCLIList, Map<Id, List<ContractLineItemTier__c>> tierMap) {
    Map<String, Object> contract = new Map<String, Object>();

    contract.putAll(thisContract.getPopulatedFieldsAsMap());

    Map<String, Object> eachItem;
    List<Map<String, Object>> lineItems = new List<Map<String, Object>>();

    for(ContractLineItem__c thisCLI : thisCLIList) {
      eachItem = new Map<String, Object>();
      eachItem.putAll(thisCLI.getPopulatedFieldsAsMap());
      eachItem.put('tiers', tierMap.get(thisCLI.Id));
      lineItems.add(eachItem);
    }
    contract.put('line_items', lineItems);

    Map<String, Map<String, Object>> contractMap = new Map<String, Map<String, Object>>();
    contractMap.put('contract', contract);
    return contractMap;
  }

  public static Map<String, Map<String, Object>> getQuote(Id quoteId){

    Map<Id, List<QuoteLineItemTier__c>> tierMap = new Map<Id, List<QuoteLineItemTier__c>>();

    Schema.SobjectType thisObjectType = thisGD.get('OrdwayLabs__Quote__c');

    Quote__c thisQuote = Database.query('SELECT Account__r.Id, Account__r.Name, ' +String.join(SObjectHelper.getAllFields(thisObjectType),',') +
                                        ' FROM OrdwayLabs__Quote__c WHERE Id =: quoteId ');

    thisObjectType = thisGD.get('OrdwayLabs__QuoteLineItem__c');

    Map<Id, QuoteLineItem__c> thisList = new Map<Id, QuoteLineItem__c>((List<QuoteLineItem__c>)Database.query(
                                                    'SELECT ' +String.join(SObjectHelper.getAllFields(thisObjectType),',') +
                                                    ' FROM OrdwayLabs__QuoteLineItem__c WHERE Quote__c =: quoteId '));
    lineItemIds = thisList.keySet();

    thisObjectType = thisGD.get('OrdwayLabs__QuoteLineItemTier__c');

    for(QuoteLineItemTier__c thisTier : Database.query(
                                'SELECT ' +String.join(SObjectHelper.getAllFields(thisObjectType),',') +
                                ' FROM OrdwayLabs__QuoteLineItemTier__c WHERE OrdwayLabs__QuoteLineItem__c IN: lineItemIds ORDER BY Tier__c ASC')){
      if(!tierMap.containsKey(thisTier.QuoteLineItem__c)) {
        tierMap.put(thisTier.QuoteLineItem__c, new List<QuoteLineItemTier__c>{});
      }
      tierMap.get(thisTier.QuoteLineItem__c).add(thisTier);
    }
   
    return getQuote(thisQuote, thisList.values(), tierMap);
  }
 
  private static Map<String, Map<String, Object>>  getQuote(Quote__c thisQuote, List<QuoteLineItem__c> thisQLIList, Map<Id, List<QuoteLineItemTier__c>> tierMap) {
    Map<String, Object> quote = new Map<String, Object>();

    quote.putAll(thisQuote.getPopulatedFieldsAsMap());

    Map<String, Object> eachItem;
    List<Map<String, Object>> lineItems = new List<Map<String, Object>>();

    for(QuoteLineItem__c thisQLI : thisQLIList) {
      eachItem = new Map<String, Object>();
      eachItem.putAll(thisQLI.getPopulatedFieldsAsMap());
      eachItem.put('tiers', tierMap.get(thisQLI.Id));
      lineItems.add(eachItem);
    }
    quote.put('line_items', lineItems);

    Map<String, Map<String, Object>> qMap = new Map<String, Map<String, Object>>();
    qMap.put('quote', quote);
    return qMap;
  }

  public static Map<String, Object> getPlan(Id planId){

    Map<Id, List<ChargeTier__c>> tierMap = new Map<Id, List<ChargeTier__c>>();

    Schema.SobjectType thisObjectType = thisGD.get('OrdwayLabs__Plan__c');

    Plan__c thisPlan =  Database.query('SELECT ' +String.join(SObjectHelper.getAllFields(thisObjectType),',') +
                                          ' FROM OrdwayLabs__Plan__c WHERE Id =: planId ');
    thisObjectType = thisGD.get('OrdwayLabs__Charge__c');

    Map<Id, Charge__c> thisList = new Map<Id, Charge__c>((List<Charge__c>)Database.query(
                                                        'SELECT ' +String.join(SObjectHelper.getAllFields(thisObjectType),',') +
                                                        ' FROM OrdwayLabs__Charge__c WHERE OrdwayLabs__OrdwayPlan__c =: planId '));
          
    lineItemIds = thisList.keySet();
    
    thisObjectType = thisGD.get('OrdwayLabs__ChargeTier__c');
    for(ChargeTier__c thisTier :  Database.query('SELECT ' +String.join(SObjectHelper.getAllFields(thisObjectType),',') +
                                ' FROM OrdwayLabs__ChargeTier__c WHERE OrdwayLabs__Charge__c IN: lineItemIds ORDER BY Tier__c ASC')){
      if(!tierMap.containsKey(thisTier.Charge__c)) {
        tierMap.put(thisTier.Charge__c, new List<ChargeTier__c>{}); 
      }
      tierMap.get(thisTier.Charge__c).add(thisTier);
    }
    
    return getPlan(thisPlan, thisList.values(), tierMap);
  }

  private static Map<String, Map<String, Object>> getPlan(Plan__c thisPlan, List<Charge__c> thisChargeList, Map<Id, List<ChargeTier__c>> tierMap) {
    Map<String, Object> plan = new Map<String, Object>();

    plan.putAll(thisPlan.getPopulatedFieldsAsMap());

    Map<String, Object> eachItem;
    List<Map<String, Object>> lineItems = new List<Map<String, Object>>();

    for(Charge__c thisCharge : thisChargeList) {
      eachItem = new Map<String, Object>();
      eachItem.putAll(thisCharge.getPopulatedFieldsAsMap());
      eachItem.put('tiers', tierMap.get(thisCharge.Id));
      lineItems.add(eachItem);
    }
    plan.put('charges', lineItems);

    Map<String, Map<String, Object>> planMap = new Map<String, Map<String, Object>>();
    planMap.put('plan', plan);
    return planMap;
  }

  public static OrdwayProduct__c getOrdwayProduct(Id ordwayProductId) {

    Schema.SobjectType thisObjectType = thisGD.get('OrdwayLabs__OrdwayProduct__c');

    return Database.query('SELECT ' +String.join(SObjectHelper.getAllFields(thisObjectType),',') +
                                        ' FROM OrdwayLabs__OrdwayProduct__c WHERE Id =: ordwayProductId ');
  }
  
}