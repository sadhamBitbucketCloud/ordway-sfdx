/**
 * @File Name          : PostInstallPackage_Test.cls
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    9/29/2019   Ordway Labs     Initial Version
 **/
@isTest
private class PostInstallPackage_Test {
  @isTest
  static void testInstallScript() {
    Test.startTest();
    PostInstallPackage postInstall = new PostInstallPackage();
    Test.testInstall(postInstall, null);
    Test.testInstall(postInstall, new Version(1, 0), true);
    System.assertEquals(
      OrdwayHelper.ORDWAY_APPLICATION_SETTING,
      ApplicationSetting__c.getValues(OrdwayHelper.ORDWAY_APPLICATION_SETTING)
        .Name,
      'Should insert the application setting and return the name of the record'
    );

    System.assertEquals(
      OrdwayHelper.OPPORTUNITY_SETTING_NAME,
      ContractPlanPickerSetting__c.getValues(
          OrdwayHelper.OPPORTUNITY_SETTING_NAME
        )
        .Name,
      'Should insert the contract/plan pickers setting and return the name of the record'
    );
    Test.stopTest();
  }
}