/**
 * @File Name          : OpportunityLineItemTriggerHandler.cls
 * @Description        : OpportunityLineItem Trigger Handler
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-25-2021
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    27/10/2019   Ordway Labs     Initial Version
 **/
public with sharing class OpportunityLineItemTriggerHandler extends TriggerHandler.TriggerHandlerBase {
  public override void beforeInsert(List<SObject> listNewSObjs) {
    for ( OpportunityLineItem thisOpportunityLineItem : (List<OpportunityLineItem>) listNewSObjs ) {
      if (
        OpportunitySettingController.useOrdwayCalculation() &&
        thisOpportunityLineItem.OrdwayUndiscountedContractValue__c != null
      ) {
        //We are not taking discounted contract value for Unit Price Calculation as Total Price will be Unit Price * Quantity - Discount
        thisOpportunityLineItem.UnitPrice =
          thisOpportunityLineItem.OrdwayUndiscountedContractValue__c /
          thisOpportunityLineItem.Quantity;
      }
      if (thisOpportunityLineItem.OrdwayDiscountedContractValue__c != null) {
        thisOpportunityLineItem.DiscountedContractValue__c = thisOpportunityLineItem.OrdwayDiscountedContractValue__c;
      }
    }
  }

  public override void afterInsert(Map<Id, SObject> mapNewSObjs) {
    OpportunityLineItemService.createOrdwayContractLineItem(
      mapNewSObjs.keySet()
    );
  }

  public override void beforeUpdate(
    Map<Id, SObject> mapOldSObjs,
    Map<Id, SObject> mapNewSObjs
  ) {
    for (
      OpportunityLineItem thisOpportunityLineItem : (List<OpportunityLineItem>) mapNewSObjs.values()
    ) {
      if (
        OpportunitySettingController.useOrdwayCalculation() &&
        thisOpportunityLineItem.OrdwayUndiscountedContractValue__c != null
      ) {
        //We are not taking discounted contract value for Unit Price Calculation as Total Price will be Unit Price * Quantity - Discount
        thisOpportunityLineItem.UnitPrice =
          thisOpportunityLineItem.OrdwayUndiscountedContractValue__c /
          thisOpportunityLineItem.Quantity;
      }

      if (thisOpportunityLineItem.OrdwayDiscountedContractValue__c != null) {
        thisOpportunityLineItem.DiscountedContractValue__c = thisOpportunityLineItem.OrdwayDiscountedContractValue__c;
      }
    }
  }

  public override void beforeDelete(Map<Id, SObject> mapOldSObjs) {
    
    Set<Id> OLIIds = new Set<Id>();
    for (OpportunityLineItem thisOpportunityLineItem : (List<OpportunityLineItem>) mapOldSObjs.values()) {
      OLIIds.add(thisOpportunityLineItem.Id);
    }
    if(!OLIIds.isEmpty()){
      TierController.deleteAllRelatedTiers(OLIIds, 'OrdwayLabs__OpportunityLineItemTier__c', 'OrdwayLabs__OpportunityProduct__c');
    }
  }

  public override void afterDelete(Map<Id, SObject> mapOldSObjs) {
    Set<Id> opportunityIds = new Set<Id>();
    Map<Id, Opportunity> opportunityMap;

    for (
      OpportunityLineItem thisOpportunityLineItem : (List<OpportunityLineItem>) mapOldSObjs.values()
    ) {
      opportunityIds.add(thisOpportunityLineItem.OpportunityId);
    }
    opportunityMap = OpportunityService.getOpportunityMap(opportunityIds);
    OpportunityLineItemService.deleteSyncedContractLineitem(
      (Map<Id, OpportunityLineItem>) mapOldSObjs,
      opportunityMap
    );
  }
}