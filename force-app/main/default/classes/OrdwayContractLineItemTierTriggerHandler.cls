public with sharing class OrdwayContractLineItemTierTriggerHandler extends TriggerHandler.TriggerHandlerBase {
	
	public static Boolean deleteTier = true;

	public override void beforeInsert(List<SObject> listNewSObjs) {

		if(deleteTier) {
			Set<Id> contractLineItemIds = new Set<Id>();

			for(OrdwayLabs__ContractLineItemTier__c thisContractLineItemTier : (List<OrdwayLabs__ContractLineItemTier__c>) listNewSObjs){
				if(thisContractLineItemTier.OrdwayLabs__ContractLineItem__c != null){
					contractLineItemIds.add(thisContractLineItemTier.OrdwayLabs__ContractLineItem__c);
				}
				
				if(!contractLineItemIds.isEmpty()){
					TierController.deleteAllRelatedTiers(contractLineItemIds, 'OrdwayLabs__ContractLineItemTier__c', 'OrdwayLabs__ContractLineItem__c');
				}
			}
		}

	}

	public override void beforeDelete(Map<Id, SObject> mapOldSObjs) {
		for(OrdwayLabs__ContractLineItemTier__c thisContractLineItemTier : (List<OrdwayLabs__ContractLineItemTier__c>) mapOldSObjs.values()){
			if(TierController.validateOnDelete){
				TierController.validateBeforeTierDelete(thisContractLineItemTier);
			}
		}
	}
}