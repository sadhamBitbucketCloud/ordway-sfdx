/**
 * @File Name          : OrdwayService_Mock.cls
 * @Description        : Mock Class for Ordway Http Request
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-27-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    2/11/2020   Ordway Labs     Initial Version
 **/
@isTest
global class OrdwayService_Mock implements HttpCalloutMock {
  Integer RESPONSE_CODE;
  String RESPONSE_BODY;
  global OrdwayService_Mock(Integer RESPONSE_CODE, String RESPONSE_BODY) {
    this.RESPONSE_CODE = RESPONSE_CODE;
    this.RESPONSE_BODY = RESPONSE_BODY;
  }

  global HTTPResponse respond(HTTPRequest request) {
    HttpResponse response = new HttpResponse();
    response.setHeader('Content-Type', 'application/json');
    response.setBody(RESPONSE_BODY);
    response.setStatusCode(RESPONSE_CODE);
    return response;
  }

  public static final String ordWayAccountString =
    '{"result": [ ' +
    ' { ' +
    ' "Name": "Ordway ", ' +
    ' "Website": "www.Ordway.com", ' +
    ' "OrdwayLabs__OrdwayCustomerId__c": "C-00107", ' +
    ' "ParentId": "testParentId", ' +
    ' "Id": "newAccountId", ' +
    ' "attributes": { ' +
    ' "type": "Account" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Ordway  Dev", ' +
    ' "Website": "www.OrdwayDev.com", ' +
    ' "OrdwayLabs__OrdwayCustomerId__c": "C-00106", ' +
    ' "ParentId": "null", ' +
    ' "Id": "newAccountId1", ' +
    ' "attributes": { ' +
    ' "type": "Account" ' +
    ' } ' +
    ' } ' +
    ' ], ' +
    ' "total_count": 2 ' +
    ' } ';

  public static final String contractString =
    ' { ' +
    ' "Id": "testContractId", ' +
    ' "IsDeleted": false, ' +
    ' "Name": "Test New Contract", ' +
    ' "CreatedDate": "2020-07-19T13:42:59.000Z", ' +
    ' "LastModifiedDate": "2020-07-19T14:47:19.000Z", ' +
    ' "OrdwayLabs__AutoRenew__c": true, ' +
    ' "OrdwayLabs__BillingStartDate__c": "2020-07-19", ' +
    ' "OrdwayLabs__ContractEffectiveDate__c": "2020-07-11", ' +
    ' "OrdwayLabs__ContractTerm__c": "12", ' +
    ' "OrdwayLabs__ExternalID__c": "S-00014", ' +
    ' "OrdwayLabs__OrdwayContractStatus__c": "Active", ' +
    ' "OrdwayLabs__OrdwaySubscriptionID__c": "S-00014", ' +
    ' "OrdwayLabs__RenewalTerm__c": "12", ' +
    ' "OrdwayLabs__ServiceStartDate__c": "2020-07-19", ' +
    ' "OrdwayLabs__SyncStatus__c": "Not Synced", ' +
    ' "OrdwayLabs__VersionType__c": "Change", ' +
    ' "OrdwayLabs__TotalContractQuantity__c": 0, ' +
    ' "OrdwayLabs__CancellationDate__c": "2020-07-23" ' +
    ' } ';

  public static final String contractLineItemTier =
  ' ['
  + ' {'
      + ' "Id": "CONTRACTLINEITEMTIER",'
      + ' "Name": "CLIT-00000",'
      + ' "CreatedDate": "2021-03-01T14:16:13.000Z",'
      + ' "CreatedById": "0050t000006z3noAAA",'
      + ' "LastModifiedById": "0050t000006z3IgAAI",'
      + ' "OrdwayLabs__ContractLineItem__c": "CLIID",'
      + ' "OrdwayLabs__EndingUnit__c": 100,'
      + ' "OrdwayLabs__ListPrice__c": 8,'
      + ' "OrdwayLabs__PricingType__c": "Per unit",'
      + ' "OrdwayLabs__StartingUnit__c": 0,'
      + ' "OrdwayLabs__Tier__c": 1'
  + ' },'
  + ' {'
      + ' "OrdwayLabs__ListPrice__c": "2",'
      + ' "OrdwayLabs__PricingType__c": "Per unit",'
      + ' "OrdwayLabs__EndingUnit__c": null,'
      + ' "OrdwayLabs__StartingUnit__c": 201,'
      + ' "OrdwayLabs__Tier__c": 4,'
      + ' "OrdwayLabs__ContractLineItem__c": "CLIID"'
  + ' }'
  + ' ]';

  public static final String contractLineItemTierToDelete =
  ' ['
  + '  {'
         + ' "Id": "cLItierToDelete",'
         + ' "Name": "CLIT-00022",'
         + ' "CreatedDate": "2021-03-18T11:53:26.000Z",'
         + ' "CreatedById": "0050t000006z3IgAAI",'
         + ' "LastModifiedById": "0050t000006z3IgAAI",'
         + ' "OrdwayLabs__ContractLineItem__c": "CLIID",'
         + ' "OrdwayLabs__ListPrice__c": 1,'
         + ' "OrdwayLabs__PricingType__c": "Per unit",'
         + ' "OrdwayLabs__StartingUnit__c": 201,'
         + ' "OrdwayLabs__Tier__c": 4'
     +  ' }'
   + ' ]';

  public static final String chargeTier =
  ' ['
  + ' {' 
        + ' "Id":"CHARGETIER",'
        + ' "CreatedDate":"2021-03-01T14:59:53.000Z",'
        + ' "CreatedById":"0050t000006z3noAAA",'
        + ' "LastModifiedById":"0050t000006z3noAAA",'
        + ' "OrdwayLabs__Charge__c":"CHARGEID",'
        + ' "OrdwayLabs__EndingUnit__c":1,'
        + ' "OrdwayLabs__ListPrice__c":1234,'
        + ' "OrdwayLabs__PricingType__c":"Per unit",'
        + ' "OrdwayLabs__StartingUnit__c":0,'
        + ' "OrdwayLabs__Tier__c":1'
    +  ' },'
    + ' {'
        + '"CreatedDate":"2021-03-19T10:57:53.000Z",'
        + '"CreatedById":"0050t000006z3noAAA",'
        + '"LastModifiedById":"0050t000006z3noAAA",'
        + '"OrdwayLabs__Charge__c":"CHARGEID",'
        + '"OrdwayLabs__EndingUnit__c":2,'
        + '"OrdwayLabs__ListPrice__c":20,'
        + '"OrdwayLabs__PricingType__c":"Per unit",'
        + '"OrdwayLabs__StartingUnit__c":1,'
        + '"OrdwayLabs__Tier__c":2'
      +  '} '
      + '  ]';

  public static final String chargeTierToDelete =
  ' ['
  + ' { ' 
        + '"Id":"CHARGETIERTODELETE",'
        +  '"Name":"CT-00071",'
        +  '"CreatedDate":"2021-03-20T01:35:44.000Z",'
        +  '"CreatedById":"0050t000006z3noAAA",'
        +  '"LastModifiedById":"0050t000006z3noAAA",'
        +  '"OrdwayLabs__Charge__c":"CHARGEID",'
        +  '"OrdwayLabs__ListPrice__c":5,'
        +  '"OrdwayLabs__PricingType__c":"Per unit",'
        +  '"OrdwayLabs__StartingUnit__c":6,'
        +  '"OrdwayLabs__Tier__c":7'
      +   '} '
      + '    ]';

  public static final String ordwayPlanPickerMDTStringOLI =
  +' [ '
  +' { '
      +' "MasterLabel": "Ordway Charge Name", '
      +' "Edit": false, '
      +' "Field": "OrdwayLabs__ChargeName__c", '
      +' "Visible": true, '
      +' "ColumnOrder": 1, '
      +' "isFormulaField": false, '
      +' "isNew": false '
  +' }, '
  +' { '
      +' "MasterLabel": "Billing Period", '
      +' "Edit": false, '
      +' "Field": "OrdwayLabs__BillingPeriod__c", '
      +' "Visible": true, '
      +' "ColumnOrder": 2, '
      +' "isFormulaField": false, '
      +' "isNew": false '
  +' } '
+' ] ';

    public static final String ordwayPlanPickerMDTStringQLI =
    +' [ '
    +' { '
        +' "MasterLabel": "Ordway Charge Name", '
        +' "Edit": false, '
        +' "Field": "OrdwayLabs__ChargeName__c", '
        +' "Visible": true, '
        +' "ColumnOrder": 1, '
        +' "isFormulaField": false, '
        +' "isNew": false '
    +' }, '
    +' { '
        +' "MasterLabel": "Billing Period", '
        +' "Edit": false, '
        +' "Field": "OrdwayLabs__BillingPeriod__c", '
        +' "Visible": true, '
        +' "ColumnOrder": 2, '
        +' "isFormulaField": false, '
        +' "isNew": false '
    +' } '
+' ] ';

    public static final String fieldMappingJson = 
     '[ '
        + '{ '
        + ' "fieldAPIName": "Name", '
        + ' "fieldDataTypeKey": "Text", '
        + ' "fieldLabel": "Ordway Product Name", '
        + ' "sourceFieldAPIName": "Name  '
        + ' } '
        + ' ] ';

  public static final String ordWayInvoiceString =
    ' {"result":[{ ' +
    ' "OrdwayLabs__ExternalID__c": "INV-00723", ' +
    ' "OrdwayLabs__CustomerID__c": "C-00105", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Invoice__c" ' +
    ' }, ' +
    ' "line_items": [ ' +
    ' { ' +
    ' "Name": "Test", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "1", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Test", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "2", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Test", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "3", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' } ' +
    ' ] ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "INV-00722", ' +
    ' "OrdwayLabs__CustomerID__c": "C-00104", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Invoice__c" ' +
    ' }, ' +
    ' "line_items": [ ' +
    ' { ' +
    ' "Name": "New Member Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "1", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "New Member Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "2", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "New Member Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "3", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' } ' +
    ' ] ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "INV-00721", ' +
    ' "OrdwayLabs__CustomerID__c": "C-00103", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Invoice__c" ' +
    ' }, ' +
    ' "line_items": [ ' +
    ' { ' +
    ' "Name": "Overage User Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "1", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Overage User Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "2", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Overage User Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "3", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Overage User Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "4", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Overage User Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "5", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Overage User Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "6", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Overage User Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "7", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Overage User Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "8", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Overage User Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "9", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Annual Subscription", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "10", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Overage User Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "11", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "New Member Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "12", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "New Member Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "13", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "New Member Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "14", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "New Member Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "15", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "New Member Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "16", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Overage User Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "17", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "New Member Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "18", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "New Member Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "19", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "New Member Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "20", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Overage User Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "21", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "Name": "Overage User Fee", ' +
    ' "OrdwayLabs__InvoiceID_LineID__c": "22", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__InvoiceLineItem__c" ' +
    ' } ' +
    ' } ' +
    ' ] ' +
    ' } ],"total_count":3}';

  public static final String ordWayCreditString =
    +' { ' +
    ' "result": [ ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "CR-00025", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Credit__c" ' +
    ' }, ' +
    ' "invoices": [ ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "INV-00621", ' +
    ' "OrdwayLabs__CustomerID__c": "", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Invoice__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "INV-00620", ' +
    ' "OrdwayLabs__CustomerID__c": "", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Invoice__c" ' +
    ' } ' +
    ' } ' +
    ' ] ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "CR-00026", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Credit__c" ' +
    ' }, ' +
    ' "invoices": [ ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "INV-00720", ' +
    ' "OrdwayLabs__CustomerID__c": "", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Invoice__c" ' +
    ' } ' +
    ' } ' +
    ' ] ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "CR-00022", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Credit__c" ' +
    ' }, ' +
    ' "invoices": [] ' +
    ' } ' +
    ' ], ' +
    ' "total_count": 3 ' +
    ' } ';

  public static final String ordWayRefundString =
    ' { ' +
    ' "result": [ ' +
    ' { ' +
    ' "OrdwayLabs__PaymentID__c": "PMT-00047", ' +
    ' "OrdwayLabs__CustomerID__c": "C-00096", ' +
    ' "OrdwayLabs__ExternalID__c": "REF-00007", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Refund__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__PaymentID__c": "", ' +
    ' "OrdwayLabs__CustomerID__c": "C-00089", ' +
    ' "OrdwayLabs__ExternalID__c": "REF-00006", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Refund__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__PaymentID__c": "", ' +
    ' "OrdwayLabs__CustomerID__c": "C-00089", ' +
    ' "OrdwayLabs__ExternalID__c": "REF-00005", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Refund__c" ' +
    ' } ' +
    ' } ' +
    ' ], ' +
    ' "total_count": 3 ' +
    ' } ';

  public static final String ordWayPaymentString =
    ' { ' +
    ' "result": [ ' +
    ' { ' +
    ' "OrdwayLabs__CustomerID__c": "C-00027", ' +
    ' "OrdwayLabs__ExternalID__c": "PMT-00035", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Payment__c" ' +
    ' }, ' +
    ' "invoices": [ ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "INV-00001", ' +
    ' "OrdwayLabs__CustomerID__c": "C-00027", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Invoice__c" ' +
    ' } ' +
    ' } ' +
    ' ] ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__CustomerID__c": "C-00027", ' +
    ' "OrdwayLabs__ExternalID__c": "PMT-00033", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Payment__c" ' +
    ' }, ' +
    ' "invoices": [ ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "INV-00397", ' +
    ' "OrdwayLabs__CustomerID__c": "", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Invoice__c" ' +
    ' } ' +
    ' } ' +
    ' ] ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__CustomerID__c": "C-00093", ' +
    ' "OrdwayLabs__ExternalID__c": "PMT-00032", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Payment__c" ' +
    ' }, ' +
    ' "invoices": [] ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__CustomerID__c": "C-00002", ' +
    ' "OrdwayLabs__ExternalID__c": "PMT-00031", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Payment__c" ' +
    ' }, ' +
    ' "invoices": [ ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "INV-00024", ' +
    ' "OrdwayLabs__CustomerID__c": "", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Invoice__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "INV-00242", ' +
    ' "OrdwayLabs__CustomerID__c": "", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Invoice__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "INV-00559", ' +
    ' "OrdwayLabs__CustomerID__c": "", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Invoice__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "INV-00108", ' +
    ' "OrdwayLabs__CustomerID__c": "", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Invoice__c" ' +
    ' } ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__ExternalID__c": "INV-00049", ' +
    ' "OrdwayLabs__CustomerID__c": "", ' +
    ' "attributes": { ' +
    ' "type": "OrdwayLabs__Invoice__c" ' +
    ' } ' +
    ' } ' +
    ' ] ' +
    ' } ' +
    ' ], ' +
    ' "total_count": 4  } ';

    public static final String planString = ' { '
    +' "charges": [ '
        +' { '
            +' "attributes": { '
                +' "type": "OrdwayLabs__Charge__c" '
            +' }, '
            +' "tiers": [ '
                +' { '
                    +' "attributes": { '
                        +' "type": "OrdwayLabs__ChargeTier__c" '
                    +' }, '
                    +' "OrdwayLabs__PricingType__c": "Per unit", '
                    +' "OrdwayLabs__ListPrice__c": "50.0", '
                    +' "OrdwayLabs__EndingUnit__c": "1.0", '
                    +' "OrdwayLabs__StartingUnit__c": "0", '
                    +' "OrdwayLabs__Tier__c": 1 '
                +' }, '
                +' { '
                    +' "attributes": { '
                        +' "type": "OrdwayLabs__ChargeTier__c" '
                    +' }, '
                    +' "OrdwayLabs__PricingType__c": "Per unit", '
                    +' "OrdwayLabs__ListPrice__c": "60.0", '
                    +' "OrdwayLabs__EndingUnit__c": "2.0", '
                    +' "OrdwayLabs__StartingUnit__c": "1.0", '
                    +' "OrdwayLabs__Tier__c": 2 '
                +' }, '
                +' { '
                    +' "attributes": { '
                        +' "type": "OrdwayLabs__ChargeTier__c" '
                    +' }, '
                    +' "OrdwayLabs__PricingType__c": "Per unit", '
                    +' "OrdwayLabs__ListPrice__c": "70.0", '
                    +' "OrdwayLabs__EndingUnit__c": "", '
                    +' "OrdwayLabs__StartingUnit__c": "2.0", '
                    +' "OrdwayLabs__Tier__c": 3 '
                +' } '
            +' ], '
            +' "OrdwayLabs__Product__c": "PRODUCTID", '
            +' "OrdwayLabs__OrdwayPlan__c": "ORDWAYPLANID", '
            +' "OrdwayLabs__ProductID__c": "P-00112", '
            +' "OrdwayLabs__PricingModel__c": "Volume", '
            +' "OrdwayLabs__ListPrice__c": null, '
            +' "OrdwayLabs__ChargeType__c": "Recurring", '
            +' "OrdwayLabs__ChargeTiming__c": "In_advance", '
            +' "OrdwayLabs__ExternalID__c": "CHG-00397", '
            +' "OrdwayLabs__BillingPeriod__c": "Monthly", '
            +' "Name": "CN - AY" , '
            +' "Id": "CHARGEID" '
        +' } '
    +' ], '
    +' "attributes": { '
    +' "type": "OrdwayLabs__Plan__c" '
    +' }, '
    +' "OrdwayLabs__PlanId__c": "PLN-00269", ' 
    +' "OrdwayLabs__ExternalID__c": "PLN-00269", '
    +' "OrdwayLabs__Active__c": true, '
    +' "Name": "AY Plan on SF", '
    +' "Id": "ORDWAYPLANID" '
+' } ';

  public static final String ordWayQuoteString =
    '{' +
    ' "Id": "testQuoteId", ' +
    ' "OrdwayLabs__QuoteTemplateId__c": "", ' +
    ' "OrdwayLabs__QuoteDate__c": "2020-05-21", ' +
    ' "OrdwayLabs__ExpiryDate__c": "2020-05-20", ' +
    ' "OrdwayLabs__ServiceStartDate__c": "2020-05-20", ' +
    ' "OrdwayLabs__BillingStartDate__c": "2020-05-20", ' +
    ' "OrdwayLabs__ContractEffectiveDate__c": "2020-05-20", ' +
    ' "OrdwayLabs__ContractTerm__c": "12", ' +
    ' "OrdwayLabs__RenewalTerm__c": "12", ' +
    ' "OrdwayLabs__AutoRenew__c": true, ' +
    ' "OrdwayLabs__PaymentRequiredonQuoteAcceptance__c": false, ' +
    ' "OrdwayLabs__RecipientName__c": "NP", ' +
    ' "OrdwayLabs__RecipientEmail__c": "np@cloudiate.com", ' +
    ' "OrdwayLabs__SenderName__c": "NP", ' +
    ' "OrdwayLabs__SenderEmail__c": "np@cloudiate.com", ' +
    ' "OrdwayLabs__ExternalID__c": "Q-00006", ' +
    ' "attributes" : { '+
      ' "type" : "OrdwayLabs__Quote__c" '+
      ' }, '+
    ' "plans": [ ' +
    ' { ' +
    ' "OrdwayLabs__OrdwayChargeId__c": "SCHG-04", ' +
    ' "OrdwayLabs__ChargeType__c": "One time", ' +
    ' "OrdwayLabs__IncludedUnits__c":null, ' +
    ' "OrdwayLabs__ChargeName__c": "Implementation Pro", ' +
    ' "OrdwayLabs__OrdwayEffectivePrice__c": "0.00", ' +
    ' "OrdwayLabs__OrdwayListPrice__c": "0.00", ' +
    ' "OrdwayLabs__OrdwayPlanId__c": "PLN-00020", ' +
    ' "OrdwayLabs__OrdwayProductId__c": "P-00001", ' +
    ' "OrdwayLabs__PricingModel__c": "Per Unit", ' +
    ' "OrdwayLabs__ProductName__c": "Implementation", ' +
    ' "OrdwayLabs__UnitofMeasure__c":null, ' +
    ' "OrdwayLabs__Quantity__c": 10 ' +
    ' }, ' +
    ' { ' +
    ' "OrdwayLabs__OrdwayChargeId__c": "SCHG-01", ' +
    ' "OrdwayLabs__ChargeType__c": "One time", ' +
    ' "OrdwayLabs__IncludedUnits__c":null, ' +
    ' "OrdwayLabs__ChargeName__c": "Implementation", ' +
    ' "OrdwayLabs__OrdwayEffectivePrice__c": "0.00", ' +
    ' "OrdwayLabs__OrdwayListPrice__c": "0.00", ' +
    ' "OrdwayLabs__OrdwayPlanId__c": "PLN-00001", ' +
    ' "OrdwayLabs__OrdwayProductId__c": "P-00001", ' +
    ' "OrdwayLabs__PricingModel__c": "Per Unit", ' +
    ' "OrdwayLabs__ProductName__c": "Implementation", ' +
    ' "OrdwayLabs__UnitofMeasure__c":null, ' +
    ' "OrdwayLabs__Quantity__c": 10 ' +
    ' } ' +
    '], ' +
    '"customer": { ' +
    '"Name": "NP Test Account", ' +
    '"Website": null ' +
    ' } ' +
    '}';
}