/**
 * @File Name          : ErrorLogController.cls
 * @Description        : Error Log Controller
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 12-28-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    10/8/2019   Ordway Labs     Initial Version
 **/
public with sharing class ErrorLogController {
  /**
   * @description Method to get Error Logs related to specific record
   * @param recordId Record Id whose Error log needs to be retrieved
   * @param limitValue Number of Error Log to be retrieved
   * @return List<ApplicationLog__c> List of Application Logs related to the specific record
   **/
  @AuraEnabled
  public static List<ApplicationLog__c> getErrorLog(
    String recordId,
    Integer limitValue
  ) {
    if (Schema.sObjectType.ApplicationLog__c.isAccessible()) {
      String Query = 'SELECT Id, Name, Age__c, HasError__c, InitialClass__c, InitialFunction__c, UserId__c, CreatedDate FROM ApplicationLog__c WHERE RecordId__c =: recordId ORDER BY CreatedDate DESC ';
      return DATABASE.query(
        limitValue == null ? Query : Query + 'limit ' + limitValue
      );
    }
    return null;
  }
}