/**
 * @File Name          : PlanTriggerHandler.cls
 * @Description        : Ordway Plan Trigger Handler
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 04-22-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    3/7/2020   Ordway Labs     Initial Version
 **/
public with sharing class PlanTriggerHandler extends TriggerHandler.TriggerHandlerBase {

  public override void beforeInsert(List<SObject> lstNewSObjs) {

    OrdwayEntityHelper.getEntities();

    for (Plan__c plan : (List<Plan__c>) lstNewSObjs) {
      PlanService.populateOrdwayEntity(plan); //Populate Ordway Entity
      PlanService.populateEntity(plan); //Populate Entity Id & Entity Name
      PlanService.populatePlanId(plan); //Populate Plan Id
    }    
  }

  public override void beforeUpdate(Map<Id, SObject> mapOldSObjs, Map<Id, SObject> mapNewSObjs) {
    
    OrdwayEntityHelper.getEntities();
    
    for (Plan__c plan : (List<Plan__c>) mapNewSObjs.values()) {
      PlanService.populateEntity(plan); //Populate Entity Id & Entity Name
      PlanService.populatePlanId(plan); //Populate Plan Id
    }
  }
}