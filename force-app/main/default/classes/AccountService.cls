/**
 * @description       : Service class for Account Object
 * @author            : Ordway Labs
 * @last modified on  : 10-26-2020
 * @last modified by  : 
 * Modifications Log
 * Ver   Date         Author        Modification
 * 1.0   08-22-2020   Ordway Labs   Initial Version
 **/
public with sharing class AccountService {
  /**
   * @description Method to get Map of Account records
   * @param recordIds Set of Account Ids
   * @return Map<Id, Account> Map of Account records
   **/
  public static Map<Id, Account> getAccount(Set<Id> recordIds) {
    Map<Id, Account> accountMap = new Map<Id, Account>();

    if (Schema.sObjectType.Account.isAccessible() && !recordIds.isEmpty()) {
      accountMap = new Map<Id, Account>(
        [
          SELECT Id, EntityID__c, EntityName__c
          FROM Account
          WHERE Id IN :recordIds
        ]
      );
    }
    return accountMap;
  }

  public static List<Account> getAccountListToSync(Map<Id, Account> accountNewMap){

    List<Account> accountListToSync = new List<Account>();

    for(Account thisAccount : accountNewMap.values()){

        if(thisAccount.OrdwayCustomerId__c != null
          && OpportunitySettingController.isAutoSyncAccountToOrdway()
          && ((OpportunitySettingController.isMultiEntityEnabled() 
            && thisAccount.EntityID__c != null)
            || !OpportunitySettingController.isMultiEntityEnabled())){
                accountListToSync.add(accountNewMap.get(thisAccount.Id));
                break; //Sync only the first account that meets the criteria
        }
    }
    return accountListToSync;
  }

  public static void enqueueAccountSyncQueueable(Map<Id, Account> accountMap){
    AccountSync_Queueable accountQueueable = new AccountSync_Queueable(accountMap);
    system.enqueueJob(accountQueueable);
  }
}