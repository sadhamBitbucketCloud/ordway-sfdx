/**
 * @File Name          : PriceBookEntryController.cls
 * @Description        : PriceBook Entry Controller
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/24/2020        Initial Version
 **/
public with sharing class PriceBookEntryController {
  /**
   * @description Method to get Pricebook entry map
   * @param pricebookId Price book Id
   * @param currencyISOCode Currency ISO Code
   * @return Map<String, PricebookEntry> Pricebook Entry Map
   **/
  public static Map<String, PricebookEntry> getPricebookEntryMap(
    String pricebookId,
    String currencyISOCode
  ) {
    Map<String, PricebookEntry> pricebookEntryMap = new Map<String, PricebookEntry>();

    Boolean isTrue = true;
    String queryString;

    if (Schema.sObjectType.PricebookEntry.isAccessible()) {
      //If multi-Currency is not enabled, then query all records
      if (!UserInfo.isMultiCurrencyOrganization()) {
        queryString =
          'SELECT ' +
          String.join(
            SObjectHelper.getAllFields(Schema.PricebookEntry.getSObjectType()),
            ','
          ) +
          ' FROM PricebookEntry WHERE Pricebook2Id=: PricebookId AND Product2.IsActive =: isTrue AND IsActive =: isTrue AND Pricebook2.IsActive =: isTrue';
      } else {
        //Use the currency ISO code from sObject record
        queryString =
          'SELECT ' +
          String.join(
            SObjectHelper.getAllFields(Schema.PricebookEntry.getSObjectType()),
            ','
          ) +
          ' FROM PricebookEntry WHERE Pricebook2Id=: PricebookId AND Product2.IsActive =: isTrue AND IsActive =: isTrue AND Pricebook2.IsActive =: isTrue AND CurrencyISOCode =: currencyISOCode';
      }

      for (PricebookEntry thisPricebookEntry : Database.query(queryString)) {
        if (!pricebookEntryMap.containsKey(thisPricebookEntry.product2Id)) {
          pricebookEntryMap.put(
            String.valueOf((Id) thisPricebookEntry.product2Id),
            thisPricebookEntry
          );
        }
      }
    }
    return pricebookEntryMap;
  }
}