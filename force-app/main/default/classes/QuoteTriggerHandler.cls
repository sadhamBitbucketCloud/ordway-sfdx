/**
 * @File Name          : QuoteTriggerHandler.cls
 * @Description        : Ordway Quote Trigger Handler
 * @Author             : 
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 08-30-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/16/2020        Initial Version
 **/
public class QuoteTriggerHandler extends TriggerHandler.TriggerHandlerBase {
  
  static Set<Id> recordIds;
  static Integer recursionCount = 1;
  //To recursively execute the code in the same transaction, set this variable to true
  //Set it to false on after update
  public static Boolean runOnce = true;
  //Quote Id which are being updated due to Opportunity Ordway To Quote Sync Process so that we can avoid recursion while sync
  public static Set<Id> syncedQuoteIds = new Set<Id>();
  public static Boolean isMultiCurrencyOrg = SObjectHelper.isMultiCurrencyOrg();

  public override void beforeInsert(List<SObject> lstNewSObjs) {

    Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
    
    opportunityMap = OpportunityService.getOpportunityMap(
      SObjectHelper.getKeyValues(lstNewSObjs, Schema.Quote__c.Opportunity__c)
    );

    for (Quote__c thisQuote : (List<Quote__c>) lstNewSObjs) {

      OrdwayQuoteService.populateDefaultQuoteTemplate(thisQuote);
      OrdwayQuoteService.defaultRenewalTerm(thisQuote);

      if (opportunityMap.containsKey(thisQuote.Opportunity__c)) {
        OrdwayQuoteService.populateOpportunityValuesToQuote(
          thisQuote,
          opportunityMap.get(thisQuote.Opportunity__c)
        );
      }
    }
  }

  public override void afterInsert(Map<Id, SObject> mapNewSObjs) {
    OrdwayQuoteLineItemService.createQLIForNewQuote(
      SobjectHelper.getMapValues( mapNewSObjs.values(), Schema.Quote__c.Opportunity__c, Schema.Quote__c.Id )
    );
  }

  public override void beforeUpdate( Map<Id, SObject> mapOldSObjs, Map<Id, SObject> mapNewSObjs ) {
    
    Map<Id, Quote__c> oldQuoteMap = (Map<Id, Quote__c>) mapOldSObjs;
    Map<Id, Quote__c> newMap = (Map<Id, Quote__c>) mapNewSObjs;

    for (Quote__c thisQuote : newMap.values()) {
      if ( SObjectHelper.isChanged( Schema.Quote__c.QuoteTemplate__c, thisQuote, (Map<Id, Quote__c>) mapOldSObjs ) ) {
        thisQuote.PreviewURL__c = null;
      }

      if (
        isMultiCurrencyOrg &&
        OrdwayQuoteService.isCurrencyChanged(thisQuote, oldQuoteMap) &&
        thisQuote.PriceBook2Id__c != null
      ) {
        thisQuote.PriceBook2Id__c = null;
      }

      if (
        SObjectHelper.isChanged( Schema.Quote__c.Status__c, thisQuote, (Map<Id, Quote__c>) mapOldSObjs )
      ) {
        //If Status Changes, Then Set Sync Status To In Sync
        thisQuote.SyncStatus__c = OrdwayHelper.SYNC_STATE_IN_SYNC;
      }

      OrdwayQuoteService.populateQuoteId(thisQuote);
      OrdwayQuoteService.defaultRenewalTerm(thisQuote);
    }
  }

  public override void afterUpdate( Map<Id, SObject> mapOldSObjs, Map<Id, SObject> mapNewSObjs ) {
    
    Set<Id> completedQuoteIds = new Set<Id>();
    Set<Id> opportunityIdsTobeSynced = new Set<Id>();
    Set<Id> qliSetToDelete = new Set<Id>();

    Map<Id, Quote__c> oldQuoteMap = (Map<Id, Quote__c>) mapOldSObjs;
    Map<Id, Quote__c> newMap = (Map<Id, Quote__c>) mapNewSObjs;

    for (Quote__c thisQuote : newMap.values()) {
      if (
        SObjectHelper.isChanged( Schema.Quote__c.Status__c, thisQuote, (Map<Id, Quote__c>) mapOldSObjs ) 
        && thisQuote.Status__c == OrdwayHelper.ORDWAY_QUOTECOMPLETED
        || oldQuoteMap.get(thisQuote.Id).Status__c == OrdwayHelper.ORDWAY_QUOTECOMPLETED
      ) {
        completedQuoteIds.add(thisQuote.Id);
        opportunityIdsTobeSynced.add(thisQuote.Opportunity__c);
      }

      if (
        SObjectHelper.isChanged( Schema.Quote__c.PriceBook2Id__c, thisQuote, (Map<Id, Quote__c>) mapOldSObjs )
        || OrdwayQuoteService.hasPricebookRemoved(thisQuote, oldQuoteMap)
        || SObjectHelper.isChanged( Schema.Quote__c.EntityId__c, thisQuote, (Map<Id, Quote__c>) mapOldSObjs ) 
      ) {
        qliSetToDelete.add(thisQuote.Id);
      }
    }
    if (!qliSetToDelete.isEmpty()) {
      OrdwayQuoteLineItemService.deleteQuoteLineItem(qliSetToDelete);
    }

    if (!opportunityIdsTobeSynced.isEmpty()) {
      OrdwayQuoteService.syncQuoteToOpportunity( completedQuoteIds, newMap, opportunityIdsTobeSynced );
      OrdwayQuoteService.updateSyncing( completedQuoteIds, opportunityIdsTobeSynced );
    }
  }

  //Recursive Check
  public static Boolean executeTrigger(Map<Id, sObject> mapNewSObjs) {
    //On Insert Return True
    if (mapNewSObjs == null) {
      return true;
    } else if (
      recordIds != null && recordIds.containsAll(mapNewSObjs.keySet()) &&
      recursionCount > 2
    ) {
      //On Update - Return False For WorkFlow Field Update / Process Builder Update
      return runOnce;
    } else {
      recordIds = new Set<Id>();
      recursionCount = math.mod(recursionCount, 2) != 0 ? 1 : recursionCount;
      recursionCount++;
      recordIds.addAll(mapNewSObjs.keySet());
      return true;
    }
  }
}