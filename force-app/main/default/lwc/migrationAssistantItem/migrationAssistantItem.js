import { LightningElement, api } from 'lwc';

export default class MigrationAssistantItem extends LightningElement {
  ENABLE_BUTTON_LABEL   = "Enable";

  @api featureUniqueKey;
  @api featureTitle;
  @api featureBadgeLabel;
  @api featureBadgeHelpText;
  @api featureDescription;
  @api featureType;
  @api isFeatureEnabled;

  handleEnableFeature(){
    this.dispatchEvent(new CustomEvent('enablefeature', { detail: this.featureUniqueKey}));
  }
}