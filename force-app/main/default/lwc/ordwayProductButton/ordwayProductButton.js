import { LightningElement, api, track, wire } from 'lwc';
import syncProductToOrdway from '@salesforce/apex/OrdwayProductController.syncToOrdway';
import getInitialWrapper from '@salesforce/apex/OrdwayProductController.getInitialWrapper';
import ORDWAY_ICON from '@salesforce/resourceUrl/OrdwayIcon';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

import hasSyncToOrdwayPermission from '@salesforce/customPermission/SynctoOrdway';
import hasViewInOrdwayPermission from '@salesforce/customPermission/ViewinOrdway';
export default class SyncToOrdway extends NavigationMixin(LightningElement) {
  @api recordId;
  ordwayIcon = ORDWAY_ICON;
  showSyncInProgress;
  @track ordwayProduct;
  namedCredential;
  externalId;
  showRefresh = false;
  entityId;
  isMultiEntityEnabled;
  namedCredential;
  ordwayProduct;

  connectedCallback(){
    this.getOrdwayProduct();
  }

  getOrdwayProduct(){
    this.showSpinner = true;
    getInitialWrapper({ ordwayProductId : this.recordId}).then(
      result => {
        this.ordwayProduct = result.thisOrdwayProduct;
        this.externalId = this.ordwayProduct.OrdwayLabs__OrdwayProductID__c;
        this.entityId = this.ordwayProduct.OrdwayLabs__OrdwayEntity__c;
        this.isMultiEntityEnabled = result.isMultiEntityEnabled;
        this.namedCredential = result.namedCredentials;
        this.showSpinner = false;
      }
    ).catch(error => {
      this.showSpinner = false;
      this.handleError(error.message);
    });
  }

  get disableSyncToOrdway() {
    return !hasSyncToOrdwayPermission;
  }

  get disableViewInOrdwayButton() {
    if(this.ordwayProduct != undefined && this.ordwayProduct != null && this.externalId && hasViewInOrdwayPermission){
      return false;
    }
    else{
      return true;
    }
  }

  handleSync(event){
    if(this.isMultiEntityEnabled && !this.entityId){
      this.handleError('Please provide entity before syncing');
    }
    else{
      this.showSyncInProgress = true;
      syncProductToOrdway({ recordId : this.recordId}).then(
        result => {
          this.handleSuccess('Ordway Product was synced');
          this.showSyncInProgress = false;
          this.showRefresh = true;
        }
      ).catch(error => {
        this.showSyncInProgress = false;
        this.handleError(error.body.message);
      });
    }
  }

  handleView(){
    var URL = this.namedCredential+'/products/'+this.externalId;
    this[NavigationMixin.Navigate]({
        "type": "standard__webPage",
        "attributes": {
            "url": URL
        }
    });
  }

  handleRefresh(){
    location.reload();
  }


  handleError(error) {
    const evt = new ShowToastEvent({
      title: '',
      message: error,
      variant: 'error',
      mode: 'sticky'
  });
  this.dispatchEvent(evt);
  }

  handleSuccess(message) {
    const evt = new ShowToastEvent({
      title: '',
      message: message,
      variant: 'success',
      mode: 'dismissable'
  });
  this.dispatchEvent(evt);
  }
}