import { LightningElement, track, wire , api} from 'lwc';
import getEntitySetting from '@salesforce/apex/OrdwayEntityController.getOrdwayEntitySetting';
import saveEntitySetting from '@salesforce/apex/OrdwayEntityController.saveOrdwayEntitySetting';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class ordwayEntitySetting extends LightningElement {
  ORDWAY_ENTITY_ID  = 'ORDWAYENTITYID';
  ID_Var = 1;
  deletedEntityIds = [];
  @track isMultiEntityEnabled;
  @track ordwayEntitySettingRecords = [];
  @track ordwayEntitySettingRecordsToDelete = [];
  @track showSpinner = true;
  @track lightningCardName ='Ordway Entities';

  connectedCallback() {
   this.getOrdwayEntitySetting();
  }

  @api handleTabChangeEvent(){
    this.getOrdwayEntitySetting();
    if(this.isMultiEntityEnabled){
      this.template.querySelector("c-ordway-entity-rule-configuration").handleTabChangeEvent(this.deletedEntityId);
    }
  } 

  getOrdwayEntitySetting(){
    this.showSpinner = true
    this.ordwayEntitySettingRecordsToDelete = [];
    getEntitySetting().then(
      result => {
        this.isMultiEntityEnabled = result.isMultiEntityEnabled;
        if(this.isMultiEntityEnabled){
          this.ordwayEntitySettingRecords = result.ordwayEntities;
        }
        this.showSpinner = false;
      }
    ).catch(error => {
      this.handleError(error.body.message);
    });
  }

  handleValidateInput(){
    const isAllValid = [...this.template.querySelectorAll('.entityinputField')].reduce((validSoFar, inputCmp) => {
      inputCmp.reportValidity(); 
      return validSoFar && inputCmp.checkValidity();
    }, true);

    var valid = true;
    for(var i=0; i<this.ordwayEntitySettingRecords.length; i++){
      if(this.ordwayEntitySettingRecords[i].OrdwayLabs__Default__c){
        valid = true;
        break;
      }
      else{
        valid = false;
      }
    }

    if(!valid){
      this.handleError('Please select a parent entity');
    }

    if(isAllValid && valid){
      // for(var i=0; i<this.ordwayEntitySettingRecordsToDelete.length; i++){
      //   if(this.ordwayEntitySettingRecordsToDelete.Id == undefined || this.ordwayEntitySettingRecordsToDelete.Id == null){
      //     this.ordwayEntitySettingRecordsToDelete.splice(i, 1);
      //   }
      // }

      for(var i=0; i<this.ordwayEntitySettingRecordsToDelete.length; i++){
        if(this.ordwayEntitySettingRecordsToDelete[i].Id.includes(this.ORDWAY_ENTITY_ID)){
          this.ordwayEntitySettingRecordsToDelete.splice(i, 1);
        }
        else{
          this.deletedEntityIds.push(this.ordwayEntitySettingRecordsToDelete[i].OrdwayLabs__EntityID__c);
        }
      }

      for(var i=0; i<this.ordwayEntitySettingRecords.length; i++){
        if(this.ordwayEntitySettingRecords[i].Id.includes(this.ORDWAY_ENTITY_ID)){
          delete this.ordwayEntitySettingRecords[i].Id;
        }
      }

     this.handleSaveSetting();
    }
  }

  handleDelete(event){
    this.ordwayEntitySettingRecordsToDelete.push(this.ordwayEntitySettingRecords[event.target.getAttribute("data-indexvar")]);
    this.ordwayEntitySettingRecords.splice(event.target.getAttribute("data-indexvar"),1);
  }

  handleNewEntity(){
    var newObject = {Id: this.ORDWAY_ENTITY_ID+this.ID_Var};
    this.ID_Var = this.ID_Var + 1;
    this.ordwayEntitySettingRecords.push(newObject);
  }
  
  handleInputValueChange(event){
    this.setDataValue(event.target.getAttribute("data-indexvar"), event.target.getAttribute("data-fieldname"), event.target.value);
  }

  handleCheckboxChange(event){
    this.setDataValue(event.target.getAttribute("data-indexvar"), event.target.getAttribute("data-fieldname"), event.target.checked);
    if(event.target.checked){
      this.ordwayEntitySettingRecords.forEach((thisEntity, i) => {
        if(i != event.target.getAttribute("data-indexvar")){
          this.ordwayEntitySettingRecords[i][event.target.getAttribute("data-fieldname")] = false;
        }
      });
    }
  }

  setDataValue(index, fieldName, fieldValue){
    this.ordwayEntitySettingRecords[index][fieldName] = fieldValue;
  }
  
  handleSaveSetting(){
    this.showSpinner = true;
    var settingToUpsert = JSON.stringify(this.ordwayEntitySettingRecords);
    var settingToDelete = this.ordwayEntitySettingRecordsToDelete.length > 0 ? JSON.stringify(this.ordwayEntitySettingRecordsToDelete) : null;

    saveEntitySetting({ OrdwayEntityToUpsert: settingToUpsert, OrdwayEntityToDelete: settingToDelete}).then(
      result => {
        this.getOrdwayEntitySetting();
        this.handleSuccess('Ordway entity setting updated successfully');
        this.showSpinner = false;
        this.template.querySelector("c-ordway-entity-rule-configuration").handleTabChangeEvent(this.deletedEntityId);
      }
    ).catch(error => {
      this.handleError(error.body.message);
    });
  }

  handleError(error) {
    this.showSpinner = false;
      const event = new ShowToastEvent({
          title: 'Error',
          message: error,
          variant: 'error',
          mode: 'dismissable'
      });
    this.dispatchEvent(event);
  }

  handleSuccess(successMessage) {
    this.showSpinner = false;
      const event = new ShowToastEvent({
          title: 'Success',
          message: successMessage,
          variant: 'success',
          mode: 'dismissable'
      });
    this.dispatchEvent(event);
  }
}