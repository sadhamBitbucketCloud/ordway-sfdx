import { LightningElement, api } from 'lwc';

export default class FieldDiffRow extends LightningElement {

  item;

  @api fieldData;

  @api
  get rowData() {
    return this.item;
  }

  // Compare two items
  compare = function (sourceValue, targetValue) {

    // Get the object type
    var itemType = Object.prototype.toString.call(sourceValue);

    // If an object or array, compare recursively
    if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
      if (!this.isEqual(sourceValue, targetValue)) return false;
    }

    // Otherwise, do a simple comparison
    else {

      // If the two items are not the same type, return false
      if (itemType !== Object.prototype.toString.call(targetValue)) return false;

      // If it's a function, convert to a string and compare
      // Otherwise, just compare
      if (itemType === '[object Function]') {
        if (sourceValue.toString() !== targetValue.toString()) return false;
      } else {
        var sourceVal = parseFloat(sourceValue);
        var targetVal = parseFloat(targetValue);

        if (isNaN(sourceVal) || isNaN(targetVal)) {
          if (sourceValue !== targetValue) return false;
        }
        else {
          if (sourceVal !== targetVal) return false;
        }
      }
    }
  };

  isEqual = function (value, other) {
  
    // Get the value type
    var type = Object.prototype.toString.call(value);
  
    // If the two objects are not the same type, return false
    if (type !== Object.prototype.toString.call(other)) return false;

    // If items are not an object or array, return false
	  if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;
    
    // Compare the length of the length of the two items
    var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
    var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
    if (valueLen !== otherLen) return false;
    
    // Compare properties
    if (type === '[object Array]') {
      for (var i = 0; i < valueLen; i++) {
        if (this.compare(value[i], other[i]) === false) return false;
      }
    } else {
      for (var key in value) {
        if (value.hasOwnProperty(key)) {
          if (this.compare(value[key], other[key]) === false) return false;
        }
      }
    }
  
    // If nothing failed, return true
    return true;
  };

  set rowData(value) {
   
    this.item = JSON.parse(JSON.stringify(value));
    this.item.hasSameValue = false;
    
    if(this.item.fieldValueSalesforce === undefined){
      this.item.hasSameValue = true;
    }
    else if (this.item.fieldAPIName === 'OrdwayLabs__TierJSON__c') {
      if (this.item.fieldValueSalesforce === undefined || this.item.fieldValueOrdway === undefined
      || this.item.fieldValueSalesforce === 'N/A' || this.item.fieldValueOrdway === 'N/A') {
        this.item.hasSameValue = false;
      }
      else {
        this.item.hasSameValue = this.isEqual(JSON.parse(this.item.fieldValueSalesforce), JSON.parse(this.item.fieldValueOrdway)); 
      }
    }
    else if(!isNaN(Date.parse(this.item.fieldValueSalesforce))
            && !isNaN(Date.parse(this.item.fieldValueOrdway))){
            var thisSalesforceDate = new Date (this.item.fieldValueSalesforce);
            var thisOrdwayDate = new Date (this.item.fieldValueOrdway);
            if(thisSalesforceDate.getFullYear() === thisOrdwayDate.getFullYear() 
                && thisSalesforceDate.getMonth() === thisOrdwayDate.getMonth() 
                && thisSalesforceDate.getDate() === thisOrdwayDate.getDate()) {
                  this.item.hasSameValue = true;
            }
    }
    else if(isNaN(parseInt(this.item.fieldValueSalesforce))
            && this.item.fieldValueSalesforce.toUpperCase() === this.item.fieldValueOrdway.toUpperCase()){
      this.item.hasSameValue = true;
    }
    else if(!isNaN(Number(this.item.fieldValueSalesforce))
         && parseFloat(this.item.fieldValueSalesforce) === parseFloat(this.item.fieldValueOrdway)){
          this.item.hasSameValue = true;
    }
    else if(this.item.fieldValueSalesforce instanceof Date 
        && this.item.fieldValueOrdway instanceof Date 
        && this.item.fieldValueSalesforce.getFullYear() === this.item.fieldValueOrdway.getFullYear() 
        && this.item.fieldValueSalesforce.getMonth() === this.item.fieldValueOrdway.getMonth() 
        && this.item.fieldValueSalesforce.getDate() === this.item.fieldValueOrdway.getDate()) {
          this.item.hasSameValue = false;
    }
    else if(this.item.fieldValueSalesforce === this.item.fieldValueOrdway){
      this.item.hasSameValue = true;
    }
  }
}