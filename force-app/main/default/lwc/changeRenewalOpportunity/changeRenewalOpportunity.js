import { LightningElement, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { NavigationMixin } from 'lightning/navigation';
import getOpenOpportunityList from '@salesforce/apex/ChangeRenewalOpportunity.getOpenOpportunities';
import getOpportunityObjectInstance from '@salesforce/apex/ChangeRenewalOpportunity.getOpportunity';



export default class ChangeRenewalOpportunity extends NavigationMixin(LightningElement) {
  ordwayContractId;
  ordwayContractName;
  ordwayContractURL;
  addPlanURL;
  recordIdToPass;
  renewalOpenOpportunityOptions;
  changeOpenOpportunityOptions;
  renewalLayoutSections;
  changeLayoutSections;
  isAutoRenew;
  isEverGreen;
  isAutoRenewError;
  opportunityRecord;

  CONST_OPPORTUNITY_OBJECT = 'Opportunity';
  CONST_OPPORTUNITY_RENEWAL = 'Renewal';
  CONST_OPPORTUNITY_CHANGE = 'Change';

  @track showSpinner = true;
  @track showRadioOption;
  @track showRecordForm;
  @track changeRenewalSelectedValue;
  @track selectedOpenOpportunity;
  @track openOpportunityList;
  @track opportunityObject;
  @track opportunityObjectId;
  @track objectApiName = this.CONST_OPPORTUNITY_OBJECT;
  @track openOpportunityOptions = [];
  @track showExistingOpenOpp;
  @track selectedOpportunityType;
  @track layoutSections;

  //Custom Message Variables
  @track variant;
  @track message;
  @track messageTitle;
  @track showMessageNotification = false;

  //Custom Message Methods
  get mainDivClass() {
    return 'slds-notify slds-notify_toast slds-theme_' + this.variant;
  }

  get messageDivClass() {
    return 'slds-icon_container slds-icon-utility-' + this.variant + ' slds-icon-utility-success slds-m-right_small slds-no-flex slds-align-top';
  }

  get iconName() {
    return 'utility:' + this.variant;
  }

  get selectedOpportunityType() {
    return this.changeRenewalSelectedValue == this.CONST_OPPORTUNITY_RENEWAL ? this.CONST_OPPORTUNITY_RENEWAL : this.CONST_OPPORTUNITY_CHANGE;
  }

  get selectOptionPlaceHolder() {
    var placeHolder = 'Select Open ';
    return placeHolder.concat(this.selectedOpportunityType);
  }


  //Change Renewal Opportunity Radio Options
  get changeRenewalOptions() {
    return [{
      label: 'Create Change Opportunity',
      value: this.CONST_OPPORTUNITY_CHANGE
    },
    {
      label: 'Create Renewal Opportunity',
      value: this.CONST_OPPORTUNITY_RENEWAL
    },
    ];
  }

  get showContinue() {
    return this.changeRenewalSelectedValue != null;
  }

  get isContinueDisabled() {
    return this.selectedOpenOpportunity == null;
  }

  @wire(CurrentPageReference)
  setCurrentPageReference(currentPageReference) {
    this.ordwayContractId    = currentPageReference.state.c__OrdwayContractID;
    this.isAutoRenew         = currentPageReference.state.c__IsAutoRenew;
    this.ordwayContractName = currentPageReference.state.c__OrdwayContractName;
    this.isEverGreen         = currentPageReference.state.c__IsEverGreen;
  }

  connectedCallback() {
    let ordwayContractURLPageRef = {
      type: 'standard__recordPage',
      attributes: {
        "recordId": this.ordwayContractId,
        "actionName": "view"
      }
    };

    this[NavigationMixin.GenerateUrl](ordwayContractURLPageRef)
      .then(url => this.ordwayContractURL = url);
    this.showSpinner = false;
    this.showRadioOption = true;
  }

  handleOpportunityTypeChange(event) {
    this.changeRenewalSelectedValue = event.target.value;
    this.selectedOpenOpportunity = null;
    this.selectedOpportunityType = this.changeRenewalSelectedValue == this.CONST_OPPORTUNITY_RENEWAL ? 'renewal opportunity' : 'change opportunity';
    this.assignOpenOpportunityList();
  }

  handleOpenOpportunitySelect(event) {
    this.selectedOpenOpportunity = event.target.value;
  }

  setshowExistingOpenOppBoolean() {
    this.showExistingOpenOpp = (this.openOpportunityOptions != null && this.openOpportunityOptions.length > 0) ? true : false;
  }

  assignOpenOpportunityList() {
    if (this.changeRenewalSelectedValue == this.CONST_OPPORTUNITY_RENEWAL) {
      if (this.renewalOpenOpportunityOptions == null) {
        this.showSpinner = true;
        this.queryOpenOpportunities(true);
      } else {
        this.openOpportunityOptions = this.renewalOpenOpportunityOptions;
      }
    } else {
      if (this.changeOpenOpportunityOptions == null) {
        this.showSpinner = true;
        this.queryOpenOpportunities(false);
      } else {
        this.openOpportunityOptions = this.changeOpenOpportunityOptions;
      }
    }
    this.setshowExistingOpenOppBoolean();
  }

  queryOpenOpportunities(isRenewalOption) {
    getOpenOpportunityList({
      contractId: this.ordwayContractId,
      isRenewal: isRenewalOption
    }).then(
      result => {
        var thisOpportunityResultList = result;
        var thisOpportunityList = [];
        for (var i = 0; i < result.length; i++) {
          var thisOpportunity = {};
          thisOpportunity["label"] = result[i].Name;
          thisOpportunity["value"] = result[i].Id;
          thisOpportunityList.push(thisOpportunity);
        }
        thisOpportunityList.sort((a, b) => a["label"] > b["label"] ? 1 : -1);
        this.openOpportunityOptions = thisOpportunityList;

        if (isRenewalOption) {
          this.renewalOpenOpportunityOptions = thisOpportunityList;
        } else {
          this.changeOpenOpportunityOptions = thisOpportunityList;
        }
        this.showSpinner = false;
        this.setshowExistingOpenOppBoolean();
      }
    ).catch(error => {
      this.error = error;
      this.handleError(error);
    });

  }

  handleCancel() {
    this.showSpinner = true;
    this.redirectToContract();
  }

  autoRenewHandler() {
    this.isAutoRenewError = true;
    if(this.isEverGreen){
      this.handleError('This is an evergreen contract');
    }
  }

  handleRecordFormCancel() {
    this.showRecordForm = false;
    this.showRadioOption = true;
    this.recordIdToPass = null;
    this.selectedOpenOpportunity = null;
  }

  navigateToOppViewPage() {
    this[NavigationMixin.Navigate]({
        type: 'standard__recordPage',
        attributes: {
            recordId: this.recordIdToPass,
            objectApiName: 'Opportunity',
            actionName: 'view'
        },
    });
  }

  navigateToNewOppPage() {
    this[NavigationMixin.Navigate]({
        type: 'standard__objectPage',
        attributes: {
            objectApiName: 'Opportunity',
            actionName: 'new'
        },
    });
  }

  handleContinue() {
    if (this.changeRenewalSelectedValue == this.CONST_OPPORTUNITY_RENEWAL 
      && (this.isAutoRenew == true || this.isEverGreen == true)) {
      this.autoRenewHandler();
    }
    else {
      if (this.changeRenewalSelectedValue != null &&
        this.selectedOpenOpportunity != null) {
        this.recordIdToPass = this.selectedOpenOpportunity;
      } else if (this.changeRenewalSelectedValue != null &&
        this.selectedOpenOpportunity == null &&
        this.ordwayContractId != null) {
        this.recordIdToPass = this.ordwayContractId;
      }
      this.navigateToOppViewPage();
    }
  }

  handleCreateNewContinue() {
    if (this.changeRenewalSelectedValue == this.CONST_OPPORTUNITY_RENEWAL 
      && this.isEverGreen == true ) {
      this.autoRenewHandler();
    }
    else {
        this.getOpportunityRecordDetails(); 
    }
  }

  getOpportunityRecordDetails() {
    getOpportunityObjectInstance({
      recordId: this.ordwayContractId,
      opportunityType: this.changeRenewalSelectedValue
    }).then(
      result => {
        this.opportunityRecord = result;
        this.opportunityObjectId = this.opportunityRecord.Id;
        this.populateOpportunityFieldValues();
      }
    ).catch(error => {
      this.error = error;
      this.handleError(error);
    });
}

  populateOpportunityFieldValues(){

    var urlToOpportunity = '/lightning/o/Opportunity/new?defaultFieldValues=Name=';
     var Name = this.opportunityRecord.Name;
    if(this.changeRenewalSelectedValue == this.CONST_OPPORTUNITY_RENEWAL){
       var Name = Name + ' Renawal';
       urlToOpportunity = urlToOpportunity+Name+',';
       urlToOpportunity = urlToOpportunity +'OrdwayLabs__OrdwayOpportunityType__c=Renewal,'
     }
     else if(this.changeRenewalSelectedValue == this.CONST_OPPORTUNITY_CHANGE){
       var Name = Name + ' Change';
       urlToOpportunity = urlToOpportunity+Name+',';
     urlToOpportunity = urlToOpportunity +'OrdwayLabs__OrdwayOpportunityType__c=Upsell/Cross-sell,'
     }
     urlToOpportunity = urlToOpportunity + 'OrdwayLabs__OrdwayContract__c='+this.ordwayContractId+',';

    for (var key in this.opportunityRecord) {
      if(!key.includes('IsDeleted') && !key.includes('IsPrivate')){
        urlToOpportunity = urlToOpportunity + key + '=' + this.opportunityRecord[key] + ',';
      }
    }
    urlToOpportunity = urlToOpportunity.slice(0, -1);
    this.navigateToCreateOpportunity(urlToOpportunity);
    }  

  navigateToCreateOpportunity(urlToOpportunity){
    this[NavigationMixin.Navigate]({
      "type": "standard__webPage",
      "attributes": {
          "url": urlToOpportunity
      }
    });
  }

  redirectToContract() {
    if (this.isAutoRenewError) {
      this.showMessageNotification = false;
      this.isAutoRenewError = false;
      this.variant = null;
      this.messageTitle = null;
      this.message = null;
    }
    else {
      window.location.replace(this.ordwayContractURL);
    }
  }

  handleError(errorMessage) {
    this.showSpinner = false;
    this.variant = 'error';
    this.messageTitle = 'Error!';
    this.message = errorMessage;
    this.showMessageNotification = true;
  }

  handleSuccess(message) {
    this.showSpinner = false;
    this.variant = 'success';
    this.messageTitle = 'Success!';
    this.message = message;
    this.showMessageNotification = true;
  }
}