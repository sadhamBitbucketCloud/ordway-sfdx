import { LightningElement, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { NavigationMixin } from 'lightning/navigation';

import updateFromOrdway from '@salesforce/apex/OrdwayContractFieldComparisonController.updateFromOrdway';
import syncToOrdway from '@salesforce/apex/OrdwayContractFieldComparisonController.syncToOrdway';
import getOrdwayContract from '@salesforce/apex/OrdwayContractFieldComparisonController.getOrdwayContractDataWrapper';
export default class ordwayContractFieldComparison extends NavigationMixin(LightningElement) {
  parentRecord;
  parentRecordId;
  isSyncFunction;
  parentRecordURL;

  firstTab;
  secondTab;
  firstTabIcon;
  secondTabIcon;
  headerIcon;
  contractLineItemFieldsToHide = ['OrdwayLabs__SubscriptionID_LineID__c'];
  
  @track variant;
  @track message;
  @track messageTitle;
  @track disableButtons = false;
  @track showMessageNotification = false;

  @track dataMap = [];
  @track parentRecordMap = [];
  @track childRecordMap = [];
  @track showSpinner = true;
  @track salesforceLineItemToTierMap;
  @track ordwayLineItemToTierStringMap;
  newPlanPicker = false;
  hasError = false;

  get functionTitle() {
    return this.isSyncFunction ? 'Sync to Ordway' : 'Update from Ordway';
  }

  get mainDivClass() {
    return 'slds-notify slds-notify_toast slds-theme_' + this.variant;
  }

  get messageDivClass() {
    return 'slds-icon_container slds-icon-utility-' + this.variant + ' slds-icon-utility-success slds-m-right_small slds-no-flex slds-align-top';
  }

  get iconName() {
    return 'utility:' + this.variant;
  }

  get disableConfirmButton() {
    return (this.hasError || this.disableButtons);
  }

  @wire(CurrentPageReference)
  setCurrentPageReference(currentPageReference) {
    this.parentRecord = currentPageReference.state.c__parentRecordObject;
    this.isSyncFunction = currentPageReference.state.c__isSync;
    this.parentRecordId = this.parentRecord.Id;
  }

  connectedCallback() {
    try{
      getOrdwayContract({ parentRecordId: this.parentRecord.Id }).then(
        result => {
          if (result.objectComparisonDataWrapper['OrdwayLabs__Quote__c'] !== undefined) {
            this.parentRecordMap = result.objectComparisonDataWrapper['OrdwayLabs__Quote__c'];
            this.headerIcon = 'action:quote';
            this.firstTab = 'Quote';
            this.secondTab = 'Quote Line Item';
            this.firstTabIcon = 'standard:quotes';
            this.secondTabIcon = 'standard:quotes';
            this.childRecordMap = result.objectComparisonDataWrapper['OrdwayLabs__QuoteLineItem__c'];
          }
          else {
            this.parentRecordMap = result.objectComparisonDataWrapper['OrdwayLabs__OrdwayContract__c'];
            this.headerIcon = 'standard:contract';
            this.firstTab = 'Contract';
            this.secondTab = 'Contract Line Item';
            this.firstTabIcon = 'standard:contract';
            this.secondTabIcon = 'standard:contract_line_item';
            this.hideContractLineItemRows(result.objectComparisonDataWrapper['OrdwayLabs__ContractLineItem__c']);
            // this.childRecordMap = result['OrdwayLabs__ContractLineItem__c'];
          }
          this.salesforceLineItemToTierMap   = result.salesforceLineItemToTierMap;
          this.ordwayLineItemToTierStringMap = result.ordwayLineItemToTierStringMap;
          this.newPlanPicker                 = result.isNewVersionEnabled;
          this.showSpinner = false;
        }
      ).catch(error => {
        this.error = error;
        this.handleError(error);
      });
      var parentRecordURLPageRef = {
        type: 'standard__recordPage',
        attributes: {
          "recordId": this.parentRecordId,
          "actionName": "view"
        }
      };
      this[NavigationMixin.GenerateUrl](parentRecordURLPageRef)
        .then(url => this.parentRecordURL = url);
    }
    catch (error) {
      this.error = error;
      this.handleError(error);
    }
  }

  handleConfirm() {
    try{
      this.disableButtons = true;
      this.showSpinner = true;
      let parentRecordListVar = [];
      parentRecordListVar[0] = this.parentRecordId;

      if (this.isSyncFunction) {
        syncToOrdway({
          recordId: this.parentRecordId
        })
          .then(result => {
            this.showSpinner = false;
            this.handleSuccess(this.firstTab+' Synced Successfully with Ordway');
          })
          .catch(error => {
            this.handleError(error);
          });
      }
      else {
        updateFromOrdway({
          recordId: this.parentRecordId
        })
          .then(() => {
            this.showSpinner = false;
            this.handleSuccess(this.firstTab+' Updated From Ordway Successfully');
          })
          .catch(error => {
            this.handleError(error);
          });
      }
    }
    catch (error) {
      this.error = error;
      this.handleError(error);
    }
  }

  hideContractLineItemRows(childRecords){
    try {
      if(this.contractLineItemFieldsToHide.length > 0){
        var finalChildRecords = [];
        for(var i=0; i< childRecords.length; i++){
          finalChildRecords[i] = JSON.parse(JSON.stringify(childRecords[i]));
          finalChildRecords[i].value = [];
          for(var j=0; j<childRecords[i].value.length; j++){
            if(!this.contractLineItemFieldsToHide.includes(childRecords[i].value[j].fieldAPIName)){
              finalChildRecords[i].value.push(childRecords[i].value[j]);
            }
          }
        }
      }
      this.childRecordMap = finalChildRecords;
    }
    catch (error) {
      this.error = error;
      this.handleError(error);
    }
  }

  handleCancel() {
    this.disableButtons = true;
    this.redirectToParentRecord();
  }

  redirectToParentRecord() {
    this.disableButtons = false;
    this.showMessageNotification = false;

    window.location.replace(this.parentRecordURL);
  }

  handleError(error) {
    this.hasError = true;
    this.showSpinner = false;
    this.variant = 'error';
    this.messageTitle = 'Error';
    this.message = error.body.message;
    this.showMessageNotification = true;
  }

  handleSuccess(message) {
    this.showSpinner = false;
    this.variant = 'success';
    this.messageTitle = 'Success!';
    this.message = message;
    this.showMessageNotification = true;
  }
}