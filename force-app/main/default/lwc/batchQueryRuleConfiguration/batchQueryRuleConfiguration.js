import { LightningElement, api } from 'lwc';
import { reduceErrors } from 'c/utility';

export default class BatchQueryRuleConfiguration extends LightningElement {
  @api ruleIndex;
  @api configurationRow = {};
  @api fieldsOptions    = [];
  @api objectFieldDataTypeMap = {};
  selectedFieldType;
  selectedDateLiteral;
  dateLiteralNValue;

  DATE_DATA_TYPE = ['Date', 'Date/Time'];

  operatorOptions = [
    {label: 'Equals', value: '='},
    {label: 'Not equals', value: '!='},
    {label: 'LIKE', value: 'LIKE'}
  ];

  dateOperationOptions = [
    {label: 'Equals', value: '='},
    {label: 'Not equals', value: '!='},
    {label: 'Less than', value: '<'},
    {label: 'Greater than', value: '>'},
    {label: 'Less than or equals', value: '<='},
    {label: 'Greater than or equals', value: '>='},
  ]

  DATE_LITERALS = [
    {label: 'Yesterday', value: 'YESTERDAY'},
    {label: 'Today', value: 'TODAY'},
    {label: 'Tomorrow', value: 'TOMORROW'},
    {label: 'Last Week', value: 'LAST_WEEK'},
    {label: 'This Week', value: 'THIS_WEEK'},
    {label: 'Next Week', value: 'NEXT_WEEK'},
    {label: 'Last Month', value: 'LAST_MONTH'},
    {label: 'This Month', value: 'THIS_MONTH'},
    {label: 'Next Month', value: 'NEXT_MONTH'},
    {label: 'Last 90 Days', value: 'LAST_90_DAYS'},
    {label: 'Next 90 Days', value: 'NEXT_90_DAYS'},
    {label: 'Last N Days', value: 'LAST_N_DAYS:n'},
    {label: 'Next N Days', value: 'NEXT_N_DAYS:n'},
    {label: 'Next N Weeks', value: 'NEXT_N_WEEKS:n'},
    {label: 'Last N Weeks', value: 'LAST_N_WEEKS:n'},
    {label: 'Next N Months', value: 'NEXT_N_MONTHS:n'},
    {label: 'Last N Months', value: 'LAST_N_MONTHS:n'},
    {label: 'This Quarter', value: 'THIS_QUARTER'},
    {label: 'Last Quarter', value: 'LAST_QUARTER'},
    {label: 'Next Quarter', value: 'NEXT_QUARTER'},
    {label: 'Next N Quarter', value: 'NEXT_N_QUARTERS:n'},
    {label: 'Last N Quarter', value: 'LAST_N_QUARTERS:n'},
    {label: 'This Year', value: 'THIS_YEAR'},
    {label: 'Last Year', value: 'LAST_YEAR'},
    {label: 'Next Year', value: 'NEXT_YEAR'},
    {label: 'Next N Year', value: 'NEXT_N_YEARS:n'},
    {label: 'Last N Year', value: 'LAST_N_YEARS:n'},
    {label: 'This Fiscal Quarter', value: 'THIS_FISCAL_QUARTER'},
    {label: 'Last Fiscal Quarter', value: 'LAST_FISCAL_QUARTER'},
    {label: 'Next Fiscal Quarter', value: 'NEXT_FISCAL_QUARTER'},
    {label: 'Next N Fiscal Quarter', value: 'NEXT_N_FISCAL_​QUARTERS:n'},
    {label: 'Last N Fiscal Quarter', value: 'LAST_N_FISCAL_​QUARTERS:n'},
    {label: 'This Fiscal Year', value: 'THIS_FISCAL_YEAR'},
    {label: 'Last Fiscal Year', value: 'LAST_FISCAL_YEAR'},
    {label: 'Next Fiscal Year', value: 'NEXT_FISCAL_YEAR'},
    {label: 'Next N Fiscal Year', value: 'NEXT_N_FISCAL_​YEARS:n'},
    {label: 'Last N Fiscal Year', value: 'LAST_N_FISCAL_​YEARS:n'},
  ]

  get fieldStyle(){
    return 'width: 35% !important;'
  }

  get operatorStyle(){
    return 'width: 25% !important;'
  }

  get valueStyle(){
    return 'width: 30% !important;'
  }

  get deleteStyle(){
    return 'width: 5% !important;'
  }

  get isDateFieldType(){
    if(this.selectedFieldType
      && this.DATE_DATA_TYPE.includes(this.selectedFieldType)){
      return true;
    }
    else{
      return false;
    }
  }

  get operations(){
    if(this.isDateFieldType){
      return this.dateOperationOptions;
    }
    else{
      return this.operatorOptions;
    }
  }

  get doesDateLiteralRequiredNValue(){
    return this.isDateFieldType && this.selectedDateLiteral && this.selectedDateLiteral.includes(":n");
  }

  get dateLiteralGridClass(){
    return this.doesDateLiteralRequiredNValue ? 'slds-col slds-size_1-of-2 slds-p-horizontal_x-small' : 'slds-col slds-size_1-of-1 slds-p-horizontal_x-small';
  }

  handleInputChange(event){
    console.log("🚀 ~ file: batchQueryRuleConfiguration.js ~ line 41 ~ BatchQueryRuleConfiguration ~ handleInputChange ~ objectFieldDataTypeMap", JSON.stringify(this.objectFieldDataTypeMap));

    try {
      var thisRow           = Object.assign({}, this.configurationRow);
      thisRow[event.target.name]  = event.target.value;
      if(event.target.name === 'fn'){
        this.selectedFieldType = this.objectFieldDataTypeMap[event.target.value];
      }
      this.resetFieldValidityByName(`[data-inputname="${event.target.dataset.inputname}"]`);
      this.configurationRow       = thisRow;
      this.fireRuleUpdateEvent();
    }
    catch(error) {
      this.handleError(`BatchQueryRuleConfiguration ( Index ${this.ruleIndex} ) => handleInputChange() `, error, true);
    }
  }

  handleDateLiteralChange(event){
    try {
      this.selectedDateLiteral = event.target.value;
      console.log(" batchQueryRuleConfiguration.js ~ line 134 ~ BatchQueryRuleConfiguration ~ handleDateLiteralChange ~ this.selectedDateLiteral", this.selectedDateLiteral)
      var thisRow           = Object.assign({}, this.configurationRow);
      thisRow.val  = this.selectedDateLiteral;
      this.configurationRow       = thisRow;
      this.resetFieldValidityByName(`[data-inputname="${event.target.dataset.inputname}"]`);
      this.fireRuleUpdateEvent();
    }
    catch(error) {
      this.handleError(`BatchQueryRuleConfiguration ( Index ${this.ruleIndex} ) => handleDateLiteralChange() `, error, true);
    }
  }

  handleDateLiteralNValueChange(event){
    try {
      this.dateLiteralNValue = event.target.value;
      //this.configurationRow.val = this.selectedDateLiteral.replace(':n', this.dateLiteralNValue);
      var thisRow           = Object.assign({}, this.configurationRow);
      thisRow.val  = this.selectedDateLiteral.replace(':n', `${':'+this.dateLiteralNValue}`);
      this.configurationRow       = thisRow;
      this.resetFieldValidityByName(`[data-inputname="${event.target.dataset.inputname}"]`);
      this.fireRuleUpdateEvent();
    }
    catch(error) {
      this.handleError(`BatchQueryRuleConfiguration ( Index ${this.ruleIndex} ) => handleDateLiteralChange() `, error, true);
    }
  }

  fireRuleUpdateEvent(){
    try {
    var configRow = Object.assign({}, this.configurationRow);
    configRow["formattedValue"] = this.getFormattedValue();
      var detailObject = {
        "index" : this.ruleIndex,
        "rule"  : configRow
      };
      this.dispatchEvent(new CustomEvent('ruleupdated', { detail: detailObject}));
    } catch (error) {
      this.handleError(`BatchQueryRuleConfiguration ( Index ${this.ruleIndex} ) => fireRuleUpdateEvent() `, error, true);
    }
  }

  resetFieldValidityByName(dataSetProperty){
    try{
      var inputField = this.template.querySelector(dataSetProperty);
      inputField.setCustomValidity("");
      inputField.reportValidity();
    }
    catch(error) {
      this.handleError(`BatchQueryRuleConfiguration ( Index ${this.ruleIndex} ) => resetFieldValidityByName() `, error, true);
    }
  }

  getFormattedValue() {
    try{
      var formattedValue;
      if(this.configurationRow.op === 'LIKE'){
        formattedValue = '%' + this.configurationRow.val + '%';
      }
      else if(this.isDateFieldType){
        formattedValue = this.configurationRow.val;
      }
      else{
        formattedValue = '\'' + this.configurationRow.val + '\'';
      }
      return formattedValue;
    }
    catch(error) {
      this.handleError(`BatchQueryRuleConfiguration ( Index ${this.ruleIndex} ) => getFormattedValue() `, error, true);
    }
  }

  handleDelete(event){
    this.resetValues();
    this.dispatchEvent(new CustomEvent('deleteruleevent', { detail: this.ruleIndex}));
  }

  resetValues(){
    this.selectedFieldType   = null
    this.selectedDateLiteral = null;
    this.dateLiteralNValue   = null;
  }

  handleError(method, error){
    this.dispatchEvent(new CustomEvent('errorevent', { detail: method + JSON.stringify(reduceErrors(error)), bubbles: true, composed: true}));
  }

  @api
  validateConfigurationRow(){
    try {
      console.log('INSIDE > validateConfigurationRow > ');

      const allValid = [...this.template.querySelectorAll(`[data-id="rulecriteriafields"]`)]
      .reduce((validSoFar, inputCmp) => {
          if(!inputCmp.value){
            console.log('INSIDE > validateConfigurationRow > '+ inputCmp.value);
            inputCmp.setCustomValidity("Complete this field");
            inputCmp.reportValidity();
          }
          return validSoFar && inputCmp.checkValidity();
      }, true);
  
      if(!allValid){
        return false;
      }
      else{
        return true;
      }
    } 
    catch (error) {
      this.handleError(`BatchQueryRuleConfiguration ( Index ${this.ruleIndex} ) => validateConfigurationRow() `, error, true);
    } 
  }
}