import { LightningElement, api } from 'lwc';

export default class TierHelper extends LightningElement {
  @api Id;
  @api thisTierRecord;
  @api tierList;
  @api indexVar;
  @api pricingTypeOptions;

  get UpToOrAndUp(){
    return this.tierList.length === this.indexVar+1 ? 'and up' : 'up to';
  }

  get showDeleteIcon(){
    return this.tierList.length === this.indexVar+1 && this.tierList.length > 1;
  }

  get disableLastEndingColumn(){
    return this.tierList.length === this.indexVar+1 ? true : false;
  }

  handleDelete(){
    var deleteDetails = {index : this.indexVar};
    const deleteTier = new CustomEvent('deletetierevent', { detail : deleteDetails });
    this.dispatchEvent(deleteTier);

    var updateDetails = {index : this.indexVar-1, fieldName : 'OrdwayLabs__EndingUnit__c', fieldValue : (this.tierList.length-1) === 1 ? 1 : null};
    const updateData = new CustomEvent('updatetier', { detail : updateDetails });
    this.dispatchEvent(updateData);

  }

  handleFieldChange(event){

    if(event.target.name === 'OrdwayLabs__ListPrice__c'){
      const showSave = new CustomEvent('saveevent', { detail : event.target.value });
      this.dispatchEvent(showSave);
    }

    if(event.target.value <= this.tierList[this.indexVar].OrdwayLabs__StartingUnit__c
      && event.target.name === 'OrdwayLabs__EndingUnit__c' ){
      this.reportCustomValidity();
    }
    else{
      this.removeCustomValidity();
      var updateDetails = {index : this.indexVar, fieldName : event.target.name, fieldValue : event.target.value};
      const updateData = new CustomEvent('updatetier', { detail : updateDetails });
      this.dispatchEvent(updateData);
      
      if((this.tierList.length-1)  != this.indexVar 
          && event.target.name === 'OrdwayLabs__EndingUnit__c'){
        var updateDetails = {index : this.indexVar+1, fieldName : 'OrdwayLabs__StartingUnit__c', fieldValue : parseInt(this.tierList[this.indexVar].OrdwayLabs__EndingUnit__c)};
        const updateData = new CustomEvent('updatetier', { detail : updateDetails });
        this.dispatchEvent(updateData);      
      }
    }
  }

  @api validateEndingValue(){
    this.tierList = [...this.tierList];
    const isAllValid = [...this.template.querySelectorAll('[data-id="tierInputField"]')].reduce((validSoFar, inputCmp) => {
      var index = this.template.querySelector(".endingValue").title;
      if(index && this.tierList[index].OrdwayLabs__StartingUnit__c > this.tierList[index].OrdwayLabs__EndingUnit__c){
        this.reportCustomValidity();
      }
      else{
        this.removeCustomValidity();
      }
      inputCmp.reportValidity(); 
      return validSoFar && inputCmp.checkValidity();
    }, true);
    return isAllValid;
  }

  reportCustomValidity(){
    var inputCmp = this.template.querySelector(".endingValue");
    inputCmp.setCustomValidity('The number is too low.');
  }

  removeCustomValidity(){
    var inputCmp = this.template.querySelector(".endingValue");
    inputCmp.setCustomValidity('');
  }

}