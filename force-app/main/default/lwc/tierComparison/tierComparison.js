import { LightningElement, api } from 'lwc';

export default class TierComparison extends LightningElement {
  @api item;
  @api recordId;
  @api salesforceLineItemToTierMap;
  @api ordwayLineItemToTierStringMap;

  icon="standard:lead_list"
  NA_STRING = 'N/A';

  SALESFORCE_TABLE_COLUMNS = [
    { label: 'Tier', fieldName: 'OrdwayLabs__Tier__c'  ,hideDefaultActions: true},
    { label: 'From', fieldName: 'OrdwayLabs__StartingUnit__c' ,hideDefaultActions: true},
    { label: 'To', fieldName: 'OrdwayLabs__EndingUnit__c' ,hideDefaultActions: true},
    { label: 'Price', fieldName: 'OrdwayLabs__ListPrice__c' ,hideDefaultActions: true},
    { label: 'Type', fieldName: 'OrdwayLabs__PricingType__c' , hideDefaultActions: true}
  ];

  ORDWAY_TABLE_COLUMNS = [
    { label: 'Tier', fieldName: 'OrdwayLabs__Tier__c'  ,hideDefaultActions: true},
    { label: 'From', fieldName: 'OrdwayLabs__StartingUnit__c' ,hideDefaultActions: true},
    { label: 'To', fieldName: 'OrdwayLabs__EndingUnit__c' ,hideDefaultActions: true},
    { label: 'Price', fieldName: 'OrdwayLabs__ListPrice__c' ,hideDefaultActions: true},
    { label: 'Type', fieldName: 'OrdwayLabs__PricingType__c' , hideDefaultActions: true}
  ];

  get hasSalesforceTier(){
    return (this.salesforceLineItemToTierMap != null && this.salesforceLineItemToTierMap != undefined && this.salesforceLineItemToTierMap.hasOwnProperty(this.recordId));
  }

  get salesforceTableData(){
    return this.hasSalesforceTier ? this.salesforceLineItemToTierMap[this.recordId] : [];
  }

  get hasOrdwayTier(){
    return (this.ordwayLineItemToTierStringMap != null && this.ordwayLineItemToTierStringMap != undefined && this.ordwayLineItemToTierStringMap.hasOwnProperty(this.recordId));
  }

  get ordwayTableData(){
    
    return this.hasOrdwayTier ? JSON.parse(this.ordwayLineItemToTierStringMap[this.recordId]) : [];
  }
  
  connectedCallback(){
    
    
 }
}