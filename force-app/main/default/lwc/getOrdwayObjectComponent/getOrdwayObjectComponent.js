import { LightningElement, track, api ,wire} from 'lwc';
import callGetOrdwayObjectBatch from '@salesforce/apex/GetOrdwayObjectComponentController.callGetOrdwayObject_Batch';
import getInitialWrapper from '@salesforce/apex/GetOrdwayObjectComponentController.getInitialWrapper';
import getStatusPicklistValue from '@salesforce/apex/GetOrdwayObjectComponentController.getPicklistWrapper';
import getbulkSyncConfigurationHelper from '@salesforce/apex/GetOrdwayObjectComponentController.getBulkSyncConfigurationRecords';
import saveBulkSyncConfiguration from '@salesforce/apex/GetOrdwayObjectComponentController.saveBulkSyncConfigurationSetting';
import deleteBulkSyncConfiguration from '@salesforce/apex/GetOrdwayObjectComponentController.deleteBulkSyncConfigurationSetting';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class GetOrdwayObjectComponent extends LightningElement {

  @api objectName;
  @api pageNumber = 1;
  @api fromDate;
  @api toDate;
  @api filter = 'None';
  @api filterValue;
  @track metaDataRecords;
  @track isMultiEntityEnabled;
  @track isMultiEntityPermissionAssigned;
  @track selectedOrdwayEntity;
  @track ordwayEntities;
  @track applicationSetting;

  @api statusObjectName;
  @api statusFieldName;

  nValue;
  @api objectNameAuto;
  @api criteriaAuto;
  @api selectedOrdwayEntityAuto;
  multiEntityOrg = false;

  showNInput = false;
  disableAdd = true;
  showContractCheckbox = false;
  includeSynced = false;
  @api bulkSyncConfiguration = [];

  disableDates;
  hideDateFields;
  hideAllFilters;
  hideFilter;
  @track disableRefresh = true;
  objectApiName;
  objectOptions = [
    {value: 'Account', label: 'Accounts'},
  ];

  objectOptionsAuto = [{value: 'Account', label: 'Accounts'}];

  criteriaOptions = [
    {value: '7', label: 'Last 7 Days'},
    {value: '21', label: 'Last 21 Days'},
    {value: '30', label: 'Last 30 Days'},
    {value: '60', label: 'Last 60 Days'},
    {value: '90', label: 'Last 90 Days'},
    {value: '120', label: 'Last 120 Days'},
    {value: '180', label: 'Last 180 Days'},
    {value: 'N', label: 'Last N Days'},
  ];

  orderNumberMap = {
    "Account" : 1,
    "Plan" : 2,
    "QuoteTemplate" : 3,
    "Contact": 4,
    "OrdwayContract" : 5,
    "Invoice" : 6,
    "Payment" : 7,
    "Credit": 8,
    "Refund" : 9,
    "Usage" : 10
  };

  objectLabelMap = {
    "Account" : "Accounts",
    "Contact": "Contacts",
    "OrdwayContract" : "Subscriptions",
    "Invoice" : "Invoices",
    "Payment" : "Payments",
    "Credit": "Credits",
    "Refund" : "Refunds",
    "Usage" : "Usages"
  };

  objectApiNameMap = {
    "OrdwayContract" : "OrdwayLabs__OrdwayContract__c",
    "Invoice" : "OrdwayLabs__Invoice__c",
    "Payment" : "OrdwayLabs__Payment__c",
    "Credit": "OrdwayLabs__Credit__c",
    "Refund" : "OrdwayLabs__Refund__c"
  };

  fieldApiNameMap = {
    "OrdwayContract" : "OrdwayLabs__OrdwayContractStatus__c",
    "Invoice" : "OrdwayLabs__Status__c",
    "Payment" : "OrdwayLabs__Status__c",
    "Credit": "OrdwayLabs__Status__c",
    "Refund" : "OrdwayLabs__RefundStatus__c"
  };

  accountStatusValue = [
      {value: 'active', label: 'Active'},
      {value: 'inactive',label: 'Inactive'}
    ];

  @track statusOptions;
  
  @track filterOptions;
  @track filtersMap = {
    'Account': [
        {
          value: 'None',
          label: '--None--'
        },
        {
          value: 'customer_id',
          label: 'Customer Id'
        },
        {
          value: 'created_date',
          label: 'Created Date'
        },
        {
          value: 'updated_date',
          label: 'Updated Date'
        },
        {
          value: 'status',
          label: 'Status'
        }
    ],
    'Invoice': [
        {
          value: 'None',
          label: '--None--'
        },
        {
          value: 'customer_id',
          label: 'Customer Id'
        },
        {
          value: 'created_date',
          label: 'Created Date'
        },
        {
          value: 'updated_date',
          label: 'Updated Date'
        },
        {
          value: 'invoice_date',
          label: 'Invoice Date'
        },
        {
          value: 'due_date',
          label: 'Due Date'
        },
        {
          value: 'status',
          label: 'Status'
        }
    ],
    'Payment': [
        {
          value: 'None',
          label: '--None--'
        },
        {
          value: 'customer_id',
          label: 'Customer Id'
        },
        {
          value: 'created_date',
          label: 'Created Date'
        },
        {
          value: 'updated_date',
          label: 'Updated Date'
        },
        {
          value: 'payment_date',
          label: 'Payment Date'
        },
        {
          value: 'status',
          label: 'Status'
        }
    ],
    'Refund': [
        {
          value: 'None',
          label: '--None--'
        },
        {
          value: 'customer_id',
          label: 'Customer Id'
        },
        {
          value: 'created_date',
          label: 'Created Date'
        },
        {
          value: 'updated_date',
          label: 'Updated Date'
        },
        {
          value: 'refund_date',
          label: 'Refund Date'
        },
        {
          value: 'refund_status',
          label: 'Status'
        }
    ],
    'Credit': [
        {
          value: 'None',
          label: '--None--'
        },
        {
          value: 'customer_id',
          label: 'Customer Id'
        },
        {
          value: 'created_date',
          label: 'Created Date'
        },
        {
          value: 'updated_date',
          label: 'Updated Date'
        },
        {
          value: 'credit_date',
          label: 'Credit Date'
        },
        {
          value: 'status',
          label: 'Status'
        }
    ],
    'OrdwayContract': [
        {
          value: 'None',
          label: '--None--'
        },
        {
          value: 'customer_id',
          label: 'Customer Id'
        },
        {
          value: 'id',
          label: 'Subscription Id'
        },
        {
          value: 'created_date',
          label: 'Created Date'
        },
        {
          value: 'updated_date',
          label: 'Updated Date'
        },
        {
          value: 'billing_start_date',
          label: 'Billing Start Date'
        },
        {
          value: 'contract_effective_date',
          label: 'Contract Effective Date'
        },       
        {
          value: 'service_start_date',
          label: 'Service Start Date'
        },
        {
          value: 'cancellation_date',
          label: 'Cancellation Date'
        },
        {
          value: 'status',
          label: 'Status'
        }
    ],
    'Contact': [
      {
        value: 'None',
        label: '--None--'
      },
      {
        value: 'customer_id',
        label: 'Ordway Customer Id'
      },
      {
        value: 'contact_id',
        label: 'Ordway Contact Id'
      },
      {
        value: 'created_date',
        label: 'Created Date'
      },
      {
        value: 'updated_date',
        label: 'Updated Date'
      }
  ],
  'Usage': [	
    {	
      value: 'None',	
      label: '--None--'	
    },	
    {	
      value: 'id',	
      label: 'Usage Id'	
    },	
    {	
      value: 'customer_id',	
      label: 'Customer Id'	
    },	
    {	
      value: 'date',	
      label: 'Usage Date'	
    },	
    {	
      value: 'subscription_id',	
      label: 'Subscription Id'	
    },	
    {	
      value: 'created_date',	
      label: 'Created Date'	
    },	
    {	
      value: 'updated_date',	
      label: 'Updated Date'	
    }
  ],
  'Plan': [	
    {	
      value: 'None',	
      label: '--None--'	
    },	
    {	
      value: 'id',	
      label: 'Plan Id'	
    }
  ],
  'OrdwayProduct': [	
    {	
      value: 'None',	
      label: '--None--'	
    },	
    {	
      value: 'id',	
      label: 'Product Id'	
    },		
    {	
      value: 'created_date',	
      label: 'Created Date'	
    },	
    {	
      value: 'updated_date',	
      label: 'Updated Date'	
    }
  ],
};

get showEntities(){
  return this.isMultiEntityPermissionAssigned && this.isMultiEntityEnabled && (this.ordwayEntities != undefined && this.ordwayEntities != null && this.ordwayEntities.length > 0);
}

get showSelected(){
  return this.bulkSyncConfiguration != undefined && this.bulkSyncConfiguration != null && this.bulkSyncConfiguration.length > 0;
}

@wire(getInitialWrapper)
  wiredGetInitialWrapper({data, error}){
    if(data){
      this.applicationSetting   = data.applicationSetting;
      this.isMultiEntityEnabled = data.isMultiEntityEnabled;
      this.isMultiEntityPermissionAssigned = data.isMultiEntityPermissionAssigned;
      if(this.isMultiEntityEnabled){
        this.ordwayEntities = data.ordwayEntities;
      }
      this.metaDataRecords = data.dynamicObjectMetadataList;
      var dynamicObjectMetadataList = data.dynamicObjectMetadataList;
      if(data.isQuoteEnabled){
        this.objectOptions.push({value: 'QuoteTemplate', label: 'Quote Templates'});                                   
      }
      for(var i=0; i<dynamicObjectMetadataList.length; i++)  {
        this.objectOptions = [...this.objectOptions ,{value: dynamicObjectMetadataList[i].DeveloperName , label: dynamicObjectMetadataList[i].MasterLabel} ];  
        if(dynamicObjectMetadataList[i].DeveloperName !== 'OrdwayProduct' && dynamicObjectMetadataList[i].DeveloperName !== 'Plan'){
          this.objectOptionsAuto = [...this.objectOptionsAuto ,{value: dynamicObjectMetadataList[i].DeveloperName , label: dynamicObjectMetadataList[i].MasterLabel} ]; 
        }
      }
      if(this.isMultiEntityPermissionAssigned && this.isMultiEntityEnabled 
        && (this.ordwayEntities != undefined && this.ordwayEntities != null && this.ordwayEntities.length > 0)){
          this.multiEntityOrg = true;
      }
    }
    else if(error){
    }
  }

  @wire(getbulkSyncConfigurationHelper)
  wiredGetbulkSyncConfigurationHelper({data, error}){
    if(data){
      this.bulkSyncConfiguration = data;
    }
    else if(error){
    }
  }

  handleStatusChange(event){
    this.filterValue = event.target.value;
  }

  handleChange(event){
    this.hideAllFilters = false;
    if(event.target.name === 'objectName'){
      this.showContractCheckbox = false;
      this.hideFilter = false;
      this.disableDates = false;
      this.filterValue = null;
      this.statusOptions = [];
      this.objectName = event.target.value;
      
      var objectApiName = event.target.value;
      if(objectApiName === 'Account'){
        this.filter = 'None';
        this.filterOptions = this.filtersMap.Account;
      }
      else if(objectApiName === 'Plan'){
        this.filter = 'None';
        this.filterOptions = this.filtersMap.Plan;
      }
      else if(objectApiName === 'OrdwayProduct'){
        this.filter = 'None';
        this.filterOptions = this.filtersMap.OrdwayProduct;
      }
      else if(objectApiName === 'QuoteTemplate'){
        this.filter = null;
        this.fromDate = null;
        this.disableDates = true; 
        this.hideAllFilters = true;
        this.toDate = null;
        this.hideDateFields = false;
        this.hideFilter = true;
      }
      else if(objectApiName === 'Invoice'){
        this.filter = 'None';
        this.filterOptions = this.filtersMap.Invoice;
      }
      else if(objectApiName === 'Credit'){
        this.filter = 'None';
        this.filterOptions = this.filtersMap.Credit;
      }
      else if(objectApiName === 'OrdwayContract'){
        this.showContractCheckbox = true;
        this.filter = 'None';
        this.filterOptions = this.filtersMap.OrdwayContract;
      }
      else if(objectApiName === 'Refund'){
        this.filter = 'None';
        this.filterOptions = this.filtersMap.Refund;
      }
      else if(objectApiName === 'Payment'){
        this.filter = 'None';
        this.filterOptions = this.filtersMap.Payment;
      }
      else if(objectApiName === 'Contact'){
        this.filter = 'None';
        this.filterOptions = this.filtersMap.Contact;
      }
      else if(objectApiName === 'Usage'){	
        this.filter = 'None';	
        this.filterOptions = this.filtersMap.Usage;	
      }
      else{
        this.filter = null;
        this.filterOptions = null;
        this.hideFilter = true;
      }
    }
    else if(event.target.name === 'fromDate'){
      this.fromDate = new Date();
      this.fromDate = event.target.value;
    }
    else if(event.target.name === 'ordwayEntity'){
      this.hideAllFilters = true;
      this.hideDateFields = false;
      this.selectedOrdwayEntity = event.target.value;
    }
    else if(event.target.name === 'toDate'){
      this.toDate = new Date();
      this.toDate = event.target.value;
    }
    else if(event.target.name === 'filter'){
      this.filter = event.target.value;
      if(this.filter === 'customer_id' || this.filter === 'status' || this.filter === 'refund_status' || this.filter === 'id'
        || this.filter === 'contact_id'){
        this.filterValue = null;
      }
      if(this.filter === 'status' || this.filter === 'refund_status'){ 
        this.showStatus = true;
        if(this.objectName === 'Account'){
          this.statusOptions = this.accountStatusValue;
        }
        else {
          this.statusFieldName = this.fieldApiNameMap[this.objectName];
          this.statusObjectName = this.objectApiNameMap[this.objectName];
          this.getstatusValue();
        }
      }
      else{
        this.hideDateFields = false;
        this.disableDates = false; 
        this.showStatus = false;
      }
    }
    else if(event.target.name === 'customerId'){
      this.filterValue = event.target.value;
    }
    if(this.filter === 'customer_id' || this.filter === 'status' || this.filter === 'id' 
      || this.filter === 'refund_status' || this.filter === 'contact_id' || this.filter === 'charge_id'	
      || this.filter === 'subscription_id'){
      this.hideDateFields = true;
      this.hideAllFilters = false;
      this.fromDate = null;
      this.toDate = null;
    }
    else if (this.filter === 'None') {
      this.hideDateFields = false;
      this.disableDates = true;
      this.fromDate = null;
      this.toDate = null;
    }
    else{
      this.filterValue = null;
      this.showStatus = false;
    }
    if( 
      (this.applicationSetting == undefined || this.applicationSetting.OrdwayLabs__Token__c == undefined || this.applicationSetting.OrdwayLabs__Token__c == null)  
      || (this.isMultiEntityEnabled && (this.selectedOrdwayEntity == null || this.selectedOrdwayEntity == undefined))){
      this.disableRefresh = true;
      this.disableAdd = true;
    }
    else{
      this.disableRefresh = false;
    }
  }

  handleNewRow(event){
    var nameVar = this.multiEntityOrg ? this.selectedOrdwayEntityAuto+'_'+this.objectNameAuto : this.objectNameAuto;
    this.bulkSyncConfiguration = [...this.bulkSyncConfiguration];
    var newObject = {Name : nameVar,
       OrdwayLabs__Entity__c:this.selectedOrdwayEntityAuto,
       OrdwayLabs__Filter__c:this.criteriaAuto,
       OrdwayLabs__Object__c:this.objectNameAuto,
       OrdwayLabs__ObjectLabel__c:this.objectLabelMap[this.objectNameAuto],
      OrdwayLabs__OrderNumber__c:this.orderNumberMap[this.objectNameAuto]};
      if(this.criteriaAuto === 'N'){
        newObject.OrdwayLabs__Filter__c = this.nValue; 
      }
    this.handleBulkSyncSave(newObject);
  }

  handleBulkSyncSave(newObject){
    this.showSpinner = true;
    let cloned = JSON.parse(JSON.stringify(newObject));
    var array = [];
    array.push(cloned);
    var bulkSyncToUpsert = JSON.stringify(array);
    saveBulkSyncConfiguration({ bulkSyncConfigurationToUpsert: bulkSyncToUpsert}).then(
      result => {
        this.bulkSyncConfiguration = result;
        this.handleSuccess('Bulk Sync Configuration updated successfully');
        this.showSpinner = false;
        this.disableAdd = true;
      }
    ).catch(error => {
      this.showSpinner = false;
      this.handleError(error.body.message);
    });
  }
  
  handleDelete(event){
   this.bulkSyncConfiguration = [...this.bulkSyncConfiguration];
   this.deletebulkSyncConfigurationHelper(this.bulkSyncConfiguration[event.target.getAttribute("data-indexvar")]
                                                                                ,event.target.getAttribute("data-indexvar"));
  }

  deletebulkSyncConfigurationHelper(objectToDelete, index){
    this.showSpinner = true;
    var array = [];
    array.push(objectToDelete);
    var bulkSyncToDelete = JSON.stringify(array);
    deleteBulkSyncConfiguration({ bulkSyncConfigurationToDelete: bulkSyncToDelete}).then(
      result => {
        this.handleSuccess('Bulk Sync Configuration Record deleted successfully');
        this.bulkSyncConfiguration = [...this.bulkSyncConfiguration];
        this.bulkSyncConfiguration.splice(index, 1);
        this.showSpinner = false;
      }
    ).catch(error => {
      this.showSpinner = false;
      this.handleError(error.body.message);
    });
  }

  handleValidateInput(event){
    const isAllValid = [...this.template.querySelectorAll('.bulkSyncInputField')].reduce((validSoFar, inputCmp) => {
      inputCmp.reportValidity(); 
      return validSoFar && inputCmp.checkValidity();
    }, true);

    if(isAllValid){
      this.handleNewRow(event);
    }
  }

  onChecboxChange(event) {
    this.includeSynced  = event.target.checked;
    if(this.includeSynced){
      this.showWarningToast();
    }
  }

  checkMetaDataConfig(){
    const selectedObjectId = this.objectName;
    const isConfigured = this.metaDataRecords.find(
      record => record.DeveloperName === selectedObjectId
    );
    if(!isConfigured){
      this.handleError('Please configure metadata for selected objects');
    }
    else{
      if (this.filter === 'customer_id' || this.filter === 'status' || this.filter === 'refund_status' 
      || this.filter === 'contact_id' || this.filter === 'id') {
        this.validateFilterValue();
      }
      else{
        this.validateDates();
      }
    }
  }

  handleRefresh(){
    this.validateInput();
  }

  validateInput(){
      if (!this.objectName) {
        this.handleError('Please select an object to sync from Ordway');
      }
      else if(this.objectName !== 'Account' && this.objectName !== 'QuoteTemplate'){
        this.checkMetaDataConfig();
      }
      else{
        if (this.filter === 'customer_id' || this.filter === 'status' || this.filter === 'refund_status' || this.filter === 'contact_id'
            || this.filter === 'id' ) {
          this.validateFilterValue();
        }
        else{
          this.validateDates();
        }
      }
  }

  validateFilterValue(){
      if (!this.filterValue) {
        this.handleError('Please provide filter value');
      }
      else{
        this.refresh();
      }
  }

  validateDates(){
    if(this.toDate && !this.fromDate){
      this.handleError('Please provide From Date if To Date is given');
    }
    else{
      this.refresh();
    }
  }

  handleAutoSectionChanges(event){
    this.disableAdd = false;
    if(event.target.name === 'criteriaAuto'){
      this.criteriaAuto = event.target.value;
      this.nValue = null;
    }
    if(event.target.name === 'NInput'){
          this.nValue = event.target.value;
    }
    if(this.criteriaAuto === 'N'){
      this.showNInput = true;
    }
    else{
      this.showNInput = false;
    }
    if(event.target.name === 'objectNameAuto'){
      this.objectNameAuto = event.target.value;
    }
    if(event.target.name === 'ordwayEntityAuto'){
      this.selectedOrdwayEntityAuto = event.target.value;
    }
  }

  getstatusValue(){
    getStatusPicklistValue({
      objectApiName : this.statusObjectName,
      picklistFieldApiName : this.statusFieldName
    })
    .then(result => {
      this.statusOptions = result.PicklistWrapperList;
    })
    .catch(error => {
      
    });
  }

  refresh(){
    
    callGetOrdwayObjectBatch( {
      pageNumber: 1 , 
      objectName: this.objectName ,  
      fromDate: this.fromDate, 
      toDate:this.toDate,
      filter : this.filter,
      filterValue: this.filterValue,
      xEntityId: this.selectedOrdwayEntity,
      includeSynced: this.includeSynced
      })
    .then(result => {
      this.disableRefresh = true;
      var message = 'To view the progress of this job, go to Setup -> Jobs -> Apex Jobs. Please do not start another sync job until the previous job has completed';
      this.handleSuccess(message);
    })
    .catch(error => {
      this.handleError(error);
    });
  }

  handleError(error) {
    const evt = new ShowToastEvent({
      title: 'Error',
      message: error,
      variant: 'error',
      mode: 'sticky'
  });
  this.dispatchEvent(evt);
  }

  handleSuccess(message) {
    const evt = new ShowToastEvent({
      title: 'Apex Job',
      message: message,
      variant: 'success',
      mode: 'sticky'
  });
  this.dispatchEvent(evt);
  }

  showWarningToast() {
    const evt = new ShowToastEvent({
        title: 'Warning',
        message: 'By enabling this option, contract records in Ordway that have Salesforce IDs will overwrite data on related Ordway Contracts and Opportunities in Salesforce.',
        variant: 'warning',
        mode: 'sticky'
    });
    this.dispatchEvent(evt);
}

}