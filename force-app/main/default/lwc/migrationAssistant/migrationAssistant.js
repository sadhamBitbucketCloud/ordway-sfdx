import { LightningElement } from 'lwc';
import { reduceErrors } from 'c/utility';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getAssistantWrapper from "@salesforce/apex/MigrationAssistant.getMigrationAssistantWrapper";
import enablePlanPickerFeature from "@salesforce/apex/MigrationAssistant.enablePlanPicker";
import enableQuoteLineItemFeature from "@salesforce/apex/MigrationAssistant.activateQuoteLineItemTier";

export default class MigrationAssistant extends LightningElement {
  spinnerBoolean = false;
  renderForm = false;

  isQuoteAssistantEnabled       = false;
  isPlanPickerVersionTwoEnabled = false;

  //Plan Picker release
  PLAN_PICKER_FEATURE_UNIQUE_KEY                      = 'PLAN_PICKER_FEATURE';
  PLAN_PICKER_FEATURE_TITLE                           = 'Plan Picker Settings';
  PLAN_PICKER_FEATURE_DESCRIPTION                     = 'Plan Picker v2.0 enables easy plan picker field configurations. Add, remove, and set the order of fields in the plan picker and configure each field to be read-only or editable. Once enabled, configure these settings in Ordway Settings > Plan Picker.';
  PLAN_PICKER_FEATURE_RELEASE_ENABLED_SUCCESS_MESSAGE = 'Plan Picker version 2.0 enabled successfully.';

  //Quote Tier release
  QUOTE_LINE_TIER_FEATURE_UNIQUE_KEY                      = 'QUOTE_LINE_ITEM_TIER';
  QUOTE_LINE_TIER_FEATURE_TITLE                           = 'Quote Line Item Tier - Custom Object';
  QUOTE_LINE_TIER_FEATURE_DESCRIPTION                     = 'A custom object for creating and storing price tiers for volume or tiered pricing charges on Quote Line Items.';
  QUOTE_LINE_TIER_FEATURE_RELEASE_ENABLED_SUCCESS_MESSAGE = 'Quote Line Item tier enabled successfully.';

  connectedCallback(){
    this.showSpinner();
    try {
      this.showSpinner();
      getAssistantWrapper()
      .then(result => {
        this.isQuoteAssistantEnabled             = result.isQuoteAssistantEnabled;
        this.isPlanPickerVersionTwoEnabled       = result.isPlanPickerVersionTwoEnabled;
        this.renderForm = true;
        this.hideSpinner();
      })
      .catch(error => {
        this.customToastNotification('Error', JSON.stringify(reduceErrors(error)), true);
        this.hideSpinner();
      });
    }
    catch(error) {
      this.customToastNotification('Error', JSON.stringify(reduceErrors(error)), true);
      this.hideSpinner();
    }
    this.hideSpinner();
  }

  enablePlanPickerReleasedFeature(){
    try {
      this.showSpinner();
      enablePlanPickerFeature()
      .then(result => {
        if(result){
          this.isPlanPickerVersionTwoEnabled = true;
          this.customToastNotification('Success', this.PLAN_PICKER_FEATURE_RELEASE_ENABLED_SUCCESS_MESSAGE, false);
        }
        this.hideSpinner();
      })
      .catch(error => {
        this.customToastNotification('Error', JSON.stringify(reduceErrors(error)), true);
        this.hideSpinner();
      });
    }
    catch(error) {
      this.customToastNotification('Error', JSON.stringify(reduceErrors(error)), true);
      this.hideSpinner();
    }
  }

  enableQuoteLineItemTierReleasedFeature(){
    try {
      this.showSpinner();
      enableQuoteLineItemFeature()
      .then(result => {
        if(result){
          this.isQuoteAssistantEnabled = true;
          this.customToastNotification('Success', this.QUOTE_LINE_TIER_FEATURE_RELEASE_ENABLED_SUCCESS_MESSAGE, false);
        }
        this.hideSpinner();
      })
      .catch(error => {
        this.customToastNotification('Error', JSON.stringify(reduceErrors(error)), true);
        this.hideSpinner();
      });
    }
    catch(error) {
      this.customToastNotification('Error', JSON.stringify(reduceErrors(error)), true);
      this.hideSpinner();
    }
  }

  showSpinner() {
    this.spinnerBoolean = true;
  }

  hideSpinner() {
    this.spinnerBoolean = false;
  }

  customToastNotification(toastTitle, toastMessage, isErrorMessage) {
    var messageString = isErrorMessage ? 'Error' : 'Success';
    const showToastEvent = new ShowToastEvent({
        Title: toastTitle,
        message: toastMessage,
        variant: messageString
    });
    this.dispatchEvent(showToastEvent);
  }
}