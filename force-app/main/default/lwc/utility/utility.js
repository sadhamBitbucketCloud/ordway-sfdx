/**
 * Utility JS file for storing utility js function which can be invoke from any LWC JS
**/

/**
 * Reduces one or more LDS errors into a string[] of error messages.
 * @param {FetchResponse|FetchResponse[]} errors
 * @return {String[]} Error messages
 */
export function reduceErrors(errors) {
  if (!Array.isArray(errors)) {
    errors = [errors];
  }

  return (
    errors
    // Remove null/undefined items
    .filter((error) => !!error)
    // Extract an error message
    .map((error) => {
        // UI API read errors
        if (Array.isArray(error.body)) {
          return error.body.map((e) => e.message);
        }
        // UI API DML, Apex and network errors
        else if (error.body && typeof error.body.message === 'string') {
          return error.body.message;
        }
        // JS errors
        else if (typeof error.message === 'string') {
          return error.message;
        }
        // Unknown error shape so try HTTP status text
        return error.statusText;
    })
    // Flatten
    .reduce((prev, curr) => prev.concat(curr), [])
    // Remove empty strings
    .filter((message) => !!message)
  );
}

/**
 * Remove an element from an array based on specific attribute and value
 * @param {Object[]} Object Array
 * @return {Object[]} Filtered Array
 */

export function removeElementFromArray(array, attributeName, value){
  return array.filter(function(element){ return element[attributeName] != value; });
}

export function sortArrayByProperty(array, attributeName){
  return array.sort((a, b) => a[attributeName] > b[attributeName] ? 1 : -1);
}

export function removeDuplicates(array){
  return [...new Set(array)];
}