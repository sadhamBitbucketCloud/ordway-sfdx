import { LightningElement, api } from 'lwc';

export default class OrdwayEntityRuleRow extends LightningElement {
  @api thisEntityRule;
  @api opportunityFields;
  @api accountFields;
  @api ordwayEntities;
  @api indexVar;
  @api ordwayEntityRuleRecordsToDelete;

  get ordwayEntitiesOptions() {
    return this.ordwayEntities;
  }

  get tdStyle(){
    return 'vertical-align: middle;'
  }

  get isEntityPopulated() {
    return (this.thisEntityRule.Name == null && this.thisEntityRule.Name == undefined) ||  (this.thisEntityRule.OrdwayLabs__EntityID__c == null && this.thisEntityRule.OrdwayLabs__EntityID__c == undefined);
  }

  get objectOptions() {
    return [
      { label: 'Opportunity', value: 'Opportunity' },
      { label: 'Account', value: 'Account' }
    ];
  }
  get isObjectPopulated(){
    return (this.thisEntityRule.OrdwayLabs__Object__c == null &&  this.thisEntityRule.OrdwayLabs__Object__c == undefined);
  } 

  get fieldOptions() {
   return this.thisEntityRule.OrdwayLabs__Object__c == 'Opportunity' ? this.opportunityFields : this.accountFields;
  }

  handleInputValueChange(event){
    var updateDetails = {index : this.indexVar, fieldName : event.target.getAttribute("data-fieldname"), fieldValue : event.target.value};
    const updateData = new CustomEvent('updateentityrule', { detail : updateDetails });
    this.dispatchEvent(updateData);
  }

  handleDelete(){
    var deletDetails = {index : this.indexVar};
    const deleteEntity = new CustomEvent('deleteentityevent', { detail : deletDetails });
    this.dispatchEvent(deleteEntity);
  }

  @api validateInput(){
    const isAllValid = [...this.template.querySelectorAll('.entityRuleinputField')].reduce((validSoFar, inputCmp) => {
      
      inputCmp.reportValidity(); 
      return validSoFar && inputCmp.checkValidity();
    }, true);
    return isAllValid;
  }
}