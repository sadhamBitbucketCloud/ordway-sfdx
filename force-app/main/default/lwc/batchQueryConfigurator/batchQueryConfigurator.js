import { LightningElement , track} from 'lwc';
import { reduceErrors} from 'c/utility';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getObjectFieldWrapper from "@salesforce/apex/BatchQueryConfiguratorController.getObjectFieldWrapper";
import saveCustomSetting from "@salesforce/apex/BatchQueryConfiguratorController.saveBatchQueryCustomSetting";
export default class BatchQueryConfigurator extends LightningElement {
  spinnerBoolean                    = false;

  SYNC_TO_ORDWAY_CONFIG                  = 'SYNC_TO_ORDWAY_CONFIG';
  ACTIVATE_CONTRACT_CONFIG               = 'ACTIVATE_CONTRACT_CONFIG';
  ADD_NEW_RULE_CRITERIA_ROW_BUTTON_LABEL = "Add Condition";
  SAVE_BUTTON_LABEL                      = "Save";


  BATCH_QUERY_CONFIGURATOR_TITLE     = 'Batch query configurator';
  OBJECT_AVAILABLE_FOR_CONFIGURATION = [{ label: 'Subscriptions', value: 'OrdwayLabs__OrdwayContract__c' }];
  //TODO: get the batch class name for Activate contract
  BATCH_CLASS_ACTIONS                        = {"OrdwayLabs__OrdwayContract__c" : [{ label: 'Sync to Ordway', value: 'SyncOrdwayContract_Batch' }]};
  BATCH_CLASS_ACTIONS_TO_SETTING_UNIQUE_NAME = {"SyncOrdwayContract_Batch" : this.SYNC_TO_ORDWAY_CONFIG};

  //Custom Setting fields:
  CUSTOM_SETTING_FIELD_NAME                = 'Name';
  CUSTOM_SETTING_FIELD_API                 = 'OrdwayLabs__API__c';
  CUSTOM_SETTING_FIELD_QUERY_CRITERIA      = 'OrdwayLabs__QueryCriteria__c';
  CUSTOM_SETTING_OBJECT                    = 'OrdwayLabs__Object__c';
  CUSTOM_SETTING_QUERY_FILTER_WHERE_CLAUSE = 'OrdwayLabs__QueryFilter__c';
  CUSTOM_SETTING_FILTER_LOGIC              = 'OrdwayLabs__Filter__c';

  selectedObject;
  selectedBatchApex;
  renderRuleCriteriaTable = true;
  whereCondition = 'WHERE ';

  @track objectFieldOptions     = [];
  @track objectFieldDataTypeMap = {};
  @track configuredRules = [];
  @track customSettingObject = {};
  filterLogic;

  get batchClassActionOptions(){
    var batchActions = [];
    if(this.selectedObject){
      batchActions = this.BATCH_CLASS_ACTIONS[this.selectedObject];
    }
    return batchActions;
  }

  get disableBatchAction(){
    return (this.selectedObject == null && this.selectedObject == undefined)
  }

  get showRuleCriteriaConfigTable(){
    return this.renderRuleCriteriaTable && this.configuredRules.length > 0;
  }

  get disableAddCondition(){
    return this.configuredRules.length === 3;
  }

  get ruleHeadingStyle(){
    return 'width: 5% !important;'
  }

  get fieldHeadingStyle(){
    return 'width: 35% !important;'
  }

  get operatorHeadingStyle(){
    return 'width: 25% !important;'
  }

  get valueHeadingStyle(){
    return 'width: 30% !important;'
  }

  get deleteHeadingStyle(){
    return 'width: 5% !important;'
  }

  get tableStyle(){
    return "z-index :9000 !important; position: relative !important;";
  }

  handleComboboxValueChange(event){
    try {
      this.showSpinner();
      if(event.target.name === 'selectedObject'){
        this.selectedBatchApex = null;
      }

      if(event.target.name
        && event.target.value){
        this[event.target.name] = event.target.value;
      }

      if(this.selectedObject
        && this.selectedBatchApex){
          this.getObjectFieldWrapperData();
        }
      this.hideSpinner();
    }
    catch(error) {
      this.customToastNotification('Error','BatchQueryConfigurator => handleComboxValueChange ' + JSON.stringify(reduceErrors(error)), true);
      this.hideSpinner();
    }
  }

  getObjectFieldWrapperData(){
    try {
      this.showSpinner();
      getObjectFieldWrapper({
        customSettingUniqueName: this.BATCH_CLASS_ACTIONS_TO_SETTING_UNIQUE_NAME[this.selectedBatchApex],
        objectAPIName : this.selectedObject
      })
      .then(result => {
        console.log("batchQueryConfigurator.js ~ line 72 ~ BatchQueryConfigurator ~ getObjectFieldWrapperData ~ result", JSON.stringify(result));
        this.objectFieldOptions     = result.fieldList;
        this.objectFieldDataTypeMap = result.fieldMap;
        this.updateRuleCriteria();
        this.hideSpinner();
      })
      .catch(error => {
        this.customToastNotification('Error','BatchQueryConfigurator => getObjecFieldWrapperData() => getObjectFieldWrapper() :' + JSON.stringify(reduceErrors(error)), true);
        this.hideSpinner();
      });
      this.hideSpinner();
    }
    catch(error) {
      this.customToastNotification('Error','BatchQueryConfigurator => getObjecFieldWrapperData ' + JSON.stringify(reduceErrors(error)), true);
      this.hideSpinner();
    }
  }

  updateRuleCriteria(){
    try {
      this.showSpinner();
      this.configuredRules  = [];
      this.filterLogic      = null;
      //TODO: update the logic based on the custom setting value
      this.addNewRuleConfigure();
      // if(!this.selectedPlatformEventForConfiguration.hasOwnProperty(this.RULE_CONFIGURATION_FIELD_API_NAME) &&
      //   !this.selectedPlatformEventForConfiguration[this.RULE_CONFIGURATION_FIELD_API_NAME]){
      //   this.addNewRuleConfigure();
      // }
      // else{
      //   var configObject      = JSON.parse(this.selectedPlatformEventForConfiguration[this.RULE_CONFIGURATION_FIELD_API_NAME]);
      //   this.filterLogic      =  configObject.filterLogic;
      //   this.configuredRules  = configObject.rules;
      //   this.evaluateIsChangedOnSelectedFields = configObject.hasOwnProperty(this.METADATA_CONFIGURATION_EVALUATE_IS_CHANGED_ON_SELECTED__FIELDS_PROPERTY_KEY) ? configObject[this.METADATA_CONFIGURATION_EVALUATE_IS_CHANGED_ON_SELECTED__FIELDS_PROPERTY_KEY] : false;
      // }
      this.hideSpinner();
    }
    catch(error) {
      this.customToastNotification('Error','BatchQueryConfigurator => updateRuleCriteria() ' + JSON.stringify(reduceErrors(error)), true);
      this.hideSpinner();
    }
  }

  handleFilterLogicUpdate(event){
    try {
      if(event.target.value){
        this.filterLogic = (event.target.value).toUpperCase();
        this.resetFilterLogicValidity();
      }
    } catch (error) {
      this.customToastNotification('Error','BatchQueryConfigurator => handleFilterLogicUpdate() ' + JSON.stringify(reduceErrors(error)), true);
    }
  }

  resetFilterLogicValidity(){
    try{
      var filterLogicInput = this.template.querySelector(`[data-id="filterlogicid"]`);
      filterLogicInput.setCustomValidity("");
      filterLogicInput.reportValidity();
    }
    catch(error) {
      this.customToastNotification('Error','BatchQueryConfigurator => resetFilterLogicValidity() ' + JSON.stringify(reduceErrors(error)), true);
    }
  }

  handleSave(){
    try{
      this.showSpinner();
      if(this.isRuleCriteriaValid){
        this.constructWhereClause();
        this.constructCustomSetting();
        console.log("🚀 ~ file: batchQueryConfigurator.js ~ line 201 ~ BatchQueryConfigurator ~ handleSave ~ this.customSettingObject", JSON.stringify(this.customSettingObject))
        
        saveCustomSetting({
          customSettingJSONString: JSON.stringify(this.customSettingObject)
        })
        .then(result => {
          //console.log("platformEventAutomationConfiguration.js ~ line 615  ~ result", JSON.stringify(result));
          console.log("Save custom setting successfull");
          this.hideSpinner();
        })
        .catch(error => {
          this.customToastNotification('Error','BatchQueryConfigurator => handleSave() => ' + JSON.stringify(reduceErrors(error)), true);
          this.showFullForm = true;
          this.hideSpinner();
        });
      }
      this.hideSpinner();
    }
    catch(error) {
      this.customToastNotification('BatchQueryConfigurator => addNewRuleConfigure() ' , JSON.stringify(reduceErrors(error)), true);
    }
  }

  constructCustomSetting(){
    try{
      var queryCriteriaRules = [];
      this.configuredRules.forEach(rule => {
        queryCriteriaRules.push({
            "sn" : rule.sn,
            "fn" : rule.fn,
            "op" : rule.op,
            "val": rule.val
        })
      });

      this.customSettingObject[this.CUSTOM_SETTING_FIELD_NAME]                = this.BATCH_CLASS_ACTIONS_TO_SETTING_UNIQUE_NAME[this.selectedBatchApex];
      this.customSettingObject[this.CUSTOM_SETTING_OBJECT]                    = this.selectedObject;
      this.customSettingObject[this.CUSTOM_SETTING_FIELD_API]                 = this.BATCH_CLASS_ACTIONS_TO_SETTING_UNIQUE_NAME[this.selectedBatchApex];
      this.customSettingObject[this.CUSTOM_SETTING_FILTER_LOGIC]              = this.filterLogic;
      this.customSettingObject[this.CUSTOM_SETTING_QUERY_FILTER_WHERE_CLAUSE] = this.whereCondition;
      this.customSettingObject[this.CUSTOM_SETTING_FIELD_QUERY_CRITERIA]      = JSON.stringify(queryCriteriaRules);
    }
    catch(error) {
      this.customToastNotification('BatchQueryConfigurator => constructCustomSetting() ' , JSON.stringify(reduceErrors(error)), true);
    }
  }

  isRuleCriteriaValid(){
    try {
      var filterLogicInput = this.template.querySelector(`[data-id="filterlogicid"]`);
      if(!filterLogicInput.value && this.configuredRules.length > 1){
        filterLogicInput.setCustomValidity("Provide custom filter logic");
        filterLogicInput.reportValidity();
      }
      else if(!filterLogicInput.value && this.configuredRules.length == 1){
        filterLogicInput.setCustomValidity("Set Filter logic as 1 if only one rule is configured");
        filterLogicInput.reportValidity();
      }
      const allValid = [...this.template.querySelectorAll("c-batch-query-rule-configuration")]
      .reduce((validSoFar, configRow) => {
          return validSoFar && configRow.validateConfigurationRow();
      }, true);

      if(!allValid){
        return false;
      }
      else{
        if(!this.filterLogic){
          console.log("🚀 ~ file: ruleCriteriaConfigurations.js ~ line 204 ~ RuleCriteriaConfigurations ~ isRuleCriteriaValid ~ this.configuredRules.length", this.configuredRules.length)
          if(!filterLogicInput.value && this.configuredRules.length > 1){
            filterLogicInput.setCustomValidity("Provide custom filter logic");
            filterLogicInput.reportValidity();
          }
          else if(!filterLogicInput.value && this.configuredRules.length == 1){
            filterLogicInput.setCustomValidity("Set Filter logic as 1 if only one rule is configured");
            filterLogicInput.reportValidity();
          }
          return false;
        }
        return true;
      }
    }
    catch(error) {
      this.customToastNotification('Error','BatchQueryConfigurator => isRuleCriteriaValid()  ' + JSON.stringify(reduceErrors(error)), true);
    }
  }

  constructWhereClause() {
    try{
      let whereClause = this.whereCondition;
      // let completeConditions = this._conditions.filter(curCondition => curCondition.fieldName && curCondition.operation && this.isValidValue(curCondition));
      let completeConditions = this.configuredRules.filter(curCondition => curCondition.fn && curCondition.op && curCondition.val);
      console.log(" conditionBuilder.js ~ line 271 ~ ConditionBuilder ~ constructWhereClause ~ completeConditions", JSON.stringify(completeConditions));
      if (completeConditions && completeConditions.length) {
          //whereClause += ' ';
            let customLogicLocal = this.buildCustomLogic(this.filterLogic);
            console.log(" conditionBuilder.js ~ line 271 ~ ConditionBuilder ~ constructWhereClause ~ customLogicLocal", JSON.stringify(customLogicLocal));

            if (customLogicLocal) {
                for (let i = 0; i < completeConditions.length; i++) {
                    const regex = new RegExp('\\$' + (i + 1) + '_', 'gi');
                    console.log("ructWhereClause ~ regex", regex);
                    
                    customLogicLocal = customLogicLocal.replace(regex, this.buildCondition(completeConditions[i]))
                    console.log("BatchQueryConfigurator ~ constructWhereClause ~ customLogicLocal", customLogicLocal)
                }
            }
            whereClause += customLogicLocal ? customLogicLocal : '';
      }
      console.log(" conditionBuilder.js ~ line 290 ~ ConditionBuilder ~ constructWhereClause ~ whereClause", whereClause);
      this.whereCondition = whereClause;
      }
    catch(error) {
      this.customToastNotification('Error','BatchQueryConfigurator => constructWhereClause()  ' + JSON.stringify(reduceErrors(error)), true);
    }
  }

  buildCustomLogic(customLogic) {
    try{
      if (customLogic) {
        const matcher = new RegExp('\\d+', 'gi');
        let matched = customLogic.match(matcher);
        if (matched) {
            matched.forEach(curMatch => {
                customLogic = customLogic.replace(curMatch, '$' + curMatch + '_')
            });
        }
    }
    return customLogic;
    }
    catch(error) {
      this.customToastNotification('BatchQueryConfigurator => buildCustomLogic() ' , JSON.stringify(reduceErrors(error)), true);
    }
  }

  buildCondition(completeCondition) {
    try{
      return ' ' + completeCondition.fn  + ' '+(completeCondition.op ? completeCondition.op : '') + ' '+completeCondition.formattedValue + ' ';
    }
    catch(error) {
      this.customToastNotification('BatchQueryConfigurator => buildCondition() ' , JSON.stringify(reduceErrors(error)), true);
    }
  }

  addNewRuleConfigure(){
    try{
      this.configuredRules.push({
          "sn"   : this.configuredRules.length + 1,
          "fn"   : '',
          "op": '',
          "val"   : ''
        });
    }
    catch(error) {
      this.customToastNotification('BatchQueryConfigurator => addNewRuleConfigure() ' , JSON.stringify(reduceErrors(error)), true);
    }
  }

  handleRuleUpdate(event){
    try {
      this.configuredRules[event.detail.index] = event.detail.rule;
     // this.updateSelectedConfiguration();
    } catch (error) {
      this.customToastNotification('Error','BatchQueryConfigurator => handleRuleUpdate() ' + JSON.stringify(reduceErrors(error)), true);
    }
  }

  handleDeleteRule(event){
    console.log('handleDeleteRule   :', event.detail);
    try {
      this.showSpinner();
      this.configuredRules.splice(event.detail, 1);
      if(this.configuredRules.length == 0){this.addNewRuleConfigure();}
      this.reEvaluateRuleOrder();
      this.hideSpinner();
    }
    catch(error) {
      this.customToastNotification('Error','BatchQueryConfigurator => handleDeleteRule() ' + JSON.stringify(reduceErrors(error)), true);
    }
  }

  reEvaluateRuleOrder(){
    try {
      var count = 1;
      this.configuredRules.forEach(thisRule =>{
        thisRule['sn'] = count;
        count++;
      });
     // this.updateSelectedConfiguration();
    }
    catch(error) {
      this.customToastNotification('Error','BatchQueryConfigurator => reEvaluateRuleOrder() ' + JSON.stringify(reduceErrors(error)), true);
    }
  }

  // updateSelectedConfiguration(){
  //   try {
  //     var configObject = {};
  //     configObject['filterLogic'] = this.filterLogic;
  //     configObject['rules']       = this.configuredRules;
  //     this.selectedPlatformEventForConfiguration["configuration"] = JSON.stringify(configObject);
  //     this.platformEventConfigurations[this.selectedPlatformEventForConfiguration.uniqueKey] = this.selectedPlatformEventForConfiguration;
  //   } catch (error) {
  //     this.customToastNotification('Error','BatchQueryConfigurator => updateSelectedConfiguration() ' + JSON.stringify(reduceErrors(error)), true);
  //   }
  // }

  childComponentError(event) {
    this.customToastNotification('Error', event.detail, true);
  }

  showSpinner() {
    this.spinnerBoolean = true;
  }

  hideSpinner() {
    this.spinnerBoolean = false;
  }

  customToastNotification(toastTitle, toastMessage, isErrorMessage) {
    var messageString = isErrorMessage ? 'Error' : 'Success';
    const showToastEvent = new ShowToastEvent({
        Title: toastTitle,
        message: toastMessage,
        variant: messageString
    });
    this.dispatchEvent(showToastEvent);
  }
}