import { LightningElement, api } from 'lwc';

export default class FieldDiffRecord extends LightningElement {
  @api item;
  @api icon;
  @api isSecondTab = false;
  @api salesforceLineItemToTierMap;
  @api ordwayLineItemToTierStringMap;
  @api newPlanPicker;

  get showTierComparison(){
    var pricingModelObject = (this.item.value).find(thisItem => thisItem["fieldAPIName"] === 'OrdwayLabs__PricingModel__c');
    var pricingModel;
    if(pricingModelObject != null && pricingModelObject != undefined){
      pricingModel = pricingModelObject.fieldValueSalesforce;
    }
    return this.isSecondTab && this.newPlanPicker && (pricingModel === 'Tiered' ||  pricingModel === 'Volume');
  }
}