import { LightningElement, api, wire, track } from 'lwc';
import getInitialWrapper from '@salesforce/apex/TierController.getInitialWrapper';
import upsertTiers from '@salesforce/apex/TierController.upsertTiers';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import CHARGETIER_OBJECT from '@salesforce/schema/ChargeTier__c';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import PRICING_TYPE from '@salesforce/schema/ChargeTier__c.PricingType__c';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class Tier extends NavigationMixin(LightningElement) {
  @api recordId;
  @api objectName;
  @track tierList = [];
  pricingTypeOptions;
  showSpinner;
  hideSave;
  showAdd;
  @track tiersToDelete = [];
  parentRecordName;
  parentRecordURL;
  parentRecord;

  objectLabelMap = {
    "OrdwayLabs__Charge__c" : "Ordway Charge",
    "OpportunityLineItem" : "Opportunity Line Item",
    "OrdwayLabs__QuoteLineItem__c" : "Ordway Quote Line Item",
    "OrdwayLabs__ContractLineItem__c" : "Ordway Contract Line Item"
  };

  childObjectMap = {
    "OrdwayLabs__Charge__c" : "Ordway Charge Tiers",
    "OpportunityLineItem" : "Opportunity Line Item Tiers",
    "OrdwayLabs__QuoteLineItem__c" : "Ordway Quote Line Item Tiers",
    "OrdwayLabs__ContractLineItem__c" : "Ordway Contract Line Item Tiers"
  };

  connectedCallback(){
    var parentRecordURLPageRef = {
      type: 'standard__recordPage',
      attributes: {
        "recordId": this.recordId,
        "actionName": "view"
      }
    };

    this[NavigationMixin.GenerateUrl](parentRecordURLPageRef)
      .then(url => this.parentRecordURL = url);
      this.getTiers();
  }

  @wire(getObjectInfo, { objectApiName: CHARGETIER_OBJECT })
    tierInfo;

    @wire(getPicklistValues, {
            recordTypeId: '$tierInfo.data.defaultRecordTypeId',
            fieldApiName: PRICING_TYPE
    })
    pricingTypeField({ data, error }) {
      if (data) {
        this.pricingTypeOptions = data.values;
      }
  }

  get hasTiers(){
    return this.tierList != undefined && this.tierList != null && this.tierList.length > 0; 
  }

  getTiers(){
    this.tierList = null;
    this.showSpinner = true;
    getInitialWrapper({ parentId : this.recordId, sObjectType : this.objectName}).then(
      result => {
        let tiers = JSON.parse(JSON.stringify(result.tierList));
        let parentRecord = JSON.parse(JSON.stringify(result.thissObject));
        this.parentRecord = parentRecord;
        if(this.parentRecord.OrdwayLabs__PricingModel__c !== 'Per Unit'){
          this.showTier = true;
        }
        this.tierList = tiers;
        this.tierList.forEach((thisTier, index) => {
          thisTier.OrdwayLabs__Tier__c = index+1;
          thisTier.isNew = false;
        });
        if(this.tierList.length == 0){
          this.handleNewRow();
        }
        this.parentRecordName = parentRecord.Name;
        this.showAdd = true;
        this.showSpinner = false;
      }
    ).catch(error => {
      this.showSpinner = false;
      this.handleError(error.message);
    });
  }

  get childObjectLabel(){
    if(this.objectName){
      return this.childObjectMap[this.objectName];
    }
  }

  get objectLabel(){
    if(this.objectName){
      return this.objectLabelMap[this.objectName];
    }
  }

  handleNewRow(){
    this.tierList = [...this.tierList];
    if(this.tierList[this.tierList.length-1]){
      this.tierList[this.tierList.length-1].OrdwayLabs__EndingUnit__c = (this.tierList[this.tierList.length-1].OrdwayLabs__StartingUnit__c * 1) + 1;
    }
    var newObject = { 
      OrdwayLabs__ListPrice__c : 0,
      OrdwayLabs__PricingType__c : "Per unit",
      OrdwayLabs__EndingUnit__c : null,
      OrdwayLabs__StartingUnit__c : this.tierList[this.tierList.length-1] ? this.tierList[this.tierList.length-1].OrdwayLabs__EndingUnit__c : 0,
      OrdwayLabs__Tier__c : this.tierList.length +1,
      isNew : true
    };
    this.tierList.push(newObject);
  }

  handleDeleteEvent(event){
    this.tierList = [...this.tierList];
    if(!this.tierList[event.detail.index].isNew){
      this.tiersToDelete.push(this.tierList[event.detail.index]);
    }
    this.tierList.splice(event.detail.index,1);
    this.tierList.forEach((thisTier, i) => {
      thisTier.OrdwayLabs__Tier__c = i+1;
    });
  }

  handleSaveEvent(event){
    if(event.detail && event.detail < 0.0){
      this.hideSave = true;
    }
    else{
      this.hideSave = false;
    }
  }

  handleFieldChange(event){
    this.tierList[event.detail.index][event.detail.fieldName] = event.detail.fieldValue;
    this.tierList = this.tierList;
  }


  handleTierSave(){

    const isAllChildValid = [...this.template.querySelectorAll('c-manage-tiers-helper')].reduce((validSoFar, childCmp) => {
      return validSoFar && childCmp.validateEndingValue();
    }, true);

      if (isAllChildValid) {
        var objectNameVar = this.objectName;
        this.showSpinner = true;
        this.tierList = [...this.tierList];
        this.tierList.forEach((thisTier, i) => {
          delete thisTier.isNew;
          delete thisTier.IsDeleted;
          delete thisTier.LastModifiedDate;
          if(objectNameVar === 'OpportunityLineItem' && !thisTier.Id){
            thisTier['OrdwayLabs__OpportunityProduct__c'] = this.recordId;
          }
          else if(objectNameVar !== 'OpportunityLineItem' && !thisTier.Id){
            thisTier[objectNameVar] = this.recordId;
          }
        });
        this.tiersToDelete.forEach((thisTier, i) => {
          delete thisTier.isNew;
          delete thisTier.IsDeleted;
          delete thisTier.LastModifiedDate;
        });
        var tierToUpsert = JSON.stringify(this.tierList);
        var tiersToDelete = this.tiersToDelete.length > 0 ? JSON.stringify(this.tiersToDelete) : null;
        upsertTiers({ tiersToUpsert: tierToUpsert, tiersToDelete: tiersToDelete, 
                      sObjectType : this.objectName, parentId : this.recordId}).then(
          result => {
            this.handleSuccess('Record updated successfully');
            this.showSpinner = false;
            this.navigateToRecordPage();
          }
        ).catch(error => {
          this.showSpinner = false;
          this.handleError(error.body.message);
        });
      } 
  }

  navigateToRecordPage() {
    window.location.replace(this.parentRecordURL);
  }

  handleError(error) {
    const evt = new ShowToastEvent({
      title: 'Error',
      message: error,
      variant: 'error',
      mode: 'sticky'
  });
  this.dispatchEvent(evt);
  }

  handleSuccess(message) {
    const evt = new ShowToastEvent({
      title: 'Success',
      message: message,
      variant: 'success',
      mode: 'dismissable'
  });
  this.dispatchEvent(evt);
  }
  
}