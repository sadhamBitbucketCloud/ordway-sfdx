import { LightningElement, track, wire , api} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import saveEntityRuleSetting from '@salesforce/apex/OrdwayEntityController.saveOrdwayEntityRuleSetting';
import getEntityRuleSetting from '@salesforce/apex/OrdwayEntityController.getOrdwayEntityRuleConfigurationInitialWrapper';

export default class OrdwayEntityRuleConfiguration extends LightningElement {
  ORDWAY_ENTITY_RULE_ID  = 'ORDWAYENTITYRULEID';
  ID_Var = 1;
  parentEventFired = false;
  @api deletedEntityIds;
  @track ordwayEntityRuleRecords = [];
  @track ordwayEntityOption;
  @track opportunityFields;
  @track accountFields;
  @track ordwayEntityRuleRecordsToDelete = [];
  @track showSpinner = true;
  @track lightningCardName ='Ordway Entity Rules';
  

  get tableStyle(){
    return "position: relative;  z-index: 5 !important; width:75%";
  }

  get entitesAvailable(){
    return this.ordwayEntityOption != null && this.ordwayEntityOption != undefined && this.ordwayEntityOption.length > 0;
  }
  
  connectedCallback() {
    this.getOrdwayEntityRuleSetting();
  }
 
  @api handleTabChangeEvent(){
    this.parentEventFired = true;
    this.getOrdwayEntityRuleSetting();
  }
  

  getOrdwayEntityRuleSetting(){
    this.showSpinner = true;
     this.ordwayEntityRuleRecordsToDelete = [];
    getEntityRuleSetting().then(
      result => {
        this.ordwayEntityRuleRecords = result.ordwayEntityRule;
        this.ordwayEntityOption      = result.ordwayEntities;
        this.opportunityFields       = result.opportunityFieldMap;
        this.accountFields           = result.accountFieldMap;
       
        if(this.parentEventFired){
          if(this.deletedEntityIds.length > 0
            && this.ordwayEntityRuleRecords != undefined 
            && this.ordwayEntityRuleRecords != null){
            for(var i=0; i<this.ordwayEntityRuleRecords.length; i++){
              if(this.ordwayEntityRuleRecords[i].OrdwayLabs__EntityID__c != undefined
               && this.deletedEntityIds.includes(this.ordwayEntityRuleRecords[i].OrdwayLabs__EntityID__c)){
                 this.ordwayEntityRuleRecords[i].OrdwayLabs__EntityID__c = undefined;
              }
            }
          }
        
          this.handleValidateInput();
        }
        this.showSpinner = false;
      }
    ).catch(error => {
      this.handleError(error.body.message);
    });
  }

  handleUpdateEntityRecord(event){
    this.ordwayEntityRuleRecords[event.detail.index][event.detail.fieldName] = event.detail.fieldValue;
    
  }

  handleDeleteEvent(event){
    this.ordwayEntityRuleRecordsToDelete.push(this.ordwayEntityRuleRecords[event.detail.index]);
    this.ordwayEntityRuleRecords.splice(event.detail.index, 1);
    this.reEvaluateOrderNumber();
  } 

  reEvaluateOrderNumber(){
    let dynamicOrder = 0;
    this.ordwayEntityRuleRecords.forEach(thisRule => {
      thisRule.Name = dynamicOrder;
      dynamicOrder++;
    });
  }

  handleNewEntity(){
    var newObject = {Id: this.ORDWAY_ENTITY_RULE_ID+this.ID_Var, Name:this.ordwayEntityRuleRecords.length};
    this.ID_Var = this.ID_Var + 1;
    this.ordwayEntityRuleRecords.push(newObject);
  }

  handleValidateInput(){

    const isAllChildValid = [...this.template.querySelectorAll('c-ordway-entity-rule-row')].reduce((validSoFar, childCmp) => {
      return validSoFar && childCmp.validateInput();
    }, true);
    
    if(isAllChildValid){
      if(this.ordwayEntityRuleRecordsToDelete != undefined 
        && this.ordwayEntityRuleRecordsToDelete != null){

        for(var i=0; i<this.ordwayEntityRuleRecordsToDelete.length; i++){
          if(this.ordwayEntityRuleRecordsToDelete[i].Id.includes(this.ORDWAY_ENTITY_RULE_ID)){
            this.ordwayEntityRuleRecordsToDelete.splice(i, 1);
          }
        }
      }

      if(this.ordwayEntityRuleRecords != undefined 
        && this.ordwayEntityRuleRecords != null){
        this.ordwayEntityRuleRecords.forEach(thisRule => {
          delete thisRule.Id;
        });
      }
   
      if(!this.parentEventFired){
        this.handleSaveSetting();
      }
      else{
        this.parentEventFired = false;
      }
    }
  }

  handleSaveSetting(){
    this.showSpinner = true;
    var settingToUpsert = JSON.stringify(this.ordwayEntityRuleRecords);
    var settingToDelete = this.ordwayEntityRuleRecordsToDelete.length > 0 ? JSON.stringify(this.ordwayEntityRuleRecordsToDelete) : null;

    saveEntityRuleSetting({ OrdwayEntityRuleToUpsert: settingToUpsert, OrdwayEntityRuleToDelete: settingToDelete}).then(
      result => {
        this.getOrdwayEntityRuleSetting();
        this.handleSuccess('Ordway entity rule configurations updated successfully');
      }
    ).catch(error => {
      this.handleError(error.body.message);
    });
  }


  handleError(error) {
    this.showSpinner = false;
      const event = new ShowToastEvent({
          title: 'Error',
          message: error,
          variant: 'error',
          mode: 'dismissable'
      });
    this.dispatchEvent(event);
  }

  handleSuccess(successMessage) {
    this.showSpinner = false;
      const event = new ShowToastEvent({
          title: 'Success',
          message: successMessage,
          variant: 'success',
          mode: 'dismissable'
      });
    this.dispatchEvent(event);
  }
}