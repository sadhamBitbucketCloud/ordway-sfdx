import { LightningElement, api, wire, track } from 'lwc';
import syncPlanToOrdway from '@salesforce/apex/OrdwayPlanController.syncToOrdway';
import ORDWAY_ICON from '@salesforce/resourceUrl/OrdwayIcon';
import getInitialWrapper from '@salesforce/apex/OrdwayPlanController.getInitialWrapper';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

import hasSyncToOrdwayPermission from '@salesforce/customPermission/SynctoOrdway';
import hasViewInOrdwayPermission from '@salesforce/customPermission/ViewinOrdway';
export default class SyncToOrdway extends NavigationMixin(LightningElement) {
  @api recordId;
  ordwayIcon = ORDWAY_ICON;
  showSyncInProgress;
  @track plan;
  namedCredential;
  externalId;
  hasCharge;
  showRefresh = false;
  entityId;
  isMultiEntityEnabled;
  namedCredential;

  connectedCallback(){
    this.getPlan();
  }

  getPlan(){
    this.showSpinner = true;
    getInitialWrapper({ planId : this.recordId}).then(
      result => {
        this.plan = result.thisPlan;
        this.externalId = this.plan.OrdwayLabs__PlanId__c;
        this.hasCharge = this.plan.OrdwayLabs__HasCharge__c > 0;
        this.entityId = this.plan.OrdwayLabs__OrdwayEntity__c;
        this.isMultiEntityEnabled = result.isMultiEntityEnabled;
        this.namedCredential = result.namedCredentials;
        this.showSpinner = false;
      }
    ).catch(error => {
      this.showSpinner = false;
      this.handleError(error.message);
    });
  }

  get disableSyncToOrdwayButton() {
    return !this.hasCharge || !hasSyncToOrdwayPermission;
  }

  get disableViewInOrdwayButton(){
    if(this.plan != undefined && this.plan != null && this.externalId && hasViewInOrdwayPermission){
      return false;
    }
    else{
      return true;
    }
  }

  handleRefresh(){
    location.reload();
  }

  handleView(){
    var URL = this.namedCredential+'/plans/'+this.externalId;
    this[NavigationMixin.Navigate]({
        "type": "standard__webPage",
        "attributes": {
            "url": URL
        }
    });
  }

  handleSync(event){
    if(this.isMultiEntityEnabled && !this.entityId){
      this.handleError('Please provide entity before syncing');
    }
    else{
      this.showSyncInProgress = true;
      syncPlanToOrdway({ recordId : this.recordId}).then(
        result => {
          this.handleSuccess('Ordway Plan was synced');
          this.showSyncInProgress = false;
          this.showRefresh = true;
        }
      ).catch(error => {
        this.showSyncInProgress = false;
        this.handleError(error.body.message);
      });
    }
  }

  handleError(error) {
    const evt = new ShowToastEvent({
      title: '',
      message: error,
      variant: 'error',
      mode: 'sticky'
  });
  this.dispatchEvent(evt);
  }

  handleSuccess(message) {
    const evt = new ShowToastEvent({
      title: '',
      message: message,
      variant: 'success',
      mode: 'dismissable'
  });
  this.dispatchEvent(evt);
  }
}