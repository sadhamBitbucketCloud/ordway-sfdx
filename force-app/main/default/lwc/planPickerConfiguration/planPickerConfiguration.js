import { LightningElement, api } from 'lwc';
import getSobjectWrapper from '@salesforce/apex/PlanPickerConfiguration.getInitialWrapper';
import deployPlanPicker from '@salesforce/apex/PlanPickerConfiguration.deployPlanPickerMetadata';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class PlanPickerConfiguration extends LightningElement {

  objectAllFieldList = [];
  @api objectFieldsList = [];
  formulaFieldList = [];
  objectOptions;
  fieldLabelValueMap;
  selectedObject;
  selectedField;
  visible;
  editable;
  ruleList = [];
  isFormulaField;
  ruleToDelete = [];
  showSpinner;

  get showTable(){
    return this.ruleList != undefined && this.ruleList != null && this.ruleList.length > 0; 
  }

  get maxColumn(){
    var counter = 0;
    for(var i=0; i<this.ruleList.length; i++)  {
      if(this.ruleList[i].Visible){
        counter++
      }
    }
    return counter; 
  }

  FIELDSTOREMOVE = ['Subtotal', 'SortOrder', 'OrdwayLabs__CalculatedTierPrice__c', 'OrdwayLabs__OrdwayProductId__c', 'OrdwayLabs__PricingType__c', 'OrdwayLabs__PlanBundleID__c', 'Name', 'OrdwayLabs__DiscountedContractValue__c'];

  objectOptions = [
    {value: 'OLI', label: 'Opportunity Products'},
    {value: 'QLI', label: 'Quote Line Item'}
  ];

  objectLabelMap = {
    "OLI" : "Opportunity Products",
    "QLI" : "Quote Line Item"
  };

  handleObjectChange(event){
    this.selectedObject = null;
    this.objectFieldsList = [];
    this.ruleList = [];
    this.objectAllFieldList = [];
    this.selectedObject = event.target.value;
    if(this.selectedObject){
      this.handleGetSobjectFields(this.selectedObject);
    }
  }
  
  handleFieldChange(event){
    this.isFormulaField = false;
    this.editable = false;
    this.visible = false;
    this.selectedField = event.target.value;
    // for(var i=0; i<this.formulaFieldList.length; i++)  {
    //   if(this.selectedField === this.formulaFieldList[i]){
    //     this.isFormulaField = true;
    //   }
    // }
  }

  handleGetSobjectFields(sObjectName){
    this.showSpinner = true;
    getSobjectWrapper({ objectApiName: sObjectName}).then(
      result => {
      console.log("🚀 ~ file: planPickerConfiguration.js ~ line 86 ~ PlanPickerConfiguration ~ handleGetSobjectFields ~ result", result)
        let objectFieldsVar = result.thisObjectFields.objectFieldsList;
        let objectFieldsListVar = JSON.parse(JSON.stringify(objectFieldsVar));
        if(result.planPickerMetadataList && result.planPickerMetadataList[0]){
          let ruleListVar = result.planPickerMetadataList[0].OrdwayLabs__Configuration__c;
          this.ruleList = JSON.parse(ruleListVar);
        }
        console.log("🚀 ~ file: planPickerConfiguration.js ~ line 93 ~ PlanPickerConfiguration ~ handleGetSobjectFields ~ this.ruleList", JSON.stringify(this.ruleList))
        //this.formulaFieldList = JSON.parse(JSON.stringify(result.formulaFieldsList));
        this.ruleList = [...this.ruleList];        
        var existingFieldsConfigured = [];
        if(this.ruleList.length > 0){
          this.ruleList.forEach((thisFieldMetadata) => {
            existingFieldsConfigured.push(thisFieldMetadata.Field);
          });
        }

        let fieldMap = new Map();
        for(var i=0; i<objectFieldsListVar.length; i++)  {
          if(!this.FIELDSTOREMOVE.includes(objectFieldsListVar[i].value)){
            this.objectAllFieldList = [...this.objectAllFieldList ,{value: objectFieldsListVar[i].value , label: objectFieldsListVar[i].label}];
            this.objectFieldsList = [... this.objectFieldsList, {value: objectFieldsListVar[i].value , label: objectFieldsListVar[i].label}]; 
          }
          fieldMap.set(objectFieldsListVar[i].value, objectFieldsListVar[i].label);
        }
        this.objectFieldsList = JSON.parse(JSON.stringify(this.objectFieldsList));
        this.objectFieldsList = this.objectFieldsList.filter(thisObjectField => !existingFieldsConfigured.includes(thisObjectField.value));

        for(var i=0; i<this.ruleList.length; i++)  {
          if(!this.ruleList[i].Visible){
            this.ruleList[i].ColumnOrder = null;
          }
          // for(var j=0; j<this.formulaFieldList.length; j++)  {
          //   if(this.ruleList[i].Field == this.formulaFieldList[j]){
          //     this.ruleList[i].isFormulaField = true;
          //   }
          // }
        }
        this.ruleList.sort((a, b) => (a.ColumnOrder > b.ColumnOrder) ? 1 : -1);
        this.fieldLabelValueMap = fieldMap;
        this.showSpinner = false;
        console.log("🚀 ~ file: planPickerConfiguration.js ~ line 127 ~ PlanPickerConfiguration ~ handleGetSobjectFields ~ this.ruleList", JSON.stringify(this.ruleList))

      }
    ).catch(error => {
      this.showSpinner = false;
      this.handleError(error.message);
    });
  }

  onChecboxChange(event) {
    if(event.target.name =='visibleCheckbox'){
      this.visible = event.target.checked;
    }
    if(event.target.name =='editableCheckbox'){
      this.editable = event.target.checked;
    }
  }

  handleColumnOrderChange(event) {
    this.ruleList = [...this.ruleList];
    var index = event.target.getAttribute("data-indexvar");
    var fieldName = event.target.name
    this.ruleList[index][fieldName] = event.target.value;
  }

  handleInlineCheckboxes(event){
    this.ruleList = [...this.ruleList];
    var index = event.target.getAttribute("data-indexvar");
    var fieldName = event.target.name
    this.ruleList[index][fieldName] = event.target.checked;
    if(fieldName === 'Visible' && !event.target.checked){
      this.ruleList[index]['ColumnOrder'] = null;
      this.amendColumnOrder();
    }
    else if(fieldName === 'Visible' && event.target.checked){
      this.amendColumnOrder();
    }
  }

  amendColumnOrder(){
    var  counter = 0;
    for(var i=0; i<this.ruleList.length; i++)  {
      if(this.ruleList[i].Visible){
        counter = counter + 1;
        this.ruleList[i]['ColumnOrder'] =  counter;
      }
    }
  }

  addDeletedField(ruleDeleted){
    this.objectAllFieldList = [...this.objectAllFieldList];
    this.objectFieldsList   = [...this.objectFieldsList];
    var objectFieldsListVar = this.objectFieldsList;
    for(var i=0; i<this.objectAllFieldList.length; i++)  {
      if(this.objectAllFieldList[i].value === ruleDeleted.Field){
        objectFieldsListVar.push(this.objectAllFieldList[i]);
      }
    }
    objectFieldsListVar.sort((a, b) => (a.label > b.label) ? 1 : -1);
    this.objectFieldsList = objectFieldsListVar;
  }

  removeAddedField(fieldToRemove){
    let objectFieldsListVar = this.objectFieldsList;
    this.objectFieldsList = [...this.objectFieldsList];
    for(var i=0; i<objectFieldsListVar.length; i++)  {
      if(objectFieldsListVar[i].value === fieldToRemove){
        this.objectFieldsList.splice(i,1);
        break;
      }
    }
    this.objectFieldsList.sort((a, b) => (a.label > b.label) ? 1 : -1);
  }

  handleNewRow(event){
    if(!this.selectedField){
      this.handleError('Please Select a Field To Add');
    }
    if(this.selectedField){
      var counter = 0;
      for(var i=0; i<this.ruleList.length; i++)  {
        if(this.ruleList[i].Visible){
          counter++;
        }
      }

        var newObject = { 
          MasterLabel : this.fieldLabelValueMap.get(this.selectedField),
          Edit : this.editable,
          Field : this.selectedField,
          Visible : true,
          ColumnOrder : counter + 1,
          isFormulaField : this.isFormulaField
        };
      this.ruleList = [...this.ruleList];
      this.ruleList.push(newObject);
      this.removeAddedField(this.selectedField);
      this.clearValues();
    }
  }

  handleUpdate(){

    var isAllValid = true;
    this.ruleList = [...this.ruleList];
    for(var i=0; i<this.ruleList.length; i++)  {
      if(!this.ruleList[i].ColumnOrder && this.ruleList[i].Visible){
        isAllValid = false;
        break;
      }
    }

    const isValid = [...this.template.querySelectorAll('.columnInputField')].reduce((validSoFar, inputCmp) => {
      inputCmp.reportValidity(); 
      return validSoFar && inputCmp.checkValidity();
    }, true);

    if(isAllValid && isValid){
      var ruleList = [];
      this.ruleList.forEach((thisRule) => {
        if(thisRule.Visible){
          ruleList.push(thisRule);
        }
      });

      ruleList.sort((a, b) => (a.ColumnOrder > b.ColumnOrder) ? 1 : -1);
      this.ruleList = ruleList;
      this.showSpinner = true;
      deployPlanPicker({ configurationJSON : JSON.stringify(ruleList), objectName : this.selectedObject}).then(
        result => {
          if(result){
            this.deployedStatusToastNotification(result);
          }
          this.showSpinner = false;
        }
      ).catch(error => {
        this.showSpinner = false;
        this.handleError(error.body.message);
      });
    }

    if(!isAllValid){
      this.handleError('Please provide the column number,if the field is visible.');
      this.showSpinner = false;
    }
  }
  
  clearValues(){
    this.selectedField = null;
  }

  handleError(error) {
    const evt = new ShowToastEvent({
      title: 'Error',
      message: error,
      variant: 'error',
      mode: 'sticky'
  });
  this.dispatchEvent(evt);
  }

  handleSuccess(message) {
    const evt = new ShowToastEvent({
      title: 'Success',
      message: message,
      variant: 'success',
      mode: 'sticky'
  });
  this.dispatchEvent(evt);
  }

  deployedStatusToastNotification(deploymentStatusURL) {
    const event = new ShowToastEvent({
      "variant": 'success',
      "mode":'sticky',
      "message": "Allow from 5-10 minutes for your changes to deploy on the server before using the application. {0}!",
      "messageData": [
          {
              url: deploymentStatusURL,
              label: 'Click here to view the deployment status'
          }
      ]
  });
  this.dispatchEvent(event);
  }
}