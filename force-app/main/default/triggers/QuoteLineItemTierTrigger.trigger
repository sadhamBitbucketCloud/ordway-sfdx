trigger QuoteLineItemTierTrigger on QuoteLineItemTier__c (before insert, before delete) {
  if (!LoadLeanSettingController.isTriggerDisabled()) {
    TriggerHandler.createAndExecuteHandler(OrdwayQuoteLineItemTierTriggerHandler.class);
  }
}