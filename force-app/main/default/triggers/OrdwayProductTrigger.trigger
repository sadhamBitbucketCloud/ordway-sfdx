trigger OrdwayProductTrigger on OrdwayProduct__c (before insert, before update, after insert, after update) {
  TriggerHandler.createAndExecuteHandler(OrdwayProductTriggerHandler.class);
}