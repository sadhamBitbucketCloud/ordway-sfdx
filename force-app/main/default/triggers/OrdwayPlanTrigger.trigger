/**
 * @File Name          : OrdwayPlanTrigger.trigger
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    3/7/2020   Ordway Labs     Initial Version
 **/
trigger OrdwayPlanTrigger on Plan__c(before insert, before update) {
  TriggerHandler.createAndExecuteHandler(PlanTriggerHandler.class);
}