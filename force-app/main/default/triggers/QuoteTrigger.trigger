/**
 * @File Name          : QuoteTrigger.trigger
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/27/2020        Initial Version
 **/
trigger QuoteTrigger on Quote__c(
  before insert,
  before update,
  after insert,
  after update,
  before delete,
  after delete
) {
  if (!LoadLeanSettingController.isTriggerDisabled()) {
    if (QuoteTriggerHandler.executeTrigger(Trigger.newMap)) {
      TriggerHandler.createAndExecuteHandler(QuoteTriggerHandler.class);
    }

    if (
      QuoteTriggerHelper_Callout.runOnce &&
      !System.isBatch() &&
      !System.isFuture() &&
      OpportunitySettingController.syncQuoteAutomatically()
    ) {
      //Helper Class To Make Callouts
      TriggerHandler.createAndExecuteHandler(QuoteTriggerHelper_Callout.class);
    }
  }
}