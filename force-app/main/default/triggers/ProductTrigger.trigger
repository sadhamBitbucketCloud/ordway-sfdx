trigger ProductTrigger on Product2 (after insert, after update) {
  TriggerHandler.createAndExecuteHandler(ProductTriggerHandler.class);
}