/**
 * @description       :
 * @author            :
 * @group             :
 * @last modified on  : 09-19-2020
 * @last modified by  : 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   08-24-2020      Initial Version
 **/
trigger ContractLineItemTrigger on ContractLineItem__c(
  before insert,
  before update
) {
  if (UserInfo.isMultiCurrencyOrganization()) {
    Set<Id> contractIds = new Set<Id>();
    for (ContractLineItem__c thisCLI : Trigger.new) {
      contractIds.add(thisCLI.OrdwayContract__c);
    }

    Map<Id, OrdwayContract__c> contractMap = OrdwayContractService.getOrdwayContract(
      contractIds
    );

    for (ContractLineItem__c thisCLI : Trigger.new) {
      thisCLI.put(
        'CurrencyIsoCode',
        contractMap.get(thisCLI.OrdwayContract__c).get('CurrencyIsoCode')
      );
    }
  }

}