/**
 * @File Name          : OrdwayContractTrigger.trigger
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    10/12/2019   Ordway Labs     Initial Version
 **/
trigger OrdwayContractTrigger on OrdwayContract__c(
  before insert,
  before update,
  after update,
  before delete
) {
  if (!LoadLeanSettingController.isTriggerDisabled()) {
    TriggerHandler.createAndExecuteHandler(OrdwayContractTriggerHandler.class);
    if (
      OrdwayContractTriggerHelper.runOnce &&
      !System.isBatch() &&
      !System.isFuture() &&
      OpportunitySettingController.isAutomaticLinkEnabled()
    ) {
      //Helper Class To Make Callouts
      TriggerHandler.createAndExecuteHandler(OrdwayContractTriggerHelper.class);
    }
  }
}