trigger OrdwayChargeTrigger on OrdwayLabs__Charge__c (before insert, before update) {
  if (!LoadLeanSettingController.isTriggerDisabled()) {
    TriggerHandler.createAndExecuteHandler(OrdwayChargeTriggerHandler.class);
  }
}