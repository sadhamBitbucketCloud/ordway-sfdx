trigger UsageTrigger on OrdwayUsage__c (before insert) {

  if ( !LoadLeanSettingController.isTriggerDisabled() ) {
    TriggerHandler.createAndExecuteHandler(UsageTriggerHandler.class);
  }
}