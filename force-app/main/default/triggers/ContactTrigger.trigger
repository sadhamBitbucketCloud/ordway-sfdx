/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 10-19-2020
 * @last modified by  : 
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   09-25-2020      Initial Version
**/
trigger ContactTrigger on Contact (
  before insert,
  before update,
  after insert,
  after update,
  before delete,
  after delete
) {
  if (
    !LoadLeanSettingController.isTriggerDisabled()
    && ContactTriggerHandler.executeTrigger(Trigger.newMap)
  ) {
    TriggerHandler.createAndExecuteHandler(ContactTriggerHandler.class);
  }
}