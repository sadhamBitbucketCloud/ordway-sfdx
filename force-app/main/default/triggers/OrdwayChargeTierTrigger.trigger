trigger OrdwayChargeTierTrigger on OrdwayLabs__ChargeTier__c (before insert, before delete) {
  if (!LoadLeanSettingController.isTriggerDisabled()) {
    TriggerHandler.createAndExecuteHandler(OrdwayChargeTierTriggerHandler.class);
  }
}