/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 10-21-2020
 * @last modified by  : 
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   09-19-2020      Initial Version
**/
trigger AccountTrigger on Account(before insert, before update, after insert, after update) {
  if (
    !LoadLeanSettingController.isTriggerDisabled() &&
    AccountTriggerHandler.executeTrigger(Trigger.newMap)
  ) {
    TriggerHandler.createAndExecuteHandler(AccountTriggerHandler.class);
  }
}