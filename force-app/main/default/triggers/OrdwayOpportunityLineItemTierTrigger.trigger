trigger OrdwayOpportunityLineItemTierTrigger on OrdwayLabs__OpportunityLineItemTier__c (before insert, before delete) {
  if (!LoadLeanSettingController.isTriggerDisabled()) {
    TriggerHandler.createAndExecuteHandler(OrdwayOppLineItemTierTriggerHandler.class);
  }
}