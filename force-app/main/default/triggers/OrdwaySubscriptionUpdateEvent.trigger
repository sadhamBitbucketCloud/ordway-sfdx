/**
 * @File Name          : OrdwaySubscriptionUpdateEvent.trigger
 * @Description        :
 * @Author             :
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    3/31/2020        Initial Version
 **/
trigger OrdwaySubscriptionUpdateEvent on OrdwaySubscriptionUpdate__e(
  after insert
) {
  Set<String> subscriptionId = new Set<String>();
  Set<String> recordIds = new Set<String>();
  String dummyToken = '';

  for (OrdwaySubscriptionUpdate__e thisEvent : Trigger.new) {
    subscriptionId.add(thisEvent.OrdwaySubscriptionID__c);
    recordIds.add(thisEvent.RecordId__c.substringBefore('_'));
    if (dummyToken == '') {
      dummyToken = thisEvent.RecordId__c.substringAfter('_');
    }
  }

  Map<Id, OrdwayContract__c> ordwayContractMap = new Map<Id, OrdwayContract__c>(
    [
      SELECT Id, SyncStatus__c
      FROM OrdwayContract__c
      WHERE OrdwaySubscriptionID__c IN :subscriptionId AND Id IN :recordIds
    ]
  );

  //Update only if the record exists
  if (!ordwayContractMap.values().isEmpty()) {
    if (OpportunitySettingController.isAutoSyncFromOrdway()) {
      OrdwayContractController.updateContractFromOrdway(
        ordwayContractMap.values()[0].Id,
        dummyToken
      );
    } else {
      //Update a field to enable the button
      List<OrdwayContract__c> contractList = new List<OrdwayContract__c>();

      for (OrdwayContract__c thisOrdwayContract : ordwayContractMap.values()) {
        thisOrdwayContract.SyncStatus__c = OrdwayHelper.SYNC_STATE_NOT_IN_SYNC;
        contractList.add(thisOrdwayContract);
      }

      //There is no need to execute the trigger on this update, so turning it off
      OrdwayContractTriggerHelper.runOnce = false;
      SObjectAccessDecision decision = Security.stripInaccessible(
        AccessType.UPDATABLE,
        contractList
      );
      update decision.getRecords();
    }
  }
}