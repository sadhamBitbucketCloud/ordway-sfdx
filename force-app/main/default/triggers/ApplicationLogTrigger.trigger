/**
 * @File Name          : ApplicationLogTrigger.trigger
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    11/24/2019   Ordway Labs     Initial Version
 **/
trigger ApplicationLogTrigger on ApplicationLog__c(before insert) {
  TriggerHandler.createAndExecuteHandler(ApplicationLogHandler.class);
}