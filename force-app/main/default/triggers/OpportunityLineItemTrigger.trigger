/**
 * @File Name          : OpportunityLineItemTrigger.trigger
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    11/9/2019   Ordway Labs     Initial Version
 **/

trigger OpportunityLineItemTrigger on OpportunityLineItem(
  before insert,
  before update,
  after insert,
  after update,
  before delete,
  after delete
) {
  if (!LoadLeanSettingController.isTriggerDisabled()) {
    TriggerHandler.createAndExecuteHandler(
      OpportunityLineItemTriggerHandler.class
    );
  }
}