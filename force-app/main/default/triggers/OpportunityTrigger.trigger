/**
 * @File Name          : OpportunityTrigger.trigger
 * @Description        :
 * @Author             : Ordway Labs
 * @Group              :
 * @Last Modified By   : 
 * @Last Modified On   : 09-19-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    10/12/2019   Ordway Labs     Initial Version
 **/
trigger OpportunityTrigger on Opportunity(
  before insert,
  before update,
  after insert,
  after update,
  before delete,
  after delete
) {
  if (
    !LoadLeanSettingController.isTriggerDisabled() &&
    OpportunityTriggerHandler.executeTrigger(Trigger.newMap)
  ) {
    TriggerHandler.createAndExecuteHandler(OpportunityTriggerHandler.class);
  }
}