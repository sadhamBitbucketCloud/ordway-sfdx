trigger OrdwayContractLineItemTierTrigger on ContractLineItemTier__c (before insert, before delete) {
  if (!LoadLeanSettingController.isTriggerDisabled()) {
    TriggerHandler.createAndExecuteHandler(OrdwayContractLineItemTierTriggerHandler.class);
  }
}